import { Component, Injector, OnInit, ViewContainerRef, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AppUiCustomizationService } from '@shared/common/ui/app-ui-customization.service';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
    templateUrl: './privacy.component.html',
    encapsulation: ViewEncapsulation.None
})
export class PrivacyComponent extends AppComponentBase {

    public constructor(
        injector: Injector,
        private _router: Router,
        private _uiCustomizationService: AppUiCustomizationService,
        viewContainerRef: ViewContainerRef
    ) {
        super(injector);
        // We need this small hack in order to catch application root view container ref for modals
        // this.viewContainerRef = viewContainerRef;
    }
}
