import { NgModule } from '@angular/core';
import { NavigationEnd, Router, RouterModule } from '@angular/router';
import { AppUiCustomizationService } from '@shared/common/ui/app-ui-customization.service';
import { PrivacyComponent } from './privacy.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: PrivacyComponent,
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class PrivacyRoutingModule {
    constructor(
        private router: Router,
        private _uiCustomizationService: AppUiCustomizationService
    ) {
        router.events.subscribe((event: NavigationEnd) => {
            setTimeout(() => {
                // this.toggleBodyCssClass(event.url);
            }, 0);
        });
    }

    
}
