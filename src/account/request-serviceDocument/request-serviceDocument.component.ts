import { Component, HostListener, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { accountModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ServiceDocumentRequestDto, ServiceDocumentRequestServiceProxy, QuotationsServiceProxy, SaveCustomerSignature, SignatureRequestDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import SignaturePad from 'signature_pad';
import { NgxSpinnerService } from 'ngx-spinner';
import { AppConsts } from '@shared/AppConsts';
import { FileUpload } from 'primeng';

@Component({
    templateUrl: './request-serviceDocument.component.html',
    styleUrls: ['./request-serviceDocument.component.less'],
    animations: [accountModuleAnimation()]
})

export class RequestServiceDocumentComponent extends AppComponentBase implements OnInit {

    uploadUrl: string;
    // @ViewChild('ExcelFileUpload', { static: false }) excelFileUpload: FileUpload;
    constructor(
        injector: Injector,
        private _httpClient: HttpClient,
        private _activatedRoute: ActivatedRoute,
        private _documentRequestServiceProxy: ServiceDocumentRequestServiceProxy,
        private spinner: NgxSpinnerService) {
        super(injector);
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/api/ServiceDocument/UploadDocuments';
    }

    //@Input() name: string;
    // @ViewChild('sigPad') sigPad;
    // sigPadElement;
    // context;
    // isDrawing = false;
    // img;
    // TD: number;
    // JD: number;
    // QD: number;
    
    
    // expired: boolean = false;
    // isChange = false;
    // ipAddress: any;
    // lat: any;
    // lng: any;
    // lat1: any;
    // lng1: any;
    // saving = false;
    // signatureImage;
    // signatrefile;
    // checkboxtrue = false;
    // signaturePad;

    // itemModel: any = [];
    // Submit: boolean = false;

    // aDeclarationShow: boolean = false;
    // aDeclarationBody: any;

    STR: string;
    documentData: ServiceDocumentRequestDto = new ServiceDocumentRequestDto();
    saveDoc: boolean = false;

    images = [];
    fileList: File[] = [];
    fileName: any;

    ngOnInit(): void {
        debugger;
        this.STR = this._activatedRoute.snapshot.queryParams['STR'];

        this._documentRequestServiceProxy.serviceDocumentRequestData(this.STR).subscribe(result => {
            this.documentData = result;

            this.fileName = this.documentData.serviceDocumnetType.replace(/\s/g, "")
        });
    }
 
    save() {
        this.showMainSpinner();
        const formData = new FormData();
        if(this.fileList.length > 0)
        {
            for (let i = 0; i < this.fileList.length; i++) {
    
                const file = this.fileList[i];
                let name = this.fileName + "_" + i;
                formData.append(name, file, file.name);
            }
            this._httpClient
            .post<any>(this.uploadUrl + "?str=" + encodeURIComponent(this.STR), formData)
            .subscribe(response => {
                if (response.success) {
                    this.saveDoc = true;
                } else if (response.error != null) {
                    //this.notify.error(this.l('ImportInvertersUploadFailed'));
                }
                this.hideMainSpinner();
            }, e => { this.hideMainSpinner(); });
        }
        else{
            this.notify.info("Please Select Image To Upload.", "Empty");
        }
        // if(this.checkboxtrue == true)
        // {
        //     this.saving = true;
        //     this.spinner.show();
        //     const input = new SaveCustomerSignature();
        //     input.encString = this.STR;
        //     input.imageData = this.signaturePad.toDataURL("image/jpeg");
        //     input.custSignLatitude = this.lat;
        //     input.custSignLongitude = this.lng;
        //     input.custSignIP = this.ipAddress;
        //     this.img = this.signaturePad.toDataURL("image/png");
        //     this._quotationsServiceProxy.saveSignature(input)
        //         .subscribe(() => {
        //             this.saving = false;
        //             this.spinner.hide();
        //             this.ngOnInit();
        //             this.Submit = true;
        //             //this.notify.info(this.l('SavedSuccessfully'));
        //         }, e => { this.notify.error("Something is wrong.");this.spinner.hide(); });
        // }
        // else {
        //     this.message.warn("Please Accept term And Condition First");
        // }
    }
    
    
        
    onFileChange(event) {
        if (event.target.files && event.target.files[0]) {
            var filesAmount = event.target.files.length;
            for (let i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = (event: any) => {
                    console.log(event.target.result);
                    this.images.push(event.target.result);
                }

                reader.readAsDataURL(event.target.files[i]);

                const file = event.target.files[i];
                // let name = this.fileName + "_" + i;
                // this.formData.append(name, file, file.name);
                this.fileList.push(file)
            }
        }
    }

    removeImage(index): void {
        this.images.splice(index,1);
        this.fileList.splice(index, 1);
        // let name = this.fileName + "_" + this.images.indexOf(url);
        // this.formData.delete(name);
    }
}

