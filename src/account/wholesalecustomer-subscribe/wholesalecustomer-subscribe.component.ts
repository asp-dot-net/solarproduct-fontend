import { Component, HostListener, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { accountModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { PromotionsServiceProxy, QuotationsServiceProxy, SaveCustomerSignature, SignatureRequestDto, WholeSalePromotionsServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './wholesalecustomer-subscribe.component.html',
    styleUrls: ['./wholesalecustomer-subscribe.component.less'],
    animations: [accountModuleAnimation()]
})

export class WholeSaleCustomerSubScribeComponent extends AppComponentBase implements OnInit {

    constructor(
        injector: Injector,
        private _httpClient: HttpClient,
        private _activatedRoute: ActivatedRoute,
        private _quotationsServiceProxy: QuotationsServiceProxy,
        private _promotionsServiceProxy: WholeSalePromotionsServiceProxy) {
        super(injector);
        // lat:any;
        // lng:any;
    }

    @Input() name: string;
    @ViewChild('sigPad') sigPad;
    sigPadElement;
    context;
    isDrawing = false;
    img;
    TD: number;
    JD: number;
    QD: number;
    STR: string;
    quotationData: SignatureRequestDto = new SignatureRequestDto();
    expired: boolean = false;
    isChange = false;
    ipAddress: any;
    lat: any;
    lng: any;
    saving = false;
    re: any;

    ngOnInit(): void {
        debugger;
        // this.sigPadElement = this.sigPad.nativeElement;
        // this.context = this.sigPadElement.getContext('2d');
        // this.context.strokeStyle = '#3742fa';
        this.STR = this._activatedRoute.snapshot.queryParams['STR'];
        debugger;
        this._promotionsServiceProxy.subScribeUnsubscribepromo(this.STR, 1).subscribe(result => {
            this.re = result;
            // if (result.custName == undefined) {
            //     this.expired = true;
            // }
            // else {
            //     this.quotationData = result;
            // }
        });

        if (navigator) {
            debugger;
            navigator.geolocation.watchPosition(pos => {
                this.lng = + pos.coords.longitude;
                this.lat = + pos.coords.latitude;
            });
        }
        const xhr = new XMLHttpRequest();
        const url = 'http://api.ipify.org/?format=Text';
        xhr.open('GET', url, false); // false for synchronous request
        xhr.send(null);
        this.ipAddress = xhr.responseText;
        // this._httpClient.get("http://api.ipify.org/?format=json").subscribe((res:any)=>{
        //     this.ipAddress = res.ip;
        // });
    }

    ngAfterViewInit(): void {
        this.sigPadElement = this.sigPad.nativeElement;
        this.context = this.sigPadElement.getContext('2d');
        this.context.strokeStyle = '#3742fa';
    }

    @HostListener('document:mouseup', ['$event'])
    onMouseUp(e) {
        this.isDrawing = false;
    }

    onMouseDown(e) {
        this.isDrawing = true;
        const coords = this.relativeCoords(e);
        this.context.moveTo(coords.x, coords.y);
    }

    onMouseMove(e) {
        if (this.isDrawing) {
            const coords = this.relativeCoords(e);
            this.context.lineTo(coords.x, coords.y);
            this.context.stroke();
        }
    }

    private relativeCoords(event) {
        const bounds = event.target.getBoundingClientRect();
        const x = event.clientX - bounds.left;
        const y = event.clientY - bounds.top;
        return { x: x, y: y };
    }

    clear() {
        this.context.clearRect(0, 0, this.sigPadElement.width, this.sigPadElement.height);
        this.context.beginPath();
    }


}
