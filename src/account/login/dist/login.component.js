"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.LoginComponent = void 0;
var core_1 = require("@angular/core");
var routerTransition_1 = require("@shared/animations/routerTransition");
var app_component_base_1 = require("@shared/common/app-component-base");
var UrlHelper_1 = require("shared/helpers/UrlHelper");
var AppConsts_1 = require("@shared/AppConsts");
var _ = require("lodash");
var LoginComponent = /** @class */ (function (_super) {
    __extends(LoginComponent, _super);
    function LoginComponent(injector, loginService, _router, _sessionService, _sessionAppService, _reCaptchaV3Service) {
        var _this = _super.call(this, injector) || this;
        _this.loginService = loginService;
        _this._router = _router;
        _this._sessionService = _sessionService;
        _this._sessionAppService = _sessionAppService;
        _this._reCaptchaV3Service = _reCaptchaV3Service;
        _this.submitting = false;
        _this.isMultiTenancyEnabled = _this.multiTenancy.isEnabled;
        _this.recaptchaSiteKey = AppConsts_1.AppConsts.recaptchaSiteKey;
        _this.tenantChangeDisabledRoutes = [
            'select-edition',
            'buy',
            'upgrade',
            'extend',
            'register-tenant',
            'stripe-purchase',
            'stripe-subscribe',
            'stripe-update-subscription',
            'paypal-purchase',
            'stripe-payment-result',
            'payment-completed',
            'stripe-cancel-payment',
            'session-locked'
        ];
        return _this;
    }
    Object.defineProperty(LoginComponent.prototype, "multiTenancySideIsTeanant", {
        get: function () {
            return this._sessionService.tenantId > 0;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(LoginComponent.prototype, "isTenantSelfRegistrationAllowed", {
        get: function () {
            return this.setting.getBoolean('App.TenantManagement.AllowSelfRegistration');
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(LoginComponent.prototype, "isSelfRegistrationAllowed", {
        get: function () {
            if (!this._sessionService.tenantId) {
                return false;
            }
            return this.setting.getBoolean('App.UserManagement.AllowSelfRegistration');
        },
        enumerable: false,
        configurable: true
    });
    LoginComponent.prototype.showTenantChange = function () {
        var _this = this;
        if (!this._router.url) {
            return false;
        }
        if (_.filter(this.tenantChangeDisabledRoutes, function (route) { return _this._router.url.indexOf('/account/' + route) >= 0; }).length) {
            return false;
        }
        return abp.multiTenancy.isEnabled && !this.supportsTenancyNameInUrl();
    };
    LoginComponent.prototype.supportsTenancyNameInUrl = function () {
        return (AppConsts_1.AppConsts.appBaseUrlFormat && AppConsts_1.AppConsts.appBaseUrlFormat.indexOf(AppConsts_1.AppConsts.tenancyNamePlaceHolderInUrl) >= 0);
    };
    LoginComponent.prototype.ngOnInit = function () {
        this.loginService.init();
        if (this._sessionService.userId > 0 && UrlHelper_1.UrlHelper.getReturnUrl() && UrlHelper_1.UrlHelper.getSingleSignIn()) {
            this._sessionAppService.updateUserSignInToken()
                .subscribe(function (result) {
                var initialReturnUrl = UrlHelper_1.UrlHelper.getReturnUrl();
                var returnUrl = initialReturnUrl + (initialReturnUrl.indexOf('?') >= 0 ? '&' : '?') +
                    'accessToken=' + result.signInToken +
                    '&userId=' + result.encodedUserId +
                    '&tenantId=' + result.encodedTenantId;
                location.href = returnUrl;
            });
        }
        var state = UrlHelper_1.UrlHelper.getQueryParametersUsingHash().state;
        if (state && state.indexOf('openIdConnect') >= 0) {
            this.loginService.openIdConnectLoginCallback({});
        }
    };
    LoginComponent.prototype.login = function () {
        debugger;
        var _this = this;
        var recaptchaCallback = function (token) {
            _this.showMainSpinner();
            _this.submitting = true;
            _this.loginService.authenticate(function () {
                _this.submitting = false;
                _this.hideMainSpinner();
            }, null, token);
        };
        if (this.useCaptcha) {
            this._reCaptchaV3Service.execute(this.recaptchaSiteKey, 'login', function (token) {
                recaptchaCallback(token);
            });
        }
        else {
            recaptchaCallback(null);
        }
    };
    LoginComponent.prototype.externalLogin = function (provider) {
        this.loginService.externalAuthenticate(provider);
    };
    Object.defineProperty(LoginComponent.prototype, "useCaptcha", {
        get: function () {
            return this.setting.getBoolean('App.UserManagement.UseCaptchaOnLogin');
        },
        enumerable: false,
        configurable: true
    });
    LoginComponent = __decorate([
        core_1.Component({
            templateUrl: './login.component.html',
            animations: [routerTransition_1.accountModuleAnimation()],
            styleUrls: ['./login.component.less']
        })
    ], LoginComponent);
    return LoginComponent;
}(app_component_base_1.AppComponentBase));
exports.LoginComponent = LoginComponent;
