import { Component, HostListener, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { accountModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { QuotationsServiceProxy, SaveCustomerSignature, SignatureRequestDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import SignaturePad from 'signature_pad';
import { NgxSpinnerService } from 'ngx-spinner';
import { AppConsts } from '@shared/AppConsts';

@Component({
    templateUrl: './efficiency-declaration.component.html',
    styleUrls: ['./efficiency-declaration.component.less'],
    animations: [accountModuleAnimation()]
})

export class EfficiencyDeclarationComponent extends AppComponentBase implements OnInit {

    constructor(
        injector: Injector,
        private _httpClient: HttpClient,
        private _activatedRoute: ActivatedRoute,
        private _quotationsServiceProxy: QuotationsServiceProxy,
        private spinner: NgxSpinnerService) {
        super(injector);
        //lat:any;
        //lng:any;
    }

    @Input() name: string;
    @ViewChild('sigPad') sigPad;
    sigPadElement;
    context;
    isDrawing = false;
    img;
    TD: number;
    JD: number;
    QD: number;
    STR: string;
    quotationData: SignatureRequestDto = new SignatureRequestDto();
    expired: boolean = false;
    isChange = false;
    ipAddress: any;
    lat: any;
    lng: any;
    lat1: any;
    lng1: any;
    saving = false;
    signatureImage;
    signatrefile;
    checkboxtrue = false;
    signaturePad;

    itemModel: any = [];
    Submit: boolean = false;

    aDeclarationShow: boolean = false;
    aDeclarationBody: any;

    ngOnInit(): void {
        this.STR = this._activatedRoute.snapshot.queryParams['STR'];
        this._quotationsServiceProxy.quotationData(this.STR).subscribe(result => {
            this.quotationData = result;
            this.quotationData.orgLogo = result.orgLogo;
            this.quotationData.orgLogo = this.quotationData.orgLogo != null && this.quotationData.orgLogo != "" ? (AppConsts.docUrl + this.quotationData.orgLogo) : "" ;
            //console.log(result);

            this.aDeclarationBody = "";
            if(this.quotationData.organizationId == 7)
            {
                this.aDeclarationBody = "<p>This Is An Agreement Between ARISE SOLAR PTY LTD (ABN 32168697907) Pty Ltd (ABN 32168697907) Referred To As We/Us, And The Customer Named In The Quote, Referred To As You/Your/Yourself, To Supply And Install A Solar PV System, Referred To As System, For You..</p>";
                this.aDeclarationBody += "<p>1) ACCEPTANCE TO THIS AGREEMENT: This Agreement Would Commence Once You Accept The Offer As Put Forward, By Us, In This Quote By Either I) Signing This Quote And Sending It Back To Us Via Post Or Email/Digitally Or Ii) Paying The Deposit Needed And/Or Instructing Us To Arrange For The System To Be Installed.</p>";
                this.aDeclarationBody += "<p>2) YOU HEREBY CONFIRM THAT: I) Have Completed 18 Years Of Age, And II) Are A Registered Property Owner Or You Are Authorized By All Registered Owners To Enter Into This Agreement; And III) You Have Read, Understood, And Accepted The Requirements As Mentioned In This Agreement.</p>";
                this.aDeclarationBody += "<p>3) PAYMENT: I) Payments Under This Agreement Can Be Made By EFT, Cash, PayID or Debid/Credit Card. II) The 'Deposit' Above Must Be Paid In Full At The Same Time As You Accept Our Offer; III) You Must Pay to Arise Solar The 'Balance (Including GST)' Prior To Installation.</p>";
                this.aDeclarationBody += "<p>4) SMALL SCALE TECHNOLOGY CERTIFICATES (STCs): I) You As A Purchaser Of The System Assign All The Rights To Create STCs To Arise Solar And Authorize Arise Solar To Create, Apply And Retain STCs And All Financial Benefits Or Value Attributable To Them. II) You Also Acknowledge And Agree That The Installation Portion Of The Price On The Order confirmation Has Been Calculated On The Basis That The Australian Government Clean Energy Regulator Will approve STCs. In Case For Whatsoever Reason Arise Solar Determines That The STCs Are Not, Or Will Not Be, Available To Arise Solar As Anticipated By Arise Solar When Calculating The Price, Or If You Refuse To Sign The STC Paperwork (I.E. Solar PV STC Assignment Form), Then You Agree To Pay An Additional Amount To Reflect The Value Of Such Benets That Are Not, Or Will Not, Be Available To Arise Solar.</p>";
                this.aDeclarationBody += "<p>5) APPLICATION FOR GRID CONNECTION: Arise Solar Will Be Applying For Your Grid Connection Approval On Your Behalf As Soon As We Receive The Required Information From You. The Agreement Depends Upon The Grid Connection Approval Being Granted. A delay In Approval May Result In A Delay In The Targeted System Installation Date.</p>";
                this.aDeclarationBody += "<p>6) SYSTEM DELIVERY AND INSTALLATION: I) Arise Solar Would Take All Necessary Actions In Order To Make Sure That We Deliver And Install Your System On The Agreed-Upon Date. II) You, However, Understand And Agree That The Agreed Date Is Only A Target Date And Not A Strict Deadline, And Arise Solar Cannot Be Held Liable To You If The System Doesn’t Get Delivered And Installed At The Installation Address By The Agreed Date. III) You May Choose To Specify Your Preference About The Location Of Solar Panels And Inverters Before The Commencement Of Any Work, Beforehand, Subject To The Provisions In This Agreement. IV) System Installation Would Be Executed As Per The Design And Specification As Shared By Arise Solar Until And Unless The Situations That Could Not Have Been Reasonably Anticipated By Arise Solar May Require A New Design Or Specification Which May Result In A Change In The Price. You Or Your Representative Must Be Present At The Premises For Any Site Inspection And For The Delivery And Installation Of The System.</p>";
                this.aDeclarationBody += "<p>7) CEC ACCREDITED INSTALLER: We Contract A Clean Energy Council Accredited Independent Licensed Electrician/Installer To Install Your System. We Agree To Provide You With Details Of Your Installer Prior To The Commencement Of Any Work Upon Request.</p>";
                this.aDeclarationBody += "<p>8) SITE ACCESS: You Grant Arise Solar And Our Installer/S The Permission, Sucient Access, And/Or Possession Of The Site As Is Necessary To Complete Site Inspections, Delivery, And The Installation Of The System And Its Components. Failure To This Access Or Restrictions Resulting In The Complication In The System Installation Works May Result In A Change In The Price.</p>";
                this.aDeclarationBody += "<p>9) METER: I) If The Meter Box Is Located At a Distance From The Inverter Installation Location, The Customer Will Have To Arrange And Pay For The Additional Work if not quoted prior by Arise Solar. II) Additional Costs In Relation To The Reconfiguration Or Change Of Your Meter To a Net/Solar Meter As Part Of The Grid Connection Process Which Comes From Your Power Distributor Or Electricity Retailer Are Not Mentioned In This Arise Solar Quotation. Arise Solar Would Not Be Liable For Any Compensation Due To Delay In Relation To The Reconfiguration Or Change Of Your Meter To Net/Solar Meter.</p>";
                this.aDeclarationBody += "<p>10) BUILDING DEFECTS: I) You Warrant That The Building Or Location At The Installation Address Is Safe, Free From Defects Including Asbestos-Free, And Fit For The Purpose Of Installing The System. II) You Acknowledge That Arise Solar Is Not Responsible For Any Damage To The System Or Your Building Howsoever So Caused As A Consequence Of A Defect Or Deciency In Your Building Or A Part Thereof. III) Arise Solar Is Not Responsible Or Liable For Diminished Or Inadequate Performance Or Installation If Your Building Contains Brittle Roof Tiles, Oxidized Metal Roof, Meter Boxes, Or Poor Roof Supports. IV) You Must Fix All Such Defects Before Arise Solar Arranges For The Installation Of The System.</p>";
                this.aDeclarationBody += "<p>11) SYSTEM GUARANTEES: Kindly Take Note That, Liability For Breach Of Any Conditions Or Warranty Implied By The Australian Consumer Law Is Limited To Any Of The Following As Determined By Arise Solar: (I) The Replacement Of The Goods Or The Supply Of Equivalent Goods; (II) The Repair Of The Goods.</p>";
                this.aDeclarationBody += "<p>11.1 The Only Conditions And Warranties Which Are Binding On Arise Solar In Respect Of The Stated, Quality, Capability Or Condition Of The Goods Or Whether They Are Suitable, Adequate Or Appropriate For The Purpose Or Purposes For Which They Are Purchased Or Anything Else In Relation To The Goods Are Those Provisions Which Cannot Be Excluded Under Any Federal Or State Law Relating To The Sale Or Supply Of Goods.</p>";
                this.aDeclarationBody += "<p>11.2 The Foregoing Warranty Does Not Apply To The System If: (I) The System Has Been Modified By Any Party Other Than Us, Or Without Our Prior Written Consent; (II) The System Has Been Improperly Installed By You Or Installed By Anyone Other Than A Clean Energy Council Accredited Independent Licensed Electrician; Or (III) An Outstanding Amount Remains Unpaid For 10 Days Or More.</p>";
                this.aDeclarationBody += "<p>11.3 For The Avoidance Of Doubt, When An Outstanding Amount Remains Unpaid To Arise Solar Then All Callouts For Maintenance, Service, And Repairs Remain Suspended Until The Outstanding Amount Is Received In Full By Arise Solar.</p>";
                this.aDeclarationBody += "<p>11.4 Although These Other Guarantees And Warranties May Not Cover Labor Costs, Travel Costs And Delivery Costs Arising From A Claim Under These Other Guarantees And Warranties. We Will Notify You If This Is The Case And Tell You The Costs Payable. The Costs Will Be Payable In Advance.</p>";
                this.aDeclarationBody += "<p>11.5 Arise Solar Offers A System Warranty Of 2+8 Years Or As Agreed In This Agreement, Solar Components Manufacturer’s Warranty Applies As Per Manufacturer's Warranty Terms.</p>";
                this.aDeclarationBody += "<p>11.6 During The Warranty Period, We Will Provide Reasonable Assistance To You In Making Any Guarantee Or Warranty Claim Against The Manufacturer Of The System, Including By Acting As Your Liaison With The Manufacturer.</p>";
                this.aDeclarationBody += "<p>12) MAKING A COMPLAINT: In Case If You Have A Complaint With Respect To The System, Its Installation, Or This Agreement Generally, You Can Make A Complaint To Us By I) Calling Us On Our Telephone Number/S Mentioned In This Quote; Or II) Sending Us Written Notice About This, By Post Or Email.</p>";
                this.aDeclarationBody += "<p>13) SYSTEM RIGHTS, RECLAMATION, AND COSTS: If The Payment Is Not Received By Arise Solar When It Is Due, Arise Solar Holds All The Rights To Enter The Installation Address With/Without Intimation And Recover The System. All The Costs Incurred For The Recovery Of The System From You, Loss In Value Of The Repossessed System, and Wasted Costs Including But Not Restricted To Handling, Installation, And Disbursements Would Be Borne By You.</p>";
                this.aDeclarationBody += "<p>14) REFUNDS: The Agreement Would End For Any Of The Enlisted Reasons Prior To The System Installation At Your Premises And We Refund You All The Money That You Would Have Paid For Your System As A Part Of This Agreement. I) Within 10 Days After Accepting This Agreement II) We Do Not Deliver And Install The System At Your Property Within 6 Weeks Post The Targeted System Installation Date III) The Notice Is Given In Case Of a Price Increase And You Chose To End The Agreement And Restrain From Accepting The Increase In The Price; And IV) In Case Of The Grid Connection Application Being Rejected By Your Energy Distributor. There Would Be An Administration Charge Of $150 Applicable.</p>";
                this.aDeclarationBody += "<p>15) PRIVACY: We Will Comply With All Relevant Privacy Legislation In Relation To Your Personal Information. Copy Of Our Privacy Policy Available On Https://Arisesolar.Com.Au/Privacy-Policy.</p>";
                this.aDeclarationBody += "<p>16) COUNTERPARTS: This Agreement May Be Executed In Any Number Of Counterparts. Such Counterparts, Taken Together, Will Be Deemed To Constitute The One Agreement.</p>";
                this.aDeclarationBody += "<p>17) ADDITIONAL TERMS AND INFORMATION: You May Choose To Visit Https://Arisesolar.Com.Au/Terms-And-Conditions.</p>";
            }
        });

        if (navigator) {
            debugger;
            navigator.geolocation.getCurrentPosition(pos => {
                this.lng = + pos.coords.longitude;
                this.lat = + pos.coords.latitude;
            });
        }

        const xhr = new XMLHttpRequest();
        const url = 'https://api.ipify.org/?format=Text';
        xhr.open('GET', url, false); // false for synchronous request
        xhr.send(null);
        this.ipAddress = xhr.responseText;
    }
 
    ngAfterViewInit(): void {
        debugger;
        const canvas = document.querySelector("canvas");

        this.signaturePad = new SignaturePad(canvas, {
            minWidth: 1,
            maxWidth: 2,
            penColor: "black", 
            backgroundColor: 'rgba(255, 255, 255, 1)'
        });
        this.resizeCanvas();
        // this.sigPadElement = this.sigPad.nativeElement;
        // this.context = this.sigPadElement.getContext('2d');
        // this.context.strokeStyle = '#3742fa';
    }

    resizeCanvas() {
        const ratio = Math.max(window.devicePixelRatio || 1, 1);
        this.signaturePad.width = this.signaturePad.offsetWidth * ratio;
        this.signaturePad.height = this.signaturePad.offsetHeight * ratio;
        // this.signaturePad.getContext("2d").scale(ratio, ratio);
        this.signaturePad.clear(); // otherwise isEmpty() might return incorrect value
    }
   
    save() {
        
        if(this.signaturePad.isEmpty() == true)
        {
            this.message.warn("Please sign the document");
        }
        else if(this.checkboxtrue == true)
        {
            this.saving = true;
            this.spinner.show();
            const input = new SaveCustomerSignature();
            input.encString = this.STR;
            input.imageData = this.signaturePad.toDataURL("image/jpeg");
            input.custSignLatitude = this.lat;
            input.custSignLongitude = this.lng;
            input.custSignIP = this.ipAddress;
            this.img = this.signaturePad.toDataURL("image/png");
            this._quotationsServiceProxy.saveSignature(input)
                .subscribe(() => {
                    this.saving = false;
                    this.spinner.hide();
                    this.ngOnInit();
                    this.Submit = true;
                    //this.notify.info(this.l('SavedSuccessfully'));
                }, e => { this.notify.error("Something is wrong.");this.spinner.hide(); });
        }
        else {
            this.message.warn("Please Accept term And Condition First");
        }
    }
    clear() {
        this.signaturePad.clear();
    }
}

