import { Component, HostListener, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { accountModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { JobAcknowledgementServiceProxy, SaveSignature, AcknoSignatureRequestDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import SignaturePad from 'signature_pad';
import { NgxSpinnerService } from 'ngx-spinner';
import { AppConsts } from '@shared/AppConsts';

@Component({
    templateUrl: './ackno-signature.component.html',
    styleUrls: ['./ackno-signature.component.less'],
    animations: [accountModuleAnimation()]
})

export class AcknoSignatureComponent extends AppComponentBase implements OnInit {

    constructor(
        injector: Injector,
        private _httpClient: HttpClient,
        private _activatedRoute: ActivatedRoute,
        private _jobAcknowledgementServiceProxy: JobAcknowledgementServiceProxy,
        private spinner: NgxSpinnerService) {
        super(injector);
        // lat:any;
        // lng:any;
    }

    @Input() name: string;
    @ViewChild('sigPad') sigPad;
    sigPadElement;
    context;
    isDrawing = false;
    img;
    TD: number;
    JD: number;
    QD: number;
    STR: string;
    acknowledementData: AcknoSignatureRequestDto = new AcknoSignatureRequestDto();    
    expired: boolean = false;
    isChange = false;
    ipAddress: any;
    lat: any;
    lng: any;
    lat1: any;
    lng1: any;
    saving = false;
    signatureImage;
    signatrefile;
    checkboxtrue = false;
    signaturePad;

    Page: string;
    yesLine: string;
    noLine: string;
    checkboxyes: boolean = false;
    checkboxno: boolean = false;
    kw: string = '0';

    Submit: boolean = false;

    hideshow: boolean;

    ngOnInit(): void {
        //debugger;
        this.STR = this._activatedRoute.snapshot.queryParams['STR'];
        this.Page = this._activatedRoute.snapshot.queryParams['p'];

        this._jobAcknowledgementServiceProxy.acknoData(this.STR).subscribe(result => {
            this.acknowledementData = result;
            this.acknowledementData.orgLogo = this.acknowledementData.orgLogo != null && this.acknowledementData.orgLogo != "" ? (AppConsts.docUrl + this.acknowledementData.orgLogo) : "" ;
            
            if (result.custName == undefined) {
                this.expired = true;
            }
            else {
                
                //this.kw = result.kw;
                //this.acknowledementData.orgLogo = this.acknowledementData.orgLogo != null && this.acknowledementData.orgLogo != "" ? (AppConsts.docUrl + this.acknowledementData.orgLogo) : "" ;

                if(this.Page == "EC"){
                    this.Page = "Export Control Acknowledgement";
                    
                    this.yesLine = 'I accept the connection approval with imposed export control limit & understand that this may impact the ROI/Expected Payback period.';
                    //this.noLine = 'No, I don’t agree with imposed export control limit. I wish to cancel the project and get refund of my deposit (if paid). Please contact your sales rep for refund process.';

                    this.hideshow = true;
                }
                else{
                    this.Page = "Feed In Tariff Form";
                    
                    this.yesLine = 'I have read above & agree to proceed with the solar system installation.';
                    // this.noLine = 'No, I don’t want to proceed further and request to refund the deposit. Please contact your sales rep for refund process.';

                    this.hideshow = false;
                }
            }
        });

        if (navigator) {
            // debugger;
            navigator.geolocation.getCurrentPosition(pos => {
                this.lng = + pos.coords.longitude;
                this.lat = + pos.coords.latitude;
            });
        }
        const xhr = new XMLHttpRequest();
        const url = 'https://api.ipify.org/?format=Text';
        xhr.open('GET', url, false); // false for synchronous request
        xhr.send(null);
        this.ipAddress = xhr.responseText;
        // this._httpClient.get("http://api.ipify.org/?format=json").subscribe((res:any)=>{
        //     this.ipAddress = res.ip;
        // });


        // const xhr1 = new XMLHttpRequest();
        // const url1 = 'https://api.ipify.org/?format=json';
        // xhr1.open('GET', url1, false); // false for synchronous request
        // xhr1.send(null);
    }
 
    ngAfterViewInit(): void {
        //debugger;
        const canvas = document.querySelector("canvas");

        this.signaturePad = new SignaturePad(canvas, {
            minWidth: 1,
            maxWidth: 2,
            penColor: "black", 
            backgroundColor: 'rgba(255, 255, 255, 1)'
        });
        this.resizeCanvas();
        // this.sigPadElement = this.sigPad.nativeElement;
        // this.context = this.sigPadElement.getContext('2d');
        // this.context.strokeStyle = '#3742fa';
    }

    resizeCanvas() {
        const ratio = Math.max(window.devicePixelRatio || 1, 1);
        this.signaturePad.width = this.signaturePad.offsetWidth * ratio;
        this.signaturePad.height = this.signaturePad.offsetHeight * ratio;
        // this.signaturePad.getContext("2d").scale(ratio, ratio);
        this.signaturePad.clear(); // otherwise isEmpty() might return incorrect value
    }
    
    save() {
        //debugger;
        if(this.signaturePad.isEmpty() == true)
        {
            this.message.warn("Please sign the document");
        }
        else if(this.checkboxyes == true || this.checkboxno == true)
        {
            this.saving = true;
            this.spinner.show();
            const input = new SaveSignature();
            input.page = this.Page;
            input.encString = this.STR;
            input.imageData = this.signaturePad.toDataURL("image/jpeg");
            input.custSignLatitude = this.lat;
            input.custSignLongitude = this.lng;
            input.custSignIP = this.ipAddress;
            this.img = this.signaturePad.toDataURL("image/png");
            if(this.checkboxyes == true) {
                input.yesNo = true;
            } else if(this.checkboxno == true) {
                input.yesNo = false;
            }

            this._jobAcknowledgementServiceProxy.saveSignature(input)
                .subscribe(() => {
                    this.saving = false;
                     this.spinner.hide();
                    this.ngOnInit();
                     this.Submit = true;
                    // this.notify.info(this.l('SavedSuccessfully'));
                }, e => {
                    this.notify.error("Something is wrong.", "Error");
                });
        }
        else {
            this.message.warn("Please Accept mandatory declaration.");
        }
       
    }

    clear() {
        this.signaturePad.clear();
    }

    checkboxClick(event: any) {
        if(event == 'yes') {
            this.checkboxno = false;
        }
        else {
            this.checkboxyes = false; 
        }
    }

}

