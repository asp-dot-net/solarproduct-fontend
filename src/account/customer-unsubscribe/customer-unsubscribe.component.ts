import { Component, HostListener, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { accountModuleAnimation } from '@shared/animations/routerTransition';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { PromotionsServiceProxy } from '@shared/service-proxies/service-proxies';

@Component({
    templateUrl: './customer-unsubscribe.component.html',
    styleUrls: ['./customer-unsubscribe.component.less'],
    animations: [accountModuleAnimation()]
})

export class CustomerUnSubsribeComponent extends AppComponentBase implements OnInit {

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _promotionsServiceProxy: PromotionsServiceProxy) {
        super(injector);
        
    }

    STR: string;
    OLogo: string;

    ngOnInit(): void {

        this.STR = this._activatedRoute.snapshot.queryParams['STR'];

        this._promotionsServiceProxy.getOrganizationLogo(this.STR).subscribe(logo => {
            this.OLogo = logo != null && logo != "" ? (AppConsts.docUrl + logo) : "" ;
            
                this._promotionsServiceProxy.subscribeUnsubscribePromo(this.STR, 2).subscribe(result => {
           
                });
        })
    }
}
