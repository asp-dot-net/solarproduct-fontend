import { Component, Injector, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { accountModuleAnimation } from '@shared/animations/routerTransition';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { PromotionsServiceProxy } from '@shared/service-proxies/service-proxies';

@Component({
    templateUrl: './customer-subscribe.component.html',
    styleUrls: ['./customer-subscribe.component.less'],
    animations: [accountModuleAnimation()]
})

export class CustomerSubScribeComponent extends AppComponentBase implements OnInit {

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _promotionsServiceProxy: PromotionsServiceProxy,
        private titleService: Title,
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Customer Subscribe");
    }

    STR: string;
    OLogo: string;
    re: any;
    ngOnInit(): void {
       
        this.STR = this._activatedRoute.snapshot.queryParams['STR'];
        
        this._promotionsServiceProxy.getOrganizationLogo(this.STR).subscribe(logo => {
            this.OLogo = logo != null && logo != "" ? (AppConsts.docUrl + logo) : "" ;
        })

        this._promotionsServiceProxy.subscribeUnsubscribePromo(this.STR, 1).subscribe(result => {
            this.re = result;
        });
    }
}
