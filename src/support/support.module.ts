import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import { TheSolarProductCommonModule } from '@shared/common/common.module';
import { FormsModule } from '@angular/forms';
import { ServiceProxyModule } from '@shared/service-proxies/service-proxy.module';
import { UtilsModule } from '@shared/utils/utils.module';
import { NgxCaptchaModule } from 'ngx-captcha';
import { ModalModule } from 'ngx-bootstrap/modal';
import { SupportRoutingModule } from './support-routing.module';
import { SupportComponent } from './support.component';

// }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        HttpClientModule,
        HttpClientJsonpModule,
        NgxCaptchaModule,
        ModalModule.forRoot(),
        TheSolarProductCommonModule,
        UtilsModule,
        ServiceProxyModule,
        SupportRoutingModule,
        // OAuthModule.forRoot(),
        // PasswordModule,
        // AppBsModalModule,
        // SignaturePadModule,
    ],
    declarations: [
        //CustomerSignatureComponent,
        SupportComponent,
       
    ],
    providers: [
        // LoginService,
        // TenantRegistrationHelperService,
        // PaymentHelperService,
        // AccountRouteGuard
    ]
})
export class PrivacyModule {

}
