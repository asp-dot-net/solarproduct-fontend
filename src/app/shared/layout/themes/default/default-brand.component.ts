import { Injector, Component, ViewEncapsulation, Inject } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DOCUMENT } from '@angular/common';
import { Subscription, timer } from 'rxjs';
import { LeadsServiceProxy } from '@shared/service-proxies/service-proxies';
import { switchMap } from 'rxjs/operators';

@Component({
    templateUrl: './default-brand.component.html',
    selector: 'default-brand',
    encapsulation: ViewEncapsulation.None
})
export class DefaultBrandComponent extends AppComponentBase {

    defaultLogo = AppConsts.appBaseUrl + '/assets/common/images/app-logo-on-' + this.currentTheme.baseSettings.menu.asideSkin + '.svg';
    remoteServiceBaseUrl: string = AppConsts.remoteServiceBaseUrl;
    subscription: Subscription;
    // SMSCount: number = 0;
    
    constructor(
        injector: Injector,
        @Inject(DOCUMENT) private document: Document,
        private _leadsServiceProxy: LeadsServiceProxy
    ) {
        super(injector);
    }

    toggleLeftAside(): void {
        this.document.body.classList.toggle('kt-aside--minimize');
        this.triggerAsideToggleClickEvent();
    }

    triggerAsideToggleClickEvent(): void {
        abp.event.trigger('app.kt_aside_toggler.onClick');
    }

    // loadNotifications(): void {
        
    //     this.subscription = timer(0, 10000).pipe(
    //         switchMap(() => this._leadsServiceProxy.getUnReadSMSCount())
    //     ).subscribe(result => {
            
    //         this.SMSCount = result;
    //     });
    // }
}
