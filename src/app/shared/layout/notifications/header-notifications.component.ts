import { Component, Injector, OnInit, ViewEncapsulation, NgZone } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { LeadsServiceProxy, NotificationServiceProxy, UserNotification } from '@shared/service-proxies/service-proxies';
import { IFormattedUserNotification, UserNotificationHelper } from './UserNotificationHelper';
import * as _ from 'lodash';
import { UrlHelper } from '@shared/helpers/UrlHelper';
import { Subscription, timer } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({
    templateUrl: './header-notifications.component.html',
    selector: '[headerNotifications]',
    encapsulation: ViewEncapsulation.None
})
export class HeaderNotificationsComponent extends AppComponentBase implements OnInit {
    subscription: Subscription;
    subscription1: Subscription;
    subscription2: Subscription;
    subscription3: Subscription;
    notifications: IFormattedUserNotification[] = [];
    unreadNotificationCount = 0;
    unreadSMSCount = 0;
    unreadPromotionCount = 0;
    unreadTotalCount = 0;
    url = 'https://wynk.in/music/playlist/weekly-top-20-english/bb_1527140401220';
    constructor(
        injector: Injector,
        private _notificationService: NotificationServiceProxy,
        private _userNotificationHelper: UserNotificationHelper,
        private _leadsServiceProxy: LeadsServiceProxy,
        public _zone: NgZone
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.loadNotifications();
        this.registerToEvents();
        
        // this.SMSNotifications();
        // this.PromotionNotifications();

        // this.TotalNotifications();
    }

    // loadNotifications(): void {
    //     if (UrlHelper.isInstallUrl(location.href)) {
    //         return;
    //     }
    //    // this._notificationService.getUserNotifications(undefined, undefined, undefined, 3, 0).subscribe(result => {
    //     this._notificationService.getUserNotifications(0, undefined, undefined, undefined, 0).subscribe(result => {    
    //    debugger;
    //         this.unreadNotificationCount = result.unreadCount;
    //         this.SMSNotifications();
    //         this.PromotionNotifications();
    //         this.notifications = [];
    //         _.forEach(result.items, (item: UserNotification) => {
    //             debugger;
    //             // if(item.notification.data.properties.message.includes("Next Set For"))
    //             // {
    //             //     this.message.confirm(
    //             //         '',
    //             //         item.notification.data.properties.message,
    //             //         (isConfirmed) => {
    //             //             if (isConfirmed) {
    //             //                 this._userNotificationHelper.setAsRead(item.notification.id);
    //             //                 this.loadNotifications(); // H
    //             //             }
    //             //         }
    //             //     );
    //             // }
    //              this.notifications.push(this._userNotificationHelper.format(<any>item));
    //             // const audio = new Audio();
    //             // audio.src = 'assets/common/images/access-granted-computer-voice-ringtone.mp3';
    //             // audio.load();
    //             // audio.play();
    //         });
         
    //     });
    // }

    loadNotifications(): void {
        if (UrlHelper.isInstallUrl(location.href)) {
            return;
        }
       
        this._notificationService.getUserNotifications(0, undefined, undefined, undefined, 0).subscribe(result => {    
            this.unreadNotificationCount = result.unreadCount;
            this.SMSNotifications();
            this.PromotionNotifications();
            this.notifications = [];
            _.forEach(result.items, (item: UserNotification) => {
                
                 this.notifications.push(this._userNotificationHelper.format(<any>item));
                
            });
         
        });
    }

    SMSNotifications(): void {
        
        // this._leadsServiceProxy.getUnReadSMSCount().subscribe(result => {
        //     this.unreadSMSCount = result;
        //     this.unreadTotalCount = this.unreadSMSCount + this.unreadPromotionCount;
        // });
    }
   
    PromotionNotifications(): void {
       
        // this._leadsServiceProxy.getUnReadPromotionCount().subscribe(result => {
        //     this.unreadPromotionCount = result;
        //     this.unreadTotalCount = this.unreadSMSCount + this.unreadPromotionCount;
        // });
    }

    // ngOnDestroy() {
    //     this.subscription.unsubscribe();
    // }

    registerToEvents() {
        let self = this;

        function onNotificationReceived(userNotification) {
            self._userNotificationHelper.show(userNotification);
            self.loadNotifications();
        }
        abp.event.on('abp.notifications.received', userNotification => {
            self._zone.run(() => {
                onNotificationReceived(userNotification);
            });
        });

        function onNotificationsRefresh() {
            self.loadNotifications();
        }

        abp.event.on('app.notifications.refresh', () => {
            self._zone.run(() => {
                onNotificationsRefresh();
            });
        });

        function onNotificationsRead(userNotificationId) {
            for (let i = 0; i < self.notifications.length; i++) {
                if (self.notifications[i].userNotificationId === userNotificationId) {
                    self.notifications[i].state = 'READ';
                }
            }

            self.unreadNotificationCount -= 1;
        }

        abp.event.on('app.notifications.read', userNotificationId => {
            self._zone.run(() => {
                onNotificationsRead(userNotificationId);
            });
        });
    }

    setAllNotificationsAsRead(): void {
        this._userNotificationHelper.setAllAsRead();
    }

    openNotificationSettingsModal(): void {
        this._userNotificationHelper.openSettingsModal();
    }

    setNotificationAsRead(userNotification: IFormattedUserNotification): void {
        debugger;
        this._userNotificationHelper.setAsRead(userNotification.userNotificationId);
        this.loadNotifications(); // H
    }

    gotoUrl(url): void {
        if (url) {
            location.href = url;
        }
    }
}
