import { Injector, Component, OnInit, ViewEncapsulation, ViewChild, Optional } from '@angular/core';
import { AbpMultiTenancyService, AbpSessionService } from 'abp-ng2-module';
import { ImpersonationService } from '@app/admin/users/impersonation.service';
import { AppAuthService } from '@app/shared/common/auth/app-auth.service';
import { LinkedAccountService } from '@app/shared/layout/linked-account.service';
import { AppConsts } from '@shared/AppConsts';
import { ThemesLayoutBaseComponent } from '@app/shared/layout/themes/themes-layout-base.component';
import { ChangeUserLanguageDto, LinkedUserDto, ProfileServiceProxy, UserLinkServiceProxy, LeadsServiceProxy, GetLeadCountsDto, JobInstallerInvoicesServiceProxy, GetSearchResultDto, WholeSaleLeadServiceProxy, GetWholeSaleLeadHeaderSerachDto } from '@shared/service-proxies/service-proxies';
import { UrlHelper } from '@shared/helpers/UrlHelper';
import * as _ from 'lodash';
import { Router } from '@angular/router';
import { ResultsComponent } from '@app/main/result/result/results.component';
import { WholeSaleLeadHeaderResultsComponent } from '@app/main/wholesaleheaderresult/wholesaleheaderresult.component';

@Component({
    templateUrl: './topbar.component.html',
    selector: 'topbar',
    styleUrls: ['./top.component.less'],
    encapsulation: ViewEncapsulation.None
})
export class TopBarComponent extends ThemesLayoutBaseComponent implements OnInit {

    @ViewChild('searchResultList', { static: true }) searchResultList: ResultsComponent;
    @ViewChild('searchWholesaleResultList', { static: true }) searchWholesaleResultList: WholeSaleLeadHeaderResultsComponent;

    isHost = false;
    languages: abp.localization.ILanguageInfo[];
    currentLanguage: abp.localization.ILanguageInfo;
    isImpersonatedLogin = false;
    isMultiTenancyEnabled = false;
    shownLoginName = '';
    tenancyName = '';
    userName = '';
    profilePicture = AppConsts.appBaseUrl + '/assets/common/images/user@2x.png';
    defaultLogo = AppConsts.appBaseUrl + '/assets/common/images/app-logo-on-' + this.currentTheme.baseSettings.menu.asideSkin + '.svg';
    recentlyLinkedUsers: LinkedUserDto[];
    leadCount: GetLeadCountsDto;
    unreadChatMessageCount = 0;
    remoteServiceBaseUrl: string = AppConsts.remoteServiceBaseUrl;
    chatConnected = false;
    isQuickThemeSelectEnabled: boolean = this.setting.getBoolean('App.UserManagement.IsQuickThemeSelectEnabled');
    installationMode = true;
    filterText: any[];
    filter1 = '';
    filteredLeadSource: string[];
    role: string = '';
    filterText1 = '';
    FiltersData = false;
    NoData = false;
    FiltersDataWholeSale = false;
    NoDataWholeSale = false;
    filterName = 'MobileNo';
    filterNameWholesale = 'MobileNo';
    strWholeSale = '';
    searchResultWholeSale: GetWholeSaleLeadHeaderSerachDto [];

    constructor(
        injector: Injector,

        private _leadsServiceProxy: LeadsServiceProxy,
        private _abpSessionService: AbpSessionService,
        private _abpMultiTenancyService: AbpMultiTenancyService,
        private _profileServiceProxy: ProfileServiceProxy,
        private _userLinkServiceProxy: UserLinkServiceProxy,
        private _authService: AppAuthService,
        private _impersonationService: ImpersonationService,
        private _linkedAccountService: LinkedAccountService,
        private _router: Router,
        private _jobInstallerInvoiceServiceProxy: JobInstallerInvoicesServiceProxy,
        private _wholeSaleLeadServiceProxy : WholeSaleLeadServiceProxy
        /// @Optional() private searchResultList? : ResultsComponent
    ) {
        super(injector);
    }

    ngOnInit() {
        this.installationMode = UrlHelper.isInstallUrl(location.href);
        this.isHost = !this._abpSessionService.tenantId;
        this.isMultiTenancyEnabled = this._abpMultiTenancyService.isEnabled;
        this.languages = _.filter(this.localization.languages, l => (l).isDisabled === false);
        this.currentLanguage = this.localization.currentLanguage;
        this.isImpersonatedLogin = this._abpSessionService.impersonatorUserId > 0;
        this.setCurrentLoginInformations();
        //this.getProfilePicture();
        //this.getRecentlyLinkedUsers();

        //this.registerToEvents();
        // this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
        //     this.role = result;
        // });
    }

    registerToEvents() {
        abp.event.on('profilePictureChanged', () => {
            this.getProfilePicture();
        });

        abp.event.on('app.chat.unreadMessageCountChanged', messageCount => {
            this.unreadChatMessageCount = messageCount;
        });

        abp.event.on('app.chat.connected', () => {
            this.chatConnected = true;
        });

        abp.event.on('app.getRecentlyLinkedUsers', () => {
            this.getRecentlyLinkedUsers();
        });

        abp.event.on('app.onMySettingsModalSaved', () => {
            this.onMySettingsModalSaved();
        });
    }

    changeLanguage(languageName: string): void {
        const input = new ChangeUserLanguageDto();
        input.languageName = languageName;

        this._profileServiceProxy.changeLanguage(input).subscribe(() => {
            abp.utils.setCookieValue(
                'Abp.Localization.CultureName',
                languageName,
                new Date(new Date().getTime() + 5 * 365 * 86400000), //5 year
                abp.appPath
            );

            window.location.reload();
        });
    }

    setCurrentLoginInformations(): void {
        this.shownLoginName = this.appSession.getShownLoginName();
        this.tenancyName = this.appSession.tenancyName;
        this.userName = this.appSession.user.userName;
    }

    getShownUserName(linkedUser: LinkedUserDto): string {
        if (!this._abpMultiTenancyService.isEnabled) {
            return linkedUser.username;
        }

        return (linkedUser.tenantId ? linkedUser.tenancyName : '.') + '\\' + linkedUser.username;
    }

    searchResult: GetSearchResultDto [];
    str = "";
    filterjobnumber(event): void {        
       
        this.saving = true;
        if(this.str != "" && this.str.length > 4)
        {
            this._jobInstallerInvoiceServiceProxy.getSearchFilter(event,this.filterName).subscribe(result => {
                this.searchResult = result;
                if(this.searchResult.length != 0 && this.searchResult != null)
                {
                    this.FiltersData = true;
                    this.NoData = false;
                    this.saving = false;
                }
                else {
                    this.FiltersData = false;
                    this.NoData = true;
                    this.saving = false;
                }
            });
        }
        else{
            this.saving = false;
            this.FiltersData = false;
        }
    }
    filterABNNumber(event): void {        
       
        this.savingW = true;
        if(this.strWholeSale != "")
        {
            this._wholeSaleLeadServiceProxy.getWholeSaleLeadSearchFilter(event,this.filterNameWholesale).subscribe(result => {
                this.searchResultWholeSale = result;
                if(this.searchResultWholeSale.length != 0 && this.searchResultWholeSale != null)
                {
                    this.FiltersDataWholeSale = true;
                    this.NoDataWholeSale = false;
                    this.savingW = false;
                }
                else {
                    this.FiltersDataWholeSale = false;
                    this.NoDataWholeSale = true;
                    this.savingW = false;
                }
            });
        }
        else{
            this.savingW = false;
            this.FiltersDataWholeSale = false;
        }
    }

    saving = false;
    savingW = false;
    clear(): void {
        this.searchResult = [];
        this.FiltersData = false;
        this.NoData = false;
    }
    clearW() :void{
        this.searchResultWholeSale = [];
        this.FiltersDataWholeSale = false;
        this.NoDataWholeSale = false;
    }

    getProfilePicture(): void {
        this._profileServiceProxy.getProfilePicture().subscribe(result => {
            if (result && result.profilePicture) {
                this.profilePicture = 'data:image/jpeg;base64,' + result.profilePicture;
            }
        });
    }

    getRecentlyLinkedUsers(): void {
        this._userLinkServiceProxy.getRecentlyUsedLinkedUsers().subscribe(result => {
            this.recentlyLinkedUsers = result.items;
        });
    }

    showLoginAttempts(): void {
        abp.event.trigger('app.show.loginAttemptsModal');
    }

    showLinkedAccounts(): void {
        abp.event.trigger('app.show.linkedAccountsModal');
    }

    showUserDelegations(): void {
        abp.event.trigger('app.show.userDelegationsModal');
    }

    changePassword(): void {
        abp.event.trigger('app.show.changePasswordModal');
    }

    changeProfilePicture(): void {
        abp.event.trigger('app.show.changeProfilePictureModal');
    }

    changeMySettings(): void {
        abp.event.trigger('app.show.mySettingsModal');
    }

    logout(): void {
        this._authService.logout();
    }

    onMySettingsModalSaved(): void {
        this.shownLoginName = this.appSession.getShownLoginName();
    }

    backToMyAccount(): void {
        this._impersonationService.backToImpersonator();
    }

    switchToLinkedUser(linkedUser: LinkedUserDto): void {
        this._linkedAccountService.switchToAccount(linkedUser.id, linkedUser.tenantId);
    }

    // downloadCollectedData(): void {
    //     this._profileServiceProxy.prepareCollectedData().subscribe(() => {
    //         this.message.success(this.l('GdprDataPrepareStartedNotification'));
    //     });
    // }

    // getLeadCount():void{
    //     this._leadsServiceProxy.getLeadCounts().subscribe(result => {
    //         this.leadCount = result;
    //     });
    // }

    commonFilter(): void {
        debugger
        this._router.routeReuseStrategy.shouldReuseRoute = function () {
            return false;
        };
        this._router.navigate(['/app/main/result/result'], { queryParams: { filterText: this.filterText } });
    }

    AllcommonFilter(event): void {
        this._router.routeReuseStrategy.shouldReuseRoute = function () {
            return false;
        };

        // this._router.navigate(['/app/main/result/result'], { queryParams: { filterText: event.leadId } });
        this._router.navigate(['/app/main/result/result'], { queryParams: { filterText: event.leadId } });
    }

    AllWholesalecommonFilter(event): void {
        this._router.routeReuseStrategy.shouldReuseRoute = function () {
            return false;
        };

        // this._router.navigate(['/app/main/result/result'], { queryParams: { filterText: event.leadId } });
        this._router.navigate(['/app/main/wholesaleheaderresult'], { queryParams: { Id: event.id } });
    }
}