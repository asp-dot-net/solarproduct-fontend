﻿import { PermissionCheckerService, FeatureCheckerService } from 'abp-ng2-module';
import { AppSessionService } from '@shared/common/session/app-session.service';
import { Injectable, OnInit } from '@angular/core';
import { AppMenu } from './app-menu';
import { AppMenuItem } from './app-menu-item';
import { Subscription, timer } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { LeadsServiceProxy, NotificationServiceProxy } from '@shared/service-proxies/service-proxies';
import { nullSafeIsEquivalent } from '@angular/compiler/src/output/output_ast';

@Injectable()
export class AppNavigationService implements OnInit {
    subscription: Subscription;
    SMSCount: number = 0;

    constructor(
        private _permissionCheckerService: PermissionCheckerService,
        private _appSessionService: AppSessionService,
        private _featureCheckerService: FeatureCheckerService,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _notificationService: NotificationServiceProxy
    ) {

    }

    ngOnInit(): void {
        alert();
    }

    // loadNotifications(): void {

    //     alert();
    //     this.subscription = timer(0, 10000).pipe(
    //         switchMap(() => this._leadsServiceProxy.getUnReadSMSCount())
    //     ).subscribe(result => {

    //         this.SMSCount = result;
    //     });
    // }

    checkFeature(feature: string): boolean {
        const result = this._featureCheckerService.isEnabled(feature);
        return result;
    }

    getMenu(): AppMenu {
        return new AppMenu('MainMenu', 'MainMenu', [
            new AppMenuItem('Dashboard', 'Pages.Administration.Host.Dashboard', 'las la-home', '/app/admin/hostDashboard'),
            new AppMenuItem('Tenants', 'Pages.Tenants', 'las la-sitemap', '/app/admin/tenants'),
            new AppMenuItem('Package', 'Pages.Editions', 'las la-box', '/app/admin/editions'),

            new AppMenuItem('Dashboard', 'Pages.Tenant.Dashboard', 'las la-home', '/app/main/dashboard'),
                        
            new AppMenuItem('Retail', 'Pages.Tenant.Retail', 'las la-store', '', [], [

                 new AppMenuItem('Dashboard', 'Pages.Tenant.Retail.Dashboard', 'las la-tachometer-alt', '', [], [
                     new AppMenuItem('Sales', 'Pages.Tenant.Dashboard.Sales', 'flaticon-more', '/app/main/dashboard-sales'),
                     new AppMenuItem('Invoice', 'Pages.Tenant.Dashboard.Invoice', 'flaticon-more', '/app/main/dashboard-invoice'),
                     new AppMenuItem('Service', 'Pages.Tenant.Dashboard.Service', 'flaticon-more', '/app/main/dashboard-service'),
                     new AppMenuItem('Dashboard 2', 'Pages.Tenant.Dashboard.Dashboard2', 'flaticon-more', '/app/main/dashboard2'),
                ]),

                new AppMenuItem('Data Vaults', 'Pages.DataVaults', 'las la-database', '', [], [
                    new AppMenuItem('Activity', 'Pages.Tenant.datavault.ActivityLog', 'flaticon-more', '/app/main/datavaultactivitylog/datavaultactivitylog'),
                    new AppMenuItem('Call Flow Queue', 'Pages.CallFlowQueues', 'flaticon-more', '/app/main/callflowqueue'),
                    new AppMenuItem('Cancel Leads', 'Pages.CancelReasons', 'flaticon-more', '/app/main/cancelReasons/cancelReasons'),
                    new AppMenuItem('CheckActive', 'Pages.CheckActive', 'flaticon-more', '/app/main/checkActives/chcekActives'),
                    new AppMenuItem('CheckApplication', 'Pages.CheckApplication', 'flaticon-more', '/app/main/chcekApplications/checkApplications'),
                    new AppMenuItem('Check Deposite Receive', 'Pages.Tenant.DataVaults.CheckDepositReceived', 'flaticon-more', '/app/main/checkDepositReceived'),
                    new AppMenuItem('Commission Range', 'Pages.CommissionRanges', 'flaticon-more', '/app/main/commission-range'),
                    new AppMenuItem('Dashboard Message', 'Pages.DashboardMessage', 'flaticon-more', '/app/main/dashboard-message'),
                    new AppMenuItem('Document Library', 'Pages.Tenant.datavault.DocumentLibrary', 'flaticon-more', '/app/main/documentLibrary/documentlibrary'),
                    new AppMenuItem('Document Type', 'Pages.Tenant.datavault.DocumentType', 'flaticon-more', '/app/main/documentTypes/documenttypes'),
                    new AppMenuItem('Elec Distributor', 'Pages.ElecDistributors', 'flaticon-more', '/app/main/jobs/elecDistributors'),
                    new AppMenuItem('Elec Retailer', 'Pages.ElecRetailers', 'flaticon-more', '/app/main/jobs/elecRetailers'),
                    new AppMenuItem('Email Templates', 'Pages.Tenant.DataVaults.EmailTemplates', 'flaticon-more', '/app/main/emailtemplates/emailtemplates'),
                    new AppMenuItem('Emp Categories', 'Pages.Categories', 'flaticon-more', '/app/main/theSolarDemo/categories'),
                    new AppMenuItem('Freebie Transports', 'Pages.FreebieTransports', 'flaticon-more', '/app/main/jobs/freebieTransports'),
                    new AppMenuItem('Hold Jobs', 'Pages.HoldReasons', 'flaticon-more', '/app/main/holdReasons/holdReasons'),
                    new AppMenuItem('House Type', 'Pages.HouseTypes', 'flaticon-more', '/app/main/jobs/houseTypes'),
                    new AppMenuItem('Html Template', 'Pages.QuotationTemplate', 'flaticon-more', '/app/main/quotationtemplate/quotationtemplate'),
                    new AppMenuItem('Installation Cost', 'Pages.Tenant.DataVaults.InstallationCost', 'las la-coins', '', [], [
                        new AppMenuItem('Battery Installation Cost', 'Pages.Tenant.DataVaults.InstallationCost.BatteryInstallationCost', 'flaticon-more', '/app/main/installation-cost/batteryInstallationCost'),
                        new AppMenuItem('Category Installation Item', 'Pages.Tenant.DataVaults.InstallationCost.CategoryInstallationItemList', 'flaticon-more', '/app/main/installation-cost/categoryInstallationItem'),
                        new AppMenuItem('Extra Ins Charges', 'Pages.Tenant.DataVaults.InstallationCost.ExtraInstallationCharges', 'flaticon-more', '/app/main/installation-cost/extraInstallationCharge'),
                        new AppMenuItem('Fixed Cost Price', 'Pages.Tenant.DataVaults.InstallationCost.FixedCostPrice', 'flaticon-more', '/app/main/installation-cost/fixCostPrice'),
                        new AppMenuItem('Installation Item List', 'Pages.Tenant.DataVaults.InstallationCost.InstallationItemList', 'flaticon-more', '/app/main/installation-cost/installationItemList'),
                        new AppMenuItem('Job Cost Fix Expense', 'Pages.Tenant.DataVaults.InstallationCost.JobCostFixExpense', 'flaticon-more', '/app/main/installation-cost/jobcostfixexpense'),
                        new AppMenuItem('Other Charges', 'Pages.Tenant.DataVaults.InstallationCost.OtherCharges', 'flaticon-more', '/app/main/installation-cost/other-charges'),
                        new AppMenuItem('Post Code Price', 'Pages.Tenant.DataVaults.InstallationCost.PostCodeCost', 'flaticon-more', '/app/main/installation-cost/postcodecost'),
                        new AppMenuItem('State Wise Cost', 'Pages.Tenant.DataVaults.InstallationCost.StateWiseInstallationCost', 'flaticon-more', '/app/main/installation-cost/statewiseCost'),   
                        new AppMenuItem('STC Cost', 'Pages.Tenant.DataVaults.InstallationCost.STCCost', 'flaticon-more', '/app/main/installation-cost/stcCost'),
                    ]),
                    new AppMenuItem('Invoice Payments', 'Pages.InvoicePaymentMethods', 'flaticon-more', '/app/main/invoices/invoicePaymentMethods'),
                    new AppMenuItem('Invoice Status', 'Pages.InvoiceStatuses', 'flaticon-more', '/app/main/invoices/invoiceStatuses'),
                    new AppMenuItem('Job Cancellation', 'Pages.Tenant.datavault.JobCancellationReason', 'flaticon-more', '/app/main/jobs/jobcancellationreason'),
                    new AppMenuItem('Job Status', 'Pages.JobStatuses', 'flaticon-more', '/app/main/jobs/jobStatuses'),
                    new AppMenuItem('Job Type', 'Pages.JobTypes', 'flaticon-more', '/app/main/jobs/jobTypes'),
                    new AppMenuItem('Lead Expenses', 'Pages.LeadExpenses', 'flaticon-more', '/app/main/expense/leadExpenses'),
                    new AppMenuItem('Lead Sources', 'Pages.LeadSources', 'flaticon-more', '/app/main/leadSources/leadSources'),
                    new AppMenuItem('Payment Method', 'Pages.PaymentOptions', 'flaticon-more', '/app/main/jobs/paymentOptions'),
                    new AppMenuItem('Payment Options', 'Pages.DepositOptions', 'flaticon-more', '/app/main/jobs/depositOptions'),
                    new AppMenuItem('Payment Term', 'Pages.FinanceOptions', 'flaticon-more', '/app/main/jobs/financeOptions'),
                    new AppMenuItem('Post Codes', 'Pages.PostCodes', 'flaticon-more', '/app/main/postCodes/postCodes'),
                    new AppMenuItem('Post Code Range', 'Pages.PostCodeRange', 'flaticon-more', '/app/main/postcoderange/postcoderange'),
                    new AppMenuItem('Postal Types', 'Pages.PostalTypes', 'flaticon-more', '/app/main/postalTypes/postalTypes'),
                    new AppMenuItem('Price Item', 'Pages.PriceItemLists', 'flaticon-more', '/app/main/priceItemLists/priceItemLists'),
                    new AppMenuItem('Price Variations', 'Pages.Variations', 'flaticon-more', '/app/main/jobs/variations'),
                    new AppMenuItem('Product Items', 'Pages.ProductItems', 'flaticon-more', '/app/main/jobs/productItems'),
                    new AppMenuItem('Product Packages', 'Pages.ProductPackages', 'flaticon-more', '/app/main/productpackage'),
                    new AppMenuItem('Product Type', 'Pages.ProductTypes', 'flaticon-more', '/app/main/jobs/productTypes'),
                    new AppMenuItem('Promotion Type', 'Pages.PromotionMasters', 'flaticon-more', '/app/main/jobs/promotionMasters'),
                    new AppMenuItem('Refund Reason', 'Pages.RefundReasons', 'flaticon-more', '/app/main/jobs/refundReasons'),
                    new AppMenuItem('Reject Leads', 'Pages.RejectReasons', 'flaticon-more', '/app/main/rejectReasons/rejectReasons'),
                    new AppMenuItem('Review Type', 'Pages.ReviewType', 'flaticon-more', '/app/main/reviewType/reviewType'),
                    new AppMenuItem('Roof Angle', 'Pages.RoofAngles', 'flaticon-more', '/app/main/jobs/roofAngles'),
                    new AppMenuItem('Roof Type', 'Pages.RoofTypes', 'flaticon-more', '/app/main/jobs/roofTypes'),
                    new AppMenuItem('Sales Teams', 'Pages.Teams', 'flaticon-more', '/app/main/theSolarDemo/teams'),
                    new AppMenuItem('Service Category', 'Pages.ServiceCategory', 'flaticon-more', '/app/main/serviceCategory/serviceCategory'),
                    new AppMenuItem('Service Document Type', 'Pages.Tenant.datavault.ServiceDocumentType', 'flaticon-more', '/app/main/serviceDocumentTypes/serviceDocumenttypes'),
                    new AppMenuItem('Service Sources', 'Pages.ServiceSources', 'flaticon-more', '/app/main/serviceSources/serviceSources'),
                    new AppMenuItem('Service Status', 'Pages.ServiceStatus', 'flaticon-more', '/app/main/serviceStatus/serviceStatus'),
                    new AppMenuItem('Service SubCategory', 'Pages.ServiceSubCategory', 'flaticon-more', '/app/main/serviceSubCategory/serviceSubCategory'),
                    new AppMenuItem('Service Type', 'Pages.ServiceType', 'flaticon-more', '/app/main/serviceType/serviceType'),
                    new AppMenuItem('SMS Templates', 'Pages.SmsTemplates', 'flaticon-more', '/app/main/smsTemplates/smsTemplates'),
                    new AppMenuItem('State', 'Pages.DataVaults.State', 'flaticon-more', '/app/main/state'),
                    new AppMenuItem('Stc Status', 'Pages.StcPvdStatus', 'flaticon-more', '/app/main/stcPvdStatus/stcpvdstatus'),
                    new AppMenuItem('Street Names', 'Pages.StreetNames', 'flaticon-more', '/app/main/streetNames/streetNames'),
                    new AppMenuItem('Street Types', 'Pages.StreetTypes', 'flaticon-more', '/app/main/streetTypes/streetTypes'),
                    new AppMenuItem('Unit Types', 'Pages.UnitTypes', 'flaticon-more', '/app/main/unitTypes/unitTypes'),
                    new AppMenuItem('Warehouse Location', 'Pages.WarehouseLocation', 'flaticon-more', '/app/main/warehouselocation'),
                    new AppMenuItem('Voucher', '', 'flaticon-more', '/app/main/vouchers'),

                    

                    // new AppMenuItem('MeterPhases', 'Pages.MeterPhases', 'flaticon-more', '/app/main/jobs/meterPhases'),
                    // new AppMenuItem('MeterUpgrades', 'Pages.MeterUpgrades', 'flaticon-more', '/app/main/jobs/meterUpgrades'),

                    //new AppMenuItem('Commission Range', 'Pages.CommissionRangeComponent', 'flaticon-more', '/app/main/commission-range'),
                    
                   
                    
                ]), 

                new AppMenuItem('Lead', 'Pages.Leads', 'las la-crosshairs', '', [], [
                    new AppMenuItem('Manage Leads', 'Pages.ManageLeads', 'flaticon-more', '/app/main/leads/leads'),
                    // new AppMenuItem('Manage Service', 'Pages.Lead.ManageService', 'flaticon-more', '/app/main/services/manageservices/manageservice'),
                    new AppMenuItem('Duplicate Lead', 'Pages.Lead.Duplicate', 'flaticon-more', '/app/main/duplicatelead/duplicatelead'),
                    new AppMenuItem('My Lead', 'Pages.MyLeads', 'flaticon-more', '/app/main/myleads/myleads'),
                    new AppMenuItem('Lead Tracker', 'Pages.LeadTracker', 'flaticon-more', '/app/main/leads/leads/leadtracker'),
                    new AppMenuItem('Cancel Lead', 'Pages.Lead.Calcel', 'flaticon-more', '/app/main/cancelleads/cancelleads'),
                    new AppMenuItem('Reject Lead', 'Pages.Lead.Rejects', 'flaticon-more', '/app/main/rejectleads/rejectleads'),
                    new AppMenuItem('Closed Lead', 'Pages.Lead.Close', 'flaticon-more', '/app/main/closedlead/closedlead'),
                    new AppMenuItem('3rd Party Leads', 'Pages.3rdPartyLeadTracker', 'flaticon-more', '/app/main/leads/leads/3rdpartyleadtracker'),
                ]),

                new AppMenuItem('Promotions', 'Pages.PromotionGroup', 'las la-volume-up', '', [], [
                    new AppMenuItem('Promotions', 'Pages.Promotions', 'flaticon-more', '/app/main/promotions/promotions'),
                    new AppMenuItem('Promotion Tracker', 'Pages.PromotionUsers', 'flaticon-more', '/app/main/promotions/promotionUsers'),
                ]),

                new AppMenuItem('Tracker', 'Pages.Tracker', 'las la-map-marker', '', [], [
                    new AppMenuItem('Essential', 'Pages.Tracker.EssentialTracker', 'flaticon-more', '/app/main/jobs/essentialTracker'),
                    new AppMenuItem('Application', 'Pages.Tracker.ApplicationTracker', 'flaticon-more', '/app/main/jobs/jobs/jobslist'),
                    new AppMenuItem('Freebies', 'Pages.Tracker.FreebiesTracker', 'flaticon-more', '/app/main/jobs/jobPromotions/freebies'),
                    new AppMenuItem('Finance', 'Pages.Tracker.FinanceTracker', 'flaticon-more', '/app/main/jobs/financeTracker/financeTracker'),
                    new AppMenuItem('Refund', 'Pages.Tracker.RefundTracker', 'flaticon-more', '/app/main/jobs/jobRefunds'),
                    new AppMenuItem('Referral', 'Pages.Tracker.Referral', 'flaticon-more', '/app/main/referral/referraltracker'),
                    new AppMenuItem('Job Active', 'Pages.Tracker.JobActiveTracker', 'flaticon-more', '/app/main/jobs/jobs/jobActiveTracker'),
                    new AppMenuItem('Grid Connect', 'Pages.Tracker.Gridconnectiontracker', 'flaticon-more', '/app/main/gridconnectiontracker/gridconnectiontracker'),
                    new AppMenuItem('STC', 'Pages.Tracker.STCTracker', 'flaticon-more', '/app/main/jobs/stctracker/stctracker'),
                    new AppMenuItem('Warranty', 'Pages.Tracker.Warranty', 'flaticon-more', '/app/main/warranty/warrantytracker'),
                    new AppMenuItem('Hold Jobs', 'Pages.HoldJobTracker', 'flaticon-more', '/app/main/jobs/holdjobs/holdJobs'),
                    new AppMenuItem('Transport Cost', 'Pages.TransportCost', 'flaticon-more', '/app/main/transportcost/transportcost'),
                    new AppMenuItem('CIMET', 'Pages.Tracker.CIMETTracker', 'flaticon-more', '/app/main/leads/leads/cimettracker'),
                    new AppMenuItem('Application Fee', 'Pages.Tracker.ApplicationFeeTracker', 'flaticon-more', '/app/main/jobs/applicationfeeTracker'),
                ]),

                new AppMenuItem('Jobs', 'Pages.JobGrid', 'las la-briefcase', '/app/main/jobs/jobGrid/jobGrid'),

                new AppMenuItem('Service', 'Pages.Service', 'las la-file-invoice-dollar', '', [], [
                    new AppMenuItem('Service', 'Pages.Service.ServicesTracker', 'flaticon-more', '/app/main/services/services/service'),
                ]),

                new AppMenuItem('Invoice', 'Pages.Invoice', 'las la-file-invoice-dollar', '', [], [
                    new AppMenuItem('Invoice Issued', 'Pages.Tracker.InvoiceIssuedTracker', 'flaticon-more', '/app/main/invoices/invoiceIssued'),
                    new AppMenuItem('Invoice Paid', 'Pages.Tracker.InvoiceTracker', 'flaticon-more', '/app/main/invoices/new-invoice-issued'),
                    new AppMenuItem('PayWay', 'Pages.InvoicePayWay', 'flaticon-more', '/app/main/invoices/invoice-payway'),
                    new AppMenuItem('Invoice File List', 'Pages.InvoiceFileList', 'flaticon-more', '/app/main/invoices/installerInvoiceFileList'),
                    // new AppMenuItem('Invoice Paid', 'Pages.Tracker.InvoiceTracker', 'flaticon-more', '/app/main/invoices/invoicePayments'),
                    // new AppMenuItem('Invoice Paid', null, 'flaticon-more', '/app/main/invoices/invoicePayments'),
                ]),

                new AppMenuItem('Installation', 'Pages.InstallationSection', 'las la-wrench', '', [], [
                    new AppMenuItem('My Installer', 'Pages.MyInstaller', 'flaticon-more', '/app/main/myinstaller/myinstaller'),
                    new AppMenuItem('Job Assign', 'Pages.PendingInstallation', 'flaticon-more', '/app/main/installation/installation/pendinginstallation'),
                    new AppMenuItem('Map', 'Pages.Map', 'flaticon-more', '/app/main/intaller-map/intaller-map'),
                    new AppMenuItem('Job Booking', 'Pages.JobBooking', 'flaticon-more', '/app/main/installation/jobBooking'),
                    new AppMenuItem('Calender', 'Pages.Calenders', 'flaticon-more', '/app/main/calenders/calenders'),
                    new AppMenuItem('Installation', 'Pages.Installation', 'flaticon-more', '/app/main/installation/installation'),
                ]),

                new AppMenuItem('Installer Invoice', 'Pages.Installer.InstallerInvitation', 'las la-file-invoice', '', [], [
                    new AppMenuItem('New', 'Pages.Installer.New', 'flaticon-more', '/app/main/installer/newinstaller'),
                    new AppMenuItem('Pending Invoice', 'Pages.Installer.Invoice', 'flaticon-more', '/app/main/installer/invoiceinstaller'),
                    // new AppMenuItem('Verified', 'Pages.Installer.Paid', 'flaticon-more', '/app/main/installer/paidinstaller'),
                    new AppMenuItem('Ready To Pay', 'Pages.ReadyToPay', 'flaticon-more', '/app/main/installer/radytoPayInstaller'),
                ]),

                new AppMenuItem('Lead Generation', 'Pages.LeadGeneration', 'las la-crosshairs', '', [], [
                    new AppMenuItem(
                        'My Leads Generation', 
                        'Pages.LeadGeneration.MyLeadsGeneration', 
                        'flaticon-more', 
                        '/app/main/my-lead-generation', 
                        [], 
                        [], 
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.LeadGeneration.MyLeadsGeneration'); }
                    ),           
                    new AppMenuItem(
                        'Appointment', 
                        'Pages.LeadGeneration.Installation', 
                        'flaticon-more', 
                        '/app/main/lead-generation-installation',
                        [], 
                        [], 
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.LeadGeneration.Installation'); }
                    ),
                    new AppMenuItem(
                        'Map', 
                        'Pages.LeadGeneration.Map', 
                        'flaticon-more', 
                        '/app/main/lead-generation-map',
                        [], 
                        [], 
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.LeadGeneration.Map'); }
                    ), 
                    new AppMenuItem(
                        'Commission', 
                        'Pages.LeadGeneration.Commission', 
                        'flaticon-more', 
                        '/app/main/commission', 
                        [], 
                        [], 
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.LeadGeneration.MyLeadsGeneration'); }
                    ), 
                ],
                undefined, undefined, () => { return this._featureCheckerService.isEnabled('App.LeadGeneration'); }
                ),

                new AppMenuItem('Service', 'Pages.Service', 'las la-cog', '', [], [
                    new AppMenuItem('Manage Service', 'Pages.Lead.ManageService', 'flaticon-more', '/app/main/services/manageservices/manageservice'),
                    new AppMenuItem('My Service', 'Pages.Service.MyService', 'flaticon-more', '/app/main/services/myservices/myservice'),
                    new AppMenuItem('Map', 'Pages.Service.ServiceMap', 'flaticon-more', '/app/main/services/service-map/service-map'),
                    new AppMenuItem('Installation', 'Pages.Service.ServiceInstallation', 'flaticon-more', '/app/main/services/serviceInstallation/serviceInstallation'),
                    new AppMenuItem('Warrenty Claim', 'Pages.Service.WarrantyClaim', 'flaticon-more', '/app/main/services/warrentyClaim/warrentyClaim'),
                    new AppMenuItem('Service Invoice', 'Pages.Service.ServiceInvoice', 'flaticon-more', '/app/main/services/warrentyClaim/serviceInvoice'),
                    // new AppMenuItem('Service Document Type', 'Pages.Tenant.Services.ServiceDocumentType', 'flaticon-more', '/app/main/services/serviceDocumentTypes/serviceDocumentTypes'),

                ]),
                
                new AppMenuItem('Review', 'Pages.Review', 'las la-poll-h', '/app/main/review/review'),
                
                new AppMenuItem('Notification', 'Pages.Notification', 'las la-bell', '', [], [
                    new AppMenuItem('SMS', 'Pages.Sms', 'flaticon-more', '/app/main/reply/smsReply'),
                    new AppMenuItem('Email', 'Pages.Email', 'flaticon-more', '/app/main/reply/emailReply'),
                ]),
                
                new AppMenuItem('Report', 'Pages.report', 'las la-book-open', '', [], [
                    
                    new AppMenuItem('Activity', 'Pages.ActivityReport', 'flaticon-more', '/app/main/report/activityreport'),
                    new AppMenuItem('ToDo Activity', 'Pages.ToDoActivityReport', 'flaticon-more', '/app/main/report/todo-activity-list'),
                    new AppMenuItem('Lead Expense', 'Pages.LeadexpenseReport', 'flaticon-more', '/app/main/report/leadexpensereport/leadexpense-report'),
                    new AppMenuItem('OutStanding', 'Pages.OutStandingReport', 'flaticon-more', '/app/main/report/outstandingreport'),
                    new AppMenuItem('Job Cost', 'Pages.Report.JobCost', 'flaticon-more', '/app/main/report/jobcost'),
                    new AppMenuItem('Lead Assign', 'Pages.Report.LeadAssign', 'flaticon-more', '/app/main/report/leadassign'), 
                    new AppMenuItem('Lead Sold', 'Pages.Report.LeadSold', 'flaticon-more', '/app/main/report/leadsold'), 
                    new AppMenuItem('Employee Job Cost', 'Pages.Report.EmpJobCost', 'flaticon-more', '/app/main/report/employeeJobCost'),
                    new AppMenuItem('Commission', 'Pages.Report.JobCommission', 'flaticon-more', '/app/main/report/jobcommission'),
                    new AppMenuItem('Commission Paid', 'Pages.Report.JobCommissionPaid', 'flaticon-more', '/app/main/report/jobcommissionpaid'),
                    new AppMenuItem('Product Sold', 'Pages.Report.ProductSold', 'flaticon-more', '/app/main/report/productSold'), 
                    new AppMenuItem('SMS Count', 'Pages.Report.SmsCount', 'flaticon-more', '/app/main/report/smscountreport'), 
                    new AppMenuItem('User Detail', 'Pages.Report.UserDetails', 'flaticon-more', '/app/main/report/userdetail'), 
                    new AppMenuItem('Comparison', 'Pages.Report.Comparison', 'flaticon-more', '/app/main/report/comparison'),
                    new AppMenuItem('Lead Comparison', 'Pages.Report.LeadComparison', 'flaticon-more', '/app/main/report/leadSources'),  
                    new AppMenuItem('Monthly Comparison', 'Pages.Report.MonthlyComparison', 'flaticon-more', '/app/main/report/monthlycomparison'), 
                    new AppMenuItem('Lead Monthly Compare', '', 'flaticon-more', '/app/main/report/leadmonthlycomparison'), 
                    new AppMenuItem('Installer Payment', 'Pages.Report.InstallerInvoicePayment', 'flaticon-more', '/app/main/report/installerinvoicepayment'), 
                    new AppMenuItem('Installer Wise Payment', 'Pages.Report.InstallerWiseInvoicePayment', 'flaticon-more', '/app/main/report/installerpaymentinstallerwise'), 
                    new AppMenuItem('Job Cost Month Wise', 'Pages.Report.JobCostMonthWise', 'flaticon-more', '/app/main/report/jobcostmonthwise'), 
                    new AppMenuItem('Job Variation', 'Pages.Report.JobVariation', 'flaticon-more', '/app/main/report/jobvariation'), 
                    new AppMenuItem('Reschedule Jobs', 'Pages.Report.RescheduleInstallation', 'flaticon-more', '/app/main/report/rescheduleinstallation'), 
                    new AppMenuItem('Review Report', 'Pages.Report.ReviewReport', 'flaticon-more', '/app/main/report/reviewReport'),
                    new AppMenuItem('Employee Profit', 'Pages.Report.EmployeeProfit', 'flaticon-more', '/app/main/report/employeeProfitReport'),
                    new AppMenuItem('Progress Report', 'Pages.Report.ProgressReport', 'flaticon-more', '/app/main/report/progessReport'),
                ]),
                new AppMenuItem('Call History', 'Pages.CallHistory', 'las la-history', '', [], [
                    new AppMenuItem('User Call History', 'Pages.CallHistory.UserCallHistory', 'flaticon-more', '/app/main/report/callhistory/usercallhistory'),
                    new AppMenuItem('State Wise', 'Pages.CallHistory.StateWiseCallHistory', 'flaticon-more', '/app/main/report/callhistory/statewisecallhistory'),
                    // new AppMenuItem('3CX Call Queue', 'Pages.CallHistory.CallFlowQueueCallHistory', 'flaticon-more', '/app/main/report/callQueue'),
                    // new AppMenuItem('3CX Call Queue Weekly', 'Pages.CallHistory.CallFlowQueueWeeklyCallHistory', 'flaticon-more', '/app/main/report/callQueueweekly'),
                    // new AppMenuItem('3CX Call Queue User Wise', 'Pages.CallHistory.CallFlowQueueUserWiseCallHistory', 'flaticon-more', '/app/main/report/callQueueUserwise'),

                    new AppMenuItem('3CX', 'Pages.CallHistory.CallFlowQueueCallHistory', 'las la-phone', '', [], [
                        new AppMenuItem('Call Queue', 'Pages.CallHistory.CallFlowQueueCallHistory', 'flaticon-more', '/app/main/report/callQueue'),
                        new AppMenuItem('Call Queue Weekly', 'Pages.CallHistory.CallFlowQueueWeeklyCallHistory', 'flaticon-more', '/app/main/report/callQueueweekly'),
                        new AppMenuItem('Call Queue User Wise', 'Pages.CallHistory.CallFlowQueueUserWiseCallHistory', 'flaticon-more', '/app/main/report/callQueueUserwise')
                        
                    ]),
                ]),
            ]),
            
            new AppMenuItem('Wholesale', 'Pages.Tenant.WholeSale', 'las la-boxes', '', [], [

            
                 new AppMenuItem('Dashboard', 'Pages.Tenant.WholeSale.Dashboard', 'las la-tachometer-alt', '/app/main/dashboard-wholesale'),

                 new AppMenuItem('Data Vaults', 'Pages.Tenant.WholeSale.DataVaults', 'las la-volume-up', '', [], [
                    new AppMenuItem('Activity', 'Pages.Tenant.WholeSale.datavault.ActivityLog', 'flaticon-more', '/app/main/wholesale/data-vaults/activity'),
                    new AppMenuItem('Wholesale Status', 'Pages.Tenant.WholeSale.DataVaults.WholeSaleStatus', 'flaticon-more', '/app/main/wholesale/data-vaults/wholesale-type'),
                     new AppMenuItem('Invoice Type', 'Pages.Tenant.WholeSale.DataVaults.Wholesaleinvoicetypes', 'flaticon-more', '/app/main/wholesale/data-vaults/invoice-type'),
                   //  new AppMenuItem('Wholesale BDM', null, 'flaticon-more', '/app/main/wholesale/data-vaults/wholesale-bdm'),
                     new AppMenuItem('Delivery Options','Pages.Tenant.WholeSale.DataVaults.WholesaleDeliveryOptions' , 'flaticon-more', '/app/main/wholesale/data-vaults/delivery-options'),
                    new AppMenuItem('Job Type', 'Pages.Tenant.WholeSale.DataVaults.WholesaleJobTypes', 'flaticon-more', '/app/main/wholesale/data-vaults/job-type'),
                    new AppMenuItem('Transport Type', 'Pages.Tenant.WholeSale.DataVaults.WholesaleTransportTypes', 'flaticon-more', '/app/main/wholesale/data-vaults/transport-type'),   
                     new AppMenuItem('Property Type', 'Pages.Tenant.WholeSale.DataVaults.WholesalePropertyTypes', 'flaticon-more', '/app/main/wholesale/data-vaults/propery-type'),  
                     new AppMenuItem('PVD Status', 'Pages.Tenant.WholeSale.DataVaults.WholesalePVDStatuses', 'flaticon-more', '/app/main/wholesale/data-vaults/pvd-status'),
                    new AppMenuItem('Document Type', 'Pages.Tenant.WholeSale.DataVaults.DocumentType', 'flaticon-more', '/app/main/wholesale/data-vaults/wholesalelead-documenttype'),                    
                    new AppMenuItem('Sms Template', 'Pages.Tenant.WholeSale.DataVaults.SmsTemplates', 'flaticon-more', '/app/main/wholesale/data-vaults/smsTemplates'),                    
                    new AppMenuItem('Email Template', 'Pages.Tenant.WholeSale.DataVaults.EmailTemplates', 'flaticon-more', '/app/main/wholesale/data-vaults/emailTemplates'),                    
                    new AppMenuItem('Series','Pages.Tenant.WholeSale.DataVaults.Serieses', 'flaticon-more', '/app/main/wholesale/data-vaults/series'), 
                    new AppMenuItem('Product Type', 'Pages.Tenant.WholeSale.DataVaults.WholesaleJobStatuses', 'flaticon-more', '/app/main/wholesale/ecommerce/producttype'),
                    new AppMenuItem('Job Status', 'Pages.Tenant.WholeSale.DataVaults.WholesaleJobStatuses', 'flaticon-more', '/app/main/wholesale/data-vaults/jobStatus'), 

                       
                ]),

                 new AppMenuItem('E-Commerce', 'Pages.Tenant.WholeSale.ECommerce', 'las la-shopping-cart', '', [],[
                    new AppMenuItem('Activity', 'Pages.Tenant.WholeSale.ECommerce.ActivityLog', 'flaticon-more', '/app/main/wholesale/ecommerce/activity'),
                     new AppMenuItem('Slider', 'Pages.Tenant.WholeSale.ECommerce.Slider', 'flaticon-more', '/app/main/wholesale/ecommerce/ecommerceslider'),
                     new AppMenuItem('Products', 'Pages.Tenant.WholeSale.ECommerce.Products', 'flaticon-more', '/app/main/wholesale/ecommerce/add-product'),
                     new AppMenuItem('Special Offers', '', 'flaticon-more', '/app/main/wholesale/ecommerce/special-offers') ,                    
                     new AppMenuItem('Branding Partner', 'Pages.Tenant.WholeSale.ECommerce.BrandingPartner', 'flaticon-more', '/app/main/wholesale/ecommerce/brandingpartner'),                     
                     new AppMenuItem('User Request', 'Pages.Tenant.WholeSale.ECommerce.UserRequest', 'flaticon-more', '/app/main/wholesale/ecommerce/userrequest')  ,                   
                     new AppMenuItem('Solar Package', 'Pages.Tenant.WholeSale.ECommerce.EcommerceSolarPackages', 'flaticon-more', '/app/main/wholesale/ecommerce/solarpackage'),
                     new AppMenuItem('Subcriber', 'Pages.Tenant.WholeSale.ECommerce.Subscribe', 'flaticon-more', '/app/main/wholesale/ecommerce/subcribers'),
                     new AppMenuItem('Specification','Pages.Tenant.WholeSale.ECommerce.EcommerceSpecifications', 'flaticon-more', '/app/main/wholesale/ecommerce/specification'),
                     new AppMenuItem('StcRegister','Pages.Tenant.WholeSale.ECommerce.EcommerceStcRegister', 'flaticon-more', '/app/main/wholesale/ecommerce/stcregister'),
                     new AppMenuItem('StcDealPrice','Pages.Tenant.WholeSale.ECommerce.EcommerceStcDealPrice', 'flaticon-more', '/app/main/wholesale/ecommerce/stcDealPrice'),             
                                         
                 ]),

                 new AppMenuItem('Promotions', 'Pages.WholeSale.PromotionGroup', 'las la-volume-up', '', [], [
                    new AppMenuItem('Promotions', 'Pages.WholeSale.Promotions', 'flaticon-more', '/app/main/wholesale/promotions/promotions'),
                    new AppMenuItem('Promotion Tracker', 'Pages.WholeSale.PromotionUsers', 'flaticon-more', '/app/main/wholesale/promotions/promotionUsers'),
                ]),

                new AppMenuItem('Leads', 'Pages.WholeSale.Leads', 'las la-crosshairs', '/app/main/wholesale/leads'),
                new AppMenuItem('Request To Transfer', 'Pages.WholeSale.RequestToTransferLeads', 'las la-crosshairs', '/app/main/wholesale/requesttotransferlead'),
                
                new AppMenuItem('Invoice', 'Pages.WholeSale.Invoice', 'las la-file-invoice-dollar', '/app/main/wholesale/invoice'),
                 
                 new AppMenuItem('Credit', 'Pages.Tenant.WholeSale.Credit', 'las la-hand-holding-usd', '', [],[
                    new AppMenuItem('Credit Applications', 'Pages.Tenant.WholeSale.Credit.CreditApplication', 'flaticon-more', '/app/main/wholesale/credit/credit-applications'),                    
                ]),
                new AppMenuItem('Contact Us', 'Pages.Tenant.WholeSale.ECommerce.EcommerceContactUs', 'las la-phone', '/app/main/wholesale/ecommerce/contactus'), 

                new AppMenuItem('Notification', 'Pages.Tenant.WholeSale.Notification', 'las la-bell', '', [], [
                    new AppMenuItem('SMS', 'Pages.Tenant.WholeSale.Sms', 'flaticon-more', '/app/main/wholesale/reply'),
                ]),
                
            ]),

             new AppMenuItem('Inventory', 'Pages.Tenant.Dashboard', 'las la-clipboard-list', '', [], [
                //  new AppMenuItem('Dashboard', null, 'las la-tachometer-alt', '/app/main/inventory/dashboard'),                
                 new AppMenuItem('Data Vaults', null, 'las la-volume-up', '', [], [
                    new AppMenuItem('Activity', 'Pages.Tenant.QuickStock.DataVaults.ActivityLog', 'flaticon-more', '/app/main/inventory/data-vaults/activitylog'),
                    new AppMenuItem('Currency', 'Pages.Tenant.QuickStock.DataVaults.Currencies', 'flaticon-more', '/app/main/quickstock/currency'),
                    new AppMenuItem('Delivery Type', 'Pages.Tenant.QuickStock.DataVaults.DeliveryTypes', 'flaticon-more', '/app/main/quickstock/deliverytype'),
                    new AppMenuItem('Freight Company', 'Pages.Tenant.QuickStock.DataVaults.FreightCompany', 'flaticon-more', '/app/main/quickstock/freightcompany'),
                    new AppMenuItem('Inco Term', 'Pages.Tenant.QuickStock.DataVaults.IncoTerm', 'flaticon-more', '/app/main/quickstock/incoTerm'),
                    new AppMenuItem('Payment Method', 'Pages.Tenant.QuickStock.DataVaults.PaymentMethod', 'flaticon-more', '/app/main/quickstock/paymentmethod'),
                    new AppMenuItem('Payment Status', 'Pages.Tenant.QuickStock.DataVaults.PaymentStatus', 'flaticon-more', '/app/main/quickstock/paymentstatus'),
                    new AppMenuItem('Payment Type', 'Pages.Tenant.QuickStock.DataVaults.PaymentType', 'flaticon-more', '/app/main/quickstock/paymenttype'),
                    new AppMenuItem('Purchase Company', 'Pages.Tenant.QuickStock.DataVaults.PurchaseCompany', 'flaticon-more', '/app/main/quickstock/PurchaseCompany'),
                    new AppMenuItem('Purchase Document List', 'Pages.Tenant.QuickStock.DataVaults.PurchaseDocumentList', 'flaticon-more', '/app/main/quickstock/purchasedocumentlist'),
                    new AppMenuItem('Stock From', 'Pages.Tenant.QuickStock.DataVaults.StockFroms', 'flaticon-more', '/app/main/quickstock/stockfrom'),
                    new AppMenuItem('Stock Order For', 'Pages.Tenant.QuickStock.DataVaults.StockOrderFors', 'flaticon-more', '/app/main/quickstock/stockorderfor'),
                    new AppMenuItem('Stock Order Status', 'Pages.Tenant.QuickStock.DataVaults.StockOrderStatus', 'flaticon-more', '/app/main/quickstock/stockorderstatus'),
                    new AppMenuItem('Transport Company', 'Pages.Tenant.QuickStock.DataVaults.TransportCompanies', 'flaticon-more', '/app/main/transportcompany'),
                    new AppMenuItem('Vendors', 'Pages.Tenant.QuickStock.DataVaults.Vendor', 'flaticon-more', '/app/main/quickstock/vendors'),
                    new AppMenuItem('Variation', 'Pages.Tenant.QuickStock.DataVaults.StockVariations', 'flaticon-more', '/app/main/quickstock/stock-variation'),
                    new AppMenuItem('SerialNo Status', 'Pages.Tenant.QuickStock.DataVaults.SerialNoStatus', 'flaticon-more', '/app/main/inventory/data-vaults/serialnostatuses'),
                 ]),
                new AppMenuItem('Stock Order', "Pages.Tenant.QuickStock.PurchaseOrder", 'las la-clipboard-list', '/app/main/inventory/purchase-order-new'),
                new AppMenuItem('Stock Transfer', "Pages.Tenant.QuickStock.StockTransfer", 'las la-shipping-fast', '/app/main/quickstock/stockTransfers'),
                new AppMenuItem('Stock Payment', "Pages.Tenant.QuickStock.StockPayment", 'las la-wallet', '/app/main/inventory/stock-payment'),
                new AppMenuItem('Stock Payment Tracker', "Pages.Tenant.QuickStock.StockPaymentTracker", 'las la-wallet', '/app/main/inventory/stock-payment-tracker'),
                new AppMenuItem('Reports', null, 'las la-volume-up', '', [], [
                    new AppMenuItem('Stock  Received Report', 'Pages.Tenant.QuickStock.Reports.StockReceivedReport', 'flaticon-more', '/app/main/inventory/reports/stockOrderReceivedReport'),
                    new AppMenuItem('SerialNo History Report', 'Pages.Tenant.QuickStock.Reports.SerialNoHistoryReport', 'flaticon-more', '/app/main/inventory/reports/serialNoHistoryReport'),
                  
                 ]),
                //  new AppMenuItem('Purchase Order', null, 'las la-cart-plus', '/app/main/inventory/purchase-order-new'),                             
             ]),

            //new AppMenuItem('DemoUiComponents', 'Pages.DemoUiComponents', 'flaticon-shapes', '/app/admin/demo-ui-components'),
            
            new AppMenuItem('Administration', 'Pages.Administration', 'las la-user-cog', '', [], [
                new AppMenuItem('Organization', 'Pages.Administration.OrganizationUnits', 'flaticon-more', '/app/admin/organization-units'),
                new AppMenuItem('Roles', 'Pages.Administration.Roles', 'flaticon-more', '/app/admin/roles'),
                new AppMenuItem('Users', 'Pages.Administration.Users', 'flaticon-more', '/app/admin/users'),
                new AppMenuItem('Installer', 'Pages.Administration.Installer', 'flaticon-more', '/app/admin/installer'),
                new AppMenuItem('Sales Rep', 'Pages.Administration.SalesRep', 'flaticon-more', '/app/admin/salesuser'),
                new AppMenuItem('Sales Manager', 'Pages.Administration.SalesManager', 'flaticon-more', '/app/admin/salesmanager'),
                //new AppMenuItem('Languages', 'Pages.Administration.Languages', 'flaticon-tabs', '/app/admin/languages', ['/app/admin/languages/{name}/texts']),
                new AppMenuItem('Audit Logs', 'Pages.Administration.AuditLogs', 'flaticon-more', '/app/admin/auditLogs'),
                new AppMenuItem('Maintenance', 'Pages.Administration.Host.Maintenance', 'flaticon-more', '/app/admin/maintenance'),

                //new AppMenuItem('Subscription', 'Pages.Administration.Tenant.SubscriptionManagement', 'flaticon-refresh', '/app/admin/subscription-management'),
                //new AppMenuItem('VisualSettings', 'Pages.Administration.UiCustomization', 'flaticon-medical', '/app/admin/ui-customization'),
                //new AppMenuItem('WebhookSubscriptions', 'Pages.Administration.WebhookSubscription', 'flaticon2-world', '/app/admin/webhook-subscriptions'),
                // new AppMenuItem('DynamicParameters', '', 'flaticon-interface-8', '', [], [
                //     new AppMenuItem('Definitions', 'Pages.Administration.DynamicParameters', '', '/app/admin/dynamic-parameter'),
                //     new AppMenuItem('EntityDynamicParameters', 'Pages.Administration.EntityDynamicParameters', '', '/app/admin/entity-dynamic-parameter'),
                // ]),
                new AppMenuItem('Settings', 'Pages.Administration.Host.Settings', 'flaticon-more', '/app/admin/hostSettings'),
                new AppMenuItem('Settings', 'Pages.Administration.Tenant.Settings', 'flaticon-more', '/app/admin/tenantSettings')
            ]),
            
        ]);
    }

    checkChildMenuItemPermission(menuItem): boolean {

        for (let i = 0; i < menuItem.items.length; i++) {
            let subMenuItem = menuItem.items[i];

            if (subMenuItem.permissionName === '' || subMenuItem.permissionName === null) {
                if (subMenuItem.route) {
                    return true;
                }
            } else if (this._permissionCheckerService.isGranted(subMenuItem.permissionName)) {
                return true;
            }

            if (subMenuItem.items && subMenuItem.items.length) {
                let isAnyChildItemActive = this.checkChildMenuItemPermission(subMenuItem);
                if (isAnyChildItemActive) {
                    return true;
                }
            }
        }
        return false;
    }

    showMenuItem(menuItem: AppMenuItem): boolean {
        if (menuItem.permissionName === 'Pages.Administration.Tenant.SubscriptionManagement' && this._appSessionService.tenant && !this._appSessionService.tenant.edition) {
            return false;
        }

        let hideMenuItem = false;

        if (menuItem.requiresAuthentication && !this._appSessionService.user) {
            hideMenuItem = true;
        }

        if (menuItem.permissionName && !this._permissionCheckerService.isGranted(menuItem.permissionName)) {
            hideMenuItem = true;
        }

        if (this._appSessionService.tenant || !abp.multiTenancy.ignoreFeatureCheckForHostUsers) {
            if (menuItem.hasFeatureDependency() && !menuItem.featureDependencySatisfied()) {
                hideMenuItem = true;
            }
        }

        if (!hideMenuItem && menuItem.items && menuItem.items.length) {
            return this.checkChildMenuItemPermission(menuItem);
        }

        return !hideMenuItem;
    }

    /**
     * Returns all menu items recursively
     */
    getAllMenuItems(): AppMenuItem[] {

        // this.loadNotifications();

        let menu = this.getMenu();
        let allMenuItems: AppMenuItem[] = [];
        menu.items.forEach(menuItem => {
            allMenuItems = allMenuItems.concat(this.getAllMenuItemsRecursive(menuItem));
        });

        return allMenuItems;
    }

    private getAllMenuItemsRecursive(menuItem: AppMenuItem): AppMenuItem[] {

        // this.loadNotifications();

        if (!menuItem.items) {
            return [menuItem];
        }

        let menuItems = [menuItem];
        menuItem.items.forEach(subMenu => {
            menuItems = menuItems.concat(this.getAllMenuItemsRecursive(subMenu));
        });

        return menuItems;
    }
}
