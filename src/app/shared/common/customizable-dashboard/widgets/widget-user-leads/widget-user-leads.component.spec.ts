import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetUserLeadsComponent } from './widget-user-leads.component';

describe('WidgetUserLeadsComponent', () => {
  let component: WidgetUserLeadsComponent;
  let fixture: ComponentFixture<WidgetUserLeadsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetUserLeadsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetUserLeadsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
