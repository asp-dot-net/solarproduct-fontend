import { Component, ElementRef, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { LeadsServiceProxy, GetLeadForViewDto, LeadDto,ActivityLogInput, GetLeadForChangeStatusOutput, LookupTableDto } from '@shared/service-proxies/service-proxies';
import * as _ from 'lodash';
import * as moment from 'moment';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
    selector: 'rejectLeadModel',
    templateUrl: './reject-lead-modal.component.html',
})
export class RejectLeadModelComponent extends AppComponentBase{

    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    RejectReason:string;
    RejectReasonId : number;
    LeadId:number;	
    active = false;
    saving = false;
    allRejectReasons: LookupTableDto[];
    constructor(
        injector: Injector,      
        private _leadsServiceProxy: LeadsServiceProxy
    ) {
        super(injector);
    }

     show(LeadId?: number): void {        
        this.LeadId = LeadId;
        this.modal.show();
        this._leadsServiceProxy.getAllRejectReasonForTableDropdown().subscribe(result => {						
            this.allRejectReasons = result;
        });
     }

     close(): void {
        this.active = false;
        this.RejectReason = '';
        this.modal.hide();
     }

     save(): void{
        let status :GetLeadForChangeStatusOutput = new GetLeadForChangeStatusOutput();
        status.id = this.LeadId;
        status.leadStatusID = 7;
        status.rejectReason = this.RejectReason;
        status.rejectReasonId = this.RejectReasonId;
        
        this._leadsServiceProxy.changeStatus(status)
            .subscribe(() => {
                this.notify.success(this.l('LeadRejectedSuccessfully'));
                this.RejectReason = '';
                this.modal.hide();
                this.modalSave.emit(null);
            });
    }
}
