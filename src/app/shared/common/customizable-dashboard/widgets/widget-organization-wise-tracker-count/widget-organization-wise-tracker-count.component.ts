
import { Component, Injector, OnInit } from "@angular/core";
import { TenantDashboardServiceProxy, OrganizationWiseApplicationTracker, OrganizationUnitDto, UserServiceProxy, TrackerCountList, CommonLookupServiceProxy } from "@shared/service-proxies/service-proxies";
import { WidgetComponentBase } from "../widget-component-base";

@Component({
  selector: 'app-widget-organization-wise-tracker-count',
  templateUrl: './widget-organization-wise-tracker-count.component.html',
  styleUrls: ['./widget-organization-wise-tracker-count.component.css']
})
export class WidgetOrganizationWiseTrackerCountComponent extends WidgetComponentBase implements OnInit {
  records: OrganizationWiseApplicationTracker = new OrganizationWiseApplicationTracker();
  resultList: TrackerCountList;
  result: TrackerCountList[];
  allOrganizationUnits: OrganizationUnitDto[];
  //filter:GetEditionTenantStatisticsInput={};
  //selectedDateRange: moment.Moment[] = [moment().add(-6, 'month').endOf('day'), moment().add(+1, 'days').startOf('day')];
  constructor(injector: Injector,
    private _tenantDashboardService: TenantDashboardServiceProxy,
    private _commonLookupService: CommonLookupServiceProxy,
    private _userServiceProxy: UserServiceProxy) {
    super(injector);
  }
  ngOnInit(): void {
    this.runDelayed(() => {
      this.getOrgWiseTrackerCount();
      this.getpvtOrgWiseTrackerCount();
    });
  }
  getOrgWiseTrackerCount(): void {
    this._commonLookupService.getOrganizationUnit().subscribe(output => {
      this.allOrganizationUnits = output;
    });
    this._tenantDashboardService.trackerList().subscribe(result => {
      this.resultList = result;
    });
  }
  getpvtOrgWiseTrackerCount(): void {
    this._tenantDashboardService.pivotTrackerList().subscribe(result => {
      
      this.result = result;
    });
  }
}