import { Component, OnInit, Injector, OnDestroy } from '@angular/core';
import { UserDashboardServiceProxy, GetLeadForDashboardDto, GetLeadForChangeStatusOutput, LeadsServiceProxy } from '@shared/service-proxies/service-proxies';
import { WidgetComponentBase } from '../widget-component-base';
import { Router } from '@angular/router';

@Component({
  selector: 'app-widget-manager-leads',
  templateUrl: './widget-manager-leads.component.html',
  styleUrls: ['./widget-manager-leads.component.css']
})
export class WidgetManagerLeadsComponent extends WidgetComponentBase implements OnInit, OnDestroy {

  helloResponse: string;
  userLead: GetLeadForDashboardDto[];
  inActiveLead: GetLeadForDashboardDto[];
  RejectReason: string;
  LeadId: number;
  Length: number = 0;
  inActiveLeadlength: number = 0;
  leadCount: number = 0;
  salesRepId = 0;
  role: string = '';
  constructor(injector: Injector,
    private _leadsServiceProxy: LeadsServiceProxy,
    private _userDashboardService: UserDashboardServiceProxy,
    private _router: Router) {
    super(injector);
  }

  ngOnInit(): void {
    this.runDelayed(() => {
      this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
        this.role = result;
        this.getUserLead(this.salesRepId);
      });

    });
    this.registerToEvents();
  }

  getUserLead = (salesRepId) => {
    // this._userDashboardService.getUserLeadForDashboard().subscribe((result) => {
    //   this.userLead = result;
    //   this.Length = result.length;
    // });

    this._userDashboardService.getInactiveLeadForDashboard(salesRepId).subscribe((result) => {
      this.inActiveLead = result;
      this.inActiveLeadlength = result.length;
    });

    this._userDashboardService.getAllUnAssignedCount().subscribe((result) => {
      this.leadCount = result;
    });
  }

  registerToEvents() {
    abp.event.on('app.onRejectModalSaved', () => {
      this.getUserLead(this.salesRepId);
    });
  }

  reject(id): void {
    abp.event.trigger('app.show.rejectLeadModal', id);
  }

  addToMyLead(leadId): void {
    this._router.navigate(['/app/main/leads/leads/createOrEdit'], { queryParams: { id: leadId, from: "myleaddb" } });
  }

  unHandled(id): void {
    let status: GetLeadForChangeStatusOutput = new GetLeadForChangeStatusOutput();
    status.id = id;
    status.leadStatusID = 2;

    this.message.confirm('',
      this.l('AreYouSure'),
      (isConfirmed) => {
        if (isConfirmed) {
          this._leadsServiceProxy.changeStatus(status)
            .subscribe(() => {
              this.getUserLead(this.salesRepId);
              this.notify.success(this.l('LeadStatusChangedSuccessfully'));
            });
        }
      }
    );
  }
}