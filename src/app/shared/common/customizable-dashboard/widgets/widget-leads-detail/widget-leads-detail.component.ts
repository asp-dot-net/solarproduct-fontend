
import { Component, Injector, OnDestroy, OnInit } from "@angular/core";
import { TenantDashboardServiceProxy, SalesDetailDto, LeadsServiceProxy, LeadUsersLookupTableDto } from "@shared/service-proxies/service-proxies";
import * as moment from "moment";
import { WidgetComponentBase } from "../widget-component-base";

@Component({
  selector: 'app-widget-leads-detail',
  templateUrl: './widget-leads-detail.component.html',
  styleUrls: ['./widget-leads-detail.component.css']
})
export class widgetLeadsDetailComponent extends WidgetComponentBase implements OnInit {
  countrecords: SalesDetailDto;
  role: string = '';
  new = 0;
  unHandled = 0;
  cold = 0;
  warm = 0;
  hot = 0;
  upgrade = 0;
  rejected = 0;
  canceled = 0;
  closed = 0;
  assigned = 0;
  reAssigned = 0;
  planned = 0;
  plannedTotalCost = 0;
  onHold = 0;
  onHoldTotalCost = 0;
  cancel = 0;
  cancelTotalCost = 0;
  depositeReceived = 0;
  depositeReceivedTotalCost = 0;
  active = 0;
  activeTotalCost = 0;
  jobBook = 0;
  jobIncomplete = 0;
  jobIncompleteTotalCost = 0;
  jobInstalled = 0;
  jobInstalledTotalCost = 0;
  totalJobs = 0;
  totalJobsCost = 0;
  totalLeads=0;
  jobBookTotalCost = 0;
  salesRepId = 0;
  filteredReps: LeadUsersLookupTableDto[];
  //filter:GetEditionTenantStatisticsInput={};
  //selectedDateRange: moment.Moment[] = [moment().add(-6, 'month').endOf('day'), moment().add(+1, 'days').startOf('day')];
  constructor(injector: Injector,
    private _tenantDashboardService: TenantDashboardServiceProxy,
    private _leadsServiceProxy: LeadsServiceProxy) {
    super(injector);
  }
  ngOnInit(): void {
    this.subDateRangeFilter();
    this.runDelayed(() => {
      this._leadsServiceProxy.getSalesRepForFilter(1, undefined).subscribe(rep => {
        this.filteredReps = rep;
      });
      this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
        this.role = result;
        this.getSaleRepDetailCount(this.salesRepId);
      });
    });
  }

  onDateRangeFilterChange = (dateRange) => {
    this.salesRepId = dateRange;
    this.runDelayed(() => {
      this.getSaleRepDetailCount(this.salesRepId);
    });
  }

  subDateRangeFilter() {
    abp.event.on('app.dashboardFilters.salesrepusers.onChange', this.onDateRangeFilterChange);
  }

  unSubDateRangeFilter() {
    abp.event.off('app.dashboardFilters.salesrepusers.onChange', this.onDateRangeFilterChange);
  }

  ngOnDestroy(): void {
    this.unSubDateRangeFilter();
  }

  getSaleRepDetailCount(salesRepId): void {
    
    this._tenantDashboardService.getLeadCountsForSalesRep(
      salesRepId
    ).subscribe(result => {
      this.countrecords = result;
      this.new = result.new;
      this.unHandled = result.unHandled;
      this.cold = result.cold;
      this.warm = result.warm;
      this.hot = result.hot;
      this.upgrade = result.upgrade;
      this.rejected = result.rejected;
      this.canceled = result.canceled;
      this.closed = result.closed;
      this.assigned = result.assigned;
      this.reAssigned = result.reAssigned;
      this.planned = result.planned;
      this.plannedTotalCost = result.plannedTotalCost;
      this.onHold = result.onHold;
      this.onHoldTotalCost = result.onHoldTotalCost;
      this.cancel = result.cancel;
      this.cancelTotalCost = result.cancelTotalCost;
      this.depositeReceived = result.depositeReceived;
      this.depositeReceivedTotalCost = result.depositeReceivedTotalCost;
      this.active = result.active;
      this.activeTotalCost = result.activeTotalCost;
      this.jobBook = result.jobBook;
      this.jobIncomplete = result.jobIncomplete;
      this.jobIncompleteTotalCost = result.jobIncompleteTotalCost;
      this.jobInstalled = result.jobInstalled;
      this.jobInstalledTotalCost = result.jobInstalledTotalCost;
      this.totalJobs = result.totalJobs;
      this.totalJobsCost = result.totalJobsCost;
      this.jobBookTotalCost = result.jobBookTotalCost;
      this.totalLeads=result.totalLeads;
    });
  }
}