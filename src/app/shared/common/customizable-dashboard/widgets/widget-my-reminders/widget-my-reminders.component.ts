
import { Component, Injector, OnDestroy, OnInit } from "@angular/core";
import { TenantDashboardServiceProxy, MyRemindersDto } from "@shared/service-proxies/service-proxies";
import * as moment from "moment";
import { WidgetComponentBase } from "../widget-component-base";

@Component({
  selector: 'app-widget-my-reminders',
  templateUrl: './widget-my-reminders.component.html',
  styleUrls: ['./widget-my-reminders.component.css']
})
export class widgetmyremindersComponent extends WidgetComponentBase implements OnInit {
  records: MyRemindersDto[] = [];
  recordslength: number = 0;
  //filter:GetEditionTenantStatisticsInput={};
  //selectedDateRange: moment.Moment[] = [moment().add(-6, 'month').endOf('day'), moment().add(+1, 'days').startOf('day')];
  constructor(injector: Injector,
    private _tenantDashboardService: TenantDashboardServiceProxy) {
    super(injector);
  }
  ngOnInit(): void {
    this.runDelayed(() => {
      this.getMyReminders();
    });
  }
  getMyReminders(): void {
    this._tenantDashboardService.myReminder().subscribe(result => {
      this.records = result;
      this.recordslength= result.length;
    });
  }
} 