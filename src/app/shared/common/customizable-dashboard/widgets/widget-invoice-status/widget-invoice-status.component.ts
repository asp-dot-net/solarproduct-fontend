
import { Component, Injector, OnDestroy, OnInit } from "@angular/core";
import { TenantDashboardServiceProxy, InvoiceStatusDetailDto } from "@shared/service-proxies/service-proxies";
import * as moment from "moment";
import { WidgetComponentBase } from "../widget-component-base";

@Component({
  selector: 'app-widget-invoice-status',
  templateUrl: './widget-invoice-status.component.html',
  styleUrls: ['./widget-invoice-status.component.css']
})
export class widgetInvoiceStatusComponent extends WidgetComponentBase implements OnInit {
  records: InvoiceStatusDetailDto[] = [];
  selectedDateRange: moment.Moment[] = [moment().add(-6, 'month').endOf('day'), moment().add(+1, 'days').startOf('day')];
  constructor(injector: Injector,
    private _tenantDashboardService: TenantDashboardServiceProxy) {
    super(injector);
  }
  ngOnInit(): void {
    this.runDelayed(() => {
      this.getOrgWiseInvoiceDetail();
    });
  }
  getOrgWiseInvoiceDetail(): void {
    this._tenantDashboardService.getInvoiceStatusDetail(this.selectedDateRange[0], this.selectedDateRange[1]).subscribe(result => {
       this.records = result;
    });
  }
  onDateRangeFilterChange = (dateRange) => {
    if (!dateRange || dateRange.length !== 2 || (this.selectedDateRange[0] === dateRange[0] && this.selectedDateRange[1] === dateRange[1])) {
      return;
    }

    this.selectedDateRange[0] = dateRange[0];
    this.selectedDateRange[1] = dateRange[1];
    this.runDelayed(() => {
     // this.getOrgWiseInvoiceDetail();
    });
  }
}