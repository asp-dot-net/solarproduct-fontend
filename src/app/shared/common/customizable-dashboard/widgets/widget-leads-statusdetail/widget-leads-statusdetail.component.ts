
import { Component, Injector, OnDestroy, OnInit } from "@angular/core";
import { TenantDashboardServiceProxy, SalesDetailDto, LeadsServiceProxy, LeadUsersLookupTableDto, LeadStatuswisecountDto } from "@shared/service-proxies/service-proxies";
import * as moment from "moment";
import { WidgetComponentBase } from "../widget-component-base";

@Component({
  selector: 'app-widget-leads-statusdetail',
  templateUrl: './widget-leads-statusdetail.component.html',
  styleUrls: ['./widget-leads-statusdetail.component.css'] 
  
})
export class widgetLeadsStatusDetailComponent extends WidgetComponentBase implements OnInit {
  countrecords: LeadStatuswisecountDto;
  role: string = '';
  tv = 0;
  google = 0;
  facebook = 0;
  refferal  = 0;
  others = 0; 

  tvupgrade = 0;
  googleupgrade = 0;
  facebookupgrade = 0;
  refferalupgrade  = 0;
  othersupgrade = 0; 

  tvsales = 0;
  googlesales = 0;
  facebooksales = 0;
  refferalsales  = 0;
  otherssales = 0; 

  tvRatio = 0;
  googleRatio = 0;
  facebookRatio = 0;
  refferalRatio  = 0;
  othersRatio = 0; 
  salesRepId = 0;
  filteredReps: LeadUsersLookupTableDto[];
  date = new Date();
  firstDay = moment(new Date(this.date.getFullYear(), this.date.getMonth(), 1));
  lastday   = moment().endOf('month');
   selectedDateRange: moment.Moment[] = [this.firstDay, this.lastday];
  //filter:GetEditionTenantStatisticsInput={};
  //selectedDateRange: moment.Moment[] = [moment().add(-6, 'month').endOf('day'), moment().add(+1, 'days').startOf('day')];
  constructor(injector: Injector,
    private _tenantDashboardService: TenantDashboardServiceProxy,
    private _leadsServiceProxy: LeadsServiceProxy) {
    super(injector);
  }
  ngOnInit(): void {
    this.subDateRangeFilter();
    this.runDelayed(() => {
      this._leadsServiceProxy.getSalesRepForFilter(1, undefined).subscribe(rep => {
        this.filteredReps = rep;
      });
      this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
        this.role = result;
        this.getSaleRepDetailCount(this.salesRepId);
      });
    });
  }

  onDateRangeFilterChange = (dateRange) => {
    this.salesRepId = dateRange;
    this.runDelayed(() => {
      this.getSaleRepDetailCount(this.salesRepId);
    });
  }

  subDateRangeFilter() {
    abp.event.on('app.dashboardFilters.salesrepusers.onChange', this.onDateRangeFilterChange);
  }

  unSubDateRangeFilter() {
    abp.event.off('app.dashboardFilters.salesrepusers.onChange', this.onDateRangeFilterChange);
  }

  ngOnDestroy(): void {
    this.unSubDateRangeFilter();
  }

  getSaleRepDetailCount(salesRepId): void {
    
    this._tenantDashboardService.getLeadStatusCountlist(
      salesRepId,this.selectedDateRange[0], this.selectedDateRange[1]
    ).subscribe(result => {
      this.countrecords = result;
      this.tv = result.tv;
      this.facebook = result.faceBook;
      this.google = result.google;
      this.others = result.others;
      this.refferal = result.refferal;

      this.tvupgrade = result.tvupgrade;
      this.facebookupgrade = result.faceBookupgrade;
      this.googleupgrade = result.googleupgrade;
      this.othersupgrade = result.othersupgrade;
      this.refferalupgrade = result.refferalupgrade;

      this.tvsales = result.tvsales;
      this.facebooksales = result.faceBooksales;
      this.googlesales= result.googlesales;
      this.otherssales = result.otherssales;
      this.refferalsales = result.refferalsales;

      this.tvRatio = result.tvRatio;
      this.facebookRatio = result.faceBookRatio;
      this.googleRatio = result.googleRatio;
      this.othersRatio = result.othersRatio;
      this.refferalRatio = result.refferalRatio;
      
    });
  }
}