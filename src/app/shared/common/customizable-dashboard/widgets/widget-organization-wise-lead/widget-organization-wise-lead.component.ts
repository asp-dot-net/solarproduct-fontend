
import { Component, Injector, OnDestroy, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { TenantDashboardServiceProxy, LeadSummary, UserServiceProxy, OrganizationUnitDto, LeadStatusWiseCount, GetEditionTenantStatisticsOutput, CommonLookupServiceProxy } from "@shared/service-proxies/service-proxies";
import * as _ from "lodash";
import * as moment from "moment";
import { WidgetComponentBase } from "../widget-component-base";

@Component({
  selector: 'app-widget-organization-wise-lead',
  templateUrl: './widget-organization-wise-lead.component.html',
  styleUrls: ['./widget-organization-wise-lead.component.css']
})
export class WidgetOrganizationWiseLeadComponent extends WidgetComponentBase implements OnInit, OnDestroy {
  records: LeadSummary[] = [];
  allOrganizationUnits: OrganizationUnitDto[];
  //filter:GetEditionTenantStatisticsInput={};
  selectedDateRange: moment.Moment[] = [moment().add(-6, 'month').endOf('day'), moment().add(+1, 'days').startOf('day')];
  count: LeadStatusWiseCount;
  showDetail: boolean = false;
  StartDate: moment.Moment;
  EndDate: moment.Moment;
  constructor(injector: Injector,
    private _tenantDashboardService: TenantDashboardServiceProxy,
    private _commonLookupService: CommonLookupServiceProxy,
    private _userServiceProxy: UserServiceProxy,
    private _router: Router) {
    super(injector);
  }

  ngOnInit(): void {
    this.subDateRangeFilter();
    this.runDelayed(() => {
      this.getOrgWiseLead();
    });
  }

  getOrgWiseLead(): void {
    this._commonLookupService.getOrganizationUnit().subscribe(output => {
      this.allOrganizationUnits = output;
    });
    this._tenantDashboardService.getOrgList(this.selectedDateRange[0], this.selectedDateRange[1]).subscribe(result => {
      this.records = result;
    });
  }

  onDateRangeFilterChange = (dateRange) => {
    if (!dateRange || dateRange.length !== 2 || (this.selectedDateRange[0] === dateRange[0] && this.selectedDateRange[1] === dateRange[1])) {
      return;
    }

    this.selectedDateRange[0] = dateRange[0];
    this.selectedDateRange[1] = dateRange[1];
    this.runDelayed(() => {
      this.getOrgWiseLead();
    });
  }

  subDateRangeFilter() {
    abp.event.on('app.dashboardFilters.dateRangePicker.onDateChange', this.onDateRangeFilterChange);
  }

  unSubDateRangeFilter() {
    abp.event.off('app.dashboardFilters.dateRangePicker.onDateChange', this.onDateRangeFilterChange);
  }

  ngOnDestroy(): void {
    this.unSubDateRangeFilter();
  }

  detail(orgId): void {
    this.StartDate = moment(this.selectedDateRange[0]);
    const SDate = this.StartDate.format('YYYY-MM-DDTHH:mm:ssZ');
    this.EndDate = moment(this.selectedDateRange[1]);
    const EDate = this.EndDate.format('YYYY-MM-DDTHH:mm:ssZ');
    this._router.navigate(['/app/main/organization-wise-lead-count/organization-wise-lead-count'], { queryParams: { orgId: orgId, sdate: SDate, edate: EDate } });

  }
}