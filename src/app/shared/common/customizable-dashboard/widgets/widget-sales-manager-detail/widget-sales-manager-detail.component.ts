
import { Component, Injector, OnDestroy, OnInit } from "@angular/core";
import { TenantDashboardServiceProxy, SalesDetailDto, LeadsServiceProxy, LeadUsersLookupTableDto } from "@shared/service-proxies/service-proxies";
import * as moment from "moment";
import { WidgetComponentBase } from "../widget-component-base";

@Component({
  selector: 'app-widget-sales-manager-detail.component',
  templateUrl: './widget-sales-manager-detail.component.html',
  styleUrls: ['./widget-sales-manager-detail.component.css']
})
export class widgetSalesManagerDetailComponent extends WidgetComponentBase implements OnInit {
  records: SalesDetailDto[] = [];
  countrecords: SalesDetailDto;
  role: string = '';
  new = 0;
  //filter:GetEditionTenantStatisticsInput={};
  //selectedDateRange: moment.Moment[] = [moment().add(-6, 'month').endOf('day'), moment().add(+1, 'days').startOf('day')];
  constructor(injector: Injector,
    private _tenantDashboardService: TenantDashboardServiceProxy,
    private _leadsServiceProxy: LeadsServiceProxy) {
    super(injector);
  }
  ngOnInit(): void {
    // this.runDelayed(() => {
        this.getSalesDetail();
    // });
  }
  getSalesDetail(): void {
    this._tenantDashboardService.salesDetail().subscribe(result => {
      this.records = result;
    });
  }
}