
import { Component, Injector, OnDestroy, OnInit } from "@angular/core";
import { TenantDashboardServiceProxy, LeadsServiceProxy, ToDoDetailDto, LeadUsersLookupTableDto } from "@shared/service-proxies/service-proxies";
import * as moment from "moment";
import { WidgetComponentBase } from "../widget-component-base";

@Component({
  selector: 'app-widget-todo-detail',
  templateUrl: './widget-todo-detail.component.html',
  styleUrls: ['./widget-todo-detail.component.css']
})
export class widgetToDoDetailComponent extends WidgetComponentBase implements OnInit {
  records: ToDoDetailDto[] = [];
  role: string = '';
  new = 0;
  salesRepId=0;
  filteredReps: LeadUsersLookupTableDto[];
  //filter:GetEditionTenantStatisticsInput={};
  //selectedDateRange: moment.Moment[] = [moment().add(-6, 'month').endOf('day'), moment().add(+1, 'days').startOf('day')];
  constructor(injector: Injector,
    private _tenantDashboardService: TenantDashboardServiceProxy,
    private _leadsServiceProxy: LeadsServiceProxy) {
    super(injector);
  }
  ngOnInit(): void {
    this.subDateRangeFilter();
    this.runDelayed(() => {
      this._leadsServiceProxy.getSalesRepForFilter(1, undefined).subscribe(rep => {
        this.filteredReps = rep;
      });
        this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
          this.role = result;
          this.gettodoDetail(this.salesRepId);
        });
    });
    this.registerToEvents();
  }

  onDateRangeFilterChange = (dateRange) => {
    debugger;
    console.log(dateRange);
    this.salesRepId = dateRange;
    this.runDelayed(() => {
      this.gettodoDetail(this.salesRepId);
    });
  }

  subDateRangeFilter() {
    abp.event.on('app.dashboardFilters.salesrepusers.onChange', this.onDateRangeFilterChange);
  }

  unSubDateRangeFilter() {
    abp.event.off('app.dashboardFilters.salesrepusers.onChange', this.onDateRangeFilterChange);
  }

  ngOnDestroy(): void {
    this.unSubDateRangeFilter();
  }

  gettodoDetail(salesRepId): void {
    this._tenantDashboardService.getToDoDetail(salesRepId).subscribe(result => {
      this.records = result;
    });
  }

  registerToEvents(){
    abp.event.on('app.onremarkModalSaved', () => {
        this.gettodoDetail(this.salesRepId);
    });
  }
  addremark(id): void {
    
    abp.event.trigger('app.show.addRemarkModal', id);
  }
  
}