import { Component, ElementRef, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { LeadsServiceProxy, GetLeadForViewDto, LeadDto,ActivityLogInput, GetLeadForChangeStatusOutput, LookupTableDto, CreateEditActivityLogDto } from '@shared/service-proxies/service-proxies';
import * as _ from 'lodash';
import * as moment from 'moment';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
    selector: 'addRemarkModel',
    templateUrl: './add-remart-modal.component.html',
})
export class AddRemarkModelComponent extends AppComponentBase{

    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    body:string;
    Id:number;	
    active = false;
    saving = false;
    allRejectReasons: LookupTableDto[];
    constructor(
        injector: Injector,      
        private _leadsServiceProxy: LeadsServiceProxy
    ) {
        super(injector);
    }

     show(Id?: number): void { 
                
        this.Id = Id;
        this.body = '';
        this.modal.show();
     }

     close(): void {
        this.active = false;
        this.body = '';
        this.modal.hide();
     }

     save(): void{
        let status :CreateEditActivityLogDto = new CreateEditActivityLogDto();
        status.id = this.Id;
        status.body = this.body;
        this._leadsServiceProxy.updateLeadActivityDetailForTodo(status)
            .subscribe(() => {
                this.notify.success(this.l('Successfully'));
                this.body = '';
                this.modal.hide();
                this.modalSave.emit(null);
            });
    }
}
