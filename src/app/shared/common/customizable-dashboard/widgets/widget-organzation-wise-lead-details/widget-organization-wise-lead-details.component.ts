
import { Component, Injector, OnDestroy, OnInit } from "@angular/core";
import { TenantDashboardServiceProxy, LeadSummaryDetails } from "@shared/service-proxies/service-proxies";
import * as moment from "moment";
import { WidgetComponentBase } from "../widget-component-base";

@Component({
  selector: 'app-widget-organization-wise-lead-details',
  templateUrl: './widget-organization-wise-lead-details.component.html',
  styleUrls: ['./widget-organization-wise-lead-details.component.css']
})
export class WidgetOrganizationWiseLeaddetailsComponent extends WidgetComponentBase implements OnInit, OnDestroy {
  records: LeadSummaryDetails[] = [];
  //filter:GetEditionTenantStatisticsInput={};
  selectedDateRange: moment.Moment[] = [moment().add(-6, 'month').endOf('day'), moment().add(+1, 'days').startOf('day')];
  constructor(injector: Injector,
    private _tenantDashboardService: TenantDashboardServiceProxy) {
    super(injector);
  }
  ngOnInit(): void {
    this.subDateRangeFilter();
    this.runDelayed(() => {
      this.getOrgWiseLead();
    });
  }
  getOrgWiseLead(): void {
    this._tenantDashboardService.getLeadSummaryDetails(this.selectedDateRange[0], this.selectedDateRange[1]).subscribe(result => {
      this.records = result;
    });
  }
  onDateRangeFilterChange = (dateRange) => {
    if (!dateRange || dateRange.length !== 2 || (this.selectedDateRange[0] === dateRange[0] && this.selectedDateRange[1] === dateRange[1])) {
      return;
    }

    this.selectedDateRange[0] = dateRange[0];
    this.selectedDateRange[1] = dateRange[1];
    this.runDelayed(() => {
      this.getOrgWiseLead();
    });
  }

  subDateRangeFilter() {
    abp.event.on('app.dashboardFilters.dateRangePicker.onDateChange', this.onDateRangeFilterChange);
  }

  unSubDateRangeFilter() {
    abp.event.off('app.dashboardFilters.dateRangePicker.onDateChange', this.onDateRangeFilterChange);
  }

  ngOnDestroy(): void {
    this.unSubDateRangeFilter();
  }
}