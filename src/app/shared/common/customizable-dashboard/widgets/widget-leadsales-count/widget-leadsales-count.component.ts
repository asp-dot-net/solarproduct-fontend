
import { Component, Injector, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { TenantDashboardServiceProxy, SalesDetailDto, LeadsServiceProxy, LeadUsersLookupTableDto, LeadStatusWiseCount, LeadStatusWiseCountDetailDto } from "@shared/service-proxies/service-proxies";
import * as moment from "moment";
//import { ModalDirective } from "ngx-bootstrap/modal";

import { WidgetComponentBase } from "../widget-component-base";
 
// import { ViewLeadcountComponent } from "./view-leadcount.component";

@Component({
  selector: 'app-widget-leadsales-count',
  templateUrl: './widget-leadsales-count.component.html',
  styleUrls: ['./widget-leadsales-count.component.css']
})
export class widgetLeadsalesCountComponent extends WidgetComponentBase implements OnInit {

  //@ViewChild('viewleadModal', { static: true }) modal: ModalDirective;
   
  countrecords: LeadStatusWiseCount;
  countrecordsMonthly: LeadStatusWiseCountDetailDto[];
  countrecordsDaily: LeadStatusWiseCountDetailDto[];
  date = new Date();
  firstDay = moment(new Date(this.date.getFullYear(), this.date.getMonth(), 1));
  lastday   = moment().endOf('month');
  show: boolean = true;
  showchild: boolean = true;
  shouldShow: boolean = false;
  addTaskShow: boolean = false;
 // selectedDateRange: moment.Moment[] = [this.firstDay, this.lastday];
  role: string = '';
  new = 0;
  unHandled = 0;
  cold = 0;
  warm = 0;
  hot = 0;
  upgrade = 0;
  rejected = 0;
  canceled = 0;
  closed = 0;
  assigned = 0;
  reAssigned = 0;
  planned = 0;
  plannedTotalCost = 0;
  onHold = 0;
  onHoldTotalCost = 0;
  cancel = 0;
  cancelTotalCost = 0;
  depositeReceived = 0;
  depositeReceivedTotalCost = 0;
  active = 0;
  activeTotalCost = 0;
  jobBook = 0;
  jobIncomplete = 0;
  jobIncompleteTotalCost = 0;
  jobInstalled = 0;
  jobInstalledTotalCost = 0;
  totalJobs = 0;
  totalJobsCost = 0;
  jobBookTotalCost = 0;
  salesRepId = 0;
  totalrecord=0;
  lead1=0;
  filteredReps: LeadUsersLookupTableDto[];
   
  
  //filter:GetEditionTenantStatisticsInput={};
  selectedDateRange: moment.Moment[] = [this.firstDay, this.lastday];
  usernm: string;
  constructor(injector: Injector,
    private _tenantDashboardService: TenantDashboardServiceProxy,
    private _leadsServiceProxy: LeadsServiceProxy) {
    super(injector);
  }
  ngOnInit(): void {
    this.subDateRangeFilter();
    this.runDelayed(() => {
      this._leadsServiceProxy.getSalesRepForFilter(1, undefined).subscribe(rep => {
        this.filteredReps = rep;
      });
      this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
        this.role = result;
        this.getSaleRepDetailCount();
      });
    });
  }

  onDateRangeFilterChange = (dateRange) => {
    this.salesRepId = dateRange;
    this.runDelayed(() => {
      this.getSaleRepDetailCount();
    });
  }

  subDateRangeFilter() {
    abp.event.on('app.dashboardFilters.salesrepusers.onChange', this.onDateRangeFilterChange);
  }

  unSubDateRangeFilter() {
    abp.event.off('app.dashboardFilters.salesrepusers.onChange', this.onDateRangeFilterChange);
  }

  ngOnDestroy(): void {
    this.unSubDateRangeFilter();
  }
  getdata(): void {

  }
  getSaleRepDetailCount( ): void {
    
    this._tenantDashboardService.leadSourceCountwiserank(this.selectedDateRange[0], this.selectedDateRange[1]
      
    ).subscribe(result => {
      debugger;
      this.countrecords = result;
      this.usernm=result.username;
      this.totalrecord=result.total;
      this.lead1=result.lead1;
    });
  }
  getSaleRepDetailCountDetail( ): void {
    
    this._tenantDashboardService.leadSourceDetailMonthly(this.selectedDateRange[0], this.selectedDateRange[1]
      
    ).subscribe(result => {
      debugger;
      this.countrecordsMonthly=result;
    });
    this._tenantDashboardService.leadSourceDetailDaily(this.selectedDateRange[0], this.selectedDateRange[1]
      
      ).subscribe(result => {
        debugger;
        this.countrecordsDaily=result;
      });
     
  }
 
}