import { Component, ViewChild, Injector, Output, EventEmitter, OnInit, ElementRef } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {
    ProductItemsServiceProxy, CreateOrEditProductItemDto, ProductItemProductTypeLookupTableDto, DemoUiComponentsServiceProxy, GetDocumentTypeForView, QuotationsServiceProxy, UploadDocumentInput, GetJobDocumentForView, JobsServiceProxy, CreateOrEditJobDto
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Observable } from "@node_modules/rxjs";
import { FileItem, FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { AppConsts } from '@shared/AppConsts';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';

@Component({
    selector: 'monyhlyleadsModal',
    templateUrl: './monyhlyleads.component.html',
    animations: [appModuleAnimation()]
})
export class MonyhlyleadsComponent extends AppComponentBase {
    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @ViewChild('myInput')
    myInputVariable: ElementRef;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    saving = false;
    productItem: CreateOrEditProductItemDto = new CreateOrEditProductItemDto();
    productTypeName = '';
    allProductTypes: ProductItemProductTypeLookupTableDto[];
    public uploader: FileUploader;
    public maxfileBytesUserFriendlyValue = 5;
    private _uploaderOptions: FileUploaderOptions = {};
    filenName = [];
    uploadUrl: string;
    job: CreateOrEditJobDto = new CreateOrEditJobDto();
    uploadedFiles: any[] = [];
    DocumentTypes: GetDocumentTypeForView[]; 
    JobDocuments: GetJobDocumentForView[];
    public fileupload: FileUploader;
    public temporaryFileUrl: string; 
    private _fileuploadoption: FileUploaderOptions = {};
    documentTypeId: number;
    constructor(
        injector: Injector, private _jobServiceProxy: JobsServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _tokenService: TokenService,
        private _productItemsServiceProxy: ProductItemsServiceProxy,
        private demoUiComponentsService: DemoUiComponentsServiceProxy,
        private _quotationsServiceProxy: QuotationsServiceProxy,
        
        private _router: Router
    ) {
        super(injector);
    }

    show(): void {
        this._jobServiceProxy.getAllDocumentType().subscribe(result => {
            this.DocumentTypes = result;
        });

        this._jobServiceProxy.getJobByLeadId(1).subscribe(result => {

             debugger;
                this.job = result.job;
            //    this.freebies =  result3.jobPromotion;
            //    this.invoicepayment =  result3.invoicePayment;
            //    this.jobrefund =  result3.refund;
            this.active = true;
            this.initializeModal();
            this.modal.show();
        })
    }
    initializeModal(): void {
        this.active = true;
        this.initFileUploader();
      }
    // save(): void {
    //     if (this.uploader.queue.length == 0) {
    //         this.notify.warn(this.l('SelectFileToUpload'));
    //         return;
    //     }
    //     this.uploader.uploadAll();
    // }

    // upload completed event
    fileChangeEvent(event: any): void {
        if (event.target.files[0].size > 5242880) { //5MB
            this.message.warn(this.l('ProfilePicture_Warn_SizeLimit', this.maxfileBytesUserFriendlyValue));
            return;
        }
        this.uploader.clearQueue();
        this.uploader.addToQueue([<File>event.target.files[0]]);
    }
    guid(): string {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }

    guid1(): string {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }

    getJobDocuments(): void {
        this._quotationsServiceProxy.getJobDocumentsByJobId(this.job.id).subscribe(result => {
            this.JobDocuments = result;
        })
    }

    updateFile(fileToken: string, fileName: string, fileType: string, filePath: string): void {
        debugger;
        const input = new UploadDocumentInput();
        input.fileToken = fileToken;
        input.jobId = this.job.id;
        input.documentTypeId = this.documentTypeId;
        input.fileName = fileName;
        input.fileType = fileType;
        input.filePath = filePath;
        this.saving = true;
        this._jobServiceProxy.saveDocument(input)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.myInputVariable.nativeElement.value = "";
                this.modalSave.emit(null);
                this.notify.info(this.l('SavedSuccessfully'));
            });
    }
    initFileUploader(): void {
        this.uploader = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Profile/UploadFile' });
        this._uploaderOptions.autoUpload = false;
        this._uploaderOptions.authToken = 'Bearer ' + this._tokenService.getToken();
        this._uploaderOptions.removeAfterUpload = true;
        this.uploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };
        this.fileupload = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Profile/UploadFile' });
        this._fileuploadoption.autoUpload = false;
        this._fileuploadoption.authToken = 'Bearer ' + this._tokenService.getToken();
        this._fileuploadoption.removeAfterUpload = true;
        this.fileupload.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

        this.uploader.onBuildItemForm = (fileItem: FileItem, form: any) => {
            form.append('FileType', fileItem.file.type);
            form.append('FileName', fileItem.file.name);
            form.append('FileToken', this.guid());
            this.filenName.push(fileItem.file.name);

        };
        this.fileupload.onBuildItemForm = (fileItem: FileItem, form: any) => {
            form.append('FileType', fileItem.file.type);
            form.append('FileName', fileItem.file.name);
            form.append('FileToken', this.guid1());
            this.filenName.push(fileItem.file.name);

        };
        this.uploader.onSuccessItem = (item, response, status) => {
            const resp = <IAjaxResponse>JSON.parse(response);
            if (resp.success) {
                this.updateFile(resp.result.fileToken, resp.result.fileName, resp.result.fileType, resp.result.filePath);
            } else {
                this.message.error(resp.error.message);
            }
        };
        
        this.uploader.setOptions(this._uploaderOptions);
        this.fileupload.setOptions(this._fileuploadoption);
    }
    savedocument(): void {
        debugger;
        if (this.documentTypeId == undefined) {
            this.notify.warn(this.l('SelectDocumentType'));
            return;
        }
        if (this.uploader.queue.length == 0) {
            this.notify.warn(this.l('SelectFileToUpload'));
            return;
        }
        this.uploader.uploadAll();
    };
    save(): void {
        this.modal.hide();
    }
    cancel(): void {
        this.modal.hide();
    }
    close(): void {
        this.modal.hide();
      }
}
