
import { Component, Injector, OnDestroy, OnInit } from "@angular/core";
import { TenantDashboardServiceProxy, LeadsServiceProxy, ToDoDetailDto } from "@shared/service-proxies/service-proxies";
import * as moment from "moment";
import { WidgetComponentBase } from "../widget-component-base";

@Component({
  selector: 'app-widget-todo-manager-detail',
  templateUrl: './widget-todo-manager-detail.component.html',
  styleUrls: ['./widget-todo-manager-detail.component.css']
})
export class widgetManagerToDoDetailComponent extends WidgetComponentBase implements OnInit {
  records: ToDoDetailDto[] = [];
  role: string = '';
  new = 0;
  salesRepId=0;
  //filter:GetEditionTenantStatisticsInput={};
  //selectedDateRange: moment.Moment[] = [moment().add(-6, 'month').endOf('day'), moment().add(+1, 'days').startOf('day')];
  constructor(injector: Injector,
    private _tenantDashboardService: TenantDashboardServiceProxy,
    private _leadsServiceProxy: LeadsServiceProxy) {
    super(injector);
  }
  ngOnInit(): void {
    this.runDelayed(() => {
        this.gettodoDetail(this.salesRepId);
     
    });
    this.registerToEvents();
  }
  gettodoDetail(salesRepId): void {
    this._tenantDashboardService.getToDoDetail(salesRepId).subscribe(result => {
      this.records = result;
    });
  }

  registerToEvents(){
    abp.event.on('app.onremarkModalSaved', () => {
        this.gettodoDetail(this.salesRepId);
    });
  }
  addremark(id): void {
    
    abp.event.trigger('app.show.addRemarkModal', id);
  }
  
}