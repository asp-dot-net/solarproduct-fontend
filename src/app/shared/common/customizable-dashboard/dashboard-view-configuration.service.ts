import { Injectable, OnInit } from '@angular/core';
import { WidgetViewDefinition, WidgetFilterViewDefinition } from './definitions';
import { DashboardCustomizationConst } from './DashboardCustomizationConsts';
import { WidgetGeneralStatsComponent } from './widgets/widget-general-stats/widget-general-stats.component';
import { WidgetDailySalesComponent } from './widgets/widget-daily-sales/widget-daily-sales.component';
import { WidgetProfitShareComponent } from './widgets/widget-profit-share/widget-profit-share.component';
import { WidgetMemberActivityComponent } from './widgets/widget-member-activity/widget-member-activity.component';
import { WidgetRegionalStatsComponent } from './widgets/widget-regional-stats/widget-regional-stats.component';
import { WidgetSalesSummaryComponent } from './widgets/widget-sales-summary/widget-sales-summary.component';
import { WidgetIncomeStatisticsComponent } from './widgets/widget-income-statistics/widget-income-statistics.component';
import { WidgetRecentTenantsComponent } from './widgets/widget-recent-tenants/widget-recent-tenants.component';
import { WidgetEditionStatisticsComponent } from './widgets/widget-edition-statistics/widget-edition-statistics.component';
import { WidgetSubscriptionExpiringTenantsComponent } from './widgets/widget-subscription-expiring-tenants/widget-subscription-expiring-tenants.component';
import { WidgetHostTopStatsComponent } from './widgets/widget-host-top-stats/widget-host-top-stats.component';
import { FilterDateRangePickerComponent } from './filters/filter-date-range-picker/filter-date-range-picker.component';
import { WidgetTopStatsComponent } from './widgets/widget-top-stats/widget-top-stats.component';
import { WidgetUserLeadsComponent } from './widgets/widget-user-leads/widget-user-leads.component';
import { WidgetOrganizationWiseLeadComponent } from './widgets/widget-organization-wise-lead/widget-organization-wise-lead.component';
import { WidgetOrganizationWiseLeaddetailsComponent } from './widgets/widget-organzation-wise-lead-details/widget-organization-wise-lead-details.component';
import { widgetmyremindersComponent } from './widgets/widget-my-reminders/widget-my-reminders.component';
import { WidgetOrganizationWiseTrackerCountComponent } from './widgets/widget-organization-wise-tracker-count/widget-organization-wise-tracker-count.component';
import { widgetSalesDetailComponent } from './widgets/widget-sales-detail/widget-sales-detail.component';
import { widgetInvoiceStatusComponent } from './widgets/widget-invoice-status/widget-invoice-status.component';
import { widgetSalesManagerDetailComponent } from './widgets/widget-sales-manager-detail/widget-sales-manager-detail.component';
import { WidgetManagerLeadsComponent } from './widgets/widget-manager-leads/widget-manager-leads.component';
import { FilterSalesRepUsersComponent } from './filter-sales-rep-users/filter-sales-rep-users.component';
import { widgetToDoDetailComponent } from './widgets/widget-todo-detail/widget-todo-detail.component';
import { widgetManagerToDoDetailComponent } from './widgets/widget-todo-manager-detail/widget-todo-manager-detail.component';
import { widgetLeadsalesCountComponent } from './widgets/widget-leadsales-count/widget-leadsales-count.component';
import { widgetLeadsDetailComponent } from './widgets/widget-leads-detail/widget-leads-detail.component';
import { widgetLeadsStatusComponent } from './widgets/widget-leads-status/widget-leads-status.component';
import { widgetLeadsStatusDetailComponent } from './widgets/widget-leads-statusdetail/widget-leads-statusdetail.component';
 

@Injectable({
  providedIn: 'root'
})
export class DashboardViewConfigurationService {
  public WidgetViewDefinitions: WidgetViewDefinition[] = [];
  public widgetFilterDefinitions: WidgetFilterViewDefinition[] = [];

  constructor(
  ) {
    this.initializeConfiguration();
  }

  private initializeConfiguration() {
    let filterDateRangePicker = new WidgetFilterViewDefinition(
      DashboardCustomizationConst.filters.filterDateRangePicker,
      FilterDateRangePickerComponent
    );

    let filterSaleRepUsers = new WidgetFilterViewDefinition(
      DashboardCustomizationConst.filters.filterSaleRepUsers,
      FilterSalesRepUsersComponent
    );
    
    //add your filters here
    this.widgetFilterDefinitions.push(filterDateRangePicker);
    this.widgetFilterDefinitions.push(filterSaleRepUsers);

    let generalStats = new WidgetViewDefinition(
      DashboardCustomizationConst.widgets.tenant.generalStats,
      WidgetGeneralStatsComponent,
      6,
      4
    );

    let dailySales = new WidgetViewDefinition(
      DashboardCustomizationConst.widgets.tenant.dailySales,
      WidgetDailySalesComponent,
    );

    let profitShare = new WidgetViewDefinition(
      DashboardCustomizationConst.widgets.tenant.profitShare,
      WidgetProfitShareComponent
    );

    let memberActivity = new WidgetViewDefinition(
      DashboardCustomizationConst.widgets.tenant.memberActivity,
      WidgetMemberActivityComponent,
    );

    let regionalStats = new WidgetViewDefinition(
      DashboardCustomizationConst.widgets.tenant.regionalStats,
      WidgetRegionalStatsComponent
    );

    let salesSummary = new WidgetViewDefinition(
      DashboardCustomizationConst.widgets.tenant.salesSummary,
      WidgetSalesSummaryComponent,
    );

    let topStats = new WidgetViewDefinition(
      DashboardCustomizationConst.widgets.tenant.topStats,
      WidgetTopStatsComponent,
    );

    let userLeads = new WidgetViewDefinition(
      DashboardCustomizationConst.widgets.tenant.userLeads,
      WidgetUserLeadsComponent,
    );
    //add your tenant side widgets here

    let incomeStatistics = new WidgetViewDefinition(
      DashboardCustomizationConst.widgets.host.incomeStatistics,
      WidgetIncomeStatisticsComponent,
    );

    let editionStatistics = new WidgetViewDefinition(
      DashboardCustomizationConst.widgets.host.editionStatistics,
      WidgetEditionStatisticsComponent,
    );

    let recentTenants = new WidgetViewDefinition(
      DashboardCustomizationConst.widgets.host.recentTenants,
      WidgetRecentTenantsComponent,
    );

    let subscriptionExpiringTenants = new WidgetViewDefinition(
      DashboardCustomizationConst.widgets.host.subscriptionExpiringTenants,
      WidgetSubscriptionExpiringTenantsComponent
    );

    let hostTopStats = new WidgetViewDefinition(
      DashboardCustomizationConst.widgets.host.topStats,
      WidgetHostTopStatsComponent,
    );
    let organizationWiseLead = new WidgetViewDefinition(
      DashboardCustomizationConst.widgets.tenant.OrganizationWiseLead,
      WidgetOrganizationWiseLeadComponent,
    );
    let OrganizationWiseLeaddetails = new WidgetViewDefinition(
      DashboardCustomizationConst.widgets.tenant.OrganizationWiseLeaddetails,
      WidgetOrganizationWiseLeaddetailsComponent,
    );
    let MyReminders = new WidgetViewDefinition(
      DashboardCustomizationConst.widgets.tenant.MyReminders,
      widgetmyremindersComponent,
    );

    let OrganizationWiseTrackerCount = new WidgetViewDefinition(
      DashboardCustomizationConst.widgets.tenant.OrganizationWiseTrackerCount,
      WidgetOrganizationWiseTrackerCountComponent,
    );

    let SalesDetails = new WidgetViewDefinition(
      DashboardCustomizationConst.widgets.tenant.SalesDetails,
      widgetSalesDetailComponent, 
    );
    let LeadsDetails = new WidgetViewDefinition(
      DashboardCustomizationConst.widgets.tenant.LeadsDetails,
      widgetLeadsDetailComponent,
    );
    

    let ManagerSalesDetails = new WidgetViewDefinition(
      DashboardCustomizationConst.widgets.tenant.ManagerSalesDetails,
      widgetSalesManagerDetailComponent,
    );


    let InvoiceStatus = new WidgetViewDefinition(
      DashboardCustomizationConst.widgets.tenant.InvoiceStatus,
      widgetInvoiceStatusComponent,
    );

    let ToDoDetails = new WidgetViewDefinition(
      DashboardCustomizationConst.widgets.tenant.ToDoDetails,
      widgetToDoDetailComponent,
    );

    let ManagerToDoDetails = new WidgetViewDefinition(
      DashboardCustomizationConst.widgets.tenant.ManagerToDoDetails,
      widgetManagerToDoDetailComponent,
    );
    
    let ManagerLeads = new WidgetViewDefinition(
      DashboardCustomizationConst.widgets.tenant.ManagerLeads,
      WidgetManagerLeadsComponent,
    );

    let LeadsSalesRank=new WidgetViewDefinition(
      DashboardCustomizationConst.widgets.tenant.LeadsSalesRank,
      widgetLeadsalesCountComponent,)

      let LeadsStatus=new WidgetViewDefinition(
        DashboardCustomizationConst.widgets.tenant.LeadsStatus,
        widgetLeadsStatusComponent,)

        let LeadsStatusDetail=new WidgetViewDefinition(
          DashboardCustomizationConst.widgets.tenant.LeadsStatusDetail,
          widgetLeadsStatusDetailComponent,)
        
    //add your host side widgets here

    this.WidgetViewDefinitions.push(generalStats);
    this.WidgetViewDefinitions.push(dailySales);
    this.WidgetViewDefinitions.push(profitShare);
    this.WidgetViewDefinitions.push(memberActivity);
    this.WidgetViewDefinitions.push(regionalStats);
    this.WidgetViewDefinitions.push(salesSummary);
    this.WidgetViewDefinitions.push(topStats);
    this.WidgetViewDefinitions.push(incomeStatistics);
    this.WidgetViewDefinitions.push(editionStatistics);
    this.WidgetViewDefinitions.push(recentTenants);
    this.WidgetViewDefinitions.push(subscriptionExpiringTenants);
    this.WidgetViewDefinitions.push(hostTopStats);
    this.WidgetViewDefinitions.push(userLeads);
    this.WidgetViewDefinitions.push(organizationWiseLead);
    this.WidgetViewDefinitions.push(OrganizationWiseLeaddetails);
    this.WidgetViewDefinitions.push(MyReminders);
    this.WidgetViewDefinitions.push(OrganizationWiseTrackerCount);
    this.WidgetViewDefinitions.push(SalesDetails);
    this.WidgetViewDefinitions.push(InvoiceStatus);
    this.WidgetViewDefinitions.push(ToDoDetails);
    this.WidgetViewDefinitions.push(ManagerSalesDetails);
    this.WidgetViewDefinitions.push(ManagerToDoDetails);
    this.WidgetViewDefinitions.push(ManagerLeads);
    this.WidgetViewDefinitions.push(LeadsSalesRank);
    this.WidgetViewDefinitions.push(LeadsDetails);
    this.WidgetViewDefinitions.push(LeadsStatus);
    this.WidgetViewDefinitions.push(LeadsStatusDetail);
    
    
  }
}
