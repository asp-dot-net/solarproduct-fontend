export class DashboardCustomizationConst {
    static widgets = {
        tenant: {
            profitShare: 'Widgets_Tenant_ProfitShare',
            memberActivity: 'Widgets_Tenant_MemberActivity',
            regionalStats: 'Widgets_Tenant_RegionalStats',
            salesSummary: 'Widgets_Tenant_SalesSummary',
            topStats: 'Widgets_Tenant_TopStats',
            generalStats: 'Widgets_Tenant_GeneralStats',
            dailySales: 'Widgets_Tenant_DailySales',
            userLeads: 'Widgets_Tenant_UserLeads',
            OrganizationWiseLead:"Widgets_Tenant_OrganizationWiseLead",
            OrganizationWiseLeaddetails:"Widgets_Tenant_OrganizationWiseLeaddetails",
            MyReminders:"Widgets_Tenant_MyReminders",
            OrganizationWiseTrackerCount:"Widgets_Tenant_OrganizationWiseTrackerCount",
            SalesDetails:"Widgets_Tenant_SalesDetails",
            InvoiceStatus:"Widgets_Tenant_InvoiceStatus",
            ToDoDetails:"Widgets_Tenant_ToDoDetails",
            ManagerSalesDetails:"Widgets_Tenant_ManagerSalesDetails",
            ManagerToDoDetails:"Widgets_Tenant_ManagerToDoDetails",
            ManagerLeads:"Widgets_Tenant_ManagerLeads",
            LeadsSalesRank:"Widgets_Tenant_LeadsSalesRank",
            LeadsDetails:"Widgets_Tenant_LeadsDetails",
            LeadsStatus:"Widgets_Tenant_LeadsStatus" ,
            LeadsStatusDetail:"Widgets_Tenant_LeadsStatusDetail" 
        },
        host: {
            topStats: 'Widgets_Host_TopStats',
            incomeStatistics: 'Widgets_Host_IncomeStatistics',
            editionStatistics: 'Widgets_Host_EditionStatistics',
            subscriptionExpiringTenants: 'Widgets_Host_SubscriptionExpiringTenants',
            recentTenants: 'Widgets_Host_RecentTenants'
        }
    };
    static filters = {
        filterDateRangePicker: 'Filters_DateRangePicker',
        filterSaleRepUsers:'Filters_SalesRepUsers'
    };
    static dashboardNames = {
        defaultTenantDashboard: 'TenantDashboard',
        defaultHostDashboard: 'HostDashboard',
    };
    static Applications = {
        Angular: 'Angular'
    };
}
