import { Component, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { LeadsServiceProxy, LeadUsersLookupTableDto } from '@shared/service-proxies/service-proxies';
import * as moment from 'moment';

@Component({
  selector: 'app-filter-sales-rep-users',
  templateUrl: './filter-sales-rep-users.component.html',
  styleUrls: ['./filter-sales-rep-users.component.css']
})
export class FilterSalesRepUsersComponent extends AppComponentBase {

  date: Date;
  selectedDateRange: moment.Moment[] = [moment().add(-6, 'days').endOf('day'), moment().add(+1, 'days').startOf('day')];
  filteredReps: LeadUsersLookupTableDto[];
  salesRepId = 0;
  role: string = '';

  constructor(injector: Injector,
    private _leadsServiceProxy: LeadsServiceProxy) {
    super(injector);
    this._leadsServiceProxy.getSalesRepForFilter(1, undefined).subscribe(rep => {
      this.filteredReps = rep;
    });
    this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
      this.role = result;
    });
  }

  onChange() {
    debugger;
    abp.event.trigger('app.dashboardFilters.salesrepusers.onChange', this.salesRepId);
  }
}
