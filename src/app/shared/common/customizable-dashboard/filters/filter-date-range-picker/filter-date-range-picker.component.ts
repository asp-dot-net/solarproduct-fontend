import { Component, Injector, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { LeadsServiceProxy, LeadUsersLookupTableDto } from '@shared/service-proxies/service-proxies';
import * as moment from 'moment';

@Component({
  selector: 'app-filter-date-range-picker',
  templateUrl: './filter-date-range-picker.component.html',
  styleUrls: ['./filter-date-range-picker.component.css']
})
export class FilterDateRangePickerComponent extends AppComponentBase implements OnInit {

  date: Date;
  selectedDateRange: moment.Moment[] = [moment().add(-6, 'days').endOf('day'), moment().add(+1, 'days').startOf('day')];
  filteredReps: LeadUsersLookupTableDto[];
  role: string = '';
  salesRepId = 0;

  constructor(injector: Injector
    ,private _leadsServiceProxy: LeadsServiceProxy) {
    super(injector);
  }

  ngOnInit(): void {
    this.runDelayed(() => {
      this._leadsServiceProxy.getSalesRepForFilter(1, undefined).subscribe(rep => {
        this.filteredReps = rep;
      });
      this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
        this.role = result;
      });
    });
  }

  onChange() {
    debugger;
    abp.event.trigger('app.dashboardFilters.dateRangePicker.onDateChange', this.selectedDateRange);
  }
}
