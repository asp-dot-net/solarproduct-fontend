import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganizationWiseLeadCountComponent } from './organization-wise-lead-count.component';

describe('OrganizationWiseLeadCountComponent', () => {
  let component: OrganizationWiseLeadCountComponent;
  let fixture: ComponentFixture<OrganizationWiseLeadCountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganizationWiseLeadCountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganizationWiseLeadCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
