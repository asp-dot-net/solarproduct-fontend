import { Component, Injector, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { TenantDashboardServiceProxy, LeadSummary, UserServiceProxy, OrganizationUnitDto, LeadStatusWiseCount, GetEditionTenantStatisticsOutput } from "@shared/service-proxies/service-proxies";
import * as _ from "lodash";
import * as moment from "moment";

@Component({
  selector: 'app-organization-wise-lead-count',
  templateUrl: './organization-wise-lead-count.component.html',
  styleUrls: ['./organization-wise-lead-count.component.css']
})
export class OrganizationWiseLeadCountComponent implements OnInit {
  selectedDateRange: moment.Moment[] = [moment().add(-6, 'month').endOf('day'), moment().add(+1, 'days').startOf('day')];
  count: LeadStatusWiseCount;
  showDetail: boolean = false;
  areaHasData = false;
  typeHasData = false;
  stateHasData = false;
  leadstatusHasData = false;
  leadsourceHasData = false;
  typetotalData;
  areatotalData;
  leadstatustotalData;
  statetotalData;
  leadsourcetotalData;
  orgId;
  sdate;
  edate;
  constructor(injector: Injector,
    private _tenantDashboardService: TenantDashboardServiceProxy,
    private _userServiceProxy: UserServiceProxy,
    private _activatedRoute: ActivatedRoute,
    private _router: Router) {
    // super(injector);
  }

  ngOnInit(): void {
    this.orgId = this._activatedRoute.snapshot.queryParams['orgId'];
    this.sdate = moment(this._activatedRoute.snapshot.queryParams['sdate']);
    this.edate = moment(this._activatedRoute.snapshot.queryParams['edate']);
    this._tenantDashboardService.leadSourceWiseCount(this.orgId, this.sdate, this.edate).subscribe(result => {
      this.count = result;
      this.showDetail = true;
      this.showareaChart(this.orgId, this.sdate, this.edate);
      this.showtypeChart(this.orgId, this.sdate, this.edate);
      this.showstateChart(this.orgId, this.sdate, this.edate);
      this.showleadstatusChart(this.orgId, this.sdate, this.edate);
      this.showleadSourceChart(this.orgId, this.sdate, this.edate);
    });
  }
  // Area Pie Chart
  showareaChart = (orgId, sDate, EDate) => {
    this._tenantDashboardService.getareaStatisticsData(orgId, sDate, EDate)
      .subscribe((editionTenantStatistics) => {
        this.areatotalData = this.normalizeareaData(editionTenantStatistics);
        this.areaHasData = _.filter(this.areatotalData, data => data.value > 0).length > 0;
      });
  }

  normalizeareaData(data: GetEditionTenantStatisticsOutput): Array<any> {
    if (!data || !data.editionStatistics || data.editionStatistics.length === 0) {
      return [];
    }

    const chartData = new Array(data.editionStatistics.length);

    for (let i = 0; i < data.editionStatistics.length; i++) {
      chartData[i] = {
        name: data.editionStatistics[i].label,
        value: data.editionStatistics[i].value
      };
    }

    return chartData;
  }

  // Type Pie Chart
  showtypeChart = (orgId, sDate, EDate) => {
    this._tenantDashboardService.gettypeStatisticsData(orgId, sDate, EDate)
      .subscribe((editionTenantStatistics) => {
        this.typetotalData = this.normalizetypeData(editionTenantStatistics);
        this.typeHasData = _.filter(this.typetotalData, data => data.value > 0).length > 0;
      });
  }

  normalizetypeData(data: GetEditionTenantStatisticsOutput): Array<any> {
    if (!data || !data.editionStatistics || data.editionStatistics.length === 0) {
      return [];
    }

    const chartData = new Array(data.editionStatistics.length);

    for (let i = 0; i < data.editionStatistics.length; i++) {
      chartData[i] = {
        name: data.editionStatistics[i].label,
        value: data.editionStatistics[i].value
      };
    }

    return chartData;
  }

  //State Pie Chart
  showstateChart = (orgId, sDate, EDate) => {
    this._tenantDashboardService.getstateStatisticsData(orgId, sDate, EDate)
      .subscribe((editionTenantStatistics) => {
        this.statetotalData = this.normalizestateData(editionTenantStatistics);
        this.stateHasData = _.filter(this.statetotalData, data => data.value > 0).length > 0;
      });
  }

  normalizestateData(data: GetEditionTenantStatisticsOutput): Array<any> {
    if (!data || !data.editionStatistics || data.editionStatistics.length === 0) {
      return [];
    }

    const chartData = new Array(data.editionStatistics.length);

    for (let i = 0; i < data.editionStatistics.length; i++) {
      chartData[i] = {
        name: data.editionStatistics[i].label,
        value: data.editionStatistics[i].value
      };
    }

    return chartData;
  }


  //leadstatus Pie Chart
  showleadstatusChart = (orgId, sDate, EDate) => {
    this._tenantDashboardService.getLeadStatusStatisticsData(orgId, sDate, EDate)
      .subscribe((editionTenantStatistics) => {
        this.leadstatustotalData = this.normalizeleadstatusData(editionTenantStatistics);
        this.leadstatusHasData = _.filter(this.leadstatustotalData, data => data.value > 0).length > 0;
      });
  }

  normalizeleadstatusData(data: GetEditionTenantStatisticsOutput): Array<any> {
    if (!data || !data.editionStatistics || data.editionStatistics.length === 0) {
      return [];
    }

    const chartData = new Array(data.editionStatistics.length);

    for (let i = 0; i < data.editionStatistics.length; i++) {
      chartData[i] = {
        name: data.editionStatistics[i].label,
        value: data.editionStatistics[i].value
      };
    }

    return chartData;
  }

  //LeadSource Pie Chart
  showleadSourceChart = (orgId, sDate, EDate) => {
    this._tenantDashboardService.getLeadSourceStatisticsData(orgId, sDate, EDate)
      .subscribe((editionTenantStatistics) => {
        this.leadsourcetotalData = this.normalizeleadSourceData(editionTenantStatistics);
        this.leadsourceHasData = _.filter(this.leadsourcetotalData, data => data.value > 0).length > 0;
      });
  }

  normalizeleadSourceData(data: GetEditionTenantStatisticsOutput): Array<any> {
    if (!data || !data.editionStatistics || data.editionStatistics.length === 0) {
      return [];
    }

    const chartData = new Array(data.editionStatistics.length);

    for (let i = 0; i < data.editionStatistics.length; i++) {
      chartData[i] = {
        name: data.editionStatistics[i].label,
        value: data.editionStatistics[i].value
      };
    }

    return chartData;
  }


  close() {
    this.showDetail = false;
    this._router.navigate(['/app/main/dashboard']);
  }

}
// function injector(injector: any) {
//   throw new Error('Function not implemented.');
// }

