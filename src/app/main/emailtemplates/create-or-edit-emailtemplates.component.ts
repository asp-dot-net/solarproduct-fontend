import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { EmailEditorComponent } from 'angular-email-editor';
import { sample } from 'lodash';
import { CommonLookupServiceProxy, CreateOrEditEmailTemplateDto, EmailTemplateServiceProxy, OrganizationUnitDto, UserServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    styleUrls: ['./create-or-edit-emailtemplates.component.less'],
    templateUrl: './create-or-edit-emailtemplates.component.html',
    animations: [appModuleAnimation()]
})

export class CreateOrEditEmailTemplateComponent extends AppComponentBase implements OnInit {
    saving = false;
    emailData = '';
    email: CreateOrEditEmailTemplateDto = new CreateOrEditEmailTemplateDto();
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnit = 0;
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _commonLookupService: CommonLookupServiceProxy,
        private _emailTemplateServiceProxy: EmailTemplateServiceProxy,
        private _router: Router,
        private _userServiceProxy: UserServiceProxy,
    ) {
        super(injector);
    }
    @ViewChild(EmailEditorComponent)
    private emailEditor: EmailEditorComponent;

    ngOnInit(): void {
        this._activatedRoute.queryParams.subscribe(params => {
            this.email.id = params['eid'];
        });
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
        });
    }

    editorLoaded(event) {
        if ((this.email.id !== null && this.email.id !== undefined)) {
            this._emailTemplateServiceProxy.getEmailTemplateForEdit(this.email.id).subscribe(result => {
                this.emailData = result.emailTemplate.body;
                this.email.subject = result.emailTemplate.subject;
                this.email.templateName = result.emailTemplate.templateName;
                this.email.organizationUnitId = result.emailTemplate.organizationUnitId;
                if (this.emailData != "") {
                    if (this.email.id == 3) {
                        this.emailEditor.editor.setMergeTags({
                            customer: {
                                name: "Customer",
                                mergeTags: {
                                    customer_name: {
                                        name: "Name",
                                        value: "{{Customer.Name}}"
                                    },
                                    address: {
                                        name: "Address",
                                        value: "{{Customer.Address}}"
                                    },
                                    mobile: {
                                        name: "Mobile",
                                        value: "{{Customer.Mobile}}"
                                    },
                                    email: {
                                        name: "Email",
                                        value: "{{Customer.Email}}"
                                    },
                                    phone: {
                                        name: "Phone",
                                        value: "{{Customer.Phone}}"
                                    }
                                }
                            },
                            salesrep: {
                                name: "Sales Rep",
                                mergeTags: {
                                    customer_name: {
                                        name: "Name",
                                        value: "{{Sales.Name}}"
                                    },
                                    mobile: {
                                        name: "Mobile",
                                        value: "{{Sales.Mobile}}"
                                    },
                                    email: {
                                        name: "Email",
                                        value: "{{Sales.Email}}"
                                    }
                                }
                            },
                            quote: {
                                name: "Quote",
                                mergeTags: {
                                    project_no: {
                                        name: "Project No",
                                        value: "{{Quote.ProjectNo}}"
                                    },
                                    quote_systemCapacity: {
                                        name: "System Capacity",
                                        value: "{{Quote.SystemCapacity}}"
                                    },
                                    quote_allproductItem: {
                                        name: "All product Item",
                                        value: "{{Quote.AllproductItem}}"
                                    },
                                    quote_totalQuoteprice: {
                                        name: "Total Quote price",
                                        value: "{{Quote.TotalQuoteprice}}"
                                    },
                                    quote_installationDate: {
                                        name: "Installation Date",
                                        value: "{{Quote.InstallationDate}}"
                                    },
                                    quote_InstallerName: {
                                        name: "Installer Name",
                                        value: "{{Quote.InstallerName}}"
                                    },
                                }
                            },
                            freebies: {
                                name: "Freebies",
                                mergeTags: {
                                    freebies_transportcompany: {
                                        name: "Transport Company",
                                        value: "{{Freebies.TransportCompany}}"
                                    },
                                    freebies_transportlink: {
                                        name: "Transport Link",
                                        value: "{{Freebies.TransportLink}}"
                                    },
                                    freebies_dispatchedDate: {
                                        name: "Dispatched Date",
                                        value: "{{Freebies.DispatchedDate}}"
                                    },
                                    freebies_trackingNo: {
                                        name: "TrackingNo",
                                        value: "{{Freebies.TrackingNo}}"
                                    },
                                    freebies_promotype: {
                                        name: "TrackingNo",
                                        value: "{{Freebies.PromoType}}"
                                    },
                                }
                            },
                            invoice: {
                                name: "User",
                                mergeTags: {
                                    freebies_transportcompany: {
                                        name: "User Name",
                                        value: "{{Invoice.UserName}}"
                                    },
                                    freebies_transportlink: {
                                        name: "User Mobile",
                                        value: "{{Invoice.UserMobile}}"
                                    },
                                    freebies_dispatchedDate: {
                                        name: "User Email",
                                        value: "{{Invoice.UserEmail}}"
                                    },
                                }
                            },
                            Organization: {
                                name: "Organization",
                                mergeTags: {
                                    org_name: {
                                        name: "Organization Name",
                                        value: "{{Organization.orgName}}"
                                    },
                                    org_email: {
                                        name: "Organization Email",
                                        value: "{{Organization.orgEmail}}"
                                    },
                                    org_transportlink: {
                                        name: "Organization Mobile",
                                        value: "{{Organization.orgMobile}}"
                                    },
                                    org_dispatchedDate: {
                                        name: "Organization logo",
                                        value: "{{Organization.orglogo}}"
                                    },
                                }
                            },
                            Review: {
                                name: "Review",
                                mergeTags: {
                                    review_googlelink: {
                                        name: "Review link",
                                        value: "{{Review.link}}"
                                    },
                                }
                            },
                        });
                    }
                    else {
                        this.emailEditor.editor.setMergeTags({
                            customer: {
                                name: "Customer",
                                mergeTags: {
                                    customer_name: {
                                        name: "Name",
                                        value: "{{Customer.Name}}"
                                    },
                                    address: {
                                        name: "Address",
                                        value: "{{Customer.Address}}"
                                    },
                                    mobile: {
                                        name: "Mobile",
                                        value: "{{Customer.Mobile}}"
                                    },
                                    email: {
                                        name: "Email",
                                        value: "{{Customer.Email}}"
                                    },
                                    phone: {
                                        name: "Phone",
                                        value: "{{Customer.Phone}}"
                                    },

                                }
                            },
                            salesrep: {
                                name: "Sales Rep",
                                mergeTags: {
                                    customer_name: {
                                        name: "Name",
                                        value: "{{Sales.Name}}"
                                    },
                                    mobile: {
                                        name: "Mobile",
                                        value: "{{Sales.Mobile}}"
                                    },
                                    email: {
                                        name: "Email",
                                        value: "{{Sales.Email}}"
                                    }
                                }
                            },
                            quote: {
                                name: "Quote",
                                mergeTags: {
                                    project_no: {
                                        name: "Project No",
                                        value: "{{Quote.ProjectNo}}"
                                    },
                                    quote_systemCapacity: {
                                        name: "System Capacity",
                                        value: "{{Quote.SystemCapacity}}"
                                    },
                                    quote_allproductItem: {
                                        name: "All product Item",
                                        value: "{{Quote.AllproductItem}}"
                                    },
                                    quote_totalQuoteprice: {
                                        name: "Total Quote price",
                                        value: "{{Quote.TotalQuoteprice}}"
                                    },
                                    quote_installationDate: {
                                        name: "Installation Date",
                                        value: "{{Quote.InstallationDate}}"
                                    },
                                    quote_InstallerName: {
                                        name: "Installer Name",
                                        value: "{{Quote.InstallerName}}"
                                    },
                                }
                            },
                            freebies: {
                                name: "Freebies",
                                mergeTags: {
                                    freebies_transportcompany: {
                                        name: "Transport Company",
                                        value: "{{Freebies.TransportCompany}}"
                                    },
                                    freebies_transportlink: {
                                        name: "Transport Link",
                                        value: "{{Freebies.TransportLink}}"
                                    },
                                    freebies_dispatchedDate: {
                                        name: "Dispatched Date",
                                        value: "{{Freebies.DispatchedDate}}"
                                    },
                                    freebies_trackingNo: {
                                        name: "TrackingNo",
                                        value: "{{Freebies.TrackingNo}}"
                                    },
                                    freebies_promotype: {
                                        name: "TrackingNo",
                                        value: "{{Freebies.PromoType}}"
                                    },
                                }
                            },
                            invoice: {
                                name: "User",
                                mergeTags: {
                                    invoice_userName: {
                                        name: "User Name",
                                        value: "{{Invoice.UserName}}"
                                    },
                                    invoice_userMobile: {
                                        name: "User Mobile",
                                        value: "{{Invoice.UserMobile}}"
                                    },
                                    invoice_userEmail: {
                                        name: "User Email",
                                        value: "{{Invoice.UserEmail}}"
                                    },
                                    invoice_owning: {
                                        name: "Owning",
                                        value: "{{Invoice.Owning}}"
                                    },
                                }
                            },
                            Organization: {
                                name: "Organization",
                                mergeTags: {
                                    org_name: {
                                        name: "Organization Name",
                                        value: "{{Organization.orgName}}"
                                    },
                                    org_email: {
                                        name: "Organization Email",
                                        value: "{{Organization.orgEmail}}"
                                    },
                                    org_transportlink: {
                                        name: "Organization Mobile",
                                        value: "{{Organization.orgMobile}}"
                                    },
                                    org_dispatchedDate: {
                                        name: "Organization logo",
                                        value: "{{Organization.orglogo}}"
                                    },
                                }
                            },
                            Review: {
                                name: "Review",
                                mergeTags: {
                                    review_googlelink: {
                                        name: "Review link",
                                        value: "{{Review.link}}"
                                    },
                                }
                            },
                        });
                    }

                    this.emailEditor.editor.loadDesign(JSON.parse(this.emailData));
                }
            });
        }

    }

    saveDesign() {
        this.emailEditor.editor.saveDesign((data) =>
            this.saveHTML(data)
        );
    }

    saveHTML(data) {
        this.email.body = JSON.stringify(data);
        this._emailTemplateServiceProxy.createOrEdit(this.email)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                //this.notify.info(this.l('SavedSuccessfully'));
                this._router.navigate(['/app/main/emailtemplates/emailtemplates']);
            });
    }

    exportHtml() {
        this.emailEditor.editor.exportHtml(
            (data) => console.log('exportHtml', data.html)
        );
    }

    cancel(): void {
        this._router.navigate(['app/main/emailtemplates/emailtemplates']);
    }
}
