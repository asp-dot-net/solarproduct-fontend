import { Component, Injector, OnInit, ViewEncapsulation } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DashboardCustomizationConst } from '@app/shared/common/customizable-dashboard/DashboardCustomizationConsts';
import { Title } from '@angular/platform-browser';
import { DatePipe } from '@angular/common';
import { assign, result } from 'lodash';
import * as moment from 'moment';
import { CommonLookupServiceProxy, DashboardInvoiceServiceProxy, DashboardSalesServiceProxy,  GetCashInvoiceFinanceInvoiceStateWiseOwingDto, GetInvoiceOverviewDto, OrganizationUnitDto, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './dashboard-invoice.component.html',
    styleUrls: ['./dashboard-invoice.component.less'],
    encapsulation: ViewEncapsulation.None 
})

export class DashboardInvoiceComponent extends AppComponentBase implements OnInit  {
    dashboardName = DashboardCustomizationConst.dashboardNames.defaultTenantDashboard;

    pipe = new DatePipe('en-US');
    today = this.pipe.transform(new Date(), 'MMM dd');
    date = this.pipe.transform(new Date(), 'MMM dd');
    StartDate: moment.Moment = moment(this.today);
    EndDate: moment.Moment = moment(this.today);

    constructor(
        injector: Injector,
        private titleService: Title,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _commonLookupService : CommonLookupServiceProxy,
        private _dashboardSalesServiceProxy : DashboardSalesServiceProxy,
        private _dashoardInvoiceServiceProxy :DashboardInvoiceServiceProxy) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Dashboard Invoice");
    }

    ngOnInit(): void {
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
                
            this.bindusers();
            let log = new UserActivityLogDto();
            log.actionId =79;
            log.actionNote ='Open';
            log.section = 'Dashboard Invoice';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        });
    }

    datetype = "Today";
    showchild: boolean = true;
    shouldShow: boolean = false;
    addTaskShow: boolean = false;
    shouldShowCustom: boolean = false;
    users : any[];
    userId : number;
    usersLength = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnitlength = 0;
    organizationUnit = 0;
    Missed = 0;
    Inbound = 0;
    Outbound = 0;
    NotAnswerd  = 0;
    invoiceOverview : GetInvoiceOverviewDto = new GetInvoiceOverviewDto();
    cash_finance_invoice_stateWiseOwing : GetCashInvoiceFinanceInvoiceStateWiseOwingDto = new GetCashInvoiceFinanceInvoiceStateWiseOwingDto();

    onClick(e) {
        debugger;
        this.datetype = e.target.innerHTML;
        if(e.target.innerHTML == "Today"){
            this.StartDate = moment(this.today);
            this.EndDate = moment(this.today);
            this.date = this.pipe.transform(new Date(), 'MMM dd');
        }

        if(e.target.innerHTML == "Yesterday"){
           
            var Yesterday = new Date();
            Yesterday.setDate(new Date().getDate() - 1)
            this.StartDate = moment(Yesterday);
            this.EndDate = moment(Yesterday);
            this.date = this.pipe.transform(Yesterday, 'MMM dd');
        }

        if(e.target.innerHTML == "Last 7 Days"){
            var mindate = new Date();
            mindate.setDate(new Date().getDate() - 7)

            this.StartDate = moment(mindate);
            this.EndDate = moment(this.today);

            this.date = this.pipe.transform(mindate, 'MMM dd') + '-' + this.pipe.transform(new Date(), 'MMM dd');
        }
        if(e.target.innerHTML == "Last 30 Days"){
            var mindate = new Date();
            mindate.setDate(new Date().getDate() - 30)

            this.StartDate = moment(mindate);
            this.EndDate = moment(this.today);

            this.date = this.pipe.transform(mindate, 'MMM dd') + '-' + this.pipe.transform(new Date(), 'MMM dd');
        }
        if(e.target.innerHTML == "This Month"){
            var date = new Date();
            var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
            var lastDay = new Date(date.getFullYear(), date.getMonth()+1, 0);

            this.StartDate = moment(firstDay);
            this.EndDate = moment(lastDay);

            this.date = this.pipe.transform(firstDay, 'MMM dd') + '-' + this.pipe.transform(lastDay, 'MMM dd');
        }
        if(e.target.innerHTML == "Last Month"){
            var date = new Date();
            var firstDay = new Date(date.getFullYear(), date.getMonth()-1, 1);
            var lastDay = new Date(date.getFullYear(), date.getMonth(), 0);

            this.StartDate = moment(firstDay);
            this.EndDate = moment(lastDay);

            this.date = this.pipe.transform(firstDay, 'MMM dd') + '-' + this.pipe.transform(lastDay, 'MMM dd');
        }
        if(e.target.innerHTML == "Custom Range"){
            this.shouldShowCustom =true;
            this.date = "";
        }
        else{
             this.shouldShowCustom =false;
            this.getSearchedData();
        }
            
    }
    bindusers():void{
        debugger;
        
        this._dashboardSalesServiceProxy.getUsers(this.organizationUnit).subscribe(result => {
            this.users = result;
            this.usersLength = result.length;
            if(result.length > 1){
             this.userId = 0;
            }
            else{
            this.userId = result[0].id;
             }
             this.getData();
            

         });
    }

    getData() : void{
        this.getCallHistory();
        this.getInvoiceOverview();
        this.getCashInvoiceFinanceInvoice();
        this.shouldShow = false;

    }
    getSearchedData() : void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Search By Filter';
        log.section = 'Dashboard Invoice';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        this.getData();

        });
    }

    getCallHistory() :void{
        this._dashboardSalesServiceProxy.getCallHistory(this.userId,this.StartDate,this.EndDate,this.organizationUnit).subscribe(result=>{
            this.Inbound = result.inbound;
            this.Outbound = result.outbound;
            this.NotAnswerd = result.notAnswerd;
            this.Missed = result.missed;
        })
    }


    getInvoiceOverview() : void{
        this._dashoardInvoiceServiceProxy.getInvoiceOverview(this.userId,this.StartDate,this.EndDate,this.organizationUnit).subscribe(result=>{
            this.invoiceOverview = result;
        })
    }

    getCashInvoiceFinanceInvoice() : void{
        this._dashoardInvoiceServiceProxy.getCashInvoice_financeInvoice_StateWiseOwing(this.userId,this.StartDate,this.EndDate,this.organizationUnit).subscribe(result=>{
            this.cash_finance_invoice_stateWiseOwing = result;
        })
    }

}
