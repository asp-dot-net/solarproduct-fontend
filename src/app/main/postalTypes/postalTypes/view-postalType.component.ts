﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { PostalTypesServiceProxy, GetPostalTypeForViewDto, PostalTypeDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
    selector: 'viewPostalType',
    templateUrl: './view-postalType.component.html',
    animations: [appModuleAnimation()]
})
export class ViewPostalTypeComponent extends AppComponentBase implements OnInit {

    active = false;
    saving = false;

    item: GetPostalTypeForViewDto;


    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
         private _postalTypesServiceProxy: PostalTypesServiceProxy
    ) {
        super(injector);
        this.item = new GetPostalTypeForViewDto();
        this.item.postalType = new PostalTypeDto();        
    }

    ngOnInit(): void {
        this.show(this._activatedRoute.snapshot.queryParams['id']);
    }

    show(postalTypeId: number): void {
      this._postalTypesServiceProxy.getPostalTypeForView(postalTypeId).subscribe(result => {      
                 this.item = result;
                this.active = true;
            });       
    }
}
