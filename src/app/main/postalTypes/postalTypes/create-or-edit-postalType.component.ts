﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { PostalTypesServiceProxy, CreateOrEditPostalTypeDto ,UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { ActivatedRoute } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
    selector: 'createOrEditPostalType',
    templateUrl: './create-or-edit-postalType.component.html',
    animations: [appModuleAnimation()]
})
export class CreateOrEditPostalTypeComponent extends AppComponentBase implements OnInit {
    active = false;
    saving = false;
    
    postalType: CreateOrEditPostalTypeDto = new CreateOrEditPostalTypeDto();

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,        
        private _postalTypesServiceProxy: PostalTypesServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.show(this._activatedRoute.snapshot.queryParams['id']);
    }

    show(postalTypeId?: number): void {

        if (!postalTypeId) {
            this.postalType = new CreateOrEditPostalTypeDto();
            this.postalType.id = postalTypeId;

            this.active = true;
        } else {
            this._postalTypesServiceProxy.getPostalTypeForEdit(postalTypeId).subscribe(result => {
                this.postalType = result.postalType;


                this.active = true;
            });
        }
        
    }

    save(): void {
            this.saving = true;

			
            this._postalTypesServiceProxy.createOrEdit(this.postalType)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
             });
    }







}
