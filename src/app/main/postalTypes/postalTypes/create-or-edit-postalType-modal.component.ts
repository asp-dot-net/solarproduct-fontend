﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { PostalTypesServiceProxy, CreateOrEditPostalTypeDto,UserActivityLogServiceProxy, UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'createOrEditPostalTypeModal',
    templateUrl: './create-or-edit-postalType-modal.component.html'
})
export class CreateOrEditPostalTypeModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    postalType: CreateOrEditPostalTypeDto = new CreateOrEditPostalTypeDto();

    constructor(
        injector: Injector,
        private _postalTypeServiceProxy: PostalTypesServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
    }
    onShown(): void {
        document.getElementById('PostalType_Name').focus();
    }
    show(postalTypeId?: number): void {

        if (!postalTypeId) {
            this.postalType = new CreateOrEditPostalTypeDto();
            this.postalType.id = postalTypeId;
            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Postal Types';
            log.section = 'Postal Types';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });

        } else {
            this._postalTypeServiceProxy.getPostalTypeForEdit(postalTypeId).subscribe(result => {
                this.postalType = result.postalType;
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Postal Types : ' + this.postalType.name;
                log.section = 'Postal Types';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        
    }

    save(): void {
            this.saving = true;
            this._postalTypeServiceProxy.createOrEdit(this.postalType)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.postalType.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Postal Types Updated : '+ this.postalType.name;
                    log.section = 'Postal Types';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Postal Types Created : '+ this.postalType.name;
                    log.section = 'Postal Types';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
