﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetPostalTypeForViewDto, PostalTypeDto ,UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
@Component({
    selector: 'viewPostalTypeModal',
    templateUrl: './view-postalType-modal.component.html'
})
export class ViewPostalTypeModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetPostalTypeForViewDto;


    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
        this.item = new GetPostalTypeForViewDto();
        this.item.postalType = new PostalTypeDto();
    }

    show(item: GetPostalTypeForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
        
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Opened Postal Types View : ' + this.item.postalType.name;
        log.section = 'Postal Types';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {            
         }); 
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
