import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { LocationServiceProxy, CreateOrEditLocationDto, CreateOrEditLeadDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import PlaceResult = google.maps.places.PlaceResult;
import { Location } from '@angular-material-extensions/google-maps-autocomplete';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'createOrEditWarehouseLocation',
    templateUrl: './create-or-edit-warehouselocation-modal.component.html'
})
export class CreateOrEditLocationModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    location: CreateOrEditLocationDto = new CreateOrEditLocationDto();

    constructor(
        injector: Injector,
        private _warehouselocationServiceProxy: LocationServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }

    show(locationId?: number): void {
        if (!locationId) {
            this.location = new CreateOrEditLocationDto();
            this.location.id = locationId;
            this.active = true;
            this.modal.show();
            // setTimeout  (() => {
            //     this.modal.show();
            // },2000);
            
let log = new UserActivityLogDto();
log.actionId = 79;
log.actionNote ='Open For Create New Warehouse Location';
log.section = 'Warehouse Location';
this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
    .subscribe(() => {
});
        } else {
            this._warehouselocationServiceProxy.getLocationForEdit(locationId).subscribe(result => {
                this.location = result.location;
                this.active = true;
                this.modal.show();
            });
        }
        // this.active = true;
        // this.modal.show();
        let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Warehouse Location : ' + this.location.address;
                log.section = 'Warehouse Location';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
    }

    onShown(): void {
       document.getElementById('Location_Address').focus();
    }
   
    save(): void {
        this.saving = true;
        this._warehouselocationServiceProxy.createOrEdit(this.location).pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.location.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Warehouse Location Updated : '+ this.location.address;
                    log.section = 'Warehouse Location';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Warehouse Location Created : '+ this.location.address;
                    log.section = 'Warehouse Location';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }

            },
            error => {
                this.saving = false;
            }
        );
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    onLocationSelected(location: Location) {
        debugger;
        this.location.latitude = location.latitude.toString();
        this.location.longitude = location.longitude.toString();
    }

    onAutocompleteSelected(result: PlaceResult) {
        debugger;
        if (result.address_components.length == 7) {
            this.location.unitNo = "";
            this.location.unitType = "";
            this.location.streetNo = "";
            this.location.streetName = "";
            this.location.streetType = "";
            this.location.postCode = "";
            this.location.address = "";
            this.location.suburb = "";
            this.location.streetNo = result.address_components[0].long_name.toUpperCase();
            this.location.suburb=result.address_components[2].long_name.toUpperCase();
            var str = result.address_components[1].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.location.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.location.streetType = splitted[3].toUpperCase().trim();
               
            }
            else if (splitted.length == 3) {
                this.location.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.location.streetType = splitted[2].toUpperCase().trim();
              
            }
            else {
                this.location.streetName = splitted[0].toUpperCase().trim();
                this.location.streetType = splitted[1].toUpperCase().trim();
                
            }
            
            this.location.postCode = result.address_components[6].long_name.toUpperCase();
            this.location.address = this.location.unitNo + " " + this.location.unitType + " " + this.location.streetNo + " " + this.location.streetName + " " + this.location.streetType;  //result.formatted_address;
        }
        else if (result.address_components.length == 6) {
            this.location.unitNo = "";
            this.location.unitType = "";
            this.location.streetNo = "";
            this.location.streetName = "";
            this.location.streetType = "";
            this.location.postCode = "";
            this.location.address = "";
            this.location.suburb = "";

            this.location.streetNo = result.address_components[0].long_name.toUpperCase();
            this.location.suburb=result.address_components[2].long_name.toUpperCase();
            var str = result.address_components[1].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.location.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.location.streetType = splitted[3].toUpperCase().trim();
                
            }
            else if (splitted.length == 3) {
                this.location.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.location.streetType = splitted[2].toUpperCase().trim();
               
            }
            else {
                this.location.streetName = splitted[0].toUpperCase().trim();
                this.location.streetType = splitted[1].toUpperCase().trim();
                
            }
            
            this.location.postCode = result.address_components[5].long_name.toUpperCase();
            this.location.address = this.location.unitNo + " " + this.location.unitType + " " + this.location.streetNo + " " + this.location.streetName + " " + this.location.streetType;  //result.formatted_address;
        }
        else {
            this.location.unitNo = "";
            this.location.unitType = "";
            this.location.streetNo = "";
            this.location.streetName = "";
            this.location.streetType = "";
            this.location.state = "";
            this.location.postCode = "";
            this.location.address = "";
            this.location.suburb = "";

            var str1 = result.address_components[0].long_name.toUpperCase();
            var splitted1 = str1.split(" ");
            if (splitted1.length == 1) {
                this.location.unitNo = splitted1[0].toUpperCase().trim();
            }
            else {
                this.location.unitNo = splitted1[1].toUpperCase().trim();
                this.location.unitType = splitted1[0].toUpperCase().trim();
            }
            this.location.streetNo = result.address_components[1].long_name.toUpperCase();
            this.location.suburb=result.address_components[2].long_name.toUpperCase();
            var str = result.address_components[2].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.location.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.location.streetType = splitted[3].toUpperCase().trim();
                
            }
            else if (splitted.length == 3) {
                this.location.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.location.streetType = splitted[2].toUpperCase().trim();
                
            }
            else {
                this.location.streetName = splitted[0].toUpperCase().trim();
                this.location.streetType = splitted[1].toUpperCase().trim();
                
            }
            
            this.location.postCode = result.address_components[7].long_name.toUpperCase();
            this.location.address = this.location.unitNo + " " + this.location.unitType + " " + this.location.streetNo + " " + this.location.streetName + " " + this.location.streetType;  //result.formatted_address;
        }
    }

}
