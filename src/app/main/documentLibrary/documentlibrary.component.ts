import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import {  DocumentLibraryServiceProxy, DocumentLibraryDto ,UserActivityLogDto,UserActivityLogServiceProxy} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import * as _ from 'lodash';
import { CreateOrEditDocumentLibraryModalComponent } from './create-or-edit-documentlibrary-modal.component';
import { ViewDocumentLibraryModalComponent } from './view-documentlibrary-model.component';
import { AppConsts } from '@shared/AppConsts';
import { Title } from '@angular/platform-browser';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './documentlibrary.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class DocumentLibrarysComponent extends AppComponentBase {

    @ViewChild('createOrEditDocumentLibraryModal', { static: true }) createOrEditDocumentLibraryModal: CreateOrEditDocumentLibraryModalComponent;
    @ViewChild('viewDocumentLibraryModal', { static: true }) viewDocumentLibraryModal: ViewDocumentLibraryModalComponent;

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    firstrowcount = 0;
    last = 0;
    public screenHeight: any;  
    testHeight = 250;
    constructor(
        injector: Injector,
        private _documentlibraryServiceProxy: DocumentLibraryServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private titleService: Title
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Document Library");
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Document Library';
        log.section = 'Document Library';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }
    getDocumentLibrary(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._documentlibraryServiceProxy.getAll(
            this.filterText,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    downfile(filename): void {

        let FileName = AppConsts.docUrl + "/" + filename;
        window.open(FileName, "_blank");
    };
    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    deleteDocumentlibrary(documentlibrary: DocumentLibraryDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._documentlibraryServiceProxy.delete(documentlibrary.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete Document Library: ' + documentlibrary.title;
                            log.section = 'Document Library';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            }); 
                        });
                }
            }
        );
    }
    searchLog() : void {
        debugger;
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Document Library';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    createDocumentType(): void {
        this.createOrEditDocumentLibraryModal.show();
    }

}
