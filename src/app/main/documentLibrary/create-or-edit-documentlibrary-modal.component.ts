import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {  DocumentLibraryServiceProxy, CreateorEditDocumentLibraryDto,UserActivityLogDto,UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AppConsts } from '@shared/AppConsts';

@Component({
    selector: 'createOrEditDocumentLibraryModal',
    templateUrl: './create-or-edit-documentlibrary-modal.component.html'
})
export class CreateOrEditDocumentLibraryModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    documentlibrary: CreateorEditDocumentLibraryDto = new CreateorEditDocumentLibraryDto();
    tenantId: number = abp.session.tenantId;
    uploadedFiles: any[] = [];
    uploadUrl: string;

    constructor(
        injector: Injector,
        private _documentliraryServiceProxy: DocumentLibraryServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
    }
    show(documenttypeId?: number): void {
        
        if (!documenttypeId) {
            this.documentlibrary = new CreateorEditDocumentLibraryDto();
            this.documentlibrary.id = 0;
            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Document Library';
            log.section = 'Document Library';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
        } else {
            this._documentliraryServiceProxy.getDocumentLibraryForEdit(documenttypeId).subscribe(result => {
                this.documentlibrary = result.documentlibraryDto;
                this.changeurl();
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Document Library : ' + this.documentlibrary.title;
                log.section = 'Document Library';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            });
        }
    }

    changeurl() {
        
        if (this.documentlibrary.id == 0 && this.documentlibrary.id == null) {
            this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/DemoUiComponents/UploadDocumentLibrary?id=' + 0 + '&title=' + this.documentlibrary.title + '&tenantId=' + this.tenantId;
        } else {
            this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/DemoUiComponents/UploadDocumentLibrary?id=' + this.documentlibrary.id + '&title=' + this.documentlibrary.title + '&tenantId=' + this.tenantId;
        }
    }

    onShown(): void {
        
        document.getElementById('Department_Name').focus();
    }

    // upload completed event
    onUpload(event): void {
        
        for (const file of event.files) {
            this.uploadedFiles.push(file);
            this.modal.hide();
            this.uploadedFiles = [];
            this.modalSave.emit(null);

        }
    }

    onBeforeSend(event): void {
        event.xhr.setRequestHeader('Authorization', 'Bearer ' + abp.auth.getToken());
    }

    downfile(): void {
        
        let FileName = AppConsts.docUrl + "/" + this.documentlibrary.filePath;
        window.open(FileName, "_blank");
    };

    save(): void {
        this.saving = true;

        this.documentlibrary.fileName = this.uploadedFiles[0].fileName;
        this.documentlibrary.fileType = '1';
        this._documentliraryServiceProxy.createOrEdit(this.documentlibrary)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.documentlibrary.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Document Library Updated : '+ this.documentlibrary.title;
                    log.section = 'Document Library';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Document Library Created : '+ this.documentlibrary.title;
                    log.section = 'Document Library';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
            });
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
