import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { DocumentLibraryDto, GetDocumentLibraryForViewDto, UserActivityLogServiceProxy,UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AppConsts } from '@shared/AppConsts';
import { finalize } from 'rxjs/operators';
@Component({
    selector: 'viewDocumentLibraryModal',
    templateUrl: './view-documentlibrary-model.component.html'
})
export class ViewDocumentLibraryModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetDocumentLibraryForViewDto;


    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
        this.item = new GetDocumentLibraryForViewDto();
        this.item.documentlibrary = new DocumentLibraryDto();
    }

    show(item: GetDocumentLibraryForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Opened Document Library View : ' + item.documentlibrary.title;
        log.section = 'Document Library';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {            
         }); 
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
    
    downfile(): void {
        
        let FileName = AppConsts.docUrl + "/" + this.item.documentlibrary.filePath;
        window.open(FileName, "_blank");
    };
}
