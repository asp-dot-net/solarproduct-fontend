﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { HoldReasonsServiceProxy, CreateOrEditHoldReasonDto, CreateOrEditServiceCategoryDto, ServiceCategorysServiceProxy, CreateOrEditServiceStatusDto, ServiceStatusesServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'createOrEditserviceStatusModal',
    templateUrl: './create-or-edit-serviceStatus-modal.component.html'
})
export class CreateOrEditServiceStatusModalComponent extends AppComponentBase {
   
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    serviceStatuses: CreateOrEditServiceStatusDto = new CreateOrEditServiceStatusDto();



    constructor(
        injector: Injector,
        private _serviceStatusesServiceProxy: ServiceStatusesServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }
    
    show(serviceStatusesId?: number): void {
    

        if (!serviceStatusesId) {
            this.serviceStatuses = new CreateOrEditServiceStatusDto();
            this.serviceStatuses.id = serviceStatusesId;

            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Service Status';
            log.section = 'Service Status';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });

        } else {
            this._serviceStatusesServiceProxy.getServiceStatusForEdit(serviceStatusesId).subscribe(result => {
                this.serviceStatuses = result.serviceStatuses;

                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Service Status : ' + this.serviceStatuses.name;
                log.section = 'Service Status';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        
    }
    onShown(): void {
        
        document.getElementById('ServiceStatus_ServiceStatus').focus();
    }
    save(): void {
            this.saving = true;
            this._serviceStatusesServiceProxy.createOrEdit(this.serviceStatuses)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.serviceStatuses.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Service Status Updated : '+ this.serviceStatuses.name;
                    log.section = 'Service Status';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Service Status Created : '+ this.serviceStatuses.name;
                    log.section = 'Service Status';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
