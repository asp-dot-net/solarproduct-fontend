"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.FinanceTrackerComponent = void 0;
var core_1 = require("@angular/core");
var app_component_base_1 = require("@shared/common/app-component-base");
var routerTransition_1 = require("@shared/animations/routerTransition");
var moment = require("moment");
var FinanceTrackerComponent = /** @class */ (function (_super) {
    __extends(FinanceTrackerComponent, _super);
    function FinanceTrackerComponent(injector, _jobsServiceProxy, _leadsServiceProxy, _userServiceProxy, _fileDownloadService) {
        var _this = _super.call(this, injector) || this;
        _this._jobsServiceProxy = _jobsServiceProxy;
        _this._leadsServiceProxy = _leadsServiceProxy;
        _this._userServiceProxy = _userServiceProxy;
        _this._fileDownloadService = _fileDownloadService;
        _this.show = true;
        _this.showchild = true;
        _this.shouldShow = false;
        _this.organizationUnitlength = 0;
        _this.advancedFiltersAreShown = false;
        _this.filterText = '';
        _this.regPlanNoFilter = '';
        _this.lotNumberFilter = '';
        _this.peakMeterNoFilter = '';
        _this.offPeakMeterFilter = '';
        _this.enoughMeterSpaceFilter = -1;
        _this.isSystemOffPeakFilter = -1;
        _this.suburbFilter = '';
        _this.stateFilter = '';
        _this.jobTypeNameFilter = '';
        _this.jobStatusNameFilter = '';
        _this.roofTypeNameFilter = '';
        _this.roofAngleNameFilter = '';
        _this.elecDistributorNameFilter = '';
        _this.leadCompanyNameFilter = '';
        _this.elecRetailerNameFilter = '';
        _this.paymentOptionNameFilter = '';
        _this.depositOptionNameFilter = '';
        _this.meterUpgradeNameFilter = '';
        _this.meterPhaseNameFilter = '';
        _this.promotionOfferNameFilter = '';
        _this.houseTypeNameFilter = '';
        _this.financeOptionNameFilter = '';
        _this.OpenRecordId = 0;
        _this.organizationUnit = 0;
        _this._entityTypeFullName = 'TheSolarProduct.Jobs.Job';
        _this.entityHistoryEnabled = false;
        _this.projectFilter = '';
        _this.stateNameFilter = '';
        _this.mobile = '';
        _this.email = '';
        _this.sampleDateRange = [moment().add(-7, 'days').endOf('day'), moment().add(0, 'days').startOf('day')];
        _this.elecDistributorId = 0;
        _this.applicationstatus = 0;
        _this.jobTypeId = 0;
        _this.isVerifiedData = undefined;
        _this.totalfinanceData = 0;
        _this.totalfinanceVerifyData = 0;
        _this.totalfinanceDatanotVerifyData = 0;
        _this.ExpandedView = true;
        _this.SelectedLeadId = 0;
        _this.totalfinanceQueryData = 0;
        return _this;
    }
    FinanceTrackerComponent.prototype.toggleBlock = function () {
        this.show = !this.show;
    };
    ;
    FinanceTrackerComponent.prototype.toggleBlockChild = function () {
        this.showchild = !this.showchild;
    };
    ;
    FinanceTrackerComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._commonLookupService.getAllStateForTableDropdown().subscribe(function (result) {
            _this.allStates = result;
        });
        this._commonLookupService.getOrganizationUnit().subscribe(function (output) {
            _this.allOrganizationUnits = output;
            _this.organizationUnit = _this.allOrganizationUnits[0].id;
            _this.organizationUnitlength = _this.allOrganizationUnits.length;
            _this.getJobs();
            _this.getCount(_this.organizationUnit);
        });
    };
    FinanceTrackerComponent.prototype.getCount = function (organizationUnit) {
        var _this = this;
        this._jobsServiceProxy.getAllApplicationTrackerCount(organizationUnit).subscribe(function (result) {
            _this.totalfinanceData = result.totalfinanceData;
            _this.totalfinanceVerifyData = result.totalfinanceVerifyData;
            _this.totalfinanceDatanotVerifyData = result.totalfinanceDatanotVerifyData;
            _this.totalfinanceQueryData = result.totalfinanceQueryData;
        });
    };
    FinanceTrackerComponent.prototype.clear = function () {
        this.filterText = '';
        this.leadCompanyNameFilter = '';
        this.stateNameFilter = '';
        this.mobile = '';
        this.email = '';
        this.getJobs();
    };
    FinanceTrackerComponent.prototype.getJobs = function (event) {
        var _this = this;
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.startDate = this.sampleDateRange[0];
        this.endDate = this.sampleDateRange[1];
        this.primengTableHelper.showLoadingIndicator();
        this._jobsServiceProxy.getAllForJobFinanceTracker(this.filterText, this.suburbFilter, this.stateFilter, this.jobTypeNameFilter, this.jobStatusNameFilter, this.roofTypeNameFilter, this.roofAngleNameFilter, this.elecDistributorNameFilter, this.leadCompanyNameFilter, this.elecRetailerNameFilter, this.paymentOptionNameFilter, this.depositOptionNameFilter, this.meterUpgradeNameFilter, this.meterPhaseNameFilter, this.promotionOfferNameFilter, this.houseTypeNameFilter, this.financeOptionNameFilter, this.organizationUnit, this.projectFilter, this.stateNameFilter, this.elecDistributorId, this.jobStatusIDFilter, this.applicationstatus, this.jobTypeId, this.startDate, this.endDate, this.mobile, this.email, this.isVerifiedData == undefined ? undefined : this.isVerifiedData, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, 0, undefined, undefined, undefined, this.primengTableHelper.getSorting(this.dataTable), this.primengTableHelper.getSkipCount(this.paginator, event), this.primengTableHelper.getMaxResultCount(this.paginator, event)).subscribe(function (result) {
            _this.primengTableHelper.totalRecordsCount = result.totalCount;
            _this.primengTableHelper.records = result.items;
            _this.primengTableHelper.hideLoadingIndicator();
            _this.shouldShow = false;
        });
        this.getCount(this.organizationUnit);
    };
    FinanceTrackerComponent.prototype.reloadPage = function ($event) {
        this.ExpandedView = true;
        this.paginator.changePage(this.paginator.getPage());
    };
    FinanceTrackerComponent.prototype.addDetails = function (jobid) {
        this.financeDetailModal.show(jobid);
    };
    FinanceTrackerComponent.prototype.exportToExcel = function () {
        var _this = this;
        this._jobsServiceProxy.getFinanaceTrackerToExcel(this.filterText, this.suburbFilter, this.stateFilter, this.jobTypeNameFilter, this.jobStatusNameFilter, this.roofTypeNameFilter, this.roofAngleNameFilter, this.elecDistributorNameFilter, this.leadCompanyNameFilter, this.elecRetailerNameFilter, this.paymentOptionNameFilter, this.depositOptionNameFilter, this.meterUpgradeNameFilter, this.meterPhaseNameFilter, this.promotionOfferNameFilter, this.houseTypeNameFilter, this.financeOptionNameFilter, this.organizationUnit, this.projectFilter, this.stateNameFilter, this.elecDistributorId, this.jobStatusIDFilter, this.applicationstatus, this.jobTypeId, this.StartDate, this.EndDate, this.mobile, this.email, this.isVerifiedData == undefined ? undefined : this.isVerifiedData, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined)
            .subscribe(function (result) {
            _this._fileDownloadService.downloadTempFile(result);
        });
    };
    FinanceTrackerComponent.prototype.quickview = function (jobid, trackerid) {
        this.viewApplicationModal.show(jobid, trackerid);
    };
    FinanceTrackerComponent.prototype.expandGrid = function () {
        this.ExpandedView = true;
    };
    FinanceTrackerComponent.prototype.navigateToLeadDetail = function (leadid) {
        this.ExpandedView = !this.ExpandedView;
        this.ExpandedView = false;
        this.SelectedLeadId = leadid;
        this.viewLeadDetail.showDetail(leadid);
    };
    __decorate([
        core_1.ViewChild('dataTable', { static: true })
    ], FinanceTrackerComponent.prototype, "dataTable");
    __decorate([
        core_1.ViewChild('paginator', { static: true })
    ], FinanceTrackerComponent.prototype, "paginator");
    __decorate([
        core_1.ViewChild('financeDetailModal', { static: true })
    ], FinanceTrackerComponent.prototype, "financeDetailModal");
    __decorate([
        core_1.ViewChild('addActivityModal', { static: true })
    ], FinanceTrackerComponent.prototype, "addActivityModal");
    __decorate([
        core_1.ViewChild('viewApplicationModal', { static: true })
    ], FinanceTrackerComponent.prototype, "viewApplicationModal");
    __decorate([
        core_1.ViewChild('financeTrackerSmsEmailModal', { static: true })
    ], FinanceTrackerComponent.prototype, "financeTrackerSmsEmailModal");
    __decorate([
        core_1.ViewChild('viewLeadDetail', { static: true })
    ], FinanceTrackerComponent.prototype, "viewLeadDetail");
    FinanceTrackerComponent = __decorate([
        core_1.Component({
            templateUrl: './financeTracker.component.html',
            encapsulation: core_1.ViewEncapsulation.None,
            animations: [routerTransition_1.appModuleAnimation()]
        })
    ], FinanceTrackerComponent);
    return FinanceTrackerComponent;
}(app_component_base_1.AppComponentBase));
exports.FinanceTrackerComponent = FinanceTrackerComponent;
