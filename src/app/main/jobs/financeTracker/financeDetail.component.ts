import { Component, ElementRef, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { JobsServiceProxy, GetJobForEditOutput, CreateOrEditJobDto, CommonLookupDto, JobFinanceOptionLookupTableDto, JobPaymentOptionLookupTableDto, JobDepositOptionLookupTableDto, CommonLookupServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import * as _ from 'lodash';
import * as moment from 'moment';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
    selector: 'financeDetailModal',
    templateUrl: './financeDetail.component.html'
})
export class JobFinanceDetailModalComponent extends AppComponentBase {

    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    saving = false;
    distAppliedDate: moment.Moment;
    MeterNumber = "";
    ApprovalRefNo = "";
    item: GetJobForEditOutput;
    comment = "";
    financeApplicationDate: moment.Moment;
    financeDocSentDate: moment.Moment;
    financeDocReceivedDate: moment.Moment;
    documentsVeridiedDate: moment.Moment;
    coolingoffPeriodEnd: moment.Moment;
    financeApplicationDates: moment.Moment;
    // financeOptionId = 0;
    // depositOptionId = 0;
    // paymentOptionId = 0;
    userList: CommonLookupDto[];
    jobFinOptions: JobFinanceOptionLookupTableDto[];
    leadCompanyName: any;
    jobPaymentOptions: JobPaymentOptionLookupTableDto[];
    jobDepositOption: JobDepositOptionLookupTableDto[];
    constructor(
        injector: Injector,
        private _jobsServiceProxy: JobsServiceProxy,
        private _router: Router,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
    ) {
        super(injector);
        this.item = new GetJobForEditOutput();
        this.item.job = new CreateOrEditJobDto();
    }

    sectionName = '';
    show(jobid: number,section = ''): void {
        this.sectionName = section;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Open Finance Notes' ;
        log.section = section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
        if (this.jobFinOptions == null) {
            this._commonLookupService.getAllFinanceOptionForTableDropdown().subscribe(result => {
                this.jobFinOptions = result;
            });
        }

        if (this.jobPaymentOptions == null) {
            this._commonLookupService.getAllPaymentOptionForTableDropdown().subscribe(result => {
                this.jobPaymentOptions = result;
            });
        }

        if (this.jobDepositOption == null) {
            this._jobsServiceProxy.getAllDepositOptionForTableDropdown().subscribe(result => {
                this.jobDepositOption = result;
            });
        }

        if (this.userList == null) {
            this._jobsServiceProxy.getAllUsers().subscribe(result => {
                this.userList = result;
            });
        }

        this._jobsServiceProxy.getJobForEdit(jobid).subscribe(result => {
            this.item = result;
            // this.financeOptionId = this.item.job.financeOptionId + "";
            // this.depositOptionId = this.item.job.depositOptionId;
            // this.paymentOptionId = this.item.job.paymentOptionId;

            this.item.job.documentsVeridiedBy = this.appSession.userId;

            this.financeApplicationDate = result.job.financeApplicationDate;
            this.financeDocSentDate = result.job.financeDocSentDate;
            this.financeDocReceivedDate = result.job.financeDocReceivedDate;
            this.coolingoffPeriodEnd = result.job.coolingoffPeriodEnd;
            this.documentsVeridiedDate = result.job.documentsVeridiedDate;
            this.leadCompanyName = result.leadCompanyName;
            this.modal.show();
        });
    }

    save(): void {
        
        this.saving = true;
        this.item.job.financeApplicationDate = this.financeApplicationDate;
        this.item.job.financeDocSentDate = this.financeDocSentDate;
        this.item.job.financeDocReceivedDate = this.financeDocReceivedDate;
        this.item.job.coolingoffPeriodEnd = this.coolingoffPeriodEnd;
        this.item.job.documentsVeridiedDate = this.documentsVeridiedDate;
        this.item.job.sectionId=3;
        this.item.job.section = this.sectionName;
        // this.item.job.financeOptionId = this.financeOptionId;
        // this.item.job.depositOptionId = this.depositOptionId;
        // this.item.job.paymentOptionId = this.paymentOptionId;
        this._jobsServiceProxy.createOrEdit(this.item.job)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.close();
                this.modalSave.emit(null);
                this.notify.info(this.l('SavedSuccessfully'));
                this.comment = "";
            });
    }

    close(): void {
        this.modal.hide();
    }

    onchange(event) {
        
        let distdate = moment(event);
        // this.coolingoffPeriodEnd = moment(distdate).add(10, 'days');
        this.coolingoffPeriodEnd = distdate.add(10, 'days');
        //this.job.coolingoffPeriodEnd = moment(distdate).add(10, 'days');
    }
}