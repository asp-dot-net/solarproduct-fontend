import { Component, Injector, ViewEncapsulation, ViewChild, Input, HostListener } from '@angular/core';
import { CommonLookupServiceProxy, GetJobForViewDto, JobPaymentOptionLookupTableDto, JobsServiceProxy, JobTrackerServiceProxy, LeadsServiceProxy, LeadStateLookupTableDto, OrganizationUnitDto, UserActivityLogDto, UserActivityLogServiceProxy, UserServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { EntityTypeHistoryModalComponent } from '@app/shared/common/entityHistory/entity-type-history-modal.component';
import * as _ from 'lodash';
import { OnInit } from '@angular/core';
import { JobFinanceDetailModalComponent } from './financeDetail.component';
import * as moment from 'moment';
// import { AddActivityModalComponent } from '@app/main/myleads/myleads/add-activity-model.component';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { ViewApplicationModelComponent } from '../jobs/view-application-model/view-application-model.component';
// import { FinanceSmsEmailModelComponent } from './finance-sms-email-model/finance-sms-email-model.component';
// import { ViewMyLeadComponent } from '@app/main/myleads/myleads/view-mylead.component';
import { Title } from '@angular/platform-browser';
import { CommentModelComponent } from '@app/main/activitylog/comment-modal.component';
import { EmailModelComponent } from '@app/main/activitylog/email-modal.component';
import { NotifyModelComponent } from '@app/main/activitylog/notify-modal.component';
import { ReminderModalComponent } from '@app/main/activitylog/reminder-modal.component';
import { SMSModelComponent } from '@app/main/activitylog/sms-modal.component';
import { ToDoModalComponent } from '@app/main/activitylog/todo-modal.component';
import { state } from '@angular/animations';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './financeTracker.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class FinanceTrackerComponent extends AppComponentBase implements OnInit {
    FiltersData = false;
    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    organizationUnitlength: number=0;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('financeDetailModal', { static: true }) financeDetailModal: JobFinanceDetailModalComponent;
    // @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    @ViewChild('viewApplicationModal', { static: true }) viewApplicationModal: ViewApplicationModelComponent;
    // @ViewChild('financeTrackerSmsEmailModal', { static: true }) financeTrackerSmsEmailModal: FinanceSmsEmailModelComponent;
    // @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
    @ViewChild('smsModel', { static: true }) smsModel: SMSModelComponent;
    @ViewChild('emailModel', { static: true }) emailModel: EmailModelComponent;
    @ViewChild('notifyModel', { static: true }) notifyModel: NotifyModelComponent;
    @ViewChild('commentModel', { static: true }) commentModel: CommentModelComponent;
    @ViewChild('todoModal', { static: true }) todoModal: ToDoModalComponent;
    @ViewChild('ReminderModal', { static: true }) ReminderModal: ReminderModalComponent;
    
    advancedFiltersAreShown = false;
    filterText = '';
    
    excelorcsvfile = 0;
    
    organizationUnit = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    
    stateFilter = '';
    paymentid = 0;
    isVerifiedData = 2;
    allStates: LeadStateLookupTableDto[];
    
    total = 0;
    totalVerify = 0;
    totalNotVerify = 0;
    totalQuery = 0;
    totalAwaiting = 0;
    ExpandedView: boolean = true;
    SelectedLeadId: number = 0;
    
    jobPaymentOptions: JobPaymentOptionLookupTableDto[];
    firstrowcount = 0;
    last = 0;

    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 330;
    toggle: boolean = true;
    filterName = "JobNumber";
    orgCode = '';
    change() {
        this.toggle = !this.toggle;
      }
    
    constructor(
        injector: Injector,
        private _jobsServiceProxy: JobsServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _userServiceProxy: UserServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private titleService: Title,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _jobTrackerService: JobTrackerServiceProxy
        ) {
            super(injector);
            this.titleService.setTitle(this.appSession.tenancyName + " |  Finance Tracker");
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
        this._commonLookupService.getAllPaymentOptionForTableDropdown().subscribe(result => {
            this.jobPaymentOptions = result;
            this.jobPaymentOptions = this.jobPaymentOptions.filter(item => item.id !== 1);
            
        });
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Finance Tracker';
            log.section = 'Finance Tracker';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length; 
            this.orgCode = this.allOrganizationUnits[0].code;
            this.getJobs();
            //this.getCount(this.organizationUnit);
        });
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }
        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }
    
    clear() {
        this.filterText = '';
        this.stateFilter = '';
        this.getJobs();
    }
    changeOrgCode() {
        this.orgCode = this.allOrganizationUnits.find(x => x.id == this.organizationUnit).code;
    }
    getJobs(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        // this._jobsServiceProxy.getAllForJobFinanceTracker(
        //     this.filterText,
        //     this.suburbFilter,
        //     this.stateFilter,
        //     this.jobTypeNameFilter,
        //     this.jobStatusNameFilter,
        //     this.roofTypeNameFilter,
        //     this.roofAngleNameFilter,
        //     this.elecDistributorNameFilter,
        //     this.leadCompanyNameFilter,
        //     this.elecRetailerNameFilter,
        //     this.paymentOptionNameFilter,
        //     this.depositOptionNameFilter,
        //     this.meterUpgradeNameFilter,
        //     this.meterPhaseNameFilter,
        //     this.promotionOfferNameFilter,
        //     this.houseTypeNameFilter,
        //     this.financeOptionNameFilter,
        //     this.organizationUnit,
        //     this.projectFilter,
        //     this.stateNameFilter,
        //     this.elecDistributorId,
        //     this.jobStatusIDFilter,
        //     this.applicationstatus,
        //     this.jobTypeId,
        //     this.startDate,
        //     this.endDate,
        //     this.mobile,
        //     this.email,
        //     this.isVerifiedData == undefined ? undefined : this.isVerifiedData,
        //     undefined,
        //     undefined,
        //     undefined,
        //     undefined,
        //     undefined,
        //     undefined,
        //     undefined,
        //     undefined,
        //     undefined,
        //     undefined,
        //     undefined,0,undefined,
        //     undefined,undefined,undefined,
        //     undefined,
        //     this.postalcodefrom,
        //     this.postalcodeTo,
        //     this.paymentid,
        //     this.areaNameFilter,
        //     undefined,
        //     this.primengTableHelper.getSorting(this.dataTable),
        //     this.primengTableHelper.getSkipCount(this.paginator, event),
        //     this.primengTableHelper.getMaxResultCount(this.paginator, event)
        // ).subscribe(result => {
        //     this.primengTableHelper.totalRecordsCount = result.totalCount;
        //     this.primengTableHelper.records = result.items;
        //     const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
        //     this.firstrowcount =  totalrows + 1;
        //     this.last = totalrows + result.items.length;
        //     this.primengTableHelper.hideLoadingIndicator();
        //     this.shouldShow = false;
        // });
        //this.getCount(this.organizationUnit);

        this._jobTrackerService.getAllFinanceTracker(
            this.filterName,
            filterText_,
            this.organizationUnit,
            this.paymentid,
            this.isVerifiedData,
            this.stateFilter,
            0,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            this.shouldShow = false;
            
            if (result.totalCount > 0) 
            {
                this.total = result.items[0].summary.total;
                this.totalVerify = result.items[0].summary.verified;
                this.totalNotVerify = result.items[0].summary.notVerified;
                this.totalQuery = result.items[0].summary.query;
                this.totalAwaiting = result.items[0].summary.awaiting;
            }
            else
            {
                this.total = 0;
                this.totalVerify = 0;
                this.totalNotVerify = 0;
                this.totalQuery = 0;
                this.totalAwaiting = 0;

            }
        });
        

    }

    reloadPage($event): void {
        this.ExpandedView = true;
        this.paginator.changePage(this.paginator.getPage());
    }

    addDetails(jobid): void {
        this.financeDetailModal.show(jobid,'Finance Tracker');
    }

    exportToExcel(excelorcsv): void {
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this.excelorcsvfile = excelorcsv;
        this._jobsServiceProxy.getFinanaceTrackerToExcel(
            this.filterName,
            filterText_,
            this.organizationUnit,
            this.paymentid,
            this.isVerifiedData,
            this.stateFilter,
            excelorcsv,
            "",
            0,
            25
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    quickview(jobid, trackerid): void {
        
        this.viewApplicationModal.show(jobid, trackerid);
    }

    expandGrid() {
        this.ExpandedView = true;
    }

    navigateToLeadDetail(leadid): void {
        // this.ExpandedView = !this.ExpandedView;
        // this.ExpandedView = false;
        // this.SelectedLeadId = leadid;
        // this.viewLeadDetail.showDetail(leadid,null,3);
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Finance Tracker';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Finance Tracker';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }

}
