import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinanceSmsEmailModelComponent } from './finance-sms-email-model.component';

describe('FinanceSmsEmailModelComponent', () => {
  let component: FinanceSmsEmailModelComponent;
  let fixture: ComponentFixture<FinanceSmsEmailModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinanceSmsEmailModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinanceSmsEmailModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
