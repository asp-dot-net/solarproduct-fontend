﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetProductTypeForViewDto, ProductTypeDto ,UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
@Component({
    selector: 'viewProductTypeModal',
    templateUrl: './view-productType-modal.component.html'
})
export class ViewProductTypeModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetProductTypeForViewDto;
    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
        this.item = new GetProductTypeForViewDto();
        this.item.productType = new ProductTypeDto();
    }

    show(item: GetProductTypeForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Opened Product Type View : ' + this.item.productType.name;
        log.section = 'Product Type';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {            
         });
         
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
