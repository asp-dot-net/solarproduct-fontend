﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { ProductTypesServiceProxy, CreateOrEditProductTypeDto,UserActivityLogServiceProxy, UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { AppConsts } from '@shared/AppConsts';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { FileUploader, FileItem, FileUploaderOptions } from 'ng2-file-upload';

@Component({
    selector: 'createOrEditProductTypeModal',
    templateUrl: './create-or-edit-productType-modal.component.html'
})
export class CreateOrEditProductTypeModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    productType: CreateOrEditProductTypeDto = new CreateOrEditProductTypeDto();

    public uploader: FileUploader;
    public maxfileBytesUserFriendlyValue = 5;
    private _uploaderOptions: FileUploaderOptions = {};
    filetoken: '';
    selectedFile : any;

    constructor(
        injector: Injector,
        private _productTypesServiceProxy: ProductTypesServiceProxy,
        private _tokenService: TokenService,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,

    ) {
        super(injector);
    }

    show(productTypeId?: number): void {

        if (!productTypeId) {
            this.productType = new CreateOrEditProductTypeDto();
            this.productType.id = productTypeId;

            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Product Type';
            log.section = 'Product Type';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._productTypesServiceProxy.getProductTypeForEdit(productTypeId).subscribe(result => {
                this.productType = result.productType;
                this.active = true;
                this.modal.show();
                
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Product Type : ' + this.productType.name;
                log.section = 'Product Type';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        this.initFileUploader();
        
    }

    onShown(): void {
        
        document.getElementById('ProductType_Name').focus();
    }
    
    save(): void {
        debugger;
            this.saving = true;

            this.productType.img = this.filetoken ?this.filetoken : ""; 

            this.productType.imgPath = this.productType.imgPath ? this.productType.imgPath :"";

            if(!this.productType.img && this.productType.imgPath.trim() == ""){
                if( this.productType.img.trim() == ""){
                    this.notify.warn("Select Image");
                    this.saving = false;
                    return;
                }
            }
			
            this._productTypesServiceProxy.createOrEdit(this.productType)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);

                if(this.productType.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Product Type Updated : '+ this.productType.name;
                    log.section = 'Product Type';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Product Type Created : '+ this.productType.name;
                    log.section = 'Product Type';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
    initFileUploader(): void {
        this.uploader = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Profile/UploadFile' });
        this._uploaderOptions.autoUpload = false;
        this._uploaderOptions.authToken = 'Bearer ' + this._tokenService.getToken();
        this._uploaderOptions.removeAfterUpload = true;
        this.uploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

        this.uploader.onBuildItemForm = (fileItem: FileItem, form: any) => {
            form.append('FileType', fileItem.file.type);
            form.append('FileName', fileItem.file.name);
            form.append('FileToken', this.guid());
        };

        this.uploader.onSuccessItem = (item, response, status) => {
            const resp = <IAjaxResponse>JSON.parse(response);
            if (resp.success) {
                this.filetoken = resp.result.fileToken;
                //this.instfilename.push(resp.result.fileName);
            } else {
                this.message.error(resp.error.message);
            }
        };

        this.uploader.setOptions(this._uploaderOptions);
    }

    fileChangeEvent(event: any): void {
        debugger
        // if (event.target.files[0].size > 5242880) { //5MB
        //     this.message.warn(this.l('ProfilePicture_Warn_SizeLimit', this.maxfileBytesUserFriendlyValue));
        //     return;
        // }
        var url = URL.createObjectURL(event.target.files[0]);
        var img = new Image;

        // img.onload = () =>{
        //     var w = img.width;
        //     var h = img.height;
        //     //alert(w + "X"+ h )
        //     if(w != 290 || h != 330){
        //         this.notify.warn("Select Image That Contain Size 290 X 330");
        //         this.selectedFile = null;
        //    }
        // };
        img.src = url;
        this.uploader.clearQueue();
        this.uploader.addToQueue([<File>event.target.files[0]]);
        this.uploader.uploadAll();

    }

    guid(): string {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }

    
    downloadfile(file): void {
        let FileName = AppConsts.oldDocUrl + file;

        window.open(FileName, "_blank");

    }
}
