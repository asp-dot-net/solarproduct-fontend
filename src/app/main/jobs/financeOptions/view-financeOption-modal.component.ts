﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetFinanceOptionForViewDto, FinanceOptionDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewFinanceOptionModal',
    templateUrl: './view-financeOption-modal.component.html'
})
export class ViewFinanceOptionModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetFinanceOptionForViewDto;


    constructor(
        injector: Injector
    ) {
        super(injector);
        this.item = new GetFinanceOptionForViewDto();
        this.item.financeOption = new FinanceOptionDto();
    }

    show(item: GetFinanceOptionForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
