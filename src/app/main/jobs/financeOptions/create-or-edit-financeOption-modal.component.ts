﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { FinanceOptionsServiceProxy, CreateOrEditFinanceOptionDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';

@Component({
    selector: 'createOrEditFinanceOptionModal',
    templateUrl: './create-or-edit-financeOption-modal.component.html'
})
export class CreateOrEditFinanceOptionModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    financeOption: CreateOrEditFinanceOptionDto = new CreateOrEditFinanceOptionDto();

    constructor(
        injector: Injector,
        private _financeOptionsServiceProxy: FinanceOptionsServiceProxy
    ) {
        super(injector);
    }

    show(financeOptionId?: number): void {

        if (!financeOptionId) {
            this.financeOption = new CreateOrEditFinanceOptionDto();
            this.financeOption.id = financeOptionId;

            this.active = true;
            this.modal.show();
        } else {
            this._financeOptionsServiceProxy.getFinanceOptionForEdit(financeOptionId).subscribe(result => {
                debugger;
                this.financeOption = result.financeOption;

                this.active = true;
                this.modal.show();
            });
        }
        
    }

    onShown(): void {
        document.getElementById('FinanceOption_Name').focus();
    }

    save(): void {
        this.saving = true;

        this._financeOptionsServiceProxy.createOrEdit(this.financeOption)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
