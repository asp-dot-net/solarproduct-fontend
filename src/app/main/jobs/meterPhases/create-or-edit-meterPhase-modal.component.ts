﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { MeterPhasesServiceProxy, CreateOrEditMeterPhaseDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';

@Component({
    selector: 'createOrEditMeterPhaseModal',
    templateUrl: './create-or-edit-meterPhase-modal.component.html'
})
export class CreateOrEditMeterPhaseModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    meterPhase: CreateOrEditMeterPhaseDto = new CreateOrEditMeterPhaseDto();



    constructor(
        injector: Injector,
        private _meterPhasesServiceProxy: MeterPhasesServiceProxy
    ) {
        super(injector);
    }

    show(meterPhaseId?: number): void {

        if (!meterPhaseId) {
            this.meterPhase = new CreateOrEditMeterPhaseDto();
            this.meterPhase.id = meterPhaseId;

            this.active = true;
            this.modal.show();
        } else {
            this._meterPhasesServiceProxy.getMeterPhaseForEdit(meterPhaseId).subscribe(result => {
                this.meterPhase = result.meterPhase;


                this.active = true;
                this.modal.show();
            });
        }
        
    }

    save(): void {
            this.saving = true;

			
            this._meterPhasesServiceProxy.createOrEdit(this.meterPhase)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
             });
    }







    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
