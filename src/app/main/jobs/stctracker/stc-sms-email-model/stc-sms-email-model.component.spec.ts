import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StcSmsEmailModelComponent } from './stc-sms-email-model.component';

describe('StcSmsEmailModelComponent', () => {
  let component: StcSmsEmailModelComponent;
  let fixture: ComponentFixture<StcSmsEmailModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StcSmsEmailModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StcSmsEmailModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
