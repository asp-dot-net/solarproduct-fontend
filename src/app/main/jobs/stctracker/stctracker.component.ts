import { Component, Injector, ViewEncapsulation, ViewChild, Input, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JobsServiceProxy, JobDto, GetLeadForViewDto, LeadsServiceProxy, InstallationServiceProxy, OrganizationUnitDto, UserServiceProxy, PvdStatusDto, LeadUsersLookupTableDto, JobStatusTableDto, JobJobTypeLookupTableDto, InstallerServiceProxy, CommonLookupDto, LeadStateLookupTableDto, CommonLookupServiceProxy, JobTrackerServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { NotifyService, TokenService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { EntityTypeHistoryModalComponent } from '@app/shared/common/entityHistory/entity-type-history-modal.component';
import * as _ from 'lodash';
import * as moment from 'moment';
// import { JobDetailModalComponent } from '@app/main/jobs/jobs/job-detail-model.component';
import { CreateOrEditJobStcModalComponent } from './add-stc.modal.component';
import { OnInit } from '@angular/core';
import { ViewApplicationModelComponent } from '../jobs/view-application-model/view-application-model.component';
// import { AddActivityModalComponent } from '@app/main/myleads/myleads/add-activity-model.component';
// import { StcSmsEmailModelComponent } from './stc-sms-email-model/stc-sms-email-model.component';
// import { ViewMyLeadComponent } from '@app/main/myleads/myleads/view-mylead.component';
import { AppConsts } from '@shared/AppConsts';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FileUpload } from 'primeng/fileupload';
import { finalize } from 'rxjs/operators';
import { Title } from '@angular/platform-browser';
import { CommentModelComponent } from '@app/main/activitylog/comment-modal.component';
import { EmailModelComponent } from '@app/main/activitylog/email-modal.component';
import { NotifyModelComponent } from '@app/main/activitylog/notify-modal.component';
import { ReminderModalComponent } from '@app/main/activitylog/reminder-modal.component';
import { SMSModelComponent } from '@app/main/activitylog/sms-modal.component';
import { ToDoModalComponent } from '@app/main/activitylog/todo-modal.component';
import { IssuedActivityLogModelComponent } from '@app/main/invoices/invoiceIssued/Invoiceactivitylog-model.component';

//import { JobsComponent } from './jobs.component';

@Component({
    templateUrl: './stctracker.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class StcTrackerComponent extends AppComponentBase implements OnInit {

    FiltersData = false;
    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    organizationUnitlength: number;
    uploadUrlstc: string;
    filterName = "JobNumber";

    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    @ViewChild('entityTypeHistoryModal', { static: true }) entityTypeHistoryModal: EntityTypeHistoryModalComponent;
    @ViewChild('addstcModal', { static: true }) addstcModal: CreateOrEditJobStcModalComponent;
    @ViewChild('viewApplicationModal', { static: true }) viewApplicationModal: ViewApplicationModelComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    // @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    // @ViewChild('stcTrackerSmsEmailModel', { static: true }) stcTrackerSmsEmailModel: StcSmsEmailModelComponent;
    // @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
    @ViewChild('ExcelFileUpload', { static: false }) excelFileUpload: FileUpload;
    @ViewChild('smsModel', { static: true }) smsModel: SMSModelComponent;
    @ViewChild('emailModel', { static: true }) emailModel: EmailModelComponent;
    @ViewChild('notifyModel', { static: true }) notifyModel: NotifyModelComponent;
    @ViewChild('commentModel', { static: true }) commentModel: CommentModelComponent;
    @ViewChild('todoModal', { static: true }) todoModal: ToDoModalComponent;
    @ViewChild('ReminderModal', { static: true }) ReminderModal: ReminderModalComponent;
    @ViewChild('issuedActivityLogModel', { static: true }) issuedActivityLogModel: IssuedActivityLogModelComponent;
    
    advancedFiltersAreShown = false;
    stausfilterSelect = 0;
    filterText = '';
    maxPanelOnFlatFilter: number;
    maxPanelOnFlatFilterEmpty: number;
    minPanelOnFlatFilter: number;
    minPanelOnFlatFilterEmpty: number;
    maxPanelOnPitchedFilter: number;
    maxPanelOnPitchedFilterEmpty: number;
    minPanelOnPitchedFilter: number;
    minPanelOnPitchedFilterEmpty: number;
    regPlanNoFilter = '';
    lotNumberFilter = '';
    peakMeterNoFilter = '';
    excelorcsvfile = 0;
    offPeakMeterFilter = '';
    enoughMeterSpaceFilter = -1;
    isSystemOffPeakFilter = -1;
    suburbFilter = '';
    stateFilter = ''; salesRepId = 0;
    installerID = 0;
    jobTypeNameFilter = '';
    jobStatusNameFilter = '';
    roofTypeNameFilter = '';
    roofAngleNameFilter = '';
    elecDistributorNameFilter = '';
    leadCompanyNameFilter = '';
    elecRetailerNameFilter = '';
    paymentOptionNameFilter = '';
    depositOptionNameFilter = '';
    meterUpgradeNameFilter = '';
    meterPhaseNameFilter = '';
    promotionOfferNameFilter = '';
    houseTypeNameFilter = '';
    financeOptionNameFilter = '';
    organizationUnit = 0;
    //pvdStatus = 0;
    pvdStatus: [];
    pvdNo = '';
    allOrganizationUnits: OrganizationUnitDto[];
    jobTypes: JobJobTypeLookupTableDto[];
    allPVDStatus: PvdStatusDto[];
    filteredReps: LeadUsersLookupTableDto[];
    alljobstatus: JobStatusTableDto[];
    OpenRecordId: number = 0;
    _entityTypeFullName = 'TheSolarProduct.Jobs.Job';
    entityHistoryEnabled = false;
    jobTypeId = 0;
    uploadUrl1: string;
    // StartDate: moment.Moment;
    // EndDate: moment.Moment;
    
    projectFilter = '';
    stateNameFilter = '';
    // date = '01/01/2021'
    //public sampleDateRange: moment.Moment[] = [moment(this.date), moment().endOf('day')];
    // sampleDateRange: moment.Moment[] = [moment().add(-6, 'days').endOf('day'), moment().add(+1, 'days').startOf('day')];
    jobStatusIDFilter: [];
    jobStatusID: [];
    InstallerList: CommonLookupDto[];
    elecDistributorId = 0;
    applicationstatus = 0;
    ExpandedView: boolean = true;
    SelectedLeadId: number = 0;
    allStates: LeadStateLookupTableDto[];
    totalstcData: any;
    pendingStc = 0.0;
    aprrovedStc = 0.0;
    pvdnoStatus = 0;
    totalStc = 0.0;
    compliancestc = 0.0;
    failedStc = 0.0;
    tradedStc = 0.0;
    paymentid = 0;
    areaNameFilter = "";
    postalcodefrom = "";
    postalcodeTo = "";
    firstrowcount = 0;
    last = 0;

    recStatus = "";

    dateNameFilter = "InstallDate";
    date = new Date();
    // firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    // startDate = moment(this.firstDay);
    // endDate = moment().endOf('day');

    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);

    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 330;
    toggle: boolean = true;
    orgCode = '';
    change() {
        this.toggle = !this.toggle;
      }
    batteryFilter = 0;
    
    constructor(
        injector: Injector,
        private _jobServiceProxy: JobsServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _userServiceProxy: UserServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _router: Router,
        private _httpClient: HttpClient,
        private _jobTrackerServiceProxy: JobTrackerServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy, private _installerServiceProxy: InstallerServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _tokenService: TokenService,
        private titleService: Title
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  STC Tracker");
        this.uploadUrlstc = AppConsts.remoteServiceBaseUrl + '/Users/ImportStcFromExcel';
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        this.entityHistoryEnabled = this.setIsEntityHistoryEnabled();
       
        this._jobServiceProxy.getAllPVDStatusDropdown().subscribe(result => {
            this.allPVDStatus = result;
        });
        this._leadsServiceProxy.getSalesRepForFilter(this.organizationUnit, undefined).subscribe(rep => {
            this.filteredReps = rep;
        });
        this._commonLookupService.getAllJobStatusTableDropdown().subscribe(result => {
            this.alljobstatus = result;
        }); this._commonLookupService.getAllJobTypeForTableDropdown().subscribe(result => {
            this.jobTypes = result;
        });
        this.getinstaller()

        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open STC Tracker';
            log.section = 'STC Tracker';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.orgCode = this.allOrganizationUnits[0].code;
            this.getJobs();
            // this.getCount(this.organizationUnit);
            this.onsalesrep()
        });
    }
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }

        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }

    onsalesrep() {
        this._commonLookupService.getSalesRepForFilter(this.organizationUnit, undefined).subscribe(rep => {
            this.filteredReps = rep;
        });
    }
    
    getinstaller() {
        this._installerServiceProxy.getAllInstallers(this.organizationUnit).subscribe(result => {
            this.InstallerList = result;
        });
    }
    changeOrgCode() {
        this.orgCode = this.allOrganizationUnits.find(x => x.id == this.organizationUnit).code;
    }
    uploadExcel1(data: { files: File }): void {

        const formData: FormData = new FormData();
        const file = data.files[0];
        formData.append('file,1' + ',' + this.organizationUnit, file, file.name);

        this._httpClient
            .post<any>(this.uploadUrl1, formData)
            .pipe(finalize(() => this.excelFileUpload.clear()))
            .subscribe(response => {
                if (response.success) {
                    this.notify.success(this.l('ImportLeadsProcessStart'));
                } else if (response.error != null) {
                    this.notify.error(this.l('ImportLeadUploadFailed'));
                }
            });
    }
    
    onUploadExcelError1(): void {
        this.notify.error(this.l('ImportLeadUploadFailed'));
    }
    
    private setIsEntityHistoryEnabled(): boolean {
        let customSettings = (abp as any).custom;
        return this.isGrantedAny('Pages.Administration.AuditLogs') && customSettings.EntityHistory && customSettings.EntityHistory.isEnabled && _.filter(customSettings.EntityHistory.enabledEntities, entityType => entityType === this._entityTypeFullName).length === 1;
    }

    getJobs(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        
        this.primengTableHelper.showLoadingIndicator();
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._jobTrackerServiceProxy.getAllSTCTracker(
            this.filterName,
            filterText_,
            this.organizationUnit,
            this.pvdStatus,
            this.pvdnoStatus,
            this.salesRepId,
            this.jobStatusIDFilter,
            this.jobTypeId,
            this.pvdNo,
            this.installerID,
            this.stateNameFilter,
            this.recStatus,
            this.dateNameFilter,
            this.startDate,
            this.endDate,
            this.batteryFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            this.shouldShow = false;
            this.totalstcData = result.items[0].summary;
        });
    }

    reloadPage($event): void {
        this.ExpandedView = true;
        this.paginator.changePage(this.paginator.getPage());
    }

    setOpenRecord(id) {
        if (id === this.OpenRecordId) { this.OpenRecordId = 0; return; }
        this.OpenRecordId = id;
    }

    showHistory(job: JobDto): void {
        this.entityTypeHistoryModal.show({
            entityId: job.id.toString(),
            entityTypeFullName: this._entityTypeFullName,
            entityTypeDescription: ''
        });
    }

    quickview(jobid): void {
        this.viewApplicationModal.show(jobid);
    }

    exportToExcel(excelorcsv): void {
        this.excelorcsvfile = excelorcsv;
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._jobTrackerServiceProxy.getAllSTCTrackerExport(
            this.filterName,
            filterText_,
            this.organizationUnit,
            this.pvdStatus,
            this.pvdnoStatus,
            this.salesRepId,
            this.jobStatusIDFilter,
            this.jobTypeId,
            this.pvdNo,
            this.installerID,
            this.stateNameFilter,
            this.recStatus,
            this.dateNameFilter,
            this.startDate,
            this.endDate,
            this.excelorcsvfile
        )
        .subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
        });
    }
    expandGrid() {
        this.ExpandedView = true;
    }
    
    uploadExcelstc(data: { files: File }): void {
        
        var headers_object = new HttpHeaders().set("Authorization", "Bearer " + this._tokenService.getToken());

        const httpOptions = {
            headers: headers_object
        };
        const formData: FormData = new FormData();
        const file = data.files[0];
        formData.append('file' + ',' + this.organizationUnit, file, file.name);
        this._httpClient
            .post<any>(this.uploadUrlstc, formData, httpOptions)
            .pipe(finalize(() => this.excelFileUpload.clear()))
            .subscribe(response => {
                if (response.success) {
                    this.notify.success(this.l('ImportStcProcessStart'));
                } else if (response.error != null) {
                    this.notify.error(this.l('ImportStcUploadFailed'));
                }
            });
    }

    onUploadExcelErrorstc(): void {
        this.notify.error(this.l('ImportSTCUploadFailed'));
    }

    navigateToLeadDetail(leadid): void {
        // this.ExpandedView = !this.ExpandedView;
        // this.ExpandedView = false;
        // this.SelectedLeadId = leadid;
        // this.viewLeadDetail.showDetail(leadid, '', 7);
    }
    clear() {
        this.stateNameFilter = '';
        this.elecDistributorId = 0;
        this.jobStatusIDFilter = [];
        this.jobTypeId = 0;
        
        let date = new Date();
        let firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
        this.startDate = moment(this.date);
        this.endDate = moment(this.date);
        
        //  this.pvdStatus = 0;
        this.pvdNo = '';
        this.jobTypeId = 0;
        this.projectFilter = '';
        this.stateNameFilter = '';
        this.batteryFilter = 0;
        this.getJobs();
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'STC Tracker';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'STC Tracker';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }
    
}