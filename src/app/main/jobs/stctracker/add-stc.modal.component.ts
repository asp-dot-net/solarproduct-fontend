import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { CreateOrEditInvoicePaymentDto, CreateOrEditJobDto, InvoicePaymentInvoicePaymentMethodLookupTableDto, InvoicePaymentsServiceProxy, JobDto, JobLeadLookupTableDto, JobsServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';

@Component({
    selector: 'addstcModal',
    templateUrl: './add-stc.modal.component.html'
})

export class CreateOrEditJobStcModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    sectionId=0;
    saving = false;
    receiveddate: moment.Moment;
    allPVDStatus: JobLeadLookupTableDto[];
    job: CreateOrEditJobDto = new CreateOrEditJobDto();
    jobid: number;
    leadCompanyName:any;
    jobNumber:any;
    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _jobServiceProxy: JobsServiceProxy,
    ) {
        super(injector);
    }
    sectionName = '';
    show(jobid: number, sectionId : number,section = ''): void {
        this.jobid = jobid;
        this.sectionId = sectionId;
        this.modal.show();
        this.sectionName =section;
        let log = new UserActivityLogDto();
             log.actionId = 79;
             log.actionNote ='Open STC Status Notes';
             log.section = section;
             this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                 .subscribe(() => {
             });
        this._jobServiceProxy.getPVDStatusList().subscribe(result => {
            this.allPVDStatus = result;
        });
        this._jobServiceProxy.getJobForEdit(jobid).subscribe(result => {
            this.job = result.job;
            this.leadCompanyName = result.leadCompanyName;
            this.jobNumber = result.job.jobNumber;
            this.modal.show();
        });

    }
    save(): void {
        this.saving = true;
        this.job.id = this.jobid;
        this.job.sectionId = this.sectionId;
        this._jobServiceProxy.update_JOb_STCDetails(this.job)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                let log = new UserActivityLogDto();
                log.actionId = 18;
                log.actionNote ="Add STC Details by for Project No: "  + this.job.jobNumber;
                log.section = this.sectionName;
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);

            });
    }

    close(): void {
        this.modal.hide();
    }
}