﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { RefundReasonsServiceProxy, CreateOrEditRefundReasonDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';


@Component({
    selector: 'createOrEditRefundReasonModal',
    templateUrl: './create-or-edit-refundReason-modal.component.html'
})
export class CreateOrEditRefundReasonModalComponent extends AppComponentBase {
   
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    refundReason: CreateOrEditRefundReasonDto = new CreateOrEditRefundReasonDto();



    constructor(
        injector: Injector,
        private _refundReasonsServiceProxy: RefundReasonsServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }
    
    show(refundReasonId?: number): void {
    

        if (!refundReasonId) {
            this.refundReason = new CreateOrEditRefundReasonDto();
            this.refundReason.id = refundReasonId;

            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Refund Reason';
            log.section = 'Refund Reason';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._refundReasonsServiceProxy.getRefundReasonForEdit(refundReasonId).subscribe(result => {
                this.refundReason = result.refundReason;


                this.active = true;
                this.modal.show();
                
let log = new UserActivityLogDto();
log.actionId = 79;
log.actionNote ='Open For Edit Refund Reason : ' + this.refundReason.name;
log.section = 'Refund Reason';
this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
    .subscribe(() => {
});
            });
        }
        
    }

    onShown(): void {
        
        document.getElementById('RefundReason_Name').focus();
    }
    
    save(): void {
            this.saving = true;
			
            this._refundReasonsServiceProxy.createOrEdit(this.refundReason)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.refundReason.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Refund Reason Updated : '+ this.refundReason.name;
                    log.section = 'Refund Reason';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Refund Reason Created : '+ this.refundReason.name;
                    log.section = 'Refund Reason';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }







    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
