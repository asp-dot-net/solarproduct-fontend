﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetRefundReasonForViewDto, RefundReasonDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
@Component({
    selector: 'viewRefundReasonModal',
    templateUrl: './view-refundReason-modal.component.html'
})
export class ViewRefundReasonModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetRefundReasonForViewDto;


    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
        this.item = new GetRefundReasonForViewDto();
        this.item.refundReason = new RefundReasonDto();
    }

    show(item: GetRefundReasonForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Opened Refund Reason View : ' + this.item.refundReason.name;
        log.section = 'Refund Reason';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {            
         });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
