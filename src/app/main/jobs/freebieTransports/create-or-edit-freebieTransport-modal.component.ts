﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { FreebieTransportsServiceProxy, CreateOrEditFreebieTransportDto,UserActivityLogServiceProxy,UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'createOrEditFreebieTransportModal',
    templateUrl: './create-or-edit-freebieTransport-modal.component.html'
})
export class CreateOrEditFreebieTransportModalComponent extends AppComponentBase implements OnInit {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    saving = false;

    freebieTransport: CreateOrEditFreebieTransportDto = new CreateOrEditFreebieTransportDto();

    constructor(
        injector: Injector,
        private _freebieTransportsServiceProxy: FreebieTransportsServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
    }
    show(freebieTransportId?: number): void {
        if (!freebieTransportId) {
            this.freebieTransport = new CreateOrEditFreebieTransportDto();
            this.freebieTransport.id = freebieTransportId;
            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Freebie Transports';
            log.section = 'Freebie Transports';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
        } else {
            this._freebieTransportsServiceProxy.getFreebieTransportForEdit(freebieTransportId).subscribe(result => {
                this.freebieTransport = result.freebieTransport;
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Freebie Transports : ' + this.freebieTransport.name;
                log.section = 'Freebie Transports';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            });
        }
    }

    onShown(): void {
        
        document.getElementById('FreebieTransport_Name').focus();
    }

    save(): void {
        this.saving = true;
        this._freebieTransportsServiceProxy.createOrEdit(this.freebieTransport)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.freebieTransport.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Freebie Transports Updated : '+ this.freebieTransport.name;
                    log.section = 'Freebie Transports';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Freebie Transports Created : '+ this.freebieTransport.name;
                    log.section = 'Freebie Transports';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
            });
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }

    ngOnInit(): void {

    }
}
