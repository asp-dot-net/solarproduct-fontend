﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { RoofAnglesServiceProxy, CreateOrEditRoofAngleDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'createOrEditRoofAngleModal',
    templateUrl: './create-or-edit-roofAngle-modal.component.html'
})
export class CreateOrEditRoofAngleModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    roofAngle: CreateOrEditRoofAngleDto = new CreateOrEditRoofAngleDto();

    constructor(
        injector: Injector,
        private _roofAnglesServiceProxy: RoofAnglesServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }

    show(roofAngleId?: number): void {

        if (!roofAngleId) {
            this.roofAngle = new CreateOrEditRoofAngleDto();
            this.roofAngle.id = roofAngleId;

            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Roof Angle';
            log.section = 'Roof Angle';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });

        } else {
            this._roofAnglesServiceProxy.getRoofAngleForEdit(roofAngleId).subscribe(result => {
                this.roofAngle = result.roofAngle;


                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Roof Angle : ' + this.roofAngle.name;
                log.section = 'Roof Angle';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        
    }

    onShown(): void {
        
        document.getElementById('RoofAngle_Name').focus();
    }

    save(): void {
            this.saving = true;

			
            this._roofAnglesServiceProxy.createOrEdit(this.roofAngle)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.roofAngle.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Roof Angle Updated : '+ this.roofAngle.name;
                    log.section = 'Roof Angle';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Roof Angle Created : '+ this.roofAngle.name;
                    log.section = 'Roof Angle';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }

             });
    }







    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
