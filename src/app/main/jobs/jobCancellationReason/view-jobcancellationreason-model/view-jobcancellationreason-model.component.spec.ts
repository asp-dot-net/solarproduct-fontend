import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewJobcancellationreasonModelComponent } from './view-jobcancellationreason-model.component';

describe('ViewJobcancellationreasonModelComponent', () => {
  let component: ViewJobcancellationreasonModelComponent;
  let fixture: ComponentFixture<ViewJobcancellationreasonModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewJobcancellationreasonModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewJobcancellationreasonModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
