import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetHouseTypeForViewDto, GetJobCancellationReasonForViewDto,  JobCancellationReasonDto ,UserActivityLogServiceProxy, UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
@Component({
  selector: 'viewjobcancellationreason',
  templateUrl: './view-jobcancellationreason-model.component.html'

})
export class ViewJobcancellationreasonModelComponent extends AppComponentBase {

  @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active = false;
  saving = false;

  item: GetJobCancellationReasonForViewDto;
  constructor(
      injector: Injector,
      private _userActivityLogServiceProxy : UserActivityLogServiceProxy

  ) {
      super(injector);
      this.item = new GetJobCancellationReasonForViewDto();
      this.item.jobCancellationReason = new JobCancellationReasonDto();
  }

  show(item: GetJobCancellationReasonForViewDto): void {
      this.item = item;
      this.active = true;
      this.modal.show();
      let log = new UserActivityLogDto();
      log.actionId = 79;
      log.actionNote ='Opened Job Cancellation View : ' + item.jobCancellationReason.name;
      log.section = 'Job Cancellation';
      this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
      .subscribe(() => {            
       }); 
  }

  close(): void {
      this.active = false;
      this.modal.hide();
  }
}
