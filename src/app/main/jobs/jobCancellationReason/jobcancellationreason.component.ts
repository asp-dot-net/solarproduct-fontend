import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { JobCancellationReasonDto, JobCancellationReasonServiceProxy,UserActivityLogServiceProxy, UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import * as _ from 'lodash';
import { finalize } from 'rxjs/operators';
import { CreateOrEditJobCancellationReasonModalComponent } from './create-or-edit-jobcancellationreason-modal.component';
import { ViewJobcancellationreasonModelComponent } from './view-jobcancellationreason-model/view-jobcancellationreason-model.component';
import { Title } from '@angular/platform-browser';


@Component({
    templateUrl: './jobcancellationreason.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class JobCancellationReasonComponent extends AppComponentBase {

    @ViewChild('createOrEditJobcancellationreasonModal', { static: true }) createOrEditJobcancellationreasonModal: CreateOrEditJobCancellationReasonModalComponent;
    @ViewChild('viewjobcancellationreason', { static: true }) viewjobcancellationreason: ViewJobcancellationreasonModelComponent;

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    firstrowcount = 0;
    last = 0;

    public screenHeight: any;  
    testHeight = 250;

    constructor(
        injector: Injector,
        private _jobcancellationreasonServiceProxy: JobCancellationReasonServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private titleService: Title
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Job Cancellation Reason");
    }
    searchLog() : void {
        debugger;
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Job Cancellation';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Job Cancellation';
        log.section = 'Job Cancellation';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
    }
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    getJobCancellationReason(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._jobcancellationreasonServiceProxy.getAll(
            this.filterText,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    deletejobcancellationreason(jobcanellationreason: JobCancellationReasonDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._jobcancellationreasonServiceProxy.delete(jobcanellationreason.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete Job Cancellation: ' + jobcanellationreason.name;
                            log.section = 'Job Cancellation';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });
                        });
                }
            }
        );
    }


    createjobcancellationreason(): void {
        this.createOrEditJobcancellationreasonModal.show();
    }

}
