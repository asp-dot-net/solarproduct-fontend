import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CreateOrEditJobCancellationReasonDto, JobCancellationReasonServiceProxy ,UserActivityLogServiceProxy, UserActivityLogDto } from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'createOrEditJobcancellationreasonModal',
    templateUrl: './create-or-edit-jobcancellationreason-model.component.html'
})
export class CreateOrEditJobCancellationReasonModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    jobcancellationreason: CreateOrEditJobCancellationReasonDto = new CreateOrEditJobCancellationReasonDto();
    constructor(
        injector: Injector,
        private _jobcancellationreasonServiceProxy: JobCancellationReasonServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
    }

    show(jobcancellationreasonId?: number): void {

        if (!jobcancellationreasonId) {
            this.jobcancellationreason = new CreateOrEditJobCancellationReasonDto();
            this.jobcancellationreason.id = jobcancellationreasonId;
            this.active = true;
            this.modal.show();

            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Job Cancellation';
            log.section = 'Job Cancellation';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._jobcancellationreasonServiceProxy.getJobCancellationReasonForEdit(jobcancellationreasonId).subscribe(result => {
                this.jobcancellationreason = result.jobCancellationReasonDto;
                this.active = true;
                this.modal.show();
                 let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Job Cancellation: ' + this.jobcancellationreason.name;
                log.section = 'Job Cancellation';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        
    }

    onShown(): void {
        
        document.getElementById('Name').focus();
    }
    
    save(): void {
            this.saving = true;
            this._jobcancellationreasonServiceProxy.createOrEdit(this.jobcancellationreason)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.jobcancellationreason.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Job Cancellation Updated : '+ this.jobcancellationreason.name;
                    log.section = 'Job Cancellation';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Job Cancellation Created : '+ this.jobcancellationreason.name;
                    log.section = 'Job Cancellation';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
