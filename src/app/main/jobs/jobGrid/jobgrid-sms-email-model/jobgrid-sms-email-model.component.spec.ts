import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobgridSmsEmailModelComponent } from './jobgrid-sms-email-model.component';

describe('JobgridSmsEmailModelComponent', () => {
  let component: JobgridSmsEmailModelComponent;
  let fixture: ComponentFixture<JobgridSmsEmailModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobgridSmsEmailModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobgridSmsEmailModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
