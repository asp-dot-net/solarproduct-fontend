﻿import { Component, Injector, ViewEncapsulation, ViewChild, ElementRef, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { JobsServiceProxy, JobDto, UserServiceProxy, OrganizationUnitDto, LeadsServiceProxy, LeadStateLookupTableDto, JobStatusTableDto, JobJobTypeLookupTableDto, JobFinanceOptionLookupTableDto, LeadUsersLookupTableDto, LeadSourceLookupTableDto, JobPaymentOptionLookupTableDto, CommonLookupServiceProxy, UserActivityLogServiceProxy, UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { EntityTypeHistoryModalComponent } from '@app/shared/common/entityHistory/entity-type-history-modal.component';
import * as _ from 'lodash';
import * as moment from 'moment';
import { OnInit } from '@angular/core';
import { ViewApplicationModelComponent } from '../jobs/view-application-model/view-application-model.component';
import { AddActivityModalComponent } from '@app/main/myleads/myleads/add-activity-model.component';
import { JobgridSmsEmailModelComponent } from './jobgrid-sms-email-model/jobgrid-sms-email-model.component';
import { ViewMyLeadComponent } from '@app/main/myleads/myleads/view-mylead.component';
import { Title } from '@angular/platform-browser';
import { SMSModelComponent } from '@app/main/activitylog/sms-modal.component';
import { EmailModelComponent } from '@app/main/activitylog/email-modal.component';
import { NotifyModelComponent } from '@app/main/activitylog/notify-modal.component';
import { CommentModelComponent } from '@app/main/activitylog/comment-modal.component';
import { ToDoModalComponent } from '@app/main/activitylog/todo-modal.component';
import { ReminderModalComponent } from '@app/main/activitylog/reminder-modal.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
@Component({
    templateUrl: './jobGrid.component.html',
    styleUrls: ['./jobGrid.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class jobGridComponent extends AppComponentBase implements OnInit {

    show: boolean = true;
    FiltersData = false;
    isShowDivIf = true;  
    showchild: boolean = true;
    shouldShow: boolean = false;
    organizationUnitlength: number = 0;
    priceType ='';
    toggle: boolean = true;
    jobTypeFilterList = [];
    change() {
        this.toggle = !this.toggle;
      }

    toggleDisplayDivIf() {  
        this.isShowDivIf = !this.isShowDivIf;  
      }      
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    @ViewChild('entityTypeHistoryModal', { static: true }) entityTypeHistoryModal: EntityTypeHistoryModalComponent;
    @ViewChild('viewApplicationModal', { static: true }) viewApplicationModal: ViewApplicationModelComponent;
    @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('jobgridTrackerSmsEmailModal', { static: true }) jobgridTrackerSmsEmailModal: JobgridSmsEmailModelComponent;
    @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
    @ViewChild('smsModel', { static: true }) smsModel: SMSModelComponent;
    @ViewChild('emailModel', { static: true }) emailModel: EmailModelComponent;
    @ViewChild('notifyModel', { static: true }) notifyModel: NotifyModelComponent;
    @ViewChild('commentModel', { static: true }) commentModel: CommentModelComponent;
    @ViewChild('todoModal', { static: true }) todoModal: ToDoModalComponent;
    @ViewChild('ReminderModal', { static: true }) ReminderModal: ReminderModalComponent;

    @ViewChild('content') content: ElementRef;
    advancedFiltersAreShown = false;
    maxPanelOnFlatFilter: number;
    minPanelOnFlatFilter: number;
    maxPanelOnPitchedFilter: number;
    minPanelOnPitchedFilter: number;
    regPlanNoFilter = '';
    lotNumberFilter = '';
    peakMeterNoFilter = '';
    offPeakMeterFilter = '';
    enoughMeterSpaceFilter = -1;
    isSystemOffPeakFilter = -1;
    suburbFilter = '';
    stateFilter = '';
    diplayifvic = false;
    jobTypeNameFilter = '';
    jobStatusNameFilter = '';
    roofTypeNameFilter = '';
    roofAngleNameFilter = '';
    elecDistributorNameFilter = '';
    leadCompanyNameFilter = '';
    elecRetailerNameFilter = '';
    paymentOptionNameFilter = '';
    depositOptionNameFilter = '';
    meterUpgradeNameFilter = '';
    meterPhaseNameFilter = '';
    promotionOfferNameFilter = '';
    houseTypeNameFilter = '';
    financeOptionNameFilter = '';
    OpenRecordId: number = 0;
    organizationUnit = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    _entityTypeFullName = 'TheSolarProduct.Jobs.Job';
    entityHistoryEnabled = false;
    
    projectFilter = '';
    stateNameFilter = '';
    date = new Date();
    
    // filterText = '821831';
    // firstDay = new Date(this.date.getFullYear() - 5, this.date.getMonth(), 1);
    
    filterName = 'JobNumber';
    filterText = '';
    
    firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    startDate: moment.Moment = moment(this.firstDay);
    endDate: moment.Moment = moment(this.date);
    
    jobStatusIDFilter = [];
    elecDistributorId = 0;
    applicationstatus = 0;
    jobTypeId = 0;
    allStates: LeadStateLookupTableDto[];
    alljobstatus: JobStatusTableDto[];
    jobTypes: JobJobTypeLookupTableDto[];
    jobFinOptions: JobFinanceOptionLookupTableDto[];
    // filteredManagers: LeadUsersLookupTableDto[];
    filteredReps: LeadUsersLookupTableDto[];
    filteredTeams: LeadUsersLookupTableDto[];
    jobPaymentOptions: JobPaymentOptionLookupTableDto[];
    salesManagerId = 0;
    salesRepId = 0;
    jobDateFilter = 'CreationDate';
    invertSuggestions: string[];
    panelSuggestions: string[];
    batterySuggestions :string[];
    panelModel = '';
    invertModel = '';
    batteryModel = '';
    readytoactive = 0;
    systemStrtRange = 0;
    systemEndRange = 0;
    ExpandedView: boolean = true;
    SelectedLeadId: number = 0;
    leadSourceIdFilter = [];
    allLeadSources: LeadSourceLookupTableDto[];
    totalNoOfPanel = 0;
    totalNoOfInvert = 0;
    totalSystemCapacity = 0;
    totalMetroSystemCapacity = 0;
    totalRegionalSystemCapacity = 0;
    totalBatteryKw = 0;
    totalCost = 0;
    depositeReceived = 0;
    active = 0;
    installJob = 0;
    hot = 0;
    closed = 0;
    price = 0;
    addressFilter = "";
    paymentid = 0;
    areaNameFilter = "";
    postalcodefrom = "";
    postalcodeTo = "";
    teamId = 0;
    excelorcsvfile = 0;
    solarvicstatus = 0;
    firstrowcount = 0;
    last = 0;

    batteryFilter = 0;

    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 330;

    showCancel: boolean = false;
    showHold: boolean = false;

    previousJobStatusId = [];

    orgCode = "";
    metroPrice = 0;
    regionalPrice = 0;
    totalMetroBatteryKw = 0;
    totalRegionalBatteryKw = 0;
     
    constructor(
        injector: Injector,
        private _jobsServiceProxy: JobsServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _router: Router,
        private titleService: Title,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private spinner: NgxSpinnerService
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Job Tracker");
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
        
        // this.scrollableCols = [
           
        //     { field: 'record.jobStatusName', header: 'JobStatus' },
        //     { field: 'record.jobSolarRebateStatus', header: 'solarRebateStatus' },
        //     { field: 'record.job.currentLeadOwaner', header: 'SalesRepName' },
        //     { field: 'record.leadSourceName', header: 'LeadSource' },
        //     { field: 'record.job.postalCode', header: 'P/CD' },
        //     { field: 'record.leadCompanyName', header: 'Contact' },
        //     { field: 'record.mobile', header: 'Mobile' },
        //     { field: 'record.job.installationDate', header: 'InstallationBookingDate' }, 
        //      { field: 'record.job.depositeRecceivedDate', header: 'depositeRecceivedDate' }
        //     ,
        //     { field: 'record.activityComment', header: 'Comment' },
        //     { field: 'record.activityReminderTime', header: 'ReminderDate' },
        //     { field: 'record.activityDescription', header: 'Reminder' },
        //     { field: 'record.job.jobGridSmsSend', header: 'SmsSend' },
        //     { field: 'record.job.jobGridEmailSend', header: 'EmailSend' }
        // ];
    
        // this.frozenCols = [
        //     { field: 'action', header: 'Actions' },
        //     { field: 'record.job.jobNumber', header: 'ProjNo' },
        // ];
        this.entityHistoryEnabled = this.setIsEntityHistoryEnabled();

        this._commonLookupService.getAllLeadSourceDropdown().subscribe(result => {
            this.allLeadSources = result;
        });
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
        this._commonLookupService.getTeamForFilter().subscribe(teams => {
            this.filteredTeams = teams;
        });
        this._commonLookupService.getAllJobStatusTableDropdown().subscribe(result => {
            this.alljobstatus = result;
        });
        this._commonLookupService.getAllPaymentOptionForTableDropdown().subscribe(result => {
            this.jobPaymentOptions = result;
        });
        this._commonLookupService.getAllJobTypeForTableDropdown().subscribe(result => {
            this.jobTypes = result;
        });

        this._commonLookupService.getAllFinanceOptionForTableDropdown().subscribe(result => {
            this.jobFinOptions = result;
        });

        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Jobs';
            log.section = 'Jobs';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.orgCode = this.allOrganizationUnits[0].code;
            // this.onsalesmanger()
            this.onsalesrep()
            this.getJobs();
        });
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }

        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }
    // onsalesmanger() {
    //     this._commonLookupService.getSalesManagerForFilter(this.organizationUnit, undefined).subscribe(manager => {
    //         this.filteredManagers = manager;
    //     });
    // }
    onsalesrep() {
        this._commonLookupService.getSalesRepForFilter(this.organizationUnit, undefined).subscribe(rep => {
            this.filteredReps = rep;
        });
    }


    private setIsEntityHistoryEnabled(): boolean {
        let customSettings = (abp as any).custom;
        return this.isGrantedAny('Pages.Administration.AuditLogs') && customSettings.EntityHistory && customSettings.EntityHistory.isEnabled && _.filter(customSettings.EntityHistory.enabledEntities, entityType => entityType === this._entityTypeFullName).length === 1;
    }

    setOpenRecord(id) {
        if (id === this.OpenRecordId) { this.OpenRecordId = 0; return; }
        this.OpenRecordId = id;
    }

    changeOrgCode() {
        this.orgCode = this.allOrganizationUnits.find(x => x.id == this.organizationUnit).code;
        this.getJobs();

    }

    getJobs(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        
        if(this.jobStatusIDFilter.includes(3) || this.jobDateFilter == "CancelDate" || this.previousJobStatusId.length > 0)
        {
            this.showCancel = true;
        }
        else
        {
            this.showCancel = false;
        }

        if(this.jobDateFilter == "HoldDate")
        {
            this.showHold = true;
        }
        else
        {
            this.showHold = false;
        }

        if (this.stateFilter == "VIC") {
            this.diplayifvic = true;
        } else { this.diplayifvic = false; }
        
        let filter = "";
        if(this.filterName == 'JobNumber' && this.filterText != undefined && this.filterText != null && this.filterText.trim() != ''){
            filter = this.orgCode + this.filterText.trim();
        }
        else {
            filter = this.filterText;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._jobsServiceProxy.getAllJobGrid(
            this.filterName,
            filter,
            this.suburbFilter,
            this.stateFilter,
            this.jobTypeNameFilter,
            this.jobStatusNameFilter,
            this.roofTypeNameFilter,
            this.roofAngleNameFilter,
            this.elecDistributorNameFilter,
            this.leadCompanyNameFilter,
            this.elecRetailerNameFilter,
            this.paymentOptionNameFilter,
            this.depositOptionNameFilter,
            this.meterUpgradeNameFilter,
            this.meterPhaseNameFilter,
            this.promotionOfferNameFilter,
            this.houseTypeNameFilter,
            this.financeOptionNameFilter,
            this.organizationUnit,
            this.projectFilter,
            this.stateNameFilter,
            this.elecDistributorId,
            this.jobStatusIDFilter,
            this.applicationstatus,
            this.jobTypeId,
            // this.startDate.toString(),
            // this.endDate.toString(),
            this.startDate,
            this.endDate,
            undefined,
            undefined,
            undefined,
            this.salesManagerId == 0 ? undefined : this.salesManagerId,
            this.salesRepId == 0 ? undefined : this.salesRepId,
            this.jobDateFilter,
            this.panelModel,
            this.invertModel,
            this.readytoactive,
            this.systemStrtRange,
            this.systemEndRange,
            this.leadSourceIdFilter,
            undefined,
            undefined, 0, undefined,
            this.teamId,
            this.addressFilter, undefined, undefined,
            this.postalcodefrom,
            this.postalcodeTo,
            this.paymentid,
            this.areaNameFilter,
            this.solarvicstatus,
            this.batteryFilter,
            this.previousJobStatusId,
            this.batteryModel,
            this.priceType,
            this.jobTypeFilterList,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            if (result.totalCount > 0) {
                //this.getLeadSummaryCount();

                this.totalNoOfPanel = result.items[0].summerCount.totalNoOfPanel;
                this.totalSystemCapacity = result.items[0].summerCount.totalSystemCapacity;
                this.totalCost = result.items[0].summerCount.totalCost;
                this.depositeReceived = result.items[0].summerCount.depositeReceived;
                this.active = result.items[0].summerCount.active;
                this.installJob = result.items[0].summerCount.installJob;
                // this.price = parseFloat((result.items[0].summerCount.totalCost / result.items[0].summerCount.totalSystemCapacity).toFixed(2));
                let totalCost = result.items[0].summerCount.totalCost != undefined && result.items[0].summerCount.totalCost != null ? result.items[0].summerCount.totalCost : 0;
                let totalSystemCapacity = result.items[0].summerCount.totalSystemCapacity != undefined && result.items[0].summerCount.totalSystemCapacity != null ? result.items[0].summerCount.totalSystemCapacity : 0;
                this.price = totalSystemCapacity > 0 ? parseFloat((totalCost / totalSystemCapacity).toFixed(2)) : 0;
                this.totalBatteryKw = result.items[0].summerCount.totalBatteryKw;
                this.totalMetroSystemCapacity =  result.items[0].summerCount.totalMetroSystemCapacity != undefined && result.items[0].summerCount.totalMetroSystemCapacity != null ? result.items[0].summerCount.totalMetroSystemCapacity : 0;;
                this.totalRegionalSystemCapacity = result.items[0].summerCount.totalRegionalSystemCapacity != undefined && result.items[0].summerCount.totalRegionalSystemCapacity != null ? result.items[0].summerCount.totalRegionalSystemCapacity : 0;;

                let totalMetroCost = result.items[0].summerCount.totalMetroCost != undefined && result.items[0].summerCount.totalMetroCost != null ? result.items[0].summerCount.totalMetroCost : 0;
                let totalRegionalCost = result.items[0].summerCount.totalRegionalCost != undefined && result.items[0].summerCount.totalRegionalCost != null ? result.items[0].summerCount.totalRegionalCost : 0;

                this.metroPrice = this.totalMetroSystemCapacity > 0 ? parseFloat((totalMetroCost / this.totalMetroSystemCapacity).toFixed(2)) : 0;
                this.regionalPrice = this.totalRegionalSystemCapacity > 0 ? parseFloat((totalRegionalCost / this.totalRegionalSystemCapacity).toFixed(2)) : 0;
                this.totalMetroBatteryKw = result.items[0].summerCount.totalMetroBatteryKw != undefined && result.items[0].summerCount.totalMetroBatteryKw != null ? result.items[0].summerCount.totalMetroBatteryKw : 0;
                this.totalRegionalBatteryKw = result.items[0].summerCount.totalRegionalBatteryKw != undefined && result.items[0].summerCount.totalRegionalBatteryKw != null ? result.items[0].summerCount.totalRegionalBatteryKw : 0;
            } else {
                this.totalNoOfPanel = 0;
                this.totalSystemCapacity = 0;
                this.totalCost = 0;
                this.depositeReceived = 0;
                this.active = 0;
                this.installJob = 0;
                this.price = 0;
                this.totalMetroSystemCapacity = 0;
                this.totalRegionalSystemCapacity = 0;
                this.metroPrice = 0;
                this.regionalPrice = 0;
                this.totalRegionalBatteryKw = 0;
                this.totalMetroBatteryKw = 0;
            }

            this.shouldShow = false;
            // if (result.totalCount > 0) {

            //     this.totalNoOfPanel = result.items[0].totalNoOfPanel;
            //     this.totalSystemCapacity = result.items[0].totalSystemCapacity;
            //     this.totalCost = result.items[0].totalCost;
            //     this.depositeReceived = result.items[0].depositeReceived;
            //     this.active = result.items[0].active;
            //     this.installJob = result.items[0].installJob;
            //     this.price = parseFloat((result.items[0].totalCost / result.items[0].totalSystemCapacity).toFixed(2));
            // } else {
            //     this.totalNoOfPanel = 0;
            //     this.totalSystemCapacity = 0;
            //     this.totalCost = 0;
            //     this.depositeReceived = 0;
            //     this.active = 0;
            //     this.installJob = 0;
            //     this.price = 0;
            // }
        },
        err => {
            this.primengTableHelper.hideLoadingIndicator();
        });

    }
    // getLeadSummaryCount() {
    //     debugger;

    //     this._jobsServiceProxy.getAlljobCount(

    //         this.filterText,
    //         this.suburbFilter,
    //         this.stateFilter,
    //         this.jobTypeNameFilter,
    //         this.jobStatusNameFilter,
    //         this.roofTypeNameFilter,
    //         this.roofAngleNameFilter,
    //         this.elecDistributorNameFilter,
    //         this.leadCompanyNameFilter,
    //         this.elecRetailerNameFilter,
    //         this.paymentOptionNameFilter,
    //         this.depositOptionNameFilter,
    //         this.meterUpgradeNameFilter,
    //         this.meterPhaseNameFilter,
    //         this.promotionOfferNameFilter,
    //         this.houseTypeNameFilter,
    //         this.financeOptionNameFilter,
    //         this.organizationUnit,
    //         this.projectFilter,
    //         this.stateNameFilter,
    //         this.elecDistributorId,
    //         this.jobStatusIDFilter,
    //         this.applicationstatus,
    //         this.jobTypeId,
    //         this.startDate,
    //         this.endDate,
    //         undefined,
    //         undefined,
    //         undefined,
    //         this.salesManagerId == 0 ? undefined : this.salesManagerId,
    //         this.salesRepId == 0 ? undefined : this.salesRepId,
    //         this.jobDateFilter,
    //         this.panelModel,
    //         this.invertModel,
    //         this.readytoactive,
    //         this.systemStrtRange,
    //         this.systemEndRange,
    //         this.leadSourceIdFilter,
    //         undefined,
    //         undefined, 0, undefined,
    //         this.teamId,
    //         this.addressFilter, undefined, undefined,
    //         this.postalcodefrom,
    //         this.postalcodeTo,
    //         this.paymentid,
    //         this.areaNameFilter,
    //         this.solarvicstatus,
    //         undefined,
    //         undefined,
    //         undefined,
    //     ).subscribe(result => {
    //         debugger;
    //         if (result) {
    //             this.totalNoOfPanel = result.totalNoOfPanel;
    //             this.totalSystemCapacity = result.totalSystemCapacity;
    //             this.totalCost = result.totalCost;
    //             this.depositeReceived = result.depositeReceived;
    //             this.active = result.active;
    //             this.installJob = result.installJob;
    //             this.price = parseFloat((result.totalCost / result.totalSystemCapacity).toFixed(2));
    //         }
    //     });
    // }
    filterPanelModel(event): void {
        this._jobsServiceProxy.getpanelmodel(event.query).subscribe(output => {
            this.panelSuggestions = output;
        });
    }
    
    filterInvertModel(event): void {
        this._jobsServiceProxy.getinvertmodel(event.query).subscribe(output => {
            this.invertSuggestions = output;
        });
    }

    filterBatteryModel(event): void {
        this._jobsServiceProxy.getBatteryModel(event.query).subscribe(output => {
            this.batterySuggestions = output;
        });
    }
    
    reloadPage($event): void {
        this.ExpandedView = true;
        this.paginator.changePage(this.paginator.getPage());
    }

    createJob(): void {
        this._router.navigate(['/app/admin/jobs/jobs/createOrEdit']);
    }

    showHistory(job: JobDto): void {
        this.entityTypeHistoryModal.show({
            entityId: job.id.toString(),
            entityTypeFullName: this._entityTypeFullName,
            entityTypeDescription: ''
        });
    }

    clear() {
        this.filterText = '';
        this.minPanelOnFlatFilter = 0;
        this.maxPanelOnFlatFilter = 0;
        this.minPanelOnPitchedFilter = 0;
        this.maxPanelOnPitchedFilter = 0;
        this.regPlanNoFilter = '';
        this.lotNumberFilter = '';
        this.peakMeterNoFilter = '';
        this.offPeakMeterFilter = '';
        this.enoughMeterSpaceFilter = -1;
        this.isSystemOffPeakFilter = -1;
        this.suburbFilter = '';
        this.stateFilter = '';
        this.jobTypeNameFilter = '';
        this.jobStatusNameFilter = '';
        this.roofTypeNameFilter = '';
        this.elecDistributorNameFilter = '';
        this.leadCompanyNameFilter = '';
        this.elecRetailerNameFilter = '';
        this.paymentOptionNameFilter = '';
        this.depositOptionNameFilter = '';
        this.meterUpgradeNameFilter = '';
        this.meterPhaseNameFilter = '';
        this.promotionOfferNameFilter = '';
        this.houseTypeNameFilter = '';
        this.financeOptionNameFilter = '';
        this.salesRepId = 0;
        this.salesManagerId = 0;
        this.panelModel = '',
        this.invertModel = '',
        this.batteryModel = '',
        this.readytoactive = 0;
        this.teamId = 0,
        this.addressFilter = "",
        this.systemStrtRange = 0;
        this.systemEndRange = 0;
        this.leadSourceIdFilter = [];
        this.batteryFilter = 0;
        this.startDate = moment(this.date);
        this.endDate = moment(this.date);
        this.getJobs();
    }

    deleteJob(job: JobDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._jobsServiceProxy.delete(job.id)
                        .subscribe(() => {
                            this.reloadPage(null);
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    exportToExcel(excelorcsv): void {
        // if (this.sampleDateRange != null) {
        //     this.startDate = this.sampleDateRange[0];
        //     this.endDate = this.sampleDateRange[1];
        // } else {
        //     this.startDate = null;
        //     this.endDate = null;
        // }
        this.excelorcsvfile = excelorcsv;
        let filter = "";
        if(this.filterName == 'JobNumber' && this.filterText != undefined && this.filterText != null && this.filterText.trim() != ''){
            filter = this.orgCode + this.filterText.trim();
        }
        else {
            filter = this.filterText;
        }
        this.spinner.show();
        this._jobsServiceProxy.getJobGirdTrackerToExcel(
            this.filterName,
            filter,
            this.suburbFilter,
            this.stateFilter,
            this.jobTypeNameFilter,
            this.jobStatusNameFilter,
            this.roofTypeNameFilter,
            this.roofAngleNameFilter,
            this.elecDistributorNameFilter,
            this.leadCompanyNameFilter,
            this.elecRetailerNameFilter,
            this.paymentOptionNameFilter,
            this.depositOptionNameFilter,
            this.meterUpgradeNameFilter,
            this.meterPhaseNameFilter,
            this.promotionOfferNameFilter,
            this.houseTypeNameFilter,
            this.financeOptionNameFilter,
            this.organizationUnit,
            this.projectFilter,
            this.stateNameFilter,
            this.elecDistributorId,
            this.jobStatusIDFilter,
            this.applicationstatus,
            this.jobTypeId,
            this.startDate,
            this.endDate,
            undefined,
            undefined,
            undefined,
            this.salesManagerId == 0 ? undefined : this.salesManagerId,
            this.salesRepId == 0 ? undefined : this.salesRepId,
            this.jobDateFilter,
            this.panelModel,
            this.invertModel,
            this.readytoactive,
            this.systemStrtRange,
            this.systemEndRange,
            this.leadSourceIdFilter,
            undefined,
            undefined,
            undefined,
            undefined,
            this.teamId,
            this.addressFilter, undefined,
            this.excelorcsvfile,
            this.paymentid,
            this.areaNameFilter,
            undefined,
            this.postalcodefrom,
            this.postalcodeTo,
            this.solarvicstatus,
            this.batteryFilter,
            this.previousJobStatusId,
            this.batteryModel,
            this.jobTypeFilterList
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
                this.spinner.hide();
            });
    }
    expandGrid() {
        this.ExpandedView = true;
    }

    exportToExcelProductDetails(excelorcsv): void {
        this.excelorcsvfile = excelorcsv;
        let filter = "";
        if(this.filterName == 'JobNumber' && this.filterText != undefined && this.filterText != null && this.filterText.trim() != ''){
            filter = this.orgCode + this.filterText.trim();
        }
        else {
            filter = this.filterText;
        }
        this._jobsServiceProxy.getJobGirdTrackerProductToExcel(
            this.filterName,
            filter,
            this.suburbFilter,
            this.stateFilter,
            this.jobTypeNameFilter,
            this.jobStatusNameFilter,
            this.roofTypeNameFilter,
            this.roofAngleNameFilter,
            this.elecDistributorNameFilter,
            this.leadCompanyNameFilter,
            this.elecRetailerNameFilter,
            this.paymentOptionNameFilter,
            this.depositOptionNameFilter,
            this.meterUpgradeNameFilter,
            this.meterPhaseNameFilter,
            this.promotionOfferNameFilter,
            this.houseTypeNameFilter,
            this.financeOptionNameFilter,
            this.organizationUnit,
            this.projectFilter,
            this.stateNameFilter,
            this.elecDistributorId,
            this.jobStatusIDFilter,
            this.applicationstatus,
            this.jobTypeId,
            this.startDate,
            this.endDate,
            undefined,
            undefined,
            undefined,
            this.salesManagerId == 0 ? undefined : this.salesManagerId,
            this.salesRepId == 0 ? undefined : this.salesRepId,
            this.jobDateFilter,
            this.panelModel,
            this.invertModel,
            this.readytoactive,
            this.systemStrtRange,
            this.systemEndRange,
            this.leadSourceIdFilter,
            undefined,
            undefined,
            undefined,
            undefined,
            this.teamId,
            this.addressFilter, undefined,
            this.excelorcsvfile,
            this.paymentid,
            this.areaNameFilter,
            undefined,
            this.postalcodefrom,
            this.postalcodeTo,
            this.solarvicstatus,
            this.batteryFilter,
            this.previousJobStatusId,
            this.batteryModel,
            this.jobTypeFilterList
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    navigateToLeadDetail(leadid): void {
        this.ExpandedView = !this.ExpandedView;
        this.ExpandedView = false;
        this.SelectedLeadId = leadid;
        this.viewLeadDetail.showDetail(leadid, '', 8,0,'Jobs');
    }

    changeState() {
        if(this.organizationUnit == 8)
        {
            this.stateFilter = "VIC";
        }
        else{
            this.stateFilter = "";
        }
    }

    exportToExcelProductItemDetails(excelorcsv): void {
        this.excelorcsvfile = excelorcsv;
        let filter = "";
        if(this.filterName == 'JobNumber' && this.filterText != undefined && this.filterText != null && this.filterText.trim() != ''){
            filter = this.orgCode + this.filterText.trim();
        }
        else {
            filter = this.filterText;
        }
        this._jobsServiceProxy.getJobGirdTrackerProductItemToExcel(
            this.filterName,
            filter,
            this.suburbFilter,
            this.stateFilter,
            this.jobTypeNameFilter,
            this.jobStatusNameFilter,
            this.roofTypeNameFilter,
            this.roofAngleNameFilter,
            this.elecDistributorNameFilter,
            this.leadCompanyNameFilter,
            this.elecRetailerNameFilter,
            this.paymentOptionNameFilter,
            this.depositOptionNameFilter,
            this.meterUpgradeNameFilter,
            this.meterPhaseNameFilter,
            this.promotionOfferNameFilter,
            this.houseTypeNameFilter,
            this.financeOptionNameFilter,
            this.organizationUnit,
            this.projectFilter,
            this.stateNameFilter,
            this.elecDistributorId,
            this.jobStatusIDFilter,
            this.applicationstatus,
            this.jobTypeId,
            this.startDate,
            this.endDate,
            undefined,
            undefined,
            undefined,
            this.salesManagerId == 0 ? undefined : this.salesManagerId,
            this.salesRepId == 0 ? undefined : this.salesRepId,
            this.jobDateFilter,
            this.panelModel,
            this.invertModel,
            this.readytoactive,
            this.systemStrtRange,
            this.systemEndRange,
            this.leadSourceIdFilter,
            undefined,
            undefined,
            undefined,
            undefined,
            this.teamId,
            this.addressFilter, undefined,
            this.excelorcsvfile,
            this.paymentid,
            this.areaNameFilter,
            undefined,
            this.postalcodefrom,
            this.postalcodeTo,
            this.solarvicstatus,
            this.batteryFilter,
            this.previousJobStatusId,
            this.batteryModel,
            this.jobTypeFilterList
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Jobs';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Jobs';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }
    
}