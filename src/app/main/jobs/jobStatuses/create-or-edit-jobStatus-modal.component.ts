﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { JobStatusesServiceProxy, CreateOrEditJobStatusDto,UserActivityLogServiceProxy, UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';


@Component({
    selector: 'createOrEditJobStatusModal',
    templateUrl: './create-or-edit-jobStatus-modal.component.html'
})
export class CreateOrEditJobStatusModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    jobStatus: CreateOrEditJobStatusDto = new CreateOrEditJobStatusDto();

    constructor(
        injector: Injector,
        private _jobStatusesServiceProxy: JobStatusesServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
    }

    show(jobStatusId?: number): void {

        if (!jobStatusId) {
            this.jobStatus = new CreateOrEditJobStatusDto();
            this.jobStatus.id = jobStatusId;
            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Job Status';
            log.section = 'Job Status';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });

        } else {
            this._jobStatusesServiceProxy.getJobStatusForEdit(jobStatusId).subscribe(result => {
                this.jobStatus = result.jobStatus;
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Job Status : ' + this.jobStatus.name;
                log.section = 'Job Status';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
    }
    onShown(): void {
        
        document.getElementById('JobStatus_Name').focus();
    }
    save(): void {
            this.saving = true;
	
            this._jobStatusesServiceProxy.createOrEdit(this.jobStatus)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.jobStatus.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Job Status Updated : '+ this.jobStatus.name;
                    log.section = 'Job Status';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Job Status Created : '+ this.jobStatus.name;
                    log.section = 'Job Status';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
