﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetJobTypeForViewDto, JobTypeDto ,UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
@Component({
    selector: 'viewJobTypeModal',
    templateUrl: './view-jobType-modal.component.html'
})
export class ViewJobTypeModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetJobTypeForViewDto;


    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
        this.item = new GetJobTypeForViewDto();
        this.item.jobType = new JobTypeDto();
    }

    show(item: GetJobTypeForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Opened Job Type View : ' + item.jobType.name;
        log.section = 'Job Type';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {            
         }); 
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
