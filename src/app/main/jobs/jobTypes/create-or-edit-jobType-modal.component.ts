﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { JobTypesServiceProxy, CreateOrEditJobTypeDto,UserActivityLogServiceProxy, UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';

@Component({
    selector: 'createOrEditJobTypeModal',
    templateUrl: './create-or-edit-jobType-modal.component.html'
})
export class CreateOrEditJobTypeModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    jobType: CreateOrEditJobTypeDto = new CreateOrEditJobTypeDto();
    constructor(
        injector: Injector,
        private _jobTypesServiceProxy: JobTypesServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
    }

    show(jobTypeId?: number): void {

        if (!jobTypeId) {
            this.jobType = new CreateOrEditJobTypeDto();
            this.jobType.id = jobTypeId;

            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Job Type';
            log.section = 'Job Type';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._jobTypesServiceProxy.getJobTypeForEdit(jobTypeId).subscribe(result => {
                this.jobType = result.jobType;


                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Job Type : ' + this.jobType.name;
                log.section = 'Job Type';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        
    }
    
    onShown(): void {
        
        document.getElementById('JobType_Name').focus();
    }

    save(): void {
            this.saving = true;

			
            this._jobTypesServiceProxy.createOrEdit(this.jobType)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.jobType.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Job Type Updated : '+ this.jobType.name;
                    log.section = 'Job Type';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Job Type Created : '+ this.jobType.name;
                    log.section = 'Job Type';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }







    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
