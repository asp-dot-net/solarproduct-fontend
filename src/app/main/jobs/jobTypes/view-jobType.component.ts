﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { JobTypesServiceProxy, GetJobTypeForViewDto, JobTypeDto, UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { finalize } from 'rxjs/operators';
@Component({
    templateUrl: './view-jobType.component.html',
    animations: [appModuleAnimation()]
})
export class ViewJobTypeComponent extends AppComponentBase implements OnInit {

    active = false;
    saving = false;

    item: GetJobTypeForViewDto;


    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
         private _jobTypesServiceProxy: JobTypesServiceProxy,
         private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
        this.item = new GetJobTypeForViewDto();
        this.item.jobType = new JobTypeDto();        
    }

    ngOnInit(): void {
        this.show(this._activatedRoute.snapshot.queryParams['id']);
    }

    show(jobTypeId: number): void {
      this._jobTypesServiceProxy.getJobTypeForView(jobTypeId).subscribe(result => {      
                 this.item = result;
                this.active = true;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Opened Job Type View : ' + this.item.jobType.name;
        log.section = 'Job Type';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {            
         }); 
            });       
    }
}
