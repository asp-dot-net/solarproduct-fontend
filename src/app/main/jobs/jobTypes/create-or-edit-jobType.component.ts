﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { JobTypesServiceProxy, CreateOrEditJobTypeDto,UserActivityLogServiceProxy, UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import {Observable} from "@node_modules/rxjs";


@Component({
    templateUrl: './create-or-edit-jobType.component.html',
    animations: [appModuleAnimation()]
})
export class CreateOrEditJobTypeComponent extends AppComponentBase implements OnInit {
    active = false;
    saving = false;
    
    jobType: CreateOrEditJobTypeDto = new CreateOrEditJobTypeDto();
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,        
        private _jobTypesServiceProxy: JobTypesServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _router: Router
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.show(this._activatedRoute.snapshot.queryParams['id']);
    }

    show(jobTypeId?: number): void {

        if (!jobTypeId) {
            this.jobType = new CreateOrEditJobTypeDto();
            this.jobType.id = jobTypeId;
            this.active = true;
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Job Type';
            log.section = 'Job Type';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._jobTypesServiceProxy.getJobTypeForEdit(jobTypeId).subscribe(result => {
                this.jobType = result.jobType;
                this.active = true;
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Job Type : ' + this.jobType.name;
                log.section = 'Job Type';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        
    }

    private saveInternal(): Observable<void> {
            this.saving = true;
            
        
        return this._jobTypesServiceProxy.createOrEdit(this.jobType)
         .pipe(finalize(() => { 
            this.saving = false;               
            this.notify.info(this.l('SavedSuccessfully'));
            if(this.jobType.id){
                let log = new UserActivityLogDto();
                log.actionId = 82;
                log.actionNote ='Job Type Updated : '+ this.jobType.name;
                log.section = 'Job Type';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }else{
                let log = new UserActivityLogDto();
                log.actionId = 81;
                log.actionNote ='Job Type Created : '+ this.jobType.name;
                log.section = 'Job Type';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }
         }));
    }
    
    save(): void {
        this.saveInternal().subscribe(x => {
             this._router.navigate( ['/app/main/jobs/jobTypes']);
        })
    }
    
    saveAndNew(): void {
        this.saveInternal().subscribe(x => {
            this.jobType = new CreateOrEditJobTypeDto();
        })
    }







}
