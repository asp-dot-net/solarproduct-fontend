﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { PromotionMastersServiceProxy, CreateOrEditPromotionMasterDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
@Component({
    selector: 'createOrEditPromotionMasterModal',
    templateUrl: './create-or-edit-promotionMaster-modal.component.html'
})
export class CreateOrEditPromotionMasterModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    promotionMaster: CreateOrEditPromotionMasterDto = new CreateOrEditPromotionMasterDto();

    constructor(
        injector: Injector,
        private _promotionMastersServiceProxy: PromotionMastersServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }

    show(promotionMasterId?: number): void {

        if (!promotionMasterId) {
            this.promotionMaster = new CreateOrEditPromotionMasterDto();
            this.promotionMaster.id = promotionMasterId;

            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Promotion Type';
            log.section = 'Promotion Type';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._promotionMastersServiceProxy.getPromotionMasterForEdit(promotionMasterId).subscribe(result => {
                this.promotionMaster = result.promotionMaster;


                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Promotion Type : ' + this.promotionMaster.name;
                log.section = 'Promotion Type';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        
    }

    onShown(): void {
        
        document.getElementById('PromotionMaster_Name').focus();
    }

    save(): void {
        
            this.saving = true;

			
            this._promotionMastersServiceProxy.createOrEdit(this.promotionMaster)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                
 if(this.promotionMaster.id){
    let log = new UserActivityLogDto();
    log.actionId = 82;
    log.actionNote ='Promotion Type Updated : '+ this.promotionMaster.name;
    log.section = 'Promotion Type';
    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {
    }); 
}else{
    let log = new UserActivityLogDto();
    log.actionId = 81;
    log.actionNote ='Promotion Type Created : '+ this.promotionMaster.name;
    log.section = 'Promotion Type';
    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {
    }); 
}
             });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
