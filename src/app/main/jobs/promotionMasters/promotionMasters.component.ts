﻿import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PromotionMastersServiceProxy, PromotionMasterDto ,UserActivityLogServiceProxy, UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { CreateOrEditPromotionMasterModalComponent } from './create-or-edit-promotionMaster-modal.component';
import { finalize } from 'rxjs/operators';
import { ViewPromotionMasterModalComponent } from './view-promotionMaster-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { Title } from '@angular/platform-browser';

@Component({
    templateUrl: './promotionMasters.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class PromotionMastersComponent extends AppComponentBase {

    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    @ViewChild('createOrEditPromotionMasterModal', { static: true }) createOrEditPromotionMasterModal: CreateOrEditPromotionMasterModalComponent;
    @ViewChild('viewPromotionMasterModalComponent', { static: true }) viewPromotionMasterModal: ViewPromotionMasterModalComponent;

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    nameFilter = '';
    firstrowcount = 0;
    last = 0;
    public screenHeight: any; 
    constructor(
        injector: Injector,
        private _promotionMastersServiceProxy: PromotionMastersServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private titleService: Title
        ) {
            super(injector);
            this.titleService.setTitle(this.appSession.tenancyName + " |  Promotion Type");
    }
    searchLog() : void {
        
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Promotion Type';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
       
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Promotion Type';
        log.section = 'Promotion Type';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
    }
    getPromotionMasters(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._promotionMastersServiceProxy.getAll(
            this.filterText,
            this.nameFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createPromotionMaster(): void {
        this.createOrEditPromotionMasterModal.show();
    }


    deletePromotionMaster(promotionMaster: PromotionMasterDto): void {
        this.message.confirm(
            this.l('AreYouSureWanttoDelete'),
            this.l('Delete'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._promotionMastersServiceProxy.delete(promotionMaster.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete Promotion Type: ' + promotionMaster.name;
                            log.section = 'Promotion Type';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });
                        });
                }
            }
        );
    }

    exportToExcel(): void {
        this._promotionMastersServiceProxy.getPromotionMastersToExcel(
            this.filterText,
            this.nameFilter,
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }
}
