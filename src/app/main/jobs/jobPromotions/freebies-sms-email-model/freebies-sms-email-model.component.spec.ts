import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FreebiesSmsEmailModelComponent } from './freebies-sms-email-model.component';

describe('FreebiesSmsEmailModelComponent', () => {
  let component: FreebiesSmsEmailModelComponent;
  let fixture: ComponentFixture<FreebiesSmsEmailModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FreebiesSmsEmailModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FreebiesSmsEmailModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
