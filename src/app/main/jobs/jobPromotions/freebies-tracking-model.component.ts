import { Component, ElementRef, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { JobPromotionsServiceProxy, GetJobPromotionForEditOutput,CommonLookupDto,CommonLookupServiceProxy, CreateOrEditJobPromotionDto, JobPromotionPromotionMasterLookupTableDto, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import * as _ from 'lodash';
import * as moment from 'moment';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'freebiesTrackingModal',
    templateUrl: './freebies-tracking-model.component.html'
})
export class FreebiesTrackingModalComponent extends AppComponentBase {

    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    saving = false;
    IsShowVoucher=false;
    TrackingNumber = "";
    Description = "";
    item: GetJobPromotionForEditOutput;
    comment = "";
    dispatchedDate: moment.Moment;
    freebieTransport: JobPromotionPromotionMasterLookupTableDto[];
    voucherList : CommonLookupDto[]
    sectionId =0


    constructor(
        injector: Injector,
        private _jobPromotionsServiceProxy: JobPromotionsServiceProxy,
        private spinner: NgxSpinnerService,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _router: Router, 
         private _commonLookupService: CommonLookupServiceProxy,
    ) {
        super(injector);
        this.item = new GetJobPromotionForEditOutput();
        this.item.jobPromotion = new CreateOrEditJobPromotionDto();
    }

    sectionName = '';
    show(jobid: number,sectionIds:number,section = '',isVoucherShow: boolean): void {
        debugger
        this.spinner.show();
        this.sectionName = section;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Open Add Tracking Number';
        log.section = section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
        if(isVoucherShow= true){
            this.IsShowVoucher=true;
        }
        if (this.freebieTransport == null) {
            this._jobPromotionsServiceProxy.getAllFreebieTransportRepositoryForTableDropdown().subscribe(result => {
                this.freebieTransport = result;
            });
        }
        this._commonLookupService.getAllVoucher().subscribe(result => {
            this.voucherList = result;
        })
        this.sectionId = sectionIds;
        this._jobPromotionsServiceProxy.getJobPromotionForEdit(jobid).subscribe(result => {
            this.item = result;
            this.dispatchedDate = this.item.jobPromotion.dispatchedDate;
            this.modal.show();
            this.spinner.hide();
        });
    }

    save(): void {
        this.saving = true;
        this.item.jobPromotion.dispatchedDate = this.dispatchedDate;
        this.item.jobPromotion.sectionId = this.sectionId;

        this._jobPromotionsServiceProxy.createOrEdit(this.item.jobPromotion)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                let log = new UserActivityLogDto();
                log.actionId = 19;
                log.actionNote ='Job Promotion Sent With Tracking No ' + this.item.jobPromotion.trackingNumber;
                log.section = this.sectionName;
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
                this.modal.hide();
                this.modalSave.emit(null);
                this.notify.info(this.l('SavedSuccessfully'));
                this.comment = "";
            });
    }

    close(): void {
        this.modal.hide();
    }

}