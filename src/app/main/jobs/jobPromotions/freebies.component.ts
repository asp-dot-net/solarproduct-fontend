﻿import { Component, Injector, ViewEncapsulation, ViewChild, OnInit, HostListener } from '@angular/core';
import { JobPromotionsServiceProxy, JobPromotionDto, OrganizationUnitDto, UserServiceProxy, JobPromotionPromotionMasterLookupTableDto, JobsServiceProxy, LeadsServiceProxy, LeadStateLookupTableDto, CommonLookupServiceProxy, JobTrackerServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import * as _ from 'lodash';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { FreebiesTrackingModalComponent } from './freebies-tracking-model.component';
import { ViewApplicationModelComponent } from '../jobs/view-application-model/view-application-model.component';
import * as moment from 'moment';
// import { AddActivityModalComponent } from '@app/main/myleads/myleads/add-activity-model.component';
// import { FreebiesSmsEmailModelComponent } from './freebies-sms-email-model/freebies-sms-email-model.component';
// import { ViewMyLeadComponent } from '@app/main/myleads/myleads/view-mylead.component';
import { Title } from '@angular/platform-browser';
import { SMSModelComponent } from '@app/main/activitylog/sms-modal.component';
import { EmailModelComponent } from '@app/main/activitylog/email-modal.component';
import { CommentModelComponent } from '@app/main/activitylog/comment-modal.component';
import { NotifyModelComponent } from '@app/main/activitylog/notify-modal.component';
import { ReminderModalComponent } from '@app/main/activitylog/reminder-modal.component';
import { ToDoModalComponent } from '@app/main/activitylog/todo-modal.component';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './freebies.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class FreebiesComponent extends AppComponentBase implements OnInit {
    FiltersData = false;
    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    organizationUnitlength: number = 0;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    @ViewChild('freebiesTrackingModal', { static: true }) freebiesTrackingModal: FreebiesTrackingModalComponent;
    @ViewChild('viewApplicationModal', { static: true }) viewApplicationModal: ViewApplicationModelComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    // @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    // @ViewChild('freebiesTrackerSmsEmailModel', { static: true }) freebiesTrackerSmsEmailModel: FreebiesSmsEmailModelComponent;
    // @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
    @ViewChild('smsModel', { static: true }) smsModel: SMSModelComponent;
    @ViewChild('emailModel', { static: true }) emailModel: EmailModelComponent;
    @ViewChild('notifyModel', { static: true }) notifyModel: NotifyModelComponent;
    @ViewChild('commentModel', { static: true }) commentModel: CommentModelComponent;
    @ViewChild('todoModal', { static: true }) todoModal: ToDoModalComponent;
    @ViewChild('ReminderModal', { static: true }) ReminderModal: ReminderModalComponent;
    advancedFiltersAreShown = false;
    filterText = '';
    projectNumber = '';
    companyNameFilter = '';
    mobileNo = '';
    email = '';
    excelorcsvfile = 0;
    address = '';
    promoType: number = 0;
    jobPromotionMaster: JobPromotionPromotionMasterLookupTableDto[];
    freebiestransportMaster: JobPromotionPromotionMasterLookupTableDto[];
    promotionMasterNameFilter = '';
    organizationUnit = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    applicationstatus = 1;
    
    total = 0;
    totalAwaiting = 0;
    totalPending = 0;
    totalSent = 0;
    
    freebieTransportId = 0;
    // date = '01/01/2021'
    // public sampleDateRange: moment.Moment[] = [moment(this.date), moment().endOf('day')];
    // startDateFilter: moment.Moment;
    // endDateFilter: moment.Moment;
    date = new Date();
    // firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    // startDate = moment(this.firstDay);
    // endDate = moment().endOf('day');

    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);

    allStates: LeadStateLookupTableDto[];
    stateNameFilter = '';
    ExpandedView: boolean = true;
    SelectedLeadId: number = 0;
    firstrowcount = 0;
    last = 0;

    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 330;
    toggle: boolean = true;
    filterName = "JobNumber";
    orgCode = '';
    change() {
        this.toggle = !this.toggle;
      }
    
    constructor(
        injector: Injector,
        private _commonLookupService: CommonLookupServiceProxy,
        private _jobPromotionsServiceProxy: JobPromotionsServiceProxy,
        private _userServiceProxy: UserServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _jobsServiceProxy: JobsServiceProxy, private _leadsServiceProxy: LeadsServiceProxy,
        private titleService: Title,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _jobTrackerServiceProxy: JobTrackerServiceProxy
        ) {
            super(injector);
            this.titleService.setTitle(this.appSession.tenancyName + " |  Freebies Tracker");
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        this._jobPromotionsServiceProxy.getAllPromotionMasterForTableDropdown().subscribe(result => {
            this.jobPromotionMaster = result;
        });

        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });

        this._jobPromotionsServiceProxy.getAllFreebieTransportRepositoryForTableDropdown().subscribe(result => {
            this.freebiestransportMaster = result;
        });

        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Freebies Tracker';
            log.section = 'Freebies Tracker';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.orgCode = this.allOrganizationUnits[0].code;
            this.getJobPromotions();
        });
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }

        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }

    addTrackingNumber(jobpromotionid,PromotionName): void {
        if(PromotionName=="Holiday Voucher"){
            this.freebiesTrackingModal.show(jobpromotionid,2,'Freebies Tracker',true);
        }
        else{
            this.freebiesTrackingModal.show(jobpromotionid,2,'Freebies Tracker',false);
        }
        
    }
    changeOrgCode() {
        this.orgCode = this.allOrganizationUnits.find(x => x.id == this.organizationUnit).code;
    }
    getJobPromotions(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        // this._jobPromotionsServiceProxy.getAll(
        //     this.filterText,
        //     this.projectNumber,
        //     this.companyNameFilter,
        //     this.mobileNo,
        //     this.email,
        //     this.address,
        //     this.promoType,
        //     this.organizationUnit,
        //     this.applicationstatus,
        //     this.freebieTransportId,
        //     undefined,
        //     undefined,
        //     this.startDate,
        //     this.endDate,
        //     this.stateNameFilter,
        //     this.primengTableHelper.getSorting(this.dataTable),
        //     this.primengTableHelper.getSkipCount(this.paginator, event),
        //     this.primengTableHelper.getMaxResultCount(this.paginator, event)
        // ).subscribe(result => {
        //     this.primengTableHelper.totalRecordsCount = result.totalCount;
        //     this.primengTableHelper.records = result.items;
        //     const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
        //     this.firstrowcount =  totalrows + 1;
        //     this.last = totalrows + result.items.length;
        //     this.primengTableHelper.hideLoadingIndicator();
        //     this.shouldShow = false;
        // });
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._jobTrackerServiceProxy.getAllFreebiesTracker(
            this.filterName,
            filterText_, 
            this.organizationUnit, 
            this.applicationstatus,
            this.promoType, 
            this.stateNameFilter, 
            this.freebieTransportId,
            this.startDate,
            this.endDate,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            console.log(result.items);
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            this.shouldShow = false;
            if (result.totalCount > 0) {
                this.total = result.items[0].summary.total;
                this.totalAwaiting = result.items[0].summary.awaiting;
                this.totalPending = result.items[0].summary.pending;
                this.totalSent = result.items[0].summary.sent;
            }
            else
            {
                this.total = 0;
                this.totalAwaiting = 0;
                this.totalPending = 0;
                this.totalSent = 0;
            }
        });
    }

    // getCount(organizationUnit: number): void {
    //     this._jobsServiceProxy.getAllApplicationTrackerCount(organizationUnit).subscribe(result => {
    //         this.totalfreebieData = result.totalfreebieData;
    //         this.totalfreebieVerifyData = result.totalfreebieVerifyData;
    //         this.totalfreebienotVerifyData = result.totalfreebienotVerifyData;
    //         this.totalAwaitingData = result.totalAwaitingData;
    //     });

    // }
    reloadPage(event): void {
        this.ExpandedView = true;
        this.paginator.changePage(this.paginator.getPage());
    }

    clear() {
        this.filterText = '';
        this.companyNameFilter = '';
        this.projectNumber = '';
        this.projectNumber = '';
        this.mobileNo = '';
        this.email = '';
        this.promoType = 0;
        this.freebieTransportId = 0;
        this.promotionMasterNameFilter = '0';
        this.stateNameFilter = '';
        this.startDate = moment(this.date);
        this.endDate = moment(this.date);
        this.getJobPromotions();
    }
    deleteJobPromotions(jobPromotion: JobPromotionDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._jobPromotionsServiceProxy.delete(jobPromotion.id)
                        .subscribe(() => {
                            this.reloadPage(null);
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    exportToExcel(excelorcsv): void {
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        // this.excelorcsvfile = excelorcsv;
        // this._jobPromotionsServiceProxy.getJobPromotionsToExcel(
        //     this.filterText,
        //     filterText_,
        //     this.companyNameFilter,
        //     this.mobileNo,
        //     this.email,
        //     this.address,
        //     this.promoType,
        //     this.organizationUnit,
        //     this.applicationstatus,
        //     this.freebieTransportId,
        //     undefined,
        //     undefined,
        //     this.startDate,
        //     this.endDate,
        //     this.excelorcsvfile,
        //     this.stateNameFilter
        // )
        //     .subscribe(result => {
        //         this._fileDownloadService.downloadTempFile(result);
        //     });
        this._jobTrackerServiceProxy.getAllFreebiesTrackerExcel(
            this.filterName,
            filterText_, 
            this.organizationUnit, 
            this.applicationstatus,
            this.promoType, 
            this.stateNameFilter, 
            this.freebieTransportId,
            this.startDate,
            this.endDate,
            excelorcsv 
        ).subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
        })
    }

    quickview(jobid): void {
        this.viewApplicationModal.show(jobid);
    }


    expandGrid() {
        this.ExpandedView = true;
    }

    navigateToLeadDetail(leadid): void {
        // this.ExpandedView = !this.ExpandedView;
        // this.ExpandedView = false;
        // this.SelectedLeadId = leadid;
        // this.viewLeadDetail.showDetail(leadid,null,2);
    }
    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Freebies Tracker';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Freebies Tracker';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }
}
