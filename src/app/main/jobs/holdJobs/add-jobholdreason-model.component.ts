import { Component, ElementRef, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { JobsServiceProxy, GetJobForEditOutput, CreateOrEditJobDto, JobElecDistributorLookupTableDto, JobElecRetailerLookupTableDto, JobRoofAngleLookupTableDto, CommonLookupDto, LeadsServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import * as _ from 'lodash';
import * as moment from 'moment';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';
import { FileItem, FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { FileUpload } from 'primeng';
import { AppConsts } from '@shared/AppConsts';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
@Component({
    selector: 'jobholdreasonModal',
    templateUrl: './add-jobholdreason-model.component.html'
})
export class JobHoldReasonModalComponent extends AppComponentBase {

    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    leadCompanyName: any;
    saving = false;
    item: GetJobForEditOutput;
    holdReasons: CommonLookupDto[];
    jobresonnotes="";
    jobresonid = 0;
    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _jobsServiceProxy: JobsServiceProxy,
    ) {
        super(injector);
        this.item = new GetJobForEditOutput();
        this.item.job = new CreateOrEditJobDto();
    }

    sectionName = '';
    show(jobid: number,section = ''): void {
        debugger;
        this.sectionName =section;
        let log = new UserActivityLogDto();
             log.actionId = 79;
             log.actionNote ='Open Add Hold Job Reason';
             log.section = section;
             this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                 .subscribe(() => {
             });
        if (this.holdReasons == null) {
            this._jobsServiceProxy.getAllHoldReasonForTableDropdown().subscribe(result => {
                this.holdReasons = result;
            });
        }
        this._jobsServiceProxy.getJobForEdit(jobid).subscribe(result => {
            this.item.job = result.job;
            this.jobresonnotes = this.item.job.jobHoldReason;
            this.jobresonid = this.item.job.jobHoldReasonId;
            this.leadCompanyName = result.leadCompanyName;
            this.modal.show();
        });
    }

    save(): void {
        debugger;
        this.saving = true;
        this.item.job.jobHoldReason =  this.jobresonnotes;
        this.item.job.jobHoldReasonId = this.jobresonid;
        this.item.job.section = this.sectionName;
        this._jobsServiceProxy.createOrEdit(this.item.job)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                
                this.close();
                this.modalSave.emit(null);
                this.notify.info(this.l('SavedSuccessfully'));
            });
    }

    close(): void {
        this.modal.hide();
    }

}