import { Component, ElementRef, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLookupDto, EmailTemplateServiceProxy, GetLeadForViewDto, JobsServiceProxy, LeadDto, LeadsServiceProxy, SmsEmailDto, SmsTemplatesServiceProxy } from '@shared/service-proxies/service-proxies';
import { EmailEditorComponent } from 'angular-email-editor';
import * as _ from 'lodash';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'holdjobsTrackerSmsEmailModal',
  templateUrl: './holdjobs-sms-email-model.component.html',
})
export class HoldjobsSmsEmailModelComponent extends AppComponentBase implements OnInit {
  @ViewChild("myNameElem") myNameElem: ElementRef;
  @ViewChild('addModal', { static: true }) modal: ModalDirective;
  ///@ViewChild('viewLeadDetail', {static: true}) viewLeadDetail: ViewMyLeadComponent;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  saving = false;
  id = 0;
  allEmailTemplates: CommonLookupDto[];
  allSMSTemplates: CommonLookupDto[];
  emailData = '';
  smsData = '';
  emailTemplate: number;
  uploadUrl: string;
  uploadedFiles: any[] = [];
  myDate = new Date();
  activityType: number;
  item: GetLeadForViewDto;
  activityLog: SmsEmailDto = new SmsEmailDto();
  activityName = "";
  total = 0;
  credit = 0;
  customeTagsId = 0;
  role: string = '';
  leadCompanyName: any;
  subject = '';
  emailfrom = "";
  fromemails: CommonLookupDto[];
  criteriaavaibaleornot = false;
  ccbox = false;
  bccbox = false;
  // viewLeadDetail: ViewMyLeadComponent;
  constructor(
    injector: Injector,
    private _leadsServiceProxy: LeadsServiceProxy,
    private _emailTemplateServiceProxy: EmailTemplateServiceProxy,
    private _smsTemplateServiceProxy: SmsTemplatesServiceProxy,
    private _router: Router,
    private spinner: NgxSpinnerService,
    private _jobsServiceProxy: JobsServiceProxy,
  ) {
    super(injector);
    this.item = new GetLeadForViewDto();
    this.item.lead = new LeadDto();
  }

  @ViewChild(EmailEditorComponent)
  private emailEditor: EmailEditorComponent;

  ///private viewLeadDetail: ViewMyLeadComponent;

  ngOnInit(): void {
    this.activityLog.emailTemplateId = 0;
    this.activityLog.smsTemplateId = 0;
    this.activityLog.customeTagsId = 0;
    this.activityLog.subject = '';

  }

  show(leadId: number, id: number, trackerid?: number): void {

    this.activityLog.emailTemplateId = 0;
    this.activityLog.smsTemplateId = 0;
    this.activityLog.customeTagsId = 0;
    this.activityLog.subject = '';
    this.activityLog.body = "";
    this.activityLog.trackerId = trackerid;
    if (id == 1) {
      this.id = id;
      this.activityType = 1;
      this._leadsServiceProxy.checkorgwisephonedynamicavaibleornot(leadId).subscribe(result => {
        this.criteriaavaibaleornot = result;
      });
    }
    else if (id == 2) {
      this.id = id;
      this.activityType = 2;
      this._leadsServiceProxy.getOrgWiseDefultandownemailadd(leadId).subscribe(result => {
        debugger;
        this.fromemails = result;
        this.activityLog.emailFrom = this.fromemails[0].displayName;
      });
    }
    this.spinner.show();
    this._leadsServiceProxy.getLeadForView(leadId, 0).subscribe(result => {
      this.item = result;
      this.leadCompanyName = result.lead.companyName;
      this.selection();
      if (!this.criteriaavaibaleornot && id == 1) {
        this.message.error("authentication issue");
      } else {
        this.modal.show();
        this.spinner.hide();
      }
    });
    this._leadsServiceProxy.getallEmailTemplates(leadId).subscribe(result => {
      this.allEmailTemplates = result;
    });

    this._leadsServiceProxy.getallSMSTemplates(leadId).subscribe(result => {
      this.allSMSTemplates = result;
    });

    this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
      this.role = result;
    });

  }

  selection(): void {

    this.activityLog.body = '';
    this.activityLog.emailTemplateId = 0;
    this.activityLog.customeTagsId = 0;
    if (this.activityType == 1) {
      this.activityName = "SMS To ";
    }
    else if (this.activityType == 2) {
      this.activityName = "Email To ";
    }
  }


  save(): void {

    this.activityLog.leadId = this.item.lead.id;
    if (this.activityType == 1) {
      this.saving = true;
      if (this.role != 'Admin') {
        if (this.total > 320) {
          this.notify.warn(this.l('You Can Not Add more than 320 characters'));
          this.saving = false;
        } else {
          this._jobsServiceProxy.sendSms(this.activityLog)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
              this.modal.hide();
              this.notify.info(this.l('SmsSendSuccessfully'));
              this.modalSave.emit(null);
              this.activityLog.body = "";
              this.activityLog.emailTemplateId = 0;
              this.activityLog.smsTemplateId = 0;
              this.activityLog.customeTagsId = 0;
              this.activityLog.subject = '';
            });
        }
      }
      else {
        this._jobsServiceProxy.sendSms(this.activityLog)
          .pipe(finalize(() => { this.saving = false; }))
          .subscribe(() => {
            this.modal.hide();
            this.notify.info(this.l('SmsSendSuccessfully'));
            this.modalSave.emit(null);
            this.activityLog.body = "";
            this.activityLog.emailTemplateId = 0;
            this.activityLog.smsTemplateId = 0;
            this.activityLog.customeTagsId = 0;
            this.activityLog.subject = '';
          });
      }
    }
    else {
      this.saving = true;
      this._jobsServiceProxy.sendEmail(this.activityLog)
        .pipe(finalize(() => { this.saving = false; }))
        .subscribe(() => {
          this.notify.info(this.l('EmailSendSuccessfully'));
          this.modal.hide();
          this.modalSave.emit(null);
          this.activityLog.body = "";
          this.activityLog.emailTemplateId = 0;
          this.activityLog.smsTemplateId = 0;
          this.activityLog.customeTagsId = 0;
          this.activityLog.subject = '';
        });
    }
  }

  countCharcters(): void {

    if (this.role != 'Admin') {
      this.total = this.activityLog.body.length;
      this.credit = Math.ceil(this.total / 160);
      if (this.total > 320) {
        this.notify.warn(this.l('You Can Not Add more than 320 characters'));
      }
    }
    else {
      this.total = this.activityLog.body.length;
      this.credit = Math.ceil(this.total / 160);
    }


  }

  close(): void {
    this.modal.hide();
  }

  onTagChange(event): void {

    const id = event.target.value;
    if (id == 1) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Customer.Name}}";
      } else {
        this.activityLog.body = "{{Customer.Name}}";
      }

    } else if (id == 2) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Customer.Mobile}}";
      } else {
        this.activityLog.body = "{{Customer.Mobile}}";
      }
    } else if (id == 3) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Customer.Phone}}";
      } else {
        this.activityLog.body = "{{Customer.Phone}}";
      }
    } else if (id == 4) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Customer.Email}}";
      } else {
        this.activityLog.body = "{{Customer.Email}}";
      }
    } else if (id == 5) {

      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Customer.Address}}";
      } else {
        this.activityLog.body = "{{Customer.Address}}";
      }
    } else if (id == 6) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Sales.Name}}";
      } else {
        this.activityLog.body = "{{Sales.Name}}";
      }
    } else if (id == 7) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Sales.Mobile}}";
      } else {
        this.activityLog.body = "{{Sales.Mobile}}";
      }
    } else if (id == 8) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Sales.Email}}";
      } else {
        this.activityLog.body = "{{Sales.Email}}";
      }
    }
    else if (id == 9) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Quote.ProjectNo}}";
      } else {
        this.activityLog.body = "{{Quote.ProjectNo}}";
      }
    }
    else if (id == 10) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Quote.SystemCapacity}}";
      } else {
        this.activityLog.body = "{{Quote.SystemCapacity}}";
      }
    }
    else if (id == 11) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Quote.AllproductItem}}";
      } else {
        this.activityLog.body = "{{Quote.AllproductItem}}";
      }
    }
    else if (id == 12) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Quote.TotalQuoteprice}}";
      } else {
        this.activityLog.body = "{{Quote.TotalQuoteprice}}";
      }
    }
    else if (id == 13) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Quote.InstallationDate}}";
      } else {
        this.activityLog.body = "{{Quote.InstallationDate}}";
      }
    }
    else if (id == 14) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Quote.InstallerName}}";
      } else {
        this.activityLog.body = "{{Quote.InstallerName}}";
      }
    }
    else if (id == 15) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Freebies.TransportCompany}}";
      } else {
        this.activityLog.body = "{{Freebies.TransportCompany}}";
      }
    }
    else if (id == 16) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Freebies.TransportLink}}";
      } else {
        this.activityLog.body = "{{Freebies.TransportLink}}";
      }
    }
    else if (id == 17) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Freebies.DispatchedDate}}";
      } else {
        this.activityLog.body = "{{Freebies.DispatchedDate}}";
      }
    }
    else if (id == 18) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Freebies.TrackingNo}}";
      } else {
        this.activityLog.body = "{{Freebies.TrackingNo}}";
      }
    }
    else if (id == 19) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Freebies.PromoType}}";
      } else {
        this.activityLog.body = "{{Freebies.PromoType}}";
      }
    }
    else if (id == 20) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Invoice.UserName}}";
      } else {
        this.activityLog.body = "{{Invoice.UserName}}";
      }
    }
    else if (id == 21) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Invoice.UserMobile}}";
      } else {
        this.activityLog.body = "{{Invoice.UserMobile}}";
      }
    }
    else if (id == 22) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Invoice.UserEmail}}";
      } else {
        this.activityLog.body = "{{Invoice.UserEmail}}";
      }
    }
    else if (id == 23) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Invoice.Owning}}";
      } else {
        this.activityLog.body = "{{Invoice.Owning}}";
      }
    }
    else if (id == 24) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Organization.orgName}}";
      } else {
        this.activityLog.body = "{{Organization.orgName}}";
      }
    }
    else if (id == 25) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Organization.orgEmail}}";
      } else {
        this.activityLog.body = "{{Organization.orgEmail}}";
      }
    }
    else if (id == 26) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Organization.orgMobile}}";
      } else {
        this.activityLog.body = "{{Organization.orgMobile}}";
      }
    }
    else if (id == 27) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Organization.orglogo}}";
      } else {
        this.activityLog.body = "{{Organization.orglogo}}";
      }
    }
    else if (id == 28) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Review.link}}";
      } else {
        this.activityLog.body = "{{Review.link}}";
      }
    }
  }


  saveDesign() {
    if (this.activityLog.emailTemplateId == 0) {
      this.saving = true;
      const emailHTML = this.activityLog.body;
      this.setHTML(emailHTML)
    }
    else {
      this.saving = true;
      this.emailEditor.editor.exportHtml((data) =>
        this.setHTML(data.html)
      );
    }
  }

  setHTML(emailHTML) {
    let htmlTemplate = this.getEmailTemplate(emailHTML);
    this.activityLog.body = htmlTemplate;
    this.save();
  }

  getEmailTemplate(emailHTML) {
    //let myTemplateStr = "Hello {{user.name}}, you are {{user.age.years}} years old";
    let myTemplateStr = emailHTML;
    let addressValue = this.item.lead.address + " " + this.item.lead.suburb + ", " + this.item.lead.state + "-" + this.item.lead.postCode;
    let myVariables = {
      Customer: {
        Name: this.item.lead.companyName,
        Address: addressValue,
        Mobile: this.item.lead.mobile,
        Email: this.item.lead.email,
        Phone: this.item.lead.phone,
        SalesRep: this.item.currentAssignUserName
      },
      Sales: {
        Name: this.item.currentAssignUserName,
        Mobile: this.item.currentAssignUserMobile,
        Email: this.item.currentAssignUserEmail
      },
      Quote: {
        ProjectNo: this.item.jobNumber,
        SystemCapacity: this.item.systemCapacity != null ? this.item.systemCapacity + " " + 'KW' : this.item.systemCapacity,
        AllproductItem: this.item.qunityAndModelList,
        TotalQuoteprice: this.item.totalQuotaion,
        InstallationDate: this.item.installationDate,
        InstallerName: this.item.installerName,
      },
      Freebies: {
        DispatchedDate: this.item.dispatchedDate,
        TrackingNo: this.item.trackingNo,
        TransportCompany: this.item.transportCompanyName,
        TransportLink: this.item.transportLink,
        PromoType: this.item.freebiesPromoType,
      },
      Invoice: {
        UserName: this.item.userName,
        UserMobile: this.item.userPhone,
        UserEmail: this.item.userEmail,
        Owning: this.item.owning,
      },
      Organization: {
        orgName: this.item.orgName,
        orgEmail: this.item.orgEmail,
        orgMobile: this.item.orgMobile,
        orglogo: AppConsts.docUrl + "/" + this.item.orgLogo,
      },
      Review: {
       link: this.item.reviewlink,
      },
    }

    // use custom delimiter {{ }}
    _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;

    // interpolate
    let compiled = _.template(myTemplateStr);
    let myTemplateCompiled = compiled(myVariables);
    return myTemplateCompiled;
  }

  editorLoaded() {

    this.activityLog.body = "";
    if ((this.activityLog.emailTemplateId != 0 && this.activityLog.emailTemplateId !== null && this.activityLog.emailTemplateId !== undefined)) {
      this._jobsServiceProxy.getEmailTemplateForEditForEmail(this.activityLog.emailTemplateId).subscribe(result => {
        this.activityLog.subject = result.emailTemplate.subject;
        this.emailData = result.emailTemplate.body;
        if (this.emailData != "") {
          this.emailData = this.getEmailTemplate(this.emailData);
          this.emailEditor.editor.loadDesign(JSON.parse(this.emailData));
        }
      });
    }

  }

  smseditorLoaded() {

    this.activityLog.body = "";
    if ((this.activityLog.smsTemplateId != 0 && this.activityLog.smsTemplateId !== null && this.activityLog.smsTemplateId !== undefined)) {
      this._jobsServiceProxy.getSmsTemplateForEditForSms(this.activityLog.smsTemplateId).subscribe(result => {
        this.smsData = result.smsTemplate.text;
        if (this.smsData != "") {
          this.setsmsHTML(this.smsData)
        }
      });
    }

  }


  setsmsHTML(smsHTML) {
    let htmlTemplate = this.getsmsTemplate(smsHTML);
    this.activityLog.body = htmlTemplate;
    this.countCharcters();
  }

  getsmsTemplate(smsHTML) {
    //let myTemplateStr = "Hello {{user.name}}, you are {{user.age.years}} years old";
    let myTemplateStr = smsHTML;
    let addressValue = this.item.lead.address + " " + this.item.lead.suburb + ", " + this.item.lead.state + "-" + this.item.lead.postCode;
    let myVariables = {
      Customer: {
        Name: this.item.lead.companyName,
        Address: addressValue,
        Mobile: this.item.lead.mobile,
        Email: this.item.lead.email,
        Phone: this.item.lead.phone,
        SalesRep: this.item.currentAssignUserName
      },
      Sales: {
        Name: this.item.currentAssignUserName,
        Mobile: this.item.currentAssignUserMobile,
        Email: this.item.currentAssignUserEmail
      },
      Quote: {
        ProjectNo: this.item.jobNumber,
        SystemCapacity: this.item.systemCapacity != null ? this.item.systemCapacity + " " + 'KW' : this.item.systemCapacity,
        AllproductItem: this.item.qunityAndModelList,
        TotalQuoteprice: this.item.totalQuotaion,
        InstallationDate: this.item.installationDate,
        InstallerName: this.item.installerName,
      },
      Freebies: {
        DispatchedDate: this.item.dispatchedDate,
        TrackingNo: this.item.trackingNo,
        TransportCompany: this.item.transportCompanyName,
        TransportLink: this.item.transportLink,
        PromoType: this.item.freebiesPromoType,
      },
      Invoice: {
        UserName: this.item.userName,
        UserMobile: this.item.userPhone,
        UserEmail: this.item.userEmail,
        Owning: this.item.owning,
      },
      Organization: {
        orgName: this.item.orgName,
        orgEmail: this.item.orgEmail,
        orgMobile: this.item.orgMobile,
        orglogo: AppConsts.docUrl + "/" + this.item.orgLogo,
      },
      Review: {
        link: this.item.reviewlink,
       },
    }

    // use custom delimiter {{ }}
    _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;

    // interpolate
    let compiled = _.template(myTemplateStr);
    let myTemplateCompiled = compiled(myVariables);
    return myTemplateCompiled;
  }

  opencc(): void {
    this.ccbox = !this.ccbox;
  }
  openbcc(): void {
    this.bccbox = !this.bccbox;
  }
}




