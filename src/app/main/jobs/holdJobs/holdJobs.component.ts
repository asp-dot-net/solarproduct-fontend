﻿import { Component, Injector, ViewEncapsulation, ViewChild, ElementRef, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JobsServiceProxy, JobDto, UserServiceProxy, OrganizationUnitDto, LeadsServiceProxy, LeadStateLookupTableDto, JobStatusTableDto, JobJobTypeLookupTableDto, JobFinanceOptionLookupTableDto, LeadUsersLookupTableDto, LeadSourceLookupTableDto, JobPaymentOptionLookupTableDto, CommonLookupServiceProxy, CommonLookupDto, UserActivityLogServiceProxy, UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { EntityTypeHistoryModalComponent } from '@app/shared/common/entityHistory/entity-type-history-modal.component';
import * as _ from 'lodash';
import * as moment from 'moment';
import { OnInit } from '@angular/core';
import { ViewApplicationModelComponent } from '../jobs/view-application-model/view-application-model.component';
// import { AddActivityModalComponent } from '@app/main/myleads/myleads/add-activity-model.component';
// import { ViewMyLeadComponent } from '@app/main/myleads/myleads/view-mylead.component';
import { Title } from '@angular/platform-browser';
// import { HoldjobsSmsEmailModelComponent } from './holdjobs-sms-email-model/holdjobs-sms-email-model.component';
import { JobHoldReasonModalComponent } from './add-jobholdreason-model.component';
import { CommentModelComponent } from '@app/main/activitylog/comment-modal.component';
import { EmailModelComponent } from '@app/main/activitylog/email-modal.component';
import { NotifyModelComponent } from '@app/main/activitylog/notify-modal.component';
import { ReminderModalComponent } from '@app/main/activitylog/reminder-modal.component';
import { SMSModelComponent } from '@app/main/activitylog/sms-modal.component';
import { ToDoModalComponent } from '@app/main/activitylog/todo-modal.component';
import { finalize } from 'rxjs/operators';
// import { JobNotesModalComponent } from './job-notes-model.component';
// import jsPDF from 'jspdf';  
// import html2canvas from 'html2canvas';
@Component({
    templateUrl: './holdJobs.component.html',
    styleUrls: ['./holdJobs.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class HoldJobsComponent extends AppComponentBase implements OnInit {

    FiltersData = false;
    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    organizationUnitlength: number = 0;
    scrollableCols: { field: string; header: string; }[];
    frozenCols: { field: string; header: string; }[];
    filterName = "JobNumber";

    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    @ViewChild('entityTypeHistoryModal', { static: true }) entityTypeHistoryModal: EntityTypeHistoryModalComponent;
    @ViewChild('viewApplicationModal', { static: true }) viewApplicationModal: ViewApplicationModelComponent;
    // @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    // @ViewChild('holdjobsTrackerSmsEmailModal', { static: true }) holdjobsTrackerSmsEmailModal: HoldjobsSmsEmailModelComponent;
    // @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
    @ViewChild('jobholdreasonModal', { static: true }) jobholdreasonModal: JobHoldReasonModalComponent;
    @ViewChild('content') content: ElementRef;
    @ViewChild('smsModel', { static: true }) smsModel: SMSModelComponent;
    @ViewChild('emailModel', { static: true }) emailModel: EmailModelComponent;
    @ViewChild('notifyModel', { static: true }) notifyModel: NotifyModelComponent;
    @ViewChild('commentModel', { static: true }) commentModel: CommentModelComponent;
    @ViewChild('todoModal', { static: true }) todoModal: ToDoModalComponent;
    @ViewChild('ReminderModal', { static: true }) ReminderModal: ReminderModalComponent;
    advancedFiltersAreShown = false;
    filterText = '';
    maxPanelOnFlatFilter: number;
    maxPanelOnFlatFilterEmpty: number;
    minPanelOnFlatFilter: number;
    minPanelOnFlatFilterEmpty: number;
    maxPanelOnPitchedFilter: number;
    maxPanelOnPitchedFilterEmpty: number;
    minPanelOnPitchedFilter: number;
    minPanelOnPitchedFilterEmpty: number;
    regPlanNoFilter = '';
    lotNumberFilter = '';
    peakMeterNoFilter = '';
    offPeakMeterFilter = '';
    enoughMeterSpaceFilter = -1;
    isSystemOffPeakFilter = -1;
    suburbFilter = '';
    stateFilter = '';
    diplayifvic = false;
    jobTypeNameFilter = '';
    jobStatusNameFilter = '';
    roofTypeNameFilter = '';
    roofAngleNameFilter = '';
    elecDistributorNameFilter = '';
    leadCompanyNameFilter = '';
    elecRetailerNameFilter = '';
    paymentOptionNameFilter = '';
    depositOptionNameFilter = '';
    meterUpgradeNameFilter = '';
    meterPhaseNameFilter = '';
    promotionOfferNameFilter = '';
    houseTypeNameFilter = '';
    financeOptionNameFilter = '';
    OpenRecordId: number = 0;
    organizationUnit = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    _entityTypeFullName = 'TheSolarProduct.Jobs.Job';
    entityHistoryEnabled = false;
    startDate: moment.Moment;
    endDate: moment.Moment;
    projectFilter = '';
    stateNameFilter = '';
    date = new Date();
    // public sampleDateRange: moment.Moment[] = [moment(this.date), moment().endOf('day')];
    sampleDateRange: moment.Moment[] = [ moment(this.date),  moment(this.date)];

    // sampleDateRange: moment.Moment[] = [moment().add(-7, 'days').endOf('day'), moment().add(0, 'days').startOf('day')];
    jobStatusIDFilter: [];
    elecDistributorId = 0;
    applicationstatus = 0;
    jobTypeId = 0;
    allStates: LeadStateLookupTableDto[];
    alljobstatus: JobStatusTableDto[];
    jobTypes: JobJobTypeLookupTableDto[];
    jobFinOptions: JobFinanceOptionLookupTableDto[];
    filteredManagers: LeadUsersLookupTableDto[];
    filteredReps: LeadUsersLookupTableDto[];
    filteredTeams: LeadUsersLookupTableDto[];
    jobPaymentOptions: JobPaymentOptionLookupTableDto[];
    salesManagerId = 0;
    salesRepId = 0;
    jobDateFilter = 'CreationDate';
    invertSuggestions: string[];
    panelSuggestions: string[];
    panelModel = '';
    invertModel = '';
    readytoactive = 0;
    systemStrtRange = 0;
    systemEndRange = 0;
    ExpandedView: boolean = true;
    SelectedLeadId: number = 0;
    leadSourceIdFilter = [];
    allLeadSources: LeadSourceLookupTableDto[];
    totalNoOfPanel = 0;
    totalNoOfInvert = 0;
    totalSystemCapacity = 0;
    totalCost = 0;
    depositeReceived = 0;
    active = 0;
    installJob = 0;
    hot = 0;
    closed = 0;
    price = 0;
    addressFilter = "";
    paymentid = 0;
    areaNameFilter = "";
    postalcodefrom = "";
    postalcodeTo = "";
    teamId = 0;
    excelorcsvfile = 0;
    solarvicstatus = 0;
    firstrowcount = 0;
    last = 0;
    holdresonsId = 0;
    holdReasons: CommonLookupDto[];
    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 330;
    toggle: boolean = true;
    orgCode = '';
    change() {
        this.toggle = !this.toggle;
      }

    constructor(
        injector: Injector,
        private _jobsServiceProxy: JobsServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        // private _notifyService: NotifyService,
        // private _tokenAuth: TokenAuthServiceProxy,
        // private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService,
        private _userServiceProxy: UserServiceProxy,
        private _router: Router,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy,
        private titleService: Title
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Hold Job Tracker");
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        debugger;
        this.entityHistoryEnabled = this.setIsEntityHistoryEnabled();

        this._commonLookupService.getAllLeadSourceDropdown().subscribe(result => {
            this.allLeadSources = result;
        });
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
        this._jobsServiceProxy.getAllHoldReasonForTableDropdown().subscribe(result => {
            this.holdReasons = result;
        });
        this._commonLookupService.getTeamForFilter().subscribe(teams => {
            this.filteredTeams = teams;
        });
        this._commonLookupService.getAllJobStatusTableDropdown().subscribe(result => {
            this.alljobstatus = result;
        });
        this._commonLookupService.getAllPaymentOptionForTableDropdown().subscribe(result => {
            this.jobPaymentOptions = result;
        });
        this._commonLookupService.getAllJobTypeForTableDropdown().subscribe(result => {
            this.jobTypes = result;
        });

        this._commonLookupService.getAllFinanceOptionForTableDropdown().subscribe(result => {
            this.jobFinOptions = result;
        });

        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Hold Jobs Tracker';
            log.section = 'Hold Jobs Tracker';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.orgCode = this.allOrganizationUnits[0].code;
            this.onsalesmanger()
            this.onsalesrep()
            this.getJobs();
        });
    }
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }
        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }
    onsalesmanger() {
        this._commonLookupService.getSalesManagerForFilter(this.organizationUnit, undefined).subscribe(manager => {
            this.filteredManagers = manager;
        });
    }
    onsalesrep() {
        this._commonLookupService.getSalesRepForFilter(this.organizationUnit, undefined).subscribe(rep => {
            this.filteredReps = rep;
        });
    }


    private setIsEntityHistoryEnabled(): boolean {
        let customSettings = (abp as any).custom;
        return this.isGrantedAny('Pages.Administration.AuditLogs') && customSettings.EntityHistory && customSettings.EntityHistory.isEnabled && _.filter(customSettings.EntityHistory.enabledEntities, entityType => entityType === this._entityTypeFullName).length === 1;
    }

    setOpenRecord(id) {
        if (id === this.OpenRecordId) { this.OpenRecordId = 0; return; }
        this.OpenRecordId = id;
    }

    getJobs(event?: LazyLoadEvent) {
        debugger;
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        if (this.sampleDateRange != null) {
            this.startDate = this.sampleDateRange[0];
            this.endDate = this.sampleDateRange[1];
        } else {
            this.startDate = null;
            this.endDate = null;
        }
        if (this.stateFilter == "VIC") {
            this.diplayifvic = true;
        } else { this.diplayifvic = false; }
        this.primengTableHelper.showLoadingIndicator();

        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._jobsServiceProxy.getAllJobHoldTrackerData(
            this.filterName,
            filterText_,
            this.suburbFilter,
            this.stateFilter,
            this.jobTypeNameFilter,
            this.jobStatusNameFilter,
            this.roofTypeNameFilter,
            this.roofAngleNameFilter,
            this.elecDistributorNameFilter,
            this.leadCompanyNameFilter,
            this.elecRetailerNameFilter,
            this.paymentOptionNameFilter,
            this.depositOptionNameFilter,
            this.meterUpgradeNameFilter,
            this.meterPhaseNameFilter,
            this.promotionOfferNameFilter,
            this.houseTypeNameFilter,
            this.financeOptionNameFilter,
            this.organizationUnit,
            this.projectFilter,
            this.stateNameFilter,
            this.elecDistributorId,
            this.holdresonsId,
            this.applicationstatus,
            this.jobTypeId,
            this.startDate,
            this.endDate,
            this.salesManagerId == 0 ? undefined : this.salesManagerId,
            this.salesRepId == 0 ? undefined : this.salesRepId,
            this.jobDateFilter,
            this.panelModel,
            this.invertModel,
            this.readytoactive,
            this.systemStrtRange,
            this.systemEndRange,
            this.leadSourceIdFilter,
            this.teamId,
            this.addressFilter,
            this.postalcodefrom,
            this.postalcodeTo,
            this.paymentid,
            this.areaNameFilter,
            this.solarvicstatus,
            undefined,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            if (result.totalCount > 0) {
                this.getLeadSummaryCount();
            } else {
                this.totalNoOfPanel = 0;
                this.totalSystemCapacity = 0;
                this.totalCost = 0;
                this.depositeReceived = 0;
                this.active = 0;
                this.installJob = 0;
                this.price = 0;
            }

            this.shouldShow = false;
        },
        err => {
            this.primengTableHelper.hideLoadingIndicator();
        });

    }
    getLeadSummaryCount() {
        debugger;
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._jobsServiceProxy.getAllJobHoldTrackerCount(
            this.filterName,
            filterText_,
            this.suburbFilter,
            this.stateFilter,
            this.jobTypeNameFilter,
            this.jobStatusNameFilter,
            this.roofTypeNameFilter,
            this.roofAngleNameFilter,
            this.elecDistributorNameFilter,
            this.leadCompanyNameFilter,
            this.elecRetailerNameFilter,
            this.paymentOptionNameFilter,
            this.depositOptionNameFilter,
            this.meterUpgradeNameFilter,
            this.meterPhaseNameFilter,
            this.promotionOfferNameFilter,
            this.houseTypeNameFilter,
            this.financeOptionNameFilter,
            this.organizationUnit,
            this.projectFilter,
            this.stateNameFilter,
            this.elecDistributorId,
            this.holdresonsId,
            this.applicationstatus,
            this.jobTypeId,
            this.startDate,
            this.endDate,
            this.salesManagerId == 0 ? undefined : this.salesManagerId,
            this.salesRepId == 0 ? undefined : this.salesRepId,
            this.jobDateFilter,
            this.panelModel,
            this.invertModel,
            this.readytoactive,
            this.systemStrtRange,
            this.systemEndRange,
            this.leadSourceIdFilter,
            this.teamId,
            this.addressFilter,
            this.postalcodefrom,
            this.postalcodeTo,
            this.paymentid,
            this.areaNameFilter,
            this.solarvicstatus,
            undefined,
            undefined,
            undefined,
            undefined,
        ).subscribe(result => {
            debugger;
            if (result) {
                this.totalNoOfPanel = result.totalNoOfPanel;
                this.totalSystemCapacity = result.totalSystemCapacity;
                this.totalCost = result.totalCost;
                this.depositeReceived = result.depositeReceived;
                this.active = result.active;
                this.installJob = result.installJob;
                this.price = parseFloat((result.totalCost / result.totalSystemCapacity).toFixed(2));
            }
        });
    }
    filterPanelModel(event): void {
        this._jobsServiceProxy.getpanelmodel(event.query).subscribe(output => {
            this.panelSuggestions = output;
        });
    }
    filterInvertModel(event): void {
        this._jobsServiceProxy.getinvertmodel(event.query).subscribe(output => {
            this.invertSuggestions = output;
        });
    }
    reloadPage($event): void {
        this.ExpandedView = true;
        this.paginator.changePage(this.paginator.getPage());
    }

    createJob(): void {
        this._router.navigate(['/app/admin/jobs/jobs/createOrEdit']);
    }

    showHistory(job: JobDto): void {
        this.entityTypeHistoryModal.show({
            entityId: job.id.toString(),
            entityTypeFullName: this._entityTypeFullName,
            entityTypeDescription: ''
        });
    }

    quickview(jobid): void {
        this.viewApplicationModal.show(jobid);
    }

    clear() {
        this.filterText = '';
        this.minPanelOnFlatFilter = 0;
        this.maxPanelOnFlatFilter = 0;
        this.minPanelOnPitchedFilter = 0;
        this.maxPanelOnPitchedFilter = 0;
        this.regPlanNoFilter = '';
        this.lotNumberFilter = '';
        this.peakMeterNoFilter = '';
        this.offPeakMeterFilter = '';
        this.enoughMeterSpaceFilter = -1;
        this.isSystemOffPeakFilter = -1;
        this.suburbFilter = '';
        this.stateFilter = '';
        this.jobTypeNameFilter = '';
        this.jobStatusNameFilter = '';
        this.roofTypeNameFilter = '';
        this.elecDistributorNameFilter = '';
        this.leadCompanyNameFilter = '';
        this.elecRetailerNameFilter = '';
        this.paymentOptionNameFilter = '';
        this.depositOptionNameFilter = '';
        this.meterUpgradeNameFilter = '';
        this.meterPhaseNameFilter = '';
        this.promotionOfferNameFilter = '';
        this.houseTypeNameFilter = '';
        this.financeOptionNameFilter = '';
        this.salesRepId = 0;
        this.salesManagerId = 0;
        this.panelModel = '',
            this.invertModel = '',
            this.readytoactive = 0;
        this.teamId = 0,
            this.addressFilter = "",
            this.systemStrtRange = 0;
        this.systemEndRange = 0;
        this.leadSourceIdFilter = [];
        this.sampleDateRange =[moment(this.date), ,  moment(this.date)];
        this.getJobs();
    }
    changeOrgCode() {
        this.orgCode = this.allOrganizationUnits.find(x => x.id == this.organizationUnit).code;
    }
    deleteJob(job: JobDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._jobsServiceProxy.delete(job.id)
                        .subscribe(() => {
                            this.reloadPage(null);
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    exportToExcel(excelorcsv): void {
        if (this.sampleDateRange != null) {
            this.startDate = this.sampleDateRange[0];
            this.endDate = this.sampleDateRange[1];
        } else {
            this.startDate = null;
            this.endDate = null;
        }
        this.excelorcsvfile = excelorcsv;
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._jobsServiceProxy.getJobHoldTrackerToExcel(
            this.filterName,
            filterText_,
            this.suburbFilter,
            this.stateFilter,
            this.jobTypeNameFilter,
            this.jobStatusNameFilter,
            this.roofTypeNameFilter,
            this.roofAngleNameFilter,
            this.elecDistributorNameFilter,
            this.leadCompanyNameFilter,
            this.elecRetailerNameFilter,
            this.paymentOptionNameFilter,
            this.depositOptionNameFilter,
            this.meterUpgradeNameFilter,
            this.meterPhaseNameFilter,
            this.promotionOfferNameFilter,
            this.houseTypeNameFilter,
            this.financeOptionNameFilter,
            this.organizationUnit,
            this.projectFilter,
            this.stateNameFilter,
            this.elecDistributorId,
            this.holdresonsId,
            this.applicationstatus,
            this.jobTypeId,
            this.startDate,
            this.endDate,
            this.salesManagerId == 0 ? undefined : this.salesManagerId,
            this.salesRepId == 0 ? undefined : this.salesRepId,
            this.jobDateFilter,
            this.panelModel,
            this.invertModel,
            this.readytoactive,
            this.systemStrtRange,
            this.systemEndRange,
            this.leadSourceIdFilter,
            this.teamId,
            this.addressFilter,
            this.postalcodefrom,
            this.postalcodeTo,
            this.paymentid,
            this.areaNameFilter,
            this.solarvicstatus,
            this.excelorcsvfile,
            undefined,
            undefined,
            undefined,
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }
    expandGrid() {
        this.ExpandedView = true;
    }

    navigateToLeadDetail(leadid): void {
        // this.ExpandedView = !this.ExpandedView;
        // this.ExpandedView = false;
        // this.SelectedLeadId = leadid;
        // this.viewLeadDetail.showDetail(leadid, '', 8);
    }

    exportTopdf(): void {
        debugger;
        // let data = document.getElementById('content');  
        // html2canvas(data).then(canvas => {
        // const contentDataURL = canvas.toDataURL('image/png')  
        // let pdf = new jsPDF('l', 'cm', 'a4'); //Generates PDF in landscape mode
        // // let pdf = new jspdf('p', 'cm', 'a4'); Generates PDF in portrait mode
        // pdf.addImage(contentDataURL, 'PNG', 0, 0, 29.7, 21.0);  
        // pdf.save('Filename.pdf'); 
        // let content = this.content.nativeElement;
        // let doc = new jsPDF();
        // doc.html(content.innerHTML);
        // doc.save('test.pdf');
        // });

    }
    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Hold Jobs Tracker';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Hold Jobs Tracker';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }
}