import { Component, ViewChild, Injector, Output, EventEmitter, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {
    ProductItemsServiceProxy, CreateOrEditProductItemDto, ProductItemProductTypeLookupTableDto,UserActivityLogServiceProxy, UserActivityLogDto, UploadDocumentInput, CommonLookupServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Observable } from "@node_modules/rxjs";
import { FileItem, FileUploader, FileUploaderOptions } from 'ng2-file-upload';


@Component({
    selector: 'editProductItemModal',
    templateUrl: './edit-productItem.component.html',
    animations: [appModuleAnimation()]
})
export class EditProductItemModalComponent extends AppComponentBase {

    @ViewChild('editProductItem', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    saving = false;
    showifactive = false;
    productItem: CreateOrEditProductItemDto = new CreateOrEditProductItemDto();
    productTypeName = '';
    allProductTypes: ProductItemProductTypeLookupTableDto[];
    public uploader: FileUploader;
    public maxfileBytesUserFriendlyValue = 5;
    private _uploaderOptions: FileUploaderOptions = {};
    filenName = [];

    constructor(
        injector: Injector,
        private _productItemsServiceProxy: ProductItemsServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
    }

    show(productItemId?: number): void {
        this._commonLookupService.getAllProductTypeForTableDropdown().subscribe(result => {
            this.allProductTypes = result;
        });
        if (!productItemId) {
            this.productItem = new CreateOrEditProductItemDto();
            this.productItem.id = productItemId;
            this.productItem.approvedDate = moment().startOf('day');
            this.productItem.expiryDate = moment().startOf('day');
            this.productTypeName = '';
            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Product Items';
            log.section = 'Product Items';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._productItemsServiceProxy.getProductItemForEdit(productItemId).subscribe(result => {
                
                this.productItem = result.productItem;
                this.productTypeName = result.productTypeName;
                this.active = true;
                this.onChange(this.productItem.isActive);
                console.log(this.productItem);
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Product Items : ' + this.productItem.name;
                log.section = 'Product Items';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        
    }
    onChange(item){
        
        if(item == true){
            this.showifactive =  true;
        } else {
            this.showifactive =  false;
        }

    }
    // save(): void {
    //     if (this.uploader.queue.length == 0) {
    //         this.notify.warn(this.l('SelectFileToUpload'));
    //         return;
    //     }
    //     this.uploader.uploadAll();
    // }

    // upload completed event
    // onUpload(event): void {
        
    //     for (const file of event.files) {
    //         this.uploadedFiles.push(file);
    //         this.modal.hide();
    //         this.uploadedFiles = [];
    //         this.modalSave.emit(null);
    //     }
    // }

    // onBeforeSend(event): void {
    //     event.xhr.setRequestHeader('Authorization', 'Bearer ' + abp.auth.getToken());
    // }
    
    cancel(): void {
        this.modal.hide();

        
    }

    private saveInternal(): Observable<void> {
        this.saving = true;
        //this.productItem.name = this.productItem.stockItem;
        return this._productItemsServiceProxy.createOrEdit(this.productItem)
            .pipe(finalize(() => {
                this.saving = false;
                this.notify.info(this.l('SavedSuccessfully'));
            }));
    }

    save(): void {
        
        this.saveInternal().subscribe(x => {
            this.cancel();
           this.modalSave.emit(null);
           if(this.productItem.id){
            let log = new UserActivityLogDto();
            log.actionId = 82;
            log.actionNote ='Product Items Updated : '+ this.productItem.name;
            log.section = 'Product Items';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
        }else{
            let log = new UserActivityLogDto();
            log.actionId = 81;
            log.actionNote ='Product Items Created : '+ this.productItem.name;
            log.section = 'Product Items';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
        }
        });
    }
}
