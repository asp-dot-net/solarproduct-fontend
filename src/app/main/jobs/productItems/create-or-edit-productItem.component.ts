﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {
    ProductItemsServiceProxy, CreateOrEditProductItemDto, ProductItemProductTypeLookupTableDto, CommonLookupServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Observable } from "@node_modules/rxjs";
import { FileItem, FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { AppConsts } from '@shared/AppConsts';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';


@Component({
    templateUrl: './create-or-edit-productItem.component.html',
    animations: [appModuleAnimation()]
})
export class CreateOrEditProductItemComponent extends AppComponentBase implements OnInit {
    active = false;
    saving = false;
    showifactive = false;
    productItem: CreateOrEditProductItemDto = new CreateOrEditProductItemDto();
    productTypeName = '';
    allProductTypes: ProductItemProductTypeLookupTableDto[];
    public uploader: FileUploader;
    public maxfileBytesUserFriendlyValue = 5;
    private _uploaderOptions: FileUploaderOptions = {};
    filenName = [];

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _tokenService: TokenService,
        private _productItemsServiceProxy: ProductItemsServiceProxy,
        private _router: Router,
        private _commonLookupService: CommonLookupServiceProxy,
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.show(this._activatedRoute.snapshot.queryParams['id']);
    }

    show(productItemId?: number): void {
        this._commonLookupService.getAllProductTypeForTableDropdown().subscribe(result => {
            this.allProductTypes = result;
        });
        if (!productItemId) {
            this.productItem = new CreateOrEditProductItemDto();
            this.productItem.id = productItemId;
            this.productItem.approvedDate = moment().startOf('day');
            this.productItem.expiryDate = moment().startOf('day');
            this.productTypeName = '';
            this.active = true;
        } else {
            this._productItemsServiceProxy.getProductItemForEdit(productItemId).subscribe(result => {
                
                this.productItem = result.productItem;
                this.productTypeName = result.productTypeName;
                this.active = true;
                this.onChange(this.productItem.isActive);
                console.log(this.productItem);
            });
        }
    }

    private saveInternal(): Observable<void> {
        this.saving = true;
        //this.productItem.name = this.productItem.stockItem;
        return this._productItemsServiceProxy.createOrEdit(this.productItem)
            .pipe(finalize(() => {
                this.saving = false;
                this.notify.info(this.l('SavedSuccessfully'));
            }));
    }

    save(): void {
        this.saveInternal().subscribe(x => {
            this._router.navigate(['/app/main/jobs/productItems']);
        })
    }
    onChange(item){
        
        if(item == true){
            this.showifactive =  true;
        } else {
            this.showifactive =  false;
        }

    }
    saveAndNew(): void {
        this.saveInternal().subscribe(x => {
            this.productItem = new CreateOrEditProductItemDto();
        })
    }

    cancel(): void {
        this._router.navigate(['/app/main/jobs/productItems']);
    }
}
