import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { CreateOrEditProductItemDto, ProductItemsServiceProxy ,UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AppConsts } from '@shared/AppConsts';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { FileUploader, FileItem, FileUploaderOptions } from 'ng2-file-upload';

@Component({
    selector: 'addproductimageModal',
    templateUrl: './add-productimage.component.html'
})
export class AddProductImageComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    saving = false;
    productItemId: number = 0;
    public uploader: FileUploader;
    public maxfileBytesUserFriendlyValue = 5;
    private _uploaderOptions: FileUploaderOptions = {};
    filetoken: string = "";
    selectedFile : any;
    productItem: CreateOrEditProductItemDto = new CreateOrEditProductItemDto();
    constructor(
        injector: Injector,
        private _tokenService: TokenService,
        private _productItemsServiceProxy: ProductItemsServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
    }

    show(productItemId: number): void {
        this._productItemsServiceProxy.getProductItemForEdit(productItemId).subscribe(result => {
            this.productItem = result.productItem;
            this.active = true;
            this.initializeModal();
            this.modal.show();
           
        });
    }

    initializeModal(): void {
        this.active = true;
        this.initFileUploader();
    }
    
    close(): void {
        this.active = false;
        this.modal.hide();
    }

    initFileUploader(): void {
        this.uploader = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Profile/UploadFile' });
        this._uploaderOptions.autoUpload = false;
        this._uploaderOptions.authToken = 'Bearer ' + this._tokenService.getToken();
        this._uploaderOptions.removeAfterUpload = true;
        this.uploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

        this.uploader.onBuildItemForm = (fileItem: FileItem, form: any) => {
            form.append('FileType', fileItem.file.type);
            form.append('FileName', fileItem.file.name);
            form.append('FileToken', this.guid());
        };

        this.uploader.onSuccessItem = (item, response, status) => {
            const resp = <IAjaxResponse>JSON.parse(response);
            if (resp.success) {
                this.filetoken = resp.result.fileToken;
                //this.instfilename.push(resp.result.fileName);
            } else {
                this.message.error(resp.error.message);
            }
        };

        this.uploader.setOptions(this._uploaderOptions);
    }

    fileChangeEvent(event: any): void {
        debugger

        let w = 0;
        let h = 0;
        if(event.target.files[0])
        {
            var url = URL.createObjectURL(event.target.files[0]);
            var img = new Image;
    
            img.onload = () =>{
                w = img.width;
                h = img.height;
                if(w != 900 || h != 700){
                    this.notify.warn("Select Image That Contain Size 900 X 700");
                    this.selectedFile = null;
                }
            };
            img.src = url;

            
            this.uploader.clearQueue();
            this.uploader.addToQueue([<File>event.target.files[0]]);
            this.uploader.uploadAll();
        }
        else
        {
            this.filetoken = "";
            this.uploader.clearQueue();
        }
    }

    guid(): string {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }
    
    // downloadfile(file): void {
    //     let FileName = AppConsts.oldDocUrl + file;

    //     window.open(FileName, "_blank");

    // }

    save(): void {
        this.saving = true;

        if(this.filetoken){
            if( this.filetoken == ""){
                this.notify.warn("Select Image.");
                this.saving = false;
                return;
            }
        }
			
        this._productItemsServiceProxy.saveProductIteamImage(this.filetoken, this.productItem.id)
            .pipe(finalize(() => { this.saving = false;}))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);

            }
        );
    }

    downloadfile(): void {
        debugger;
        let FileName = AppConsts.docUrl + this.productItem.imagePath + this.productItem.imageName;

        window.open(FileName, "_blank");

    }
}
