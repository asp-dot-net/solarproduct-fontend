﻿import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductItemsServiceProxy, ProductItemDto, ProductItemProductTypeLookupTableDto, CommonLookupServiceProxy ,UserActivityLogServiceProxy, UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { NotifyService, TokenService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { FileUpload } from 'primeng/fileupload';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { finalize } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppConsts } from '@shared/AppConsts';
import { AddAttachmentProductItemComponent } from './add-attachment.component';
import { Title } from '@angular/platform-browser';
import { AddProductImageComponent } from './add-productimage.component';
import { EditProductItemModalComponent } from './edit-productItem.component';

@Component({
    templateUrl: './productItems.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ProductItemsComponent extends AppComponentBase {

    @ViewChild('ExcelFileUpload', { static: false }) excelFileUpload: FileUpload;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('addAttachmentModal', { static: true }) addAttachmentModal: AddAttachmentProductItemComponent;
    @ViewChild('addproductimageModal', { static: true }) addproductimageModal: AddProductImageComponent;
    @ViewChild('editProductItemModal', { static: true }) editProductItemModal: EditProductItemModalComponent;
    shouldShow: boolean = false;
    uploadUrl: string;
    uploadUrlPanel: string;
    advancedFiltersAreShown = false;
    filterText = '';
    nameFilter = '';
    manufacturerFilter = '';
    modelFilter = '';
    seriesFilter = '';
    maxSizeFilter: number;
    maxSizeFilterEmpty: number;
    minSizeFilter: number;
    minSizeFilterEmpty: number;
    shortNameFilter = '';
    maxCodeFilter: number;
    maxCodeFilterEmpty: number;
    minCodeFilter: number;
    minCodeFilterEmpty: number;
    inverterCertFilter = '';
    maxStockLocationFilter: number;
    maxStockLocationFilterEmpty: number;
    minStockLocationFilter: number;
    minStockLocationFilterEmpty: number;
    maxMinLevelFilter: number;
    maxMinLevelFilterEmpty: number;
    minMinLevelFilter: number;
    minMinLevelFilterEmpty: number;
    maxCostPriceFilter: number;
    maxCostPriceFilterEmpty: number;
    minCostPriceFilter: number;
    minCostPriceFilterEmpty: number;
    maxExpiryDateFilter: moment.Moment;
    minExpiryDateFilter: moment.Moment;
    acPowerFilter = '';
    stockItemFilter = '';
    stockIdFilter = '';
    productTypeNameFilter = '';
    allProductTypes: ProductItemProductTypeLookupTableDto[];
    activeOrDeactive = 'Active';
    productTypeId = 0;
    firstrowcount = 0;
    last = 0;
    public screenHeight: any;  
    testHeight = 250;
    attachment = 'All';
    locationIDFilter = [];
    allLocation = [
        {id : 1, displayName:'QLD'},
        {id : 2, displayName:'VIC'},
        {id : 3, displayName:'NSW'},
        {id : 4, displayName:'WA'},
        {id : 5, displayName:'NT'},
        {id : 6, displayName:'TAS'},
        {id : 7, displayName:'SA'},
        {id : 8, displayName:'AUS'},
    ];

    fetchProductItemUrl: string;

    constructor(
        injector: Injector,
        private _productItemsServiceProxy: ProductItemsServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _httpClient: HttpClient,
        private _fileDownloadService: FileDownloadService,
        private _router: Router,
        private titleService: Title,
        private _commonLookupService: CommonLookupServiceProxy,
        private _tokenService: TokenService,
        ) {
            super(injector);
            this.titleService.setTitle(this.appSession.tenancyName + " |  Product Items");
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/Users/ImportInverterFromExcel';
        this.uploadUrlPanel = AppConsts.remoteServiceBaseUrl + '/Users/ImportPanelFromExcel';
        this.fetchProductItemUrl = AppConsts.remoteServiceBaseUrl + '/Users/FetchProductItemFormGreenDeal';
        
    }
    
    searchLog() : void {
        debugger;
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Product Items';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
        this._commonLookupService.getAllProductTypeForTableDropdown().subscribe(result => {
            this.allProductTypes = result;
        });
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Product Items';
        log.section = 'Product Items';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
    }
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }
    getProductItems(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._productItemsServiceProxy.getAll(
            this.filterText,
            this.nameFilter,
            this.manufacturerFilter,
            this.modelFilter,
            this.seriesFilter,
            this.maxSizeFilter == null ? this.maxSizeFilterEmpty : this.maxSizeFilter,
            this.minSizeFilter == null ? this.minSizeFilterEmpty : this.minSizeFilter,
            this.shortNameFilter,
            this.maxCodeFilter == null ? this.maxCodeFilterEmpty : this.maxCodeFilter,
            this.minCodeFilter == null ? this.minCodeFilterEmpty : this.minCodeFilter,
            this.inverterCertFilter,
            this.maxStockLocationFilter == null ? this.maxStockLocationFilterEmpty : this.maxStockLocationFilter,
            this.minStockLocationFilter == null ? this.minStockLocationFilterEmpty : this.minStockLocationFilter,
            this.maxMinLevelFilter == null ? this.maxMinLevelFilterEmpty : this.maxMinLevelFilter,
            this.minMinLevelFilter == null ? this.minMinLevelFilterEmpty : this.minMinLevelFilter,
            this.maxCostPriceFilter == null ? this.maxCostPriceFilterEmpty : this.maxCostPriceFilter,
            this.minCostPriceFilter == null ? this.minCostPriceFilterEmpty : this.minCostPriceFilter,
            this.maxExpiryDateFilter,
            this.minExpiryDateFilter,
            this.acPowerFilter,
            this.stockItemFilter,
            this.stockIdFilter,
            this.activeOrDeactive,
            this.productTypeNameFilter,
            this.productTypeId,
            this.attachment,
            this.locationIDFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            this.shouldShow = false;
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createProductItem(): void {
        this._router.navigate(['/app/main/jobs/productItems/createOrEdit']);
    }

    fetchProductItem(): void {
        this._productItemsServiceProxy.fetchAndInsert()
            .subscribe(() => {
                this.reloadPage();
                this.notify.success(this.l('DataFetchSuccessfully'));
            }
        );
    }

    fetchProductItemFromGreenDeal(): void {
        // this._productItemsServiceProxy.fetchProductItemFormGreenDeal()
        //     .subscribe(() => {
        //         this.reloadPage();
        //         this.notify.success(this.l('DataFetchSuccessfully'));
        //     }
        // );

        var headers_object = new HttpHeaders().set("Authorization", "Bearer " + this._tokenService.getToken());

        const httpOptions = {
            headers: headers_object
        };

        this._httpClient.post<any>(this.fetchProductItemUrl, null, httpOptions)
            .subscribe(response => {
                if (response.success) {
                    this.notify.success(this.l('FetchProductItemProcessStart'));
                } else if (response.error != null) {
                    this.notify.error(this.l('FetchUsersUploadFailed'));
                }
            }
        );
    }

    deleteProductItem(productItem: ProductItemDto): void {
        this.message.confirm(
            this.l('AreYouSureWanttoDelete'),
            this.l('Delete'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._productItemsServiceProxy.delete(productItem.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete Product Items: ' + productItem.name;
                            log.section = 'Product Items';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });
                        }
                    );
                }
            }
        );
    }

    downloadfile(docpath: string, filename: string): void {

        let FileName = AppConsts.docUrl + "/" + docpath + filename;
        window.open(FileName, "_blank");
    }

    exportToExcel(): void {
        this._productItemsServiceProxy.getProductItemsToExcel(
            this.filterText,
            this.nameFilter,
            this.manufacturerFilter,
            this.modelFilter,
            this.seriesFilter,
            this.maxSizeFilter == null ? this.maxSizeFilterEmpty : this.maxSizeFilter,
            this.minSizeFilter == null ? this.minSizeFilterEmpty : this.minSizeFilter,
            this.shortNameFilter,
            this.maxCodeFilter == null ? this.maxCodeFilterEmpty : this.maxCodeFilter,
            this.minCodeFilter == null ? this.minCodeFilterEmpty : this.minCodeFilter,
            this.inverterCertFilter,
            this.maxStockLocationFilter == null ? this.maxStockLocationFilterEmpty : this.maxStockLocationFilter,
            this.minStockLocationFilter == null ? this.minStockLocationFilterEmpty : this.minStockLocationFilter,
            this.maxMinLevelFilter == null ? this.maxMinLevelFilterEmpty : this.maxMinLevelFilter,
            this.minMinLevelFilter == null ? this.minMinLevelFilterEmpty : this.minMinLevelFilter,
            this.maxCostPriceFilter == null ? this.maxCostPriceFilterEmpty : this.maxCostPriceFilter,
            this.minCostPriceFilter == null ? this.minCostPriceFilterEmpty : this.minCostPriceFilter,
            this.maxExpiryDateFilter,
            this.minExpiryDateFilter,
            this.acPowerFilter,
            this.stockItemFilter,
            this.stockIdFilter,
            this.productTypeNameFilter,
            this.activeOrDeactive,
            this.productTypeId,
            this.attachment,
            this.locationIDFilter
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    uploadExcel(data: { files: File }): void {
        const formData: FormData = new FormData();
        const file = data.files[0];
        formData.append('file', file, file.name);

        this._httpClient
            .post<any>(this.uploadUrl, formData)
            .pipe(finalize(() => this.excelFileUpload.clear()))
            .subscribe(response => {
                if (response.success) {
                    this.notify.success(this.l('ImportInvertersProcessStart'));
                } else if (response.error != null) {
                    this.notify.error(this.l('ImportInvertersUploadFailed'));
                }
            });
    }

    uploadExcelPanel(data: { files: File }): void {
        const formData: FormData = new FormData();
        const file = data.files[0];
        formData.append('file', file, file.name);

        this._httpClient
            .post<any>(this.uploadUrlPanel, formData)
            .pipe(finalize(() => this.excelFileUpload.clear()))
            .subscribe(response => {
                if (response.success) {
                    this.notify.success(this.l('ImportPanelsProcessStart'));
                } else if (response.error != null) {
                    this.notify.error(this.l('ImportPanelUploadFailed'));
                }
            });
    }

    onUploadExcelError(): void {
        this.notify.error(this.l('ImportInvertersUploadFailed'));
    }

    onUploadExcelErrorPanel(): void {
        this.notify.error(this.l('ImportPanelUploadFailed'));
    }


    clear() {
        this.activeOrDeactive = 'Active',
            this.productTypeId = 0,
            this.getProductItems();
    }
}
