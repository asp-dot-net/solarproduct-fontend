﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ProductItemsServiceProxy, GetProductItemForViewDto, ProductItemDto,UserActivityLogServiceProxy, UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { AppConsts } from '@shared/AppConsts';
import { finalize } from 'rxjs/operators';
@Component({
    templateUrl: './view-productItem.component.html',
    animations: [appModuleAnimation()]
})
export class ViewProductItemComponent extends AppComponentBase implements OnInit {

    active = false;
    saving = false;

    item: GetProductItemForViewDto;
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
         private _productItemsServiceProxy: ProductItemsServiceProxy,
         private _router: Router,
         private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
        this.item = new GetProductItemForViewDto();
        this.item.productItem = new ProductItemDto();        
    }

    ngOnInit(): void {
        this.show(this._activatedRoute.snapshot.queryParams['id']);
    }

    show(productItemId: number): void {
      this._productItemsServiceProxy.getProductItemForView(productItemId).subscribe(result => {      
                 this.item = result;
                this.active = true;
                let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Opened Product Items View : ' + this.item.productItem.name;
        log.section = 'Product Items';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {            
         });
            });       
    }
    
    downloadfile(item): void {
        let FileName = AppConsts.docUrl + "/" + item.productItem.filePath + item.productItem.fileName;
        window.open(FileName, "_blank");
    };

    cancel(): void {
        this._router.navigate( ['/app/main/jobs/productItems']);
    }
}
