﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetPaymentOptionForViewDto, PaymentOptionDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewPaymentOptionModal',
    templateUrl: './view-paymentOption-modal.component.html'
})
export class ViewPaymentOptionModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetPaymentOptionForViewDto;


    constructor(
        injector: Injector
    ) {
        super(injector);
        this.item = new GetPaymentOptionForViewDto();
        this.item.paymentOption = new PaymentOptionDto();
    }

    show(item: GetPaymentOptionForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
