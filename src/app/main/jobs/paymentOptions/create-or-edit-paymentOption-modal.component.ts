﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { PaymentOptionsServiceProxy, CreateOrEditPaymentOptionDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';

@Component({
    selector: 'createOrEditPaymentOptionModal',
    templateUrl: './create-or-edit-paymentOption-modal.component.html'
})
export class CreateOrEditPaymentOptionModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    paymentOption: CreateOrEditPaymentOptionDto = new CreateOrEditPaymentOptionDto();



    constructor(
        injector: Injector,
        private _paymentOptionsServiceProxy: PaymentOptionsServiceProxy
    ) {
        super(injector);
    }

    show(paymentOptionId?: number): void {

        if (!paymentOptionId) {
            this.paymentOption = new CreateOrEditPaymentOptionDto();
            this.paymentOption.id = paymentOptionId;

            this.active = true;
            this.modal.show();
        } else {
            this._paymentOptionsServiceProxy.getPaymentOptionForEdit(paymentOptionId).subscribe(result => {
                this.paymentOption = result.paymentOption;


                this.active = true;
                this.modal.show();
            });
        }
        
    }

    onShown(): void {
        
        document.getElementById('PaymentOption_Name').focus();
    }

    save(): void {
            this.saving = true;

			
            this._paymentOptionsServiceProxy.createOrEdit(this.paymentOption)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
             });
    }







    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
