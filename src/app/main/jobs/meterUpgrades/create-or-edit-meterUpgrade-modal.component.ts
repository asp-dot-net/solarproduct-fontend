﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { MeterUpgradesServiceProxy, CreateOrEditMeterUpgradeDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';

@Component({
    selector: 'createOrEditMeterUpgradeModal',
    templateUrl: './create-or-edit-meterUpgrade-modal.component.html'
})
export class CreateOrEditMeterUpgradeModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    meterUpgrade: CreateOrEditMeterUpgradeDto = new CreateOrEditMeterUpgradeDto();



    constructor(
        injector: Injector,
        private _meterUpgradesServiceProxy: MeterUpgradesServiceProxy
    ) {
        super(injector);
    }

    show(meterUpgradeId?: number): void {

        if (!meterUpgradeId) {
            this.meterUpgrade = new CreateOrEditMeterUpgradeDto();
            this.meterUpgrade.id = meterUpgradeId;

            this.active = true;
            this.modal.show();
        } else {
            this._meterUpgradesServiceProxy.getMeterUpgradeForEdit(meterUpgradeId).subscribe(result => {
                this.meterUpgrade = result.meterUpgrade;


                this.active = true;
                this.modal.show();
            });
        }
        
    }

    save(): void {
            this.saving = true;

			
            this._meterUpgradesServiceProxy.createOrEdit(this.meterUpgrade)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
             });
    }







    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
