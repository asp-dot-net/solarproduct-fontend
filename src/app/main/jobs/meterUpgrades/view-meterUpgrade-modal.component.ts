﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetMeterUpgradeForViewDto, MeterUpgradeDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewMeterUpgradeModal',
    templateUrl: './view-meterUpgrade-modal.component.html'
})
export class ViewMeterUpgradeModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetMeterUpgradeForViewDto;


    constructor(
        injector: Injector
    ) {
        super(injector);
        this.item = new GetMeterUpgradeForViewDto();
        this.item.meterUpgrade = new MeterUpgradeDto();
    }

    show(item: GetMeterUpgradeForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
