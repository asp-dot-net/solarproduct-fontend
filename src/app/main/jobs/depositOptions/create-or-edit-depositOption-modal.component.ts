﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { DepositOptionsServiceProxy, CreateOrEditDepositOptionDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';

@Component({
    selector: 'createOrEditDepositOptionModal',
    templateUrl: './create-or-edit-depositOption-modal.component.html'
})
export class CreateOrEditDepositOptionModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    depositOption: CreateOrEditDepositOptionDto = new CreateOrEditDepositOptionDto();



    constructor(
        injector: Injector,
        private _depositOptionsServiceProxy: DepositOptionsServiceProxy
    ) {
        super(injector);
    }

    show(depositOptionId?: number): void {

        if (!depositOptionId) {
            this.depositOption = new CreateOrEditDepositOptionDto();
            this.depositOption.id = depositOptionId;

            this.active = true;
            this.modal.show();
        } else {
            this._depositOptionsServiceProxy.getDepositOptionForEdit(depositOptionId).subscribe(result => {
                this.depositOption = result.depositOption;


                this.active = true;
                this.modal.show();
            });
        }
        
    }

    onShown(): void {
        
        document.getElementById('DepositOption_Name').focus();
    }

    save(): void {
            this.saving = true;

			
            this._depositOptionsServiceProxy.createOrEdit(this.depositOption)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
             });
    }







    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
