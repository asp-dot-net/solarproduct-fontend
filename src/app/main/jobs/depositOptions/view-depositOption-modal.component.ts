﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetDepositOptionForViewDto, DepositOptionDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewDepositOptionModal',
    templateUrl: './view-depositOption-modal.component.html'
})
export class ViewDepositOptionModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetDepositOptionForViewDto;


    constructor(
        injector: Injector
    ) {
        super(injector);
        this.item = new GetDepositOptionForViewDto();
        this.item.depositOption = new DepositOptionDto();
    }

    show(item: GetDepositOptionForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
