import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { CreateOrEditJobDto, GetDocumentTypeForView, GetJobForEditOutput, DocumentRequestServiceProxy, QuotationsServiceProxy, SendDocumentRequestInputDto, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'documentRequestModal',
    templateUrl: './document-request-link.component.html'
})

export class DocumentRequestLinkModalComponent extends AppComponentBase {

    @ViewChild('documentrequestlinkModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    saving = false;
    DocumentTypes: GetDocumentTypeForView[];
    jobId:number;
    documentTypeId:number;
    jobNumber: string;
    sectionId: number;
    SectionName = '';

    constructor(
        injector: Injector,
        private _quotationsServiceProxy: QuotationsServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _documentRequestServiceProxy: DocumentRequestServiceProxy
    ) {
        super(injector);
    }

    show(jobId: number, jobNumber: any, sectionId: any,Section = ''): void {
        debugger;
        this.showMainSpinner();
        this.jobNumber = jobNumber;
        this.jobId = jobId;
        this.sectionId = sectionId;
        this.SectionName = Section;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote = 'Open Document Request ';
        log.section = Section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
        if (sectionId == 45) {
            this._quotationsServiceProxy.getEssentialActiveDocumentType().subscribe(result => {
                this.DocumentTypes = result;
                this.modal.show();
                this.hideMainSpinner();
            }, e => {
                this.hideMainSpinner();
            });
        } 
        else {
            this._quotationsServiceProxy.getActiveDocumentType().subscribe(result => {
                this.DocumentTypes = result;
                this.modal.show();
                this.hideMainSpinner();
            }, e => {
                this.hideMainSpinner();
            });
        } 
        // else if (sectionId == 8) {
        //     this._quotationsServiceProxy.getActiveDocumentType().subscribe(result => {
        //         this.DocumentTypes = result;
        //         this.modal.show();
        //         this.hideMainSpinner();
        //     }, e => {
        //         this.hideMainSpinner();
        //     });
        // }
        // else {
        //     this.hideMainSpinner();
        // }
    }
    

    save(sendMode: any): void {
        this.saving = true;

        let sendDocumentRequestInputDto = new SendDocumentRequestInputDto;
        sendDocumentRequestInputDto.jobId = this.jobId;
        sendDocumentRequestInputDto.docTypeId = this.documentTypeId;
        sendDocumentRequestInputDto.sectionId = this.sectionId;
        sendDocumentRequestInputDto.sendMode = sendMode

        let str = sendMode == 'Email' ? "Email Send Successfully" : "SMS Send Successfully"

        this._documentRequestServiceProxy.sendDocumentRequestForm(sendDocumentRequestInputDto).pipe(finalize(() => { this.saving = false;}))
        .subscribe(() => {
            let log = new UserActivityLogDto();
            log.actionId = 12;
            log.actionNote = "Document Request Sent On " + sendMode;
            log.section = this.SectionName;
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
            this.notify.info(str);
            this.close();
            this.modalSave.emit(null);
        });  
    }

    close(): void {
        this.modal.hide();
    }
}