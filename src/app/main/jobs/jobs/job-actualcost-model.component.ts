import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetLeadSourceForViewDto, LeadSourceDto, UserCallHistoryServiceProxy, GetAllCallFlowQueueCallHistoryDetailsDto, RescheduleInstallationsServiceProxy, GetRescheduleInstallationHistoryDto, JobsServiceProxy, UserActivityLogServiceProxy, UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { FileDownloadService } from '@shared/utils/file-download.service';


@Component({
    selector: 'JobActualCostModal',
    templateUrl: './job-actualcost-model.component.html'
})
export class JobActualCostModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    queueExtentionNumber = '';
    extentionNumber = '';
    item: GetRescheduleInstallationHistoryDto[];
   jobnumber = ''
    ActualCostList = [];
    constructor(
        injector: Injector,
        private _jobsServiceProxy: JobsServiceProxy
        ,private _fileDownloadService: FileDownloadService,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
    }

    show(ActualCostList : any[], jobnumber : string,section : string): void {
        let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote = 'Open Job Actual Cost';
            log.section = section;
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
        this.jobnumber = jobnumber;
        this.ActualCostList = ActualCostList;
        this.modal.show();
        this.active = true;
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    

}
function finalize(arg0: () => void): import("rxjs").OperatorFunction<void, unknown> {
    throw new Error('Function not implemented.');
}

