import { Component, ViewChild, Injector, Output, EventEmitter, OnInit} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { QuotationsServiceProxy, QuotationDataDto, CommonLookupServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
// import { NgxSpinnerService } from 'ngx-spinner';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { SendQuoteModalComponent } from './send-quote-email.component';
import * as _ from 'lodash';
import { AppConsts } from '@shared/AppConsts';
import { parse } from 'path';


@Component({
    selector: 'viewQuoteModel',
    templateUrl: './viewquotation.component.html'
})

export class ViewQuoteModalComponent extends AppComponentBase implements OnInit {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    // @ViewChild('sendQuoteModal', { static: true }) sendQuoteModal: SendQuoteModalComponent;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    quotationData: QuotationDataDto;

    saving = false;
   
    jobid:number = 0;
    quoteId:number = 0;
    leadId:number = 0;

    ngOnInit(): void {
        
      }

    constructor(
        injector: Injector,
        private _quotationServiceProxy: QuotationsServiceProxy,
        // private spinner: NgxSpinnerService,
        private sanitizer: DomSanitizer,
        private _quotationsServiceProxy: QuotationsServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _commonLookupServiceProxy: CommonLookupServiceProxy
    ) {
        super(injector);
        this.quotationData = new QuotationDataDto();
    }

    htmlContentBody: any;
    SectionName = '';

    show(jobid:number, quoteId?: number, leadId?: number,Section = ''): void {
        this.jobid = jobid;
        this.quoteId = quoteId;
        this.leadId = leadId;
        this.spinnerService.show();
        this.SectionName = Section;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote = 'View Quotation';
        log.section = Section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
        this._quotationServiceProxy.getQuotationDataByQuoateIdJobId(jobid, quoteId).subscribe(result => {
             debugger;
            console.log(result);
            this.quotationData = result;

            //this.setHtml(test);

            this.spinnerService.hide();
            this.modal.show();
            this.setHtml(result.viewHtml);

        },
        err => {
            this.spinnerService.hide();
        });
        
    }

    setHtml(emailHTML) {
        debugger;
        let htmlTemplate = this.getQuotation(emailHTML)
        this.htmlContentBody = htmlTemplate;
        this.htmlContentBody = this.sanitizer.bypassSecurityTrustHtml(this.htmlContentBody);

        // if(htmlTemplate != '')
        // {
        //     this.spinner.hide();
        //     this.modal.show();
        // }
    }

    getQuotation(emailHTML) {
        // debugger;
        let myTemplateStr = emailHTML;
        let TableList = [];

        if (this.quotationData.qunityAndModelLists != null) {
            this.quotationData.qunityAndModelLists.forEach(obj => {
                //TableList.push("<div class='mb5'>" + obj.name + "</div>");
                TableList.push("<li class='mb5'>" + obj.name + "</li>");
            })
        }

        let SolarVICRebate = this.quotationData.solarVICRebate != null && this.quotationData.solarVICRebate != "" ? this.quotationData.solarVICRebate : "0.00";
        let SolarVICLoan = this.quotationData.solarVICLoanDiscount != null && this.quotationData.solarVICLoanDiscount != "" ? this.quotationData.solarVICLoanDiscount : "0.00";

        let SystemWith = this.quotationData.capecity + " Kw Solar<br>Photovoltaic System include,";
        if(this.quotationData.systemWith == "Only Inverter") {
            SystemWith = "";
        }
        else if(this.quotationData.systemWith == "Only Battery" || this.quotationData.systemWith == "Panel Without Battery") {
            SystemWith = this.quotationData.batteryCapecity + " Kw Battery, <br>include,";
        }
        else if(this.quotationData.systemWith == "Panel With Battery") {
            SystemWith = this.quotationData.capecity + " Kw Solar, " + this.quotationData.batteryCapecity + " Kw Battery, <br>Photovoltaic System include,";
        }


        // if(this.quotationData.systemWith == "With Battery") {
        //     SystemWith = this.quotationData.capecity + " Kw Solar, " + this.quotationData.batteryCapecity + " Kw Battery, <br>Photovoltaic System include,";
        // }
        // else if(this.quotationData.systemWith == "Only Battery") {
        //     SystemWith = this.quotationData.batteryCapecity + " Kw Battery, <br>include,";
        // }
        // else if(this.quotationData.systemWith == "Only Inverter") {
        //     SystemWith = "";
        // }
        // else if(this.quotationData.systemWith == "Without Battery") {
        //     SystemWith = this.quotationData.capecity + " Kw Solar<br>Photovoltaic System include,";
        // }
        // else if(this.quotationData.systemWith == "Without Panel") {
        //     SystemWith = this.quotationData.batteryCapecity + " Kw Battery, <br>include,";
        // }

        let myVariables = {
            Q: {
                JobNumber: this.quotationData.quoteNumber,
                Date: this.quotationData.date,
                JobNotes: this.quotationData.notes,
                InverterLocation: this.quotationData.inverterLocation,
                NearmapSrc: this.quotationData.nearMap1 != null && this.quotationData.nearMap1 != "" ? (AppConsts.docUrl + this.quotationData.nearMap1) : "",
                RebateRefNo: this.quotationData.rebateRefNo,
                Cust: {
                    Name: this.quotationData.name,
                    Mobile: this.quotationData.mobile,
                    Email: this.quotationData.email,
                    AddressLine1: this.quotationData.address1,
                    AddressLine2: this.quotationData.address2,
                    MeterPhase: this.quotationData.meaterPhase,
                    MeterUpgrad: this.quotationData.meaterUpgrade,
                    RoofType: this.quotationData.roofTypes,
                    PropertyType: this.quotationData.noofStory,
                    RoofPitch: this.quotationData.roofPinch,
                    ElecDist: this.quotationData.energyDist,
                    ElecRetailer: this.quotationData.energyRetailer,
                    NIMINumber: this.quotationData.nmiNumber,
                    MeterNo: this.quotationData.meterNo
                },

                SolarAdviser: {
                    Name: this.quotationData.instName,
                    Mobile: this.quotationData.instMobile,
                    Email: this.quotationData.instEmail
                },

                PI: {
                    Capecity: this.quotationData.capecity,
                    // CapecityDescription: this.quotationData.capecityDescription,
                    TCost: this.quotationData.total != null && this.quotationData.total != "" ? this.quotationData.total : "0.00",
                    SystemDetailsTable: TableList.toString().replace(/,/g, ''),
                    SubTotal: this.quotationData.subTotal != null && this.quotationData.subTotal != "" ? this.quotationData.subTotal : "0.00",
                    Stc: this.quotationData.stc != null && this.quotationData.total != "" ? this.quotationData.stc : "0.00",
                    StcDesc: this.quotationData.stcDesc != null && this.quotationData.stcDesc != "" ? this.quotationData.stcDesc : "0.00",
                    GTotal: this.quotationData.grandTotal != null && this.quotationData.grandTotal != "" ? this.quotationData.grandTotal : "0.00",
                    ACharge: this.quotationData.additionalCharges != null && this.quotationData.additionalCharges != "" ? this.quotationData.additionalCharges : "0.00",
                    Discount: this.quotationData.specialDiscount != null && this.quotationData.specialDiscount != "" ? this.quotationData.specialDiscount : "0.00",
                    Deposit: this.quotationData.deposite != null && this.quotationData.deposite != "" ? this.quotationData.deposite : "0.00",
                    DepositeText: this.quotationData.depositeText,
                    Balance: this.quotationData.balance != null && this.quotationData.balance != "" ? this.quotationData.balance : "0.00",
                    TotalPriceStcRebate: this.quotationData.stcIncetive != null && this.quotationData.stcIncetive != "" ? this.quotationData.stcIncetive : "0.00",
                    SolarVICRebate: this.quotationData.solarVICRebate != null && this.quotationData.solarVICRebate != "" ? this.quotationData.solarVICRebate : "0.00",
                    SolarVICLoan: this.quotationData.solarVICLoanDiscount != null && this.quotationData.solarVICLoanDiscount != "" ? this.quotationData.solarVICLoanDiscount : "0.00",
                    VicRebate: this.quotationData.vicRebate,
                    VicWithRebate: this.quotationData.vicRebate == "With Rebate Install" ? "<Tr><Td>Less - Solar VIC Rebate *</Td><Td>$"+ SolarVICRebate +"</Td></Tr><Tr><Td>Less - Solar VIC Loan *</Td><Td>$"+ SolarVICLoan +"</Td></Tr>" : "",
                    System: SystemWith,
                    DiscountSep : (this.quotationData.specialDiscount != null && this.quotationData.specialDiscount != "" ? this.quotationData.specialDiscount : "0.00") != "0.00" ? "<Tr><Td>Special Discount</Td><Td>$ "+this.quotationData.specialDiscount+"</Td></Tr>" : "",
                    AChergeT : (this.quotationData.additionalCharges != null && this.quotationData.additionalCharges != "" ? this.quotationData.additionalCharges : "0.00")!= "0.00"? "<Tr><Td>Additional Charges</Td><Td>$ "+this.quotationData.additionalCharges+"</Td></Tr>" : "",
                    BatteryRebate : this.quotationData.batteryRebate >  0 ? "<Tr><Td>Battery Rebate (C)</Td><Td>$ "+this.quotationData.batteryRebate+"</Td></Tr>" : "",
                    GTotalLabel : this.quotationData.batteryRebate >  0 ? "Grand Total (A-B-C)" : "Grand Total (A-B)"
                },
                
                CustomerSign: {
                    SignSrc: this.quotationData.signSrc != null && this.quotationData.signSrc != "" ? (AppConsts.docUrl + this.quotationData.signSrc) : "" ,
                    Name: this.quotationData.signName,
                    Date: this.quotationData.signDate,
                },

                RepresentativeSign: {
                    SignSrc: this.quotationData.representativeSignSrc
                }
            }
        }
debugger;
        // use custom delimiter {{ }}
        _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;
    
        // interpolate
        let compiled = _.template(myTemplateStr);
        let myTemplateCompiled = compiled(myVariables);
        return myTemplateCompiled;
    }

    close(): void {
        this.modal.hide();
    }

    htmlstr: string = '';
    generatePdf(): void {

        this.saving = true;
        //this.spinner.show();
        this.spinnerService.show();
        this.htmlstr = this.htmlContentBody.changingThisBreaksApplicationSecurity;
        this._commonLookupServiceProxy.downloadQuote(this.quotationData.quoteNumber, "Quotation", this.htmlstr).subscribe(result => {
            let FileName = result;
            this.saving = false;
            this.spinnerService.hide();
            window.open(result, "_blank");
        },
        err => {
            this.spinnerService.hide();
        });
        

    }

    transform(value:string): string {
        let first = value.substr(0,1).toUpperCase();
        return first + value.substr(1); 
    }
}