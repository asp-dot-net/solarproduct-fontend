import { Component, ElementRef, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { JobsServiceProxy, GetJobForEditOutput, CreateOrEditJobDto, JobElecDistributorLookupTableDto, JobElecRetailerLookupTableDto, JobRoofAngleLookupTableDto, CommonLookupDto, LeadsServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import * as _ from 'lodash';
import * as moment from 'moment';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';
import { FileItem, FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { FileUpload } from 'primeng';
import { AppConsts } from '@shared/AppConsts';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
@Component({
    selector: 'jobNotesModal',
    templateUrl: './job-notes-model.component.html'
})
export class JobNotesModalComponent extends AppComponentBase {

    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    saving = false;
    distAppliedDate: moment.Moment;
    MeterNumber = "";
   
    item: GetJobForEditOutput;
    comment = "";
    jobElecDistributors: JobElecDistributorLookupTableDto[];
    jobElecRetailers: JobElecRetailerLookupTableDto[];
    jobRoofAngles: JobRoofAngleLookupTableDto[];
    allUsers: CommonLookupDto[];
    currentElecDistId: any;
    currentdistApproveBy: any;
    elecRetailerId: any;
    leadCompanyName: any;
    distExpiryDate: moment.Moment;
    updateExpiryDate: moment.Moment;
    ischange=false;
    trackerid = 0;
    filenName = [];
    public maxfileBytesUserFriendlyValue = 5;
    public uploader: FileUploader;
    t = 0;
    
    dbDistApprovalDate: any = null;
    private _uploaderOptions: FileUploaderOptions = {};
    //distdate: moment.Moment;
    constructor(
        injector: Injector,
        private _jobsServiceProxy: JobsServiceProxy,
        private _router: Router,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _tokenService: TokenService,
    ) {
        super(injector);
        this.item = new GetJobForEditOutput();
        this.item.job = new CreateOrEditJobDto();
    }


    sectionName = '';
    show(jobid: number, trackerid?: number,section = ''): void {
        this.t =0;
        this.dbDistApprovalDate = null;
        this.distExpiryDate = null;
        this.trackerid = trackerid;
        this.sectionName =section;
        let log = new UserActivityLogDto();
             log.actionId = 79;
             log.actionNote ='Open Application Tracker Notes';
             log.section = section;
             this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                 .subscribe(() => {
             });
        if (this.jobElecDistributors == null) {
            this._jobsServiceProxy.getAllElecDistributorForTableDropdown().subscribe(result => {
                this.jobElecDistributors = result;
            });
        }
        if (this.jobElecRetailers == null) {
            this._jobsServiceProxy.getAllElecRetailerForTableDropdown().subscribe(result => {
                this.jobElecRetailers = result;
            });
        }
        if (this.jobRoofAngles == null) {
            this._jobsServiceProxy.getAllRoofAngleForTableDropdown().subscribe(result => {
                this.jobRoofAngles = result;
            });
        }
        if (this.allUsers == null) {
            // this._jobsServiceProxy.getAllUsers().subscribe(result => {
            //     this.allUsers = result;
            // });
            this._jobsServiceProxy.getAllUsersTableDropdown().subscribe(result => {
                this.allUsers = result;
            });
        }
        this._jobsServiceProxy.getJobForEdit(jobid).subscribe(result => {
            this.item.job = result.job;
            this.item.job.empId = result.assignToUserID;
            this.item.job.approvalRef=result.job.approvalRef;
            this._leadsServiceProxy.getCurrentUserId().subscribe(r => {
                this.item.job.appliedBy = r;
            });
             this.leadCompanyName = result.leadCompanyName;
            this.distExpiryDate = this.item.job.distExpiryDate;
            this.dbDistApprovalDate = this.item.job.distApproveDate;
            this.initializeModal();
            this.modal.show();
        });
        // if(trackerid == 1)
        // {
        //     this.item.job.projectNotes.
        // }
    }

    currentuser(event) {
        const id = event.target.value;
        if (id != this.currentElecDistId || id != this.elecRetailerId) {
            this._leadsServiceProxy.getCurrentUserId().subscribe(result => {
                this.item.job.distApproveBy = result;
            });
        } else {
            this.item.job.distApproveBy = this.currentdistApproveBy;
        }
    }

    
    changeDate(event) {
       
        // if (this.distExpiryDate == null && this.item.job.distApproveDate != null) {
            
        //     let distdate = moment(event);
        //     this.distExpiryDate = moment(distdate).add(90, 'days');
        //     this.ischange==false;
        // }
        // else
        // { 
        //     let distdate = moment(event);
        //     this.updateExpiryDate = moment(distdate).add(90, 'days');
        //     this.ischange=true;
        // }

        if(this.t > 0)
        {
            // let distdate = moment(event).add(90, 'days');
            var end_date = moment(event).clone().add(90, 'days');
            this.item.job.distExpiryDate = end_date;
        }
        else if(this.dbDistApprovalDate == null) {
            var end_date = moment(event).clone().add(90, 'days');
            this.item.job.distExpiryDate = end_date;
            this.dbDistApprovalDate = this.item.job.distApproveDate;
        }

        this.t = this.t + 1;

        // let distdate = moment(event).add(90, 'days');
        // this.item.job.distExpiryDate = distdate;

        //this.item.job.distExpiryDate = distdate;

    }

    initializeModal(): void {
       
        this.initFileUploader();
    }
    
    
    filechangeEvent(event: any): void {
     
        if (event.target.files[0].size > 5242880) { //5MB
          this.message.warn(this.l('ProfilePicture_Warn_SizeLimit', this.maxfileBytesUserFriendlyValue));
          return;
        }
        this.uploader.clearQueue();
        this.uploader.addToQueue([<File>event.target.files[0]]);
    }

    initFileUploader(): void {
        this.uploader = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Profile/UploadFile' });
        this._uploaderOptions.autoUpload = false;
        this._uploaderOptions.authToken = 'Bearer ' + this._tokenService.getToken();
        this._uploaderOptions.removeAfterUpload = true;
        this.uploader.onAfterAddingFile = (file) => {
          file.withCredentials = false;
        };
    
        this.uploader.onBuildItemForm = (fileItem: FileItem, form: any) => {
          form.append('FileType', fileItem.file.type);
          form.append('FileName', 'ProfilePicture');
          form.append('FileToken', this.guid());
          this.filenName.push(fileItem.file.name);
        };
    
        this.uploader.onSuccessItem = (item, response, status) => {
          const resp = <IAjaxResponse>JSON.parse(response);
          if (resp.success) {
            this.updateInstallerInvoiceFile(resp.result.fileToken, resp.result.fileName, resp.result.fileType, resp.result.filePath);
          } else {
            this.message.error(resp.error.message);
          }
        };
    
        this.uploader.setOptions(this._uploaderOptions);
    }

    updateInstallerInvoiceFile(fileToken: string, fileName: string, fileType: string, filePath: string): void {
        this.item.job.fileToken = fileToken;
        this.item.job.fileName = this.filenName[0];
        this.item.job.sectionId = this.trackerid;

        this.item.job.distAppliedDate = this.distAppliedDate;
        
        this.item.job.additionalComments = this.comment;
        this.item.job.meterNumber = this.MeterNumber;
        
        this._jobsServiceProxy.updateDataFromTracker(this.item.job)
        .pipe(finalize(() => { this.saving = false; }))
        .subscribe(() => {
            let log = new UserActivityLogDto();
                log.actionId = 11;
                log.actionNote ='Job Data Updated From ' + this.sectionName;
                log.section = this.sectionName;
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            this.modal.hide();
            this.notify.info(this.l('SavedSuccessfully'));
            this.modalSave.emit(null);
            this.comment = "";
        });
    }

    guid(): string {
        function s4() {
          return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }
    
    save(): void {
       
        this.saving = true;
        // if(this.ischange==true)
        // {
        //     this.item.job.distExpiryDate = this.updateExpiryDate;
        // }
        // else{
        // this.item.job.distExpiryDate = this.distExpiryDate;
        // }

        if (this.uploader.queue.length == 0) {
            this.notify.warn(this.l('SelectFileToUpload'));
            this.saving = false; 
            return;
        }
       
        this.saving = true; 
        this.uploader.uploadAll();
        // this.item.job.distAppliedDate = this.distAppliedDate;
        
        // this.item.job.additionalComments = this.comment;
        // this.item.job.meterNumber = this.MeterNumber;
        // this._jobsServiceProxy.updateDataFromTracker(this.item.job)
        //     .pipe(finalize(() => { this.saving = false; }))
        //     .subscribe(() => {
        //         this.modal.hide();
        //         this.notify.info(this.l('SavedSuccessfully'));
        //         this.modalSave.emit(null);
        //         this.comment = "";
        //     });
    }

    close(): void {
        this.t = 0;
        this.modal.hide();
    }

}