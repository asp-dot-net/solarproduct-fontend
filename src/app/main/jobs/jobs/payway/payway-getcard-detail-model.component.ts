import { Component, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLookupDto, CommonLookupServiceProxy, InvoicePaymentsServiceProxy, JobsServiceProxy, OrganizationUnitDto, OrganizationUnitServiceProxy, PayWayTokenData, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'paywaygetcarddetailModel',
  templateUrl: './payway-getcard-detail-model.component.html',
  styleUrls: ['./payway-getcard-detail-model.component.css']
})
export class PaywayGetcardDetailModelComponent extends AppComponentBase {
  @ViewChild('viewModel', { static: true }) modal: ModalDirective;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  active = false;
  saving = false;
  jobId = 0;
  allOrganizationUnits: OrganizationUnitDto[];
  paywaydata: PayWayTokenData = new PayWayTokenData();
  organizationUnit = 0;
  withorwithoutSurcharge = 0;
  organizationUnitlength: number = 0;
  allInvoicePaymentStatus:CommonLookupDto[];
  sectionId = 0;
  constructor(
    injector: Injector,
    private _jobsServiceProxy: JobsServiceProxy,
    private _commonLookupService: CommonLookupServiceProxy,
    private _organizationUnitService: OrganizationUnitServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _invoicePaymentsServiceProxy: InvoicePaymentsServiceProxy,
    spinner: NgxSpinnerService,
  ) {
    super(injector);
  }
  SectionName = '';

  show(jobid: number, orgId: number,withorwithoutsurcharge: number, sectionId? :number,Section = ''): void {
    debugger;
    this.jobId = jobid;
    this.sectionId = sectionId;
    this.withorwithoutSurcharge = withorwithoutsurcharge;
    this._invoicePaymentsServiceProxy.getAllInvoiceStatusForTableDropdown().subscribe(result => {						
      this.allInvoicePaymentStatus = result;
    });
    this._commonLookupService.getOrganizationUnit().subscribe(output => {
      this.allOrganizationUnits = output;
      this.organizationUnit = orgId;
      this.onChangeorganizationUnit(orgId);
      this.active = true;
      this.modal.show();
    });
    this.SectionName =Section;
      let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote = 'Open Add PayWay Details';
        log.section = Section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
  }

  saveJobInstallerInvoic(): void {
    debugger;
    this.paywaydata.jobid = this.jobId;
    this.paywaydata.withOrWithoutSurcharge = this.withorwithoutSurcharge;
    this.paywaydata.sectionId = this.sectionId;
    this._jobsServiceProxy.createPaywayData(this.paywaydata).subscribe((result) => {
      let log = new UserActivityLogDto();
        log.actionId = 16;
        log.actionNote = 'Invoice Payment taken from Payway';
        log.section = this.SectionName;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
      this.notify.info(this.l('Transaction Sucessfull'));
      this.close();
      this.modalSave.emit(null);
    });
  }

  close(): void {
    this.active = false;
    this.modal.hide();
  }

  onChangeorganizationUnit(organizationId): void {
    this._organizationUnitService.getOrganizationDetailsById(organizationId).subscribe(result => {
      console.log('getOrganizationDetailsById', result);
      if (result) {
        this.paywaydata.paymentMethod = result.paymentMethod;
        this.paywaydata.cardNumber = result.cardNumber;
        this.paywaydata.expiryDateMonth = parseInt(result.expiryDateMonth);
        this.paywaydata.expiryDateYear = parseInt(result.expiryDateYear);
        this.paywaydata.cvn = result.cvn;
        this.paywaydata.cardholderName = result.cardholderName;
      }
    });
  }
}
