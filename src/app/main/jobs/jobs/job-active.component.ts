import { Component, ElementRef, EventEmitter, Injector, Output, ViewChild, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { JobsServiceProxy, GetJobForEditOutput, CreateOrEditJobDto, CommonLookupDto, LeadsServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import * as _ from 'lodash';
import * as moment from 'moment';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
    selector: 'jobActiveModel',
    templateUrl: './job-active.component.html'
})
export class ActiveJobModalComponent extends AppComponentBase {

    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    saving = false;
    item: GetJobForEditOutput;
    leadCompanyName: any;
    showDepReceived: boolean = false;
    showActive: boolean = false;
    saveBtn = true;

    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _jobsServiceProxy: JobsServiceProxy,
    ) {
        super(injector);
        this.item = new GetJobForEditOutput();
        this.item.job = new CreateOrEditJobDto();
    }

    sectionName = '';
    show(jobId: number,section =''): void {
        this.sectionName = section;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Open Active Tracker Notes';
        log.section = section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
        this.item.job = new CreateOrEditJobDto();
        this.showDepReceived = false;
        this.showActive = false;
        this.saveBtn = false;
        this.saving = false;

        this._jobsServiceProxy.getJobForEdit(jobId).subscribe(result => {
         
            this.item.job = result.job;
            this.leadCompanyName = result.leadCompanyName;

            this._jobsServiceProxy.getJobActiveStatus(this.item.job.id).subscribe(res => { 
                if (res.houseTypeId && res.elecDistributorId && res.roofAngleId && res.roofTypeId && result.job.firstDepositDate) {
                    this.showDepReceived = true;
                }

                if(res.paymentType && res.payementoptionid == 1)
                {
                    if (result.job.firstDepositDate && result.job.depositeRecceivedDate && res.meterUpdgrade && res.meterBoxPhoto && res.signedQuote && res.finance && res.elecRetailerId && res.peakMeterNos && res.distApproved && res.paymentVerified) {
                        if (res.state == 'VIC' && res.vicRebate == 'With Rebate Install') {
                            if (res.solarRebateStatus && res.expiryDate) {
                                this.showActive = true;
                            }
                            else {
                                this.showActive = false;
                            }
                        }
                        else {
                            this.showActive = true;
                        }
                    }
                }
                else {
                    if (result.job.firstDepositDate && result.job.depositeRecceivedDate && res.meterUpdgrade && res.meterBoxPhoto && res.signedQuote && res.finance && res.elecRetailerId && res.peakMeterNos && res.distApproved) {
                        if (res.state == 'VIC' && res.vicRebate == 'With Rebate Install') {
                            if (res.solarRebateStatus && res.expiryDate) {
                                this.showActive = true;
                            }
                            else {
                                this.showActive = false;
                            }
                        }
                        else {
                            this.showActive = true;
                        }
                    }
                }
                
                if (result.job.depositeRecceivedDate == null && this.showDepReceived == true) {
                    this.saveBtn = false;
                }
                else if (result.job.activeDate == null && this.showActive == true) {
                    this.saveBtn = false;
                }
                else {
                    this.saveBtn = true;
                }
                this.modal.show();
            });

            
        });
    }

    save(): void {
        this.saving = true;
        // this.item.job.activeDate = this.activeDate;
        // this.item.job.depositeRecceivedDate = this.depositeRecivedDate;
        // this.item.job.firstDepositDate = this.firstDepositDate;
        this.item.job.section = this.sectionName;
        this._jobsServiceProxy.activeJob(this.item.job)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {

                this.modal.hide();
                this.notify.info(this.l('SavedSuccessfully'));
                
                this.modalSave.emit(null);
            });
    }



    close(): void {
        this.modal.hide();
    }

}