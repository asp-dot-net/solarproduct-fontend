import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobActiveSmsEmailModelComponent } from './job-active-sms-email-model.component';

describe('JobActiveSmsEmailModelComponent', () => {
  let component: JobActiveSmsEmailModelComponent;
  let fixture: ComponentFixture<JobActiveSmsEmailModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobActiveSmsEmailModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobActiveSmsEmailModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
