import { Component, ViewChild, Injector, Output, EventEmitter, OnInit, ElementRef} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { CreateOrUpdateGreenDealJobOutputDto, ProductPackageServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
// import { NgxSpinnerService } from 'ngx-spinner';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import * as _ from 'lodash';
import { AppConsts } from '@shared/AppConsts';

@Component({
    selector: 'greenDealErrorModal',
    templateUrl: './greendealerror.component.html'
})

export class GreenDealErrorModalComponent extends AppComponentBase implements OnInit {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    errorList: any[] = [];
    errorMessage: string;
    //saving = false;
   
    ngOnInit(): void {
        this.errorList = [];
    }

    constructor(
        
        injector: Injector,
        private _productPackageServiceProxy: ProductPackageServiceProxy
    ) {
        super(injector);
    }

    show(result: CreateOrUpdateGreenDealJobOutputDto): void {
        // this.spinnerService.show()
        // 
        // this._productPackageServiceProxy.getAllPackageDetails().subscribe(result => {
        //     this.productPackages = result;
        //     this.modal.show();
        //     this.spinnerService.hide();
        // },
        // error => {
        //     this.spinnerService.hide();
        // });
        this.errorList = [];
        if(result.errorList != null)
        {
            this.errorList = result.errorList;
        }
        this.errorMessage = result.message;
        this.modal.show();
            // this.spinnerService.hide();
    }

    close(): void {
        this.modal.hide();
    }

    expandcollapse(index, item: any): void {

        if(item.description != null) {
            var divName = 'div' + index;
            var iName = 'i' + index;
            var div = document.getElementById(divName);
            var i = document.getElementById(iName);
    
            if (div.style.display == "none") {
                div.style.display = "inline";
                i.className = 'las la-chevron-circle-down fs-24';
            } else {
                div.style.display = "none";
                i.className = 'las la-chevron-circle-right fs-24';
            }
        }
    }

    // select(id): void {
    //     this.modalSave.emit(id);
    //     this.modal.hide();
    // }
}