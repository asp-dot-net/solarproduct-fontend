import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { CommonLookupDto,QuickstockServiceProxy, CommonLookupServiceProxy, EditPickListDto, GetPicklistItemForReport, InstallerServiceProxy, JobProductItemProductItemLookupTableDto, JobProductItemsServiceProxy, JobsServiceProxy, PickListServiceProxy, ProductItemsServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { NgxSpinnerService } from 'ngx-spinner';
import { forEach, values } from 'lodash';

@Component({
    selector: 'editPickListModal',
    templateUrl: './edit-picklist-model.component.html'
})

export class CreateOrEditPickListModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    saving = false;
    receiveddate: moment.Moment = moment().add(0, 'days').endOf('day');
    PicklistProducts: GetPicklistItemForReport[];
    PicklistProductsList: GetPicklistItemForReport[];
    // productItems: JobProductItemProductItemLookupTableDto[];
    productTypes: CommonLookupDto[];
    
    picklist: EditPickListDto = new EditPickListDto();
    picklistid: number;
    state: string;
    jobid = 0;
    sectionid = 0;
    picklistinstallationList: CommonLookupDto[];
    warehouselocationlist: CommonLookupDto[];
    picklistinstallerId = 0;
    picklistwarehouseLocation = 0;
    stockWithId = 0;
    pickID = 0;
    disbaleifsalerep = false;

    installerName = '';
    sectionName = '';
    productItemSuggestions: any[];
    constructor(
        injector: Injector,
        private _picklistServiceProxy: PickListServiceProxy,
        private _jobServiceProxy: JobsServiceProxy,
        private spinner: NgxSpinnerService,
        private _commonLookupService: CommonLookupServiceProxy,
        private _installerServiceProxy: InstallerServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _quickstockServiceProxy: QuickstockServiceProxy
    ) {
        super(injector);
    }
    show(picklistid: number, state: string, jobid: number, sectionId: number, organizationId: number,Section = ''): void {
        this.sectionName = Section;
        this.state = state;
        this.jobid = jobid;
        this.sectionid = sectionId;
        this.picklistid = picklistid;
        if(this.picklistinstallationList == null || this.picklistinstallationList == undefined){
            this._installerServiceProxy.getAllInstallers(organizationId).subscribe(result => {
                this.picklistinstallationList = result;
            });
        }
        
        if(this.warehouselocationlist == null)
        {
            this._jobServiceProxy.getWareHouseDropdown(state).subscribe(result => {
                this.warehouselocationlist = result;
            });
        }
        if(this.productTypes == null)
        {
            this._commonLookupService.getAllProductTypeForTableDropdown().subscribe(result => {
                this.productTypes = result;
            });
        }
        
        // if(this.productItems == null)
        // {
        //     this._commonLookupService.getAllProductItemForTableDropdown().subscribe(result => {
        //         this.productItems = result;
        //     });
        // }
       
        this.spinner.show();
        this._picklistServiceProxy.getPickListForReport(picklistid).subscribe(result => {
            console.log(result);
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote = 'Open For Edit Picklist';
            log.section = Section;
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
            this.PicklistProducts = result;
            this.picklist.pickListResultList = result;
            this.picklistinstallerId  =  this.picklist.pickListResultList[0].picklistinstallerId;
            this.picklistwarehouseLocation  = this.picklist.pickListResultList[0].picklistwarehouseLocation;
            this.stockWithId = this.picklist.pickListResultList[0].stockWith;
            this.pickID = this.picklist.pickListResultList[0].pickID;
            
            this._installerServiceProxy.getAllInstallers(organizationId).subscribe(result => {
                let pItem = result.find(x => x.id == this.picklistinstallerId)
                this.installerName = pItem.displayName;
                console.log(this.installerName)
            });
            this.getStockOnHandForAllItemIntial();
            this.modal.show();
            this.spinner.hide();
        }, e => { this.spinner.hide(); });
    }
    
    getStockOnHandForAllItem() {

        if(!this.checkStockOnHandFeatures())
        {
            return;
        }

        for (let i = 0; i < this.PicklistProducts.length; i++) {
            this._quickstockServiceProxy.getStockDetails(this.PicklistProducts[i].productItemId, this.picklistwarehouseLocation).subscribe(result => {
                this.PicklistProducts[i].stock = result.stockOnHand;
                this.checkStockOnHandWithQty(i, this.PicklistProducts[i].qty, this.PicklistProducts[i].stock)
            });
        }
    }

    getStockOnHandForAllItemIntial() {

        if(!this.checkStockOnHandFeatures())
        {
            return;
        }

        for (let i = 0; i < this.PicklistProducts.length; i++) {
            this._quickstockServiceProxy.getStockDetails(this.PicklistProducts[i].productItemId, this.picklistwarehouseLocation).subscribe(result => {
                this.PicklistProducts[i].stock = result.stockOnHand;
            });
        }
    }

    // Called When Picklist Product Item Entered.
    filterPicklistProductIteams(event, i): void {
        let Id = this.PicklistProducts[i].productTypeId;

        this._jobServiceProxy.getPicklistProductItemList(Id, event.query).subscribe(output => {
            this.productItemSuggestions = output;
        });
    }

    // getPickListProductTypeId(i) {
    //     let pItem = this.productItems.find(x => x.displayName == this.PicklistProducts[i].item)
    //     this.PicklistProducts[i].productItemId = pItem.id;
    // }

    // Called When Picklist Product Item Selected.
    selectPicklistProductItem(event, i) {

        this.PicklistProducts[i].productItemId = event.id;
        this.PicklistProducts[i].item = event.productItem;
        this.PicklistProducts[i].model = event.productModel;
        this._quickstockServiceProxy.getStockDetails(this.PicklistProducts[i].productItemId, this.picklistwarehouseLocation).subscribe(result => {
            this.PicklistProducts[i].stock = result.stockOnHand;
            this.checkStockOnHandWithQty(i, this.PicklistProducts[i].qty, this.PicklistProducts[i].stock)
        });
        // this.PicklistProducts[i].size = event.size;

        // if(this.checkStockOnHandFeatures())
        // {
        //     this._quickstockServiceProxy.getStockDetails(this.PicklistProducts[i].productItemId, this.picklistwarehouseLocation).subscribe(result => {
        //         this.PicklistProducts[i].stock = result.stockOnHand;
        //     });
        // }

        // this.checkStockOnHandWithQty(i, this.PicklistProducts[i].quantity, this.PicklistProducts[i].stock)
    }

    addPicklistProduct(): void {
        this.PicklistProducts.push(new GetPicklistItemForReport);
    }

    save(): void {

        this.saving = true;
        this.picklist.editedPickList = [];
        this.picklist.jobId = this.jobid;
        this.picklist.sectionId = this.sectionid;
        this.picklist.picklistinstallerId = this.picklistinstallerId;
        this.picklist.picklistwarehouseLocation = this.picklistwarehouseLocation;
        this.picklist.stockWithId = this.stockWithId;
        this.picklist.pickID = this.pickID
        this.PicklistProducts.map((item) => {
            let GetPicklistItem = new GetPicklistItemForReport();
            GetPicklistItem.productTypeId = item.productTypeId;
            GetPicklistItem.productItemId = item.productItemId;
            GetPicklistItem.qty = item.qty;
            GetPicklistItem.model = item.model;
            GetPicklistItem.item = item.item;
            GetPicklistItem.description = item.description;
            GetPicklistItem.pickListId = item.pickListId;
            GetPicklistItem.stock = item.stock;
            this.picklist.editedPickList.push(GetPicklistItem);
        });
        this._picklistServiceProxy.update(this.picklist)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                let log = new UserActivityLogDto();
                log.actionId = 33;
                log.actionNote = 'PickList Modified';
                log.section = this.sectionName;
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
                this.modal.hide();
                this.modalSave.emit(null);
                this.notify.info(this.l('SavedSuccessfully'));
            });
    }

    close(): void {
        this.modal.hide();
    }

    removePicklistProduct(JobProduct): void {
        debugger;

        if (this.PicklistProducts.length == 1)
            return;
        this.PicklistProducts = this.PicklistProducts.filter(item => item.productItemId != JobProduct.productItemId);

    }

    installerSearchResult: any [];
    filterInstaller(event): void {
        this.installerSearchResult = Object.assign([], this.picklistinstallationList).filter(
            item => item.displayName.toLowerCase().indexOf(event.query.toLowerCase()) > -1
         )
    }

    selectInstaller(event): void {
        this.picklistinstallerId = event.id;
        this.installerName = event.displayName;
    }


    checkStockOnHandWithQty(index, qty, stockOnHand) {

        if(qty == '' || qty == 0)
        {
            this.PicklistProducts[index].qty = 0;
            return;
        }

        if(!this.checkStockOnHandFeatures())
        {
            return;
        }

        if(this.appSession.user.id == 2 || this.appSession.user.id == 51)
        {
            return;
        }

        if(stockOnHand < qty) {
            this.PicklistProducts[index].qty = 0;
            this.notify.warn("Stock quantity grater then live stock.", "Stock Warning!");
        }
    }

    checkStockOnHandFeatures() {
        return this.feature.isEnabled("App.StockManagement.Picklist.CheckStockOnHand");
    }

}