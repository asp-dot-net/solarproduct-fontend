import { Component, ViewChild, Injector, Output, EventEmitter, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {  JobSuggestedProductItemDto, CreateOrEditJobSuggestedProductDto, OrganizationUnitDto, CommonLookupServiceProxy, CommonLookupDto
, JobsServiceProxy, UserActivityLogServiceProxy, UserActivityLogDto ,
JobCostServiceProxy,
JobSuggestedProductServiceProxy} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as _ from 'lodash';

@Component({
    selector: 'suggestedProductModal',
    templateUrl: './suggested-product-modal.component.html'
})
export class SuggestedProductModalComponent extends AppComponentBase implements OnInit {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    jobId: number;
    sysCapacity: number;
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnitlength: number=0;
    date = new Date();
    JobSuggestedProductItemList: any[];
    JobSuggestedProducts: any[];
    JobSuggestedProduct: CreateOrEditJobSuggestedProductDto = new CreateOrEditJobSuggestedProductDto();
    productTypes: CommonLookupDto[];
    productItemSuggestions: any[];


    constructor(
        injector: Injector,
        private _jobSuggestedProductServiceProxyServiceProxy: JobSuggestedProductServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _jobServiceProxy: JobsServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
        this.JobSuggestedProduct = new CreateOrEditJobSuggestedProductDto();
    }

    ngOnInit(): void {
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
            // this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
        });

        this._commonLookupService.getAllProductTypeForTableDropdown().subscribe(result => {
            this.productTypes = result;
        });
    }
    onShown(): void {
        document.getElementById('Name').focus();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
    show( jobId?: number): void {
        debugger;
     
        this.jobId=jobId;
            this.JobSuggestedProduct = new CreateOrEditJobSuggestedProductDto();
            this.JobSuggestedProduct.id = null;
            this.JobSuggestedProduct.jobId=jobId;
            
            this.JobSuggestedProductItemList = [
                { id: 0, productTypeId: 0, productItemId: 0, productItemName: "", amount:0, productModel: "",size:0 }
            ];
        

        this.loadSuggestedProducts();
        this.active = true;
        this.modal.show();
        let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Suggested Products';
            log.section = 'Jobs';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
    }
    Reset(): void{
        this.JobSuggestedProduct = new CreateOrEditJobSuggestedProductDto();
        this.JobSuggestedProduct.id = null;
        this.JobSuggestedProduct.jobId=this.jobId;
        this.JobSuggestedProductItemList = [
            { id: 0, productTypeId: 0, productItemId: 0, productItemName: "", amount:0, productModel: "" }
        ];
    }
    edit(id?: number){
    debugger
        this._jobSuggestedProductServiceProxyServiceProxy.getJobSuggestedForEdit(id).subscribe((result) => {
            this.JobSuggestedProduct = result;
            
            this.JobSuggestedProductItemList = result.jobSuggestedProductItemDto.map((item) => ({
                id: item.id,
                productTypeId: item.productTypeId,
                productItemId: item.productItemId,
                productItemName: item.productItem,
                quantity: item.quantity,
                productModel: item.modelName,
                size: item.size

            }));
    this.calculateSystemCapacity();
    })}
    
    loadSuggestedProducts(): void {
        this.showMainSpinner();
        this._jobSuggestedProductServiceProxyServiceProxy.getAllByJobId(this.jobId).subscribe((result) => {
          this.JobSuggestedProducts = result;
          this.hideMainSpinner();
        });
    }
    save(): void {
        debugger
        this.saving = true;
        this.JobSuggestedProduct.jobSuggestedProductItemDto = [];
        this.JobSuggestedProductItemList.map((item) => {
            let JobSuggestedProductItem = new JobSuggestedProductItemDto();
            JobSuggestedProductItem.id = item.id;
            JobSuggestedProductItem.productItemId = item.productItemId;
            JobSuggestedProductItem.productItem = item.productItemName;
            JobSuggestedProductItem.quantity= item.quantity;
            JobSuggestedProductItem.modelName= item.productModel;
            JobSuggestedProductItem.size=item.size;
            this.JobSuggestedProduct.jobSuggestedProductItemDto.push(JobSuggestedProductItem);
        });
        this._jobSuggestedProductServiceProxyServiceProxy.createOrEdit(this.JobSuggestedProduct)
        .pipe(finalize(() => { this.saving = false; }))
        .subscribe(() => {
            this.notify.info(this.l('SavedSuccessfully'));
            this.modalSave.emit(null);
            if(this.JobSuggestedProduct.id){
                let log = new UserActivityLogDto();
                log.actionId = 82;
                log.actionNote ='Jobs Suggested Product Updated : '+ this.JobSuggestedProduct.name;
                log.section = 'Jobs';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }else{
                let log = new UserActivityLogDto();
                log.actionId = 81;
                log.actionNote ='Jobs Suggested Product Created : '+ this.JobSuggestedProduct.name;
                log.section = 'Jobs';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }
            this.hideMainSpinner();
            this.loadSuggestedProducts()
            this. Reset();
        },
        e => {
            this.notify.error(this.l('SaveFailed'));
        });
    }
    filterProductIteams(event, i): void {
        let Id = this.JobSuggestedProductItemList[i].productTypeId;

        this._jobServiceProxy.getPicklistProductItemList(Id, event.query).subscribe(output => {

            debugger;
            var items =this.JobSuggestedProductItemList.map(i=>i.productItemName+'');
            this.productItemSuggestions = output.filter(x=> !items.includes(x.productItem))
        });
    }
     selectProductItem(event, i) {

        this.JobSuggestedProductItemList[i].productItemId = event.id;
        this.JobSuggestedProductItemList[i].productItemName = event.productItem;
        this.JobSuggestedProductItemList[i].productModel = event.productModel;
        this.JobSuggestedProductItemList[i].size = event.size;
    }
    removeProductItem(ProductItem): void {
        if (this.JobSuggestedProductItemList.length == 1)
            return;

        if (this.JobSuggestedProductItemList.indexOf(ProductItem) === -1) {

        } else {
            this.JobSuggestedProductItemList.splice(this.JobSuggestedProductItemList.indexOf(ProductItem), 1);
        }
     this. calculateSystemCapacity();
    }
    addProductItem(): void {
        let JobSuggestedProducts = { id: 0, productTypeId: 0, productItemId: 0, productItemName: "", amount:0, productModel: "",size:0 }
        this.JobSuggestedProductItemList.push(JobSuggestedProducts);

    }
    changeProductType(i) : void {
        this.JobSuggestedProductItemList[i].productItemId = "";
        this.JobSuggestedProductItemList[i].productItemName = "";
        this.JobSuggestedProductItemList[i].amount = 0;
        this.JobSuggestedProductItemList[i].productModel ="";
        this.JobSuggestedProductItemList[i].size =0;
    }
    delete(Id?: number): void {

        this.message.confirm(
            this.l('AreYouSureWanttoDelete'),
            this.l('Delete'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._jobSuggestedProductServiceProxyServiceProxy.delete(Id)
                        .subscribe(() => {

                            this.notify.success(this.l('SuccessfullyDeleted'));
                            this.loadSuggestedProducts()

                        });
                }
            }
        );
    }
    calculateSystemCapacity() {
        let totalCapacity = 0;
    
        setTimeout(() => {
            this.JobSuggestedProductItemList.forEach((item) => {
                if (item.productTypeId == 1) {
                    const quantity = item.quantity || 0; 
                    const size = item.size || 0;  
                    const capacity = (quantity * size) / 1000;
                    totalCapacity += isNaN(capacity) ? 0 : capacity;
                }
            });
    
            this.JobSuggestedProduct.sysCapacity = totalCapacity;
        });
    }
    
   
}



