import { Component, Injector, ViewEncapsulation, ViewChild, Input, HostListener } from '@angular/core';
import { CommonLookupServiceProxy, GetJobForViewDto, JobPaymentOptionLookupTableDto, JobsServiceProxy, OrganizationUnitDto, UserServiceProxy, 
    JobTrackerServiceProxy, LeadsServiceProxy,
    JobStatusTableDto,
    CommonLookupDto, LeadUsersLookupTableDto,
    UserActivityLogDto,
    UserActivityLogServiceProxy} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import * as _ from 'lodash';
import { OnInit } from '@angular/core';
import { ActiveJobModalComponent } from './job-active.component';
import * as moment from 'moment';
import { ViewApplicationModelComponent } from './view-application-model/view-application-model.component';
import { FileDownloadService } from '@shared/utils/file-download.service';
// import { AddActivityModalComponent } from '@app/main/myleads/myleads/add-activity-model.component';
// import { JobActiveSmsEmailModelComponent } from './job-active-sms-email-model/job-active-sms-email-model.component';
// import { ViewMyLeadComponent } from '@app/main/myleads/myleads/view-mylead.component';
import { Title } from '@angular/platform-browser';
import { JobActiveModalComponent } from './check-job-active.component';
import { CommentModelComponent } from '@app/main/activitylog/comment-modal.component';
import { EmailModelComponent } from '@app/main/activitylog/email-modal.component';
import { NotifyModelComponent } from '@app/main/activitylog/notify-modal.component';
import { ReminderModalComponent } from '@app/main/activitylog/reminder-modal.component';
import { SMSModelComponent } from '@app/main/activitylog/sms-modal.component';
import { ToDoModalComponent } from '@app/main/activitylog/todo-modal.component';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './job-active-request.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class JobActiveRequestComponent extends AppComponentBase implements OnInit {

    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('jobActiveModel', { static: true }) jobActiveModel: ActiveJobModalComponent;
    @ViewChild('viewApplicationModal', { static: true }) viewApplicationModal: ViewApplicationModelComponent;
    // @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    // @ViewChild('addjobactiveSmsEmailModal', { static: true }) addjobactiveSmsEmailModal: JobActiveSmsEmailModelComponent;
    // @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;

    @ViewChild('jobActiveModal', { static: true }) jobActiveModal: JobActiveModalComponent;
    @ViewChild('smsModel', { static: true }) smsModel: SMSModelComponent;
    @ViewChild('emailModel', { static: true }) emailModel: EmailModelComponent;
    @ViewChild('notifyModel', { static: true }) notifyModel: NotifyModelComponent;
    @ViewChild('commentModel', { static: true }) commentModel: CommentModelComponent;
    @ViewChild('todoModal', { static: true }) todoModal: ToDoModalComponent;
    @ViewChild('ReminderModal', { static: true }) ReminderModal: ReminderModalComponent;
    // @ViewChild('JobActiveAddNoteModel', { static: true }) JobActiveAddNoteModel: JobActiveModalNotesComponent;
    filteredReps: LeadUsersLookupTableDto[];
    salesRepId = 0;
    role = '';
    leadStatusId = 0;
    dateFilterType = 'Creation';

    advancedFiltersAreShown = false;
    filterText = '';
    maxPanelOnFlatFilter: number;
    maxPanelOnFlatFilterEmpty: number;
    minPanelOnFlatFilter: number;
    minPanelOnFlatFilterEmpty: number;
    maxPanelOnPitchedFilter: number;
    maxPanelOnPitchedFilterEmpty: number;
    minPanelOnPitchedFilter: number;
    minPanelOnPitchedFilterEmpty: number;
    regPlanNoFilter = '';
    lotNumberFilter = '';
    peakMeterNoFilter = '';
    offPeakMeterFilter = '';
    enoughMeterSpaceFilter = -1;
    isSystemOffPeakFilter = -1;
    suburbFilter = '';
    stateFilter = '';
    jobTypeNameFilter = '';
    jobStatusNameFilter = '';
    roofTypeNameFilter = '';
    roofAngleNameFilter = '';
    elecDistributorNameFilter = '';
    leadCompanyNameFilter = '';
    elecRetailerNameFilter = '';
    paymentOptionNameFilter = '';
    depositOptionNameFilter = '';
    meterUpgradeNameFilter = '';
    meterPhaseNameFilter = '';
    promotionOfferNameFilter = '';
    houseTypeNameFilter = '';
    financeOptionNameFilter = '';
    OpenRecordId: number = 0;
    organizationUnit = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    _entityTypeFullName = 'TheSolarProduct.Jobs.Job';
    entityHistoryEnabled = false;
    projectFilter = '';
    stateNameFilter = '';
    
    dateType = "DepositeReceivedDate";
    date = new Date();
    firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    // startDate = moment(this.firstDay);
    // endDate = moment().endOf('day');
    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);

    jobStatusIDFilter: [];
    elecDistributorId = 0;
    applicationstatus = 3;
    jobTypeId = 0;
    totalActiveData = 0;
    totalActiveVerifyData = 0;
    totalActiveDatanotVerifyData = 0;
    totalActiveDatanotAwaitingData = 0;
    totalcount: GetJobForViewDto;
    ExpandedView: boolean = true;
    SelectedLeadId: number = 0;
    paymentid = 0;
    state = "";
    areaNameFilter = "";
    postalcodefrom = "";
    postalcodeTo = "";
    jobPaymentOptions: JobPaymentOptionLookupTableDto[];
    // excelorcsvfile = 0;
    firstrowcount = 0;
    last = 0;
    FiltersData = false;

    alljobstatus: JobStatusTableDto[];
    allState: CommonLookupDto[];

    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 330;
    toggle: boolean = true;
    filterName = "JobNumber";
    orgCode = '';
    change1() {
        this.toggle = !this.toggle;
      }
    
    constructor(
        injector: Injector,
        private _jobsServiceProxy: JobsServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _userServiceProxy: UserServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private titleService: Title,
        private _jobTrackerServiceProxy: JobTrackerServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy
        ) {
            super(injector);
            this.titleService.setTitle(this.appSession.tenancyName + " |  Job Active Requests");
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.orgCode = this.allOrganizationUnits[0].code;
            this.getJobs();
        });
        this._commonLookupService.getAllPaymentOptionForTableDropdown().subscribe(result => {
            this.jobPaymentOptions = result;
        });

        this._commonLookupService.getAllJobStatusTableDropdown().subscribe(result => {
            this.alljobstatus = result;
        });

        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allState = result;
        });
        this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
            this.role = result;
            if (this.role == 'Sales Rep') {
                this.leadStatusId = 10;
                this.dateFilterType = 'Assign';
            }
            
        });

        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Job Active Tracker';
            log.section = 'Job Active Tracker';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.orgCode = this.allOrganizationUnits[0].code;

            this.onsalesrep();

        });
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }
        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }
    changeOrgCode() {
        this.orgCode = this.allOrganizationUnits.find(x => x.id == this.organizationUnit).code;
    }
    clear() {
        this.filterText = '';
        this.financeOptionNameFilter = '';
        this.state = '';

        this.startDate = moment(this.date);
        this.endDate = moment(this.date);

        this.getJobs();
    }

    // getCount(organizationUnit: number) {
    //     this._jobsServiceProxy.getAllApplicationTrackerCount(organizationUnit).subscribe(result => {
    //         this.totalActiveData = result.totalActiveData;
    //         this.totalActiveVerifyData = result.totalActiveVerifyData;
    //         this.totalActiveDatanotVerifyData = result.totalActiveDatanotVerifyData;
    //         this.totalActiveDatanotAwaitingData = result.totalActiveDatanotAwaitingData;
    //     });
    // }

    getJobs(event?: LazyLoadEvent) {
        
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        // this._jobsServiceProxy.getAllForJobActiveTracker(
        //     this.filterText,
        //     this.suburbFilter,
        //     this.stateFilter,
        //     this.jobTypeNameFilter,
        //     this.jobStatusNameFilter,
        //     this.roofTypeNameFilter,
        //     this.roofAngleNameFilter,
        //     this.elecDistributorNameFilter,
        //     this.leadCompanyNameFilter,
        //     this.elecRetailerNameFilter,
        //     this.paymentOptionNameFilter,
        //     this.depositOptionNameFilter,
        //     this.meterUpgradeNameFilter,
        //     this.meterPhaseNameFilter,
        //     this.promotionOfferNameFilter,
        //     this.houseTypeNameFilter,
        //     this.financeOptionNameFilter,
        //     this.organizationUnit,
        //     this.projectFilter,
        //     this.stateNameFilter,
        //     this.elecDistributorId,
        //     this.jobStatusIDFilter,
        //     this.applicationstatus,
        //     this.jobTypeId,
        //     this.StartDate,
        //     this.EndDate,
        //     undefined,
        //     undefined,
        //     undefined,
        //     undefined,
        //     undefined,
        //     undefined,
        //     undefined,
        //     undefined,
        //     undefined,
        //     undefined,
        //     undefined,
        //     undefined,
        //     undefined,
        //     undefined,undefined,undefined,
        //     undefined,
        //     undefined,undefined,undefined,
        //     this.postalcodefrom,
        //     this.postalcodeTo,
        //     this.paymentid,
        //     this.areaNameFilter,
        //     undefined,
        //     this.primengTableHelper.getSorting(this.dataTable),
        //     this.primengTableHelper.getSkipCount(this.paginator, event),
        //     this.primengTableHelper.getMaxResultCount(this.paginator, event)
        // ).subscribe(result => {
        //     this.primengTableHelper.totalRecordsCount = result.totalCount;
        //     this.primengTableHelper.records = result.items;
        //     const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
        //     this.firstrowcount =  totalrows + 1;
        //     this.last = totalrows + result.items.length;
        //     this.primengTableHelper.hideLoadingIndicator();
        //     this.shouldShow = false;
        //     this.getCount(this.organizationUnit);
        // },
        // err => {
        //     this.primengTableHelper.hideLoadingIndicator();
        // });

        this._jobTrackerServiceProxy.getAllJobActiveTracker(
            this.filterName,
            filterText_,
            this.organizationUnit,
            this.applicationstatus,
            this.paymentid,
            this.jobStatusIDFilter,
            this.dateType,
            this.startDate,
            this.endDate,
            this.state,
            this.salesRepId == 0 ? undefined : this.salesRepId,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {

            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            this.shouldShow = false;
            //this.getCount(this.organizationUnit);

            if (result.totalCount > 0) {
                this.totalActiveData = result.items[0].summary.total;
                this.totalActiveVerifyData = result.items[0].summary.active;
                this.totalActiveDatanotVerifyData = result.items[0].summary.activeReady;
                this.totalActiveDatanotAwaitingData = result.items[0].summary.awaiting;

            } else {
                // this.totalActiveData = 0;
                // this.totalActiveVerifyData = 0;
                // this.totalActiveDatanotVerifyData = 0;
                // this.totalActiveDatanotAwaitingData = 0;
            }

        },
        err => {
            this.primengTableHelper.hideLoadingIndicator();
        });
        
    }

    onsalesrep() {
        this._commonLookupService.getSalesRepForFilter(this.organizationUnit, undefined).subscribe(rep => {
            this.filteredReps = rep;
        });
    }

    reloadPage($event): void {
        this.ExpandedView = true;
        this.paginator.changePage(this.paginator.getPage());
    }

    // addDetails(jobid,statusid): void {
    //     this.jobActiveModel.show(jobid,statusid);
    // }

    quickview(jobid): void {
        this.viewApplicationModal.show(jobid);
    }

    exportToExcel(excelorcsv): void {
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._jobTrackerServiceProxy.getAllJobActiveTrackerExport(
            this.filterName,
            filterText_,
            this.organizationUnit,
            this.applicationstatus,
            this.paymentid,
            this.jobStatusIDFilter,
            this.dateType,
            this.startDate,
            this.endDate,
            excelorcsv,
            this.salesRepId == 0 ? undefined : this.salesRepId,            
            this.state
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    exportToExcelCheckActive(excelorcsv): void {
        this._jobTrackerServiceProxy.getAllJobActiveCheckActiveTrackerExport(
            this.filterName,
            this.filterText,
            this.organizationUnit,
            this.applicationstatus,
            this.paymentid,
            this.jobStatusIDFilter,
            this.dateType,
            this.startDate,
            this.endDate,
            excelorcsv,
            this.salesRepId == 0 ? undefined : this.salesRepId,
            this.state
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    expandGrid() {
        this.ExpandedView = true;
    }

    navigateToLeadDetail(leadid): void {
        // this.ExpandedView = !this.ExpandedView;
        // this.ExpandedView = false;
        // this.SelectedLeadId = leadid;
        // this.viewLeadDetail.showDetail(leadid,null,5);
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Job Active Tracker';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Job Active Tracker';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }
}
