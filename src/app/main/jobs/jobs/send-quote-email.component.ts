import { ConvertActionBindingResult } from '@angular/compiler/src/compiler_util/expression_converter';
import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLookupDto, CreateOrEditQuotationDto, GetJobForEditOutput, GetLeadForViewDto, JobsServiceProxy, LeadDto, LeadsServiceProxy, ProductItemAttachmentListDto, QuotationDataDto, QuotationsServiceProxy, QuotationTamplateServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy} from '@shared/service-proxies/service-proxies';
import { EmailEditorComponent } from 'angular-email-editor';
import * as _ from 'lodash';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'sendQuoteModal',
    templateUrl: './send-quote-email.component.html'
})
export class SendQuoteModalComponent extends AppComponentBase {

    @ViewChild(EmailEditorComponent)
    private emailEditor: EmailEditorComponent;

    @ViewChild('sendQuote', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    quotationData: QuotationDataDto;

    saving = false;    
    productItemAttachments: ProductItemAttachmentListDto[];
    emailData = '';
    //item: GetJobForEditOutput;
    Quotation: CreateOrEditQuotationDto = new CreateOrEditQuotationDto();
    allEmailTemplates: CommonLookupDto[];
    emailTemplateId = 0;
    // emailTemplateId: number;
    emailTo = '';
    emailFrom = '';
    cc ='';
    bcc = '';
    subject= '';
    item: GetLeadForViewDto;
    ccbox = false;
    bccbox = false;
    fromemails: CommonLookupDto[];

    htmlContentBody: any;

    Template = 1;
    temp = true;

    templateBody: any;

    quotationTemplate = [];
    customTemplateId: any = 0;
    sectionId = 0;
    type : number;
    SectionName = '';

    constructor(
        injector: Injector,
        private _jobsServiceProxy: JobsServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy,
        private spinner: NgxSpinnerService,
        private _quotationsServiceProxy: QuotationsServiceProxy,
        private sanitizer: DomSanitizer,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _quotationTamplateProxy: QuotationTamplateServiceProxy
    ) {
        super(injector);
        this.item = new GetLeadForViewDto();
        this.item.lead = new LeadDto();
        this.quotationData = new QuotationDataDto();
    }

    show(jobid: number,leadId:number,sectionId? : number,type? : number,Section = ''): void {   
        this.spinner.show();     
        this.type = type;
        this.Quotation.emailBody = "";
        this.viewTemplate();
        this.sectionId= sectionId;
        this.SectionName = Section;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote = 'Open Send Quotation Email';
        log.section = Section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
        this._leadsServiceProxy.getLeadForView(leadId, 0).subscribe(result => {
            this.item = result;
            this.item.lead = result.lead;
            this.emailTo =  result.lead.email;
        });

        this._leadsServiceProxy.getOrgWiseDefultandownemailadd(leadId).subscribe(result => {
            this.fromemails = result;
            this.emailFrom = this.fromemails[0].displayName;
          });

        if(type == 1){
          this._jobsServiceProxy.productItemAttachmentList(jobid).subscribe(result => {
            this.productItemAttachments = result;
          });
        }
        else {
          this.productItemAttachments =[];
        }
        
        this._leadsServiceProxy.getallEmailTemplates(leadId).subscribe(result => {
            this.allEmailTemplates = result;
            debugger;
        });

        this._quotationTamplateProxy.getCustomTemplateForTableDropdown(leadId).subscribe(result => {
          this.quotationTemplate = result;
        });

        // this._jobsServiceProxy.getJobForEdit(jobid).subscribe(result => { 
        //     this.item = result;
        //     this.emailTo =  result.leadEmail;
        //     //this.spinner.show();
        //     // this._jobsServiceProxy.getEmailTemplateForEditForEmail(this.emailTemplateId).subscribe(result => {
        //     //    debugger;
        //     //     this.emailData = result.emailTemplate.body;
        //     //     this.subject = result.emailTemplate.subject;
        //     //     this.emailTemplateId =  result.emailTemplate.id;
        //     //     if (this.emailData != "") {
        //     //         this.emailData = this.getEmailTemplate(this.emailData);
        //     //         this.emailEditor.editor.loadDesign(JSON.parse(this.emailData));
        //     //         this.modal.show();
        //     //         this.spinner.hide();
        //     //     }
        //     // });
        // });   

        this._quotationsServiceProxy.getQuotationDataByQuoateIdJobId(jobid, undefined).subscribe(result => {
          this.quotationData = result;
          // this.setHtml(result.viewHtml);
          let htmlTemplate = this.getQuotation(result.viewHtml)
          this.htmlContentBody = htmlTemplate;
          this.htmlContentBody = this.sanitizer.bypassSecurityTrustHtml(this.htmlContentBody);
        },
        err => {
          // this.spinner.hide();
        }); 
        
        this.modal.show();
        this.spinner.hide();
    }

    downloadfile(file): void {
        let FileName = AppConsts.docUrl + "/" + file.filePath + file.fileName;
        window.open(FileName, "_blank");      
    }

    saveDesign() {
        this.saving = true;

        if(this.customTemplateId == 1)
        {
          this.emailEditor.editor.exportHtml((data) =>
            this.setHTML(data.html)
          );
        }
        else{
          this.Quotation.emailBody = this.templateBody.changingThisBreaksApplicationSecurity;
          this.save();
        }
        // if(this.type == 1){
          
            // this.Quotation.emailBody = this.templateBody.changingThisBreaksApplicationSecurity;
        // }
        // this.save();


        
    }

    setHTML(emailHTML) {
        let htmlTemplate = this.getEmailTemplate(emailHTML);
        this.Quotation.emailBody = htmlTemplate;
        this.save();
    }

    save(): void {
      debugger;
        this.saving = true;
        this.Quotation.jobId = this.item.jobid;
        this.Quotation.emailTo = this.emailTo;
        this.Quotation.cc = this.cc;
        this.Quotation.bcc = this.bcc;
        this.Quotation.subject = this.subject;
        this.Quotation.quoteMode = this.type == 1 ? "sendQuote" : "sendQuoteOnlyEmail";
        this.Quotation.emailFrom = this.emailFrom;
        this.Quotation.quotationString = this.htmlContentBody.changingThisBreaksApplicationSecurity;
        this.Quotation.sectionId =this.sectionId;
        this._quotationsServiceProxy.sendQuotation(this.Quotation)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
              let log = new UserActivityLogDto();
              log.actionId = 12;
              log.actionNote = 'Quotation Sent On Email';
              log.section = this.SectionName;
              this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
              .subscribe(() => {
              }); 
                this.modal.hide();
                this.emailTemplateId = 0;
                this.emailData = '';
                this.emailFrom = '';
                this.emailTo = '';
                this.subject = '';

                this.templateBody = '';
                this.Template = 1,
                this.customTemplateId = 0;
                this.cc = "";
                this.bcc = "";
                this.quotationData = new QuotationDataDto();
                this.notify.info(this.l('SavedSuccessfully'));
                this.modal.hide();
            });
    }

    close(): void {
        this.cc = "";
        this.bcc = "";
        this.emailData = '';
        this.emailTemplateId = 0;
        this.emailFrom = '';
        this.emailTo = '';
        this.subject = '';

        this.templateBody = '';
        this.Template = 1,
        this.customTemplateId = 0;

        this.modal.hide();
    }

    editorLoaded() {  
        if(this.emailTemplateId != 0)
        {
            this._jobsServiceProxy.getEmailTemplateForEditForEmail(this.emailTemplateId).subscribe(result => {
                debugger;
                this.emailData = result.emailTemplate.body;
                this.subject = result.emailTemplate.subject;
                if (this.emailData != "") {
                    if(this.item != null){
                        this.emailData = this.getEmailTemplate(this.emailData);
                        this.emailEditor.editor.loadDesign(JSON.parse(this.emailData));
                    }                
                    // this.emailEditor.editor.exportHtml((data) =>
                    //     this.setHTML(data.html)
                    // );
                }
            });
        }  
        else{

        }    
        
    }

    // getEmailTemplate(emailHTML) {
    //     let myTemplateStr = emailHTML;        
    //     let addressValue = this.item.job.address + " " + this.item.job.suburb + ", " + this.item.job.state + "-" + this.item.job.postalCode;
    //     let myVariables = {
    //         Customer: {
    //             Name: "this.item.lead.companyName",
    //             Address: addressValue,
    //             Mobile: "this.item.lead.mobile",
    //             Email: "this.item.lead.email",
    //             Phone: "this.item.lead.phone",
    //             SalesRep: "this.item.currentAssignUserName"
    //         },
    //         Sales: {
    //             Name: "this.item.currentAssignUserName",
    //             Mobile: "this.item.currentAssignUserMobile",
    //             Email: "this.item.currentAssignUserEmail"
    //         }
    //     }

    //     // use custom delimiter {{ }}
    //     _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;

    //     // interpolate
    //     let compiled = _.template(myTemplateStr);
    //     let myTemplateCompiled = compiled(myVariables);
    //     return myTemplateCompiled;
    // }

    getEmailTemplate(emailHTML) {
        //let myTemplateStr = "Hello {{user.name}}, you are {{user.age.years}} years old";

        debugger;


        let myTemplateStr = emailHTML;
        let addressValue = this.item.lead.address + " " + this.item.lead.suburb + ", " + this.item.lead.state + "-" + this.item.lead.postCode;
        let myVariables = {
          Customer: {
            Name: this.item.lead.companyName,
            Address: addressValue,
            Mobile: this.item.lead.mobile,
            Email: this.item.lead.email,
            Phone: this.item.lead.phone,
            SalesRep: this.item.currentAssignUserName
          },
          Sales: {
            Name: this.item.currentAssignUserName,
            Mobile: this.item.currentAssignUserMobile,
            Email: this.item.currentAssignUserEmail
          },
          Quote: {
            ProjectNo: this.item.jobNumber,
            SystemCapacity: this.item.systemCapacity != null ? this.item.systemCapacity + " " + 'KW' : this.item.systemCapacity,
            AllproductItem: this.item.qunityAndModelList,
            TotalQuoteprice: this.item.totalQuotaion,
            InstallationDate: this.item.installationDate,
            InstallerName: this.item.installerName,
          },
          Freebies: {
            DispatchedDate: this.item.dispatchedDate,
            TrackingNo: this.item.trackingNo,
            TransportCompany: this.item.transportCompanyName,
            TransportLink: this.item.transportLink,
            PromoType: this.item.freebiesPromoType,
          },
          Invoice: {
            UserName: this.item.userName,
            UserMobile: this.item.userPhone,
            UserEmail: this.item.userEmail,
            Owning: this.item.owning,
          },
          Organization: {
            orgName: this.item.orgName,
            orgEmail: this.item.orgEmail,
            orgMobile: this.item.orgMobile,
            orglogo: AppConsts.docUrl + "/" + this.item.orgLogo,
          },
          Review: {
            link: "",
          },
        }
    
        // use custom delimiter {{ }}
        _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;
    
        // interpolate
        let compiled = _.template(myTemplateStr);
        let myTemplateCompiled = compiled(myVariables);
        return myTemplateCompiled;
    }

    opencc(): void {
      this.ccbox = !this.ccbox;
    }
    openbcc(): void {
      this.bccbox = !this.bccbox;
    }

    getQuotation(emailHTML) {
      
      let myTemplateStr = emailHTML;
      let TableList = [];

      if (this.quotationData.qunityAndModelLists != null) {
          this.quotationData.qunityAndModelLists.forEach(obj => {
            // TableList.push("<div class='mb5 f16'>" + obj.name + "</div>");
            TableList.push("<li class='mb5'>" + obj.name + "</li>");
          }
        )
      }

      let SolarVICRebate = this.quotationData.solarVICRebate != null && this.quotationData.solarVICRebate != "" ? this.quotationData.solarVICRebate : "0.00";
      let SolarVICLoan = this.quotationData.solarVICLoanDiscount != null && this.quotationData.solarVICLoanDiscount != "" ? this.quotationData.solarVICLoanDiscount : "0.00";

      let SystemWith = this.quotationData.capecity + " Kw Solar<br>Photovoltaic System include,";
      if (this.quotationData.systemWith == "Only Inverter") {
        SystemWith = "";
      }
      else if (this.quotationData.systemWith == "Only Battery" || this.quotationData.systemWith == "Panel Without Battery") {
        SystemWith = this.quotationData.batteryCapecity + " Kw Battery, <br>include,";
      }
      else if (this.quotationData.systemWith == "Panel With Battery") {
        SystemWith = this.quotationData.capecity + " Kw Solar, " + this.quotationData.batteryCapecity + " Kw Battery, <br>Photovoltaic System include,";
      }

      let myVariables = {
        Q: {
            JobNumber: this.quotationData.quoteNumber,
            Date: this.quotationData.date,
            JobNotes: this.quotationData.notes,
            InverterLocation: this.quotationData.inverterLocation,
            NearmapSrc: this.quotationData.nearMap1 != null && this.quotationData.nearMap1 != "" ? (AppConsts.docUrl + this.quotationData.nearMap1) : "",

            Cust: {
                Name: this.quotationData.name,
                Mobile: this.quotationData.mobile,
                Email: this.quotationData.email,
                AddressLine1: this.quotationData.address1,
                AddressLine2: this.quotationData.address2,
                MeterPhase: this.quotationData.meaterPhase,
                MeterUpgrad: this.quotationData.meaterUpgrade,
                RoofType: this.quotationData.roofTypes,
                PropertyType: this.quotationData.noofStory,
                RoofPitch: this.quotationData.roofPinch,
                ElecDist: this.quotationData.energyDist,
                ElecRetailer: this.quotationData.energyRetailer,
                NIMINumber: this.quotationData.nmiNumber,
                MeterNo: this.quotationData.meterNo
            },

            SolarAdviser: {
                Name: this.quotationData.instName,
                Mobile: this.quotationData.instMobile,
                Email: this.quotationData.instEmail
            },

            PI: {
                Capecity: this.quotationData.capecity,
                // CapecityDescription: this.quotationData.capecityDescription,
                TCost: this.quotationData.total != null && this.quotationData.total != "" ? this.quotationData.total : "0.00",
                SystemDetailsTable: TableList.toString().replace(/,/g, ''),
                SubTotal: this.quotationData.subTotal != null && this.quotationData.subTotal != "" ? this.quotationData.subTotal : "0.00",
                Stc: this.quotationData.stc != null && this.quotationData.total != "" ? this.quotationData.stc : "0.00",
                StcDesc: this.quotationData.stcDesc != null && this.quotationData.stcDesc != "" ? this.quotationData.stcDesc : "0.00",
                GTotal: this.quotationData.grandTotal != null && this.quotationData.grandTotal != "" ? this.quotationData.grandTotal : "0.00",
                ACharge: this.quotationData.additionalCharges != null && this.quotationData.additionalCharges != "" ? this.quotationData.additionalCharges : "0.00",
                Discount: this.quotationData.specialDiscount != null && this.quotationData.specialDiscount != "" ? this.quotationData.specialDiscount : "0.00",
                Deposit: this.quotationData.deposite != null && this.quotationData.deposite != "" ? this.quotationData.deposite : "0.00",
                Balance: this.quotationData.balance != null && this.quotationData.balance != "" ? this.quotationData.balance : "0.00",
                TotalPriceStcRebate: this.quotationData.stcIncetive != null && this.quotationData.stcIncetive != "" ? this.quotationData.stcIncetive : "0.00",
                SolarVICRebate: this.quotationData.solarVICRebate != null && this.quotationData.solarVICRebate != "" ? this.quotationData.solarVICRebate : "0.00",
                SolarVICLoan: this.quotationData.solarVICLoanDiscount != null && this.quotationData.solarVICLoanDiscount != "" ? this.quotationData.solarVICLoanDiscount : "0.00",
                VicRebate: this.quotationData.vicRebate,

                VicWithRebate: this.quotationData.vicRebate == "With Rebate Install" ? "<Tr><Td>Less - Solar VIC Rebate *</Td><Td>$"+ SolarVICRebate +"</Td></Tr><Tr><Td>Less - Solar VIC Loan *</Td><Td>$"+ SolarVICLoan +"</Td></Tr>" : "",
                System: SystemWith,
                DiscountSep : (this.quotationData.specialDiscount != null && this.quotationData.specialDiscount != "" ? this.quotationData.specialDiscount : "0.00") != "0.00" ? "<Tr><Td>Special Discount</Td><Td>$ "+this.quotationData.specialDiscount+"</Td></Tr>" : "",
                AChergeT : (this.quotationData.additionalCharges != null && this.quotationData.additionalCharges != "" ? this.quotationData.additionalCharges : "0.00")!= "0.00"? "<Tr><Td>Additional Charges</Td><Td>$ "+this.quotationData.additionalCharges+"</Td></Tr>" : "",
                BatteryRebate : this.quotationData.batteryRebate >  0 ? "<Tr><Td>Battery Rebate(C)</Td><Td>$ "+this.quotationData.batteryRebate+"</Td></Tr>" : "",
                GTotalLabel : this.quotationData.batteryRebate >  0 ? "Grand Total (A-B-C)" : "Grand Total (A-B)"
            },
            
            CustomerSign: {
                SignSrc: this.quotationData.signSrc,
                Name: this.quotationData.signName,
                Date: this.quotationData.signDate,
            },

            RepresentativeSign: {
                SignSrc: this.quotationData.representativeSignSrc
            }
          },
          Finance : {
            PaymentOption : this.quotationData.paymentOption,
            FinPaymentType : this.quotationData.finPaymentType,
            DepositOption : this.quotationData.depositOption,
            FinancePurchaseNo : this.quotationData.financePurchaseNo,
            FinanceAmount : this.quotationData.financeAmount,
            FinanceDepositeAmount : this.quotationData.financeDepositeAmount,
            FinanceNetAmount : this.quotationData.financeNetAmount,
            PaymentTerm : this.quotationData.paymentTerm,
            RepaymentAmount : this.quotationData.repaymentAmount,

          },
          Warranty : {
            PerformanceWarranty: this.quotationData.performanceWarranty,
            ProductWarranty: this.quotationData.productWarranty,
            ExtendedWarranty: this.quotationData.extendedWarranty,
            WorkmanshipWarranty: this.quotationData.workmanshipWarranty,
          },
          Manu:{
            Phone : this.quotationData.manuPhone,
            Mobile : this.quotationData.manuMobile,
            Email : this.quotationData.manuEmail,
            Website : this.quotationData.manuWebsite 
          }
        }

      // use custom delimiter {{ }}
      _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;
  
      // interpolate
      let compiled = _.template(myTemplateStr);
      let myTemplateCompiled = compiled(myVariables);
      return myTemplateCompiled;
    }

  
  OnTemplateChnage(): void {
    this.viewTemplate();
  }
  
  viewTemplate()
  {
    if(this.Template == 1) {
      this.temp = true;
    }
    else {
      this.temp = false;
    }
  }

  customTemplateOnChange(): void {
    
    if(this.customTemplateId == 0 || this.customTemplateId == null || this.customTemplateId == undefined)
    {
      this.notify.warn("Select Template");
      return;
    }

    this.setCustomTemplate();
    
    //this.subject = "Your " + this.item.orgName + " Quotation No is " + this.item.jobNumber;
  }

  setCustomTemplate()
  {
    this._quotationTamplateProxy.getQuotationTemplateForEdit(this.customTemplateId).subscribe(result => {


      let htmlTemplate = this.getHtmlForCustomTemplate(result.quotationTemplate.viewHtml);
      this.subject = this.getHtmlForCustomTemplate(result.quotationTemplate.subject);
      this.templateBody = htmlTemplate;
      this.templateBody = this.sanitizer.bypassSecurityTrustHtml(this.templateBody);
    });
  }

  getHtmlForCustomTemplate(emailHTML) {
    let myTemplateStr = emailHTML;

    let TableList = [];

    if (this.quotationData.qunityAndModelLists != null) {
        this.quotationData.qunityAndModelLists.forEach(obj => {
            if(this.item.organizationId == 8)
            {
              TableList.push("<li style='font-family: 'Roboto', sans-serif; font-size:14px; color:#1a1a1a;'>" + obj.name + "</li>");
            }
            else
            {
              TableList.push("<li style='font-family: 'Montserrat', sans-serif; font-size:14px; color:#1a1a1a;'>" + obj.name + "</li>");
            }
      })
    }

    let TCost = "0.00";
    if (this.item.lead.state == "VIC") {
      TCost = this.quotationData.balance != null && this.quotationData.balance != "" ? this.quotationData.balance : "0.00";
    } else {
      TCost = this.quotationData.totalCost != null && this.quotationData.totalCost != "" ? this.quotationData.totalCost : "0.00";
    }
    
    
    let financeDepositeAmount = this.quotationData.financeDepositeAmount != null && this.quotationData.financeDepositeAmount != undefined ? this.quotationData.financeDepositeAmount : "0.00";
    let repaymentAmount = this.quotationData.repaymentAmount != null && this.quotationData.repaymentAmount != undefined ? this.quotationData.repaymentAmount : "0.00";
    let financeCashDiv = "";
    if(this.quotationData.paymentOption == 'Cash')
    {
      financeCashDiv = "<div style='font-size:14px; font-weight:normal; font-family: Montserrat, sans-serif; color:#1a1a1a; margin-bottom: 15px;'><strong>Upfront Discounted Cost: $" + TCost + "</strong> Incl. GST</div>";
    }
    else 
    {
      financeCashDiv = "<div style='font-size:14px; font-weight:normal; font-family: Montserrat, sans-serif; color:#1a1a1a; margin-bottom: 15px;'><strong>Cost with Payment Plan: $"+ TCost +"</strong>.</div><div style='font-size:14px; font-weight:normal; font-family: Montserrat, sans-serif; color:#1a1a1a; margin-bottom: 15px;'><strong>After $"+ financeDepositeAmount +" deposit, "+ this.quotationData.paymentTerm +" repayment will be $"+ repaymentAmount +" for "+ this.quotationData.finPaymentType +".</strong> Fees may apply.</div>";
    }

    let systemCap = this.quotationData.capecity + " Kw Solar";
    if (this.quotationData.systemWith == "Only Inverter") {
      systemCap = this.quotationData.capecity + " Kw Solar";
    }
    else if (this.quotationData.systemWith == "Only Battery" || this.quotationData.systemWith == "Panel Without Battery") {
      systemCap = this.quotationData.batteryCapecity + " Kw Battery";
    }
    else if (this.quotationData.systemWith == "Panel With Battery") {
      systemCap = this.quotationData.capecity + " Kw Solar, " + this.quotationData.batteryCapecity + " Kw Battery";
    }

    let warranty = "<div style='font-size:14px; font-weight:normal; font-family: Montserrat, sans-serif; color:#1a1a1a; margin-bottom: 15px;'><strong>Local Warranties:</strong></div><ul style='font-family: Montserrat, sans-serif; font-size:14px; margin:0 0 15px;'><li style='font-family: Montserrat, sans-serif; font-size:14px; color:#1a1a1a; margin-bottom: 10px;'>"+ this.quotationData.performanceWarranty +" Years Linear Performance Warranty on Solar Panels</li><li style='font-family: Montserrat, sans-serif; font-size:14px; color:#1a1a1a; margin-bottom: 10px;'>"+ this.quotationData.productWarranty +" Years Product Warranty On Solar Panels</li><li style='font-family: Montserrat, sans-serif; font-size:14px; color:#1a1a1a; margin-bottom: 10px;'>10 Years Product Warranty on Inverter (upon online registration)</li></ul>"
    if(this.quotationData.systemWith == "Only Battery" || this.quotationData.systemWith == "Panel Without Battery") {
      warranty = ""
    }

    let myVariables = {
      Q: {

        JobNumber: this.quotationData.quoteNumber,

        Cust: {
          Name: this.quotationData.name,
        },

        PI: {
          // Capecity: this.quotationData.capecity,
          Capecity: systemCap,
          TCost: this.quotationData.totalCost != null && this.quotationData.totalCost != "" ? this.quotationData.totalCost : "0",
          AllproductItem: TableList.toString().replace(/,/g, ''),

          Stc: this.quotationData.stc != null && this.quotationData.stc != "" ? this.quotationData.stc : "0.00",
                
          GTotal: this.quotationData.grandTotal != null && this.quotationData.grandTotal != "" ? this.quotationData.grandTotal : "0.00",
          Balance: this.quotationData.balance != null && this.quotationData.balance != "" ? this.quotationData.balance : "0.00",
          SolarVICRebate: this.quotationData.solarVICRebate != null && this.quotationData.solarVICRebate != "" ? this.quotationData.solarVICRebate : "0.00",
          SolarVICLoan: this.quotationData.solarVICLoanDiscount != null && this.quotationData.solarVICLoanDiscount != "" ? this.quotationData.solarVICLoanDiscount : "0.00",
          ACharge: this.quotationData.additionalCharges != null && this.quotationData.additionalCharges != "" ? this.quotationData.additionalCharges : "0.00",
          Deposit: this.quotationData.deposite != null && this.quotationData.deposite != "" ? this.quotationData.deposite : "0.00",          
          DiscountSep : (this.quotationData.specialDiscount != null && this.quotationData.specialDiscount != "" ? this.quotationData.specialDiscount : "0.00") != "0.00" ? "<Tr><Td>Special Discount</Td><Td>$ "+this.quotationData.specialDiscount+"</Td></Tr>" : "",
          AChergeT : (this.quotationData.additionalCharges != null && this.quotationData.additionalCharges != "" ? this.quotationData.additionalCharges : "0.00")!= "0.00"? "<Tr><Td>Additional Charges</Td><Td>$ "+this.quotationData.additionalCharges+"</Td></Tr>" : "",
          BatteryRebate : this.quotationData.batteryRebate >  0 ? "<Tr><Td>Battery Rebate(C)</Td><Td>$ "+this.quotationData.batteryRebate+"</Td></Tr>" : "",
          GTotalLabel : this.quotationData.batteryRebate >  0 ? "Grand Total (A-B-C)" : "Grand Total (A-B)"
        },

        SolarAdviser: {
          Name: this.item.currentAssignUserName,
          Mobile: this.item.currentAssignUserMobile,
          Email: this.item.currentAssignUserEmail
        },
      },

      O: {
        OrgName: this.item.orgName,
        OrgEmail: this.item.orgEmail,
        OrgPhone: this.item.orgMobile,
        OrgLogo: AppConsts.docUrl + this.item.orgLogo
      },
      Finance : {
        PaymentOption : this.quotationData.paymentOption,
        FinPaymentType : this.quotationData.finPaymentType,
        DepositOption : this.quotationData.depositOption,
        FinancePurchaseNo : this.quotationData.financePurchaseNo,
        FinanceAmount : this.quotationData.financeAmount,
        FinanceDepositeAmount : this.quotationData.financeDepositeAmount,
        FinanceNetAmount : this.quotationData.financeNetAmount,
        PaymentTerm : this.quotationData.paymentTerm,
        RepaymentAmount : this.quotationData.repaymentAmount,

      },
      // Warranty : {
      //   PerformanceWarranty: this.quotationData.performanceWarranty,
      //   ProductWarranty: this.quotationData.productWarranty,
      //   ExtendedWarranty: this.quotationData.extendedWarranty,
      //   WorkmanshipWarranty: this.quotationData.workmanshipWarranty,
      // },

      // FinanceCashDiv: financeCashDiv.toString().replace(/,/g, ''),
      FinanceCashDiv: financeCashDiv,

      Warranty: warranty
    };

    // use custom delimiter {{ }}
    //_.templateSettings.interpolate = /{{([\s\S]+?)}}/g;

    // myTemplateStr = myTemplateStr.replace("")

    // interpolate
    let compiled = _.template(myTemplateStr);
    let myTemplateCompiled = compiled(myVariables);
    return myTemplateCompiled;
    //return emailHTML;
  }
  
}