import { Component, ViewChild, Injector, Output, EventEmitter, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { CalculateJobProductItemDeatilDto, JobsServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as _ from 'lodash';

@Component({
    selector: 'viewCalculatorModel',
    templateUrl: './cal.component.html'
})

export class ViewCalculatorModalComponent extends AppComponentBase implements OnInit {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    result: any[];
    totAmt: any;

    saving = false;
   
    ngOnInit(): void {
        
    }

    constructor(
        injector: Injector,
        private _jobsServiceProxy: JobsServiceProxy
    ) {
        super(injector);
    }

    show(itemList: CalculateJobProductItemDeatilDto[]): void {

        this.spinnerService.show()
        this.result = [];
        this._jobsServiceProxy.getCalculateJobProductItemDeatils(itemList).subscribe(result => {
            this.result = result;

            this.totAmt = result.reduce((sum, current) => sum + current.amount, 0);
            console.log(itemList);
            this.modal.show();
            this.spinnerService.hide();
        },
        error => {
            this.spinnerService.hide();
        });
    }

    close(): void {
        this.modal.hide();
    }
}