import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { CommonLookupDto, CreateOrEditJobDto, GetJobForEditOutput, JobActiveStatusDto, JobJobStatusLookupTableDto, JobsServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'cancelrequestapprovedreject',
    templateUrl: './approve-reject-jobcancelreq.component.html'
})

export class ApproveRejectCancelRequestComponent extends AppComponentBase {

    @ViewChild('activeModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    cash = false;
    saving = false;
    jobId: number;
    jobStatus: JobActiveStatusDto = new JobActiveStatusDto();
    item: GetJobForEditOutput;
    jobCancelRequestReason: any;
    isJobCancelRequest: any;
    cancelReasons: CommonLookupDto[];
    approved = false;
    reject = false;
    jobstatus :any;
    SectionId = 0;
    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _jobServiceProxy: JobsServiceProxy,
    ) {
        super(injector);
        this.item = new GetJobForEditOutput();
        this.item.job = new CreateOrEditJobDto();
    }

    show(jobId: number,SectionId? : number,Section? : string): void {
        this.jobId = jobId;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote = 'Open Approved / Reject / Cancel Request';
        log.section = Section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
        this.SectionId = SectionId;
        this._jobServiceProxy.getAllCancelReasonForTableDropdown().subscribe(result => {
            this.cancelReasons = result;
        });
        this._jobServiceProxy.getJobForEdit(jobId).subscribe(result => {
            this.item.job = result.job;
            this.item.job.section = Section;
            this.item.job.sectionId = SectionId;
            this.jobCancelRequestReason = result.job.jobCancelRequestReason;
            this.isJobCancelRequest = result.job.isJobCancelRequest;
            if(result.job.isJobCancelRequest = true){
                this.jobstatus = 1;
            }
            if(result.job.isJobCancelRequest = false){
                this.jobstatus = 2;
            }
            this.OnChnage(event);
            this.modal.show();
        });
    }

    OnChnage(event): void {
        
        if (event.target.value == 1) {
            this.approved = true;
            this.reject = false;
        } else if (event.target.value == 2) {
            this.approved = false;
            this.reject = true;
        } else {
            this.approved = false;
            this.reject = false;
        }
    }

    save(): void {
       if(this.jobstatus == 1){
        this.item.job.isJobCancelRequest = true;
       }
       if (this.jobstatus == 2){
        this.item.job.isJobCancelRequest = false;
       }
        this._jobServiceProxy.updateCanacelationRequest(this.item.job)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.modal.hide();
    }
}