import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { JobActiveStatusDto, DisplayNameStatusDto, JobsServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'jobActiveModal',
    templateUrl: './check-job-active.component.html'
})

export class JobActiveModalComponent extends AppComponentBase {

    @ViewChild('activeModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    cash = false;
    saving = false;
    jobId : number;
    jobStatus:JobActiveStatusDto = new JobActiveStatusDto();
    depositStatuses: DisplayNameStatusDto[] = [];
    activeStatuses: DisplayNameStatusDto[] = [];
    applicationStatuses: DisplayNameStatusDto[] = [];
  
    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _jobServiceProxy: JobsServiceProxy,
        private spinner: NgxSpinnerService  
    ) {
        super(injector);
    }

    show(jobId: number,Section? :string): void {
        this.jobId = jobId;       
        this.spinner.show();
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote = 'Open Check Job Active';
        log.section = Section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
        this._jobServiceProxy.getDisplayNameStatuses(jobId).subscribe(result => {
  
            this.depositStatuses = result.depositStatuses;
            this.activeStatuses = result.activeStatuses;
            this.applicationStatuses = result.applicationStatuses;

         
            this.modal.show();
            this.spinner.hide();
        }); 
        // this._jobServiceProxy.getJobActiveStatus(jobId).subscribe(result => {
        //     debugger;
        //     this.jobStatus = result;
        //     if(this.jobStatus.payementoptionid == 1)
        //     {
        //         this.cash = false;
        //     }else {
        //         this.cash = true;
        //     }
        //     this.modal.show();
        //     this.spinner.hide();
        // });      
    }

    save(): void {
        
    }
    
    close(): void {
        this.modal.hide();
    }
}