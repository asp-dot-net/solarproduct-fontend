import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { CommonLookupDto, CreateOrEditInvoicePaymentDto, InvoicePaymentInvoicePaymentMethodLookupTableDto, InvoicePaymentsServiceProxy, LeadsServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { NgxSpinnerService } from 'ngx-spinner';
import { TimingServiceProxy } from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'editJobInvoiceModal',
    templateUrl: './edit-jobinvoice-model.component.html'
})

export class CreateOrEditJobInvoiceModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    saving = false;
    //receiveddate : moment.Moment = moment().add(0, 'days').endOf('day');
    allInvoicePaymentMethods: InvoicePaymentInvoicePaymentMethodLookupTableDto[];
    allInvoicePaymentStatus:CommonLookupDto[];
    jobInvoice: CreateOrEditInvoicePaymentDto = new CreateOrEditInvoicePaymentDto();
    jobid:number;

    role: string;
    sectionId =0;
    SectionName = '';
    constructor(
        injector: Injector,
        private _invoicePaymentsServiceProxy: InvoicePaymentsServiceProxy,
        private spinner: NgxSpinnerService,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _leadsServiceProxy : LeadsServiceProxy
    ) {
        super(injector);
    }

    show(jobid:number,jobInvoiceId?: number, section? : number,SectionName = ''): void {
        this.jobid = jobid;
        this.sectionId = section;
        this.SectionName = SectionName;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote = jobInvoiceId > 0 ? 'Open Job Invoice For Edit':'Open Job Invoice For Create New Invoice';
        log.section = SectionName;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
        if(this.allInvoicePaymentMethods == null){
            this._invoicePaymentsServiceProxy.getAllInvoicePaymentMethodForTableDropdown().subscribe(result => {						
                this.allInvoicePaymentMethods = result;
            });
        }
        
        if(this.allInvoicePaymentStatus == null){
            this._invoicePaymentsServiceProxy.getAllInvoiceStatusForTableDropdown().subscribe(result => {						
                this.allInvoicePaymentStatus = result;
            });
        }

        this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
            this.role = result;
        });

        if (!jobInvoiceId) {
            this.jobInvoice = new CreateOrEditInvoicePaymentDto();

            const date = new Date();
            this.jobInvoice.invoicePayDate = moment(date);;
            this.jobInvoice.id = jobInvoiceId;
            this.modal.show();
        } 
        else {
            this._invoicePaymentsServiceProxy.getInvoicePaymentForEdit(jobInvoiceId).subscribe(result => {
                this.jobInvoice = result.invoicePayment;
                ///this.receiveddate = result.invoicePayment.invoicePayDate;
                this.modal.show();
            });
        }
    }

    calculateGST(): void{
        this.jobInvoice.invoicePayGST = this.jobInvoice.invoicePayTotal * 10 /100;
        if(this.jobInvoice.invoicePaymentMethodId == 1){
            this.jobInvoice.ccSurcharge = this.jobInvoice.invoicePayTotal * 1 /100;
        }
    };

    save(): void {
        this.saving = true;
        this.jobInvoice.jobId = this.jobid;
        this.jobInvoice.sectionId = this.sectionId;
       /// this.jobInvoice.invoicePayDate = this.receiveddate;
       this.spinner.show();
        this._invoicePaymentsServiceProxy.createOrEdit(this.jobInvoice)
            .pipe(finalize(() => { this.saving = false;}))
            .subscribe(() => {
                let log = new UserActivityLogDto();
                log.actionId = 16;
                log.actionNote = this.jobInvoice.id > 0 ? 'Invoice Modified': "Invoice Created With Amount $" + this.jobInvoice.invoicePayTotal;
                log.section = this.SectionName;
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            this.notify.info(this.l('SavedSuccessfully'));
            this.close();
            this.modalSave.emit(null);
            this.spinner.hide();
            });
    }

    close(): void {
        this.modal.hide();
    }
}