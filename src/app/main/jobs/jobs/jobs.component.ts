import { Component, Injector, ViewEncapsulation, Input, Output, EventEmitter, ViewChild, OnInit, ElementRef } from '@angular/core';
import {
    JobsServiceProxy, CreateOrEditJobDto, JobJobTypeLookupTableDto, JobRoofTypeLookupTableDto,
    JobRoofAngleLookupTableDto, JobElecDistributorLookupTableDto, JobElecRetailerLookupTableDto,
    JobDepositOptionLookupTableDto, JobPaymentOptionLookupTableDto, JobMeterUpgradeLookupTableDto,
    JobHouseTypeLookupTableDto, JobFinanceOptionLookupTableDto,
    JobMeterPhaseLookupTableDto, JobPromotionPromotionMasterLookupTableDto, JobVariationsServiceProxy, ProductTypesServiceProxy,
    JobProductItemsServiceProxy, JobPromotionsServiceProxy, CreateOrEditJobVariationDto, CreateOrEditJobProductDto,
    CreateOrEditJobPromotionDto,
    CreateOrEditJobProductItemDto,
    LeadsServiceProxy,
    LeadStateLookupTableDto,
    JobVariationVariationLookupTableDto,
    ProductItemsServiceProxy,
    JobJobStatusLookupTableDto,
    STCZoneRatingDto,
    STCPostalCodeDto,
    STCYearWiseRateDto,
    UploadDocumentInput, QuotationsServiceProxy, GetInvoicePaymentForViewDto, GetQuotationForViewDto, CreateOrEditQuotationDto, CreateOrEditInvoicePaymentDto, InvoicePaymentsServiceProxy, LeadUsersLookupTableDto, GetDocumentTypeForView, GetJobDocumentForView, FileDto, CommonLookupDto, JobInstallationDto, JobRefundsServiceProxy, CreateOrEditJobRefundDto, JobRefundRefundReasonLookupTableDto, LeadDto, ReportServiceProxy, PickListServiceProxy, CreateOrEditPickListDto, GetPicklistItemInput, JobInstallerInvoicesServiceProxy, CreateOrEditJobInstallerInvoiceDto, JobProductsServiceProxy, ExistJobSiteAddDto, GetJobForEditOutput, GetJobPromotionForEditOutput, DetailJobPromotionDto, DetailDto, GetDetailForJobEditOutPut, CreateOrEditJobOdSysDetails, CommonLookupServiceProxy, InstallerServiceProxy
    , JobAcknowledgementServiceProxy, CreateOrEditAcnoDto, GetJobAcnoForViewDto, SendAcknowledgementInputDto, GetPicklistDataDto
    , QuickstockServiceProxy,
    ProductPackageServiceProxy,
    TexInvoiceDto,
    PaymentReceiptDto,
    CalculateJobProductItemDeatilDto,
    GetActualCostItemListInput,
    DeclarationFormServiceProxy,CreateOrEditDeclarationFormDto, GetDeclarationFormForViewDto,SendDeclarationFormInputDto, GetActualCostOutput,
    GreenDealServiceProxy,
    UserActivityLogServiceProxy,
    UserActivityLogDto
    
} from '@shared/service-proxies/service-proxies';
import { NotifyService, TokenService, IAjaxResponse } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { finalize, retry } from 'rxjs/operators';
import * as _ from 'lodash';
import * as moment from 'moment';
import { FileUploader, FileUploaderOptions, FileItem } from 'ng2-file-upload';
import PlaceResult = google.maps.places.PlaceResult;
import { Location } from '@angular-material-extensions/google-maps-autocomplete';
import { AppConsts } from '@shared/AppConsts';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { CreateOrEditJobRefundModalComponent } from '../jobRefunds/create-or-edit-jobRefund-modal.component';
import { CreateOrEditJobInvoiceModalComponent } from './edit-jobinvoice-model.component';
import { JobStatusModalComponent } from './job-status.component';
import { JobActiveModalComponent } from './check-job-active.component';
import { SendQuoteModalComponent } from './send-quote-email.component';
import { CreateOrEditPickListModalComponent } from './edit-picklist-model.component';
import { ApproveRejectCancelRequestComponent } from './approve-reject-jobcancelreq.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { PaywayGetcardDetailModelComponent } from './payway/payway-getcard-detail-model.component';
import { ViewQuoteModalComponent } from './viewquotation.component';
import { DomSanitizer } from '@angular/platform-browser';
import { ViewPackageModalComponent } from './viewpackage.component';
import { ViewCalculatorModalComponent } from './cal.component';
import { ViewPaymentComponent } from '@app/main/invoices/view-payment.component';
import { DocumentRequestLinkModalComponent } from './document-request-link.component';
import { SuggestedProductModalComponent } from './suggested-product-modal.component';
import { JobActualCostModalComponent } from './job-actualcost-model.component';

@Component({
    selector: 'jobCreateOrEdit',
    templateUrl: './jobs.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class JobsComponent extends AppComponentBase implements OnInit {

    @ViewChild('createOrEditJobRefundModal', { static: true }) createOrEditJobRefundModal: CreateOrEditJobRefundModalComponent;
    @ViewChild('editJobInvoiceModal', { static: true }) editJobInvoiceModal: CreateOrEditJobInvoiceModalComponent;
    @ViewChild('jobStatusModal', { static: true }) jobStatusModal: JobStatusModalComponent;
    @ViewChild('jobActiveModal', { static: true }) jobActiveModal: JobActiveModalComponent;
    @ViewChild('sendQuoteModal', { static: true }) sendQuoteModal: SendQuoteModalComponent;
    @ViewChild('editPickListModal', { static: true }) editPickListModal: CreateOrEditPickListModalComponent;
    @ViewChild('cancelrequestapprovedreject', { static: true }) cancelrequestapprovedreject: ApproveRejectCancelRequestComponent;
    @ViewChild('paywaygetcarddetailModel', { static: true }) paywaygetcarddetailModel: PaywayGetcardDetailModelComponent;
    @ViewChild('viewQuoteModel', { static: true }) viewQuoteModel: ViewQuoteModalComponent;
    @ViewChild('viewPackegeModel', { static: true }) viewPackegeModel: ViewPackageModalComponent;
    @ViewChild('viewCalculatorModel', { static: true}) viewCalculatorModel: ViewCalculatorModalComponent;
    @ViewChild('suggestedProductModal', { static: true}) suggestedProductModal: SuggestedProductModalComponent;
    @ViewChild('viewpaymentdetail', { static: false }) viewpaymentdetail: ViewPaymentComponent;
    @ViewChild('documentRequestModal', { static: true }) documentRequestModal: DocumentRequestLinkModalComponent;
    @ViewChild('JobActualCostModal', { static: true }) JobActualCostModal: JobActualCostModalComponent;
    @Input() LeadId: number = 0;
    bonus ="No";
    broadbandWifiConnection ="null";
    cancelReasons: CommonLookupDto[];
    isdiablediv =true;
    total = 0;
    jobCurrentStatus: string;
    active1 = true;
    jractive = true;
    saving1 = false;
    active = true;
    saving = false;
    changeAddress = false;
    jobid: number = 0;
    depositRequired: number = 0;
    billToPay: number = 0;
    leadId: number = 0;
    isGoogle = "Google";
    job: CreateOrEditJobDto = new CreateOrEditJobDto();
    picklist: CreateOrEditPickListDto = new CreateOrEditPickListDto();
    jobTypes: JobJobTypeLookupTableDto[];
    // Installeruser: JobJobTypeLookupTableDto[];
    jobHouseTypes: JobHouseTypeLookupTableDto[];
    jobRoofTypes: JobRoofTypeLookupTableDto[];
    jobRoofAngles: JobRoofAngleLookupTableDto[];
    jobStatuses: JobJobStatusLookupTableDto[];
    jobElecDistributors: JobElecDistributorLookupTableDto[];
    jobElecRetailers: JobElecRetailerLookupTableDto[];
    jobDepositOption: JobDepositOptionLookupTableDto[];
    jobPaymentOptions: JobPaymentOptionLookupTableDto[];
    jobFinOptions: JobFinanceOptionLookupTableDto[];
    jobMeterUpgrade: JobMeterUpgradeLookupTableDto[];
    jobMeterPhase: JobMeterPhaseLookupTableDto[];
    variations: JobVariationVariationLookupTableDto[];
    productTypes: CommonLookupDto[];
    
    // productItems: JobProductItemProductItemLookupTableDto[];
    // EditproductItems: JobProductItemProductItemLookupTableDto[];
    
    PickListtems: GetPicklistItemInput[];
    jobPromotionMaster: JobPromotionPromotionMasterLookupTableDto[];
    JobVariations: CreateOrEditJobVariationDto[];
    JobProducts: any[];
    JobOldSystemDetail: CreateOrEditJobOdSysDetails[];
    itemss: GetJobForEditOutput;
    PicklistProducts: any[];
    JobPromotions: CreateOrEditJobPromotionDto[];
    allStates: LeadStateLookupTableDto[];
    jobInstallation: JobInstallationDto = new JobInstallationDto();
    isInstallationDetail: boolean = false;
    isInstallationDate: boolean = false;
    ShowAddress: boolean = true;
    unitType: any;
    filename: any[];
    filenName = [];
    streetType: any;
    streetName: any;
    suburb: any;
    stockWithId: any;
    postalUnitType: any;
    postalStreetType: any;
    postalStreetName: any;
    postalSuburb: any;
    picklistwarehouseLocation: any;
    picklistinstallerId: any;
    unitTypesSuggestions: string[];
    streetTypesSuggestions: string[];
    streetNamesSuggestions: string[];
    suburbSuggestions: string[];
    postalunitTypesSuggestions: string[];
    postalstreetTypesSuggestions: string[];
    postalstreetNamesSuggestions: string[];
    postalsuburbSuggestions: string[];
    
    STCZoneRating: STCZoneRatingDto[];
    STCPostalCode: STCPostalCodeDto[];
    STCYearWiseRate: STCYearWiseRateDto[];
    InstallerList: any[];
    ElectrictionList: CommonLookupDto[];
    warehouselocationlist: CommonLookupDto[];
    DesignerList: CommonLookupDto[];
    selectAddress: boolean = true;
    selectAddress2: boolean = true;
    from: string;
    lead = new LeadDto();
    reportPath: string;
    InvTypeId: number;
    JobInvNo: any;
    Amount: number;
    InvDate: moment.Moment;
    AdvanceAmount: number;
    LessDeductAmount: number;
    jobInstallerInv: number;
    AdvancePayDate: moment.Moment;
    PayDate: moment.Moment;
    PaymentTypeId: moment.Moment;
    InstallerId: number;
    Notes: string;
    TotalAmount: number;
    PaymentsTypeId: number;
    InvNo: number;
    jobcancelreq = false;
    cancelreq: any;
    SalesRepManageradmin = false;
    disbaleifsalerep = false;
    reasonofpicklist = false;
    jobaddress = "";
    public uploader: FileUploader;
    public fileupload: FileUploader;
    public temporaryFileUrl: string;
    public maxfileBytesUserFriendlyValue = 5;
    private _uploaderOptions: FileUploaderOptions = {};
    private _fileuploadoption: FileUploaderOptions = {};
    documentTypeId: any;
    DocumentTypes: GetDocumentTypeForView[];
    JobQuotations: GetQuotationForViewDto[];
    JobDocuments: GetJobDocumentForView[];
    JobInvoices: GetInvoicePaymentForViewDto[];
    Invoice: CreateOrEditInvoicePaymentDto = new CreateOrEditInvoicePaymentDto();
    receiveddate: moment.Moment;
    Quotation: CreateOrEditQuotationDto = new CreateOrEditQuotationDto();
    allUsers: LeadUsersLookupTableDto[];
    totalInvoiceAmountPaid: any = 0;
    pendingInvoiceAmount: any = 0;
    sampleDate: moment.Moment;
    Post_completiondate: moment.Moment;
    Post_inspectionDate: moment.Moment;
    approvalDate: moment.Moment;
    expiryDate: moment.Moment;
    financeid: boolean = true;
    jobRefund: CreateOrEditJobRefundDto = new CreateOrEditJobRefundDto();
    installerInv: CreateOrEditJobInstallerInvoiceDto = new CreateOrEditJobInstallerInvoiceDto();
    jobRefundList: CreateOrEditJobRefundDto[];
    allRefundReasons: JobRefundRefundReasonLookupTableDto[];
    totalsolarVLCRebate: number;
    totalsolarVLCLoan: number;
    solarVLCRebate: number;
    solarVICLoanDiscont: number;
    items: GetJobPromotionForEditOutput;
    detailitem: GetDetailForJobEditOutPut = new GetDetailForJobEditOutPut();
    docavailable = false;
    picklistreason = '';
    role: string = '';
    rolewisededucton = false;
    sectionId = 0;
    picklistinstallationList = [];
    retailerdetail = false;
    SendAcknowledgement: SendAcknowledgementInputDto = new SendAcknowledgementInputDto();
    jobAcknowledgement: CreateOrEditAcnoDto = new CreateOrEditAcnoDto();
    declarationForm: CreateOrEditDeclarationFormDto = new CreateOrEditDeclarationFormDto();
    exportControl: GetJobAcnoForViewDto[];
    feedInTariif: GetJobAcnoForViewDto[];
    shadingDeclaration : GetDeclarationFormForViewDto[];
    efficiencyDeclaration : GetDeclarationFormForViewDto[];
    SendDeclarationForm : SendDeclarationFormInputDto = new SendDeclarationFormInputDto();

    warehouseDistance: any;

    productPackages: any[];

    basicCost: number;

    taxInvoiceData: TexInvoiceDto;
    paymentReceiptData: PaymentReceiptDto;

    productItemSuggestions: any[];

    actualCost: number = 0;
    category: string;
    panelCost: number = 0;
    pricePerWatt: number = 0;
    inverterCost: number = 0;
    batteryCost: number = 0;
    installationCost: number = 0;
    batteryInstallationCost: number = 0;

    isPanelInverter: boolean = false;
    vppConnectionList : CommonLookupDto[];
    vicqRcodeScanDocument : GetJobDocumentForView = new GetJobDocumentForView(); 

    stcprovider : CommonLookupDto[] = [];
    providerid = 0 ;
    ActualCostList = [];
    GWTID='';
    ExtraCostValue=0
    sitevisit ="No";
    SectionName = '';

    divBasicCost = 0;
    isbettaryRebateUpdate = false;

    constructor(
        injector: Injector,
        private _jobServiceProxy: JobsServiceProxy,
        private _picklistServiceProxy: PickListServiceProxy,
        private _jobVariationsServiceProxy: JobVariationsServiceProxy,
        private _jobProductItemsServiceProxy: JobProductItemsServiceProxy,
        private _jobPromotionsServiceProxy: JobPromotionsServiceProxy,
        // private _productItemsServiceProxy: ProductItemsServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _reportServiceProxy: ReportServiceProxy,
        private _tokenService: TokenService,
        private _quotationsServiceProxy: QuotationsServiceProxy,
        private _invoicePaymentServiceProxy: InvoicePaymentsServiceProxy,
        private _jobRefundsServiceProxy: JobRefundsServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _jobInstallerInvoiceServiceProxy: JobInstallerInvoicesServiceProxy,
        private spinner: NgxSpinnerService,
        private _installerServiceProxy: InstallerServiceProxy,
        private _jobAcknowledgementServiceProxy: JobAcknowledgementServiceProxy,
        private _declarationFormServiceProxy : DeclarationFormServiceProxy,
        private _quickstockServiceProxy: QuickstockServiceProxy,
        private _productPackageServiceProxy: ProductPackageServiceProxy,
        private _greenDealServiceProxy: GreenDealServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private sanitizer: DomSanitizer
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.job.basicCost = 0;
        this.job.solarVLCRebate = 0;
        this.job.solarVLCLoan = 0;
        this.job.totalCost = 0;
        this.job.systemCapacity = 0;
        this.job.stc = 0;
        this.job.stcPrice = 0;
        this.job.rebate = 0;
        this.job.exportDetail=0;
        
        this._jobServiceProxy.getAllHouseTypeForTableDropdown().subscribe(result => {
            this.jobHouseTypes = result;
        });
        this._commonLookupService.getAllJobTypeForTableDropdown().subscribe(result => {
            this.jobTypes = result;
        });
        // this._jobInstallerInvoiceServiceProxy.getInstallerForTableDropdown().subscribe(result => {
        //     this.Installeruser = result;
        // });
        this._jobServiceProxy.getAllRoofTypeForTableDropdown().subscribe(result => {
            this.jobRoofTypes = result;
        });
        this._jobServiceProxy.getAllRoofAngleForTableDropdown().subscribe(result => {
            this.jobRoofAngles = result;
        });
        this._jobServiceProxy.getAllElecDistributorForTableDropdown().subscribe(result => {
            this.jobElecDistributors = result;
        });
        this._jobServiceProxy.getAllElecRetailerForTableDropdown().subscribe(result => {
            this.jobElecRetailers = result;
        });
        this._commonLookupService.getAllPaymentOptionForTableDropdown().subscribe(result => {
            this.jobPaymentOptions = result;
        });
        this._jobServiceProxy.getAllDepositOptionForTableDropdown().subscribe(result => {
            this.jobDepositOption = result;
        });
        this._commonLookupService.getAllFinanceOptionForTableDropdown().subscribe(result => {
            this.jobFinOptions = result;
        });
        this._jobServiceProxy.getAllMeterUpgradeForTableDropdown().subscribe(result => {
            this.jobMeterUpgrade = result;
        });
        this._jobServiceProxy.getAllMeterPhaseForTableDropdown().subscribe(result => {
            this.jobMeterPhase = result;
        });
        this._commonLookupService.getAllVariationForTableDropdown().subscribe(result => {
            this.variations = result;
        });
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
        
        // this._commonLookupService.getAllProductItemForTableDropdown().subscribe(result => {
        //     this.productItems = result;
        // });
        // this._commonLookupService.getAllProductItemForTableDropdownForEdit().subscribe(result => {
        //     this.EditproductItems = result;
        // });

        this._commonLookupService.getAllProductTypeForTableDropdown().subscribe(result => {
            this.productTypes = result;
        });
        this._jobPromotionsServiceProxy.getAllPromotionMasterForTableDropdown().subscribe(result => {
            this.jobPromotionMaster = result;
        });
        this._jobServiceProxy.getSTCZoneRating().subscribe(result => {
            this.STCZoneRating = result;
        });
        this._jobServiceProxy.getSTCPostalCode().subscribe(result => {
            this.STCPostalCode = result;
        });
        this._jobServiceProxy.getSTCYearWiseRate().subscribe(result => {
            this.STCYearWiseRate = result;
        });
        this._quotationsServiceProxy.getAllDocumentType().subscribe(result => {
            this.DocumentTypes = result;
        });
        this._jobRefundsServiceProxy.getAllRefundReasonForTableDropdown().subscribe(result => {
            this.allRefundReasons = result;
        });
        this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
            this.role = result;
        });
        
        this.temporaryFileUrl = "";

        // this._productPackageServiceProxy.getProductPackageForDropdown().subscribe(result => {
        //     this.productPackages = result;
        // });

        this._jobServiceProxy.getWareHouseDropdown('').subscribe(result => {
            this.warehouselocationlist = result;
        });
        this._commonLookupService.getVppConnection().subscribe(result => {
            this.vppConnectionList = result;
        });

        this._jobServiceProxy.getAllCancelReasonForTableDropdown().subscribe(result => {
            this.cancelReasons = result;
        });


    }

    jobInstallationInstallerId : any;
    jobInstallationElectrictionId : any;
    jobInstallationDesignerId : any;
    flexibleRange='';
    isVarify: boolean = false;
    isStateBatteryRebate: boolean = false;
    isOnHoldEdit: boolean = false;

    countCharcters(): void {

        if(this.job.note != null && this.job.note != undefined)
        {
            this.total = this.job.note.length;
        }
    }

    getJobDetailByLeadId(lead, sectionId, disablediv = false,section? : string): void {
        debugger;

        this.isdiablediv = disablediv;
        this.SectionName = section;
        this.retailerdetail = false;
        this.job.removeOldSys = 0
        this.sectionId = sectionId;
        if (lead.id > 0) {
            this.lead = lead;
            this.saving = true;
            this.changeAddress = false;
            this.LeadId = lead.id;
            this.spinnerService.show()
            this._jobServiceProxy.getJobByLeadId(lead.id).subscribe(result => {
                
                if(lead.streetNo != '' && lead.streetName != '' && lead.streetType != '' && lead.suburb != '' && lead.state != '' && lead.postCode != '')
                {
                    this.isVarify = true;
                }
                else
                {
                    this.isVarify = false;
                }

                this.warehouseDistance = result.warehouseDistance;
                if (result.job) {
                    debugger;
                    
                    this.job = result.job;
                    this.GWTID=result.job.gwtId;
                    this.ExtraCostValue=result.extraCostValue;
                    this.flexibleRange=result.job.flexibleRange;
                    this.isStateBatteryRebate = result.isStateBatteryRebate;
                    this.isOnHoldEdit = result.isOnHoldEdit;
                    this.jobaddress = this.job.unitNo + " " + this.job.unitType + " " + this.job.streetNo + " " + this.job.streetName + " " + this.job.streetType;  //result.formatted_address;
                    
                    this.job.state = result.job.state;
                    this.isGoogle = "Google";
                    this.selectAddress = false;
                    this.active = true;
                    this.active1 = false;
                    this.streetType = this.job.streetType;
                    this.streetName = this.job.streetName;
                    this.unitType = this.job.unitType;
                    this.suburb = this.job.suburb;
                    this.approvalDate = this.job.approvalDate;
                    this.sitevisit = this.job.siteVisit == true ? 'Yes' : 'No';
                    this.bonus=this.job.bonus== true ? 'Yes' : 'No';
                    debugger
                    this.broadbandWifiConnection = this.job.isBroadbandWifiConnection === true ? 'Yes' : this.job.isBroadbandWifiConnection === false ? 'No' : 'null';
                    this.expiryDate = this.job.expiryDate;
                    this.job.houseTypeId = (this.job.houseTypeId == null) ? undefined : this.job.houseTypeId;
                    this.job.roofTypeId = (this.job.roofTypeId == null) ? undefined : this.job.roofTypeId;
                    this.job.roofAngleId = (this.job.roofAngleId == null) ? undefined : this.job.roofAngleId;
                    this.job.elecRetailerId = (this.job.elecRetailerId == null) ? undefined : this.job.elecRetailerId;
                    this.job.elecDistributorId = (this.job.elecDistributorId == null) ? undefined : this.job.elecDistributorId;
                    this.job.meterUpgradeId = (this.job.meterUpgradeId == null) ? undefined : this.job.meterUpgradeId;
                    this.job.offPeakMeter = (this.job.offPeakMeter == null) ? undefined : this.job.offPeakMeter;
                    this.job.paymentOptionId = (this.job.paymentOptionId == null) ? undefined : this.job.paymentOptionId;
                    this.job.depositOptionId = (this.job.depositOptionId == null) ? undefined : this.job.depositOptionId;
                    this.job.financeOptionId = (this.job.financeOptionId == null) ? undefined : this.job.financeOptionId;
                    this.job.basicCost = (this.job.basicCost == null) ? 0 : this.job.basicCost;
                    this.basicCost = (this.job.basicCost == null) ? 0 : this.job.basicCost;
                    this.job.solarVLCRebate = (this.job.solarVLCRebate == null) ? 0 : this.job.solarVLCRebate;
                    this.job.solarVLCLoan = (this.job.solarVLCLoan == null) ? 0 : this.job.solarVLCLoan;
                    this.job.depositRequired = (this.job.depositRequired == null) ? 0 : this.job.depositRequired;
                    this.billToPay = (this.billToPay == null) ? 0 : this.billToPay;
                    this.leadId = this.job.leadId;

                    this.job.systemCapacity = (this.job.systemCapacity == null) ? 0 : this.job.systemCapacity;
                    this.job.stc = (this.job.stc == null) ? 0 : this.job.stc;
                    this.job.rebate = (this.job.rebate == null) ? 0 : this.job.rebate;
                    this.job.section = this.SectionName;

                    this.divBasicCost = (this.job.basicCost == null) ? 0 : this.job.basicCost;

                    if(result.job.priceType == ''){
                        this.job.priceType = 'undefined';
                    }
                    
                    this._jobServiceProxy.getJobStatus(this.job.id).subscribe(result => {
                        this.jobCurrentStatus = result;
                    });
                    this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
                        this.role = result;

                        if (this.role == 'Sales Rep' && this.job.jobStatusId == 4) {
                            this.disbaleifsalerep = false;
                            this.SalesRepManageradmin = true;
                        }
                        else if ((this.role == 'Sales Manager' || this.role == 'Sales Rep') && this.job.jobStatusId >= 5) {
                           
                            this.SalesRepManageradmin = false;

                            if(this.role == 'Sales Manager' && this.job.jobStatusId <= 5)
                            {
                                this.disbaleifsalerep = true;
                                this.SalesRepManageradmin = true;
                            }
                        } else if (this.role == 'Admin') {
                            this.SalesRepManageradmin = true;
                            this.disbaleifsalerep = true;
                        }
                        else {
                            this.SalesRepManageradmin = true;
                            this.disbaleifsalerep = true;
                        }
                    });
                    
                    if (this.job.isJobCancelRequest == true && this.role != 'Sales Rep') {
                        this.jobcancelreq = true;
                    } else {
                        this.jobcancelreq = false;
                    }
                    //====Bind Pricklist
                    this._jobServiceProxy.getJobDetailEditByJobID(this.job.id).subscribe(result => {
                        //;
                        this.detailitem = result;
                        //    this.freebies =  result3.jobPromotion;
                        //    this.invoicepayment =  result3.invoicePayment;
                        //    this.jobrefund =  result3.refund;
                    })
                    // this._jobPromotionsServiceProxy.getJobPromotionForEdit(this.job.id).subscribe(result => {      
                    //     this.items = result;
                    // })
                    this._installerServiceProxy.getAllInstallers(this.lead.organizationId).subscribe(result => {
                        this.picklistinstallationList = result;
                    });
                    this._commonLookupService.getSTCProviderDropdown(this.lead.organizationId).subscribe(result => {
                            this.stcprovider = result;
                        });
                        


                    this.picklistwarehouseLocation = undefined;
                    this.getPicklistItems(this.job.id);
                    this._jobProductItemsServiceProxy.getJobProductItemByJobId(this.job.id).subscribe(result => {
                        
                        this.JobProducts = [];
                        this.PicklistProducts = [];
                        this.job.batteryKw = 0;
                        result.map((item) => {
                            var jobproductcreateOrEdit = { id: 0, productItemId: 0, productItemName: "", quantity: 0, jobId: 0, size: 0, model: "", productItems: [], productTypeId: 0, stock: "", datasheetUrl: null, refNo : "" }
                            jobproductcreateOrEdit.productItemId = item.jobProductItem.productItemId;
                            jobproductcreateOrEdit.productTypeId = item.jobProductItem.productTypeId;
                            jobproductcreateOrEdit.quantity = item.jobProductItem.quantity;
                            jobproductcreateOrEdit.id = item.jobProductItem.id;
                            jobproductcreateOrEdit.jobId = this.jobid;
                            jobproductcreateOrEdit.size = item.size;
                            jobproductcreateOrEdit.model = item.model;
                            jobproductcreateOrEdit.productItemName = item.productItemName;
                            jobproductcreateOrEdit.datasheetUrl = item.datasheetUrl;
                            jobproductcreateOrEdit.refNo = item.jobProductItem.refNo;
                            // jobproductcreateOrEdit.productItems = this.productItems;
                            // jobproductcreateOrEdit.productItems = this.productItems.filter(x => x.productTypeId == item.jobProductItem.productTypeId);
                            this.JobProducts.push(jobproductcreateOrEdit);
                            //let pitemname = this.EditproductItems.find(x => x.id == item.jobProductItem.productItemId);
                            //jobproductcreateOrEdit.productItemName = pitemname.displayName;
                            if(item.jobProductItem.productTypeId == 5){
                                this.job.batteryKw = this.job.batteryKw + (item.size * item.jobProductItem.quantity);
                            }
                            this.getStockOnHandForAllView();

                            if (this.PickListtems.length == 0) {
                                this.PicklistProducts.push(jobproductcreateOrEdit);
                                //this.getStockOnHandForAllItem();
                            }
                            debugger;
                            
                        })

                        debugger;
                        if(this.isStateBatteryRebate && !(this.job.batteryRebate > 0)){ 
                            this.job.batteryRebate = Math.floor(this.job.batteryBessPerPrice * this.job.batteryKw * 100) / 100;
                            this.job.batteryBess = Math.floor(this.job.batteryBessState * this.job.batteryKw * 100) / 100;
                        }

                       
                        if(this.isbettaryRebateUpdate){
                            this.CalculateRebate();
                            this.isbettaryRebateUpdate = false;
                        }

                        if (this.PickListtems.length > 0) {
                            this.insName = "";
                            this.picklistType = 1;
                            this.stockWithId = undefined;
                            this.picklistwarehouseLocation = undefined;

                            this.PicklistProducts.push(new CreateOrEditJobProductDto);
                        }

                        if (result.length == 0) {
                            var jobproductcreateOrEdit = { id: 0, productItemId: 0, productItemName: "", quantity: 0, jobId: 0, size: 0, model: "", productItems: [], productTypeId: 0, datasheetUrl: null, refNo : "" }
                            //jobproductcreateOrEdit.productItems = this.EditproductItems;
                            this.JobProducts.push(jobproductcreateOrEdit);
                        }
                        let that = this;
                        setTimeout(function () {
                            that.calculateRates();
                            that.getActualCost();
                            that.invoiceBtnCheck();
                            that.picklistItemCheck();
                        }, 5000);
                        this.saving = false;
                        
                        //this.getStockOnHandForAllView();
                       
                        //this.getStockOnHandForAllItem();
                        
                    });
                    this._jobPromotionsServiceProxy.getJobPromotionByJobId(this.job.id).subscribe(result => {
                        this.JobPromotions = [];
                        result.map((item) => {
                            var jobpromotioncreateOrEdit = new CreateOrEditJobPromotionDto()
                            jobpromotioncreateOrEdit.jobId = item.jobPromotion.jobId;
                            jobpromotioncreateOrEdit.promotionMasterId = item.jobPromotion.promotionMasterId;
                            jobpromotioncreateOrEdit.id = item.jobPromotion.id;
                            this.JobPromotions.push(jobpromotioncreateOrEdit)
                            if (item.jobPromotion.id != undefined && item.jobPromotion.promotionMasterId != undefined) {
                                //console.log('this.JobPromotions', this.JobPromotions);
                            }
                        })

                        if (result.length == 0) {
                            this.JobPromotions.push(new CreateOrEditJobPromotionDto);
                        }
                    });
                    
                    this._jobServiceProxy.getJobOldSystemDetailByJobId(this.job.id).subscribe(result => {
                        // debugger;
                        this.JobOldSystemDetail = [];
                        result.map((item) => {
                            var joboldsystemdetail = new CreateOrEditJobOdSysDetails()
                            joboldsystemdetail.id = item.jobOldSysDetail.id;
                            joboldsystemdetail.jobId = item.jobOldSysDetail.jobId;
                            joboldsystemdetail.quantity = item.jobOldSysDetail.quantity;
                            joboldsystemdetail.name = item.jobOldSysDetail.name;
                            if (item.jobOldSysDetail.id != undefined && item.jobOldSysDetail.jobId != undefined && item.jobOldSysDetail.quantity != undefined) {
                                this.JobOldSystemDetail.push(joboldsystemdetail)
                            }
                        })
                        if (result.length == 0) {
                            var joboldsystemdetail = new CreateOrEditJobOdSysDetails()
                            this.JobOldSystemDetail.push(joboldsystemdetail);
                        }
                    });
                    
                    this._jobVariationsServiceProxy.getJobVariationByJobId(this.job.id).subscribe(result => {
                        
                        this.JobVariations = [];
                        result.map((item) => {
                            debugger;
                            var jobvariationcreateOrEdit = new CreateOrEditJobVariationDto()
                            jobvariationcreateOrEdit.jobId = this.jobid;
                            jobvariationcreateOrEdit.variationId = item.jobVariation.variationId;
                            jobvariationcreateOrEdit.cost = item.jobVariation.cost;
                            jobvariationcreateOrEdit.id = item.jobVariation.id;
                            jobvariationcreateOrEdit.notes = item.jobVariation.notes;
                            if (item.jobVariation.id != undefined && item.jobVariation.jobId != undefined && item.jobVariation.variationId != undefined) {
                                this.JobVariations.push(jobvariationcreateOrEdit)
                            }
                        })
                        if (result.length == 0) {
                            this.JobVariations.push(new CreateOrEditJobVariationDto);
                        }

                        let that = this;
                        setTimeout(function () {
                            that.calculateRates();
                        }, 5000);

                        // let statename = this.job.state;
                        // this._jobServiceProxy.getWareHouseDropdown(statename).subscribe(result => {
                        //     this.warehouselocationlist = result;
                        // });
                        
                    });
                    
                    this._jobServiceProxy.getJobInstallationDetailsForEdit(this.job.id).subscribe(result => {
                        
                        this.jobInstallation.installationDate = result.installationDate;
                        this.jobInstallation.installationTime = result.installationTime;
                        this.jobInstallation.id = result.id,
                        this.jobInstallation.installationNotes = result.installationNotes;
                        this.jobInstallation.installerId = result.installerId;
                        this.jobInstallation.electricianId = result.electricianId;
                        this.jobInstallation.designerId = result.designerId;
                        this.jobInstallation.installerName = result.installerName;
                        this.jobInstallation.designerName = result.designerName;
                        this.jobInstallation.electricianName = result.electricianName;
                        this.jobInstallation.estimateCost = result.estimateCost;
                        if (this.jobInstallation.installationDate != null && this.jobInstallation.installationDate != undefined) {
                            this.jobInstallation.warehouseLocation = result.warehouseLocation;
                        }

                        if(this.jobInstallation.installationDate == null && this.jobInstallation.installationDate == undefined){
                            this.isInstallationDetail = false;
                        }
                        if (this.jobInstallation.installationDate != null && this.jobInstallation.installationDate != undefined) {
                            this.isInstallationDetail = true;
                        }
                    });
                    
                    this.getJobRefund();
                    this.initFileUploader();
                    this.getJobInvoices();
                    this.getJobDocuments();
                    this.getJobQuotations();
                    this.onexportdetail(this.job.exportDetail);
                    this.getExportControl();
                    this.getFeedInTarrif();

                    this.countCharcters();
                    this.getShadingDeclaration();
                    this.getEfficiencyDeclaration();
                    // this.getActualCost();

                    this.spinnerService.hide()

                }
                else {
                    this.active1 = true;
                    this.saving1 = false;
                    this.active = false;
                    this.saving = false;
                    this.jobid = 0;
                    this.isGoogle = "Google";
                    this.selectAddress = false;
                    this.job = new CreateOrEditJobDto();
                    this.job.section = section;
                    this.job.sectionId = sectionId;
                    this.streetType = lead.streetType;
                    this.streetName = lead.streetName;
                    this.unitType = lead.unitType;
                    this.suburb = lead.suburb;
                    this.job.unitNo = lead.unitNo;
                    this.job.streetNo = lead.streetNo;
                    this.job.state = lead.state;
                    this.job.postalCode = lead.postCode;
                    this.JobPromotions = [];
                    this.job.paymentOptionId = 1;
                    this.JobPromotions.push(new CreateOrEditJobPromotionDto);
                    this.JobVariations = [];
                    this.JobVariations.push(new CreateOrEditJobVariationDto);
                    this.JobProducts = [];
                    this.JobProducts.push(new CreateOrEditJobProductDto);
                    this.job.jobTypeId = 1;
                    this.jobaddress = this.job.unitNo + " " + this.job.unitType + " " + this.job.streetNo + " " + this.job.streetName + " " + this.job.streetType;  //result.formatted_address;

                    this.spinnerService.hide()
                }

                // this.getActualCost();
            }, err => {
                this.spinnerService.hide()
            });

            
        }

        
    }

    CalculateRebate() : void{
        debugger;
        this.job.batteryBessPerPrice = this.job.batteryBessPerPriceState;
        this.job.batteryRebate = Math.floor(this.job.batteryBessPerPrice * this.job.batteryKw * 100) / 100;
        this.job.batteryBess = Math.floor(this.job.batteryBessState * this.job.batteryKw * 100) / 100;
    }

    getPicklistItems(jobid): void {
        this.PickListtems = [];
        this._picklistServiceProxy.getPickList(jobid).subscribe(result => {
            this.PickListtems = result;
            if (this.PickListtems.length > 0) {
                this.PicklistProducts = [];
                this.reasonofpicklist = true;
                this.PicklistProducts.push(new CreateOrEditJobProductDto);
            }
        });
    }
    
    savegreenboat(jobid): void {
        if(this.providerid > 0) {
            if(this.providerid == 3) //For Green Deal
            {
                this.spinnerService.show();
                this._greenDealServiceProxy.createOrUpdateGreenDealJob(jobid,this.providerid).subscribe(result => {

                    
                    if(result.code == 200) {
                        this.notify.success("Data Sent Sucessfully", 'Success');
                        this.spinnerService.hide();
                    }
                    else {
                        abp.event.trigger('app.show.viewgreendealerror', result);
                        this.spinnerService.hide();
                    }
                    console.log(result);

                }, e => {  this.spinnerService.hide(); });
            }
            else
            {
                this.spinnerService.show();
                this._jobServiceProxy.createGreenBoatData(jobid,this.providerid).subscribe(result => {
                    this.notify.success("Data Sent Sucessfully", 'Success');
                    this.spinnerService.hide();
                }, e => {  this.spinnerService.hide(); });
            }
            
        }
        else {
            this.notify.warn("Please Select Provider")
        }
    }
    saveGWTID(jobid): void {
        if(this.GWTID != null) {
            
                this.spinnerService.show();
                this._jobServiceProxy.updateGWTID(jobid,this.GWTID).subscribe(result => {
                    this.notify.success("Saved Sucessfully", 'Success');
                    this.spinnerService.hide();
                }, e => {  this.spinnerService.hide(); });
            
            
        }
        else {
            this.notify.warn("Please Provide GWTID")
        }
    }

    generatePickList(PicklistID): void {
        this.downloadPicklist(this.job.id, PicklistID);
    }

    // filterProductItem(item, i): void {
    //     //this.JobProducts[i].productItems = this.productItems.filter(x => x.productTypeId == item.productTypeId);
    // }

    // filterPickListItem(item, i): void {
    //     //this.PicklistProducts[i].productItems = this.productItems.filter(x => x.productTypeId == item.productTypeId);
    // }

    getJobStatus(): void {
        this._jobServiceProxy.getJobStatus(this.job.id).subscribe(result => {
            this.jobCurrentStatus = result;
            this.getJobDetailByLeadId(this.lead, this.sectionId);

            if (this.jobCurrentStatus == "Cancel") {
                this.getJobRefund();
            }
        });
    }

    getJobRefund(): void {
        this._jobRefundsServiceProxy.getJobRefundByJobId(this.job.id).subscribe(result => {
            this.jobRefundList = result;
        });
    }

    getJobInvoices(): void {
        this.totalInvoiceAmountPaid = 0;
        this._invoicePaymentServiceProxy.getInvoicesByJobId(this.job.id).subscribe(result => {
            this.JobInvoices = result;
            this.JobInvoices.map((item) => {
                this.totalInvoiceAmountPaid = this.totalInvoiceAmountPaid + item.invoicePayment.invoicePayTotal;
            });
            this._jobServiceProxy.getJobStatus(this.job.id).subscribe(result => {
                this.jobCurrentStatus = result;
            });
        });
    }

    getJobDocuments(): void {
        this._quotationsServiceProxy.getJobDocumentsByJobId(this.job.id).subscribe(result => {
            this.JobDocuments = result;
            this.vicqRcodeScanDocument = result.find(e => e.documentTitle == 'COES');
        })
    }

    getJobQuotations(): void {
        this._quotationsServiceProxy.getAll(this.job.id, '', 0, 9999).subscribe(result => {

            this.JobQuotations = result.items;
            if (result.items.length > 0) {
                this.reportPath = this.JobQuotations[0].quotation.quoteFilePath;
                this.docavailable = true;
            } else {
                this.docavailable = false;
            }
        })
    }

    fillstatepostcode(): void {
        var splitted = this.suburb.split("||");
        this.job.suburb = splitted[0];
        this.job.state = splitted[1].trim();
        this.job.postalCode = splitted[2].trim();
        this.job.country = "AUSTRALIA";
    }

    postalfillstatepostcode(): void {
        var splitted = this.postalSuburb.split("||");
        this.job.suburb = splitted[0];
        this.job.state = splitted[1].trim();
        this.job.postalCode = splitted[2].trim();
    }

    onAutocompleteSelected(result: PlaceResult) {

        if (result.address_components.length == 7) {
            this.job.unitNo = "";
            this.unitType = "";
            this.job.unitType = "";
            this.job.streetNo = "";
            this.streetName = "";
            this.streetType = "";
            this.suburb = "";
            this.job.state = "";
            this.job.suburbId = 0;
            this.job.stateId = 0;
            this.job.postalCode = "";
            this.job.address = "";
            this.job.country = "";

            this.job.streetNo = result.address_components[0].long_name.toUpperCase();
            var str = result.address_components[1].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.streetType = splitted[3].toUpperCase().trim();
            }
            else if (splitted.length == 3) {
                this.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.streetType = splitted[2].toUpperCase().trim();
            }
            else {
                this.streetName = splitted[0].toUpperCase().trim();
                this.streetType = splitted[1].toUpperCase().trim();
            }
            this.suburb = result.address_components[2].long_name.toUpperCase().trim();
            this.job.state = result.address_components[4].short_name;
            this._leadsServiceProxy.getSuburbId(result.address_components[6].long_name).subscribe(result => {
                this.job.suburbId = result;
            });
            this._leadsServiceProxy.stateId(result.address_components[4].short_name).subscribe(result => {
                this.job.stateId = result;
            });
            this.job.postalCode = result.address_components[6].long_name.toUpperCase();
            this.job.address = this.job.unitNo + " " + this.job.unitType + " " + this.job.streetNo + " " + this.streetName + " " + this.streetType;  //result.formatted_address;
            this.job.country = result.address_components[5].long_name.toUpperCase();
        }
        else if (result.address_components.length == 6) {
            this.job.unitNo = "";
            this.unitType = "";
            this.job.unitType = "";
            this.job.streetNo = "";
            this.streetName = "";
            this.streetType = "";
            this.suburb = "";
            this.job.state = "";
            this.job.suburbId = 0;
            this.job.stateId = 0;
            this.job.postalCode = "";
            this.job.address = "";
            this.job.country = "";

            this.job.streetNo = result.address_components[0].long_name.toUpperCase();
            var str = result.address_components[1].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.streetType = splitted[3].toUpperCase().trim();
            }
            else if (splitted.length == 3) {
                this.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.streetType = splitted[2].toUpperCase().trim();
            }
            else {
                this.streetName = splitted[0].toUpperCase().trim();
                this.streetType = splitted[1].toUpperCase().trim();
            }
            this.suburb = result.address_components[2].long_name.toUpperCase().trim();
            this.job.state = result.address_components[3].short_name;
            this._leadsServiceProxy.getSuburbId(result.address_components[5].long_name).subscribe(result => {
                this.job.suburbId = result;
            });
            this._leadsServiceProxy.stateId(result.address_components[3].short_name).subscribe(result => {
                this.job.stateId = result;
            });
            this.job.postalCode = result.address_components[5].long_name.toUpperCase();
            this.job.address = this.job.unitNo + " " + this.job.unitType + " " + this.job.streetNo + " " + this.streetName + " " + this.streetType;  //result.formatted_address;
            this.job.country = result.address_components[4].long_name.toUpperCase();
        }
        else {
            this.job.unitNo = "";
            this.unitType = "";
            this.job.unitType = "";
            this.job.streetNo = "";
            this.streetName = "";
            this.streetType = "";
            this.suburb = "";
            this.job.state = "";
            this.job.suburbId = 0;
            this.job.stateId = 0;
            this.job.postalCode = "";
            this.job.address = "";
            this.job.country = "";

            var str1 = result.address_components[0].long_name.toUpperCase();
            var splitted1 = str1.split(" ");
            if (splitted1.length == 1) {
                this.job.unitNo = splitted1[0].toUpperCase().trim();
            }
            else {
                this.job.unitNo = splitted1[1].toUpperCase().trim();
                this.unitType = splitted1[0].toUpperCase().trim();
                this.job.unitType = splitted1[0].toUpperCase().trim();
            }
            this.job.streetNo = result.address_components[1].long_name.toUpperCase();
            var str = result.address_components[2].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.streetType = splitted[3].toUpperCase().trim();
            }
            else if (splitted.length == 3) {
                this.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.streetType = splitted[2].toUpperCase().trim();
            }
            else {
                this.streetName = splitted[0].toUpperCase().trim();
                this.streetType = splitted[1].toUpperCase().trim();
            }
            this.suburb = result.address_components[3].long_name.toUpperCase().trim();
            this.job.state = result.address_components[5].short_name;
            this._leadsServiceProxy.getSuburbId(result.address_components[7].long_name).subscribe(result => {
                this.job.suburbId = result;
            });
            this._leadsServiceProxy.stateId(result.address_components[5].short_name).subscribe(result => {
                this.job.stateId = result;
            });
            this.job.postalCode = result.address_components[7].long_name.toUpperCase();
            this.job.address = this.job.unitNo + " " + this.job.unitType + " " + this.job.streetNo + " " + this.streetName + " " + this.streetType;  //result.formatted_address;
            this.job.country = result.address_components[6].long_name.toUpperCase();
        }
    }

    onLocationSelected(location: Location) {
        this.job.latitude = location.latitude.toString();
        this.job.longitude = location.longitude.toString();
    }

    filterunitTypes(event): void {
        this._leadsServiceProxy.getAllUnitType(event.query).subscribe(output => {
            this.unitTypesSuggestions = output;
        });
    }

    postalfilterunitTypes(event): void {
        this._leadsServiceProxy.getAllUnitType(event.query).subscribe(output => {
            this.postalunitTypesSuggestions = output;
        });
    }

    // get StreetType
    filterstreettypes(event): void {
        this._leadsServiceProxy.getAllStreetType(event.query).subscribe(output => {
            this.streetTypesSuggestions = output;
        });
    }

    postalfilterstreettypes(event): void {
        this._leadsServiceProxy.getAllStreetType(event.query).subscribe(output => {
            this.postalstreetTypesSuggestions = output;
        });
    }

    // get StreetName
    filterstreetnames(event): void {
        this._leadsServiceProxy.getAllStreetName(event.query).subscribe(output => {
            this.streetNamesSuggestions = output;
        });
    }

    postalfilterstreetnames(event): void {
        this._leadsServiceProxy.getAllStreetName(event.query).subscribe(output => {
            this.postalstreetNamesSuggestions = output;
        });
    }

    // get Suburb
    filtersuburbs(event): void {
        this._leadsServiceProxy.getAllSuburb(event.query).subscribe(output => {
            this.suburbSuggestions = output;
        });
    }

    // filterProductIteams(event, i): void {
    //     let Id = 0;
    //     for (let j = 0; j < this.JobProducts.length; j++) {
    //         if (j == i) {
    //             Id = this.JobProducts[i].productTypeId
    //             this.JobProducts[i].model = '';
    //         }
    //     }
    //     this._jobServiceProxy.getProductItemList(Id, event.query, this.job.state, true).subscribe(output => {
    //         this.productItemSuggestions = output;
    //     });
    // }

    // filterProductIteamsPickList(event, i): void {
    //     let Id = 0;
    //     for (let j = 0; j < this.PicklistProducts.length; j++) {
    //         if (j == i) {
    //             Id = this.PicklistProducts[i].productTypeId
    //         }
    //     }

    //     this._jobServiceProxy.getProductItemList(Id, event.query, this.job.state, false).subscribe(output => {
    //         this.productItemSuggestions = output;
    //     });
    // }

    getPickListProductTypeId(i) {
        // let pItem = this.productItems.find(x => x.displayName == this.PicklistProducts[i].productItemName)
        // this.PicklistProducts[i].productItemId = pItem.id;

        // if(this.checkStockOnHandFeatures())
        // {
        //     this._quickstockServiceProxy.getStockDetails(this.PicklistProducts[i].productItemId, this.picklistwarehouseLocation).subscribe(result => {
        //         this.PicklistProducts[i].stock = result.stockOnHand;
                
        //     });
        // }
    }

    postalfiltersuburbs(event): void {
        this._leadsServiceProxy.getAllSuburb(event.query).subscribe(output => {
            this.postalsuburbSuggestions = output;
        });
    }

    saveJob(): void {
        this.saving = true;
        this.job.leadId = this.LeadId;
        this.job.streetName = this.streetName;
        this.job.unitType = this.unitType;
        this.job.streetType = this.streetType;
        this.job.suburb = this.suburb;
        let input = {} as ExistJobSiteAddDto;
        input.unitNo = this.job.unitNo;
        input.unitType = this.job.unitType;
        input.streetNo = this.job.streetNo;
        input.streetName = this.job.streetName;
        input.streetType = this.job.streetType;
        input.suburb = this.job.suburb;
        input.state = this.job.state;
        input.postCode = this.job.postalCode;
        input.jobTypeId = this.job.jobTypeId;
        input.jobid = this.job.id;
        input.leadid = this.LeadId;
        input.organizationId = this.lead.organizationId;
        this.job.sectionId = this.sectionId;




        if (input.jobid == null) {
            this._jobServiceProxy.checkjobcreatepermission(input.leadid)
                .subscribe(result1 => {
                    if (result1 != 0) {
                        this._jobServiceProxy.checkExistJobsiteAddList(input)
                            .subscribe(result => {
                                if (result) {
                                    this.saving = false;
                                    this.notify.info(this.l('AddreessAlredyExist'));
                                } else {
                                    this._jobServiceProxy.createOrEdit(this.job)
                                        .pipe(finalize(() => { this.saving = false; }))
                                        .subscribe(() => {
                                            
                                            this.notify.info(this.l('SavedSuccessfully'));
                                            this.active = true;
                                            this.active1 = false;
                                            this.changeAddress = false;
                                            this.selectAddress = true;
                                                    this.isbettaryRebateUpdate = true;
                                                    this.getJobStatus();
                                            this._jobServiceProxy.getJobByLeadId(this.LeadId).subscribe(result => {
                                                if (result.job) {
                                                    this.job = result.job;
                                                    this.job.section = this.SectionName;
                                                    this.job.sectionId = this.sectionId
                                                    this.jobaddress = this.job.unitNo + " " + this.job.unitType + " " + this.job.streetNo + " " + this.job.streetName + " " + this.job.streetType;  //result.formatted_address;

                                                    this.jobid = this.job.id;
                                                    this.getJobStatus();
                                                }
                                            });
                                        });
                                }
                            });
                    } else {
                        this.notify.info(this.l('OnlyAssignUserCanCreateJob'));
                    }
                });
        } else {
            this.message.confirm('Are You Sure You Want To Change Address in Leads',
                                    "",
                                    (isConfirmed) => {
                                        this.job.isLeadAddresschange = isConfirmed;
                                        this._jobServiceProxy.checkExistJobsiteAddList(input)
                                        .subscribe(result => {
                                            if (result) {
                                                this.saving = false;
                                                this.notify.info(this.l('AddreessAlredyExist'));
                                            } else {
                                                this._jobServiceProxy.createOrEdit(this.job)
                                                    .pipe(finalize(() => { this.saving = false; }))
                                                    .subscribe(() => {
                                                        this.notify.info(this.l('SavedSuccessfully'));
                                                        this.active = true;
                                                        this.active1 = false;
                                                        this.changeAddress = false;
                                                        this.selectAddress = true;
                                                        this.isbettaryRebateUpdate = true;
                                                        this.getJobStatus();
                                                        this._jobServiceProxy.getJobByLeadId(this.LeadId).subscribe(result => {
                                                            if (result.job) {
                                                                this.job = result.job;
                                                                this.job.section = this.SectionName;
                                                                this.job.sectionId = this.sectionId;
                                                                this.jobaddress = this.job.unitNo + " " + this.job.unitType + " " + this.job.streetNo + " " + this.job.streetName + " " + this.job.streetType;  //result.formatted_address;

                                                                this.jobid = this.job.id;
                                                                this.getJobStatus();

                                                            }
                                                        });
                                                    });
                                            }
                                        });
                                       
                                        
                                    })


            
        }

    }

    saveSales(): void {

        debugger;
        
        this.saving = true;
        this.job.leadId = this.LeadId;
        this.job.approvalDate = this.approvalDate;
        this.job.siteVisit = this.sitevisit == 'Yes' ? true : false;
        this.job.bonus=this.bonus =='Yes' ? true : false;
        this.job.isBroadbandWifiConnection = this.broadbandWifiConnection === 'Yes' ? true : this.broadbandWifiConnection === 'No' ? false : null;
        this.job.expiryDate = this.expiryDate;
        this.job.jobProductItems = [];
        this.job.sectionId = this.sectionId;
        this.job.flexibleRange=this.flexibleRange;
        if(this.JobProducts.length == 0){
            this.notify.error("Please Select Atleast One Product")
        }
        this.JobProducts.map((item) => {
            let createOrEditJobProductItemDto = new CreateOrEditJobProductItemDto();
            createOrEditJobProductItemDto.productItemId = item.productItemId;
            createOrEditJobProductItemDto.quantity = item.quantity;
            createOrEditJobProductItemDto.jobId = this.jobid;
            createOrEditJobProductItemDto.id = item.id;
            createOrEditJobProductItemDto.refNo = item.refNo;
            if (item.productTypeId != 0 && item.productTypeId != undefined && item.productItemId != undefined && item.productItemId != 0 && item.quantity != undefined && item.model != undefined) {
                this.job.jobProductItems.push(createOrEditJobProductItemDto);
            }else{
                    this.notify.error("Please Select Atleast One Product")
            }
        });

        this.job.jobOldSystemDetail = [];
        this.JobOldSystemDetail.map((item) => {
            var joboldsystemdetail = new CreateOrEditJobOdSysDetails()
            joboldsystemdetail.id = item.id;
            joboldsystemdetail.jobId = item.jobId;
            joboldsystemdetail.quantity = item.quantity;
            joboldsystemdetail.name = item.name;
            if (item.quantity != undefined) {
                this.job.jobOldSystemDetail.push(joboldsystemdetail)
            }
        });

        this.job.jobPromotion = [];
        this.JobPromotions.map((item) => {
            var jobpromotioncreateOrEdit = new CreateOrEditJobPromotionDto()
            jobpromotioncreateOrEdit.jobId = item.jobId;
            jobpromotioncreateOrEdit.promotionMasterId = item.promotionMasterId;
            jobpromotioncreateOrEdit.id = item.id;
            if (item.promotionMasterId != undefined) {
                this.job.jobPromotion.push(jobpromotioncreateOrEdit)
            }
        });

        this.job.jobVariation = [];
        this.JobVariations.map((item) => {
            var jobvariationcreateOrEdit = new CreateOrEditJobVariationDto()
            jobvariationcreateOrEdit.jobId = this.jobid;
            jobvariationcreateOrEdit.variationId = item.variationId;
            jobvariationcreateOrEdit.cost = item.cost;
            jobvariationcreateOrEdit.id = item.id;
            jobvariationcreateOrEdit.notes = item.notes;
            if (item.variationId != undefined  && item.cost != undefined && item.notes != undefined) {
                this.job.jobVariation.push(jobvariationcreateOrEdit)
            }
        });

       
        let input = {} as ExistJobSiteAddDto;
        input.unitNo = this.job.unitNo;
        input.unitType = this.job.unitType;
        input.streetNo = this.job.streetNo;
        input.streetName = this.job.streetName;
        input.streetType = this.job.streetType;
        input.suburb = this.job.suburb;
        input.state = this.job.state;
        input.postCode = this.job.postalCode;
        input.id = this.job.jobTypeId;
        input.id = this.job.id;

        this.JobVariations.map((item) => {
            if(item.variationId){
                var displayName = this.variations.find(x => x.id == item.variationId).displayName;
                if(displayName.includes("Discount") && !item.notes){
                    this.saving = false;
                    return this.notify.error("Please Enter Variation Notes For " +displayName);
                }
            }
        });

        // ;
        // this._jobServiceProxy.checkValidation(this.job)
        //     .subscribe(result => {
        //         if (result) {
        if(this.saving == true){
            
            this._jobServiceProxy.checkExistJobsiteAddList(input)
            .subscribe(result => {
                if (result) {
                    this.saving = false;
                    this.notify.info(this.l('AddreessAlredyExist'));
                } else {
                this.job.address = this.job.unitNo + " " + this.job.unitType + " " + this.job.streetNo + " " + this.streetName + " " + this.streetType;  //result.formatted_address;
                    debugger;
                if(this.job.address == this.jobaddress){
                    debugger
                    this._jobServiceProxy.createOrEdit(this.job)
                    .pipe(finalize(() => { this.saving = false; }))
                    .subscribe(() => {

                        this.JobOldSystemDetail = [];
                        this._jobServiceProxy.getJobOldSystemDetailByJobId(this.job.id).subscribe(result => {
                            result.map((item) => {
                                var joboldsystemdetail = new CreateOrEditJobOdSysDetails()
                                joboldsystemdetail.id = item.jobOldSysDetail.id;
                                joboldsystemdetail.jobId = item.jobOldSysDetail.jobId;
                                joboldsystemdetail.quantity = item.jobOldSysDetail.quantity;
                                joboldsystemdetail.name = item.jobOldSysDetail.name;
                                if (item.jobOldSysDetail.id != undefined && item.jobOldSysDetail.jobId != undefined && item.jobOldSysDetail.quantity != undefined) {
                                    this.JobOldSystemDetail.push(joboldsystemdetail)
                                }
                            })
                            if (result.length == 0) {
                                var joboldsystemdetail = new CreateOrEditJobOdSysDetails()
                                this.JobOldSystemDetail.push(joboldsystemdetail);
                            }
                        });
                        this._jobProductItemsServiceProxy.getJobProductItemByJobId(this.job.id).subscribe(result => {
                            //debugger;
                            this.JobProducts = [];
                            this.PicklistProducts = [];
                            result.map((item) => {
                                var jobproductcreateOrEdit = { id: 0, productItemId: 0, productItemName: "", quantity: 0, jobId: 0, size: 0, model: "", productItems: [], productTypeId: 0, stock: 0, datasheetUrl: null, refno : "" }
                                jobproductcreateOrEdit.productItemId = item.jobProductItem.productItemId;
                                jobproductcreateOrEdit.productTypeId = item.jobProductItem.productTypeId;
                                jobproductcreateOrEdit.quantity = item.jobProductItem.quantity;
                                jobproductcreateOrEdit.id = item.jobProductItem.id;
                                jobproductcreateOrEdit.jobId = this.jobid;
                                jobproductcreateOrEdit.size = item.size;
                                jobproductcreateOrEdit.model = item.model;
                                jobproductcreateOrEdit.productItemName = item.productItemName;
                                jobproductcreateOrEdit.datasheetUrl = item.datasheetUrl;
                                jobproductcreateOrEdit.refno = item.jobProductItem.refNo;
                                // jobproductcreateOrEdit.productItems = this.productItems;
                                //jobproductcreateOrEdit.productItems = this.productItems.filter(x => x.productTypeId == item.jobProductItem.productTypeId);
                                this.JobProducts.push(jobproductcreateOrEdit);
                                // let pitemname = this.EditproductItems.find(x => x.id == item.jobProductItem.productItemId);
                                // jobproductcreateOrEdit.productItemName = pitemname.displayName;
                                
                                if (this.PickListtems.length == 0) {
                                    this.PicklistProducts.push(jobproductcreateOrEdit);
                                }
                                else{
                                    this.PicklistProducts.push(new CreateOrEditJobProductDto);
                                }
                                
                            })
                            if (result.length == 0) {
                                var jobproductcreateOrEdit = { id: 0, productItemId: 0, productItemName: "", quantity: 0, jobId: 0, size: 0, model: "", productItems: [], productTypeId: 0, stock: 0, datasheetUrl: null, refNo :"" }
                                //jobproductcreateOrEdit.productItems = this.EditproductItems;
                                this.JobProducts.push(jobproductcreateOrEdit);
                            }
                            let that = this;
                            setTimeout(function () {
                                that.calculateRates();
                            }, 5000);
                            this.saving = false;

                           
                        });

                        this._jobPromotionsServiceProxy.getJobPromotionByJobId(this.job.id).subscribe(result => {
                            this.JobPromotions = [];
                            result.map((item) => {
                                var jobpromotioncreateOrEdit = new CreateOrEditJobPromotionDto()
                                jobpromotioncreateOrEdit.jobId = this.jobid;
                                jobpromotioncreateOrEdit.promotionMasterId = item.jobPromotion.promotionMasterId;
                                jobpromotioncreateOrEdit.id = item.jobPromotion.id;
                                if (item.jobPromotion.id != undefined && item.jobPromotion.promotionMasterId != undefined) {
                                    this.JobPromotions.push(jobpromotioncreateOrEdit)
                                    //console.log('this.JobPromotions', this.JobPromotions);
                                }
                            })

                            if (result.length == 0) {
                                this.JobPromotions.push(new CreateOrEditJobPromotionDto);
                            }
                        });
                        
                        this._jobVariationsServiceProxy.getJobVariationByJobId(this.job.id).subscribe(result => {
                            this.JobVariations = [];
                            result.map((item) => {
                                var jobvariationcreateOrEdit = new CreateOrEditJobVariationDto()
                                jobvariationcreateOrEdit.jobId = this.jobid;
                                jobvariationcreateOrEdit.variationId = item.jobVariation.variationId;
                                jobvariationcreateOrEdit.cost = item.jobVariation.cost;
                                jobvariationcreateOrEdit.id = item.jobVariation.id;
                                jobvariationcreateOrEdit.notes = item.jobVariation.notes;
                                if (item.jobVariation.id != undefined && item.jobVariation.jobId != undefined && item.jobVariation.variationId != undefined && item.jobVariation.cost != undefined) {
                                    this.JobVariations.push(jobvariationcreateOrEdit)
                                }
                            })
                            if (result.length == 0) {
                                this.JobVariations.push(new CreateOrEditJobVariationDto);
                            }

                            let that = this;
                            setTimeout(function () {
                                that.calculateRates();
                            }, 5000);

                            let statename = this.job.state;
                            this._jobServiceProxy.getWareHouseDropdown(statename).subscribe(result => {
                                this.warehouselocationlist = result;
                            });
                        });
                        if (this.job.paymentOptionId == 1) {
                            this.job.financeOptionId = undefined;
                            this.job.depositOptionId = undefined;
                            this.job.financePurchaseNo = null;
                        }
                        
                        this.divBasicCost = this.job.basicCost;

                        this.notify.info(this.l('SavedSuccessfully'));   
                        debugger;                         
                    });
                }
                else{
                    this.message.confirm('Are You Sure You Want To Change Address in Leads',
                        "",
                    (isConfirmed) => {

                        this.job.isLeadAddresschange = isConfirmed ;
                        this._jobServiceProxy.createOrEdit(this.job)
                        .pipe(finalize(() => { this.saving = false; }))
                        .subscribe(() => {

                            this.JobOldSystemDetail = [];
                            this._jobServiceProxy.getJobOldSystemDetailByJobId(this.job.id).subscribe(result => {
                                result.map((item) => {
                                    var joboldsystemdetail = new CreateOrEditJobOdSysDetails()
                                    joboldsystemdetail.id = item.jobOldSysDetail.id;
                                    joboldsystemdetail.jobId = item.jobOldSysDetail.jobId;
                                    joboldsystemdetail.quantity = item.jobOldSysDetail.quantity;
                                    joboldsystemdetail.name = item.jobOldSysDetail.name;
                                    if (item.jobOldSysDetail.id != undefined && item.jobOldSysDetail.jobId != undefined && item.jobOldSysDetail.quantity != undefined) {
                                        this.JobOldSystemDetail.push(joboldsystemdetail)
                                    }
                                })
                                if (result.length == 0) {
                                    var joboldsystemdetail = new CreateOrEditJobOdSysDetails()
                                    this.JobOldSystemDetail.push(joboldsystemdetail);
                                }
                            });
                            this._jobProductItemsServiceProxy.getJobProductItemByJobId(this.job.id).subscribe(result => {
                                //debugger;
                                this.JobProducts = [];
                                this.PicklistProducts = [];
                                result.map((item) => {
                                    var jobproductcreateOrEdit = { id: 0, productItemId: 0, productItemName: "", quantity: 0, jobId: 0, size: 0, model: "", productItems: [], productTypeId: 0, stock: 0, datasheetUrl: null, refNo : "" }
                                    jobproductcreateOrEdit.productItemId = item.jobProductItem.productItemId;
                                    jobproductcreateOrEdit.productTypeId = item.jobProductItem.productTypeId;
                                    jobproductcreateOrEdit.quantity = item.jobProductItem.quantity;
                                    jobproductcreateOrEdit.id = item.jobProductItem.id;
                                    jobproductcreateOrEdit.jobId = this.jobid;
                                    jobproductcreateOrEdit.size = item.size;
                                    jobproductcreateOrEdit.model = item.model;
                                    jobproductcreateOrEdit.productItemName = item.productItemName;
                                    jobproductcreateOrEdit.datasheetUrl = item.datasheetUrl;
                                    jobproductcreateOrEdit.refNo = item.jobProductItem.refNo;
                                    // jobproductcreateOrEdit.productItems = this.productItems;
                                    //jobproductcreateOrEdit.productItems = this.productItems.filter(x => x.productTypeId == item.jobProductItem.productTypeId);
                                    this.JobProducts.push(jobproductcreateOrEdit);
                                    // let pitemname = this.EditproductItems.find(x => x.id == item.jobProductItem.productItemId);
                                    // jobproductcreateOrEdit.productItemName = pitemname.displayName;
                                    
                                    if (this.PickListtems.length == 0) {
                                        this.PicklistProducts.push(jobproductcreateOrEdit);
                                    }
                                    else{
                                        this.PicklistProducts.push(new CreateOrEditJobProductDto);
                                    }
                                    
                                })
                                if (result.length == 0) {
                                    var jobproductcreateOrEdit = { id: 0, productItemId: 0, productItemName: "", quantity: 0, jobId: 0, size: 0, model: "", productItems: [], productTypeId: 0, stock: 0, datasheetUrl: null, refNo : "" }
                                    //jobproductcreateOrEdit.productItems = this.EditproductItems;
                                    this.JobProducts.push(jobproductcreateOrEdit);
                                }
                                let that = this;
                                setTimeout(function () {
                                    that.calculateRates();
                                }, 5000);
                                this.saving = false;

                               
                            });

                            this._jobPromotionsServiceProxy.getJobPromotionByJobId(this.job.id).subscribe(result => {
                                this.JobPromotions = [];
                                result.map((item) => {
                                    var jobpromotioncreateOrEdit = new CreateOrEditJobPromotionDto()
                                    jobpromotioncreateOrEdit.jobId = this.jobid;
                                    jobpromotioncreateOrEdit.promotionMasterId = item.jobPromotion.promotionMasterId;
                                    jobpromotioncreateOrEdit.id = item.jobPromotion.id;
                                    if (item.jobPromotion.id != undefined && item.jobPromotion.promotionMasterId != undefined) {
                                        this.JobPromotions.push(jobpromotioncreateOrEdit)
                                        //console.log('this.JobPromotions', this.JobPromotions);
                                    }
                                })

                                if (result.length == 0) {
                                    this.JobPromotions.push(new CreateOrEditJobPromotionDto);
                                }
                            });
                            
                            this._jobVariationsServiceProxy.getJobVariationByJobId(this.job.id).subscribe(result => {
                                this.JobVariations = [];
                                result.map((item) => {
                                    var jobvariationcreateOrEdit = new CreateOrEditJobVariationDto()
                                    jobvariationcreateOrEdit.jobId = this.jobid;
                                    jobvariationcreateOrEdit.variationId = item.jobVariation.variationId;
                                    jobvariationcreateOrEdit.cost = item.jobVariation.cost;
                                    jobvariationcreateOrEdit.id = item.jobVariation.id;
                                    jobvariationcreateOrEdit.notes = item.jobVariation.notes;
                                    if (item.jobVariation.id != undefined && item.jobVariation.jobId != undefined && item.jobVariation.variationId != undefined && item.jobVariation.cost != undefined) {
                                        this.JobVariations.push(jobvariationcreateOrEdit)
                                    }
                                })
                                if (result.length == 0) {
                                    this.JobVariations.push(new CreateOrEditJobVariationDto);
                                }

                                let that = this;
                                setTimeout(function () {
                                    that.calculateRates();
                                }, 5000);

                                let statename = this.job.state;
                                this._jobServiceProxy.getWareHouseDropdown(statename).subscribe(result => {
                                    this.warehouselocationlist = result;
                                });
                            });
                            if (this.job.paymentOptionId == 1) {
                                this.job.financeOptionId = undefined;
                                this.job.depositOptionId = undefined;
                                this.job.financePurchaseNo = null;
                            }
                            
                            this.divBasicCost = this.job.basicCost;
                            this.notify.info(this.l('SavedSuccessfully'));   
                            debugger;                         
                        });
                        
                    });
}
                  

                    
                    

                }
            });
        }
        

        // }
        // else {
        //     this.saving = false;
        //     this.notify.info(this.l('Please Fill All Require Fields'));
        // }
        // });
    }

    picklistType: number = 1;
    savePicklistDetails(): void {
        debugger;
        this.saving = true;
        this.picklist.jobProductItems = [];
        this.picklist.jobId = this.job.id;
        this.picklist.reason = this.picklistreason;
        this.picklist.sectionId = this.sectionId;
        this.picklist.picklistwarehouseLocation = this.picklistwarehouseLocation;
        this.picklist.picklistinstallerId = this.picklistinstallerId;
        this.picklist.stockWithId = this.stockWithId;
        this.picklist.picklistType = this.picklistType;
        this.picklist.sectionId =this.sectionId;
        this.PicklistProducts.map((item) => {
            let createOrEditJobProductItemDto = new CreateOrEditJobProductItemDto();
            createOrEditJobProductItemDto.productItemId = item.productItemId;
            createOrEditJobProductItemDto.productTypeId = item.productTypeId;
            createOrEditJobProductItemDto.quantity = item.quantity;
            createOrEditJobProductItemDto.jobId = this.job.id;
            createOrEditJobProductItemDto.productTypeId = item.productTypeId;
            createOrEditJobProductItemDto.stock = item.stock;
            this.picklist.jobProductItems.push(createOrEditJobProductItemDto);
        });

        // this._picklistServiceProxy.savePickList(this.picklist)
        //     .pipe(finalize(() => { this.saving = false; }))
        //     .subscribe(() => {
        //         this.picklist.reason = null;
        //         this.getPicklistItems(this.job.id);
        //         this.notify.info(this.l('SavedSuccessfully'));
        //     });

        this._picklistServiceProxy.checkInstallationPicklistExists(this.job.id).subscribe(res => {
            if(res && this.picklistType == 1)
            {
                this.message.confirm(
                    this.l('PicklistAlertDetails'),
                    this.l('PicklistAlert'),
                    (isConfirmed) => {
                        if (isConfirmed) {
                            this._picklistServiceProxy.savePickList(this.picklist)
                            .pipe(finalize(() => { this.saving = false; }))
                            .subscribe(() => {
                                let log = new UserActivityLogDto();
                                log.actionId = 13;
                                log.actionNote = 'View Payment Detail';
                                log.section = this.SectionName;
                                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                    .subscribe(() => {
                                }); 
                                this.picklist.reason = null;
                                this.getPicklistItems(this.job.id);
                                this.notify.success(this.l('SavedSuccessfully'));
                            });
                        }
                        else {
                            this.saving = false;
                        }
                    }
                );
            }
            else{
                this._picklistServiceProxy.savePickList(this.picklist)
                .pipe(finalize(() => { this.saving = false; }))
                .subscribe(() => {
                    let log = new UserActivityLogDto();
                    log.actionId = 13;
                    log.actionNote = 'View Payment Detail';
                    log.section = this.SectionName;
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                    this.picklist.reason = null;
                    this.getPicklistItems(this.job.id);
                    this.notify.success(this.l('SavedSuccessfully'));
                });
            }
        }, err => {
            this.saving = false;
        } );
        
    }

    deletePicklistDetails(picklistID): void {
        this.message.confirm(
            this.l('AreYouSureWanttoDelete'),
            this.l('Delete'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._picklistServiceProxy.deletePickList(picklistID, this.leadId,this.sectionId)
                        .subscribe(() => {
                            let log = new UserActivityLogDto();
                            log.actionId = 34;
                            log.actionNote = 'PickList Deleted';
                            log.section = this.SectionName;
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            }); 
                            this.PickListtems = this.PickListtems.filter(x => x.id != picklistID);
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            this.getJobDetailByLeadId(this.lead, this.sectionId);
                        });
                }
            }
        );
    }

    deleteDocument(documentId) : void {
        this.message.confirm(
            this.l('AreYouSureWanttoDelete'),
            this.l('Delete'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._quotationsServiceProxy.deleteJobDocument(documentId,  this.sectionId)
                        .subscribe(() => {
                            let log = new UserActivityLogDto();
                            log.actionId = 44;
                            log.actionNote = 'Document Deleted';
                            log.section = this.SectionName;
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            }); 
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            this.getJobDocuments();
                        });
                }
            }
        );
    }
    deletejobAcknowledgement(Id) : void {
        this.message.confirm(
            this.l('AreYouSureWanttoDelete'),
            this.l('Delete'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._jobAcknowledgementServiceProxy.deletejobAcknowledgement(Id,  this.sectionId)
                        .subscribe(() => {
                            let log = new UserActivityLogDto();
                            log.actionId = 44;
                            log.actionNote = 'Job Acknowledgement Form Deleted';
                            log.section = this.SectionName;
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            }); 
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            this.getExportControl();
                            this.getFeedInTarrif();
                        });
                }
            }
        );
    }
    
    deleteDeclarationForm(Id) : void {
        this.message.confirm(
            this.l('AreYouSureWanttoDelete'),
            this.l('Delete'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._declarationFormServiceProxy.deleteDeclaration(Id,  this.sectionId)
                        .subscribe(() => {
                            let log = new UserActivityLogDto();
                            log.actionId = 44;
                            log.actionNote = 'Declaration Form Deleted';
                            log.section = this.SectionName;
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            }); 
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            this.getShadingDeclaration();
                            this.getEfficiencyDeclaration();
                        });
                }
            }
        );
    }
    validateJob() {
        if (this.job.jobProductItems.length > 0) {
            let hasduplicateProduct = this.job.jobProductItems.some(function (item) {
                return this.job.jobProductItems.indexOf(item) !== this.job.jobProductItems.lastIndexOf(item);
            });
            if (hasduplicateProduct) {
                this.notify.error(this.l('ProductItemsDuplicate'));
            }
        } else {
            this.notify.error(this.l('AtleastOneProductItemsRequired'));
        }

        if (this.job.jobPromotion.length > 0) {
            let hasduplicatePromotion = this.job.jobPromotion.some(function (item) {
                return this.job.jobPromotion.indexOf(item) !== this.job.jobPromotion.lastIndexOf(item);
            });
            if (hasduplicatePromotion) {
                this.notify.error(this.l('DuplicatePromotion'));
            }
        }

        if (this.job.jobVariation.length > 0) {
            let hasduplicateVariation = this.job.jobVariation.some(function (item) {
                return this.job.jobVariation.indexOf(item) !== this.job.jobVariation.lastIndexOf(item);
            });
            if (hasduplicateVariation) {
                this.notify.error(this.l('DuplicatePromotion'));
            }
        } else {
            this.notify.error(this.l('AtleastOneVariationRequired'));
        }
    }

    changeAddressToggle(): void {
        this.changeAddress = true;
    }

    calculateRates(): void {
        let that = this;
        let BasicTotal = this.job.basicCost;
        let SolarVLCRebate = this.job.solarVICRebate;
        let SolarVICLoanDiscont = this.job.solarVICLoanDiscont;
        if (this.JobVariations) {
            this.JobVariations.forEach(function (item) {
                let varobj = that.variations.filter(x => x.id == item.variationId);
                if (item.cost > 0) {
                    if (varobj[0].actionName == "Minus") {
                        BasicTotal = parseFloat(BasicTotal.toString()) - parseFloat(item.cost.toString());
                    }
                    else {
                        BasicTotal = parseFloat(BasicTotal.toString()) + parseFloat(item.cost.toString());
                    }
                } else {
                    BasicTotal = parseFloat(BasicTotal.toString());
                }
            })
            if (this.job.solarVICRebate != null && this.job.solarVICLoanDiscont != null) {
                this.job.totalCost = parseFloat(BasicTotal.toString());
                this.job.solarVICRebate = parseFloat(SolarVLCRebate.toString());
                this.job.solarVICLoanDiscont = parseFloat(SolarVICLoanDiscont.toString());
                let totalCostafterdic = (this.job.totalCost) - (this.job.solarVICRebate + this.job.solarVICLoanDiscont)
                if (this.job.depositRequired < 0) {
                    this.job.depositRequired = Math.round(totalCostafterdic * 10 / 100);
                }
                this.billToPay = (totalCostafterdic) - (this.job.depositRequired);
            } else {
                this.job.totalCost = parseFloat(BasicTotal.toString());
                if (this.job.depositRequired < 0) {
                    this.job.depositRequired = Math.round(this.job.totalCost * 10 / 100);
                }
                this.billToPay = (this.job.totalCost) - (this.job.depositRequired);
            }
        }
    }

    finalAmount(): void {

        if (this.job.state == 'VIC') {
            this.billToPay = (this.job.totalCost) - (this.job.solarVICRebate + this.job.solarVICLoanDiscont) - (this.job.depositRequired);

        } else {
            this.billToPay = (this.job.totalCost) - (this.job.depositRequired);
        }
    }

    calculateSTCRebate(i): void {
        debugger;
        let id = this.productTypes.find(x => x.id == 1);

        if(this.job.state == 'QLD' && this.JobProducts[i].productTypeId == 5){
            if(this.JobProducts[i].productItemId > 0 && this.JobProducts[i].quantity >0){
                this._jobServiceProxy.getRefNoForItme(this.JobProducts[i].productItemId, this.JobProducts[i].quantity).subscribe(result => {
                    if(result != null && result != undefined && result.trim() != ""){
                        this.job.rebateRefNo = result;

                    }
                });
            }
        }
        this.job.batteryKw = 0
        this.JobProducts.forEach(element => {
            if(element.productTypeId == 5 && element.quantity > 0){
                var kw = element.quantity * element.size;

                this.job.batteryKw = this.job.batteryKw + kw;
            }
        });
        this.CalculateRebate();
        

        if (this.JobProducts[i].productTypeId == id.id) {
            this.job.systemCapacity = 0;
            this.job.stc = 0;
            this.job.stcPrice = 0;
            this.job.rebate = 0;
            this.job.stcPrice = 36;

            debugger

            if (this.STCPostalCode.length > 0) {
                let stcpostalcode = this.STCPostalCode.find(x => parseInt(x.postCodeFrom) <= parseInt(this.job.postalCode) && parseInt(x.postCodeTo) >= parseInt(this.job.postalCode));
                if (this.STCYearWiseRate.length > 0) {
                    let stcRate = this.STCYearWiseRate.find(x => x.year == (new Date()).getFullYear());
                    if (stcpostalcode) {
                        if (this.STCZoneRating.length > 0) {
                            let zoneRating = this.STCZoneRating.find(x => x.id === stcpostalcode.zoneId);
                            
                            let size = this.JobProducts[i].size;
                            // if (pItem) {
                            this.job.systemCapacity = parseFloat((this.JobProducts[i].quantity * size / 1000).toFixed(2));
                            this.job.stc = Math.floor((this.job.systemCapacity * zoneRating.rating * stcRate.rate));
                            this.job.rebate = this.job.stc * 36;
                            //     this.JobProducts[i].model = pItem.model;
                            // }
                        }
                    }
                }
            }
        }
        else {
            // this.job.systemCapacity = 0;
            // this.job.stc = 0;
            // this.job.stcPrice = 0;
            // this.job.rebate = 0;
            // this.job.stcPrice = 36;
            
            if (this.STCPostalCode.length > 0) {
                let stcpostalcode = this.STCPostalCode.find(x => x.postCodeFrom <= this.job.postalCode && x.postCodeTo >= this.job.postalCode);
                if (this.STCYearWiseRate.length > 0) {
                    let stcRate = this.STCYearWiseRate.find(x => x.year == (new Date()).getFullYear());
                    if (stcpostalcode) {
                        if (this.STCZoneRating.length > 0) {
                            let zoneRating = this.STCZoneRating.find(x => x.id === stcpostalcode.zoneId);
                            //let pItem = this.productItems.find(x => x.id == this.JobProducts[i].productItemId);
                            // if (pItem) {
                            //     this.JobProducts[i].model = pItem.model;
                            // }
                        }
                    }
                }
            }
        }

        this.getActualCost();
    }

    addJobVariation(item): void {
        debugger;
        if(!item.variationId){
            this.notify.error("Please Select variationId");
            return;
        }
        if(!item.cost){
            this.notify.error("Please Enter Cost");
            return;
        }
        var displayName = this.variations.find(e => e.id == item.variationId).displayName;
        
        if(displayName.includes("Discount") && !item.notes){
            this.notify.error("Please Enter Notes");
            return;
        }
        
        this.JobVariations.push(new CreateOrEditJobVariationDto);
    }

    addJobProduct(): void {
        this.JobProducts.push(new CreateOrEditJobProductDto);
    }
    
    addOldSysDetail(): void {
        this.JobOldSystemDetail.push(new CreateOrEditJobOdSysDetails);
    }

    addPicklistProduct(): void {
        this.PicklistProducts.push(new CreateOrEditJobProductDto);
    }

    addJobPromotion(): void {
        this.JobPromotions.push(new CreateOrEditJobPromotionDto);
    }

    removeJobVariation(rowitem): void {
        debugger;
        // if (this.JobVariations.length == 1)
        //     return;
        // this.JobVariations = this.JobVariations.filter(item => item.variationId != JobVariation.variationId);

        if (this.JobVariations.indexOf(rowitem) === -1) {
            // this.JobVariations.push(rowitem);
        } else {
            this.JobVariations.splice(this.JobVariations.indexOf(rowitem), 1);
        }

        if(this.JobVariations.length == 0){
            this.JobVariations.push(new CreateOrEditJobVariationDto);
        }

        this.calculateRates();
        this.getActualCost();
    }

    removeJobProduct(JobProduct): void {
        if (this.JobProducts.length == 1)
            return;
       
        if (this.JobProducts.indexOf(JobProduct) === -1) {
            
        } else {
            this.JobProducts.splice(this.JobProducts.indexOf(JobProduct), 1);
        }
        this.calculateSTCRebate(0);
        this.invoiceBtnCheck();

        if(JobProduct.productTypeId == 1)
        {
            this.job.systemCapacity = 0;
            this.job.stc = 0;
            this.job.stcPrice = 0;
            this.job.rebate = 0;
        }
    }

    removeJoboldsysdetail(Joboldsystemdetails): void {
        if (this.JobOldSystemDetail.length == 1)
            return;
       
        if (this.JobOldSystemDetail.indexOf(Joboldsystemdetails) === -1) {
            
        } else {
            this.JobOldSystemDetail.splice(this.JobOldSystemDetail.indexOf(Joboldsystemdetails), 1);
        }
    }
    removePicklistProduct(picklist): void {
        
        if (this.PicklistProducts.length == 1)
        return;
        
        if (this.PicklistProducts.indexOf(picklist) === -1) {
        } else {
            this.PicklistProducts.splice(this.PicklistProducts.indexOf(picklist), 1);
        }
    }

    removeJobPromotion(JobPromotion): void {
        if (this.JobPromotions.length == 1)
            return;
        // this.JobPromotions = this.JobPromotions.filter(item => item.promotionMasterId != JobPromotion.promotionMasterId);
        if (this.JobPromotions.indexOf(JobPromotion) === -1) {
            // this.JobPromotions.push(JobPromotion);
        } else {
            this.JobPromotions.splice(this.JobPromotions.indexOf(JobPromotion), 1);
        }
    }

    google(): void {
        this.selectAddress = false;
    }

    database(): void {
        this.selectAddress = true;
    }

    PostInstallationstatuschanged(): void {
        if (this.job.postInstallationStatus == 2) {
            this.job.installedcompleteDate = null;
        }
        else {
            this.job.incompleteReason = "";
        }

    }
    //Quotation 
    fileChangeEvent(event: any): void {
        debugger
        if (event.target.files[0].size > 5242880) { //5MB
            this.message.warn(this.l('ProfilePicture_Warn_SizeLimit', this.maxfileBytesUserFriendlyValue));
            return;
        }

        this.uploader.clearQueue();
        this.uploader.addToQueue([<File>event.target.files[0]]);
    }

    initFileUploader(): void {
        debugger
        this.uploader = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Profile/UploadFile' });
        this._uploaderOptions.autoUpload = false;
        this._uploaderOptions.authToken = 'Bearer ' + this._tokenService.getToken();
        this._uploaderOptions.removeAfterUpload = true;
        this.uploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };
        this.fileupload = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Profile/UploadFile' });
        this._fileuploadoption.autoUpload = false;
        this._fileuploadoption.authToken = 'Bearer ' + this._tokenService.getToken();
        this._fileuploadoption.removeAfterUpload = true;
        this.fileupload.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

        this.uploader.onBuildItemForm = (fileItem: FileItem, form: any) => {
            form.append('FileType', fileItem.file.type);
            form.append('FileName', fileItem.file.name);
            form.append('FileToken', this.guid());
            this.filenName.push(fileItem.file.name);

        };
        this.fileupload.onBuildItemForm = (fileItem: FileItem, form: any) => {
            form.append('FileType', fileItem.file.type);
            form.append('FileName', fileItem.file.name);
            form.append('FileToken', this.guid1());
            this.filenName.push(fileItem.file.name);

        };
        this.uploader.onSuccessItem = (item, response, status) => {
            const resp = <IAjaxResponse>JSON.parse(response);
            if (resp.success) {
                this.updateFile(resp.result.fileToken, resp.result.fileName, resp.result.fileType, resp.result.filePath);
            } else {
                this.message.error(resp.error.message);
            }
        };
        this.fileupload.onSuccessItem = (item, response, status) => {
            const resp = <IAjaxResponse>JSON.parse(response);
            if (resp.success) {
                this.updateInstallerInvoiceFile(resp.result.fileToken, resp.result.fileName, resp.result.fileType, resp.result.filePath);
            } else {
                this.message.error(resp.error.message);
            }
        };
        this.uploader.setOptions(this._uploaderOptions);
        this.fileupload.setOptions(this._fileuploadoption);
    }

    guid(): string {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }

    guid1(): string {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }

    updateFile(fileToken: string, fileName: string, fileType: string, filePath: string): void {
        const input = new UploadDocumentInput();
        input.fileToken = fileToken;
        input.jobId = this.job.id;
        input.documentTypeId = this.documentTypeId;
        input.fileName = fileName;
        input.fileType = fileType;
        input.filePath = filePath;
        input.sectionName = "Quotation";
        this.saving = true;
        this.spinner.show();
        input.sectionId =this.sectionId;
        this._quotationsServiceProxy.saveDocument(input)
            .pipe(finalize(() => {
                this.saving = false;
                this.spinner.hide();
            }))
            .subscribe(() => {
                let log = new UserActivityLogDto();
                log.actionId = 17;
                log.actionNote = 'Job Document Uploaded';
                log.section = this.SectionName;
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
                this.getJobDocuments();
                this.notify.info(this.l('SavedSuccessfully'));

            });
    }

    updateInstallerInvoiceFile(fileToken: string, fileName: string, fileType: string, filePath: string): void {
        this.installerInv.fileToken = fileToken;
        this.installerInv.jobId = this.job.id;
        this.installerInv.fileName = fileName;
        this.installerInv.filePath = filePath;
        this.installerInv.sectionId = this.sectionId;
        this.saving = true;
        this._jobInstallerInvoiceServiceProxy.createOrEdit(this.installerInv)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.installerInv.invTypeId = null;
                this.installerInv.invNo = null;
                this.installerInv.amount = null;
                this.installerInv.invDate = null;
                this.installerInv.advanceAmount = null;
                this.installerInv.lessDeductAmount = null;
                this.installerInv.totalAmount = null;
                this.installerInv.advancePayDate = null;
                this.installerInv.payDate = null;
                this.installerInv.paymentsTypeId = null;
                this.installerInv.installerId = null;
                this.installerInv.notes = null;
                this.fileupload = null;
            });
    }

    // filechangeEvent(event: any): void {
    //     debugger
    //     if (event.target.files[0].size > 5242880) { //5MB
    //         this.message.warn(this.l('ProfilePicture_Warn_SizeLimit', this.maxfileBytesUserFriendlyValue));
    //         return;
    //     }
    //     this.fileupload.clearQueue();
    //     this.fileupload.addToQueue([<File>event.target.files[0]]);
    // }


    downloadfile(file): void {

        var date = moment(file.creationTime).toDate()
        var MigrationDate = new Date('2022/09/17')

        if(this.lead.organizationId == 7) {
            MigrationDate = new Date('2023/01/04')
        }

        if(this.lead.organizationId == 8 || this.lead.organizationId == 7)
        {
            if(date < MigrationDate)
            {
                let FileName = AppConsts.oldDocUrl + "/" + file.documentPath + file.fileName;
                window.open(FileName, "_blank");
            }
            else{
                let FileName = AppConsts.docUrl + "/" + file.documentPath + file.fileName;
                window.open(FileName, "_blank");
            }
        }
        else{
            let FileName = AppConsts.docUrl + "/" + file.documentPath + file.fileName;
            window.open(FileName, "_blank");
        }
    }

    downloadSignQuotefile(file): void {

        var Qdate = moment(file.quotation.quoteDate).toDate()
        var MigrationDate = new Date('2022/09/17')

        if(this.lead.organizationId == 7) {
            MigrationDate = new Date('2023/01/04')
        }

        if(this.lead.organizationId == 8 || this.lead.organizationId == 7)
        {
            if(Qdate < MigrationDate)
            {
                let FileName = AppConsts.oldDocUrl + "/" + file.quotation.quoteFilePath + file.quotation.signedQuoteFileName;
                window.open(FileName, "_blank");
            }
        }
        else{
            let FileName = AppConsts.docUrl + "/" + file.quotation.quoteFilePath + file.quotation.signedQuoteFileName;
            window.open(FileName, "_blank");
        }
    };

    // downfile(file): void {
    //     let FileName = AppConsts.docUrl + "/" + file.filePath;
    //     window.open(FileName, "_blank");
    // };

    downloadQuotefile(file): void {

        var Qdate = moment(file.quotation.quoteDate).toDate()
        
        var MigrationDate = new Date('2022/09/17')

        if(this.lead.organizationId == 7) {
            MigrationDate = new Date('2023/01/04')
        }

        if(this.lead.organizationId == 8 || this.lead.organizationId == 7)
        {
            if(Qdate < MigrationDate)
            {
                let FileName = AppConsts.oldDocUrl + "/" + file.quotation.quoteFilePath + file.quotation.quoteFileName;
                window.open(FileName, "_blank");
            }
        }
        else {
            let FileName = AppConsts.docUrl + "/" + file.quotation.quoteFilePath + file.quotation.quoteFileName;
            window.open(FileName, "_blank");
        }
    };

    savedocument(): void {
        debugger
        if (this.documentTypeId == undefined) {
            this.notify.warn(this.l('SelectDocumentType'));
            return;
        }
        if (this.uploader.queue.length == 0) {
            this.notify.warn(this.l('SelectFileToUpload'));
            return;
        }
        this.uploader.uploadAll();
    };

    saveQuotations(): void {
        this.saving = true;
        this.Quotation.jobId = this.job.id;
        this.Quotation.quoteMode = "";
        this.spinner.show();
        this.Quotation.sectionId =this.sectionId;
        this._quotationsServiceProxy.createOrEdit(this.Quotation)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                    let log = new UserActivityLogDto();
                    log.actionId = 12;
                    log.actionNote = this.Quotation.id > 0  ? 'Quotation Updated': 'New Quotation Added';
                    log.section = this.SectionName;
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                    }); 
                this.getJobQuotations();
                this.notify.info(this.l('SavedSuccessfully'));
                this.spinner.hide();
            }, err => {
                this.spinner.hide();
            });
    };

    check(event) {
        if (event.target.value < 1) {
            event.target.value = '';
        }
    }

    sendQuotations(quotemode): void {
        this.saving = true;
        this.Quotation.jobId = this.job.id;
        this.Quotation.quoteMode = quotemode;
        this.spinner.show();
        this.Quotation.sectionId =this.sectionId;
        this._quotationsServiceProxy.sendQuotation(this.Quotation)
            .pipe(finalize(() => {
                let log = new UserActivityLogDto();
                log.actionId = 12;
                log.actionNote = quotemode == 'SMS' ? 'Customer Signature Request Sent On SMS': 'Customer Signature Request Sent On Email';
                log.section = this.SectionName;
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
                }); 
                this.saving = false;
                this.spinner.hide();
            }))
            .subscribe(() => {
                this.getJobQuotations();
                this.notify.info(this.l('SavedSuccessfully'));
            });
    }

    calculateGST(): void {
        this.Invoice.invoicePayGST = this.Invoice.invoicePayTotal * 10 / 100;
        this.Invoice.ccSurcharge = this.Invoice.invoicePayTotal * 1 / 100;
    };
   
    saveInvoice(): void {

        let totalPaid = 0;
        if (this.Invoice.invoicePayTotal <= 0) {
            this.notify.error(this.l('AmountMustBeGreaterThenZero'));
            return;
        }
        this.JobInvoices.map((item) => {
            totalPaid += item.invoicePayment.invoicePayTotal;
        })
        this.saving = true;
        if (totalPaid < this.job.totalCost) {
            this.Invoice.jobId = this.job.id;
            this.Invoice.invoicePayDate = this.receiveddate;
            this._invoicePaymentServiceProxy.createOrEdit(this.Invoice)
                .pipe(finalize(() => { this.saving = false; }))
                .subscribe(() => {
                    let log = new UserActivityLogDto();
                    log.actionId = 16;
                    log.actionNote = this.Invoice.id > 0 ? 'Invoice Modified': "Invoice Created With Amount $" + this.Invoice.invoicePayTotal;
                    log.section = this.SectionName;
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                    this.totalInvoiceAmountPaid = 0;
                    this.getJobInvoices();
                    this.Invoice.invoicePayTotal = 0;
                    this.Invoice.invoicePayGST = 0;
                    this.Invoice.ccSurcharge = 0;
                    this.notify.info(this.l('SavedSuccessfully'));
                });
        } else {
            this.notify.error(this.l('AmountIsFullyPaid'));
        }
    };

    getInstaller(): void {

        // if(this.jobInstallation.installationDate != null)
        // {
        //     this.InstallerList = [];
        //     this._jobServiceProxy.getAllInstallerAvailabilityList(this.jobInstallation.installationDate, this.job.id).subscribe(result => {
        //         this.InstallerList = result;
        //     });

        //     this.ElectrictionList = [];
        //     this._jobServiceProxy.getAllElectricianAvailabilityList(this.jobInstallation.installationDate, this.job.id).subscribe(result => {
        //         this.ElectrictionList = result;
        //     });

        //     this.DesignerList = [];
        //     this._jobServiceProxy.getAllDesignerAvailabilityList(this.jobInstallation.installationDate, this.job.id).subscribe(result => {
        //         this.DesignerList = result;
        //     });
        // }
    };

    selectDesiElec(): void {
        if (this.jobInstallation.installerId) {
            this.jobInstallation.electricianId = this.jobInstallation.installerId;
            this.jobInstallation.designerId = this.jobInstallation.installerId;
        }
        else {
            this.jobInstallation.electricianId = 0;
            this.jobInstallation.designerId = 0;
        }
    }

    saveInstallationDetails(): void {
        this.saving = true;

        let stcpostalcode = this.STCPostalCode.find(x => parseInt(x.postCodeFrom) <= parseInt(this.job.postalCode) && parseInt(x.postCodeTo) >= parseInt(this.job.postalCode));
        let year = moment(this.jobInstallation.installationDate).year();
        let stcRate = this.STCYearWiseRate.find(x => x.year == year);
        
        if (stcpostalcode) {
            if (this.STCZoneRating.length > 0) {
                let zoneRating = this.STCZoneRating.find(x => x.id === stcpostalcode.zoneId);
                
                this.job.stc = Math.floor((this.job.systemCapacity * zoneRating.rating * stcRate.rate));
                this.job.rebate = this.job.stc * 36;
            }
        }
       
        this.jobInstallation.id = this.job.id;
        this.jobInstallation.stc = this.job.stc;
        this.jobInstallation.rebate = this.job.rebate; 
        this.jobInstallation.sectionId = this.sectionId;
        this._jobServiceProxy.updateJobInstallationDetails(this.jobInstallation)
            .pipe(finalize(() => { this.saving = false; })).subscribe(() => {
                let log = new UserActivityLogDto();
                log.actionId = 13;
                log.actionNote = 'Installation Details Updated';
                log.section = this.SectionName;
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
                this.isInstallationDetail = true;
                this._jobServiceProxy.getJobStatus(this.job.id).subscribe(result => {
                    this.jobCurrentStatus = result;
                });
                this.notify.info(this.l('SavedSuccessfully'));
            });

            // for (let i = 0; i < this.JobProducts.length; i++) {
            //     this.calculateSTCRebate(i);
            // }
    };

    savePostInstallationdetails(): void {
        this.saving = true;
        this.job.id = this.job.id;
        this.job.sectionId = this.sectionId;
        if(this.job.state == 'VIC'){
            if(this.job.vicqRcodeScanDate)
            {
                if(this.job.inspectionDate){
                    if(this.job.inspectionDate < this.job.vicqRcodeScanDate){
                        this.notify.warn("Enter Inspection Date Greater Than Or Equal To VIC QRcode Scan Date");
                        this.job.inspectionDate = this.job.vicqRcodeScanDate;
                        this.saving = false;
                        return;
                    }
                }
                
            }
            else{
                this.notify.warn("Enter VIC QRcode Scan Date");
                this.saving = false;
                return;
            }
        }
        this._jobServiceProxy.update_PostInstallationDetails(this.job)
            .pipe(finalize(() => { this.saving = false; })).subscribe(() => {
                let log = new UserActivityLogDto();
                log.actionId = 14;
                log.actionNote = this.job.postInstallationStatus == 1 ? 'Job Status Change To Job Installed' : 'Job Status Change To Job Incomplete';
                log.section = this.SectionName;
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
                this._jobServiceProxy.getJobStatus(this.job.id).subscribe(result => {
                    this.jobCurrentStatus = result;
                });
                this.notify.info(this.l('SavedSuccessfully'));
            });
    };

    // removeInstallationDate(): void {
    //     this.message.confirm(
    //         this.l('AreYouSureWanttoRemoveInstallationDate'),
    //         this.l('RemoveInstallationDate'),
    //         (isConfirmed) => {
    //             if (isConfirmed) {
    //                 this.saving = true;
    //                 this.sampleDate = null;
    //                 this.jobInstallation.installationDate = null;
    //                 this.jobInstallation.installationTime = null;
    //                 this.jobInstallation.id = this.job.id;
    //                 this._jobServiceProxy.updateJobInstallationDetails(this.jobInstallation)
    //                     .pipe(finalize(() => { this.saving = false; }))
    //                     .subscribe(() => {
    //                         this.isInstallationDetail = true;
    //                         this._jobServiceProxy.getJobStatus(this.job.id).subscribe(result => {
    //                             this.jobCurrentStatus = result;
    //                         });
    //                         this.notify.info(this.l('SavedSuccessfully'));
    //                     });
    //             }
    //         }
    //     );
    // };

    removeInstallationDetail(): void {
        this.message.confirm(
            this.l('AreYouSureWanttoReschedualInstallation'),
            this.l('ReschedualInstallation'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this.saving = true;
                    this.sampleDate = null;
                    this.jobInstallation.installationDate = null;
                    this.jobInstallation.installationTime = null;
                    this.jobInstallation.installerId = 0;
                    this.jobInstallation.electricianId = 0;
                    this.jobInstallation.designerId = 0;
                    this.jobInstallation.warehouseLocation = 0;
                    this.jobInstallation.installationNotes = null;
                    this.jobInstallation.id = this.job.id;
                    this.jobInstallation.stc = this.job.stc;
                    this.jobInstallation.rebate = this.job.rebate;
                    this._jobServiceProxy.updateJobInstallationDetails(this.jobInstallation)
                        .pipe(finalize(() => { this.saving = false; }))
                        .subscribe(() => {
                            let log = new UserActivityLogDto();
                            log.actionId = 13;
                            log.actionNote = 'Installation Details Removed';
                            log.section = this.SectionName;
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            }); 
                            this.isInstallationDetail = false;
                            this._jobServiceProxy.getJobStatus(this.job.id).subscribe(result => {
                                this.jobCurrentStatus = result;
                            });
                            this.notify.info(this.l('SavedSuccessfully'));
                        });
                }
            }
        );
    };

    
    saveJobRefund(): void {
        this.saving = true;
        this.jobRefund.jobId = this.job.id;
        this.jobRefund.sectionId = this.sectionId;
        this.spinner.show();
        this._jobRefundsServiceProxy.createOrEdit(this.jobRefund)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                let log = new UserActivityLogDto();
                log.actionId = this.jobRefund.id > 0 || this.jobRefund.refundReasonId == 1 ? 15 : 14;
                log.actionNote = this.jobRefund.id > 0 || this.jobRefund.refundReasonId == 1 ? 'Job Refund Request Create' : 'Job Refund Request Completed';
                log.section = this.SectionName;


                
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
                this.getJobRefund();
                this.notify.info(this.l('SavedSuccessfully'));
                this.spinner.hide();
                this.jobRefund.amount = 0;
                this.jobRefund.refundPaymentType = "";
                this.jobRefund.refundReasonId = 0;
                this.jobRefund.bankName = "";
                this.jobRefund.accountName = "";
                this.jobRefund.bsbNo = "";
                this.jobRefund.accountNo = "";
                this.jobRefund.notes = "";
            });
    };

    req : boolean;
    checkReq(event) : void {
        if(event.target.value == "CC") {
            this.req = false;
        }
        else {
            this.req = true;
        }
    }

    saveJobInstallerInvoic(): void {
        this.fileupload.uploadAll();
        this.saving = true;
        
        this.installerInv.jobId = this.job.id;
        this.spinner.show();
        this.installerInv.sectionId = this.sectionId;
        this._jobInstallerInvoiceServiceProxy.createOrEdit(this.installerInv)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                let log = new UserActivityLogDto();
                log.actionId = 26;
                log.actionNote = this.installerInv.id > 0 ? 'Installer Invoice Modified' : 'Installer Invoices Created';
                log.section = this.SectionName;
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
                this.notify.info(this.l('SavedSuccessfully'));
                this.spinner.hide();
                this.installerInv.invTypeId = 0;
                this.installerInv.invNo = 0;
                this.installerInv.amount = 0;
                this.installerInv.invDate = null;
                this.installerInv.advanceAmount = 0;
                this.installerInv.lessDeductAmount = 0;
                this.installerInv.totalAmount = 0;
                this.installerInv.advancePayDate = null;
                this.installerInv.payDate = null;
                this.installerInv.paymentsTypeId = 0;
                this.installerInv.installerId = 0;
                this.installerInv.notes = "";
            });
    };

    RefStr: any;
    refundPDF(Id): void {
        
        this.spinnerService.show();
        this._reportServiceProxy.getRefundByJobId(Id).subscribe(result => {
    
            if(result.viewHtml != '')
            {
                this.RefStr = this.GetRefundReferralForm(result);
                this.RefStr = this.sanitizer.bypassSecurityTrustHtml(this.RefStr);
    
                let html = this.RefStr.changingThisBreaksApplicationSecurity;
                this._commonLookupService.downloadPdf(this.job.jobNumber, "RefundForm", html).subscribe(result => {
                    this.spinnerService.hide();
                    window.open(result, "_blank");
                },
                err => {
                    this.spinnerService.hide();
                });
            }
        },
        err => {
            this.spinnerService.hide();
        });
    }

    htmlStr: any;
    taxInvoice(): void {

            this.spinnerService.show();
            this._reportServiceProxy.taxInvoiceByJobId(this.job.id).subscribe(result => {
                this.taxInvoiceData = result;
    
                if(this.taxInvoiceData.viewHtml != '')
                {
                    this.htmlStr = this.GetTaxInvoice(this.taxInvoiceData.viewHtml);
                    this.htmlStr = this.sanitizer.bypassSecurityTrustHtml(this.htmlStr);
    
                    let html = this.htmlStr.changingThisBreaksApplicationSecurity;
                    this._commonLookupService.downloadPdf(this.taxInvoiceData.jobNumber, "TaxInvoice", html).subscribe(result => {
                        let FileName = result;
                        this.spinnerService.hide();
                        window.open(result, "_blank");
                    },
                    err => {
                        this.spinnerService.hide();
                    });
                }
            },
            err => {
                this.spinnerService.hide();
            });
    }

    paymentReceipt(): void {

        this.spinnerService.show();
        this._reportServiceProxy.paymentReceiptByJobId(this.job.id).subscribe(result => {
            this.paymentReceiptData = result;

            if(this.paymentReceiptData.viewHtml != '')
            {
                let html = this.GetPaymentReceipt(this.paymentReceiptData.viewHtml);

                this._commonLookupService.downloadPdf(this.paymentReceiptData.jobNumber, "PaymetReceipt", html).subscribe(result => {
                    let FileName = result;
                    this.spinnerService.hide();
                    window.open(result, "_blank");
                },
                err => {
                    this.spinnerService.hide();
                });
            }
        },
        err => {
            this.spinnerService.hide();
        });
    }

    onexportdetail(val): void
    {
        if(val == 2)
        {
            this.retailerdetail = true;
        } else {
            this.retailerdetail = false;
        }
    }

    saveAcknowledgement(type): void {
        this.saving = true;
        var exportcont = true;
        var feedintariff = true;
        if(type == "Export Control"){
            if(this.job.exportkw == null)
            {
                exportcont = false;
            }
        } 
        else {
            if(this.job.approxFeedTariff == null)
            {
                feedintariff = false;
            }
        }

        if(exportcont == false)
        {
            this.notify.warn("Could not find export kw.");
            this.spinner.hide();
        } else if(feedintariff == false){
            this.notify.warn("Could not find approx feed in tariff.");
            this.spinner.hide();
        }
        else{
            this.jobAcknowledgement.jobId = this.job.id;
            this.jobAcknowledgement.docType = type;
            this.jobAcknowledgement.name = this.lead.companyName;
            this.jobAcknowledgement.address = this.lead.address;
            this.jobAcknowledgement.mobile = this.lead.mobile;
            this.jobAcknowledgement.email = this.lead.email;
            if(this.job.exportkw == null) {
                this.jobAcknowledgement.kw = '0';
            }
            else {
                this.jobAcknowledgement.kw = this.job.exportkw.toString();
            }
            
            this.spinner.show();
            this.jobAcknowledgement.sectionId = this.sectionId;
            this._jobAcknowledgementServiceProxy.createOrEdit(this.jobAcknowledgement)
                .pipe(finalize(() => { this.saving = false; }))
                .subscribe(() => {
                    let log = new UserActivityLogDto();
                    log.actionId = 12;
                    log.actionNote = "New " + type + " Form Cretaed";
                    log.section = this.SectionName;
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                    if(type == "Export Control"){
                        this.getExportControl();
                    } 
                    else {
                        this.getFeedInTarrif();
                    }
                    this.notify.info(this.l('SavedSuccessfully'));
                    this.spinner.hide();
                });
        }
        
    };

    saveDeclarationForm(type): void {
        this.saving = true;

        
            this.declarationForm.jobId = this.job.id;
            this.declarationForm.docType = type;
            this.declarationForm.sectionId =this.sectionId;
            this.spinner.show();
            this._declarationFormServiceProxy.createOrEdit(this.declarationForm)
                .pipe(finalize(() => { this.saving = false; }))
                .subscribe(() => {
                    let log = new UserActivityLogDto();
                    log.actionId = 12;
                    log.actionNote = "New " + type + " Form Cretaed";
                    log.section = this.SectionName;
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                    if(type == "Shading Declaration"){
                        this.getShadingDeclaration();
                    } 
                    else {
                        this.getEfficiencyDeclaration();
                    }
                    this.notify.info(this.l('SavedSuccessfully'));
                    this.spinner.hide();
                });
        
        
    };
   
    getExportControl(): void {
        this._jobAcknowledgementServiceProxy.getAll(this.job.id, "Export Control", '', 0, 9999).subscribe(result => {
            this.exportControl = result.items;
        });
    }

    getFeedInTarrif(): void {
        this._jobAcknowledgementServiceProxy.getAll(this.job.id, "Feed In Tariff", '', 0, 9999).subscribe(result => {
            this.feedInTariif = result.items;
        });
    }
    getShadingDeclaration(): void {
        this._declarationFormServiceProxy.getAll(this.job.id, "Shading Declaration", '', 0, 9999).subscribe(result => {
            this.shadingDeclaration = result.items;
        });
    }

    getEfficiencyDeclaration(): void {
        this._declarationFormServiceProxy.getAll(this.job.id, "Efficiency Declaration", '', 0, 9999).subscribe(result => {
            this.efficiencyDeclaration = result.items;
        });
    }

    sendAcknowledgement(sendMode, docType): void {
        this.saving = true;
        this.SendAcknowledgement.jobId = this.job.id;
        this.SendAcknowledgement.sendMode = sendMode;
        this.SendAcknowledgement.docType = docType;
        this.SendAcknowledgement.sectionId = this.sectionId;
        this.spinner.show();
        this._jobAcknowledgementServiceProxy.sendAcknowledgement(this.SendAcknowledgement)
            .pipe(finalize(() => {                
                this.saving = false;
                this.spinner.hide();
            }))
            .subscribe(() => {
                let log = new UserActivityLogDto();
                log.actionId = 12;
                log.actionNote = docType + ' Form Signature Request Sent On ' + sendMode;
                log.section = this.SectionName;
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
                if(docType == "Export Control"){
                    this.getExportControl();
                } 
                else {
                    this.getFeedInTarrif();
                }
                this.notify.info(this.l('SavedSuccessfully'));
                
            });
    }

   
    sendDeclaration(sendMode, docType): void {
        this.saving = true;
        this.SendDeclarationForm.jobId = this.job.id;
        this.SendDeclarationForm.sendMode = sendMode;
        this.SendDeclarationForm.docType = docType;
        this.spinner.show();
        this.SendDeclarationForm.sectionId = this.sectionId;
        this._declarationFormServiceProxy.sendDeclarationForm(this.SendDeclarationForm)
            .pipe(finalize(() => {
                this.saving = false;
                this.spinner.hide();
            }))
            .subscribe(() => {
                let log = new UserActivityLogDto();
                log.actionId = 12;
                log.actionNote = docType + ' Form Signature Request Sent On ' + sendMode;
                log.section = this.SectionName;
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
                if(docType == "Shading Declaration"){
                    this.getShadingDeclaration();
                } 
                else {
                    this.getEfficiencyDeclaration();
                }
                this.notify.info(this.l('SavedSuccessfully'));
                
            });
    }

    picklistData: GetPicklistDataDto; 
    downloadPicklist(JobId, PicklistId) {
        this.spinnerService.show();
        this._reportServiceProxy.picklistByPickListId(JobId, PicklistId).subscribe(result => {

            this.picklistData = result;

            if(this.picklistData.viewHtml != '')
            {
                let html = this.getPicklist(this.picklistData.viewHtml);

                this._commonLookupService.downloadPdf(this.picklistData.jobNumber, "Picklist", html).subscribe(result => {
                    let FileName = result;
                    this.spinnerService.hide();
                    window.open(result, "_blank");
                },
                err => {
                    this.spinnerService.hide();
                });
            }
        },
        err => {
            this.spinnerService.hide();
        });
    }
    
    getPicklist(html) {
        let htmlstring = html;

        let myTemplateStr = htmlstring;
        let TableList = [];

        if (this.picklistData.picklistItemDetails != null) {
            this.picklistData.picklistItemDetails.forEach(obj => {
                // TableList.push("<div class='mb5'>" + obj.name + "</div>");
                TableList.push("<tr><td>" + obj.category + "</td><td>" + obj.item + "</td><td>" + obj.model + "</td><td>" + obj.qty + "</td><td></td></tr>");
            })
        }

        let myVariables = {
            PL: {
                JobNumber: this.picklistData.jobNumber,
                Date: this.picklistData.date,
                PicklistItemDetails: TableList.toString().replace(/,/g, ''),
                
                Cust: {
                    Name: this.picklistData.name,
                    Mobile: this.picklistData.mobile,
                    Email: this.picklistData.email,
                    AddressLine1: this.picklistData.addressLine1,
                    AddressLine2: this.picklistData.addressLine2,
                    MeterPhase: this.picklistData.meterPhase,
                    MeterUpgrad: this.picklistData.meterUpgrad,
                    RoofType: this.picklistData.roofType,
                    PropertyType: this.picklistData.propertyType,
                    RoofPitch: this.picklistData.roofPitch,
                    ElecDist: this.picklistData.elecDist,
                    ElecRetailer: this.picklistData.elecRetailer,
                    NIMINumber: this.picklistData.nmiNumber,
                    MeterNo: this.picklistData.meterNo,
                    ManualQuoteNo: this.picklistData.manualQuoteNo,
                    StoreLoc: this.picklistData.storeLoc
                },

                ID: {
                    InstallerName: this.picklistData.installerName,
                    InstallerMobile: this.picklistData.installerMobile,
                    InstallationDate: this.picklistData.installationDate,
                    SA: {
                        AddressLine1: this.picklistData.siteAddressLine1,
                        AddressLine2: this.picklistData.siteAddressLine2
                    }
                },

                SN: {
                    Notes: this.picklistData.notes,
                    InstallerNotes: this.picklistData.installerNotes
                },

                CustomerSign: {
                    PickedBy: "",
                    PickedDate: "",
                    SignSrc: ""
                }
            }
        }

        // use custom delimiter {{ }}
        _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;
    
        // interpolate
        let compiled = _.template(myTemplateStr);
        let myTemplateCompiled = compiled(myVariables);
        return myTemplateCompiled;
    }

    checkStockOnHandWithQty(index, qty, stockOnHand) {

        if(qty == '' || qty == 0)
        {
            this.PicklistProducts[index].quantity = '';
            return;
        }

        if(!this.checkStockOnHandFeatures())
        {
            return;
        }
        
        if(this.permission.isGranted('Pages.JobPickList.NagativeStock'))
        {
            return;
        }

        if(stockOnHand < qty) {
            this.PicklistProducts[index].quantity = '';
            this.notify.warn("Stock quantity grater then live stock.", "Stock Warning!");
        }
    }

    getStockOnHandForAllItem() {

        if(!this.checkStockOnHandFeatures())
        {
            return;
        }

        for (let i = 0; i < this.PicklistProducts.length; i++) {
            this._quickstockServiceProxy.getStockDetails(this.PicklistProducts[i].productItemId, this.picklistwarehouseLocation).subscribe(result => {
                this.PicklistProducts[i].stock = result.stockOnHand;
                this.checkStockOnHandWithQty(i, this.PicklistProducts[i].quantity, this.PicklistProducts[i].stock)
            });
        }
    }

    checkStockOnHandFeatures() {
        return this.feature.isEnabled("App.StockManagement.Picklist.CheckStockOnHand");
    }

    checkNetSituationFeatures() {
        return this.feature.isEnabled("App.StockManagement.ProductItems.NetSituation");
    }

    pr : any;

    changePackage(event) {
       
        var packageId = event;

        this.spinnerService.show();
        if(packageId == 0)
        {
            this._jobProductItemsServiceProxy.getJobProductItemByJobId(this.job.id).subscribe(result => {
                this.JobProducts = [];
                
                result.map((item) => {
                    var jobproductcreateOrEdit = { id: 0, productItemId: 0, productItemName: "", quantity: 0, jobId: 0, size: 0, model: "", productItems: [], productTypeId: 0, stock: "", datasheetUrl: null, refNo : "" }
                    jobproductcreateOrEdit.productItemId = item.jobProductItem.productItemId;
                    jobproductcreateOrEdit.productItemName = item.productItemName;
                    jobproductcreateOrEdit.productTypeId = item.jobProductItem.productTypeId;
                    jobproductcreateOrEdit.quantity = item.jobProductItem.quantity;
                    jobproductcreateOrEdit.id = item.jobProductItem.id;
                    jobproductcreateOrEdit.jobId = this.jobid;
                    jobproductcreateOrEdit.size = item.size;
                    jobproductcreateOrEdit.model = item.model;
                    jobproductcreateOrEdit.datasheetUrl = item.datasheetUrl;
                    jobproductcreateOrEdit.refNo = item.jobProductItem.refNo;
                
                    //let pitemname = this.EditproductItems.find(x => x.id == item.jobProductItem.productItemId);
                    //jobproductcreateOrEdit.productItemName = pitemname.displayName;

                    this.JobProducts.push(jobproductcreateOrEdit);
                });

                if (result.length == 0) {
                    var jobproductcreateOrEdit = { id: 0, productItemId: 0, productItemName: "", quantity: 0, jobId: 0, size: 0, model: "", productItems: [], productTypeId: 0 , stock: "", datasheetUrl: null, refNo : "" }
                    //jobproductcreateOrEdit.productItems = this.EditproductItems;
                    this.JobProducts.push(jobproductcreateOrEdit);
                }

                this.job.basicCost = this.basicCost;

                let that = this;
                setTimeout(function () {
                    that.calculateRates();
                }, 5000);

                this.getStockOnHandForAllView();

                this.saving = false;
                this.spinnerService.hide();
            });
        }
        else {
            this._productPackageServiceProxy.getProductPackageItemByPackageId(packageId).subscribe(result => {
                
                //this.JobProducts = []; 

                if (this.JobProducts[0].id == 0 && this.JobProducts[0].productItemId == 0 && this.JobProducts[0].productItemName == "" && this.JobProducts[0].quantity == 0 && this.JobProducts[0].productTypeId == 0) {
                    this.JobProducts = [];
                }

                result.map((item) => {
                    var jobproductcreateOrEdit = { id: 0, productItemId: 0, productItemName: "", quantity: 0, jobId: 0, size: 0, model: "", productItems: [], productTypeId: 0, datasheetUrl: null, refNo : "" }

                    jobproductcreateOrEdit.productItemId = item.productItemId;
                    jobproductcreateOrEdit.productItemName = item.productItem;
                    jobproductcreateOrEdit.productTypeId = item.productTypeId;
                    jobproductcreateOrEdit.quantity = item.quantity;
                    jobproductcreateOrEdit.jobId = this.jobid;
                    jobproductcreateOrEdit.size = item.size;
                    jobproductcreateOrEdit.model = item.model;
                    jobproductcreateOrEdit.datasheetUrl = item.datasheetUrl;
                    jobproductcreateOrEdit.refNo = item.refNo;

                    //let pitemname = this.EditproductItems.find(x => x.id == item.productItemId);
                    //jobproductcreateOrEdit.productItemName = pitemname.displayName;

                    this.JobProducts.push(jobproductcreateOrEdit);
                });

                // this._productPackageServiceProxy.getProductPackageForEdit(packageId).subscribe(result => {
                //     this.job.basicCost = result.productPackage.price;
                // });

                this.getStockOnHandForAllView();

                let that = this;
                setTimeout(function () {
                    that.calculateRates();
                }, 5000);
                this.saving = false;
                this.spinnerService.hide();
            });
        }
    }

    getStockOnHandForAllView() {
        if(!this.checkNetSituationFeatures())
        {
            return;
        }
        
        for (let i = 0; i < this.JobProducts.length; i++) {
            this._quickstockServiceProxy.getStockDetailByState(this.JobProducts[i].productItemId, this.job.state).subscribe(result => {
                this.JobProducts[i].stock = result.stockOnHand;
            });
        }

        // let that = this;
        // setTimeout(function () {
        //     that.invoiceBtnCheck();
        // }, 1000);
        
    }

    deleteInvoice(Id): void {
        this.message.confirm(
            this.l('AreYouSureWanttoDelete'),
            this.l('Delete'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._invoicePaymentServiceProxy.delete(Id,this.sectionId)
                        .subscribe(() => {
                            let log = new UserActivityLogDto();
                            log.actionId = 16;
                            log.actionNote = 'Invoice Deleted';
                            log.section = this.SectionName;
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            }); 
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            this.getJobInvoices();
                        }
                    );
                }
            }
        );
    }

    acknowledgement(id, date): void {
        
        this.spinnerService.show();
        this._reportServiceProxy.getAcknowledgementById(id).subscribe(result => {
            let htmlTemplate = this.getAcknoHtml(result, date)

            this._commonLookupService.downloadPdf(result.jobNumber, result.docType, htmlTemplate).subscribe(re => {
                window.open(re, "_blank");
                this.spinnerService.hide();
            }, 
            e => 
            { 
                this.spinnerService.hide(); 
            });
        }, err => {this.spinnerService.hide();});
    }

    declaration(id, date): void {
        
        this.spinnerService.show();
        this._reportServiceProxy.getDeclarationById(id).subscribe(result => {
            let htmlTemplate = this.getDeclarationHtml(result, date)

            this._commonLookupService.downloadPdf(result.jobNumber, result.docType, htmlTemplate).subscribe(re => {
                window.open(re, "_blank");
                this.spinnerService.hide();
            }, 
            e => 
            { 
                this.spinnerService.hide(); 
            });
        }, err => {this.spinnerService.hide();});
    }

    getAcknoHtml(result, date) {
        let htmlstring = result.viewHtml;
        let myTemplateStr = htmlstring;

        var QDate = moment(date).toDate()
        var MigrationDate = new Date('2023/01/04')

        debugger
        let appUrl = AppConsts.docUrl;
        if(this.lead.organizationId == 7)
        {
            if(QDate < MigrationDate)
            {
                appUrl = AppConsts.oldDocUrl;
            }
        }

        let myVariables = {
            A: {
                Name: result.name,
                AddressLine1: result.address1,
                AddressLine2: result.address2,
                Mobile: result.mobile,
                Email: result.email,
                JobNumber: result.jobNumber,
                Kw: result.kw,
                Y: AppConsts.appBaseUrl + (result.yn == true ? '/assets/common/images/tick.png' : '/assets/common/images/untick.png' ),
                N: AppConsts.appBaseUrl + (result.yn == false ? '/assets/common/images/tick.png' : '/assets/common/images/untick.png'),

                CustomerSign: {
                    SignSrc: result.signSrc == null ? "" : appUrl + result.signSrc,
                    Date: result.signDate
                }
            },
      
            O: {
              OrgLogo: AppConsts.docUrl + result.orgLogo
            }
        };

        // use custom delimiter {{ }}
        _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;

        // interpolate
        let compiled = _.template(myTemplateStr);
        let myTemplateCompiled = compiled(myVariables);
        return myTemplateCompiled;
    }

    getDeclarationHtml(result, date) {
        let htmlstring = result.viewHtml;
        let myTemplateStr = htmlstring;

        var QDate = moment(date).toDate()
        var MigrationDate = new Date('2023/01/04')

        let myVariables = {
            A: {
                Name: result.name,
                AddressLine1: result.address1,
                AddressLine2: result.address2,
                Mobile: result.mobile,
                Email: result.email,
                JobNumber: result.jobNumber,
                State: result.state,
                CustomerSign: {
                    SignSrc: result.signSrc == null ? "" : AppConsts.docUrl + result.signSrc,
                    Date: result.signDate
                }
            },
      
            O: {
              OrgLogo: AppConsts.docUrl + result.orgLogo
            }
        };

        // use custom delimiter {{ }}
        _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;

        // interpolate
        let compiled = _.template(myTemplateStr);
        let myTemplateCompiled = compiled(myVariables);
        return myTemplateCompiled;
    }

    GetTaxInvoice(html) {
        let htmlstring = html;

        let myTemplateStr = htmlstring;
        let TableList = [];

        if (this.taxInvoiceData.qunityAndModelLists != null) {
            this.taxInvoiceData.qunityAndModelLists.forEach(obj => {
                TableList.push("<div class='mb5'>" + obj.name + "</div>");
            })
        }

        var netCost = this.taxInvoiceData.netCost != "" ? parseFloat(this.taxInvoiceData.netCost) : 0;
        var gst = (netCost * 10) / 100;

        let myVariables = {
            TI: {
                JobNumber: this.taxInvoiceData.jobNumber,
                Date: this.taxInvoiceData.date,
                BalanceDue: this.taxInvoiceData.invoicePaymentBalanceDue,

                Cust: {
                    Name: this.taxInvoiceData.name,
                    Mobile: this.taxInvoiceData.mobile,
                    Email: this.taxInvoiceData.email,
                    AddressLine1: this.taxInvoiceData.addressLine1,
                    AddressLine2: this.taxInvoiceData.addressLine2,
                    MeterPhase: this.taxInvoiceData.meterPhase,
                    MeterUpgrad: this.taxInvoiceData.meterUpgrad,
                    RoofType: this.taxInvoiceData.roofType,
                    PropertyType: this.taxInvoiceData.propertyType,
                    RoofPitch: this.taxInvoiceData.roofPitch,
                    ElecDist: this.taxInvoiceData.elecDist,
                    ElecRetailer: this.taxInvoiceData.elecRetailer,
                    NIMINumber: this.taxInvoiceData.nmiNumber,
                    MeterNo: this.taxInvoiceData.meterNo
                },

                PD: {

                    SystemDetails: TableList.toString().replace(/,/g, ''),
                    TCost: this.taxInvoiceData.totalCost != "" && this.taxInvoiceData.totalCost != "0" ? this.taxInvoiceData.totalCost : "0.00",
                    TotalCost: this.taxInvoiceData.tCost != "" && this.taxInvoiceData.tCost != "0" ? this.taxInvoiceData.tCost : "0.00",
                    Stc: this.taxInvoiceData.stc != "" && this.taxInvoiceData.stc != "0" ? this.taxInvoiceData.stc : "0.00",
                    SolarRebate: this.taxInvoiceData.solarRebate != "" && this.taxInvoiceData.solarRebate != "0" ? this.taxInvoiceData.solarRebate : "0.00",
                    SolarLoan: this.taxInvoiceData.solarLoan != "" && this.taxInvoiceData.solarLoan != "0" ? this.taxInvoiceData.solarLoan : "0.00",
                    NetCost: this.taxInvoiceData.netCost != "" && this.taxInvoiceData.netCost != "0" ? this.taxInvoiceData.netCost : "0.00",
                    Deposit: this.taxInvoiceData.invoicePaymentPaid != "" && this.taxInvoiceData.invoicePaymentPaid != "0" ? this.taxInvoiceData.invoicePaymentPaid : "0.00",
                    ACharge: this.taxInvoiceData.aCharge != "" && this.taxInvoiceData.aCharge != "0" ? this.taxInvoiceData.aCharge : "0.00",
                    Discount: this.taxInvoiceData.discount != "" && this.taxInvoiceData.discount != "0" ? this.taxInvoiceData.discount : "0.00",
                    Gst: gst
                }
            }
        }

        // use custom delimiter {{ }}
        _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;
    
        // interpolate
        let compiled = _.template(myTemplateStr);
        let myTemplateCompiled = compiled(myVariables);
        return myTemplateCompiled;
    }

    GetPaymentReceipt(html) {
        let htmlstring = html;

        let myTemplateStr = htmlstring;
        let TableList = [];

        if (this.paymentReceiptData.invoicePaymentPaymentDetails != null) {
            this.paymentReceiptData.invoicePaymentPaymentDetails.forEach(obj => {
                TableList.push("<tr><td>" + obj.date + "</td><td>$" + obj.amount + "</td><td>$" + obj.ssCharge + "</td><td>" + obj.method + "</td></tr>");
            })
        }

        let myVariables = {
            PR: {
                JobNumber: this.paymentReceiptData.jobNumber,
                Date: this.paymentReceiptData.date,
                
                Cust: {
                    Name: this.paymentReceiptData.name,
                    Mobile: this.paymentReceiptData.mobile,
                    Email: this.paymentReceiptData.email,
                    AddressLine1: this.paymentReceiptData.addressLine1,
                    AddressLine2: this.paymentReceiptData.addressLine2
                },

                ID: {
                    TCost: this.paymentReceiptData.totalCost != "" && this.paymentReceiptData.totalCost != "0" ? this.paymentReceiptData.totalCost : "0.00",
                    LessStcRebate: this.paymentReceiptData.lessStcRebate != "" && this.paymentReceiptData.lessStcRebate != "0" ? this.paymentReceiptData.lessStcRebate : "0.00",
                    SolarVICRebate: this.paymentReceiptData.solarVICRebate != "" && this.paymentReceiptData.solarVICRebate != "0" ? this.paymentReceiptData.solarVICRebate : "0.00",
                    SolarVICLoan: this.paymentReceiptData.solarVICLoan != "" && this.paymentReceiptData.solarVICLoan != "0" ? this.paymentReceiptData.solarVICLoan : "0.00",
                    ACharge: this.paymentReceiptData.aCharge != "" && this.paymentReceiptData.aCharge != "0" ? this.paymentReceiptData.aCharge : "0.00",
                    Discount: this.paymentReceiptData.discount != "" && this.paymentReceiptData.discount != "0" ? this.paymentReceiptData.discount : "0.00",
                    NetCost: this.paymentReceiptData.netCost != "" && this.paymentReceiptData.netCost != "0" ? this.paymentReceiptData.netCost : "0.00",
                    BalanceDue: this.paymentReceiptData.invoicePaymentBalanceDue != "" && this.paymentReceiptData.invoicePaymentBalanceDue != "0" ? this.paymentReceiptData.invoicePaymentBalanceDue : "0.00"
                },

                PD: {
                    PaymentDetails: TableList.toString().replace(/,/g, '')
                    
                },

                SA: {
                    AddressLine1: this.paymentReceiptData.siteAddressLine1,
                    AddressLine2: this.paymentReceiptData.siteAddressLine2
                }
            },
        }

        // use custom delimiter {{ }}
        _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;
    
        // interpolate
        let compiled = _.template(myTemplateStr);
        let myTemplateCompiled = compiled(myVariables);
        return myTemplateCompiled;
    }

    GetRefundReferralForm(result) {
        let htmlstring = result.viewHtml;

        let myTemplateStr = htmlstring;

        console.log(result);
        let myVariables = {
            R: {
                JobNumber: result.jobNumber,
                JobStatus: result.jobStatus,
                Date: result.date,
                
                Name: result.name,
                Mobile: result.mobile,
                Email: result.email,
                AddressLine1: result.addressLine1,
                AddressLine2: result.addressLine2,
                MeterPhase: result.meterPhase,
                MeterUpgrad: result.meterUpgrad,
                RoofType: result.roofType,
                NoofStorey: result.propertyType,
                RoofPitch: result.roofPitch,
                EnergyDist: result.elecDist,
                EnergyRetailer: result.elecRetailer,
                SalesRepName: result.salesRepName,
                DepositDate: result.depositDate,
                EntryDate: result.date,
                Manager: result.manager,
                NoOfPanels: result.noOfPanels,
                RefundReason: result.refundReason,
                CusBankName: result.cusBankName,
                CusBsb: result.cusBSB,
                CusAccNumber: result.cusAccNumber,
                CusAccName: result.cusAccName,
                RefundAmount: result.refundAmount,
                RefundDate: result.refundDate,
            }
        }

        // use custom delimiter {{ }}
        _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;
    
        // interpolate
        let compiled = _.template(myTemplateStr);
        let myTemplateCompiled = compiled(myVariables);
        return myTemplateCompiled;
    }

    deleteRefund(Id): void {
        this.message.confirm(
            this.l('AreYouSureWanttoDelete'),
            this.l('Delete'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._jobRefundsServiceProxy.delete(Id)
                        .subscribe(() => {
                            let log = new UserActivityLogDto();
                            log.actionId = 15;
                            log.actionNote = 'Job Refund Request Deleted';
                            log.section = this.SectionName;
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            }); 
                            this.getJobRefund();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    calItems: CalculateJobProductItemDeatilDto[];
    viewCal() : void {
        this.calItems = [];
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote = 'Open Calculated Job Product Costing';
        log.section = this.SectionName;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {
        }); 
        this.JobProducts.map((item) => {
            var items = new CalculateJobProductItemDeatilDto();
            items.productItemId = item.productItemId;
            items.model = item.model;
            items.quantity = item.quantity == null || item.quantity == undefined ? 0 : item.quantity;
            this.calItems.push(items);
        });

    }

    installerSearchResult: any [];
    filterInstaller(event): void {
        this.installerSearchResult = Object.assign([], this.picklistinstallationList).filter(
            item => item.displayName.toLowerCase().indexOf(event.query.toLowerCase()) > -1
        )
    }
    
    insName: string = "";
    selectInstaller(event): void {
        
        if(this.permission.isGranted("Pages.Jobs.CheckActualAudit"))        
        {
            this.picklistinstallerId = event.id;
            this.insName = event.displayName;
        }
        else
        {
            this.showMainSpinner();
            this._quickstockServiceProxy.getCheckActualAudit(event.id).subscribe(result => {
                if(result)
                {
                    this.picklistinstallerId = event.id;
                    this.insName = event.displayName;
                }
                else{
                    this.picklistinstallerId = 0;
                    this.insName = "";
                    this.notify.warn("Audit Greter then 300..", "Stock Alert");
                }
                this.hideMainSpinner();
            }, err => { this.hideMainSpinner(); });
        }
        
    }

    selectInsInstaller(event): void {
        this.jobInstallation.installerId = event.id;
        this.jobInstallation.designerId = event.id;
        this.jobInstallation.electricianId = event.id;

        this.jobInstallation.installerName = event.displayName;
        this.jobInstallation.designerName = event.displayName;
        this.jobInstallation.electricianName = event.displayName;
    }

    selectInsDesignerId(event): void {
        this.jobInstallation.designerId = event.id;
        this.jobInstallation.designerName = event.displayName;
    }

    selectInsElectricianId(event): void {
        this.jobInstallation.electricianId = event.id;
        this.jobInstallation.electricianName = event.displayName;
    }

    // Called When Product Type Change
    changeProductType(i) : void {
        this.JobProducts[i].productItemId = null;
        this.JobProducts[i].productItemName = '';
        this.JobProducts[i].model = '';
        this.JobProducts[i].stock = 0;
        this.JobProducts[i].datasheetUrl = null;
        this.JobProducts[i].refno = "";

    }

    // Called When Product Item Entered.
    filterProductIteams(event, i): void {
        let Id = this.JobProducts[i].productTypeId;

        this._jobServiceProxy.getProductItemList(Id, event.query, this.job.state, true).subscribe(output => {
            this.productItemSuggestions = output;
        });
    }

    // Called When Product Item Selected.
    selectProductItem(event, i) {
        
        if (this.checkProductWarrantyFeaturesAndPermission()) {
            if (this.JobProducts[i].productTypeId == 1) {
                if (event.performanceWarranty == null || event.performanceWarranty == undefined) {
                    this.JobProducts[i].productItemName = "";
                    this.notify.warn("Please Check Performance Warranty");
                    return;
                } else if (event.productWarranty == null || event.productWarranty == undefined) {
                    this.JobProducts[i].productItemName = "";
                    this.notify.warn("Please Check Product Warranty");
                    return;
                }
            } else if (this.JobProducts[i].productTypeId == 2) {
                if (event.extendedWarranty == null || event.extendedWarranty == undefined) {
                    this.JobProducts[i].productItemName = "";
                    this.notify.warn("Please Check Extended Warranty");
                    return;
                }
            }
        }

        this.JobProducts[i].productItemId = event.id;
        this.JobProducts[i].productItemName = event.productItem;
        this.JobProducts[i].model = event.productModel;
        this.JobProducts[i].size = event.size;
        this.JobProducts[i].datasheetUrl = event.datasheetUrl;
        if (this.JobProducts[i].quantity == undefined || this.JobProducts[i].quantity == null) {
            this.JobProducts[i].quantity = 0;
        }
        else {
            this.JobProducts[i].quantity = this.JobProducts[i].quantity;
        }

        this.calculateSTCRebate(i);

        if(this.checkNetSituationFeatures())
        {
            this._quickstockServiceProxy.getStockDetailByState(this.JobProducts[i].productItemId, this.job.state).subscribe(result => {
                this.JobProducts[i].stock = result.stockOnHand;

                if(result.stockOnHand < 0)
                {
                    this.JobProducts[i].productItemId = 0;
                    this.JobProducts[i].productItemName = "";
                    this.JobProducts[i].model = "";
                    this.JobProducts[i].size = 0;
                    this.JobProducts[i].datasheetUrl = null;

                    this.notify.warn("Product Out Of Stock.");
                }

                this.invoiceBtnCheck();
            });
        }
        

        // this.getActualCost();
    }

    // filterProductIteamsPickList(event, i): void {
    //     let Id = 0;
    //     for (let j = 0; j < this.PicklistProducts.length; j++) {
    //         if (j == i) {
    //             Id = this.PicklistProducts[i].productTypeId
    //         }
    //     }

    //     this._jobServiceProxy.getProductItemList(Id, event.query, this.job.state, false).subscribe(output => {
    //         this.productItemSuggestions = output;
    //     });
    // }

    // Called When Picklist Product Type Change
    changePicklistProductType(i) : void {
        this.PicklistProducts[i].productItemId = null;
        this.PicklistProducts[i].productItemName = '';
        this.PicklistProducts[i].stock = 0;
    }

    // Called When Picklist Product Item Entered.
    filterPicklistProductIteams(event, i): void {
        let Id = this.PicklistProducts[i].productTypeId;

        this._jobServiceProxy.getPicklistProductItemList(Id, event.query).subscribe(output => {
            this.productItemSuggestions = output;
        });
    }

    // Called When Picklist Product Item Selected.
    selectPicklistProductItem(event, i) {

        console.log(event.isExpired);
        this.PicklistProducts[i].productItemId = event.id;
        this.PicklistProducts[i].productItemName = event.productItem;
        this.PicklistProducts[i].model = event.productModel;
        this.PicklistProducts[i].size = event.size;

        //this.calculateSTCRebate(i);

        if(this.checkStockOnHandFeatures())
        {
            this._quickstockServiceProxy.getStockDetails(this.PicklistProducts[i].productItemId, this.picklistwarehouseLocation).subscribe(result => {
                this.PicklistProducts[i].stock = result.stockOnHand;

                this.checkStockOnHandWithQty(i, this.PicklistProducts[i].quantity, result.stockOnHand);
            });
        }
    }

    checkActualCostFeatures() {
        return this.feature.isEnabled("App.LeadDetails.Job.ActualCost");
    }

    checkActualCostFeaturesForView() {
        return this.feature.isEnabled("App.LeadDetails.Job.ActualCost");
    }

    checkActualCostFeaturesWithSkipPermission() {
        return this.feature.isEnabled("App.LeadDetails.Job.ActualCost") && this.permission.isGranted("Pages.LeadDetails.SkipActualCost");
    }

    productItem: GetActualCostItemListInput[];
    jobVariationIds: any[];
    getActualCost() : void {

        if(!this.checkActualCostFeatures())
        {
            return;
        }

        this.productItem = [];

        this.JobProducts.map((item) => {
            let jobProductItem = new GetActualCostItemListInput();
            jobProductItem.productTypeId = item.productTypeId;
            jobProductItem.productItemId = item.productItemId;
            jobProductItem.size = item.size;
            jobProductItem.qty = item.quantity;
            if (jobProductItem.productItemId != 0 && jobProductItem.productItemId != undefined && jobProductItem.qty != 0 && jobProductItem.qty != undefined) {
                this.productItem.push(jobProductItem);
            }
        });

        let jobVariationIds = [];
        this.JobVariations.map((item) => {
            if(item.variationId != undefined && item.variationId != null && item.variationId != 0)
            {
                jobVariationIds.push(item.variationId);
            }
        });

        let systemCapacity = this.job.systemCapacity == null ? undefined : this.job.systemCapacity;
        let houseTypeId = this.job.houseTypeId == null ? undefined : this.job.houseTypeId;
        let roofTypeId = this.job.roofTypeId == null ? undefined : this.job.roofTypeId;
        let roofAngleId = this.job.roofAngleId == null ? undefined : this.job.roofAngleId;
        let stc = this.job.stc == null ? undefined : this.job.stc;
        let warehouseDistance = this.warehouseDistance == null ? undefined : this.warehouseDistance;
        let financeOptionId = this.job.financeOptionId == null ? undefined : this.job.financeOptionId;
        let totalCost = this.job.totalCost == null ? undefined : this.job.totalCost;

        let categoryDDiscountAmt = 0;
        categoryDDiscountAmt = this.job.categoryDDiscountAmt == null ? undefined : this.job.categoryDDiscountAmt;
        // if(this.permission.isGranted("Pages.LeadDetails.CategoryD"))
        // {
        //     categoryDDiscountAmt = this.job.categoryDDiscountAmt == null ? undefined : this.job.categoryDDiscountAmt;
        // }

        this._jobServiceProxy.getActualCost(this.lead.id, this.job.id, this.lead.area, this.job.state, systemCapacity, this.productItem, houseTypeId, roofTypeId, roofAngleId, jobVariationIds
            ,  stc, warehouseDistance, financeOptionId, totalCost, categoryDDiscountAmt).subscribe(result => {
            this.actualCost = result.actualCost;
            this.category = result.category;
            this.panelCost = result.panelCost;
            this.pricePerWatt = result.pricePerWatt;
            this.inverterCost = result.inverterCost;
            this.batteryCost = result.batteryCost;
            this.installationCost = result.installationCost;
            this.batteryInstallationCost = result.betteryInstallationCost;

            this.checkCategory();
        });

        this._jobServiceProxy.getActualCostList(this.lead.id, this.job.id, this.lead.area, this.job.state, systemCapacity, this.productItem, houseTypeId, roofTypeId, roofAngleId, jobVariationIds
            ,  stc, warehouseDistance, financeOptionId, totalCost, categoryDDiscountAmt).subscribe(result => {
            this.ActualCostList = result;
        });
    }

    getJobValidationForStatus(role: string, jobStatus: any): boolean {
        if(role == "Admin")
        {
            return true;
        }
        else{
            if(jobStatus < 6)
            {
                return true;
            }
            else {
                return false;
            }
        }
    }

    clickPylon = false;
    fetchPylon(): void {
        this.clickPylon = true;
        this._jobServiceProxy.getPylon(this.job.id, this.sectionId).subscribe(result => {
            this.clickPylon = false;
            this.getJobDocuments();
        }, error => { this.clickPylon = false; });
    }

    checkProductStockPermission() {
        return this.permission.isGranted("Pages.Jobs.CheckProductStock");
    }

    btnInvoieHide: boolean = false;
    invoiceBtnCheck() : void {
        let minusStockCount = 0;

        if(!this.checkNetSituationFeatures())
        {
            return;
        }

        if(!this.checkProductStockPermission())
        {
            return;
        }

        for (let i = 0; i < this.JobProducts.length; i++) {

            if(this.JobProducts[i].stock != "" && this.JobProducts[i].stock != null && this.JobProducts[i].stock != undefined)
            {
                if(this.JobProducts[i].stock < 0)
                {
                    minusStockCount++;
                }
            }
        }

        if(minusStockCount > 0)
        {
            this.btnInvoieHide = true;
        }
        else
        {
            this.btnInvoieHide = false;
        }

        //console.log(this.btnInvoieHide);
    }

    checkpicklistItemPermission() {
        return this.permission.isGranted("Pages.Jobs.SecondPicklistWithPanelInverter");
    }

    picklistItemCheck() : void {
        if(this.checkpicklistItemPermission())
        {
            return;
        }
        
        // isPanelInverter
        this._picklistServiceProxy.checkSecondPicklistItems(this.job.id).subscribe(result => {
            this.isPanelInverter = result;
        });
    }

    ActualtaxInvoice(): void {

        this.spinnerService.show();
        this._reportServiceProxy.taxInvoiceByJobId(this.job.id).subscribe(result => {
            this.taxInvoiceData = result;

            //console.log(this.taxInvoiceData);

            if(this.taxInvoiceData.viewHtml != '')
            {
                this.htmlStr = this.GetActualTaxInvoice(this.taxInvoiceData.viewHtml);
                this.htmlStr = this.sanitizer.bypassSecurityTrustHtml(this.htmlStr);

                let html = this.htmlStr.changingThisBreaksApplicationSecurity;
                this._commonLookupService.downloadPdf(this.taxInvoiceData.jobNumber, "TaxInvoice", html).subscribe(result => {
                    let FileName = result;
                    this.spinnerService.hide();
                    window.open(result, "_blank");
                },
                err => {
                    this.spinnerService.hide();
                });
            }
        },
        err => {
            this.spinnerService.hide();
        });
        
    }

    ActualPaymentReceipt(): void {
        

        this.spinnerService.show();
        this._reportServiceProxy.paymentReceiptByJobId(this.job.id).subscribe(result => {
            this.paymentReceiptData = result;

            console.log(this.paymentReceiptData);

            if(this.paymentReceiptData.viewHtml != '')
            {
                
                let html = this.GetActualPaymentReceipt(this.paymentReceiptData.viewHtml);

                this._commonLookupService.downloadPdf(this.paymentReceiptData.jobNumber, "PaymetReceipt", html).subscribe(result => {
                    let FileName = result;
                    this.spinnerService.hide();
                    window.open(result, "_blank");
                },
                err => {
                    this.spinnerService.hide();
                });
            }
        },
        err => {
            this.spinnerService.hide();
        });

    }

    GetActualPaymentReceipt(html) {
        let htmlstring = html;

        let myTemplateStr = htmlstring;
        let TableList = [];

        if (this.paymentReceiptData.paymentDetails != null) {
            this.paymentReceiptData.paymentDetails.forEach(obj => {
                // TableList.push("<div class='mb5'>" + obj.name + "</div>");
                TableList.push("<tr><td>" + obj.date + "</td><td>$" + obj.amount + "</td><td>$" + obj.ssCharge + "</td><td>" + obj.method + "</td></tr>");
            })
        }

        let myVariables = {
            PR: {
                JobNumber: this.paymentReceiptData.jobNumber,
                Date: this.paymentReceiptData.date,
                
                Cust: {
                    Name: this.paymentReceiptData.name,
                    Mobile: this.paymentReceiptData.mobile,
                    Email: this.paymentReceiptData.email,
                    AddressLine1: this.paymentReceiptData.addressLine1,
                    AddressLine2: this.paymentReceiptData.addressLine2
                },

                ID: {
                    TCost: this.paymentReceiptData.totalCost != "" && this.paymentReceiptData.totalCost != "0" ? this.paymentReceiptData.totalCost : "0.00",
                    LessStcRebate: this.paymentReceiptData.lessStcRebate != "" && this.paymentReceiptData.lessStcRebate != "0" ? this.paymentReceiptData.lessStcRebate : "0.00",
                    SolarVICRebate: this.paymentReceiptData.solarVICRebate != "" && this.paymentReceiptData.solarVICRebate != "0" ? this.paymentReceiptData.solarVICRebate : "0.00",
                    SolarVICLoan: this.paymentReceiptData.solarVICLoan != "" && this.paymentReceiptData.solarVICLoan != "0" ? this.paymentReceiptData.solarVICLoan : "0.00",
                    ACharge: this.paymentReceiptData.aCharge != "" && this.paymentReceiptData.aCharge != "0" ? this.paymentReceiptData.aCharge : "0.00",
                    Discount: this.paymentReceiptData.discount != "" && this.paymentReceiptData.discount != "0" ? this.paymentReceiptData.discount : "0.00",
                    NetCost: this.paymentReceiptData.netCost != "" && this.paymentReceiptData.netCost != "0" ? this.paymentReceiptData.netCost : "0.00",
                    BalanceDue: this.paymentReceiptData.balanceDue != "" && this.paymentReceiptData.balanceDue != "0" ? this.paymentReceiptData.balanceDue : "0.00"
                },

                PD: {
                    PaymentDetails: TableList.toString().replace(/,/g, '')
                    //PaymentDetails: TableList.toString()
                },

                SA: {
                    AddressLine1: this.paymentReceiptData.siteAddressLine1,
                    AddressLine2: this.paymentReceiptData.siteAddressLine2
                }
            },
        }

        // use custom delimiter {{ }}
        _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;
    
        // interpolate
        let compiled = _.template(myTemplateStr);
        let myTemplateCompiled = compiled(myVariables);
        return myTemplateCompiled;
    }

    GetActualTaxInvoice(html) {
        let htmlstring = html;

        let myTemplateStr = htmlstring;
        let TableList = [];

        if (this.taxInvoiceData.qunityAndModelLists != null) {
            this.taxInvoiceData.qunityAndModelLists.forEach(obj => {
                TableList.push("<div class='mb5'>" + obj.name + "</div>");
            })
        }

        var netCost = this.taxInvoiceData.netCost != "" ? parseFloat(this.taxInvoiceData.netCost) : 0;
        var gst = (netCost * 10) / 100;

        let myVariables = {
            TI: {
                JobNumber: this.taxInvoiceData.jobNumber,
                Date: this.taxInvoiceData.date,
                BalanceDue: this.taxInvoiceData.balanceDue,

                Cust: {
                    Name: this.taxInvoiceData.name,
                    Mobile: this.taxInvoiceData.mobile,
                    Email: this.taxInvoiceData.email,
                    AddressLine1: this.taxInvoiceData.addressLine1,
                    AddressLine2: this.taxInvoiceData.addressLine2,
                    MeterPhase: this.taxInvoiceData.meterPhase,
                    MeterUpgrad: this.taxInvoiceData.meterUpgrad,
                    RoofType: this.taxInvoiceData.roofType,
                    PropertyType: this.taxInvoiceData.propertyType,
                    RoofPitch: this.taxInvoiceData.roofPitch,
                    ElecDist: this.taxInvoiceData.elecDist,
                    ElecRetailer: this.taxInvoiceData.elecRetailer,
                    NIMINumber: this.taxInvoiceData.nmiNumber,
                    MeterNo: this.taxInvoiceData.meterNo
                },

                PD: {

                    SystemDetails: TableList.toString().replace(/,/g, ''),
                    //SystemDetails: '',
                    TCost: this.taxInvoiceData.totalCost != "" && this.taxInvoiceData.totalCost != "0" ? this.taxInvoiceData.totalCost : "0.00",
                    TotalCost: this.taxInvoiceData.tCost != "" && this.taxInvoiceData.tCost != "0" ? this.taxInvoiceData.tCost : "0.00",
                    Stc: this.taxInvoiceData.stc != "" && this.taxInvoiceData.stc != "0" ? this.taxInvoiceData.stc : "0.00",
                    SolarRebate: this.taxInvoiceData.solarRebate != "" && this.taxInvoiceData.solarRebate != "0" ? this.taxInvoiceData.solarRebate : "0.00",
                    SolarLoan: this.taxInvoiceData.solarLoan != "" && this.taxInvoiceData.solarLoan != "0" ? this.taxInvoiceData.solarLoan : "0.00",
                    NetCost: this.taxInvoiceData.netCost != "" && this.taxInvoiceData.netCost != "0" ? this.taxInvoiceData.netCost : "0.00",
                    Deposit: this.taxInvoiceData.deposit != "" && this.taxInvoiceData.deposit != "0" ? this.taxInvoiceData.deposit : "0.00",
                    ACharge: this.taxInvoiceData.aCharge != "" && this.taxInvoiceData.aCharge != "0" ? this.taxInvoiceData.aCharge : "0.00",
                    Discount: this.taxInvoiceData.discount != "" && this.taxInvoiceData.discount != "0" ? this.taxInvoiceData.discount : "0.00",
                    Gst: gst
                }
            }
        }

        // use custom delimiter {{ }}
        _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;
    
        // interpolate
        let compiled = _.template(myTemplateStr);
        let myTemplateCompiled = compiled(myVariables);
        return myTemplateCompiled;
    }

    changeInspection() : void {
        if(this.job.state == 'VIC'){
            if(this.job.vicqRcodeScanDate){
                if(this.job.inspectionDate){

                    if(this.job.inspectionDate < this.job.vicqRcodeScanDate){
                        this.notify.warn("Enter Inspection Date Greater Than Or Equal To VIC QRcode Scan Date");
                        this.job.inspectionDate = this.job.vicqRcodeScanDate;
                    }
                }
            }
            else{
                this.notify.warn("Enter VIC QRcode Scan Date");
            }
        }
        
    }

    categoryHideShow: boolean = true;
    categoryHideShowInstallation: boolean = true;
    ShowInstallationSave: boolean = false;

    checkCategory()
    {
        if(this.checkActualCostFeaturesWithSkipPermission())
        {
            this.categoryHideShow = true;
        }
        else if(this.role != 'Admin' && this.category == 'D' && this.checkActualCostFeatures())
        {
            this.categoryHideShow = false;
        }
        else if(this.checkActualCostFeatures() && (this.actualCost == undefined || this.actualCost == 0 || this.actualCost == null))
        {
            this.categoryHideShow = false;
        }
        else
        {
            this.categoryHideShow = true;
        }

        let startYearDate = moment(new Date(2024, 1, 1));
        if(this.role != 'Admin' && this.category == 'D' && this.checkActualCostFeatures() && (this.job.activeDate > startYearDate))
        {
            debugger;
            this.categoryHideShowInstallation = false;
        }
        else
        {
            this.categoryHideShowInstallation = true;
        }
        
        this.ShowInstallationSave = false;

        this._jobServiceProxy.getJobActiveStatus(this.job.id).subscribe(jobStatus => {
            debugger;
            if(jobStatus.nmiNumber && jobStatus.approvedReferenceNumber && jobStatus.distApproved && jobStatus.meterUpdgrade && jobStatus.meterBoxPhoto && jobStatus.signedQuote 
                && jobStatus.deposite && jobStatus.elecRetailerId && jobStatus.peakMeterNos
            ){
                if(jobStatus.payementoptionid == 1){
                    if(jobStatus.finance){
                        if(jobStatus.state == 'VIC'){
                            if(jobStatus.expiryDate){
                                if(jobStatus.state == 'VIC' && jobStatus.vicRebate == 'With Rebate Install'){
                                    if(jobStatus.solarRebateStatus){
                                        if(jobStatus.paymentType && jobStatus.payementoptionid == 1){
                                            if(jobStatus.paymentVerified){
                                                if(jobStatus.quote){
                                                    if(jobStatus.quoteSigned){
                                                        if(jobStatus.exportControlForm){
                                                            if(jobStatus.exportControlFormSigned){
                                                                if(jobStatus.feedInTariff){
                                                                    if(jobStatus.feedInTariffSigned){
                                                                        if(jobStatus.shadingDeclaration){
                                                                            if(jobStatus.shadingDeclarationSigned){
                                                                                if(jobStatus.efficiencyDeclaration){
                                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                                    }                                                                                    
                                                                                }
                                                                                else {
                                                                                    this.ShowInstallationSave = true;
                                                                                }
                                                                            }
                                                                            
                                                                        }
                                                                        else {
                                                                            if(jobStatus.efficiencyDeclaration){
                                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                                }                                                                                    
                                                                            }
                                                                            else {
                                                                                this.ShowInstallationSave = true;
                                                                            }
                                                                        }
                                                                    }
                                                                    
                                                                }
                                                                else {
                                                                    if(jobStatus.shadingDeclaration){
                                                                        if(jobStatus.shadingDeclarationSigned){
                                                                            if(jobStatus.efficiencyDeclaration){
                                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                                }                                                                                    
                                                                            }
                                                                            else {
                                                                                this.ShowInstallationSave = true;
                                                                            }
                                                                        }
                                                                        
                                                                    }
                                                                    else {
                                                                        if(jobStatus.efficiencyDeclaration){
                                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                                 this.ShowInstallationSave = true;                                                                                        
                                                                            }                                                                                    
                                                                        }
                                                                        else {
                                                                            this.ShowInstallationSave = true;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.feedInTariff){
                                                                if(jobStatus.feedInTariffSigned){
                                                                    if(jobStatus.shadingDeclaration){
                                                                        if(jobStatus.shadingDeclarationSigned){
                                                                            if(jobStatus.efficiencyDeclaration){
                                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                                }                                                                                    
                                                                            }
                                                                            else {
                                                                                this.ShowInstallationSave = true;
                                                                            }
                                                                        }
                                                                        
                                                                    }
                                                                    else {
                                                                        if(jobStatus.efficiencyDeclaration){
                                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                                 this.ShowInstallationSave = true;                                                                                        
                                                                            }                                                                                    
                                                                        }
                                                                        else {
                                                                            this.ShowInstallationSave = true;
                                                                        }
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.shadingDeclaration){
                                                                    if(jobStatus.shadingDeclarationSigned){
                                                                        if(jobStatus.efficiencyDeclaration){
                                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                                 this.ShowInstallationSave = true;                                                                                        
                                                                            }                                                                                    
                                                                        }
                                                                        else {
                                                                            this.ShowInstallationSave = true;
                                                                        }
                                                                    }
                                                                    
                                                                }
                                                                else {
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                   
                                                }
                                                else{
                                                    if(jobStatus.exportControlForm){
                                                        if(jobStatus.exportControlFormSigned){
                                                            if(jobStatus.feedInTariff){
                                                                if(jobStatus.feedInTariffSigned){
                                                                    if(jobStatus.shadingDeclaration){
                                                                        if(jobStatus.shadingDeclarationSigned){
                                                                            if(jobStatus.efficiencyDeclaration){
                                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                                }                                                                                    
                                                                            }
                                                                            else {
                                                                                this.ShowInstallationSave = true;
                                                                            }
                                                                        }
                                                                        
                                                                    }
                                                                    else {
                                                                        if(jobStatus.efficiencyDeclaration){
                                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                                 this.ShowInstallationSave = true;                                                                                        
                                                                            }                                                                                    
                                                                        }
                                                                        else {
                                                                            this.ShowInstallationSave = true;
                                                                        }
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.shadingDeclaration){
                                                                    if(jobStatus.shadingDeclarationSigned){
                                                                        if(jobStatus.efficiencyDeclaration){
                                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                                 this.ShowInstallationSave = true;                                                                                        
                                                                            }                                                                                    
                                                                        }
                                                                        else {
                                                                            this.ShowInstallationSave = true;
                                                                        }
                                                                    }
                                                                    
                                                                }
                                                                else {
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.feedInTariff){
                                                            if(jobStatus.feedInTariffSigned){
                                                                if(jobStatus.shadingDeclaration){
                                                                    if(jobStatus.shadingDeclarationSigned){
                                                                        if(jobStatus.efficiencyDeclaration){
                                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                                 this.ShowInstallationSave = true;                                                                                        
                                                                            }                                                                                    
                                                                        }
                                                                        else {
                                                                            this.ShowInstallationSave = true;
                                                                        }
                                                                    }
                                                                    
                                                                }
                                                                else {
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.shadingDeclaration){
                                                                if(jobStatus.shadingDeclarationSigned){
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            
                                        }
                                        else{
                                            if(jobStatus.quote){
                                                if(jobStatus.quoteSigned){
                                                    if(jobStatus.exportControlForm){
                                                        if(jobStatus.exportControlFormSigned){
                                                            if(jobStatus.feedInTariff){
                                                                if(jobStatus.feedInTariffSigned){
                                                                    if(jobStatus.shadingDeclaration){
                                                                        if(jobStatus.shadingDeclarationSigned){
                                                                            if(jobStatus.efficiencyDeclaration){
                                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                                }                                                                                    
                                                                            }
                                                                            else {
                                                                                this.ShowInstallationSave = true;
                                                                            }
                                                                        }
                                                                        
                                                                    }
                                                                    else {
                                                                        if(jobStatus.efficiencyDeclaration){
                                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                                 this.ShowInstallationSave = true;                                                                                        
                                                                            }                                                                                    
                                                                        }
                                                                        else {
                                                                            this.ShowInstallationSave = true;
                                                                        }
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.shadingDeclaration){
                                                                    if(jobStatus.shadingDeclarationSigned){
                                                                        if(jobStatus.efficiencyDeclaration){
                                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                                 this.ShowInstallationSave = true;                                                                                        
                                                                            }                                                                                    
                                                                        }
                                                                        else {
                                                                            this.ShowInstallationSave = true;
                                                                        }
                                                                    }
                                                                    
                                                                }
                                                                else {
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.feedInTariff){
                                                            if(jobStatus.feedInTariffSigned){
                                                                if(jobStatus.shadingDeclaration){
                                                                    if(jobStatus.shadingDeclarationSigned){
                                                                        if(jobStatus.efficiencyDeclaration){
                                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                                 this.ShowInstallationSave = true;                                                                                        
                                                                            }                                                                                    
                                                                        }
                                                                        else {
                                                                            this.ShowInstallationSave = true;
                                                                        }
                                                                    }
                                                                    
                                                                }
                                                                else {
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.shadingDeclaration){
                                                                if(jobStatus.shadingDeclarationSigned){
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                               
                                            }
                                            else{
                                                if(jobStatus.exportControlForm){
                                                    if(jobStatus.exportControlFormSigned){
                                                        if(jobStatus.feedInTariff){
                                                            if(jobStatus.feedInTariffSigned){
                                                                if(jobStatus.shadingDeclaration){
                                                                    if(jobStatus.shadingDeclarationSigned){
                                                                        if(jobStatus.efficiencyDeclaration){
                                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                                 this.ShowInstallationSave = true;                                                                                        
                                                                            }                                                                                    
                                                                        }
                                                                        else {
                                                                            this.ShowInstallationSave = true;
                                                                        }
                                                                    }
                                                                    
                                                                }
                                                                else {
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.shadingDeclaration){
                                                                if(jobStatus.shadingDeclarationSigned){
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    
                                                }
                                                else {
                                                    if(jobStatus.feedInTariff){
                                                        if(jobStatus.feedInTariffSigned){
                                                            if(jobStatus.shadingDeclaration){
                                                                if(jobStatus.shadingDeclarationSigned){
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.shadingDeclaration){
                                                            if(jobStatus.shadingDeclarationSigned){
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    
                                }
                                else{
                                    if(jobStatus.paymentType && jobStatus.payementoptionid == 1){
                                        if(jobStatus.paymentVerified){
                                            if(jobStatus.quote){
                                                if(jobStatus.quoteSigned){
                                                    if(jobStatus.exportControlForm){
                                                        if(jobStatus.exportControlFormSigned){
                                                            if(jobStatus.feedInTariff){
                                                                if(jobStatus.feedInTariffSigned){
                                                                    if(jobStatus.shadingDeclaration){
                                                                        if(jobStatus.shadingDeclarationSigned){
                                                                            if(jobStatus.efficiencyDeclaration){
                                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                                }                                                                                    
                                                                            }
                                                                            else {
                                                                                this.ShowInstallationSave = true;
                                                                            }
                                                                        }
                                                                        
                                                                    }
                                                                    else {
                                                                        if(jobStatus.efficiencyDeclaration){
                                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                                 this.ShowInstallationSave = true;                                                                                        
                                                                            }                                                                                    
                                                                        }
                                                                        else {
                                                                            this.ShowInstallationSave = true;
                                                                        }
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.shadingDeclaration){
                                                                    if(jobStatus.shadingDeclarationSigned){
                                                                        if(jobStatus.efficiencyDeclaration){
                                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                                 this.ShowInstallationSave = true;                                                                                        
                                                                            }                                                                                    
                                                                        }
                                                                        else {
                                                                            this.ShowInstallationSave = true;
                                                                        }
                                                                    }
                                                                    
                                                                }
                                                                else {
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.feedInTariff){
                                                            if(jobStatus.feedInTariffSigned){
                                                                if(jobStatus.shadingDeclaration){
                                                                    if(jobStatus.shadingDeclarationSigned){
                                                                        if(jobStatus.efficiencyDeclaration){
                                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                                 this.ShowInstallationSave = true;                                                                                        
                                                                            }                                                                                    
                                                                        }
                                                                        else {
                                                                            this.ShowInstallationSave = true;
                                                                        }
                                                                    }
                                                                    
                                                                }
                                                                else {
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.shadingDeclaration){
                                                                if(jobStatus.shadingDeclarationSigned){
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                               
                                            }
                                            else{
                                                if(jobStatus.exportControlForm){
                                                    if(jobStatus.exportControlFormSigned){
                                                        if(jobStatus.feedInTariff){
                                                            if(jobStatus.feedInTariffSigned){
                                                                if(jobStatus.shadingDeclaration){
                                                                    if(jobStatus.shadingDeclarationSigned){
                                                                        if(jobStatus.efficiencyDeclaration){
                                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                                 this.ShowInstallationSave = true;                                                                                        
                                                                            }                                                                                    
                                                                        }
                                                                        else {
                                                                            this.ShowInstallationSave = true;
                                                                        }
                                                                    }
                                                                    
                                                                }
                                                                else {
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.shadingDeclaration){
                                                                if(jobStatus.shadingDeclarationSigned){
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    
                                                }
                                                else {
                                                    if(jobStatus.feedInTariff){
                                                        if(jobStatus.feedInTariffSigned){
                                                            if(jobStatus.shadingDeclaration){
                                                                if(jobStatus.shadingDeclarationSigned){
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.shadingDeclaration){
                                                            if(jobStatus.shadingDeclarationSigned){
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        
                                    }
                                    else{
                                        if(jobStatus.quote){
                                            if(jobStatus.quoteSigned){
                                                if(jobStatus.exportControlForm){
                                                    if(jobStatus.exportControlFormSigned){
                                                        if(jobStatus.feedInTariff){
                                                            if(jobStatus.feedInTariffSigned){
                                                                if(jobStatus.shadingDeclaration){
                                                                    if(jobStatus.shadingDeclarationSigned){
                                                                        if(jobStatus.efficiencyDeclaration){
                                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                                 this.ShowInstallationSave = true;                                                                                        
                                                                            }                                                                                    
                                                                        }
                                                                        else {
                                                                            this.ShowInstallationSave = true;
                                                                        }
                                                                    }
                                                                    
                                                                }
                                                                else {
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.shadingDeclaration){
                                                                if(jobStatus.shadingDeclarationSigned){
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    
                                                }
                                                else {
                                                    if(jobStatus.feedInTariff){
                                                        if(jobStatus.feedInTariffSigned){
                                                            if(jobStatus.shadingDeclaration){
                                                                if(jobStatus.shadingDeclarationSigned){
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.shadingDeclaration){
                                                            if(jobStatus.shadingDeclarationSigned){
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                           
                                        }
                                        else{
                                            if(jobStatus.exportControlForm){
                                                if(jobStatus.exportControlFormSigned){
                                                    if(jobStatus.feedInTariff){
                                                        if(jobStatus.feedInTariffSigned){
                                                            if(jobStatus.shadingDeclaration){
                                                                if(jobStatus.shadingDeclarationSigned){
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.shadingDeclaration){
                                                            if(jobStatus.shadingDeclarationSigned){
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                    }
                                                }
                                                
                                            }
                                            else {
                                                if(jobStatus.feedInTariff){
                                                    if(jobStatus.feedInTariffSigned){
                                                        if(jobStatus.shadingDeclaration){
                                                            if(jobStatus.shadingDeclarationSigned){
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                    }
                                                    
                                                }
                                                else {
                                                    if(jobStatus.shadingDeclaration){
                                                        if(jobStatus.shadingDeclarationSigned){
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.efficiencyDeclaration){
                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                 this.ShowInstallationSave = true;                                                                                        
                                                            }                                                                                    
                                                        }
                                                        else {
                                                            this.ShowInstallationSave = true;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            
                        }
                        else{
                            if(jobStatus.state == 'VIC' && jobStatus.vicRebate == 'With Rebate Install'){
                                if(jobStatus.solarRebateStatus){
                                    if(jobStatus.paymentType && jobStatus.payementoptionid == 1){
                                        if(jobStatus.paymentVerified){
                                            if(jobStatus.quote){
                                                if(jobStatus.quoteSigned){
                                                    if(jobStatus.exportControlForm){
                                                        if(jobStatus.exportControlFormSigned){
                                                            if(jobStatus.feedInTariff){
                                                                if(jobStatus.feedInTariffSigned){
                                                                    if(jobStatus.shadingDeclaration){
                                                                        if(jobStatus.shadingDeclarationSigned){
                                                                            if(jobStatus.efficiencyDeclaration){
                                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                                }                                                                                    
                                                                            }
                                                                            else {
                                                                                this.ShowInstallationSave = true;
                                                                            }
                                                                        }
                                                                        
                                                                    }
                                                                    else {
                                                                        if(jobStatus.efficiencyDeclaration){
                                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                                 this.ShowInstallationSave = true;                                                                                        
                                                                            }                                                                                    
                                                                        }
                                                                        else {
                                                                            this.ShowInstallationSave = true;
                                                                        }
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.shadingDeclaration){
                                                                    if(jobStatus.shadingDeclarationSigned){
                                                                        if(jobStatus.efficiencyDeclaration){
                                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                                 this.ShowInstallationSave = true;                                                                                        
                                                                            }                                                                                    
                                                                        }
                                                                        else {
                                                                            this.ShowInstallationSave = true;
                                                                        }
                                                                    }
                                                                    
                                                                }
                                                                else {
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.feedInTariff){
                                                            if(jobStatus.feedInTariffSigned){
                                                                if(jobStatus.shadingDeclaration){
                                                                    if(jobStatus.shadingDeclarationSigned){
                                                                        if(jobStatus.efficiencyDeclaration){
                                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                                 this.ShowInstallationSave = true;                                                                                        
                                                                            }                                                                                    
                                                                        }
                                                                        else {
                                                                            this.ShowInstallationSave = true;
                                                                        }
                                                                    }
                                                                    
                                                                }
                                                                else {
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.shadingDeclaration){
                                                                if(jobStatus.shadingDeclarationSigned){
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                               
                                            }
                                            else{
                                                if(jobStatus.exportControlForm){
                                                    if(jobStatus.exportControlFormSigned){
                                                        if(jobStatus.feedInTariff){
                                                            if(jobStatus.feedInTariffSigned){
                                                                if(jobStatus.shadingDeclaration){
                                                                    if(jobStatus.shadingDeclarationSigned){
                                                                        if(jobStatus.efficiencyDeclaration){
                                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                                 this.ShowInstallationSave = true;                                                                                        
                                                                            }                                                                                    
                                                                        }
                                                                        else {
                                                                            this.ShowInstallationSave = true;
                                                                        }
                                                                    }
                                                                    
                                                                }
                                                                else {
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.shadingDeclaration){
                                                                if(jobStatus.shadingDeclarationSigned){
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    
                                                }
                                                else {
                                                    if(jobStatus.feedInTariff){
                                                        if(jobStatus.feedInTariffSigned){
                                                            if(jobStatus.shadingDeclaration){
                                                                if(jobStatus.shadingDeclarationSigned){
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.shadingDeclaration){
                                                            if(jobStatus.shadingDeclarationSigned){
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        
                                    }
                                    else{
                                        if(jobStatus.quote){
                                            if(jobStatus.quoteSigned){
                                                if(jobStatus.exportControlForm){
                                                    if(jobStatus.exportControlFormSigned){
                                                        if(jobStatus.feedInTariff){
                                                            if(jobStatus.feedInTariffSigned){
                                                                if(jobStatus.shadingDeclaration){
                                                                    if(jobStatus.shadingDeclarationSigned){
                                                                        if(jobStatus.efficiencyDeclaration){
                                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                                 this.ShowInstallationSave = true;                                                                                        
                                                                            }                                                                                    
                                                                        }
                                                                        else {
                                                                            this.ShowInstallationSave = true;
                                                                        }
                                                                    }
                                                                    
                                                                }
                                                                else {
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.shadingDeclaration){
                                                                if(jobStatus.shadingDeclarationSigned){
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    
                                                }
                                                else {
                                                    if(jobStatus.feedInTariff){
                                                        if(jobStatus.feedInTariffSigned){
                                                            if(jobStatus.shadingDeclaration){
                                                                if(jobStatus.shadingDeclarationSigned){
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.shadingDeclaration){
                                                            if(jobStatus.shadingDeclarationSigned){
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                           
                                        }
                                        else{
                                            if(jobStatus.exportControlForm){
                                                if(jobStatus.exportControlFormSigned){
                                                    if(jobStatus.feedInTariff){
                                                        if(jobStatus.feedInTariffSigned){
                                                            if(jobStatus.shadingDeclaration){
                                                                if(jobStatus.shadingDeclarationSigned){
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.shadingDeclaration){
                                                            if(jobStatus.shadingDeclarationSigned){
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                    }
                                                }
                                                
                                            }
                                            else {
                                                if(jobStatus.feedInTariff){
                                                    if(jobStatus.feedInTariffSigned){
                                                        if(jobStatus.shadingDeclaration){
                                                            if(jobStatus.shadingDeclarationSigned){
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                    }
                                                    
                                                }
                                                else {
                                                    if(jobStatus.shadingDeclaration){
                                                        if(jobStatus.shadingDeclarationSigned){
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.efficiencyDeclaration){
                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                 this.ShowInstallationSave = true;                                                                                        
                                                            }                                                                                    
                                                        }
                                                        else {
                                                            this.ShowInstallationSave = true;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                
                            }
                            else{
                                if(jobStatus.paymentType && jobStatus.payementoptionid == 1){
                                    if(jobStatus.paymentVerified){
                                        if(jobStatus.quote){
                                            if(jobStatus.quoteSigned){
                                                if(jobStatus.exportControlForm){
                                                    if(jobStatus.exportControlFormSigned){
                                                        if(jobStatus.feedInTariff){
                                                            if(jobStatus.feedInTariffSigned){
                                                                if(jobStatus.shadingDeclaration){
                                                                    if(jobStatus.shadingDeclarationSigned){
                                                                        if(jobStatus.efficiencyDeclaration){
                                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                                 this.ShowInstallationSave = true;                                                                                        
                                                                            }                                                                                    
                                                                        }
                                                                        else {
                                                                            this.ShowInstallationSave = true;
                                                                        }
                                                                    }
                                                                    
                                                                }
                                                                else {
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.shadingDeclaration){
                                                                if(jobStatus.shadingDeclarationSigned){
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    
                                                }
                                                else {
                                                    if(jobStatus.feedInTariff){
                                                        if(jobStatus.feedInTariffSigned){
                                                            if(jobStatus.shadingDeclaration){
                                                                if(jobStatus.shadingDeclarationSigned){
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.shadingDeclaration){
                                                            if(jobStatus.shadingDeclarationSigned){
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                           
                                        }
                                        else{
                                            if(jobStatus.exportControlForm){
                                                if(jobStatus.exportControlFormSigned){
                                                    if(jobStatus.feedInTariff){
                                                        if(jobStatus.feedInTariffSigned){
                                                            if(jobStatus.shadingDeclaration){
                                                                if(jobStatus.shadingDeclarationSigned){
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.shadingDeclaration){
                                                            if(jobStatus.shadingDeclarationSigned){
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                    }
                                                }
                                                
                                            }
                                            else {
                                                if(jobStatus.feedInTariff){
                                                    if(jobStatus.feedInTariffSigned){
                                                        if(jobStatus.shadingDeclaration){
                                                            if(jobStatus.shadingDeclarationSigned){
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                    }
                                                    
                                                }
                                                else {
                                                    if(jobStatus.shadingDeclaration){
                                                        if(jobStatus.shadingDeclarationSigned){
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.efficiencyDeclaration){
                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                 this.ShowInstallationSave = true;                                                                                        
                                                            }                                                                                    
                                                        }
                                                        else {
                                                            this.ShowInstallationSave = true;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    
                                }
                                else{
                                    if(jobStatus.quote){
                                        if(jobStatus.quoteSigned){
                                            if(jobStatus.exportControlForm){
                                                if(jobStatus.exportControlFormSigned){
                                                    if(jobStatus.feedInTariff){
                                                        if(jobStatus.feedInTariffSigned){
                                                            if(jobStatus.shadingDeclaration){
                                                                if(jobStatus.shadingDeclarationSigned){
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.shadingDeclaration){
                                                            if(jobStatus.shadingDeclarationSigned){
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                    }
                                                }
                                                
                                            }
                                            else {
                                                if(jobStatus.feedInTariff){
                                                    if(jobStatus.feedInTariffSigned){
                                                        if(jobStatus.shadingDeclaration){
                                                            if(jobStatus.shadingDeclarationSigned){
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                    }
                                                    
                                                }
                                                else {
                                                    if(jobStatus.shadingDeclaration){
                                                        if(jobStatus.shadingDeclarationSigned){
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.efficiencyDeclaration){
                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                 this.ShowInstallationSave = true;                                                                                        
                                                            }                                                                                    
                                                        }
                                                        else {
                                                            this.ShowInstallationSave = true;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                       
                                    }
                                    else{
                                        if(jobStatus.exportControlForm){
                                            if(jobStatus.exportControlFormSigned){
                                                if(jobStatus.feedInTariff){
                                                    if(jobStatus.feedInTariffSigned){
                                                        if(jobStatus.shadingDeclaration){
                                                            if(jobStatus.shadingDeclarationSigned){
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                    }
                                                    
                                                }
                                                else {
                                                    if(jobStatus.shadingDeclaration){
                                                        if(jobStatus.shadingDeclarationSigned){
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.efficiencyDeclaration){
                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                 this.ShowInstallationSave = true;                                                                                        
                                                            }                                                                                    
                                                        }
                                                        else {
                                                            this.ShowInstallationSave = true;
                                                        }
                                                    }
                                                }
                                            }
                                            
                                        }
                                        else {
                                            if(jobStatus.feedInTariff){
                                                if(jobStatus.feedInTariffSigned){
                                                    if(jobStatus.shadingDeclaration){
                                                        if(jobStatus.shadingDeclarationSigned){
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.efficiencyDeclaration){
                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                 this.ShowInstallationSave = true;                                                                                        
                                                            }                                                                                    
                                                        }
                                                        else {
                                                            this.ShowInstallationSave = true;
                                                        }
                                                    }
                                                }
                                                
                                            }
                                            else {
                                                if(jobStatus.shadingDeclaration){
                                                    if(jobStatus.shadingDeclarationSigned){
                                                        if(jobStatus.efficiencyDeclaration){
                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                 this.ShowInstallationSave = true;                                                                                        
                                                            }                                                                                    
                                                        }
                                                        else {
                                                            this.ShowInstallationSave = true;
                                                        }
                                                    }
                                                    
                                                }
                                                else {
                                                    if(jobStatus.efficiencyDeclaration){
                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                             this.ShowInstallationSave = true;                                                                                        
                                                        }                                                                                    
                                                    }
                                                    else {
                                                        this.ShowInstallationSave = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                   
                }
                else{
                    if(jobStatus.state == 'VIC'){
                        if(jobStatus.expiryDate){
                            if(jobStatus.state == 'VIC' && jobStatus.vicRebate == 'With Rebate Install'){
                                if(jobStatus.solarRebateStatus){
                                    if(jobStatus.paymentType && jobStatus.payementoptionid == 1){
                                        if(jobStatus.paymentVerified){
                                            if(jobStatus.quote){
                                                if(jobStatus.quoteSigned){
                                                    if(jobStatus.exportControlForm){
                                                        if(jobStatus.exportControlFormSigned){
                                                            if(jobStatus.feedInTariff){
                                                                if(jobStatus.feedInTariffSigned){
                                                                    if(jobStatus.shadingDeclaration){
                                                                        if(jobStatus.shadingDeclarationSigned){
                                                                            if(jobStatus.efficiencyDeclaration){
                                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                                }                                                                                    
                                                                            }
                                                                            else {
                                                                                this.ShowInstallationSave = true;
                                                                            }
                                                                        }
                                                                        
                                                                    }
                                                                    else {
                                                                        if(jobStatus.efficiencyDeclaration){
                                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                                 this.ShowInstallationSave = true;                                                                                        
                                                                            }                                                                                    
                                                                        }
                                                                        else {
                                                                            this.ShowInstallationSave = true;
                                                                        }
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.shadingDeclaration){
                                                                    if(jobStatus.shadingDeclarationSigned){
                                                                        if(jobStatus.efficiencyDeclaration){
                                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                                 this.ShowInstallationSave = true;                                                                                        
                                                                            }                                                                                    
                                                                        }
                                                                        else {
                                                                            this.ShowInstallationSave = true;
                                                                        }
                                                                    }
                                                                    
                                                                }
                                                                else {
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.feedInTariff){
                                                            if(jobStatus.feedInTariffSigned){
                                                                if(jobStatus.shadingDeclaration){
                                                                    if(jobStatus.shadingDeclarationSigned){
                                                                        if(jobStatus.efficiencyDeclaration){
                                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                                 this.ShowInstallationSave = true;                                                                                        
                                                                            }                                                                                    
                                                                        }
                                                                        else {
                                                                            this.ShowInstallationSave = true;
                                                                        }
                                                                    }
                                                                    
                                                                }
                                                                else {
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.shadingDeclaration){
                                                                if(jobStatus.shadingDeclarationSigned){
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                               
                                            }
                                            else{
                                                if(jobStatus.exportControlForm){
                                                    if(jobStatus.exportControlFormSigned){
                                                        if(jobStatus.feedInTariff){
                                                            if(jobStatus.feedInTariffSigned){
                                                                if(jobStatus.shadingDeclaration){
                                                                    if(jobStatus.shadingDeclarationSigned){
                                                                        if(jobStatus.efficiencyDeclaration){
                                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                                 this.ShowInstallationSave = true;                                                                                        
                                                                            }                                                                                    
                                                                        }
                                                                        else {
                                                                            this.ShowInstallationSave = true;
                                                                        }
                                                                    }
                                                                    
                                                                }
                                                                else {
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.shadingDeclaration){
                                                                if(jobStatus.shadingDeclarationSigned){
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    
                                                }
                                                else {
                                                    if(jobStatus.feedInTariff){
                                                        if(jobStatus.feedInTariffSigned){
                                                            if(jobStatus.shadingDeclaration){
                                                                if(jobStatus.shadingDeclarationSigned){
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.shadingDeclaration){
                                                            if(jobStatus.shadingDeclarationSigned){
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        
                                    }
                                    else{
                                        if(jobStatus.quote){
                                            if(jobStatus.quoteSigned){
                                                if(jobStatus.exportControlForm){
                                                    if(jobStatus.exportControlFormSigned){
                                                        if(jobStatus.feedInTariff){
                                                            if(jobStatus.feedInTariffSigned){
                                                                if(jobStatus.shadingDeclaration){
                                                                    if(jobStatus.shadingDeclarationSigned){
                                                                        if(jobStatus.efficiencyDeclaration){
                                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                                 this.ShowInstallationSave = true;                                                                                        
                                                                            }                                                                                    
                                                                        }
                                                                        else {
                                                                            this.ShowInstallationSave = true;
                                                                        }
                                                                    }
                                                                    
                                                                }
                                                                else {
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.shadingDeclaration){
                                                                if(jobStatus.shadingDeclarationSigned){
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    
                                                }
                                                else {
                                                    if(jobStatus.feedInTariff){
                                                        if(jobStatus.feedInTariffSigned){
                                                            if(jobStatus.shadingDeclaration){
                                                                if(jobStatus.shadingDeclarationSigned){
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.shadingDeclaration){
                                                            if(jobStatus.shadingDeclarationSigned){
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                           
                                        }
                                        else{
                                            if(jobStatus.exportControlForm){
                                                if(jobStatus.exportControlFormSigned){
                                                    if(jobStatus.feedInTariff){
                                                        if(jobStatus.feedInTariffSigned){
                                                            if(jobStatus.shadingDeclaration){
                                                                if(jobStatus.shadingDeclarationSigned){
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.shadingDeclaration){
                                                            if(jobStatus.shadingDeclarationSigned){
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                    }
                                                }
                                                
                                            }
                                            else {
                                                if(jobStatus.feedInTariff){
                                                    if(jobStatus.feedInTariffSigned){
                                                        if(jobStatus.shadingDeclaration){
                                                            if(jobStatus.shadingDeclarationSigned){
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                    }
                                                    
                                                }
                                                else {
                                                    if(jobStatus.shadingDeclaration){
                                                        if(jobStatus.shadingDeclarationSigned){
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.efficiencyDeclaration){
                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                 this.ShowInstallationSave = true;                                                                                        
                                                            }                                                                                    
                                                        }
                                                        else {
                                                            this.ShowInstallationSave = true;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                
                            }
                            else{
                                if(jobStatus.paymentType && jobStatus.payementoptionid == 1){
                                    if(jobStatus.paymentVerified){
                                        if(jobStatus.quote){
                                            if(jobStatus.quoteSigned){
                                                if(jobStatus.exportControlForm){
                                                    if(jobStatus.exportControlFormSigned){
                                                        if(jobStatus.feedInTariff){
                                                            if(jobStatus.feedInTariffSigned){
                                                                if(jobStatus.shadingDeclaration){
                                                                    if(jobStatus.shadingDeclarationSigned){
                                                                        if(jobStatus.efficiencyDeclaration){
                                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                                 this.ShowInstallationSave = true;                                                                                        
                                                                            }                                                                                    
                                                                        }
                                                                        else {
                                                                            this.ShowInstallationSave = true;
                                                                        }
                                                                    }
                                                                    
                                                                }
                                                                else {
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.shadingDeclaration){
                                                                if(jobStatus.shadingDeclarationSigned){
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    
                                                }
                                                else {
                                                    if(jobStatus.feedInTariff){
                                                        if(jobStatus.feedInTariffSigned){
                                                            if(jobStatus.shadingDeclaration){
                                                                if(jobStatus.shadingDeclarationSigned){
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.shadingDeclaration){
                                                            if(jobStatus.shadingDeclarationSigned){
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                           
                                        }
                                        else{
                                            if(jobStatus.exportControlForm){
                                                if(jobStatus.exportControlFormSigned){
                                                    if(jobStatus.feedInTariff){
                                                        if(jobStatus.feedInTariffSigned){
                                                            if(jobStatus.shadingDeclaration){
                                                                if(jobStatus.shadingDeclarationSigned){
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.shadingDeclaration){
                                                            if(jobStatus.shadingDeclarationSigned){
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                    }
                                                }
                                                
                                            }
                                            else {
                                                if(jobStatus.feedInTariff){
                                                    if(jobStatus.feedInTariffSigned){
                                                        if(jobStatus.shadingDeclaration){
                                                            if(jobStatus.shadingDeclarationSigned){
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                    }
                                                    
                                                }
                                                else {
                                                    if(jobStatus.shadingDeclaration){
                                                        if(jobStatus.shadingDeclarationSigned){
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.efficiencyDeclaration){
                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                 this.ShowInstallationSave = true;                                                                                        
                                                            }                                                                                    
                                                        }
                                                        else {
                                                            this.ShowInstallationSave = true;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    
                                }
                                else{
                                    if(jobStatus.quote){
                                        if(jobStatus.quoteSigned){
                                            if(jobStatus.exportControlForm){
                                                if(jobStatus.exportControlFormSigned){
                                                    if(jobStatus.feedInTariff){
                                                        if(jobStatus.feedInTariffSigned){
                                                            if(jobStatus.shadingDeclaration){
                                                                if(jobStatus.shadingDeclarationSigned){
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.shadingDeclaration){
                                                            if(jobStatus.shadingDeclarationSigned){
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                    }
                                                }
                                                
                                            }
                                            else {
                                                if(jobStatus.feedInTariff){
                                                    if(jobStatus.feedInTariffSigned){
                                                        if(jobStatus.shadingDeclaration){
                                                            if(jobStatus.shadingDeclarationSigned){
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                    }
                                                    
                                                }
                                                else {
                                                    if(jobStatus.shadingDeclaration){
                                                        if(jobStatus.shadingDeclarationSigned){
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.efficiencyDeclaration){
                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                 this.ShowInstallationSave = true;                                                                                        
                                                            }                                                                                    
                                                        }
                                                        else {
                                                            this.ShowInstallationSave = true;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                       
                                    }
                                    else{
                                        if(jobStatus.exportControlForm){
                                            if(jobStatus.exportControlFormSigned){
                                                if(jobStatus.feedInTariff){
                                                    if(jobStatus.feedInTariffSigned){
                                                        if(jobStatus.shadingDeclaration){
                                                            if(jobStatus.shadingDeclarationSigned){
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                    }
                                                    
                                                }
                                                else {
                                                    if(jobStatus.shadingDeclaration){
                                                        if(jobStatus.shadingDeclarationSigned){
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.efficiencyDeclaration){
                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                 this.ShowInstallationSave = true;                                                                                        
                                                            }                                                                                    
                                                        }
                                                        else {
                                                            this.ShowInstallationSave = true;
                                                        }
                                                    }
                                                }
                                            }
                                            
                                        }
                                        else {
                                            if(jobStatus.feedInTariff){
                                                if(jobStatus.feedInTariffSigned){
                                                    if(jobStatus.shadingDeclaration){
                                                        if(jobStatus.shadingDeclarationSigned){
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.efficiencyDeclaration){
                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                 this.ShowInstallationSave = true;                                                                                        
                                                            }                                                                                    
                                                        }
                                                        else {
                                                            this.ShowInstallationSave = true;
                                                        }
                                                    }
                                                }
                                                
                                            }
                                            else {
                                                if(jobStatus.shadingDeclaration){
                                                    if(jobStatus.shadingDeclarationSigned){
                                                        if(jobStatus.efficiencyDeclaration){
                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                 this.ShowInstallationSave = true;                                                                                        
                                                            }                                                                                    
                                                        }
                                                        else {
                                                            this.ShowInstallationSave = true;
                                                        }
                                                    }
                                                    
                                                }
                                                else {
                                                    if(jobStatus.efficiencyDeclaration){
                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                             this.ShowInstallationSave = true;                                                                                        
                                                        }                                                                                    
                                                    }
                                                    else {
                                                        this.ShowInstallationSave = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                    }
                    else{
                        if(jobStatus.state == 'VIC' && jobStatus.vicRebate == 'With Rebate Install'){
                            if(jobStatus.solarRebateStatus){
                                if(jobStatus.paymentType && jobStatus.payementoptionid == 1){
                                    if(jobStatus.paymentVerified){
                                        if(jobStatus.quote){
                                            if(jobStatus.quoteSigned){
                                                if(jobStatus.exportControlForm){
                                                    if(jobStatus.exportControlFormSigned){
                                                        if(jobStatus.feedInTariff){
                                                            if(jobStatus.feedInTariffSigned){
                                                                if(jobStatus.shadingDeclaration){
                                                                    if(jobStatus.shadingDeclarationSigned){
                                                                        if(jobStatus.efficiencyDeclaration){
                                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                                 this.ShowInstallationSave = true;                                                                                        
                                                                            }                                                                                    
                                                                        }
                                                                        else {
                                                                            this.ShowInstallationSave = true;
                                                                        }
                                                                    }
                                                                    
                                                                }
                                                                else {
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.shadingDeclaration){
                                                                if(jobStatus.shadingDeclarationSigned){
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    
                                                }
                                                else {
                                                    if(jobStatus.feedInTariff){
                                                        if(jobStatus.feedInTariffSigned){
                                                            if(jobStatus.shadingDeclaration){
                                                                if(jobStatus.shadingDeclarationSigned){
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.shadingDeclaration){
                                                            if(jobStatus.shadingDeclarationSigned){
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                           
                                        }
                                        else{
                                            if(jobStatus.exportControlForm){
                                                if(jobStatus.exportControlFormSigned){
                                                    if(jobStatus.feedInTariff){
                                                        if(jobStatus.feedInTariffSigned){
                                                            if(jobStatus.shadingDeclaration){
                                                                if(jobStatus.shadingDeclarationSigned){
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.shadingDeclaration){
                                                            if(jobStatus.shadingDeclarationSigned){
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                    }
                                                }
                                                
                                            }
                                            else {
                                                if(jobStatus.feedInTariff){
                                                    if(jobStatus.feedInTariffSigned){
                                                        if(jobStatus.shadingDeclaration){
                                                            if(jobStatus.shadingDeclarationSigned){
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                    }
                                                    
                                                }
                                                else {
                                                    if(jobStatus.shadingDeclaration){
                                                        if(jobStatus.shadingDeclarationSigned){
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.efficiencyDeclaration){
                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                 this.ShowInstallationSave = true;                                                                                        
                                                            }                                                                                    
                                                        }
                                                        else {
                                                            this.ShowInstallationSave = true;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    
                                }
                                else{
                                    if(jobStatus.quote){
                                        if(jobStatus.quoteSigned){
                                            if(jobStatus.exportControlForm){
                                                if(jobStatus.exportControlFormSigned){
                                                    if(jobStatus.feedInTariff){
                                                        if(jobStatus.feedInTariffSigned){
                                                            if(jobStatus.shadingDeclaration){
                                                                if(jobStatus.shadingDeclarationSigned){
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.shadingDeclaration){
                                                            if(jobStatus.shadingDeclarationSigned){
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                    }
                                                }
                                                
                                            }
                                            else {
                                                if(jobStatus.feedInTariff){
                                                    if(jobStatus.feedInTariffSigned){
                                                        if(jobStatus.shadingDeclaration){
                                                            if(jobStatus.shadingDeclarationSigned){
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                    }
                                                    
                                                }
                                                else {
                                                    if(jobStatus.shadingDeclaration){
                                                        if(jobStatus.shadingDeclarationSigned){
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.efficiencyDeclaration){
                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                 this.ShowInstallationSave = true;                                                                                        
                                                            }                                                                                    
                                                        }
                                                        else {
                                                            this.ShowInstallationSave = true;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                       
                                    }
                                    else{
                                        if(jobStatus.exportControlForm){
                                            if(jobStatus.exportControlFormSigned){
                                                if(jobStatus.feedInTariff){
                                                    if(jobStatus.feedInTariffSigned){
                                                        if(jobStatus.shadingDeclaration){
                                                            if(jobStatus.shadingDeclarationSigned){
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                    }
                                                    
                                                }
                                                else {
                                                    if(jobStatus.shadingDeclaration){
                                                        if(jobStatus.shadingDeclarationSigned){
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.efficiencyDeclaration){
                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                 this.ShowInstallationSave = true;                                                                                        
                                                            }                                                                                    
                                                        }
                                                        else {
                                                            this.ShowInstallationSave = true;
                                                        }
                                                    }
                                                }
                                            }
                                            
                                        }
                                        else {
                                            if(jobStatus.feedInTariff){
                                                if(jobStatus.feedInTariffSigned){
                                                    if(jobStatus.shadingDeclaration){
                                                        if(jobStatus.shadingDeclarationSigned){
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.efficiencyDeclaration){
                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                 this.ShowInstallationSave = true;                                                                                        
                                                            }                                                                                    
                                                        }
                                                        else {
                                                            this.ShowInstallationSave = true;
                                                        }
                                                    }
                                                }
                                                
                                            }
                                            else {
                                                if(jobStatus.shadingDeclaration){
                                                    if(jobStatus.shadingDeclarationSigned){
                                                        if(jobStatus.efficiencyDeclaration){
                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                 this.ShowInstallationSave = true;                                                                                        
                                                            }                                                                                    
                                                        }
                                                        else {
                                                            this.ShowInstallationSave = true;
                                                        }
                                                    }
                                                    
                                                }
                                                else {
                                                    if(jobStatus.efficiencyDeclaration){
                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                             this.ShowInstallationSave = true;                                                                                        
                                                        }                                                                                    
                                                    }
                                                    else {
                                                        this.ShowInstallationSave = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            
                        }
                        else{
                            if(jobStatus.paymentType && jobStatus.payementoptionid == 1){
                                if(jobStatus.paymentVerified){
                                    if(jobStatus.quote){
                                        if(jobStatus.quoteSigned){
                                            if(jobStatus.exportControlForm){
                                                if(jobStatus.exportControlFormSigned){
                                                    if(jobStatus.feedInTariff){
                                                        if(jobStatus.feedInTariffSigned){
                                                            if(jobStatus.shadingDeclaration){
                                                                if(jobStatus.shadingDeclarationSigned){
                                                                    if(jobStatus.efficiencyDeclaration){
                                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                                             this.ShowInstallationSave = true;                                                                                        
                                                                        }                                                                                    
                                                                    }
                                                                    else {
                                                                        this.ShowInstallationSave = true;
                                                                    }
                                                                }
                                                                
                                                            }
                                                            else {
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.shadingDeclaration){
                                                            if(jobStatus.shadingDeclarationSigned){
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                    }
                                                }
                                                
                                            }
                                            else {
                                                if(jobStatus.feedInTariff){
                                                    if(jobStatus.feedInTariffSigned){
                                                        if(jobStatus.shadingDeclaration){
                                                            if(jobStatus.shadingDeclarationSigned){
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                    }
                                                    
                                                }
                                                else {
                                                    if(jobStatus.shadingDeclaration){
                                                        if(jobStatus.shadingDeclarationSigned){
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.efficiencyDeclaration){
                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                 this.ShowInstallationSave = true;                                                                                        
                                                            }                                                                                    
                                                        }
                                                        else {
                                                            this.ShowInstallationSave = true;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                       
                                    }
                                    else{
                                        if(jobStatus.exportControlForm){
                                            if(jobStatus.exportControlFormSigned){
                                                if(jobStatus.feedInTariff){
                                                    if(jobStatus.feedInTariffSigned){
                                                        if(jobStatus.shadingDeclaration){
                                                            if(jobStatus.shadingDeclarationSigned){
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                    }
                                                    
                                                }
                                                else {
                                                    if(jobStatus.shadingDeclaration){
                                                        if(jobStatus.shadingDeclarationSigned){
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.efficiencyDeclaration){
                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                 this.ShowInstallationSave = true;                                                                                        
                                                            }                                                                                    
                                                        }
                                                        else {
                                                            this.ShowInstallationSave = true;
                                                        }
                                                    }
                                                }
                                            }
                                            
                                        }
                                        else {
                                            if(jobStatus.feedInTariff){
                                                if(jobStatus.feedInTariffSigned){
                                                    if(jobStatus.shadingDeclaration){
                                                        if(jobStatus.shadingDeclarationSigned){
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.efficiencyDeclaration){
                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                 this.ShowInstallationSave = true;                                                                                        
                                                            }                                                                                    
                                                        }
                                                        else {
                                                            this.ShowInstallationSave = true;
                                                        }
                                                    }
                                                }
                                                
                                            }
                                            else {
                                                if(jobStatus.shadingDeclaration){
                                                    if(jobStatus.shadingDeclarationSigned){
                                                        if(jobStatus.efficiencyDeclaration){
                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                 this.ShowInstallationSave = true;                                                                                        
                                                            }                                                                                    
                                                        }
                                                        else {
                                                            this.ShowInstallationSave = true;
                                                        }
                                                    }
                                                    
                                                }
                                                else {
                                                    if(jobStatus.efficiencyDeclaration){
                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                             this.ShowInstallationSave = true;                                                                                        
                                                        }                                                                                    
                                                    }
                                                    else {
                                                        this.ShowInstallationSave = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                
                            }
                            else{
                                if(jobStatus.quote){
                                    if(jobStatus.quoteSigned){
                                        if(jobStatus.exportControlForm){
                                            if(jobStatus.exportControlFormSigned){
                                                if(jobStatus.feedInTariff){
                                                    if(jobStatus.feedInTariffSigned){
                                                        if(jobStatus.shadingDeclaration){
                                                            if(jobStatus.shadingDeclarationSigned){
                                                                if(jobStatus.efficiencyDeclaration){
                                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                                         this.ShowInstallationSave = true;                                                                                        
                                                                    }                                                                                    
                                                                }
                                                                else {
                                                                    this.ShowInstallationSave = true;
                                                                }
                                                            }
                                                            
                                                        }
                                                        else {
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                    }
                                                    
                                                }
                                                else {
                                                    if(jobStatus.shadingDeclaration){
                                                        if(jobStatus.shadingDeclarationSigned){
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.efficiencyDeclaration){
                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                 this.ShowInstallationSave = true;                                                                                        
                                                            }                                                                                    
                                                        }
                                                        else {
                                                            this.ShowInstallationSave = true;
                                                        }
                                                    }
                                                }
                                            }
                                            
                                        }
                                        else {
                                            if(jobStatus.feedInTariff){
                                                if(jobStatus.feedInTariffSigned){
                                                    if(jobStatus.shadingDeclaration){
                                                        if(jobStatus.shadingDeclarationSigned){
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.efficiencyDeclaration){
                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                 this.ShowInstallationSave = true;                                                                                        
                                                            }                                                                                    
                                                        }
                                                        else {
                                                            this.ShowInstallationSave = true;
                                                        }
                                                    }
                                                }
                                                
                                            }
                                            else {
                                                if(jobStatus.shadingDeclaration){
                                                    if(jobStatus.shadingDeclarationSigned){
                                                        if(jobStatus.efficiencyDeclaration){
                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                 this.ShowInstallationSave = true;                                                                                        
                                                            }                                                                                    
                                                        }
                                                        else {
                                                            this.ShowInstallationSave = true;
                                                        }
                                                    }
                                                    
                                                }
                                                else {
                                                    if(jobStatus.efficiencyDeclaration){
                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                             this.ShowInstallationSave = true;                                                                                        
                                                        }                                                                                    
                                                    }
                                                    else {
                                                        this.ShowInstallationSave = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                   
                                }
                                else{
                                    if(jobStatus.exportControlForm){
                                        if(jobStatus.exportControlFormSigned){
                                            if(jobStatus.feedInTariff){
                                                if(jobStatus.feedInTariffSigned){
                                                    if(jobStatus.shadingDeclaration){
                                                        if(jobStatus.shadingDeclarationSigned){
                                                            if(jobStatus.efficiencyDeclaration){
                                                                if(jobStatus.efficiencyDeclarationSigned){
                                                                     this.ShowInstallationSave = true;                                                                                        
                                                                }                                                                                    
                                                            }
                                                            else {
                                                                this.ShowInstallationSave = true;
                                                            }
                                                        }
                                                        
                                                    }
                                                    else {
                                                        if(jobStatus.efficiencyDeclaration){
                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                 this.ShowInstallationSave = true;                                                                                        
                                                            }                                                                                    
                                                        }
                                                        else {
                                                            this.ShowInstallationSave = true;
                                                        }
                                                    }
                                                }
                                                
                                            }
                                            else {
                                                if(jobStatus.shadingDeclaration){
                                                    if(jobStatus.shadingDeclarationSigned){
                                                        if(jobStatus.efficiencyDeclaration){
                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                 this.ShowInstallationSave = true;                                                                                        
                                                            }                                                                                    
                                                        }
                                                        else {
                                                            this.ShowInstallationSave = true;
                                                        }
                                                    }
                                                    
                                                }
                                                else {
                                                    if(jobStatus.efficiencyDeclaration){
                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                             this.ShowInstallationSave = true;                                                                                        
                                                        }                                                                                    
                                                    }
                                                    else {
                                                        this.ShowInstallationSave = true;
                                                    }
                                                }
                                            }
                                        }
                                        
                                    }
                                    else {
                                        if(jobStatus.feedInTariff){
                                            if(jobStatus.feedInTariffSigned){
                                                if(jobStatus.shadingDeclaration){
                                                    if(jobStatus.shadingDeclarationSigned){
                                                        if(jobStatus.efficiencyDeclaration){
                                                            if(jobStatus.efficiencyDeclarationSigned){
                                                                 this.ShowInstallationSave = true;                                                                                        
                                                            }                                                                                    
                                                        }
                                                        else {
                                                            this.ShowInstallationSave = true;
                                                        }
                                                    }
                                                    
                                                }
                                                else {
                                                    if(jobStatus.efficiencyDeclaration){
                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                             this.ShowInstallationSave = true;                                                                                        
                                                        }                                                                                    
                                                    }
                                                    else {
                                                        this.ShowInstallationSave = true;
                                                    }
                                                }
                                            }
                                            
                                        }
                                        else {
                                            if(jobStatus.shadingDeclaration){
                                                if(jobStatus.shadingDeclarationSigned){
                                                    if(jobStatus.efficiencyDeclaration){
                                                        if(jobStatus.efficiencyDeclarationSigned){
                                                             this.ShowInstallationSave = true;                                                                                        
                                                        }                                                                                    
                                                    }
                                                    else {
                                                        this.ShowInstallationSave = true;
                                                    }
                                                }
                                                
                                            }
                                            else {
                                                if(jobStatus.efficiencyDeclaration){
                                                    if(jobStatus.efficiencyDeclarationSigned){
                                                         this.ShowInstallationSave = true;                                                                                        
                                                    }                                                                                    
                                                }
                                                else {
                                                    this.ShowInstallationSave = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
        }); 
    }

    downloadItemDatasheet(datasheetUrl: any) : void {
        let _fileName = AppConsts.docUrl + datasheetUrl;
        window.open(_fileName, "_blank");
    }

    checkProductWarrantyFeaturesAndPermission() {
        return this.feature.isEnabled("App.StockManagement.ProductItems.NetSituation") && this.permission.isGranted("Pages.LeadDetails.CheckWarranty");
    }
}