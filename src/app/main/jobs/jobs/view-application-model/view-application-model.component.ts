import { Component, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLookupServiceProxy, CreateOrEditJobDto, DeclarationFormServiceProxy, GetDeclarationFormForViewDto, GetJobAcnoForViewDto, GetQuotationForViewDto, JobAcknowledgementServiceProxy, JobApplicationViewDto, JobsServiceProxy, LeadDto, QuotationsServiceProxy, ReportServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import * as _ from 'lodash';
import * as moment from 'moment';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ViewQuoteModalComponent } from '../viewquotation.component';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'viewApplicationModal',
  templateUrl: './view-application-model.component.html',
  styleUrls: ['./view-application-model.component.css']
})
export class ViewApplicationModelComponent extends AppComponentBase {
  @ViewChild('viewModel', { static: true }) modal: ModalDirective;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  active = false;
  jobid: number;
  finance = false;
  invoiceinstaller=0;
  applicationview: JobApplicationViewDto = new JobApplicationViewDto();
  job: CreateOrEditJobDto = new CreateOrEditJobDto();
  exportControl: GetJobAcnoForViewDto[];
  feedInTariif: GetJobAcnoForViewDto[];
  shadingDeclaration : GetDeclarationFormForViewDto[];
  efficiencyDeclaration : GetDeclarationFormForViewDto[];

  mapSrc: any = '';

  billToPay: number = 0;
  JobQuotations: GetQuotationForViewDto[];
  @ViewChild('viewQuoteModel', { static: true }) viewQuoteModel: ViewQuoteModalComponent;
  
  constructor(
    injector: Injector,
    private _jobsServiceProxy: JobsServiceProxy,
    private spinner: NgxSpinnerService,
    private _quotationsServiceProxy: QuotationsServiceProxy,
    private _jobAcknowledgementServiceProxy: JobAcknowledgementServiceProxy,
    private _declarationFormServiceProxy : DeclarationFormServiceProxy,
    private _reportServiceProxy: ReportServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
    ) {
    super(injector);
  }

  show(jobid: number, trackerid?: number,section = ''): void {
    debugger;
    this.spinner.show();
    let log = new UserActivityLogDto();
    log.actionId = 79;
    log.actionNote ='View job Detail';
    log.section = section;
    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {
    });
    this._jobsServiceProxy.getDataForQuickViewByJobID(jobid,trackerid).subscribe(result => {
      this.applicationview = result;
      this.invoiceinstaller=trackerid;

      this.job = result.job;

      if (this.job.solarVICRebate != null && this.job.solarVICLoanDiscont != null) {
        let totalCostafterdic = (this.job.totalCost) - (this.job.solarVICRebate + this.job.solarVICLoanDiscont)
        if (this.job.depositRequired < 0) {
            this.job.depositRequired = Math.round(totalCostafterdic * 10 / 100);
        }
        this.billToPay = (totalCostafterdic) - (this.job.depositRequired);
      } else {
          if (this.job.depositRequired < 0) {
              this.job.depositRequired = Math.round(this.job.totalCost * 10 / 100);
          }
          this.billToPay = (this.job.totalCost) - (this.job.depositRequired);
      }

      if(this.applicationview.job.latitude != null && this.applicationview.job.longitude != null)
      {
        this.mapSrc = "https://maps.google.com/?q=" + this.applicationview.job.latitude + "," + this.applicationview.job.longitude
      }

      if (trackerid == 3) {
        this.finance = true;
      } else {
        this.finance = false;
      }
      this.getJobQuotations(jobid);
      this.getExportControl(jobid);
      this.getFeedInTarrif(jobid);
      this.getShadingDeclaration(jobid);
      this.getEfficiencyDeclaration(jobid);

      this.modal.show();
      this.spinner.hide();
    });

  }
  downloadfile(file): void {
     debugger
    let FileName = AppConsts.docUrl + "/" + file.documentPath + file.fileName;
    window.open(FileName, "_blank");

  }
  downloadfile1(file): void {
    debugger
      let FileName = AppConsts.docUrl + "/" + file.filePathinv + file.filenameinv;
    window.open(FileName, "_blank");

  }
  downloadaprovefile(file): void {
    debugger
      let FileName = AppConsts.docUrl + "/" + file.gridFilePath + file.gridFilename;
    window.open(FileName, "_blank");

    // var date = moment(file.creationTime).toDate()
    //     var MigrationDate = new Date('2022/09/17')

    //     if(this.lead.organizationId == 8 || this.lead.organizationId == 7)
    //     {
    //         if(date < MigrationDate)
    //         {
    //             let FileName = AppConsts.oldDocUrl + "/" + file.documentPath + file.fileName;
    //             window.open(FileName, "_blank");
    //         }
    //     }
    //     else{
    //         let FileName = AppConsts.docUrl + "/" + file.documentPath + file.fileName;
    //         window.open(FileName, "_blank");
    //     }
  }

  close(): void {
    this.active = false;
    this.modal.hide();
  }

  getJobQuotations(jobid : number): void {
    this._quotationsServiceProxy.getAll(jobid, '', 0, 9999).subscribe(result => {

        this.JobQuotations = result.items;
        // if (result.items.length > 0) {
        //     this.reportPath = this.JobQuotations[0].quotation.quoteFilePath;
        //     this.docavailable = true;
        // } else {
        //     this.docavailable = false;
        // }
    })
  }
  downloadSignQuotefile(file): void {

    var Qdate = moment(file.quotation.quoteDate).toDate()
    var MigrationDate = new Date('2022/09/17')

    if(this.job.orgid == 7) {
        MigrationDate = new Date('2023/01/04')
    }

    if(this.job.orgid == 8 || this.job.orgid == 7)
    {
        if(Qdate < MigrationDate)
        {
            let FileName = AppConsts.oldDocUrl + "/" + file.quotation.quoteFilePath + file.quotation.signedQuoteFileName;
            window.open(FileName, "_blank");
        }
    }
    else{
        let FileName = AppConsts.docUrl + "/" + file.quotation.quoteFilePath + file.quotation.signedQuoteFileName;
        window.open(FileName, "_blank");
    }
  };
  downloadQuotefile(file): void {

    var Qdate = moment(file.quotation.quoteDate).toDate()
  
    var MigrationDate = new Date('2022/09/17')

    if(this.job.orgid == 7) {
      MigrationDate = new Date('2023/01/04')
    }

    if(this.job.orgid == 8 || this.job.orgid == 7)
    {
      if(Qdate < MigrationDate)
      {
          let FileName = AppConsts.oldDocUrl + "/" + file.quotation.quoteFilePath + file.quotation.quoteFileName;
          window.open(FileName, "_blank");
      }
    }
    else {
      let FileName = AppConsts.docUrl + "/" + file.quotation.quoteFilePath + file.quotation.quoteFileName;
      window.open(FileName, "_blank");
    }
  };
  getExportControl(jobid : number): void {
    this._jobAcknowledgementServiceProxy.getAll(jobid, "Export Control", '', 0, 9999).subscribe(result => {
        this.exportControl = result.items;
    });
  }

  getFeedInTarrif(jobid : number): void {
    this._jobAcknowledgementServiceProxy.getAll(jobid, "Feed In Tariff", '', 0, 9999).subscribe(result => {
        this.feedInTariif = result.items;
    });
  }
  getShadingDeclaration(jobid : number): void {
    this._declarationFormServiceProxy.getAll(jobid, "Shading Declaration", '', 0, 9999).subscribe(result => {
        this.shadingDeclaration = result.items;
    });
  }

  getEfficiencyDeclaration(jobid : number): void {
    this._declarationFormServiceProxy.getAll(jobid, "Efficiency Declaration", '', 0, 9999).subscribe(result => {
        this.efficiencyDeclaration = result.items;
    });
  }

  acknowledgement(id, date): void {
        
    this.spinnerService.show();
    this._reportServiceProxy.getAcknowledgementById(id).subscribe(result => {
      let htmlTemplate = this.getAcknoHtml(result, date)

      this._commonLookupService.downloadPdf(result.jobNumber, result.docType, htmlTemplate).subscribe(re => {
          window.open(re, "_blank");
          this.spinnerService.hide();
      }, 
      e => 
      { 
          this.spinnerService.hide(); 
      });
    }, err => {this.spinnerService.hide();});
  }

  getAcknoHtml(result, date) {
    let htmlstring = result.viewHtml;
    let myTemplateStr = htmlstring;

    var QDate = moment(date).toDate()
    var MigrationDate = new Date('2023/01/04')

    debugger
    let appUrl = AppConsts.docUrl;
    if(this.job.orgid == 7)
    {
        if(QDate < MigrationDate)
        {
            appUrl = AppConsts.oldDocUrl;
        }
    }

    let myVariables = {
        A: {
            Name: result.name,
            AddressLine1: result.address1,
            AddressLine2: result.address2,
            Mobile: result.mobile,
            Email: result.email,
            JobNumber: result.jobNumber,
            Kw: result.kw,
            Y: AppConsts.appBaseUrl + (result.yn == true ? '/assets/common/images/tick.png' : '/assets/common/images/untick.png' ),
            N: AppConsts.appBaseUrl + (result.yn == false ? '/assets/common/images/tick.png' : '/assets/common/images/untick.png'),

            CustomerSign: {
                SignSrc: result.signSrc == null ? "" : appUrl + result.signSrc,
                Date: result.signDate
            }
        },
  
        O: {
          OrgLogo: AppConsts.docUrl + result.orgLogo
        }
    };

    // use custom delimiter {{ }}
    _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;

    // interpolate
    let compiled = _.template(myTemplateStr);
    let myTemplateCompiled = compiled(myVariables);
    return myTemplateCompiled;
  }

  declaration(id, date): void {
        
    this.spinnerService.show();
    this._reportServiceProxy.getDeclarationById(id).subscribe(result => {
        let htmlTemplate = this.getDeclarationHtml(result, date)

        this._commonLookupService.downloadPdf(result.jobNumber, result.docType, htmlTemplate).subscribe(re => {
            window.open(re, "_blank");
            this.spinnerService.hide();
        }, 
        e => 
        { 
            this.spinnerService.hide(); 
        });
    }, err => {this.spinnerService.hide();});
  }

  getDeclarationHtml(result, date) {
    let htmlstring = result.viewHtml;
    let myTemplateStr = htmlstring;

    var QDate = moment(date).toDate()
    var MigrationDate = new Date('2023/01/04')

    let myVariables = {
        A: {
            Name: result.name,
            AddressLine1: result.address1,
            AddressLine2: result.address2,
            Mobile: result.mobile,
            Email: result.email,
            JobNumber: result.jobNumber,
            State: result.state,
            CustomerSign: {
                SignSrc: result.signSrc == null ? "" : AppConsts.docUrl + result.signSrc,
                Date: result.signDate
            }
        },
  
        O: {
          OrgLogo: AppConsts.docUrl + result.orgLogo
        }
    };

    // use custom delimiter {{ }}
    _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;

    // interpolate
    let compiled = _.template(myTemplateStr);
    let myTemplateCompiled = compiled(myVariables);
    return myTemplateCompiled;
}

}
