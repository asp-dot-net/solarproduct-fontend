import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { CommonLookupDto, JobJobStatusLookupTableDto, JobsServiceProxy, UpdateJobStatusDto, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'jobStatusModal',
    templateUrl: './job-status.component.html'
})

export class JobStatusModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    saving = false;
    jobId: number;
    jobStatusIds: JobJobStatusLookupTableDto[];
    jobStatus: string;
    jobChangeStatusId: number;
    updateJobStatus: UpdateJobStatusDto = new UpdateJobStatusDto();
    sampleDate: moment.Moment;
    cancelReasons: CommonLookupDto[];
    holdReasons: CommonLookupDto[];
    SectionName = '';
    sectionId = 0;
    constructor(
        injector: Injector,
        private _jobServiceProxy: JobsServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private spinner: NgxSpinnerService
    ) {
        super(injector);
    }

    show(jobId: number, jobStatusChangeId: number, jobinstalldate?: moment.Moment, Section? : string, sectionId = 0): void {
        debugger;
        this.jobId = jobId;
        this.SectionName = Section;
        this.sectionId  = sectionId;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote = 'Open Job Status';
        log.section = Section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
        this.updateJobStatus.activeStatusNotes = '';
        this.updateJobStatus.depositeStatusNotes = '';
        this.updateJobStatus.id = null;
        this.updateJobStatus.isJobCancelRequest = null;
        this.updateJobStatus.isRefund = null;
        this.updateJobStatus.jobCancelReason = '';
        this.updateJobStatus.jobCancelReasonId = undefined;
        this.updateJobStatus.jobCancelRequestReason = '';
        this.updateJobStatus.jobHoldReason = '';
        this.updateJobStatus.jobHoldReasonId = null;
        this.updateJobStatus.jobStatusId = null;
        this.updateJobStatus.nextFollowUpDate = null;

        this.updateJobStatus.id = jobId;
        this.updateJobStatus.jobStatusId = jobStatusChangeId;
        this._jobServiceProxy.getAllJobStatusForTableDropdown().subscribe(result => {
            this.jobStatusIds = result;
        });
        this._jobServiceProxy.getAllCancelReasonForTableDropdown().subscribe(result => {
            this.cancelReasons = result;
        });
        this._jobServiceProxy.getAllHoldReasonForTableDropdown().subscribe(result => {
            this.holdReasons = result;
        });
        this.spinner.show();
        this._jobServiceProxy.getJobStatus(jobId).subscribe(result => {
            this.jobStatus = result;
            if(jobStatusChangeId == 3 && jobinstalldate != null)
            {
                this.message.error("Please Remove Installation Date");
            }
            else {
                this.modal.show();
                this.spinner.hide();
            }
        });
    }

    save(): void {
        debugger;
        this.saving = true;
        this.updateJobStatus.nextFollowUpDate = this.sampleDate;
        this.updateJobStatus.sectionName = this.SectionName;
        this.updateJobStatus.sectionId = this.sectionId;
        this._jobServiceProxy.updateJobStatus(this.updateJobStatus)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.modal.hide();
    }
}