import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobSmsEmailModelComponent } from './job-sms-email-model.component';

describe('JobSmsEmailModelComponent', () => {
  let component: JobSmsEmailModelComponent;
  let fixture: ComponentFixture<JobSmsEmailModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobSmsEmailModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobSmsEmailModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
