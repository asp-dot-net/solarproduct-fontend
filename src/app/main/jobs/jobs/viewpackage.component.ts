import { Component, ViewChild, Injector, Output, EventEmitter, OnInit, ElementRef} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { ProductPackageServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
// import { NgxSpinnerService } from 'ngx-spinner';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import * as _ from 'lodash';
import { AppConsts } from '@shared/AppConsts';

@Component({
    selector: 'viewPackegeModel',
    templateUrl: './viewpackage.component.html'
})

export class ViewPackageModalComponent extends AppComponentBase implements OnInit {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    productPackages: any[];

    saving = false;
   SectionName = '';
    ngOnInit(): void {
        
    }

    constructor(
        
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _productPackageServiceProxy: ProductPackageServiceProxy
    ) {
        super(injector);
    }

    show(Section = ''): void {
        this.SectionName = Section;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote = 'View Package';
        log.section = Section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
        this.spinnerService.show()
        this.productPackages = [];
        this._productPackageServiceProxy.getAllPackageDetails().subscribe(result => {
            this.productPackages = result;
            this.modal.show();
            this.spinnerService.hide();
        },
        error => {
            this.spinnerService.hide();
        });
    }

    close(): void {
        this.modal.hide();
    }

    expandcollapse(index,name): void {
        var divName = 'div' + index;
        var iName = 'i' + index;
        var div = document.getElementById(divName);
        var i = document.getElementById(iName);

        if (div.style.display == "none") {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote = 'Expanded Package ' + name;
            log.section = this.SectionName;
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
            }); 
            div.style.display = "inline";
            i.className = 'las la-chevron-circle-down fs-24';
        } else {
            div.style.display = "none";
            i.className = 'las la-chevron-circle-right fs-24';
        }
    }

    select(id): void {
        this.modalSave.emit(id);
        this.modal.hide();
    }
}