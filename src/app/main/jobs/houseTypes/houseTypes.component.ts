﻿import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { HouseTypesServiceProxy, HouseTypeDto,UserActivityLogServiceProxy,UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CreateOrEditHouseTypeModalComponent } from './create-or-edit-houseType-modal.component';
import { finalize } from 'rxjs/operators';
import { ViewHouseTypeModalComponent } from './view-houseType-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import { Title } from '@angular/platform-browser';

@Component({
    templateUrl: './houseTypes.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class HouseTypesComponent extends AppComponentBase {

    show: boolean = true;
    showchild: boolean = true;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };
    firstrowcount = 0;
    last = 0;
    @ViewChild('createOrEditHouseTypeModal', { static: true }) createOrEditHouseTypeModal: CreateOrEditHouseTypeModalComponent;
    @ViewChild('viewHouseTypeModalComponent', { static: true }) viewHouseTypeModal: ViewHouseTypeModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    advancedFiltersAreShown = false;
    filterText = '';
    nameFilter = '';
    maxDisplayOrderFilter: number;
    maxDisplayOrderFilterEmpty: number;
    minDisplayOrderFilter: number;
    minDisplayOrderFilterEmpty: number;
    public screenHeight: any;  
    testHeight = 250;
    constructor(
        injector: Injector,
        private _houseTypesServiceProxy: HouseTypesServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private titleService: Title
        ) {
            super(injector);
            this.titleService.setTitle(this.appSession.tenancyName + " |  House Types");
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open House Types';
        log.section = 'House Types';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
    }
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }
    getHouseTypes(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();
        this._houseTypesServiceProxy.getAll(
            this.filterText,
            this.nameFilter,
            this.maxDisplayOrderFilter == null ? this.maxDisplayOrderFilterEmpty : this.maxDisplayOrderFilter,
            this.minDisplayOrderFilter == null ? this.minDisplayOrderFilterEmpty : this.minDisplayOrderFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }
    searchLog() : void {
        debugger;
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'House Types';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }
    createHouseType(): void {
        this.createOrEditHouseTypeModal.show();
    }
    deleteHouseType(houseType: HouseTypeDto): void {
        this.message.confirm(
            this.l('AreYouSureWanttoDelete'),
            this.l('Delete'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._houseTypesServiceProxy.delete(houseType.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete House Types: ' + houseType.name;
                            log.section = 'House Types';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            }); 
                        });
                }
            }
        );
    }

    exportToExcel(): void {
        this._houseTypesServiceProxy.getHouseTypesToExcel(
            this.filterText,
            this.nameFilter,
            this.maxDisplayOrderFilter == null ? this.maxDisplayOrderFilterEmpty : this.maxDisplayOrderFilter,
            this.minDisplayOrderFilter == null ? this.minDisplayOrderFilterEmpty : this.minDisplayOrderFilter,
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }
}
