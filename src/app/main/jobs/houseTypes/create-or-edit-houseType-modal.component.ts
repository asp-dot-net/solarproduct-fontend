﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { HouseTypesServiceProxy, CreateOrEditHouseTypeDto ,UserActivityLogServiceProxy,UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
@Component({
    selector: 'createOrEditHouseTypeModal',
    templateUrl: './create-or-edit-houseType-modal.component.html'
})
export class CreateOrEditHouseTypeModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    saving = false;
    houseType: CreateOrEditHouseTypeDto = new CreateOrEditHouseTypeDto();
    constructor(
        injector: Injector,
        private _houseTypesServiceProxy: HouseTypesServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
    }
    show(houseTypeId?: number): void {
        if (!houseTypeId) {
            this.houseType = new CreateOrEditHouseTypeDto();
            this.houseType.id = houseTypeId;
            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New House Types';
            log.section = 'House Types';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 

        } else {
            this._houseTypesServiceProxy.getHouseTypeForEdit(houseTypeId).subscribe(result => {
                this.houseType = result.houseType;
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit House Types : ' + this.houseType.name;
                log.section = 'House Types';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            });
        }
    }
    onShown(): void {
        document.getElementById('HouseType_Name').focus();
    }
    save(): void {
            this.saving = true;
            this._houseTypesServiceProxy.createOrEdit(this.houseType)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.houseType.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='House Types Updated : '+ this.houseType.name;
                    log.section = 'House Types';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='House Types Created : '+ this.houseType.name;
                    log.section = 'House Types';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
