﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetJobRefundForViewDto, JobRefundDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewJobRefundModal',
    templateUrl: './view-jobRefund-modal.component.html'
})
export class ViewJobRefundModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    saving = false;

    item: GetJobRefundForViewDto;


    constructor(
        injector: Injector
    ) {
        super(injector);
        this.item = new GetJobRefundForViewDto();
        this.item.jobRefund = new JobRefundDto();
    }

    show(item: GetJobRefundForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
