﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit, Input } from '@angular/core';
import { finalize } from 'rxjs/operators';
import {
    JobRefundsServiceProxy, CreateOrEditJobRefundDto, JobRefundPaymentOptionLookupTableDto
    
    , JobRefundRefundReasonLookupTableDto,
    UserActivityLogDto,
    UserActivityLogServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
    selector: 'createOrEditJobRefundModal',
    templateUrl: './create-or-edit-jobRefund-modal.component.html'
})
export class CreateOrEditJobRefundModalComponent extends AppComponentBase implements OnInit {

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Input() jobId: number;
    active = false;
    saving = false;
    jobRefund: CreateOrEditJobRefundDto = new CreateOrEditJobRefundDto();
    paymentOptionName = '';
    jobNote = '';
    refundReasonName = '';
    allPaymentOptions: JobRefundPaymentOptionLookupTableDto[];
    // allJobs: JobRefundJobLookupTableDto[];
    allRefundReasons: JobRefundRefundReasonLookupTableDto[];
    leadCompanyName: any;
    jobNumber: any;
    sectionName = '';
    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _jobRefundsServiceProxy: JobRefundsServiceProxy
    ) {
        super(injector);
    }

    show(jobId: number, jobRefundId?: number, Section = ''): void {
        this.sectionName = Section;
        this.jobId = jobId;
        let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote = !jobRefundId ? 'Open For Edit Job Refund' : 'Open For Create New Job Refund';
            log.section = Section;
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
        if (!jobRefundId) {
            this.jobRefund = new CreateOrEditJobRefundDto();
            this.jobRefund.id = jobRefundId;
            this.active = true;
            this.modal.show();
        } else {
            this._jobRefundsServiceProxy.getJobRefundForEdit(jobRefundId).subscribe(result => {
                this.jobRefund = result.jobRefund;
                this.leadCompanyName = result.leadCompanyName;
                this.jobNumber = result.jobNumber;
                this.active = true;
                this.modal.show();
            });
        }
    }

    ngOnInit(): void {
        this._jobRefundsServiceProxy.getAllPaymentOptionForTableDropdown().subscribe(result => {
            this.allPaymentOptions = result;
        });
        // this._jobRefundsServiceProxy.getAllJobForTableDropdown().subscribe(result => {
        //     this.allJobs = result;
        // });
        this._jobRefundsServiceProxy.getAllRefundReasonForTableDropdown().subscribe(result => {
            this.allRefundReasons = result;
        });

    }

    save(): void {
        this.saving = true;
        this.jobRefund.jobId = this.jobId;
        this._jobRefundsServiceProxy.createOrEdit(this.jobRefund)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                let log = new UserActivityLogDto();
                    log.actionId = this.jobRefund.id > 0 || this.jobRefund.refundReasonId == 1 ? 15 : 14;
                    log.actionNote = this.jobRefund.id > 0 || this.jobRefund.refundReasonId == 1 ? 'Job Refund Request Completed' : 'Job Status Change To Cancel';
                    log.section = this.sectionName;
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                this.close();
                this.notify.info(this.l('SavedSuccessfully'));
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.modal.hide();
    }

    req : boolean;
    checkReq(event) : void {
        if(event.target.value == "CC") {
            this.req = false;
        }
        else {
            this.req = true;
        }
    }
}
