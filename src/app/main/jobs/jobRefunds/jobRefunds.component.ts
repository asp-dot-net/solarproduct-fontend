﻿import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JobRefundsServiceProxy, JobRefundDto, UserServiceProxy, OrganizationUnitDto, GetJobForViewDto, JobsServiceProxy, LeadsServiceProxy, LeadUsersLookupTableDto, LeadStateLookupTableDto, JobRefundRefundReasonLookupTableDto, JobStatusTableDto, CommonLookupServiceProxy, JobTrackerServiceProxy, ReportServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { CreateOrEditJobRefundModalComponent } from './create-or-edit-jobRefund-modal.component';

import { ViewJobRefundModalComponent } from './view-jobRefund-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { RefundJobModalComponent } from './job-refund-model.component';
import { OnInit } from '@angular/core';
import { ViewApplicationModelComponent } from '../jobs/view-application-model/view-application-model.component';
import { AddActivityModalComponent } from '@app/main/myleads/myleads/add-activity-model.component';
import { JobRefundSmsemailModelComponent } from './job-refund-smsemail-model/job-refund-smsemail-model.component';
import { ViewMyLeadComponent } from '@app/main/myleads/myleads/view-mylead.component';
import { DomSanitizer, Title } from '@angular/platform-browser';
import { CommentModelComponent } from '@app/main/activitylog/comment-modal.component';
import { EmailModelComponent } from '@app/main/activitylog/email-modal.component';
import { NotifyModelComponent } from '@app/main/activitylog/notify-modal.component';
import { ReminderModalComponent } from '@app/main/activitylog/reminder-modal.component';
import { SMSModelComponent } from '@app/main/activitylog/sms-modal.component';
import { ToDoModalComponent } from '@app/main/activitylog/todo-modal.component';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './jobRefunds.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class JobRefundsComponent extends AppComponentBase implements OnInit {  
    @ViewChild('jobrefundModal', { static: true }) jobrefundModal: RefundJobModalComponent;
    // @ViewChild('createOrEditJobRefundModal', { static: true }) createOrEditJobRefundModal: CreateOrEditJobRefundModalComponent;
    @ViewChild('viewJobRefundModalComponent', { static: true }) viewJobRefundModal: ViewJobRefundModalComponent;
    @ViewChild('viewApplicationModal', { static: true }) viewApplicationModal: ViewApplicationModelComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    // @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    // @ViewChild('jobRefundTrackerSmsEmailModel', { static: true }) jobRefundTrackerSmsEmailModel: JobRefundSmsemailModelComponent;
    // @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
    @ViewChild('smsModel', { static: true }) smsModel: SMSModelComponent;
    @ViewChild('emailModel', { static: true }) emailModel: EmailModelComponent;
    @ViewChild('notifyModel', { static: true }) notifyModel: NotifyModelComponent;
    @ViewChild('commentModel', { static: true }) commentModel: CommentModelComponent;
    @ViewChild('todoModal', { static: true }) todoModal: ToDoModalComponent;
    @ViewChild('ReminderModal', { static: true }) ReminderModal: ReminderModalComponent;
    FiltersData = false;
    shouldShow: boolean = false;
    advancedFiltersAreShown = false;
    filterText = '';
    dateType = 'Receive';
    filterName = "JobNumber";

    // maxPaidDateFilter: moment.Moment;
    // minPaidDateFilter: moment.Moment;
    salesRepId = 0;
    // filteredManagers: LeadUsersLookupTableDto[];
    filteredReps: LeadUsersLookupTableDto[];
    organizationUnit = 0;
    applicationstatus = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    isVerify: boolean | null = undefined;

    totalRefunded = 0;
    totalRefundedAmt = 0;
    totalPending = 0;
    totalPendingAmt = 0;
    total = 0;
    totalAmt = 0;

    date = new Date();
    // firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    // startDate = moment(this.firstDay);
    // endDate = moment().endOf('day');
    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);
    alljobstatus: JobStatusTableDto[];
    allRefundReason: JobRefundRefundReasonLookupTableDto[];
    ExpandedView: boolean = true;
    SelectedLeadId: number = 0;
    jobStatusID: [];
    jobRefundID = 0;
    excelorcsvfile = 0;
    organizationUnitlength: number = 0;
    firstrowcount = 0;
    last = 0;
    refundPaymentType = "";
    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 330;
    toggle: boolean = true;
    orgCode = '';
    areaNameFilter = "";
    stateFilter = '';
    allStates: LeadStateLookupTableDto[];
    tableRecords: any;
    count = 0
    saving = false;

    change() {
        this.toggle = !this.toggle;
      }

    constructor(
        injector: Injector,
        private _jobRefundsServiceProxy: JobRefundsServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _userServiceProxy: UserServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _jobTrackerServiceProxy: JobTrackerServiceProxy,
        private _reportServiceProxy: ReportServiceProxy,
        private sanitizer: DomSanitizer,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private titleService: Title
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Job Refunds");
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        this._commonLookupService.getAllJobStatusTableDropdown().subscribe(result => {
            this.alljobstatus = result;
        });
        this._jobRefundsServiceProxy.getAllRefundReasonForTableDropdown().subscribe(result => {
            this.allRefundReason = result;
        });
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            debugger;
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Refund Tracker';
            log.section = 'Refund Tracker';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.orgCode = this.allOrganizationUnits[0].code;
            // this._commonLookupService.getSalesManagerForFilter(this.organizationUnit, undefined).subscribe(manager => {
            //     this.filteredManagers = manager;
            // });
            this._leadsServiceProxy.getSalesRepForFilter(this.organizationUnit, undefined).subscribe(rep => {
                this.filteredReps = rep;
            });
            this.getJobRefunds();
        });
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }
        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }

    getJobRefunds(event?: LazyLoadEvent) {

        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._jobTrackerServiceProxy.getAllRefundTracker(
            this.filterName,
            filterText_,
            this.organizationUnit,
            this.applicationstatus,
            this.salesRepId,
            this.jobStatusID,
            this.jobRefundID,
            this.dateType,
            this.startDate,
            this.endDate,
            this.isVerify,
            this.refundPaymentType,
            this.stateFilter,
            this.areaNameFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            this.shouldShow = false;
            this.tableRecords = result.items;

            if (result.totalCount > 0) {

                this.total = result.items[0].summary.total;
                this.totalAmt = result.items[0].summary.totalAmt;
                this.totalPending = result.items[0].summary.pending;
                this.totalPendingAmt = result.items[0].summary.pendingAmt;
                this.totalRefunded = result.items[0].summary.refunded;
                this.totalRefundedAmt = result.items[0].summary.refundedAmt;
            }
            else
            {
                this.total = 0;
                this.totalPending = 0;
                this.totalRefunded = 0;
            }
        },
        err => {
            this.primengTableHelper.hideLoadingIndicator();
        });

        // this._jobRefundsServiceProxy.getAll(
        //     this.filterText,
        //     null,
        //     null,
        //     null,
        //     null,
        //     null,
        //     this.accountNoFilter,
        //     this.maxPaidDateFilter,
        //     this.minPaidDateFilter,
        //     null,
        //     null,
        //     null,
        //     this.organizationUnit,
        //     this.applicationstatus,
        //     0,
        //     this.salesRepId == 0 ? undefined : this.salesRepId,
        //     this.jobStatusID,
        //     this.jobRefundID,
        //     this.primengTableHelper.getSorting(this.dataTable),
        //     this.primengTableHelper.getSkipCount(this.paginator, event),
        //     this.primengTableHelper.getMaxResultCount(this.paginator, event)
        // ).subscribe(result => {
        //     this.primengTableHelper.totalRecordsCount = result.totalCount;
        //     this.primengTableHelper.records = result.items;
        //     const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
        //     this.firstrowcount =  totalrows + 1;
        //     this.last = totalrows + result.items.length;
        //     this.primengTableHelper.hideLoadingIndicator();
        //     this.shouldShow = false;
        // },
        // err => {
        //     this.primengTableHelper.hideLoadingIndicator();
        // });
        
    }
    changeOrgCode() {
        this.orgCode = this.allOrganizationUnits.find(x => x.id == this.organizationUnit).code;
    }
    reloadPage($event): void {
        this.ExpandedView = true;
        this.paginator.changePage(this.paginator.getPage());
    }

    createJobRefund(): void {
        // this.createOrEditJobRefundModal.show();        
    }

    verifyorNotVerifyJobRefund(id, vefifyornot, msg): void {
        let jobRefund= new JobRefundDto;
        jobRefund.id = id;

        this.message.confirm(
            '',
            "Are You Sure You Want to " + msg,
            (isConfirmed) => {
                if (isConfirmed) {
                    debugger;
                    this._jobRefundsServiceProxy.verifyorNotVerifyJobRefund(vefifyornot, 4, jobRefund)
                        .subscribe(() => {
                            let log = new UserActivityLogDto();
                            log.actionId = 15;
                            log.actionNote = vefifyornot ? 'Job Refund Verify' : 'Job Refund Not Verify';
                            log.section = 'Refund Tracker';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });
                            this.reloadPage(null);
                            this.notify.success("Sucessfully " + msg);
                        });
                }
            }
        );
    }

    deleteJobRefund(id): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._jobRefundsServiceProxy.delete(id)
                        .subscribe(() => {
                            let log = new UserActivityLogDto();
                            log.actionId = 15;
                            log.actionNote = 'Job Refund Request Deleted';
                            log.section = 'Refund Tracker';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });
                            this.reloadPage(null);
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    exportToExcel(excelorcsv): void {
        // if (this.sampleDateRange != null) {
        //     this.maxPaidDateFilter = this.sampleDateRange[0];
        //     this.minPaidDateFilter = this.sampleDateRange[1];
        // } else {
        //     this.maxPaidDateFilter = null;
        //     this.minPaidDateFilter = null;
        // }
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this.excelorcsvfile = excelorcsv;
        this._jobRefundsServiceProxy.getJobRefundsToExcel(
            this.filterName,
            filterText_,
            this.organizationUnit,
            this.applicationstatus,
            this.salesRepId,
            this.jobStatusID,
            this.jobRefundID,
            this.dateType,
            this.startDate,
            this.endDate,
            this.isVerify,
            this.refundPaymentType,
            this.excelorcsvfile,
            this.stateFilter,
            this.areaNameFilter
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    quickview(jobid): void {
        this.viewApplicationModal.show(jobid);
    }

    clear() {
        this.filterText = '';
        this.dateType = 'Receive';
        this.salesRepId = 0;
        this.startDate = moment(this.date);
        this.endDate = moment(this.date);
        this.getJobRefunds();
    }

    expandGrid() {
        this.ExpandedView = true;
    }

    navigateToLeadDetail(leadid): void {
        // this.ExpandedView = !this.ExpandedView;
        // this.ExpandedView = false;
        // this.SelectedLeadId = leadid;
        // this.viewLeadDetail.showDetail(leadid, null, 4);
    }

    RefStr: any;
    refundPDF(Id,jobnumber): void {
        
        this.spinnerService.show();
        this._reportServiceProxy.getRefundByJobId(Id).subscribe(result => {
    
            if(result.viewHtml != '')
            {
                this.RefStr = this.GetRefundReferralForm(result);
                this.RefStr = this.sanitizer.bypassSecurityTrustHtml(this.RefStr);
    
                let html = this.RefStr.changingThisBreaksApplicationSecurity;
                this._commonLookupService.downloadPdf(jobnumber, "RefundForm", html).subscribe(result => {
                    this.spinnerService.hide();
                    window.open(result, "_blank");
                },
                err => {
                    this.spinnerService.hide();
                });
            }
        },
        err => {
            this.spinnerService.hide();
        });
    }
    GetRefundReferralForm(result) {
        let htmlstring = result.viewHtml;

        let myTemplateStr = htmlstring;

        console.log(result);
        let myVariables = {
            R: {
                JobNumber: result.jobNumber,
                JobStatus: result.jobStatus,
                Date: result.date,
                
                Name: result.name,
                Mobile: result.mobile,
                Email: result.email,
                AddressLine1: result.addressLine1,
                AddressLine2: result.addressLine2,
                MeterPhase: result.meterPhase,
                MeterUpgrad: result.meterUpgrad,
                RoofType: result.roofType,
                NoofStorey: result.propertyType,
                RoofPitch: result.roofPitch,
                EnergyDist: result.elecDist,
                EnergyRetailer: result.elecRetailer,
                SalesRepName: result.salesRepName,
                DepositDate: result.depositDate,
                EntryDate: result.date,
                Manager: result.manager,
                NoOfPanels: result.noOfPanels,
                RefundReason: result.refundReason,
                CusBankName: result.cusBankName,
                CusBsb: result.cusBSB,
                CusAccNumber: result.cusAccNumber,
                CusAccName: result.cusAccName,
                RefundAmount: result.refundAmount,
                RefundDate: result.refundDate,
            }
        }

        // use custom delimiter {{ }}
        _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;
    
        // interpolate
        let compiled = _.template(myTemplateStr);
        let myTemplateCompiled = compiled(myVariables);
        return myTemplateCompiled;
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Refund Tracker';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Refund Tracker';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }

    isAllChecked() {
        if (this.tableRecords)
            return this.tableRecords.every(_ => _.isSelect);
    }

    checkAll(ev) {

        this.tableRecords.forEach(x => x.isSelect = x.verifyornot ? ev.target.checked : false);
        this.oncheckboxCheck();
    }

    submit() : void {
        this.saving = true;
        debugger
        // let transferPermission = this.permission.isGranted("Pages.Leads.LeadTracker.Action.TransferLeftSalesRepLead");
        this.tableRecords.forEach((lead) => {
            if (lead.isSelect && lead.vefifyornot) {
                this.refundPDF(lead.id,lead.jobNumber);
            }
        });  
        this.saving = false;

            
       
    }

    oncheckboxCheck() {

        
        this.count = 0;
        this.tableRecords.forEach(item => {
            debugger;
            if (item.isSelect == true) {
                this.count = this.count + 1;
            }
        })
    }

}
