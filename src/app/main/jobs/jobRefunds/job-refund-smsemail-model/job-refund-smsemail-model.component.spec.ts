import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobRefundSmsemailModelComponent } from './job-refund-smsemail-model.component';

describe('JobRefundSmsemailModelComponent', () => {
  let component: JobRefundSmsemailModelComponent;
  let fixture: ComponentFixture<JobRefundSmsemailModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobRefundSmsemailModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobRefundSmsemailModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
