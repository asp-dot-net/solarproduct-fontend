import { Component, ElementRef, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { JobRefundsServiceProxy, GetJobForEditOutput, CreateOrEditJobRefundDto, JobRefundPaymentOptionLookupTableDto, InvoiceServiceProxy, InvoicePaymentsServiceProxy, InvoicePaymentInvoicePaymentMethodLookupTableDto, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import * as _ from 'lodash';
import * as moment from 'moment';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
    selector: 'jobrefundModal',
    templateUrl: './job-refund-model.component.html'
})
export class RefundJobModalComponent extends AppComponentBase {

    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    saving = false;
    paidDate: moment.Moment;
    paymentOptionId = undefined;
    remarks = "";
    item: GetJobForEditOutput;
    comment = "";
    receiptNo = "";
    amount = 0;
    active = false;
    jobRefund: CreateOrEditJobRefundDto = new CreateOrEditJobRefundDto();
    paymentOptionName = '';
    xeroInvCreated=false;
    jobNote = '';
    refundReasonName = '';
    leadCompanyName: any;
    jobNumber: any;
    allPaymentOptions: JobRefundPaymentOptionLookupTableDto[];
    allInvoicePaymentMethods: InvoicePaymentInvoicePaymentMethodLookupTableDto[];
    constructor(
        injector: Injector,
        private _jobRefundsServiceProxy: JobRefundsServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _invoicePaymentsServiceProxy: InvoicePaymentsServiceProxy
    ) {
        super(injector);
    }

    sectionName = '';
    show(jobRefundId?: number, sectionId: number = 0,section = ''): void {

        this.active = true;
        this.sectionName = section;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Open Update Refund';
        log.section = section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
        
        if (!jobRefundId) {
            this.jobRefund = new CreateOrEditJobRefundDto();
            this.jobRefund.id = jobRefundId;
            this.paymentOptionId = 0;
            this.jobRefund.paidDate = moment().startOf('day');
            this.paymentOptionName = '';
            this.jobNote = '';
            this.amount = 0;
            this.refundReasonName = '';
            this.xeroInvCreated= false;
            this.modal.show();
            this.active = true;
        } else {
            if (this.allPaymentOptions == null) {
                this._jobRefundsServiceProxy.getAllPaymentOptionForTableDropdown().subscribe(result => {
                    this.allPaymentOptions = result;
                });
            }
            if (this.allInvoicePaymentMethods == null) {
                this._invoicePaymentsServiceProxy.getAllInvoicePaymentMethodForTableDropdown().subscribe(result => {
                    this.allInvoicePaymentMethods = result;
                });
            }
            this._jobRefundsServiceProxy.getJobRefundForEdit(jobRefundId).subscribe(result => {
                this.jobRefund = result.jobRefund;
                this.leadCompanyName = result.leadCompanyName;
                this.jobNumber = result.jobNumber;
                this.jobRefund.sectionId=sectionId;
                this.active = true;
                this.modal.show();
            });
        }
    }

    save(): void {
        this.saving = true;
        this._jobRefundsServiceProxy.createOrEdit(this.jobRefund)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                let log = new UserActivityLogDto();
                log.actionId = 15;
                log.actionNote = this.jobRefund.id > 0 ? 'Job Refund Request Completed' : 'Job Refund Request Create';
                log.section = this.sectionName;
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
                this.modal.hide();
                this.modalSave.emit(null);
                this.notify.info(this.l('SavedSuccessfully'));
            });
    }

    close(): void {
        this.modal.hide();
    }

}