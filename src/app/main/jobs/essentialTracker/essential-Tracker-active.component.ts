import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { EssentialTrackerActiveStatusDto, JobJobStatusLookupTableDto, JobsServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'EssentialTrackerActiveModal',
    templateUrl: './essential-Tracker-active.component.html'
})

export class EssentialTrackerActiveModalComponent extends AppComponentBase {

    @ViewChild('EssentialactiveModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    cash = false;
    saving = false;
    jobId : number;
    essentialStatus:EssentialTrackerActiveStatusDto = new EssentialTrackerActiveStatusDto();
    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _jobServiceProxy: JobsServiceProxy,
        private spinner: NgxSpinnerService  
    ) {
        super(injector);
    }

    sectionName = '';
    show(jobId: number,section = ''): void {
        debugger;
        this.sectionName =section;
      let log = new UserActivityLogDto();
      log.actionId = 79;
      log.actionNote ='Open Essential Tracker Status';
      log.section = section;
      this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
          .subscribe(() => {
      });
        this.jobId = jobId;       
        this.spinner.show();
        this._jobServiceProxy.getEssentialTrackerActiveStatus(jobId).subscribe(result => {
            debugger;
            this.essentialStatus = result;
            this.modal.show();
            this.spinner.hide();
        });      
    }

    save(): void {
        
    }
    
    close(): void {
        this.modal.hide();
    }
}