import { Component, ElementRef, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { JobsServiceProxy, GetJobForEditOutput, CreateOrEditJobDto, JobElecDistributorLookupTableDto, JobElecRetailerLookupTableDto, JobRoofAngleLookupTableDto, CommonLookupDto, LeadsServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import * as _ from 'lodash';
import * as moment from 'moment';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';
import { FileItem, FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { FileUpload } from 'primeng';
import { AppConsts } from '@shared/AppConsts';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
@Component({
    selector: 'AddEssentialNotesModal',
    templateUrl: './add-notes-model.component.html'
})
export class AddEssentialNotesModalComponent extends AppComponentBase {

    @ViewChild('addEssentialModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    saving = false;
    distAppliedDate: moment.Moment;
    MeterNumber = "";
    ApprovalRefNo = "";
    item: GetJobForEditOutput;
    comment = "";
    
    
    dbDistApprovalDate: any = null;
    private _uploaderOptions: FileUploaderOptions = {};
    
    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _jobsServiceProxy: JobsServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy,
        
    ) {
        super(injector);
        this.item = new GetJobForEditOutput();
        this.item.job = new CreateOrEditJobDto();
    }
    sectionName = '';

    show(jobid: number, trackerid?: number,section = ''): void {
        
        this.dbDistApprovalDate = null;
        
       this.sectionName =section;
       let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Essential Tracker Notes';
            log.section = section;
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        this._jobsServiceProxy.getJobForEdit(jobid).subscribe(result => {
            this.item.job = result.job;
            if(this.item.job.applicationfeespaid==null )
            {this.item.job.applicationfeespaid=0;}
         
            this.item.job.empId = result.assignToUserID;
            
            this._leadsServiceProxy.getCurrentUserId().subscribe(r => {
                this.item.job.appliedBy = r;
            });
            
        
            this.dbDistApprovalDate = this.item.job.distApproveDate;
        
            this.modal.show();
        });
      
    }

   

    
    save(): void {
        debugger;
            this.saving = true;
            debugger;
            this.item.job.sectionId = 45;
            this._jobsServiceProxy.updateDataFromTracker(this.item.job)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                let log = new UserActivityLogDto();
                log.actionId = 11;
                log.actionNote ='Job Data Updated From ' + this.sectionName;
                log.section = this.sectionName;
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
                this.notify.info(this.l('SavedSuccessfully'));
                this.modal.hide();
                this.modalSave.emit(null);
             });
             
    }

    close(): void {
        
        this.modal.hide();
    }

}