﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { ElecRetailersServiceProxy, CreateOrEditElecRetailerDto,UserActivityLogServiceProxy,UserActivityLogDto  } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';


@Component({
    selector: 'createOrEditElecRetailerModal',
    templateUrl: './create-or-edit-elecRetailer-modal.component.html'
})
export class CreateOrEditElecRetailerModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    saving = false;

    elecRetailer: CreateOrEditElecRetailerDto = new CreateOrEditElecRetailerDto();
    constructor(
        injector: Injector,
        private _elecRetailersServiceProxy: ElecRetailersServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }

    show(elecRetailerId?: number): void {

        if (!elecRetailerId) {
            this.elecRetailer = new CreateOrEditElecRetailerDto();
            this.elecRetailer.id = elecRetailerId;
            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Electricity Retailer';
            log.section = 'Electricity Retailer';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 

        } else {
            this._elecRetailersServiceProxy.getElecRetailerForEdit(elecRetailerId).subscribe(result => {
                this.elecRetailer = result.elecRetailer;
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Electricity Retailer : ' + this.elecRetailer.name;
                log.section = 'Electricity Retailer';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            });
        }
        
    }

    onShown(): void {
        
        document.getElementById('ElecRetailer_Name').focus();
    }
    
    save(): void {
            this.saving = true;	
            this._elecRetailersServiceProxy.createOrEdit(this.elecRetailer)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.elecRetailer.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Electricity Retailer Updated : '+ this.elecRetailer.name;
                    log.section = 'Electricity Retailer';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Electricity Retailer Created : '+ this.elecRetailer.name;
                    log.section = 'Electricity Retailer';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
