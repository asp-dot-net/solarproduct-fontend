﻿import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ElecRetailersServiceProxy, ElecRetailerDto,UserActivityLogServiceProxy,UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CreateOrEditElecRetailerModalComponent } from './create-or-edit-elecRetailer-modal.component';
import { ViewElecRetailerModalComponent } from './view-elecRetailer-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import { Title } from '@angular/platform-browser';
import { finalize } from 'rxjs/operators';
@Component({
    templateUrl: './elecRetailers.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ElecRetailersComponent extends AppComponentBase {

    @ViewChild('createOrEditElecRetailerModal', { static: true }) createOrEditElecRetailerModal: CreateOrEditElecRetailerModalComponent;
    @ViewChild('viewElecRetailerModalComponent', { static: true }) viewElecRetailerModal: ViewElecRetailerModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    advancedFiltersAreShown = false;
    filterText = '';
    nameFilter = '';
    nswFilter = -1;
    saFilter = -1;
    qldFilter = -1;
    vicFilter = -1;
    waFilter = -1;
    actFilter = -1;
    tasFilter = -1;
    ntFilter = -1;
    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    public screenHeight: any;  
    testHeight = 250;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };
    firstrowcount = 0;
    last = 0;

    constructor(
        injector: Injector,
        private _elecRetailersServiceProxy: ElecRetailersServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private titleService: Title
        ) {
            super(injector);
            this.titleService.setTitle(this.appSession.tenancyName + " |  Electricity Retailers");
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Electricity Retailer';
        log.section = 'Electricity Retailer';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
    }
    searchLog() : void {
        debugger;
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Electricity Retailer';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }
    getElecRetailers(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();
        this._elecRetailersServiceProxy.getAll(
            this.filterText,
            this.nameFilter,
            this.nswFilter,
            this.saFilter,
            this.qldFilter,
            this.vicFilter,
            this.waFilter,
            this.actFilter,
            this.tasFilter,
            this.ntFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createElecRetailer(): void {
        this.createOrEditElecRetailerModal.show();
    }

    clear() {
        this.nameFilter = '';
    }

    deleteElecRetailer(elecRetailer: ElecRetailerDto): void {
        this.message.confirm(
            this.l('AreYouSureWanttoDelete'),
            this.l('Delete'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._elecRetailersServiceProxy.delete(elecRetailer.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete Electricity Retailer: ' + elecRetailer.name;
                            log.section = 'Electricity Retailer';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            }); 
                        });
                }
            }
        );
    }

    exportToExcel(): void {
        this._elecRetailersServiceProxy.getElecRetailersToExcel(
            this.filterText,
            this.nameFilter,
            this.nswFilter,
            this.saFilter,
            this.qldFilter,
            this.vicFilter,
            this.waFilter,
            this.actFilter,
            this.tasFilter,
            this.ntFilter,
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }
}
