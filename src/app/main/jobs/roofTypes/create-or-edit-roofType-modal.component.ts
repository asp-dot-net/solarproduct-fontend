﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { RoofTypesServiceProxy, CreateOrEditRoofTypeDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'createOrEditRoofTypeModal',
    templateUrl: './create-or-edit-roofType-modal.component.html'
})
export class CreateOrEditRoofTypeModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    roofType: CreateOrEditRoofTypeDto = new CreateOrEditRoofTypeDto();

    constructor(
        injector: Injector,
        private _roofTypesServiceProxy: RoofTypesServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }

    show(roofTypeId?: number): void {

        if (!roofTypeId) {
            this.roofType = new CreateOrEditRoofTypeDto();
            this.roofType.id = roofTypeId;

            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Roof Types';
            log.section = 'Roof Types';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._roofTypesServiceProxy.getRoofTypeForEdit(roofTypeId).subscribe(result => {
                this.roofType = result.roofType;


                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Roof Types : ' + this.roofType.name;
                log.section = 'Roof Types';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        
    }

    onShown(): void {
        
        
        document.getElementById('RoofType_Name').focus();
    }

    save(): void {
            this.saving = true;

            this._roofTypesServiceProxy.createOrEdit(this.roofType)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.roofType.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Roof Types Updated : '+ this.roofType.name;
                    log.section = 'Roof Types';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Roof Types Created : '+ this.roofType.name;
                    log.section = 'Roof Types';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }

             });
    }







    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
