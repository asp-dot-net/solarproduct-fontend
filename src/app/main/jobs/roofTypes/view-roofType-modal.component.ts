﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetRoofTypeForViewDto, RoofTypeDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
@Component({
    selector: 'viewRoofTypeModal',
    templateUrl: './view-roofType-modal.component.html'
})
export class ViewRoofTypeModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetRoofTypeForViewDto;


    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
        this.item = new GetRoofTypeForViewDto();
        this.item.roofType = new RoofTypeDto();
    }

    show(item: GetRoofTypeForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();

        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Opened Roof Types View : ' + this.item.roofType.name;
        log.section = 'Roof Types';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {            
         });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
