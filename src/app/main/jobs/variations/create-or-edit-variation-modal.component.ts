﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { VariationsServiceProxy, CreateOrEditVariationDto,UserActivityLogServiceProxy, UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';

@Component({
    selector: 'createOrEditVariationModal',
    templateUrl: './create-or-edit-variation-modal.component.html'
})
export class CreateOrEditVariationModalComponent extends AppComponentBase {
   
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    variation: CreateOrEditVariationDto = new CreateOrEditVariationDto();

    constructor(
        injector: Injector,
        private _variationsServiceProxy: VariationsServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
    }

    show(variationId?: number): void {

        if (!variationId) {
            this.variation = new CreateOrEditVariationDto();
            this.variation.id = variationId;
            this.variation.action = "Plus";
            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto(); 
            log.actionId = 79;
            log.actionNote ='Open For Create New Price Variations';
            log.section = 'Price Variations';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._variationsServiceProxy.getVariationForEdit(variationId).subscribe(result => {
                this.variation = result.variation;
                

                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Price Variations : ' + this.variation.name;
                log.section = 'Price Variations';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        
    }

    onShown(): void {
        
        document.getElementById('Variation_Name').focus();
    }

    save(): void {
            this.saving = true;
            this._variationsServiceProxy.createOrEdit(this.variation)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);

                if(this.variation.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Price Variations Updated : '+ this.variation.name;
                    log.section = 'Price Variations';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Price Variations Created : '+ this.variation.name;
                    log.section = 'Price Variations';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }

             });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
