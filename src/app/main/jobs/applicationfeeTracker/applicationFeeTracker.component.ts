import { Component, Injector, ViewEncapsulation, ViewChild, Input, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JobsServiceProxy, JobDto, GetLeadForViewDto, LeadsServiceProxy, OrganizationUnitDto, UserServiceProxy, LeadStateLookupTableDto, JobElecDistributorLookupTableDto, JobJobTypeLookupTableDto, JobStatusTableDto, GetJobForViewDto, NameValueOfInt32, CommonLookupServiceProxy, JobTrackerServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { EntityTypeHistoryModalComponent } from '@app/shared/common/entityHistory/entity-type-history-modal.component';
import * as _ from 'lodash';
import * as moment from 'moment';
// import { JobNotesModalComponent } from './job-notes-model.component';
// import { JobsComponent } from './jobs.component';
// import { JobDetailModalComponent } from './job-detail-model.component';
import { OnInit } from '@angular/core';
// import { ViewApplicationModelComponent } from './view-application-model/view-application-model.component';
// import { AddActivityModalComponent } from '@app/main/myleads/myleads/add-activity-model.component';
// import { JobSmsEmailModelComponent } from './job-sms-email-model/job-sms-email-model.component';
// import { DocumentRequestLinkModalComponent } from './document-request-link.component';
// import { ViewMyLeadComponent } from '@app/main/myleads/myleads/view-mylead.component';
import { id } from '@swimlane/ngx-charts';
import { Title } from '@angular/platform-browser';
 import { JobActiveModalComponent } from '@app/main/jobs/jobs/check-job-active.component';
import { SMSModelComponent } from '@app/main/activitylog/sms-modal.component';
import { EmailModelComponent } from '@app/main/activitylog/email-modal.component';
import { NotifyModelComponent } from '@app/main/activitylog/notify-modal.component';
import { CommentModelComponent } from '@app/main/activitylog/comment-modal.component';
import { ToDoModalComponent } from '@app/main/activitylog/todo-modal.component';
import { ReminderModalComponent } from '@app/main/activitylog/reminder-modal.component';
import { LeadDetailsComponent } from '@app/main/leaddetails/leaddetails.component';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './applicationFeeTracker.component.html',
    // styleUrls: ['./jobslist.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ApplicationFeeTrackerComponent extends AppComponentBase implements OnInit {
    FiltersData = false
    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    organizationUnitlength: number = 0;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    @ViewChild('entityTypeHistoryModal', { static: true }) entityTypeHistoryModal: EntityTypeHistoryModalComponent;
    // @ViewChild('jobNotesModal', { static: true }) jobNotesModal: JobNotesModalComponent;
    // @ViewChild('jobDetailModal', { static: true }) jobDetailModal: JobDetailModalComponent;
    // @ViewChild('viewApplicationModal', { static: true }) viewApplicationModal: ViewApplicationModelComponent;
    // @ViewChild('documentRequestModal', { static: true }) documentRequestModal: DocumentRequestLinkModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    // @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    // @ViewChild('addSmsEmailModal', { static: true }) addSmsEmailModal: JobSmsEmailModelComponent;
    // @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
    @ViewChild('jobActiveModal', { static: true }) jobActiveModal: JobActiveModalComponent;
    @ViewChild('smsModel', { static: true }) smsModel: SMSModelComponent;
    @ViewChild('emailModel', { static: true }) emailModel: EmailModelComponent;
    @ViewChild('notifyModel', { static: true }) notifyModel: NotifyModelComponent;
    @ViewChild('commentModel', { static: true }) commentModel: CommentModelComponent;
    @ViewChild('todoModal', { static: true }) todoModal: ToDoModalComponent;
    @ViewChild('ReminderModal', { static: true }) ReminderModal: ReminderModalComponent;
    
    @ViewChild('leaddetails', { static: true }) leaddetails: LeadDetailsComponent;

    selectedCar:any;
    advancedFiltersAreShown = false;
    
    maxPanelOnFlatFilter: number;
    maxPanelOnFlatFilterEmpty: number;
    minPanelOnFlatFilter: number;
    minPanelOnFlatFilterEmpty: number;
    maxPanelOnPitchedFilter: number;
    maxPanelOnPitchedFilterEmpty: number;
    minPanelOnPitchedFilter: number;
    minPanelOnPitchedFilterEmpty: number;
    regPlanNoFilter = '';
    lotNumberFilter = '';
    peakMeterNoFilter = '';
    offPeakMeterFilter = '';
    enoughMeterSpaceFilter = -1;
    isSystemOffPeakFilter = -1;
    suburbFilter = '';
    stateFilter = '';
    jobTypeNameFilter = '';
    jobStatusNameFilter = '';
    roofTypeNameFilter = '';
    roofAngleNameFilter = '';
    elecDistributorNameFilter = '';
    leadCompanyNameFilter = '';
    elecRetailerNameFilter = '';
    paymentOptionNameFilter = '';
    depositOptionNameFilter = '';
    meterUpgradeNameFilter = '';
    meterPhaseNameFilter = '';
    promotionOfferNameFilter = '';
    houseTypeNameFilter = '';
    
    OpenRecordId: number = 0;
    organizationUnit = 0;
    elecDistributorId = 0;
    jobTypeId = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    _entityTypeFullName = 'TheSolarProduct.Jobs.Job';
    entityHistoryEnabled = false;
    
    allStates: LeadStateLookupTableDto[];
    jobElecDistributors: JobElecDistributorLookupTableDto[];
    jobTypes: JobJobTypeLookupTableDto[];
    projectFilter = '';
    stateNameFilter = '';
    jobStatusIDFilter: [];
    alljobstatus: JobStatusTableDto[];

    applicationstatus = 0; //1
    date = new Date();
    //filterText = '821831';
    filterText = '';
    dateTypeFilter = 'DepositeReceived';
    
    //startDate: moment.Moment;
    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);
    ExpandedView: boolean = true;
    SelectedLeadId: number = 0;
    paymentid = 0;
    areaNameFilter = "";
    postalcodefrom = "";
    postalcodeTo = "";
    excelorcsvfile = 0;
    firstrowcount = 0;
    last = 0;
    
    total = 0;
    totalPaid = 0;
    totalPending = 0;
    totalComplete = 0;
    totalExpiry = 0;

    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 330;
    toggle: boolean = true;
    filterName = "JobNumber";
    orgCode = '';
    paidStatus: number = 0;
    feesPaid: number = 0;
    paidById: number = 0;
    change() {
        this.toggle = !this.toggle;
      }

    constructor(
        injector: Injector,
        private _jobsServiceProxy: JobsServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _userServiceProxy: UserServiceProxy,
        private _router: Router,
        private titleService: Title,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _jobTrackerService: JobTrackerServiceProxy
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Application Fee Tracker");
    }
   
    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        this.entityHistoryEnabled = this.setIsEntityHistoryEnabled();
   
        this._commonLookupService.getAllJobStatusTableDropdown().subscribe(result => {
            this.alljobstatus = result;
        });
        this._commonLookupService.getAllJobTypeForTableDropdown().subscribe(result => {
            this.jobTypes = result;
        });
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
        this._jobsServiceProxy.getAllElecDistributorForTableDropdown().subscribe(result => {
            this.jobElecDistributors = result;
        });
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Application Fee Tracker';
            log.section = 'Application Fee Tracker';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.orgCode = this.allOrganizationUnits[0].code;
            this.getJobs();
        });
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }

        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }

    private setIsEntityHistoryEnabled(): boolean {
        let customSettings = (abp as any).custom;
        return this.isGrantedAny('Pages.Administration.AuditLogs') && customSettings.EntityHistory && customSettings.EntityHistory.isEnabled && _.filter(customSettings.EntityHistory.enabledEntities, entityType => entityType === this._entityTypeFullName).length === 1;
    }

    // addNotes(jobid, trackerid?): void {

    //     this.jobNotesModal.show(jobid, trackerid,'Application Tracker');
    // }

    // quickview(jobid): void {
    //     this.viewApplicationModal.show(jobid);
    // }
    changeOrgCode() {
        this.orgCode = this.allOrganizationUnits.find(x => x.id == this.organizationUnit).code;
    }
    getJobs(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._jobTrackerService.getAllJobApplicationFeeTracker(
            this.filterName,
            filterText_,
            this.organizationUnit,
            this.applicationstatus,
            this.stateNameFilter,
            this.elecDistributorId,
            this.jobStatusIDFilter,
            this.jobTypeId,
            this.dateTypeFilter,
            this.startDate,
            this.endDate,
            0,
            // undefined,undefined,undefined,
            this.paidById,
           this.paidStatus
           ,this.feesPaid,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            debugger;
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            this.shouldShow = false;
            if (result.totalCount > 0) {
                this.total = result.items[0].summary.total;
                this.totalPaid = result.items[0].summary.paid;
                this.totalPending = result.items[0].summary.pending;
               
            }
        });
    }

    clear() {
        this.stateNameFilter = '';
        this.elecDistributorId = 0;
        this.jobStatusIDFilter = [];
        this.jobTypeId = 0;
        this.startDate = moment(this.date);
        this.endDate = moment(this.date);
        //this.sampleDateRange = [moment().add(-7, 'days').endOf('day'), moment().add(0, 'days').startOf('day')];
        this.getJobs();
    }

   reloadPage($event): void {
        this.ExpandedView = true;
        this.paginator.changePage(this.paginator.getPage());
    } 

    createJob(): void {
        this._router.navigate(['/app/admin/jobs/jobs/createOrEdit']);
    }

    showHistory(job: JobDto): void {
        this.entityTypeHistoryModal.show({
            entityId: job.id.toString(),
            entityTypeFullName: this._entityTypeFullName,
            entityTypeDescription: ''
        });
    }

    deleteJob(job: JobDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._jobsServiceProxy.delete(job.id)
                        .subscribe(() => {
                            this.reloadPage(null);
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    exportToExcel(excelorcsv): void {
        // if (this.sampleDateRange != null) {
        //     this.startDate = this.sampleDateRange[0];
        //     this.endDate = this.sampleDateRange[1];
        // } else {
        //     this.startDate = null;
        //     this.endDate = null;
        // }
        this.primengTableHelper.showLoadingIndicator();
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this.excelorcsvfile = excelorcsv;
        this._jobsServiceProxy.getApplicationFeeTrackerToExcel(
            this.filterName,
            filterText_,
            this.organizationUnit,
            this.applicationstatus,
            this.stateNameFilter,
            this.elecDistributorId,
            this.jobStatusIDFilter,
            this.jobTypeId,
            this.dateTypeFilter,
            this.startDate,
            this.endDate,
            1,
            // undefined,undefined,undefined,
            this.paidById,
           this.paidStatus
           ,this.feesPaid,
        )
            .subscribe(result => {
                
                this._fileDownloadService.downloadTempFile(result);
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    expandGrid() {
        this.ExpandedView = true;
    }

    navigateToLeadDetail(leadid): void {
        // this.ExpandedView = !this.ExpandedView;
        // this.ExpandedView = false;
        // this.SelectedLeadId = leadid;
        // this.viewLeadDetail.showDetail(leadid,null,1);

        // this.ExpandedView = !this.ExpandedView;
        // this.ExpandedView = false;
        // this.SelectedLeadId = leadid;
        // this.leaddetails.show(leadid, 1, null);
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Application Tracker';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Application Tracker';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }

}