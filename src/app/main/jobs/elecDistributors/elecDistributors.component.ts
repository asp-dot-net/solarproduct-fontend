﻿import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ElecDistributorsServiceProxy, ElecDistributorDto ,UserActivityLogDto,UserActivityLogServiceProxy} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CreateOrEditElecDistributorModalComponent } from './create-or-edit-elecDistributor-modal.component';
import { ViewElecDistributorModalComponent } from './view-elecDistributor-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import { Title } from '@angular/platform-browser';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './elecDistributors.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ElecDistributorsComponent extends AppComponentBase {
    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    @ViewChild('createOrEditElecDistributorModal', { static: true }) createOrEditElecDistributorModal: CreateOrEditElecDistributorModalComponent;
    @ViewChild('viewElecDistributorModalComponent', { static: true }) viewElecDistributorModal: ViewElecDistributorModalComponent;

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    nameFilter = '';
    maxSeqFilter: number;
    maxSeqFilterEmpty: number;
    minSeqFilter: number;
    minSeqFilterEmpty: number;
    nswFilter = -1;
    saFilter = -1;
    qldFilter = -1;
    vicFilter = -1;
    waFilter = -1;
    actFilter = -1;
    tasFilter = '';
    ntFilter = -1;
    electDistABBFilter = '';
    elecDistAppReqFilter = -1;
    maxGreenBoatDistributorFilter: number;
    maxGreenBoatDistributorFilterEmpty: number;
    minGreenBoatDistributorFilter: number;
    minGreenBoatDistributorFilterEmpty: number;
    firstrowcount = 0;
    last = 0;
    public screenHeight: any;  
    testHeight = 250;

    constructor(
        injector: Injector,
        private _elecDistributorsServiceProxy: ElecDistributorsServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private titleService: Title,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Electricity Distributors");        
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Electricity Distributor';
        log.section = 'Electricity Distributor';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });    
    }
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    } 
    searchLog() : void {
        debugger;
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Electricity Distributor';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    getElecDistributors(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._elecDistributorsServiceProxy.getAll(
            this.filterText,
            this.nameFilter,
            this.maxSeqFilter == null ? this.maxSeqFilterEmpty : this.maxSeqFilter,
            this.minSeqFilter == null ? this.minSeqFilterEmpty : this.minSeqFilter,
            this.nswFilter,
            this.saFilter,
            this.qldFilter,
            this.vicFilter,
            this.waFilter,
            this.actFilter,
            this.tasFilter,
            this.ntFilter,
            this.electDistABBFilter,
            this.elecDistAppReqFilter,
            this.maxGreenBoatDistributorFilter == null ? this.maxGreenBoatDistributorFilterEmpty : this.maxGreenBoatDistributorFilter,
            this.minGreenBoatDistributorFilter == null ? this.minGreenBoatDistributorFilterEmpty : this.minGreenBoatDistributorFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createElecDistributor(): void {
        this.createOrEditElecDistributorModal.show();
    }


    deleteElecDistributor(elecDistributor: ElecDistributorDto): void {
        this.message.confirm(
            this.l('AreYouSureWanttoDelete'),
            this.l('Delete'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._elecDistributorsServiceProxy.delete(elecDistributor.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete Electricity Distributor: ' + elecDistributor.name;
                            log.section = 'Electricity Distributor';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            }); 
                        });
                }
            }
        );
    }
    clear() {
        this.nameFilter = '';
        this.electDistABBFilter = '';

    }
    exportToExcel(): void {
        this._elecDistributorsServiceProxy.getElecDistributorsToExcel(
            this.filterText,
            this.nameFilter,
            this.maxSeqFilter == null ? this.maxSeqFilterEmpty : this.maxSeqFilter,
            this.minSeqFilter == null ? this.minSeqFilterEmpty : this.minSeqFilter,
            this.nswFilter,
            this.saFilter,
            this.qldFilter,
            this.vicFilter,
            this.waFilter,
            this.actFilter,
            this.tasFilter,
            this.ntFilter,
            this.electDistABBFilter,
            this.elecDistAppReqFilter,
            this.maxGreenBoatDistributorFilter == null ? this.maxGreenBoatDistributorFilterEmpty : this.maxGreenBoatDistributorFilter,
            this.minGreenBoatDistributorFilter == null ? this.minGreenBoatDistributorFilterEmpty : this.minGreenBoatDistributorFilter,
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }
}
