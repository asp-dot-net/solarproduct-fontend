﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetElecDistributorForViewDto, ElecDistributorDto ,UserActivityLogDto, UserActivityLogServiceProxy} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
@Component({
    selector: 'viewElecDistributorModal',
    templateUrl: './view-elecDistributor-modal.component.html'
})
export class ViewElecDistributorModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetElecDistributorForViewDto;


    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
        this.item = new GetElecDistributorForViewDto();
        this.item.elecDistributor = new ElecDistributorDto();
    }

    show(item: GetElecDistributorForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Opened Electricity Distributor View : ' + item.elecDistributor.name;
        log.section = 'Electricity Distributor';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {            
         }); 
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
