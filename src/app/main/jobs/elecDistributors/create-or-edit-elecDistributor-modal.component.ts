﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { ElecDistributorsServiceProxy, CreateOrEditElecDistributorDto, UserActivityLogDto, UserActivityLogServiceProxy} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
@Component({
    selector: 'createOrEditElecDistributorModal',
    templateUrl: './create-or-edit-elecDistributor-modal.component.html'
})
export class CreateOrEditElecDistributorModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    saving = false;
    elecDistributor: CreateOrEditElecDistributorDto = new CreateOrEditElecDistributorDto();

    constructor(
        injector: Injector,
        private _elecDistributorsServiceProxy: ElecDistributorsServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
    }

    show(elecDistributorId?: number): void {

        if (!elecDistributorId) {
            this.elecDistributor = new CreateOrEditElecDistributorDto();
            this.elecDistributor.id = elecDistributorId;
            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Electricity Distributor';
            log.section = 'Electricity Distributor';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
        } else {
            this._elecDistributorsServiceProxy.getElecDistributorForEdit(elecDistributorId).subscribe(result => {
                this.elecDistributor = result.elecDistributor;
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Electricity Distributor : ' + this.elecDistributor.name;
                log.section = 'Electricity Distributor';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            });
        }
        
    }

    onShown(): void {
        
        document.getElementById('ElecDistributor_Name').focus();
    }
    
    save(): void {
            this.saving = true;
            this._elecDistributorsServiceProxy.createOrEdit(this.elecDistributor)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.elecDistributor.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Electricity Distributor Updated : '+ this.elecDistributor.name;
                    log.section = 'Electricity Distributor';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Electricity Distributor Created : '+ this.elecDistributor.name;
                    log.section = 'Electricity Distributor';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
