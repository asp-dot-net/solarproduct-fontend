import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { CreateOrEditVouchersDto,MultipleOrganizationDto, OrganizationUnitDto,UserActivityLogServiceProxy,CommonLookupServiceProxy, UserActivityLogDto, VoucherServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { AppConsts } from '@shared/AppConsts';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { FileUploader, FileItem, FileUploaderOptions } from 'ng2-file-upload';

@Component({
    selector: 'createOrEditVoucherModal',
    templateUrl: './create-or-edit-voucher-modal.component.html'
})
export class CreateOrEditVoucherModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    Vouchers: CreateOrEditVouchersDto = new CreateOrEditVouchersDto();
    
    public uploader: FileUploader;
    public maxfileBytesUserFriendlyValue = 5;
    private _uploaderOptions: FileUploaderOptions = {};
    filetoken: '';
    selectedFile : any;
    OrgList: any[];
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnit = 0;
    constructor(
        injector: Injector,
        private _voucherServiceProxy: VoucherServiceProxy,
        private _tokenService: TokenService,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
    ) {
        super(injector);
    }

    show(VoucherId?: number): void {
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
     
        });
        if (!VoucherId) {
            this.Vouchers = new CreateOrEditVouchersDto();
            this.Vouchers.id = VoucherId;
            this.OrgList = [{
                id: 0,
                orgId :0
                
            }];
            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Voucher';
            log.section = 'Voucher';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._voucherServiceProxy.getVouchersForEdit(VoucherId).subscribe(result => {
                this.Vouchers = result;
                this.OrgList = result.multipleOrganization.map((item) => ({
                    id: item.id,
                    orgId: item.orgId,
                    
                    
                }));
                this.active = true;
                this.modal.show();
                
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Voucher : ' + this.Vouchers.voucherName;
                log.section = 'Voucher';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        this.initFileUploader();
        
    }

    onShown(): void {
        
        document.getElementById('ProductType_Name').focus();
    }
    removeOrg(Org): void {
        if (this.OrgList.length == 1)
            return;
       
        if (this.OrgList.indexOf(Org) === -1) {
            
        } else {
            this.OrgList.splice(this.OrgList.indexOf(Org), 1);
        }
       
    }
    addOrg(): void {
        let OrgnizationList = { id: 0, OrgId: 0}
        this.OrgList.push(OrgnizationList);
        
    }
    save(): void {
        this.saving = true;
        this.Vouchers.fileToken = this.filetoken ?this.filetoken : ""; 

        this.Vouchers.fileName = this.Vouchers.fileName ? this.Vouchers.fileName : ""; 

        if(!this.Vouchers.fileToken ){
            if( this.Vouchers.fileToken.trim() == ""){
                this.notify.warn("Select file");
                this.saving = false;
                return;
            }
        }
        this.Vouchers.multipleOrganization = [];
        this.OrgList.map((item) => {
                         let MultipleOrganizationDtos = new MultipleOrganizationDto();
                         MultipleOrganizationDtos.id = item.id;
                         MultipleOrganizationDtos.orgId = item.orgId;
                       
                         this.Vouchers.multipleOrganization.push(MultipleOrganizationDtos);
                     });
        this._voucherServiceProxy.createOrEdit(this.Vouchers)
         .pipe(finalize(() => { this.saving = false;}))
         .subscribe(() => {
            this.notify.info(this.l('SavedSuccessfully'));
            this.close();
            this.modalSave.emit(null);
            if(this.Vouchers.id){
                let log = new UserActivityLogDto();
                log.actionId = 82;
                log.actionNote ='Voucher Updated : '+ this.Vouchers.voucherName;
                log.section = 'Voucher';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }else{
                let log = new UserActivityLogDto();
                log.actionId = 81;
                log.actionNote ='Voucher Created : '+ this.Vouchers.voucherName;
                log.section = 'Voucher';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }
         });
}

    close(): void {
        this.active = false;
        this.modal.hide();
    }
    initFileUploader(): void {
        this.uploader = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Profile/UploadFile' });
        this._uploaderOptions.autoUpload = false;
        this._uploaderOptions.authToken = 'Bearer ' + this._tokenService.getToken();
        this._uploaderOptions.removeAfterUpload = true;
        this.uploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

        this.uploader.onBuildItemForm = (fileItem: FileItem, form: any) => {
            form.append('FileType', fileItem.file.type);
            form.append('FileName', fileItem.file.name);
            form.append('FileToken', this.guid());
        };

        this.uploader.onSuccessItem = (item, response, status) => {
            const resp = <IAjaxResponse>JSON.parse(response);
            if (resp.success) {
                this.filetoken = resp.result.fileToken;
                this.Vouchers.fileName = resp.result.fileName;
            } else {
                this.message.error(resp.error.message);
            }
        };

        this.uploader.setOptions(this._uploaderOptions);
    }

    fileChangeEvent(event: any): void {
        debugger
        // if (event.target.files[0].size > 5242880) { //5MB
        //     this.message.warn(this.l('ProfilePicture_Warn_SizeLimit', this.maxfileBytesUserFriendlyValue));
        //     return;
        // }
        var url = URL.createObjectURL(event.target.files[0]);
        var img = new Image;

        // img.onload = () =>{
        //     var w = img.width;
        //     var h = img.height;
        //     //alert(w + "X"+ h )
        //     if(w != 290 || h != 330){
        //         this.notify.warn("Select Image That Contain Size 290 X 330");
        //         this.selectedFile = null;
        //    }
        // };
        img.src = url;
        this.uploader.clearQueue();
        this.uploader.addToQueue([<File>event.target.files[0]]);
        this.uploader.uploadAll();

    }

    guid(): string {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }

    
    downloadfile(fileName, filePath): void {
        let FileName = AppConsts.oldDocUrl + filePath+ fileName;

        window.open(FileName, "_blank");

    }
}
