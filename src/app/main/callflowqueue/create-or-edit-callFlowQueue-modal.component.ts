﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { CallFlowQueueServiceProxy, CreateOrEditCallFlowQueueDto, OrganizationUnitDto, CommonLookupServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';

@Component({
    selector: 'createOrEditCallFlowQueueModal',
    templateUrl: './create-or-edit-callFlowQueue-modal.component.html'
})
export class createOrEditCallFlowQueueModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    
    isActive = false;
    active = false;
    saving = false;

    CallFlowQueue: CreateOrEditCallFlowQueueDto = new CreateOrEditCallFlowQueueDto();

    organizationUnit = [];
    allOrganizationUnits: OrganizationUnitDto[];

    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _callFlowQueueServiceProxy: CallFlowQueueServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy
    ) {
        super(injector);
    }

    show(CallFlowQueueId?: number): void { 
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
        });

        if (!CallFlowQueueId) {
            this.CallFlowQueue = new CreateOrEditCallFlowQueueDto();
            this.CallFlowQueue.id = CallFlowQueueId;
            this.active = true;
            this.isActive = true;
            setTimeout  (() => {
                this.modal.show();
            },2000);
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Call Flow Queue';
            log.section = 'Call Flow Queue';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
        } else {
            this._callFlowQueueServiceProxy.getCallFlowQueueForEdit(CallFlowQueueId).subscribe(result => {
                this.CallFlowQueue = result.callFlowQueueDto;
                this.active = true;
                this.isActive = true;
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Call Flow Queue : ' + this.CallFlowQueue.name;
                log.section = 'Call Flow Queue';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
                
                this.modal.show();
            });
        }    
        
    }

    save(): void {
        this.saving = true;

        this._callFlowQueueServiceProxy.createOrEdit(this.CallFlowQueue)
            .pipe(finalize(() => { this.saving = false;}))
            .subscribe(() => {
            this.notify.info(this.l('SavedSuccessfully'));
            this.close();
            this.modalSave.emit(null);
            if(this.CallFlowQueue.id){
                let log = new UserActivityLogDto();
                log.actionId = 82;
                log.actionNote ='Call Flow Queue Updated : '+ this.CallFlowQueue.name;
                log.section = 'Call Flow Queue';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }else{
                let log = new UserActivityLogDto();
                log.actionId = 81;
                log.actionNote ='Call Flow Queue Created : '+ this.CallFlowQueue.name;
                log.section = 'Call Flow Queue';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }
           
        });
    }

    onShown(): void {
        
        document.getElementById('LeadSource_Name').focus();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
