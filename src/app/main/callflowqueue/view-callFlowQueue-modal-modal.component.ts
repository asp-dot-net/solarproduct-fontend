﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetCallFlowQueueForViewDto, CallFlowQueueDto, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'viewcallFlowQueueModal',
    templateUrl: './view-callFlowQueue-modal-modal.component.html'
})
export class viewcallFlowQueueModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetCallFlowQueueForViewDto;


    constructor(
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        injector: Injector
    ) {
        super(injector);
        this.item = new GetCallFlowQueueForViewDto();
        this.item.callFlowQueue = new CallFlowQueueDto();
    }

    show(item: GetCallFlowQueueForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Opened Call Flow Queue View : ' + item.callFlowQueue.name;
        log.section = 'Call Flow Queue';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {            
         }); 
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
