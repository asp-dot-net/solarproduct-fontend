﻿import {AppConsts} from '@shared/AppConsts';
import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute , Router} from '@angular/router';
import { CallFlowQueueServiceProxy, CallFlowQueueDto, UserActivityLogDto, UserActivityLogServiceProxy  } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';


import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { createOrEditCallFlowQueueModalComponent } from './create-or-edit-callFlowQueue-modal.component';
import { viewcallFlowQueueModalComponent } from './view-callFlowQueue-modal-modal.component';
import { Title } from '@angular/platform-browser';
import { finalize } from 'rxjs/operators';


@Component({
    templateUrl: './call-flow-queue.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class CallFlowQueueComponent extends AppComponentBase {
    
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
     @ViewChild('createOrEditCallFlowQueueModal', { static: true }) createOrEditCallFlowQueueModal: createOrEditCallFlowQueueModalComponent;
     @ViewChild('viewcallFlowQueueModal', { static: true }) viewcallFlowQueueModal: viewcallFlowQueueModalComponent;
    
    advancedFiltersAreShown = false;
    filterText = '';
    nameFilter = '';
    firstrowcount = 0;
    last = 0;
    shouldShow: boolean = false;
    // startDate = moment().startOf('day');
    // endDate = moment().endOf('day');
    date = new Date();
    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);
    public screenHeight: any;  
    testHeight = 250;

    constructor(
        injector: Injector,
        private _callFlowQueueServiceProxy :CallFlowQueueServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _fileDownloadService: FileDownloadService,
            private titleService: Title
            ) {
                super(injector);
                this.titleService.setTitle(this.appSession.tenancyName + " |  Call flow Queue");
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        let log = new UserActivityLogDto();
            log.actionId =79;
            log.actionNote ='Open Call Flow Queue';
            log.section = 'Call Flow Queue';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  

    getCallFlowQueue(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._callFlowQueueServiceProxy.getAll(
            this.filterText,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    // createLeadSource(): void {
    //     this._router.navigate(['/app/main/leadSources/leadSources/createOrEdit']);        
    // }
    searchLog() : void {
        debugger;
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Call Flow Queue';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    deleteLeadSource(callFlowQueue: CallFlowQueueDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._callFlowQueueServiceProxy.delete(callFlowQueue.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete Call Flow Queue : ' + callFlowQueue.name;
                            log.section = 'Call Flow Queue';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            }); 
                        });
                }
            }
        );
    }

    // exportToExcel(): void {
    //     this._leadSourcesServiceProxy.getLeadSourcesToExcel(
    //     this.filterText,
    //         this.nameFilter,
    //     )
    //     .subscribe(result => {
    //         this._fileDownloadService.downloadTempFile(result);
    //      });
    // }
    
    
    
    
    
}
