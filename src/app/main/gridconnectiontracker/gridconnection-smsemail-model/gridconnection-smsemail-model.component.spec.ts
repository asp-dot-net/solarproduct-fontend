import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GridconnectionSmsemailModelComponent } from './gridconnection-smsemail-model.component';

describe('GridconnectionSmsemailModelComponent', () => {
  let component: GridconnectionSmsemailModelComponent;
  let fixture: ComponentFixture<GridconnectionSmsemailModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GridconnectionSmsemailModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GridconnectionSmsemailModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
