import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { CommonLookupDto, CreateOrEditInvoicePaymentDto, CreateOrEditJobDto, InvoicePaymentInvoicePaymentMethodLookupTableDto, InvoicePaymentsServiceProxy, JobDto, JobElecDistributorLookupTableDto, JobElecRetailerLookupTableDto, JobLeadLookupTableDto, JobRoofAngleLookupTableDto, JobsServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';

@Component({
    selector: 'addMeterModal',
    templateUrl: './add-meterdetail-modal.component.html'
})

export class AddMeterDetailModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    sectionId = 0;
    saving = false;
    receiveddate: moment.Moment;
    allPVDStatus: JobLeadLookupTableDto[];
    job: CreateOrEditJobDto = new CreateOrEditJobDto();
    jobid: number;
    leadCompanyName:any;
    jobNumber:any;
    jobElecDistributors: JobElecDistributorLookupTableDto[];
    jobElecRetailers: JobElecRetailerLookupTableDto[];
    jobRoofAngles: JobRoofAngleLookupTableDto[];
    allUsers: CommonLookupDto[];
    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _jobServiceProxy: JobsServiceProxy,
    ) {
        super(injector);
    }
    sectionName = '';
    show(jobid: number , sectionId: number,section =''): void {
        this.jobid = jobid;
        this.sectionId =sectionId;
        this.sectionName = section;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Open Meter Application Details';
        log.section = section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
        if (this.jobElecDistributors == null) {
            this._jobServiceProxy.getAllElecDistributorForTableDropdown().subscribe(result => {
                this.jobElecDistributors = result;
            });
        }
        if (this.jobElecRetailers == null) {
            this._jobServiceProxy.getAllElecRetailerForTableDropdown().subscribe(result => {
                this.jobElecRetailers = result;
            });
        }
        if (this.jobRoofAngles == null) {
            this._jobServiceProxy.getAllRoofAngleForTableDropdown().subscribe(result => {
                this.jobRoofAngles = result;
            });
        }
        if (this.allUsers == null) {
            this._jobServiceProxy.getAllUsers().subscribe(result => {
                this.allUsers = result;
            });
        }
        if(this.allPVDStatus == null)
        {
            this._jobServiceProxy.getPVDStatusList().subscribe(result => {
                this.allPVDStatus = result;
            });
        }
       
        this._jobServiceProxy.getJobForEdit(jobid).subscribe(result => {
            this.job = result.job;
            this.leadCompanyName = result.leadCompanyName;
            this.jobNumber = result.job.jobNumber;
            this.modal.show();
        });

    }
    save(): void {
        this.saving = true;
        this.job.id = this.jobid;
        this.job.sectionId = this.sectionId;
        this._jobServiceProxy.update_MeterDetails(this.job)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                let log = new UserActivityLogDto();
                log.actionId = 14;
                log.actionNote = 'Meter Details are updated ' + this.job.jobNumber;
                log.section = this.sectionName;
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);

            });
    }

    close(): void {
        this.modal.hide();
    }
}