import { Component, ElementRef, EventEmitter, Injector, Input, OnInit, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { JobsServiceProxy, GetJobForEditOutput, CreateOrEditJobDto, LeadsServiceProxy, GetLeadForViewDto } from '@shared/service-proxies/service-proxies';
import * as _ from 'lodash';
import * as moment from 'moment';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { JobsComponent } from '@app/main/jobs/jobs/jobs.component';


@Component({
    templateUrl: './gridconnectiondetail.component.html'
})

export class GridConnectionDetailComponent extends AppComponentBase implements OnInit {

    @ViewChild('jobCreateOrEdit', { static: true }) jobCreateOrEdit: JobsComponent;

    item: GetLeadForViewDto;
    @Input() SelectedLeadId: number = 0;

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _leadsServiceProxy: LeadsServiceProxy,
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.show(this._activatedRoute.snapshot.queryParams['id']);
    }

    show(leadId: number): void {
      
        this._leadsServiceProxy.getLeadForView(leadId,0).subscribe(result => {
            this.item = result;
        
            this.jobCreateOrEdit.getJobDetailByLeadId(this.item.lead,0);
        });
    }

    close(): void {

    }

}