import { Component, Injector, ViewEncapsulation, ViewChild, Input, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JobsServiceProxy, JobDto, GetLeadForViewDto, LeadsServiceProxy, InstallationServiceProxy, OrganizationUnitDto, UserServiceProxy, LeadStateLookupTableDto, JobElecDistributorLookupTableDto, JobStatusTableDto, JobJobTypeLookupTableDto, CommonLookupDto, InstallerServiceProxy, CommonLookupServiceProxy, JobTrackerServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { EntityTypeHistoryModalComponent } from '@app/shared/common/entityHistory/entity-type-history-modal.component';
import * as _ from 'lodash';
import * as moment from 'moment';
// import { JobDetailModalComponent } from '@app/main/jobs/jobs/job-detail-model.component';

import { OnInit } from '@angular/core';
import { ViewApplicationModelComponent } from '../jobs/jobs/view-application-model/view-application-model.component';
// import { AddActivityModalComponent } from '../myleads/myleads/add-activity-model.component';
// import { GridconnectionSmsemailModelComponent } from './gridconnection-smsemail-model/gridconnection-smsemail-model.component';
// import { ViewMyLeadComponent } from '../myleads/myleads/view-mylead.component';
import { AddMeterDetailModalComponent } from './add-meterdetail-modal.component';
import { Title } from '@angular/platform-browser';
import { CommentModelComponent } from '../activitylog/comment-modal.component';
import { EmailModelComponent } from '../activitylog/email-modal.component';
import { NotifyModelComponent } from '../activitylog/notify-modal.component';
import { ReminderModalComponent } from '../activitylog/reminder-modal.component';
import { SMSModelComponent } from '../activitylog/sms-modal.component';
import { ToDoModalComponent } from '../activitylog/todo-modal.component';
import { finalize } from 'rxjs/operators';

//import { JobsComponent } from './jobs.component';

@Component({
    templateUrl: './gridconnectiontracker.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class GridConnectionTrackerComponent extends AppComponentBase implements OnInit {

    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    organizationUnitlength: number = 0;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    toggle: boolean = true;

    change() {
        this.toggle = !this.toggle;
    }
    filterName = "JobNumber";

    @ViewChild('entityTypeHistoryModal', { static: true }) entityTypeHistoryModal: EntityTypeHistoryModalComponent;
    // @ViewChild('PendingInstallationDetail', {static: true}) PendingInstallationDetail: PendingInstallationDetailComponent;
    @ViewChild('viewApplicationModal', { static: true }) viewApplicationModal: ViewApplicationModelComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    // @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    // @ViewChild('griddConnectionTrackerSmsEmailModel', { static: true }) griddConnectionTrackerSmsEmailModel: GridconnectionSmsemailModelComponent;
    // @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
    @ViewChild('addMeterModal', { static: true }) addMeterModal: AddMeterDetailModalComponent;
    @ViewChild('smsModel', { static: true }) smsModel: SMSModelComponent;
    @ViewChild('emailModel', { static: true }) emailModel: EmailModelComponent;
    @ViewChild('notifyModel', { static: true }) notifyModel: NotifyModelComponent;
    @ViewChild('commentModel', { static: true }) commentModel: CommentModelComponent;
    @ViewChild('todoModal', { static: true }) todoModal: ToDoModalComponent;
    @ViewChild('ReminderModal', { static: true }) ReminderModal: ReminderModalComponent;
    
    FiltersData = false;
    advancedFiltersAreShown = false;
    stausfilterSelect = 0;
    filterText = '';
    maxPanelOnFlatFilter: number;
    maxPanelOnFlatFilterEmpty: number;
    minPanelOnFlatFilter: number;
    minPanelOnFlatFilterEmpty: number;
    maxPanelOnPitchedFilter: number;
    maxPanelOnPitchedFilterEmpty: number;
    minPanelOnPitchedFilter: number;
    minPanelOnPitchedFilterEmpty: number;
    regPlanNoFilter = '';
    lotNumberFilter = '';
    peakMeterNoFilter = '';
    offPeakMeterFilter = '';
    enoughMeterSpaceFilter = -1;
    isSystemOffPeakFilter = -1;
    suburbFilter = '';
    stateFilter = '';
    jobTypeNameFilter = '';
    jobStatusNameFilter = '';
    roofTypeNameFilter = '';
    roofAngleNameFilter = '';
    elecDistributorNameFilter = '';
    leadCompanyNameFilter = '';
    elecRetailerNameFilter = '';
    paymentOptionNameFilter = '';
    depositOptionNameFilter = '';
    meterUpgradeNameFilter = '';
    meterPhaseNameFilter = '';
    promotionOfferNameFilter = '';
    houseTypeNameFilter = '';
    financeOptionNameFilter = '';
    dateNameFilter = 'DepositeReceived';
    organizationUnit = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    OpenRecordId: number = 0;
    _entityTypeFullName = 'TheSolarProduct.Jobs.Job';
    entityHistoryEnabled = false;
   
    projectFilter = '';
    stateNameFilter = '';
    installerID = 0;
    jobStatusIDFilter: [];
    elecDistributorId = 0;
    applicationstatus = 1;
    jobTypeId = 0;
    ExpandedView: boolean = true;
    SelectedLeadId: number = 0;
    allStates: LeadStateLookupTableDto[];
    jobElecDistributors: JobElecDistributorLookupTableDto[];
    alljobstatus: JobStatusTableDto[];
    jobTypes: JobJobTypeLookupTableDto[];
    InstallerList: CommonLookupDto[];
    paymentid = 0;
    areaNameFilter = "";
    postalcodefrom = "";
    postalcodeTo = "";
    pendingGrid = 0;
    awaitingGrid = 0;
    totalGrid = 0;
    aprrovedGrid = 0;
    excelorcsvfile = 0;
    firstrowcount = 0;
    last = 0;
    nMINumber='';
    date = new Date();
    firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    // startDate = moment(this.firstDay);
    // endDate = moment().endOf('day');
    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);

    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 330;
    orgCode = '';
    
    constructor(
        injector: Injector,
        private _jobServiceProxy: JobsServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _userServiceProxy: UserServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _router: Router,
        private _jobTrackerServiceProxy: JobTrackerServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _installerServiceProxy: InstallerServiceProxy,
        private titleService: Title
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Grid Connection Tracker");
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });

        this._jobServiceProxy.getAllElecDistributorForTableDropdown().subscribe(result => {
            this.jobElecDistributors = result;
        });

        this._commonLookupService.getAllJobStatusTableDropdown().subscribe(result => {
            this.alljobstatus = result;
        });

        this._commonLookupService.getAllJobTypeForTableDropdown().subscribe(result => {
            this.jobTypes = result;
        });
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Grid Connect Tracker';
            log.section = 'Grid Connect Tracker';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.orgCode = this.allOrganizationUnits[0].code;
            this.getJobs();
        });
        this.getInstaller();
        
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  

    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }
        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }

    getInstaller() {
        this._installerServiceProxy.getAllInstallers(this.organizationUnit).subscribe(result => {
            this.InstallerList = result;
        });
    }
    changeOrgCode() {
        this.orgCode = this.allOrganizationUnits.find(x => x.id == this.organizationUnit).code;
    }
    getJobs(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._jobTrackerServiceProxy.getAllGridConnectionTracker(
            this.filterName,
            filterText_,
            this.organizationUnit,
            this.applicationstatus,
            this.stateNameFilter,
            this.elecDistributorId,
            this.jobStatusIDFilter,
            this.jobTypeId,
            this.installerID,
            this.dateNameFilter,
            this.startDate,
            this.endDate,
            this.nMINumber,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            this.shouldShow = false;

            if (result.items.length > 0) {
                this.totalGrid = result.items[0].summary.total;
                this.awaitingGrid = result.items[0].summary.awaiting;
                this.pendingGrid = result.items[0].summary.pending;
                this.aprrovedGrid = result.items[0].summary.complete;
            } else {
                this.totalGrid = 0;
                this.awaitingGrid = 0;
                this.pendingGrid = 0;
                this.aprrovedGrid = 0;
            }
        });
    }

    clear() {
        this.stateNameFilter = '';
        this.elecDistributorId = 0;
        this.jobStatusIDFilter = [];
        this.jobTypeId = 0;
        
        let date = new Date();
        let firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
        this.startDate = moment(this.date);
        this.endDate = moment(this.date);

        this.getJobs();
    }
    reloadPage($event): void {
        this.ExpandedView = true;
        this.paginator.changePage(this.paginator.getPage());
    }


    setOpenRecord(id) {
        if (id === this.OpenRecordId) { this.OpenRecordId = 0; return; }
        this.OpenRecordId = id;
    }

    showHistory(job: JobDto): void {
        this.entityTypeHistoryModal.show({
            entityId: job.id.toString(),
            entityTypeFullName: this._entityTypeFullName,
            entityTypeDescription: ''
        });
    }

    // getCount(organizationUnit: number): void {
    //     this._jobsServiceProxy.getAllApplicationTrackerCount(organizationUnit).subscribe(result => {
    //         this.pendingGrid = result.pendingGrid;
    //         this.awaitingGrid = result.awaitingGrid;
    //         this.totalGrid = result.totalGrid;
    //         this.aprrovedGrid = result.aprrovedGrid;
    //     });

    // }

    quickview(jobid): void {
        this.viewApplicationModal.show(jobid);
    }

    exportToExcel(excelorcsv): void {
        // if (this.sampleDateRange != null) {
        //     this.StartDate = this.sampleDateRange[0];
        //     this.EndDate = this.sampleDateRange[1];
        // } else {
        //     this.StartDate = null;
        //     this.EndDate = null;
        // }
        // this.excelorcsvfile = excelorcsv;
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._jobTrackerServiceProxy.getAllGridConnectionTrackerExcel(
            this.filterName,
            filterText_,
            this.organizationUnit,
            this.applicationstatus,
            this.stateNameFilter,
            this.elecDistributorId,
            this.jobStatusIDFilter,
            this.jobTypeId,
            this.installerID,
            this.dateNameFilter,
            this.startDate,
            this.endDate,
            this.nMINumber,
            excelorcsv
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    expandGrid() {
        this.ExpandedView = true;
    }

    navigateToLeadDetail(leadid): void {
        // this.ExpandedView = !this.ExpandedView;
        // this.ExpandedView = false;
        // this.SelectedLeadId = leadid;
        // this.viewLeadDetail.showDetail(leadid, '', 6);
    }

    gridConnectExportToExcel(): void {
        // if (this.sampleDateRange != null) {
        //     this.StartDate = this.sampleDateRange[0];
        //     this.EndDate = this.sampleDateRange[1];
        // } else {
        //     this.StartDate = null;
        //     this.EndDate = null;
        // }
        // this.excelorcsvfile = excelorcsv;
        // var filterText_ = this.filterText;
        // if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
        //     filterText_ = this.orgCode + this.filterText.trim();
        // }
        this._jobTrackerServiceProxy.getAllGridConnectExcel(
            "",
            "",
            this.organizationUnit,
            0,
            this.stateNameFilter,
            0,
            undefined,
            undefined,
            undefined,
            this.dateNameFilter,
            this.startDate,
            this.endDate,
            undefined,
            1
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Grid Connect Tracker';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Grid Connect Tracker';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }

}
