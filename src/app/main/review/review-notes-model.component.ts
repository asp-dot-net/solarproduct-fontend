import { Component, ElementRef, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { JobsServiceProxy, GetJobForEditOutput, CreateOrEditJobDto,GetJobReviewForEditOutput, JobElecDistributorLookupTableDto, JobElecRetailerLookupTableDto, JobRoofAngleLookupTableDto, CommonLookupDto, CreateOrEditJobReviewDto, LeadsServiceProxy, CommonLookupServiceProxy, ReviewServiceProxy, GetReviewViewDto, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import * as _ from 'lodash';
import { ModalDirective } from 'ngx-bootstrap/modal';
import {  FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { FileUpload } from 'primeng';
import { HttpClient } from '@angular/common/http';
import { AppConsts } from '@shared/AppConsts';
import {  TokenService } from 'abp-ng2-module';
@Component({
    selector: 'reviewNotesModal',
    templateUrl: './review-notes-model.component.html'
})
export class ReviewNotesModalComponent extends AppComponentBase {

    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    saving = false;
   
   orgid:number;
    trackerid = 0;
    jobid = 0;
    images = [];
    fileList: File[] = [];
    fileName: any;
    public uploader: FileUploader;
    uploadUrl: string;
    STR: string;
    CreateId: number;
    saveDoc: boolean = false;
    tenantId: number;
    allReviewType: CommonLookupDto[];
    private _uploaderOptions: FileUploaderOptions = {};
    JobReviews: CreateOrEditJobReviewDto= new CreateOrEditJobReviewDto();
    item: GetJobForEditOutput;
    documents: GetJobReviewForEditOutput[] = [];
    leadCompanyName: any;
    constructor(
        injector: Injector,
        private _jobsServiceProxy: JobsServiceProxy,
        private _httpClient: HttpClient,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _tokenService: TokenService,
        private _commonLookupService: CommonLookupServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _reviewServiceProxy: ReviewServiceProxy
    ) {
        super(injector);
        this.item = new GetJobForEditOutput();
        this.item.job = new CreateOrEditJobDto();
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/api/Document/UploadReviewNoteDocuments';
    }

  SectionName = '';

  show(jobid: number, orgid: number, section: string, trackerid: number = 0) {
    this._commonLookupService.getReviewTypeDropdown().subscribe(result => {
        this.allReviewType = result;
    });
    this.CreateId=this.appSession.userId;
    this.jobid = jobid;
    this.orgid = orgid;
    this.tenantId = this.appSession.tenantId;
    this.JobReviews = new CreateOrEditJobReviewDto();
    this.getDocumentList();

    this.modal.show();
}


    getDocumentList() {
        this._reviewServiceProxy.getAllJobReview(this.jobid).subscribe(result => {
            this.documents = result.items;
        });
    }
    deleteJobReview(id: number): void {
        this.message.confirm(
            this.l('AreYouSureWanttoDelete'),
            this.l('Delete'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._reviewServiceProxy.deleteJobReview(id)
                        .subscribe(() => { 
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            this.getDocumentList();
                        });
                }
            }
        );
    }
    downloadfile(file,filename): void {
        debugger;
        let FileName = AppConsts.docUrl + file+ "/" +filename;
        window.open(FileName, "_blank");
    }

    ResetFileInput(){
        this.fileList = []; 
        const fileInput = document.querySelector('.fileuploadboxbtn') as HTMLInputElement;
        if (fileInput) {
            fileInput.value = ''; 
        } 
        this.JobReviews.reviewTypeId=0;
        this.JobReviews.rating=0;
        this.JobReviews.reviewNotes="";
    }
   
    onFileChange(event) {
        debugger
       
        if (event.target.files && event.target.files[0]) {
            const file = event.target.files[0];
            
            var filesAmount = event.target.files.length;
            for (let i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = (event: any) => {
                    this.images.push(event.target.result);
                }

                reader.readAsDataURL(event.target.files[i]);

                const file = event.target.files[i];
                this.fileList.push(file)
            }
        }
    }
    save() {
        debugger;
       
        this.STR = `${this.tenantId},${this.jobid},${this.JobReviews.reviewTypeId},${this.JobReviews.rating},${this.JobReviews.reviewNotes},${this.orgid},${this.CreateId}`;
        this.showMainSpinner();
        const formData = new FormData();
        if (this.fileList.length > 0) {
            for (let i = 0; i < this.fileList.length; i++) {
                const file = this.fileList[i];
                let name = this.fileName + "_" + i;
                formData.append(name, file, file.name);
            }
            this._httpClient
                .post<any>(this.uploadUrl + "?str=" + encodeURIComponent(this.STR), formData)
                .subscribe(response => {
                    if (response.success) {
                        this.saveDoc = true;
                        this.getDocumentList();
                        this.ResetFileInput();
                    } else if (response.error != null) {
                        this.notify.error(this.l('Upload failed.'));
                    }
                    this.hideMainSpinner();
                }, e => {
                    this.hideMainSpinner();
                });
        } else {
            this.notify.info("Please select an image to upload.", "Empty");
        }
    }
    // save(): void {
    //     // console.log('this.JobReviews', this.JobReviews.length);
    //     // if (this.JobReviews.length > 0) {
    //     //     alert("btn unable");
    //     // }

    //     this.saving = true;
    //     // this.JobReviews.forEach(item => {
    //     //     item.jobId = this.jobid;
    //     // });
    //     // this.review = this.JobReviews;
    //     // this._reviewServiceProxy.updateReview(this.review)
    //     //     .pipe(finalize(() => { this.saving = false; }))
    //     //     .subscribe(() => {
    //     //         let log = new UserActivityLogDto();
    //     //         log.actionId = 28;
    //     //         log.actionNote = "Review Added";
    //     //         log.section = this.SectionName;
    //     //         this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
    //     //             .subscribe(() => {
    //     //         });
    //     //         this.close();
    //     //         this.modalSave.emit(null);
    //     //         this.notify.info(this.l('SavedSuccessfully'));
    //     //         this.comment = "";
    //     //     });
    // }

    close(): void {
        this.modal.hide();
    }

    // addJobReview(): void {
    //     this.JobReviews.push(new CreateOrEditJobReviewDto);
    // }

    // removeJobReview(rowitem): void {
    //     if (this.JobReviews.length == 1)
    //         return;

    //     if (this.JobReviews.indexOf(rowitem) === -1) {
    //     } else {
    //         this.JobReviews.splice(this.JobReviews.indexOf(rowitem), 1);
    //     }
    // }
}