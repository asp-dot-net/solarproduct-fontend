import { Component, Injector, ViewChild, ViewEncapsulation, HostListener } from '@angular/core';
import { CommonLookupDto, CommonLookupServiceProxy, JobElecDistributorLookupTableDto, JobJobTypeLookupTableDto, JobStatusTableDto, LeadsServiceProxy, LeadStateLookupTableDto, LeadUsersLookupTableDto, OrganizationUnitDto, ReviewServiceProxy, TokenAuthServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy, UserServiceProxy } from '@shared/service-proxies/service-proxies';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import * as _ from 'lodash';
import * as moment from 'moment';
import { HttpClient } from '@angular/common/http';
import { OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { finalize } from 'rxjs/operators';
import { Title } from '@angular/platform-browser';
import { ViewMyLeadComponent } from '../myleads/myleads/view-mylead.component';
import { ReviewSmsemailModelComponent } from './review/review-smsemail-model.component';
import { AddActivityModalComponent } from '../myleads/myleads/add-activity-model.component';
import { ViewApplicationModelComponent } from '../jobs/jobs/view-application-model/view-application-model.component';
import { ReviewNotesModalComponent } from './review-notes-model.component';
import { CommentModelComponent } from '../activitylog/comment-modal.component';
import { EmailModelComponent } from '../activitylog/email-modal.component';
import { NotifyModelComponent } from '../activitylog/notify-modal.component';
import { ReminderModalComponent } from '../activitylog/reminder-modal.component';
import { SMSModelComponent } from '../activitylog/sms-modal.component';
import { ToDoModalComponent } from '../activitylog/todo-modal.component';

@Component({
    selector: 'app-review',
    templateUrl: './review.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ReviewComponent extends AppComponentBase implements OnInit {

    saving = false;
    show: boolean = true;
    readunreadsmstag = 0;
    showchild: boolean = true;
    shouldShow: boolean = false;
    organizationUnitlength: number = 0;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };
    @ViewChild('viewApplicationModal', { static: true }) viewApplicationModal: ViewApplicationModelComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
    @ViewChild('reviewSmsEmailModel', { static: true }) reviewSmsEmailModel: ReviewSmsemailModelComponent;
    @ViewChild('reviewNotesModal', { static: true }) reviewNotesModal: ReviewNotesModalComponent;
    
    @ViewChild('notifyModel', { static: true }) notifyModel: NotifyModelComponent;
    @ViewChild('commentModel', { static: true }) commentModel: CommentModelComponent;
    @ViewChild('todoModal', { static: true }) todoModal: ToDoModalComponent;
    @ViewChild('ReminderModal', { static: true }) ReminderModal: ReminderModalComponent;
    
    rate = 0;
    FiltersData = true;
    advancedFiltersAreShown = false;
    stausfilterSelect = 0;
    filterText = '';
    ExpandedView: boolean = true;
    organizationUnit = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    _entityTypeFullName = 'TheSolarProduct.Jobs.Job';
    entityHistoryEnabled = false;
    // StartDate: moment.Moment;
    // EndDate: moment.Moment;
    projectFilter = '';
    stateNameFilter = '';
    // sampleDateRange: moment.Moment[] = [moment().add(-6, 'days').endOf('day'), moment().add(+1, 'days').startOf('day')];
    jobStatusIDFilter: [];
    jobTypeId = 0;
    SelectedLeadId: number = 0;
    tableRecords: any;
    excelorcsvfile = 0;
    firstrowcount = 0;
    last = 0;
    assignservice = 0;
    allStates: LeadStateLookupTableDto[];
    jobElecDistributors: JobElecDistributorLookupTableDto[];
    alljobstatus: JobStatusTableDto[];
    jobTypes: JobJobTypeLookupTableDto[];
    reviewrate = 0;
    total = 0;
    yes = 0;
    no = 0;
    count = 0;
    role: string = '';
    emailsend = 0;
    isAssign = 2;
    reviewType = 0;
    allServiceReviewUsers: LeadUsersLookupTableDto[];
    allReviewType: CommonLookupDto[];
    totalemailsend = 0;
    sendemail = 0;
    pendingemail = 0;
    reviewdatefilter = "InstalledCompleteDate";
    isreviewtypecolumnshow = false;
    isproductreviewtypecolumnshow = false;
    filterName = 'JobNumber';
    orgCode = '';
    date = new Date();
    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);
    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 330;
    toggle: boolean = true;

    change() {
        this.toggle = !this.toggle;
    }

    constructor(
        injector: Injector,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private titleService: Title,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _reviewServiceProxy: ReviewServiceProxy,

    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Review");
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
        this._commonLookupService.getAllJobStatusTableDropdown().subscribe(result => {
            this.alljobstatus = result;
        });

        this._commonLookupService.getAllJobTypeForTableDropdown().subscribe(result => {
            this.jobTypes = result;
        });
        this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
            this.role = result;
        });
        this._commonLookupService.getReviewTypeDropdown().subscribe(result => {
            this.allReviewType = result;
            // this.allReviewType.forEach(item => {
            //     if (item.displayName == 'Google Review') {
            //         this.reviewType = item.id;
            //     }
            // });
        });
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Review';
            log.section = 'Review';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.orgCode = this.allOrganizationUnits[0].code;
            this.getusers();
            this.getReview();
        });
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + 82 ;
        }
        else {
            this.testHeight = this.testHeight - 82 ;
        }
    }
    
    getusers() {
        this._commonLookupService.getServiceReviewUserForFilter(this.organizationUnit).subscribe(result => {
            this.allServiceReviewUsers = result;
        });
    }
    clear() {
        this.filterText = '';
        // this.sampleDateRange = [moment().add(-7, 'days').endOf('day'), moment().startOf('day')];
        this.startDate =  moment(this.date);
        this.endDate =  moment(this.date);
        this.getReview();
    }

    changeOrgCode() {
        this.orgCode = this.allOrganizationUnits.find(x => x.id == this.organizationUnit).code;
    }

    getReview(event?: LazyLoadEvent) {
        debugger;
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        // this.StartDate = null;
        // this.EndDate = null;
        // if(this.sampleDateRange !=null){
        //     this.StartDate = this.sampleDateRange[0];
        //     this.EndDate = this.sampleDateRange[1];
        // }
        this.organizationUnit,
        this.primengTableHelper.showLoadingIndicator();
        
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._reviewServiceProxy.getReviewData(
            this.filterName,
            filterText_,
            this.organizationUnit,
            this.stateNameFilter,
            this.jobStatusIDFilter,
            this.jobTypeId,
            this.startDate,
            this.endDate,
            this.reviewrate,
            this.isAssign,
            this.reviewType,
            this.emailsend,
            this.reviewdatefilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();

            if (this.reviewType != 0) {
                if (this.primengTableHelper.records[0].job.reviewType != null) {
                    this.isreviewtypecolumnshow = true;
                } else {
                    this.isreviewtypecolumnshow = false;
                }
                if (this.primengTableHelper.records[0].job.productReviewType != null) {
                    this.isproductreviewtypecolumnshow = true;
                } else {
                    this.isproductreviewtypecolumnshow = false;
                }
            } else {
                this.isreviewtypecolumnshow = true;
                this.isproductreviewtypecolumnshow = true;
            }
            if (result.totalCount > 0) {
                this.total = result.totalCount;
                // this.yes = result.items[0].yes;
                // this.no = result.totalCount[0].no;
                this.totalemailsend = result.items[0].totalEmailSend;
                this.sendemail = result.items[0].emailSend;
                this.pendingemail = result.items[0].emailPending;
                this.rate = parseFloat((result.items[0].totalRating / result.items[0].totalraterecords).toFixed(1));
            } else {
                this.total = 0;
                this.totalemailsend = 0;
                this.sendemail = 0;
                this.pendingemail = 0;
            }
            this.shouldShow = false;
        });
    }


    isAllChecked() {
        if (this.tableRecords)
            return this.tableRecords.every(_ => _.isSelected);
    }
    checkAll(ev) {

        this.tableRecords.forEach(x => x.isSelected = ev.target.checked);
        this.oncheckboxCheck();
        /// this.totalcount = this.tableRecords.forEach(x => x.lead.isSelected = ev.target.checked);
    }

    oncheckboxCheck() {

        this.count = 0;
        this.tableRecords.forEach(item => {
            if (item.isSelected == true) {
                this.count = this.count + 1;
            }
        })
    }
    submit(): void {
        debugger;
        let selectedids = [];
        // this.saving = true;
        this.primengTableHelper.records.forEach(function (job) {
            if (job.isSelected) {
                selectedids.push(job.service.id);
            }
        });
        if (selectedids.length == 0) {
            this.notify.warn(this.l('PleaseSelectRecord'));
            return;
        }
        if (this.assignservice == 0) {
            this.notify.warn(this.l('PleaseSelectUser'));
            return;
        }
        this._reviewServiceProxy.updateAssignUserID(this.assignservice, selectedids)
            .pipe(finalize(() => { }))
            .subscribe(() => {
                let log = new UserActivityLogDto();
                log.actionId = 82;
                log.actionNote = 'Job ReAssigned';
                log.section = 'Review';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
                this.reloadPage(null);
                this.notify.info(this.l('AssignedSuccessfully'));
            });
    }
reviewNotes(id){
    this.reviewNotesModal.show(id, this.organizationUnit, 'Review', 23);

}
    expandGrid() {
        this.ExpandedView = true;
    }

    navigateToLeadDetail(leadid): void {
        this.ExpandedView = !this.ExpandedView;
        this.ExpandedView = false;
        this.SelectedLeadId = leadid;
        this.viewLeadDetail.showDetail(leadid, '', 6, 0,'Review');
    }
    exportToExcel(): void {

    }
    reloadPage(event): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Review';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Review';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }

}
