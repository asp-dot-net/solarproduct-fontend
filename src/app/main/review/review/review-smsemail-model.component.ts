import { Component, ElementRef, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivityLogServiceProxy, CommonLookupDto, EmailTemplateServiceProxy, GetLeadForSMSEmailDto, GetLeadForSMSEmailTemplateDto, GetLeadForViewDto, JobsServiceProxy, LeadDto, LeadsServiceProxy, ReviewServiceProxy, SmsEmailDto, SmsTemplatesServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { EmailEditorComponent } from 'angular-email-editor';
import * as _ from 'lodash';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'reviewSmsEmailModel',
  templateUrl: './review-smsemail-model.component.html',
})
export class ReviewSmsemailModelComponent extends AppComponentBase implements OnInit {
  @ViewChild("myNameElem") myNameElem: ElementRef;
  @ViewChild('addModal', { static: true }) modal: ModalDirective;
  ///@ViewChild('viewLeadDetail', {static: true}) viewLeadDetail: ViewMyLeadComponent;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  saving = false;
  id = 0;
  allEmailTemplates: CommonLookupDto[];
  allSMSTemplates: CommonLookupDto[];
  emailData = '';
  smsData = '';
  emailTemplate: number;
  uploadUrl: string;
  uploadedFiles: any[] = [];
  myDate = new Date();
  activityType: number;
  //item: GetLeadForViewDto;
  activityLog: SmsEmailDto = new SmsEmailDto();
  activityName = "";
  total = 0;
  credit = 0;
  customeTagsId = 0;
  leadCompanyName: any;
  role: string = '';
  subject = '';
  emailfrom = "";
  fromemails: CommonLookupDto[];
  criteriaavaibaleornot = false;
  ccbox = false;
  bccbox = false;
  lead : GetLeadForSMSEmailDto = new GetLeadForSMSEmailDto();
  item : GetLeadForSMSEmailTemplateDto = new GetLeadForSMSEmailTemplateDto();
  // viewLeadDetail: ViewMyLeadComponent;
  constructor(
    injector: Injector,
    private _leadsServiceProxy: LeadsServiceProxy,
    private _emailTemplateServiceProxy: EmailTemplateServiceProxy,
    private _smsTemplateServiceProxy: SmsTemplatesServiceProxy,
    private _router: Router,
    private _jobsServiceProxy: JobsServiceProxy,
    private _reviewServiceProxy: ReviewServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _activityLogServiceProxy : ActivityLogServiceProxy
    ) {
    super(injector);
    // this.item = new GetLeadForViewDto();
    // this.item.lead = new LeadDto();
  }

  @ViewChild(EmailEditorComponent)
  private emailEditor: EmailEditorComponent;

  ///private viewLeadDetail: ViewMyLeadComponent;

  ngOnInit(): void {
    this.activityLog.emailTemplateId = 0;
    this.activityLog.smsTemplateId = 0;
    this.activityLog.customeTagsId = 0;
    this.activityLog.subject = '';

  }

  SectionName = '';
  show(leadId: number, id: number, trackerid?: number, section = ''): void {
    this.SectionName = section;
    let log = new UserActivityLogDto();
    log.actionId = 79;
    log.actionNote = id == 1 ? "Open For SMS" : "Open For Email";
    log.section = section;
    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {
    });
    this.showMainSpinner();
      this.item = new GetLeadForSMSEmailTemplateDto();
      this.lead = new GetLeadForSMSEmailDto();
    this.activityLog.leadId = leadId;
    this.activityLog.emailTemplateId = 0;
    this.activityLog.smsTemplateId = 0;
    this.activityLog.customeTagsId = 0;
    this.activityLog.subject = '';
    this.activityLog.body = "";
    this.activityLog.trackerId = trackerid;
    if (id == 1) {
      this.id = id;
      this.activityType = 1;
      this._leadsServiceProxy.checkorgwisephonedynamicavaibleornot(leadId).subscribe(result => {
        this.criteriaavaibaleornot = result;
      });
    }
    else if (id == 2) {
      this.id = id;
      this.activityType = 2;
      this._leadsServiceProxy.getOrgWiseDefultandownemailadd(leadId).subscribe(result => {
        debugger;
        this.fromemails = result;
        this.activityLog.emailFrom = this.fromemails[0].displayName;
      });
    }
    // this._leadsServiceProxy.getLeadForView(leadId, 0).subscribe(result => {
    //   this.item = result;
    //   this.leadCompanyName = result.lead.companyName;
    //   this.selection();
    //   if (!this.criteriaavaibaleornot && id == 1) {
    //     this.message.error("authentication issue");
    //   } else {
    //     this.modal.show();
    //   }
    // });
    this._activityLogServiceProxy.getLeadForSMSEmailActivity(leadId).subscribe(result => {
      this.lead = result;
      // this.header = result.companyName + (result.jobnumber != "" ? " - " + result.jobnumber : "")
      this.item = new GetLeadForSMSEmailTemplateDto();
        this.leadCompanyName = result.companyName;
      this.selection();
      if(!this.criteriaavaibaleornot && id == 1)
      {
        this.message.error("authentication issue");
      this.hideMainSpinner();

      } else {
        this.modal.show();
      this.hideMainSpinner();

      }
  },err => {
    this.hideMainSpinner();
})
    this._leadsServiceProxy.getallEmailTemplates(leadId).subscribe(result => {
      this.allEmailTemplates = result;
    });

    this._leadsServiceProxy.getallSMSTemplates(leadId).subscribe(result => {
      this.allSMSTemplates = result;
    });

    this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
      this.role = result;
    });

  }

  selection(): void {

    this.activityLog.body = '';
    this.activityLog.emailTemplateId = 0;
    this.activityLog.customeTagsId = 0;
    this.activityLog.subject = '';
    if (this.activityType == 1) {
      this.activityName = "SMS To ";
    }
    else if (this.activityType == 2) {
      this.activityName = "Email To ";
    }
  }

  save(): void {

    if (this.activityType == 1) {
      this.saving = true;
      if (this.role != 'Admin') {
        if (this.total > 320) {
          this.notify.warn(this.l('You Can Not Add more than 320 characters'));
          this.saving = false;
        } else {
          this._reviewServiceProxy.sendSms(this.activityLog)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
              let log = new UserActivityLogDto();
              log.actionId = 6;
              log.actionNote = 'SMS Sent';
              log.section = this.SectionName;
              this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                  .subscribe(() => {
              });
              this.modal.hide();
              this.notify.info(this.l('SmsSendSuccessfully'));
              this.modalSave.emit(null);
              this.activityLog.body = "";
              this.activityLog.emailTemplateId = 0;
              this.activityLog.smsTemplateId = 0;
              this.activityLog.customeTagsId = 0;
              this.activityLog.subject = '';
            });
        }
      }
      else {
        this._reviewServiceProxy.sendSms(this.activityLog)
          .pipe(finalize(() => { this.saving = false; }))
          .subscribe(() => {
            let log = new UserActivityLogDto();
            log.actionId = 6;
            log.actionNote = 'SMS Sent';
            log.section = this.SectionName;
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.modal.hide();
            this.notify.info(this.l('SmsSendSuccessfully'));
            this.modalSave.emit(null);
            this.activityLog.body = "";
            this.activityLog.emailTemplateId = 0;
            this.activityLog.smsTemplateId = 0;
            this.activityLog.customeTagsId = 0;
            this.activityLog.subject = '';
          });
      }
    }
    else {
      this.saving = true;
      this._reviewServiceProxy.sendEmail(this.activityLog)
        .pipe(finalize(() => { this.saving = false; }))
        .subscribe(() => {
          let log = new UserActivityLogDto();
              log.actionId = 7;
              log.actionNote = 'Email Sent';
              log.section = this.SectionName;
              this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                  .subscribe(() => {
              });
          this.notify.info(this.l('EmailSendSuccessfully'));
          this.modal.hide();
          this.modalSave.emit(null);
          this.activityLog.body = "";
          this.activityLog.emailTemplateId = 0;
          this.activityLog.smsTemplateId = 0;
          this.activityLog.customeTagsId = 0;
          this.activityLog.subject = '';
        });
    }
  }

  countCharcters(): void {

    if (this.role != 'Admin') {
      this.total = this.activityLog.body.length;
      this.credit = Math.ceil(this.total / 160);
      if (this.total > 320) {
        this.notify.warn(this.l('You Can Not Add more than 320 characters'));
      }
    }
    else {
      this.total = this.activityLog.body.length;
      this.credit = Math.ceil(this.total / 160);
    }


  }
  close(): void {
    this.modal.hide();
  }
  opencc(): void {
    this.ccbox = !this.ccbox;
  }
  openbcc(): void {
    this.bccbox = !this.bccbox;
  }

  onTagChange(event): void {

    const id = event.target.value;
    if (id == 1) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Customer.Name}}";
      } else {
        this.activityLog.body = "{{Customer.Name}}";
      }

    } else if (id == 2) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Customer.Mobile}}";
      } else {
        this.activityLog.body = "{{Customer.Mobile}}";
      }
    } else if (id == 3) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Customer.Phone}}";
      } else {
        this.activityLog.body = "{{Customer.Phone}}";
      }
    } else if (id == 4) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Customer.Email}}";
      } else {
        this.activityLog.body = "{{Customer.Email}}";
      }
    } else if (id == 5) {

      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Customer.Address}}";
      } else {
        this.activityLog.body = "{{Customer.Address}}";
      }
    } else if (id == 6) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Sales.Name}}";
      } else {
        this.activityLog.body = "{{Sales.Name}}";
      }
    } else if (id == 7) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Sales.Mobile}}";
      } else {
        this.activityLog.body = "{{Sales.Mobile}}";
      }
    } else if (id == 8) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Sales.Email}}";
      } else {
        this.activityLog.body = "{{Sales.Email}}";
      }
    }
    else if (id == 9) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Quote.ProjectNo}}";
      } else {
        this.activityLog.body = "{{Quote.ProjectNo}}";
      }
    }
    else if (id == 10) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Quote.SystemCapacity}}";
      } else {
        this.activityLog.body = "{{Quote.SystemCapacity}}";
      }
    }
    else if (id == 11) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Quote.AllproductItem}}";
      } else {
        this.activityLog.body = "{{Quote.AllproductItem}}";
      }
    }
    else if (id == 12) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Quote.TotalQuoteprice}}";
      } else {
        this.activityLog.body = "{{Quote.TotalQuoteprice}}";
      }
    }
    else if (id == 13) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Quote.InstallationDate}}";
      } else {
        this.activityLog.body = "{{Quote.InstallationDate}}";
      }
    }
    else if (id == 14) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Quote.InstallerName}}";
      } else {
        this.activityLog.body = "{{Quote.InstallerName}}";
      }
    }
    else if (id == 15) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Freebies.TransportCompany}}";
      } else {
        this.activityLog.body = "{{Freebies.TransportCompany}}";
      }
    }
    else if (id == 16) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Freebies.TransportLink}}";
      } else {
        this.activityLog.body = "{{Freebies.TransportLink}}";
      }
    }
    else if (id == 17) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Freebies.DispatchedDate}}";
      } else {
        this.activityLog.body = "{{Freebies.DispatchedDate}}";
      }
    }
    else if (id == 18) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Freebies.TrackingNo}}";
      } else {
        this.activityLog.body = "{{Freebies.TrackingNo}}";
      }
    }
    else if (id == 19) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Freebies.PromoType}}";
      } else {
        this.activityLog.body = "{{Freebies.PromoType}}";
      }
    }
    else if (id == 20) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Invoice.UserName}}";
      } else {
        this.activityLog.body = "{{Invoice.UserName}}";
      }
    }
    else if (id == 21) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Invoice.UserMobile}}";
      } else {
        this.activityLog.body = "{{Invoice.UserMobile}}";
      }
    }
    else if (id == 22) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Invoice.UserEmail}}";
      } else {
        this.activityLog.body = "{{Invoice.UserEmail}}";
      }
    }
    else if (id == 23) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Invoice.Owning}}";
      } else {
        this.activityLog.body = "{{Invoice.Owning}}";
      }
    }
    else if (id == 24) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Organization.orgName}}";
      } else {
        this.activityLog.body = "{{Organization.orgName}}";
      }
    }
    else if (id == 25) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Organization.orgEmail}}";
      } else {
        this.activityLog.body = "{{Organization.orgEmail}}";
      }
    }
    else if (id == 26) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Organization.orgMobile}}";
      } else {
        this.activityLog.body = "{{Organization.orgMobile}}";
      }
    }
    else if (id == 27) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Organization.orglogo}}";
      } else {
        this.activityLog.body = "{{Organization.orglogo}}";
      }
    }
    else if (id == 28) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Review.link}}";
      } else {
        this.activityLog.body = "{{Review.link}}";
      }
    }
  }



  saveDesign() {
    if (this.activityLog.emailTemplateId == 0) {
      this.saving = true;
      const emailHTML = this.activityLog.body;
      this.setHTML(emailHTML)
    }
    else {
      this.saving = true;
      this.emailEditor.editor.exportHtml((data) =>
        this.setHTML(data.html)
      );
    }
  }

  setHTML(emailHTML) {
    // let htmlTemplate = this.getEmailTemplate(emailHTML);
    // this.activityLog.body = htmlTemplate;
    // this.save();
    var ed = '';
        ed = emailHTML;
        if(!ed.includes('{{')){
          this.activityLog.body = emailHTML;
          this.save();
          return;
        }
        this.showMainSpinner();
              if(!this.item.id || this.item.id == 0){
                this._activityLogServiceProxy.getLeadForSMSEmailTemplate(this.activityLog.leadId,0,0).subscribe(result => {
                  this.item = result;
                  let htmlTemplate = this.getEmailTemplate(emailHTML,result);
                  this.activityLog.body = htmlTemplate;
                  this.save();
                  this.hideMainSpinner(); 
                }, err => {
                    this.hideMainSpinner(); 
                });
              }
              else{
                let htmlTemplate = this.getEmailTemplate(emailHTML,this.item);
                this.activityLog.body = htmlTemplate;
                this.save();
                this.hideMainSpinner(); 
              }
  }

  getEmailTemplate(emailHTML,templateData : GetLeadForSMSEmailTemplateDto) {
    //let myTemplateStr = "Hello {{user.name}}, you are {{user.age.years}} years old";
    let myTemplateStr = emailHTML;
    //let addressValue = this.item.lead.address + " " + this.item.lead.suburb + ", " + this.item.lead.state + "-" + this.item.lead.postCode;
    let myVariables = {
      Customer: {
        Name: templateData.companyName,
        Address: templateData.address,
        Mobile: templateData.mobile,
        Email: templateData.email,
        Phone: templateData.phone,
        SalesRep: templateData.currentAssignUserName
      },
      Sales: {
        Name: templateData.currentAssignUserName,
        Mobile: templateData.currentAssignUserMobile,
        Email: templateData.currentAssignUserEmail
      },
      Quote: {
        ProjectNo: templateData.jobNumber,
        SystemCapacity: templateData.systemCapacity,
        AllproductItem: templateData.qunityAndModelList,
        TotalQuoteprice: templateData.totalQuotaion,
        InstallationDate: templateData.installationDate,
        InstallerName: templateData.installerName,
      },
      Freebies: {
        DispatchedDate: templateData.dispatchedDate,
        TrackingNo: templateData.trackingNo,
        TransportCompany: templateData.transportCompanyName,
        TransportLink: templateData.transportLink,
        PromoType: templateData.promoType,
      },
      Invoice: {
        UserName: templateData.userName,
        UserMobile: templateData.userMobile,
        UserEmail: templateData.userEmail,
        Owning: templateData.owing,
    },
    Organization: {
      orgName: templateData.orgName,
      orgEmail: templateData.orgEmail,
      orgMobile: templateData.orgMobile,
      orglogo: AppConsts.docUrl + "/" + templateData.orglogo,
    },
    Review: {
      link: templateData.link,
     },
    }

    // use custom delimiter {{ }}
    _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;

    // interpolate
    let compiled = _.template(myTemplateStr);
    let myTemplateCompiled = compiled(myVariables);
    return myTemplateCompiled;
  }

  editorLoaded() {

    this.activityLog.body = "";
    if ((this.activityLog.emailTemplateId != 0 && this.activityLog.emailTemplateId !== null && this.activityLog.emailTemplateId !== undefined)) {
      this._jobsServiceProxy.getEmailTemplateForEditForEmail(this.activityLog.emailTemplateId).subscribe(result => {
        this.activityLog.subject = result.emailTemplate.subject;
        this.emailData = result.emailTemplate.body;
        if (this.emailData != "") {
          // this.emailData = this.getEmailTemplate(this.emailData);
          // this.emailEditor.editor.loadDesign(JSON.parse(this.emailData));
          this.showMainSpinner();
          if(!this.item.id || this.item.id == 0){
            this._activityLogServiceProxy.getLeadForSMSEmailTemplate(this.activityLog.leadId,0,0).subscribe(result => {
              this.item = result;
              this.emailData = this.getEmailTemplate(this.emailData,result);
              this.emailEditor.editor.loadDesign(JSON.parse(this.emailData));
              this.hideMainSpinner(); 
            }, err => {
                this.hideMainSpinner(); 
            });
          }
          else{
            this.emailData = this.getEmailTemplate(this.emailData, this.item);
            this.emailEditor.editor.loadDesign(JSON.parse(this.emailData));
            this.hideMainSpinner();
          }
        }
      });
    }

  }

  smseditorLoaded() {

    this.activityLog.body = "";
    if ((this.activityLog.smsTemplateId != 0 && this.activityLog.smsTemplateId !== null && this.activityLog.smsTemplateId !== undefined)) {
      this._jobsServiceProxy.getSmsTemplateForEditForSms(this.activityLog.smsTemplateId).subscribe(result => {
        this.smsData = result.smsTemplate.text;
        if (this.smsData != "") {
          this.setsmsHTML(this.smsData)
        }
      });
    }

  }


  setsmsHTML(smsHTML) {
    // let htmlTemplate = this.getsmsTemplate(smsHTML);
    // this.activityLog.body = htmlTemplate;
    // this.countCharcters();
    this.showMainSpinner();
    if(!this.item.id || this.item.id == 0){
      this._activityLogServiceProxy.getLeadForSMSEmailTemplate(this.activityLog.leadId,0,0).subscribe(result => {
        this.item = result;
        let htmlTemplate = this.getsmsTemplate(smsHTML,result);
        this.activityLog.body = htmlTemplate;
        this.countCharcters();
        this.hideMainSpinner(); 
      }, err => {
          this.hideMainSpinner(); 
      });
    }
    else{
      let htmlTemplate = this.getsmsTemplate(smsHTML,this.item);
      this.activityLog.body = htmlTemplate;
      this.countCharcters();
      this.hideMainSpinner();
    }
  }

  getsmsTemplate(smsHTML,templateData : GetLeadForSMSEmailTemplateDto) {
    //let myTemplateStr = "Hello {{user.name}}, you are {{user.age.years}} years old";
    let myTemplateStr = smsHTML;
    //let addressValue = this.item.lead.address + " " + this.item.lead.suburb + ", " + this.item.lead.state + "-" + this.item.lead.postCode;
    let myVariables = {
      Customer: {
        Name: templateData.companyName,
        Address: templateData.address,
        Mobile: templateData.mobile,
        Email: templateData.email,
        Phone: templateData.phone,
        SalesRep: templateData.currentAssignUserName
      },
      Sales: {
        Name: templateData.currentAssignUserName,
        Mobile: templateData.currentAssignUserMobile,
        Email: templateData.currentAssignUserEmail
      },
      Quote: {
        ProjectNo: templateData.jobNumber,
        SystemCapacity: templateData.systemCapacity,
        AllproductItem: templateData.qunityAndModelList,
        TotalQuoteprice: templateData.totalQuotaion,
        InstallationDate: templateData.installationDate,
        InstallerName: templateData.installerName,
      },
      Freebies: {
        DispatchedDate: templateData.dispatchedDate,
        TrackingNo: templateData.trackingNo,
        TransportCompany: templateData.transportCompanyName,
        TransportLink: templateData.transportLink,
        PromoType: templateData.promoType,
      },
      Invoice: {
        UserName: templateData.userName,
        UserMobile: templateData.userMobile,
        UserEmail: templateData.userEmail,
        Owning: templateData.owing,
    },
    Organization: {
      orgName: templateData.orgName,
      orgEmail: templateData.orgEmail,
      orgMobile: templateData.orgMobile,
      orglogo: AppConsts.docUrl + "/" + templateData.orglogo,
    },
    Review: {
      link: templateData.link,
     },
    }

    // use custom delimiter {{ }}
    _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;

    // interpolate
    let compiled = _.template(myTemplateStr);
    let myTemplateCompiled = compiled(myVariables);
    return myTemplateCompiled;
  }
}





