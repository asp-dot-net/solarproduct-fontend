import { Component, ElementRef, Injector, Optional, ViewChild } from '@angular/core';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import {  LeadGenerationServiceProxy , CreateOrEditLeadGenAppointmentDto,  UserServiceProxy, CommonLookupServiceProxy , CommonLookupDto, LeadsServiceProxy, UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { MyLeadGenerationComponent } from './myLeadGeneration.component';
import { finalize } from 'rxjs/operators';
import * as moment from 'moment';
@Component({
    selector: 'CreateEditMyLeadGenModal',
    templateUrl: './create-edit-myLeadGeneration-modal.component.html',
})
export class CreateEditMyLeadGenModal extends AppComponentBase {
    @ViewChild('CreateEditMyLeadGenModal', { static: true }) modal: ModalDirective;
    @ViewChild("myNameElem") myNameElem: ElementRef;

    ExpandedViewApp: boolean = true;
    active = false; 
    saving = true;  
    leadId = 0;
    leadGenAppointment : CreateOrEditLeadGenAppointmentDto =new CreateOrEditLeadGenAppointmentDto();
    userList: CommonLookupDto[];
    sectionId : number;
    // roleName = ['Leadgen manager', 'Leadgen SalesRep'];
    //currRoleName = '';

    constructor(injector: Injector
        , private _dateTimeService: DateTimeService
        , private _leadGenerationServiceProxy : LeadGenerationServiceProxy
        ,private _userServiceProxy : UserServiceProxy
        ,private _userActivityLogServiceProxy : UserActivityLogServiceProxy
        , private _commonLookupService : CommonLookupServiceProxy
        , private _leadsServiceProxy : LeadsServiceProxy
        , @Optional() private viewLeadDetail?: MyLeadGenerationComponent) {
        super(injector);
    }

  SectionName = '';
  show(leadId : number , orgId? : number , sectionId? : number, section = '') {
        this.SectionName = section;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Open For Create Appointment';
        log.section = section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
        this.leadGenAppointment = new CreateOrEditLeadGenAppointmentDto();
        this.leadGenAppointment.notes = "";
        this.leadGenAppointment.appointmentFor= null;
        this.myNameElem.nativeElement.value = '';

        this.leadId = leadId;
        this.sectionId = sectionId; 
        // let assignUserRole = ['Leadgen SalesRep'];
        this._commonLookupService.getAllUsersForAppointment(orgId).subscribe(result => {
            this.userList = result;
        });
        this.modal.show();        
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;                
    }

    expandApp() {
        this.ExpandedViewApp = false;
    }

    appointmentSelect(){
        this.leadGenAppointment.appointmentDate = this.myNameElem.nativeElement.value.toString();
        this._leadGenerationServiceProxy.checkAppointmentExist(this.leadGenAppointment.appointmentDate, this.leadGenAppointment.appointmentFor).subscribe(result => {
            if(result) {
                this.notify.warn("Appointment Is Already Exist");
                this.leadGenAppointment.appointmentFor= null;
                document.getElementById('appointmentFor').focus();
            }
        });
    }

    save(): void {
        this.saving = true;
        this.leadGenAppointment.leadId = this.leadId;
        this.leadGenAppointment.sectionId = this.sectionId;
        this.leadGenAppointment.appointmentDate = this.myNameElem.nativeElement.value.toString();
        
        this._leadGenerationServiceProxy.createLeadGenAppointment(this.leadGenAppointment)
            .pipe(finalize(() => { this.saving = false;}))
            .subscribe(() => {
                let log = new UserActivityLogDto();
                log.actionId = 43;
                log.actionNote ='New Appintment Created';
                log.section = this.SectionName;
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            this.notify.info(this.l('SavedSuccessfully'));
            this.close();
            this.viewLeadDetail.reloadmyLeadGeneration.emit(null);
        });
    }
}