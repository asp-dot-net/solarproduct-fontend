﻿import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LeadsServiceProxy, LeadDto, LeadStatusLookupTableDto, OrganizationUnitDto,JobStatusTableDto, UserServiceProxy, LeadStateLookupTableDto, LeadSourceLookupTableDto, LookupTableDto, LeadUsersLookupTableDto, CommonLookupServiceProxy, AssignOrTransferLeadInput, UserActivityLogServiceProxy, UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { AppConsts } from '@shared/AppConsts';
import { HttpClient } from '@angular/common/http';
import { finalize } from 'rxjs/operators';
import { FileUpload } from 'primeng/fileupload';
import { OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';


@Component({
    templateUrl: './cancelleads.component.html',
    styleUrls: ['./cancelleads.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class CancelleadsComponent extends AppComponentBase implements OnInit {

    FiltersData = false;
    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    organizationUnitlength: number=0;
    alljobstatus: JobStatusTableDto[];
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    @ViewChild('ExcelFileUpload', { static: false }) excelFileUpload: FileUpload;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    excelorcsvfile = 0;
    uploadUrl: string;
    advancedFiltersAreShown = false;
    filterText = '';
    copanyNameFilter = '';
    emailFilter = '';
    phoneFilter = '';
    mobileFilter = '';
    addressFilter = '';
    requirementsFilter = '';
    postCodeSuburbFilter = '';
    stateNameFilter = '';
    streetNameFilter = '';
    postCodePostalCode2Filter = '';
    leadSourceNameFilter = '';
    //leadSubSourceNameFilter = '';
    leadSourceIdFilter = [];
    leadStatusName = '';
    typeNameFilter = '';
    areaNameFilter = '';
    cancelReasonFilter = 0;
    leadStatus: any;
    allLeadStatus: LeadStatusLookupTableDto[];
    allLeadSources: LeadSourceLookupTableDto[];
    leadStatusId: number = 0;
    date = new Date();
    StartDate: moment.Moment = moment(this.date);
    EndDate: moment.Moment = moment(this.date);
    PageNo: number;
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnit = 0;
    allStates: LeadStateLookupTableDto[];
    allcancelReason: LookupTableDto[];
    suburbSuggestions: string[];
    // firstDay = moment(new Date(this.date.getFullYear(), this.date.getMonth(), 1));
    sampleDateRange:  moment.Moment[] = [moment(this.date),moment(this.date)];
    dateFilterType = 'Cancel';
    filteredManagers: LeadUsersLookupTableDto[];
    filteredReps: LeadUsersLookupTableDto[];
    salesManagerId = 0;
    salesRepId = 0;
    role: string = '';
    filteredTeams: LeadUsersLookupTableDto[];
    teamId = 0;
    FaceBook = "";
    Referral = "";
    Tv = "";
    Online = "";
    Others = "";
    Total = "";
    firstrowcount = 0;
    last = 0;
    orgCode = 'AS';
    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 330;
    filterName = 'CompanyName';

    toggle: boolean = true;
    change() {
        this.toggle = !this.toggle;
      }
      
      changeOrganization = 0;
      PlaceHolderVal = '';
      action = 0;
      assignUserId = 0;
      assignManagerId = 0;
      assignUser: any = [];
      assignManager: any = [];
      organizationChangeUnit = 0;
      allSalesrepUsers: LeadUsersLookupTableDto[];
      assignLead1: number = 0;
      saving = false;
      count = 0;
      tableRecords: any;
      jobStatusIDFilter = [];
    constructor(
        injector: Injector,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _notifyService: NotifyService,
        private _commonLookupService: CommonLookupServiceProxy,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _httpClient: HttpClient,
        private _userServiceProxy: UserServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _router: Router,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private titleService: Title
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Cancel Leads");
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/Users/ImportLeadFromExcel';
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight;   
        this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
            this.role = result;
        });
        this._commonLookupService.getAllJobStatusTableDropdown().subscribe(result => {
            this.alljobstatus = result;
        });
        this._commonLookupService.getAllLeadSourceDropdown().subscribe(result => {
            this.allLeadSources = result;
        });

        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });

        this._leadsServiceProxy.getAllCancelReasonForTableDropdown().subscribe(result => {
            this.allcancelReason = result;
        });

        this._commonLookupService.getTeamForFilter().subscribe(teams => {
            this.filteredTeams = teams;
        });

        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Cancel Leads';
            log.section = 'Cancel Lead';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.orgCode = this.allOrganizationUnits[0].code;

            this.getLeads();

            this.bindUsers();

            // this._leadsServiceProxy.getSalesRepForFilter(this.organizationUnit, this.teamId).subscribe(rep => {
            //     this.filteredReps = rep;
            // });
        });
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }

        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }

    bindUsers()
    {
        this._commonLookupService.getSalesManagerForFilter(this.organizationUnit, this.teamId).subscribe(manager => {
            this.filteredManagers = manager;
        });

        this._leadsServiceProxy.getSalesRepForFilter(this.organizationUnit, this.teamId).subscribe(rep => {
            this.filteredReps = rep;
        });
    }
    changeOrgCode() {
        this.orgCode = this.allOrganizationUnits.find(x => x.id == this.organizationUnit).code;
    }
    clear() {
        this.filterText = '';
        this.postCodeSuburbFilter = '';
        this.stateNameFilter = '';
        this.postCodePostalCode2Filter = '';
        this.leadSourceIdFilter = [];
        this.leadSourceNameFilter = '';
        this.cancelReasonFilter = 0;
        this.salesManagerId = 0;
        this.salesRepId = 0;
        this.dateFilterType = 'Cancel';
        this.teamId = 0;
        this.sampleDateRange = [ moment(this.date), moment(this.date)]; 
        this.getLeads();
    }

    filtersuburbs(event): void {
        this._commonLookupService.getOnlySuburb(event.query).subscribe(output => {
            this.suburbSuggestions = output;
        });
    }

    getLeads(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        // if(this.sampleDateRange != null){
        //     this.StartDate = this.sampleDateRange[0];
        //     this.EndDate = this.sampleDateRange[1];
        // } else {
        //     this.StartDate = this.sampleDateRange[0];
        //     this.EndDate = this.sampleDateRange[1];
        // }
        this.primengTableHelper.showLoadingIndicator();
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._leadsServiceProxy.getAllForCancelLead(
            this.filterName,
            filterText_,
            this.copanyNameFilter,
            this.emailFilter,
            this.phoneFilter,
            this.mobileFilter,
            this.addressFilter,
            this.requirementsFilter,
            this.postCodeSuburbFilter,
            this.stateNameFilter,
            this.streetNameFilter,
            this.postCodePostalCode2Filter,
            this.leadSourceNameFilter,
            this.leadSourceIdFilter,
            // undefined,
            //this.leadSubSourceNameFilter,
            this.leadStatusName,
            this.typeNameFilter,
            this.areaNameFilter,
            (this.leadStatusId == 0) ? undefined : this.leadStatusId,
            0,
            this.StartDate,
            this.EndDate,
            this.organizationUnit,
            this.teamId == 0 ? undefined : this.teamId,
            this.salesManagerId,
            this.salesRepId,
            this.dateFilterType,
            undefined,
            undefined,
            undefined,
            this.cancelReasonFilter,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            0,
            "",
            this.jobStatusIDFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            this.tableRecords = result.items;

            if(result.totalCount > 0)
            {
                this.getLeadSummaryCount();
            }
            else {
                this.FaceBook ="0";
                this.Online ="0";
                this.Others ="0";
                this.Tv = "0";
                this.Referral ="0";
                this.Total = "0";
            }
            this.shouldShow = false;
            
        }, e =>  { this.primengTableHelper.hideLoadingIndicator();});
    }
    getLeadSummaryCount(){
        debugger;
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._leadsServiceProxy.getAllForCancelLeadCount(
        
           this.filterName,
           filterText_,
            this.copanyNameFilter,
            this.emailFilter,
            this.phoneFilter,
            this.mobileFilter,
            this.addressFilter,
            this.requirementsFilter,
            this.postCodeSuburbFilter,
            this.stateNameFilter,
            this.streetNameFilter,
            this.postCodePostalCode2Filter,
            this.leadSourceNameFilter,
            this.leadSourceIdFilter,
            // undefined,
            //this.leadSubSourceNameFilter,
            this.leadStatusName,
            this.typeNameFilter,
            this.areaNameFilter,
            (this.leadStatusId == 0) ? undefined : this.leadStatusId,
            0,
            this.StartDate,
            this.EndDate,
            this.organizationUnit,
            this.teamId == 0 ? undefined : this.teamId,
            this.salesManagerId,
            this.salesRepId,
            this.dateFilterType,
            undefined,
            undefined,
            undefined,
            this.cancelReasonFilter,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            0,
            "",
            this.jobStatusIDFilter,
            undefined,
            undefined,
            undefined
        ).subscribe(result => {
            debugger;
            if (result) {
                this.FaceBook = result.facebook;
                this.Online = result.online;
                this.Others = result.others;
                this.Tv = result.tv;
                this.Referral = result.referral;
                this.Total = result.total;
            }
        });
    }
    filterCountries(event): void {
        this._commonLookupService.getAllLeadStatusForTableDropdown(event.query).subscribe(result => {
            this.allLeadStatus = result;
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createLead(): void {
        this._router.navigate(['/app/main/leads/leads/createOrEdit']);
    }

    navigateToLeadDetail(leadid): void {
        this._router.navigate(['/app/main/closedlead/closedlead/viewclosedlead'], { queryParams: { LeadId: leadid } });
    }

    deleteLead(lead: LeadDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    // this._leadsServiceProxy.delete(lead.id)
                    //     .subscribe(() => {
                    //         this.reloadPage();
                    //         this.notify.success(this.l('SuccessfullyDeleted'));
                    //     });
                }
            }
        );
    }


    exportToExcel(excelorcsv): void {
        // if(this.sampleDateRange != null){
        //     this.StartDate = this.sampleDateRange[0];
        //     this.EndDate = this.sampleDateRange[1];
        // } else {
        //     this.StartDate = this.sampleDateRange[0];
        // this.EndDate = this.sampleDateRange[1];
        // }
        this.excelorcsvfile = excelorcsv;
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._leadsServiceProxy.getCancelLeadsToExcel(
            this.filterName,
            filterText_,
            this.copanyNameFilter,
            this.emailFilter,
            this.phoneFilter,
            this.mobileFilter,
            this.addressFilter,
            this.requirementsFilter,
            this.postCodeSuburbFilter,
            this.stateNameFilter,
            this.streetNameFilter,
            this.postCodePostalCode2Filter,
            this.leadSourceNameFilter,
            this.leadSourceIdFilter,
            // undefined,
            //this.leadSubSourceNameFilter,
            this.leadStatusName,
            this.typeNameFilter,
            this.areaNameFilter,
            (this.leadStatusId == 0) ? undefined : this.leadStatusId,
            0,
            this.StartDate,
            this.EndDate,
            this.organizationUnit,
            undefined,
            this.salesManagerId,
            this.salesRepId,
            this.dateFilterType,
            undefined,
            undefined,
            undefined,
            this.cancelReasonFilter,
            undefined,
            this.excelorcsvfile,
            undefined,
            undefined,
            undefined
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    uploadExcel(data: { files: File }): void {
        const formData: FormData = new FormData();
        const file = data.files[0];
        formData.append('file', file, file.name);

        this._httpClient
            .post<any>(this.uploadUrl, formData)
            .pipe(finalize(() => this.excelFileUpload.clear()))
            .subscribe(response => {
                if (response.success) {
                    this.notify.success(this.l('Import Leads Process Start'));
                } else if (response.error != null) {
                    this.notify.error(this.l('Import Lead Upload Failed'));
                }
            });
    }

    onUploadExcelError(): void {
        this.notify.error(this.l('Import Users Upload Failed'));
    }

    reassign(leadid :number): void {
        this.message.confirm(
            this.l('Are You Sure Want to Reassign Lead'),
            this.l('Reassign'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._leadsServiceProxy.reAssignCancelOrRejectLead(leadid)
                        .subscribe(() => {
                            let log = new UserActivityLogDto();
                            log.actionId = 11;
                            log.actionNote ='Lead Reassign';
                            log.section = 'Cancel Lead';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });
                            this.getLeads()
                            this.notify.success(this.l('Successfully ReAssign'));
                        });
                }
            }
        );
    }

    changeAction() : void {
        
        if(this.action == 2)
        {
            this.PlaceHolderVal = 'Select Sales Rep';

            this.spinnerService.show();
            this.assignUser = [];
            this.assignUserId = 0;
            this._commonLookupService.getAllUsersByRoleNameTableDropdown("Sales Manager", this.changeOrganization).subscribe(result => {
                this.assignManager = result;
                this.spinnerService.hide();
            }, e => { this.spinnerService.hide(); });
        } 
        else if (this.action == 3)
        {
            debugger;
            this.PlaceHolderVal = 'Select Users';

            this.assignUserId = 0;
            
            this.spinnerService.show();
            this._commonLookupService.getAllUsersByRoleNameTableDropdown("Without Installer", this.changeOrganization).subscribe(result => {
                this.assignUser = result;
                this.spinnerService.hide();
            }, e => { this.spinnerService.hide(); });
        }
        else if (this.action == 4)
        {
            this.PlaceHolderVal = 'Select Lead Gen Manager';

            this.assignUserId = 0;

            this.spinnerService.show();
            this._commonLookupService.getAllUsersByRoleNameTableDropdown("Leadgen Manager", this.changeOrganization).subscribe(result => {
                this.assignUser = result;
                this.spinnerService.hide();
            }, e => { this.spinnerService.hide(); });
        } 
        else {
            this.PlaceHolderVal = 'Select Sales Manager';

            this.assignUserId = 0;
            
            this.spinnerService.show();
            this._commonLookupService.getAllUsersByRoleNameTableDropdown("Sales Manager", this.changeOrganization).subscribe(result => {
                this.assignUser = result;
                this.spinnerService.hide();
            }, e => { this.spinnerService.hide(); });
        }
        
    }


    getSaleRep() : void {
        this.spinnerService.show();
        this._leadsServiceProxy.getSalesRepBySalesManagerid(this.assignManagerId, this.changeOrganization).subscribe(result => {
            this.assignUser = result;
            this.spinnerService.hide();
        }, e => { this.spinnerService.hide(); });
    }

    UserSearchResult : any [];
    filterUserSearchResult(event): void {
      this.UserSearchResult = Object.assign([], this.assignUser).filter(
        item => item.displayName.toLowerCase().indexOf(event.query.toLowerCase()) > -1
      )
    }

    selectAssignUser(event): void {
        this.assignUserId = event.id == undefined ? 0 : event.id;
    }

    submit() : void {
        let selectedids = [];
        this.saving = true;
        debugger
        let transferPermission = this.permission.isGranted("Pages.Leads.LeadTracker.Action.TransferLeftSalesRepLead");

        let action = this.action;

        this.primengTableHelper.records.forEach(function (lead) {
            
            let rtVal = false;
            if(lead.jobStatusId < 4)
            {
                rtVal = true;
            }
            else if(lead.jobStatusId == 4 || lead.jobStatusId == 5)
            {
                if(lead.isLeftSalesRep == false && transferPermission == true)
                {
                    rtVal = true;
                }
            }

            if(action == 4)
            {
                if (lead.lead.isSelected && rtVal == true) {
                    selectedids.push(lead.lead.id);
                }
            }
            else {
                if (lead.lead.isSelected && rtVal == true) {
                    selectedids.push(lead.lead.id);
                }
            }
        });
            
        let leads: AssignOrTransferLeadInput = new AssignOrTransferLeadInput();
        leads.assignToUserID = this.assignUserId;
        leads.leadIds = selectedids;
        leads.organizationID = this.changeOrganization;
        leads.leadActionId = this.action;
        leads.sectionId = 39;
        leads.section = 'Cancel Lead'
        
        if (this.changeOrganization == 0) {
            this.notify.warn(this.l('PleaseSelectOrganization'));
            this.saving = false;
            return;
        }

        if(this.assignUserId == 0)
        {
            this.notify.warn("Please " + this.PlaceHolderVal);
            this.saving = false;
            return;
        }

        if(selectedids.length == 0)
        {
            this.notify.warn(this.l('NoLeadsSelected'));
            this.saving = false;
            return;
        }

        this._leadsServiceProxy.asssignOrTransferLeads(leads)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.getLeads();
                this.saving = false;
                this.notify.info(this.l('AssignedSuccessfully'));
            }, e => { this.saving = false; });
    }

    oncheckboxCheck() {

        
        this.count = 0;
        this.tableRecords.forEach(item => {

            let transfer = this.hideTransfer(item);
            if (item.lead.isSelected == true && transfer == true) {
                this.count = this.count + 1;
            }
        })
    }
    checkAll(ev) {

        this.tableRecords.forEach(x => x.lead.isSelected = ev.target.checked);
        this.oncheckboxCheck();
    }

    isAllChecked() {
        if (this.tableRecords)
            return this.tableRecords.every(_ => _.lead.isSelected);
    }

    hideTransfer(record: any) : Boolean {
        
        let rtVal = false;

        if(record.jobStatusId < 4)
        {
            rtVal = true;
        }
        else if(record.jobStatusId == 4 || record.jobStatusId == 5)
        {
            if(record.isLeftSalesRep == false && this.permission.isGranted("Pages.Leads.LeadTracker.Action.TransferLeftSalesRepLead") == true)
            {
                rtVal = true;
            }
        }

        return rtVal;
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Lead Tracker';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Lead Tracker';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }
}