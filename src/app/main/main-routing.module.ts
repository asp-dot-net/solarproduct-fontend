﻿import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SmsTemplatesComponent } from './smsTemplates/smsTemplates/smsTemplates.component';
import { FreebieTransportsComponent } from './jobs/freebieTransports/freebieTransports.component';
import { InvoiceStatusesComponent } from './invoices/invoiceStatuses/invoiceStatuses.component';
import { HoldReasonsComponent } from './holdReasons/holdReasons/holdReasons.component';
import { RefundReasonsComponent } from './jobs/refundReasons/refundReasons.component';
import { JobRefundsComponent } from './jobs/jobRefunds/jobRefunds.component';
import { CalendersComponent } from './calenders/calenders/calenders.component';
import { VariationsComponent } from './jobs/variations/variations.component';
import { InvoicePaymentMethodsComponent } from './invoices/invoicePaymentMethods/invoicePaymentMethods.component';
import { InvoicePaymentsComponent } from './invoices/invoicePayments/invoicePayments.component';
import { CreateOrEditInvoicePaymentComponent } from './invoices/invoicePayments/create-or-edit-invoicePayment.component';
import { PromotionMastersComponent } from './jobs/promotionMasters/promotionMasters.component';
import { FinanceOptionsComponent } from './jobs/financeOptions/financeOptions.component';
import { HouseTypesComponent } from './jobs/houseTypes/houseTypes.component';
import { CancelReasonsComponent } from './cancelReasons/cancelReasons/cancelReasons.component';
import { RejectReasonsComponent } from './rejectReasons/rejectReasons/rejectReasons.component';
import { MeterPhasesComponent } from './jobs/meterPhases/meterPhases.component';
import { MeterUpgradesComponent } from './jobs/meterUpgrades/meterUpgrades.component';
import { ElecRetailersComponent } from './jobs/elecRetailers/elecRetailers.component';
import { DepositOptionsComponent } from './jobs/depositOptions/depositOptions.component';
import { PaymentOptionsComponent } from './jobs/paymentOptions/paymentOptions.component';
import { JobStatusesComponent } from './jobs/jobStatuses/jobStatuses.component';
import { ProductItemsComponent } from './jobs/productItems/productItems.component';
import { CreateOrEditProductItemComponent } from './jobs/productItems/create-or-edit-productItem.component';
import { ViewProductItemComponent } from './jobs/productItems/view-productItem.component';
import { ElecDistributorsComponent } from './jobs/elecDistributors/elecDistributors.component';
import { RoofAnglesComponent } from './jobs/roofAngles/roofAngles.component';
import { RoofTypesComponent } from './jobs/roofTypes/roofTypes.component';
import { ProductTypesComponent } from './jobs/productTypes/productTypes.component';
import { JobTypesComponent } from './jobs/jobTypes/jobTypes.component';
import { CreateOrEditJobTypeComponent } from './jobs/jobTypes/create-or-edit-jobType.component';
import { ViewJobTypeComponent } from './jobs/jobTypes/view-jobType.component';
import { PromotionUsersComponent } from './promotions/promotionUsers/promotionUsers.component';
import { PromotionsComponent } from './promotions/promotions/promotions.component';
import { PromotionResponseStatusesComponent } from './promotions/promotionResponseStatuses/promotionResponseStatuses.component';
import { DepartmentsComponent } from './departments/departments/departments.component';
import { LeadsComponent } from './leads/leads/leads.component';
import { CreateOrEditLeadComponent } from './leads/leads/create-or-edit-lead.component';
import { ViewLeadComponent } from './leads/leads/view-lead.component';
import { ActivityLogLeadComponent } from './leads/leads/activity-log-lead.component';
import { MyLeadsComponent } from './myleads/myleads/myleads.component';
import { ViewMyLeadComponent } from './myleads/myleads/view-mylead.component';
import { ActivityLogMyLeadComponent } from './myleads/myleads/activity-log-mylead.component';
import { AssignOrTransferLeadComponent } from './leads/leads/assign-or-transfer-lead.component';
import { LeadSourcesComponent } from './leadSources/leadSources/leadSources.component';
import { CreateOrEditLeadSourceComponent } from './leadSources/leadSources/create-or-edit-leadSource.component';
import { ViewLeadSourceComponent } from './leadSources/leadSources/view-leadSource.component';
import { CreateOrEditLeadSourceModalComponent } from './leadSources/leadSources/create-or-edit-leadSource-modal.component';
import { ViewLeadSourceModalComponent } from './leadSources/leadSources/view-leadSource-modal.component';

import { PostalTypesComponent } from './postalTypes/postalTypes/postalTypes.component';
import { CreateOrEditPostalTypeModalComponent } from './postalTypes/postalTypes/create-or-edit-postalType-modal.component';
import { ViewPostalTypeModalComponent } from './postalTypes/postalTypes/view-postalType-modal.component';

import { PostCodesComponent } from './postCodes/postCodes/postCodes.component';
import { CreateOrEditPostCodeModalComponent } from './postCodes/postCodes/create-or-edit-postCode-modal.component';
import { ViewPostCodeModalComponent } from './postCodes/postCodes/view-postCode-modal.component';

import { StreetNamesComponent } from './streetNames/streetNames/streetNames.component';
import { CreateOrEditStreetNameModalComponent } from './streetNames/streetNames/create-or-edit-streetName-modal.component';
import { ViewStreetNameModalComponent } from './streetNames/streetNames/view-streetName-modal.component';
import { StreetTypesComponent } from './streetTypes/streetTypes/streetTypes.component';
// import { CreateOrEditStreetTypeComponent } from './streetTypes/streetTypes/create-or-edit-streetType.component';
// import { ViewStreetTypeComponent } from './streetTypes/streetTypes/view-streetType.component';
import { CreateOrEditStreetTypeModalComponent } from './streetTypes/streetTypes/create-or-edit-streetType-modal.component';
import { ViewStreetTypeModalComponent } from './streetTypes/streetTypes/view-streetType-modal.component';

import { UnitTypesComponent } from './unitTypes/unitTypes/unitTypes.component';
import { CreateOrEditUnitTypeModalComponent } from './unitTypes/unitTypes/create-or-edit-unitType-modal.component';
import { ViewUnitTypeModalComponent } from './unitTypes/unitTypes/view-unitType-modal.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DashboardInvoiceComponent } from './dashboard-invoice/dashboard-invoice.component';
import { DashboardSalesComponent } from './dashboard-sales/dashboard-sales.component';
import { DashboardServiceComponent } from './dashboard-service/dashboard-service.component';

import { LeadExpensesComponent } from './expense/leadExpenses/leadExpenses.component';
import { CreateOrEditLeadExpenseModalComponent } from './expense/leadExpenses/create-or-edit-leadExpense-modal.component';
import { ViewLeadExpenseModalComponent } from './expense/leadExpenses/view-leadExpense-modal.component';

import { UserTeamsComponent } from './theSolarDemo/userTeams/userTeams.component';
import { CreateOrEditUserTeamComponent } from './theSolarDemo/userTeams/create-or-edit-userTeam.component';
import { ViewUserTeamComponent } from './theSolarDemo/userTeams/view-userTeam.component';

import { CategoriesComponent } from './theSolarDemo/categories/categories.component';
import { CreateOrEditCategoryModalComponent } from './theSolarDemo/categories/create-or-edit-category-modal.component';
import { ViewCategoryModalComponent } from './theSolarDemo/categories/view-category-modal.component';

import { TeamsComponent } from './theSolarDemo/teams/teams.component';
import { CreateOrEditTeamComponent } from './theSolarDemo/teams/create-or-edit-team.component';
import { ViewTeamComponent } from './theSolarDemo/teams/view-team.component';
import { CreateOrEditTeamModalComponent } from './theSolarDemo/teams/create-or-edit-team-modal.component';
import { ViewTeamModalComponent } from './theSolarDemo/teams/view-team-modal.component';

import { ChangeStatusLeadComponent } from './leads/leads/change-status-lead.component';
import { UserDashboardComponent } from './userdashboard/user-dashboard.component';
import { DuplicateleadsComponent } from './duplicatelead/duplicatelead/duplicateleads.component';
import { ClosedleadsComponent } from './closedlead/closedlead/closedleads.component';
import { ViewClosedLeadComponent } from './closedlead/closedlead/view-closed-lead.component';
import { LeadTrackerComponent } from './leads/leads/lead-tracker.component';
import { ResultsComponent } from './result/result/results.component';
import { ViewResultLeadComponent } from './result/result/view-result-lead.component';
import { JobsListComponent } from './jobs/jobs/jobslist.component';
import { FreebiesComponent } from './jobs/jobPromotions/freebies.component'
import { InstallationComponent } from './installation/installation/installation.component';
import { CancelleadsComponent } from './cancelleads/cancelleads/cancelleads.component';
import { jobGridComponent } from './jobs/jobGrid/jobGrid.component';
import { EmailEditorModule } from 'angular-email-editor';
import { EmailTemplateComponent } from './emailtemplates/emailtemplates.component';
import { CreateOrEditEmailTemplateComponent } from './emailtemplates/create-or-edit-emailtemplates.component';
import { DocumentTypesComponent } from './documentTypes/documenttypes.component';
import { JobCancellationReasonComponent } from './jobs/jobCancellationReason/jobcancellationreason.component';
import { JobDetailModalComponent } from './jobs/jobs/job-detail-model.component';
import { PendingInstallationComponent } from './installation/installation/pendinginstallation.component';

import { PendingInstallationDetailComponent } from './installation/installation/pendinginstallationdetail.component';
import { StcTrackerComponent } from './jobs/stctracker/stctracker.component';
import { GridConnectionTrackerComponent } from './gridconnectiontracker/gridconnectiontracker.component';
import { GridConnectionDetailComponent } from './gridconnectiontracker/gridconnectiondetail.component';
import { FinanceTrackerComponent } from './jobs/financeTracker/financeTracker.component';
import { RejectleadsComponent } from './rejectleads/rejectleads/rejectleads.component';
import { JobActiveRequestComponent } from './jobs/jobs/job-active-request.component';
import { NewinstallerComponent } from './installer/newinstaller/newinstaller.component';
import { SmsReplyComponent } from './reply/smsreply/sms-reply.component';
import { EmailReplyComponent } from './reply/emailReply/email-reply.component';
import { DocumentLibrarysComponent } from './documentLibrary/documentlibrary.component';
import { InvoiceIssuedComponent } from './invoices/invoiceIssued/invoiceIssued.component';
import { InvoiceinstallerComponent } from './installer/invoiceinstaller/invoiceinstaller.component';
import { IntallerMapComponent } from './intaller-map/intaller-map.component';
import { ReadytoPayinstallerComponent } from './installer/radytoPayInstaller/readytoPayinstaller.component';
import { InstallerInvoiceFileListComponent } from './installer/installerInvoiceFileList/installerInvoiceFileList.component';
import { CreateOrEditPromotionComponent } from './promotions/promotions/create-or-edit-promotion.component';
import { NewInvoiceIssuedComponent } from './invoices/new-invoice-issued/new-invoice-issued.component';
import { JobBookingComponent } from './installation/jobBooking.component';
import { Stcpvdstatuscomponent } from './stcPvdStatus/stcpvdstatus/stcpvdstatus.component';
import { TodoActivityListComponent } from './report/todo-activity-list/todo-activity-list.component';
import { ActivityreportComponent } from './report/activityreport/activityreport.component';
import { ReferraltrackerComponent } from './referral/referraltracker.component';
import { WarrantytrackerComponent } from './warranty/warrantytracker.component';
import { LeadexpenseReportComponent } from './report/leadexpensereport/leadexpense-report/leadexpense-report.component';
import { OutStandingReportComponent } from './report/outstandingreport/outstandingreport.component';
import { ServiceCategoryComponent } from './serviceCategory/serviceCategory/serviceCategory.component';
import { ServiceSourcesComponent } from './serviceSources/serviceSources/serviceSources.component';
import { ServiceStatusComponent } from './serviceStatus/serviceStatus/serviceStatus.component';
import { ServiceTypeComponent } from './serviceType/serviceType/serviceType.component';
import { ServiceSubCategoryComponent } from './serviceSubCategory/serviceSubCategory/serviceSubCategory.component';
import { ManageServicesComponent } from './services/manageservices/manageservice.component';
import { MyServicesComponent } from './services/myservices/myservice.component';
import { ReviewComponent } from './review/review.component';
import { ReviewTypeComponent } from './reviewType/reviewType/reviewType.component';
import { ServiceMapComponent } from './services/service-map/service-map.component';
import { InvoicePayWayComponent } from './invoices/invoice-payway/invoice-payway.component';
import { MyinstallerComponent } from './installation/myinstaller/myinstaller.component';
import { ServiceInstallationComponent } from './services/serviceInstallation/serviceInstallation.component';
import { PriceItemListsComponent } from './priceItemLists/priceItemLists/priceItemLists.component';
import { HoldJobsComponent } from './jobs/holdJobs/holdJobs.component';
import { QuotationTemplateComponent } from './quotationtemplate/quotationtemplate/quotationtemplate.component';
import { CreateOrEditQuotationTemplateModalComponent } from './quotationtemplate/quotationtemplate/create-or-edit-quotationtemplate-modal.component';
import { PostCodeRangeComponent } from './postcoderange/postcoderange/postcoderange.component';
import { WarehouseLocationComponent } from './warehouselocation/warehouselocation.component';
import { ProductPackageComponent } from './productpackage/productpackage.component';

import { MyLeadGenerationComponent } from './my-lead-generation/myLeadGeneration.component';
import { LeadGenMapComponent } from './lead-generation-map/lead-gen-map.component';
import { LeadGenInstallationComponent } from './lead-generation-installation/lead-generation-installation.component';
import { DashboardWholesaleComponent } from './dashboard-wholesale/dashboard-wholesale.component';
import { WholesaleLeadsComponent } from './wholesale/leads/wholesaleleads.component';
import { CreateEditWholesaleLeadComponent } from './wholesale/leads/create-edit-wholesalelead.component';
import { WholesalePromotionsComponent } from './wholesale/promotions/promotions/promotions.component';
import { CreateOrEditWhlPromotionComponent } from './wholesale/promotions/promotions/create-or-edit-promotion.component';
import { WholesaleCreateOrEditPromotionModalComponent } from './wholesale/promotions/promotions/create-or-edit-promotion-modal.component';
//import { WholesalePromotionUsersComponent } from './wholesale/promotions/promotionUsers/promotionUsers.component';
import { WholesaleInvoiceComponent } from './wholesale/invoice/invoice.component';
import { createEditWholesaleInvoiceComponent } from './wholesale/invoice/create-edit-invoice.component';
import { PromotionUsersWholesaleComponent } from './wholesale/promotions/promotionUsers/promotionUsers.component';
import { WholesaleStatusTypesComponent } from './wholesale/data-vaults/wholesale-type/WholesaleStatusTypes.component';
import { InvoiceTypesComponent } from './wholesale/data-vaults/invoice-type/InvoiceTypes.component';
import { WholesaleBDMComponent } from './wholesale/data-vaults/wholesale-bdm/wholesale-bdm.component';
import { DeliveryOptionsComponent } from './wholesale/data-vaults/delivery-options/delivery-options.component';
import { WholesaleJobTypesComponent } from './wholesale/data-vaults/job-type/JobTypes.component';
import { TransportTypesComponent } from './wholesale/data-vaults/transport-type/TransportTypes.component';
import { Dashboard2Component } from './dashboard2/dashboard-kanban.component';
import { DashboardInventoryComponent } from './inventory/dashboard/dashboard-inventory.component';
// import { VendorsComponent } from './inventory/data-vaults/vendors/vendors.component';
// import { TransportCompanyComponent } from './inventory/data-vaults/transport-company/transport-company.component';
// import { StockOrderStatusComponent } from './inventory/data-vaults/stock-order-status/stock-order-status.component';
// import { PaymentMethodComponent } from './inventory/data-vaults/payment-method/payment-method.component';
// import { DeliveryTypeComponent } from './inventory/data-vaults/delivery-type/delivery-type.component';
// import { StockOrderComponent } from './inventory/data-vaults/stock-order/stock-order.component';
// import { StockCurrencyComponent } from './inventory/data-vaults/stock-currency/stock-currency.component';
// import { StockFromComponent } from './inventory/data-vaults/stock-from/stock-from.component';
// import { PurchaseCompanyComponent } from './inventory/data-vaults/purchase-company/purchase-company.component';
import { StockTransferComponent } from './inventory/stock-transfer/stock-transfer.component';
import { PurchaseOrderComponent } from './inventory/purchase-order-new/purchase-order-new.component';
import { StockOrderDetailModal } from './inventory/purchase-order-new/stock-order-detail-modal.component';
import { OtherChargesComponent } from './installation-cost/other-charges/other-charges.component';
import { CreateOrEditChargeModalComponent } from './installation-cost/other-charges/create-or-edit-charges-modal.component';
import { StateWiseCostComponent } from './installation-cost/statewiseCost/state-wise-cost.component';
import { createOrEditStateWiseCostModal } from './installation-cost/statewiseCost/createEdit-state-wise-cost-modal.component';
import { InstallationItemComponent } from './installation-cost/installationItemList/installation-item-list.component';
import { CreateOrEditInstallationModalComponent } from './installation-cost/installationItemList/createEdit-installation-item-modal.component';
import { FixCostPriceComponent } from './installation-cost/fixCostPrice/fix-cost-price.component';
import { createOrEditFixCostModal } from './installation-cost/fixCostPrice/createEdit-fix-cost-price-modal.component';
import { ViewInstallationModalComponent } from './installation-cost/installationItemList/view-installation-item-modal.component';
import { ExtraInstallatIonChargeComponent } from './installation-cost/extraInstallationCharge/extra-installation-cost.component';
import { createOrEditExtraInstallationModal } from './installation-cost/extraInstallationCharge/createEdit-extra-installation-cost-modal.component';
import { JobCostComponent } from './report/jobcost/jobcost.component';

import { ViewDetailsModelComponent } from './result/result/view-detail-model/view-model.component';

import { STCCostComponent } from './installation-cost/stcCost/stcCost.componenet';
import { CreateOrEditSTCCosteModalComponent } from './installation-cost/stcCost/create-or-edit-stcCost-model.componenet';
import { BatteryInstallationCostComponent } from './installation-cost/batteryInstallationCost/batteryInstalltionCost.coponent';
import { UserCallHistoryComponent } from './report/callhistory/usercallhistory/usercallhistory.component';
import { StateWiseCallHistoryComponent } from './report/callhistory/statewisecallhistory/statewisecallhistory.component';
import{ProgressReportComponent}from './report/progessReport/progressReport.component';
import { LeadAssignReportComponent } from './report/leadassign/leadassign.component';

import { CallFlowQueueComponent } from './callflowqueue/call-flow-queue.component';
import { createOrEditCallFlowQueueModalComponent } from './callflowqueue/create-or-edit-callFlowQueue-modal.component';
import { CxCallQueueComponent } from './report/callQueue/csx-call-queue.component';
import { CsxCallQueueReportModalComponent } from './report/callQueue/csx-call-queue-modal.component';
import { UserCsxCallQueueReportModalComponent } from './report/callQueue/user-csx-call-queue-modal.component';
import { WarrentyClaimComponent } from './services/warrentyClaim/warrentyClaim.component';
import { ServiceDocumentTypesComponent } from './serviceDocumentTypes/serviceDocumenttypes.component';
import { EmployeeJobCostComponent } from './report/employeeJobCost/employeeJobcost.component';
import { ServiceInvoiceModelComponent } from './services/warrentyClaim/serviceInvoice.component';
import { WholeSaleLeadDocumentTypesComponent } from './wholesale/data-vaults/wholesalelead-documenttype/wholeSaleLeadDocumenttypes.component';
import { WholeSaleSmsTemplatesComponent } from './wholesale/data-vaults/smsTemplates/smsTemplates.component';
import { WholeSaleEmailTemplatesComponent } from './wholesale/data-vaults/emailTemplates/emailTemplates.component';
import { CreateOrEditWholeSaleEmailTemplateModalComponent } from './wholesale/data-vaults/emailTemplates/create-or-edit-emailTemplates-modal.component';
import { LeadSoldReportComponent } from './report/LeadSold/leadsold.component';
import { WholesaleDashboardComponent } from './wholesale/dashboard-kanban/dashboard-kanban.component';
import { CommissionComponent } from './commission/commission.component';
import { JobCommissionComponent } from './report/jobcommission/jobcommission.component';
import { CommissionRangeComponent } from './commission-range/commission-range.component';
import { createOrEditCommissionRangeModalComponent } from './commission-range/commission-range-modal.component';
import { viewcommissionRangeModalComponent } from './commission-range/view-commission-range-modal.component';
import { DetailModalComponent } from './report/jobcommission/detailuserwisemodal.component';
import { DashboardMessageComponent } from './dashboard-message/dashboard-messages.component';
import { createOrEditDashboardMessageModalComponent } from './dashboard-message/create-edit-dashboard-messages.component';
import { viewdashboardMessageModalComponent } from './dashboard-message/view-dashboard-messages-modal.component';
import { CategoryInstallationItemComponent } from './installation-cost/categoryInstallationItem/category-installation-item.component';
import { CreateOrEditCategoryInstallationModalComponent } from './installation-cost/categoryInstallationItem/createEdit-category-installation-item-modal.component';
import { ViewCategoryInstallationModalComponent } from './installation-cost/categoryInstallationItem/view-category-installation-item-modal.component';
import { ProductSoldReportComponent } from './report/productSold/productsold.component';
import { JobCommissionPaidComponent } from './report/jobcommissionpaid/jobcommissionpaid.component';
import { SmsCountReportComponent } from './report/smscountreport/smscountreport.component';
import { ECommerceSliderComponent } from './wholesale/ecommerce/ecommerceslider/ecommerceslider.component';
import { WholesaleProductComponent } from './wholesale/ecommerce/add-product/add-product.component';
import { CreateOrEditProductModalComponent } from './wholesale/ecommerce/add-product/create-or-edit-product-modal.component';
import { ViewProductModalComponent } from './wholesale/ecommerce/add-product/view-product-modal.component';
import { SpecialOffersComponent } from './wholesale/ecommerce/special-offers/special-offers.component';
import { CreateOrEditSpecialOfferModalComponent } from './wholesale/ecommerce/special-offers/create-or-edit-offer-modal.component';
import { ViewSpecialOfferModalComponent } from './wholesale/ecommerce/special-offers/view-offer-modal.component';
import { CreditApplicationsComponent } from './wholesale/credit/credit-applications/credit-applications.component';
import { ViewCreditApplicationModalComponent } from './wholesale/credit/credit-applications/view-credit-application-modal.component';
import { postCodeWiseCostComponent } from './installation-cost/postcodecost/postcode-wise-cost.component';
import { BrandingPartnerComponent } from './wholesale/ecommerce/brandingpartner/brandingpartner.component';
import { UserRequestComponent } from './wholesale/ecommerce/userrequest/userrequest.component';
import { EcommerceSolarPackagePackageComponent } from './wholesale/ecommerce/solarpackage/solarpackage.component';
import { CxCallQueueWeeklyComponent } from './report/callQueueweekly/csx-call-queue-weekly.component';
import { CxCallQueueUserWiseComponent } from './report/callQueueUserwise/csx-call-queue-userwise.component';
import { ContactUsComponent } from './wholesale/ecommerce/contactus/contactus.component';
import { UserDetailReportComponent } from './report/userdetail/userdetail.component';
import { ComparisonReportComponent } from './report/comparison/comparison.component';
import { ThirdrdPartyLeadTrackerComponent } from './leads/leads/3rd-party-lead-tracker.component';
import { DataVaultActivityLogComponent } from './datavault-activitylog/datavaultactivitylog.component';
import { StateComponent } from './state/state.component';
import { InstallerInvoicePaymentReportComponent } from './report/installerinvoicepayment/installerinvoicepayment.component';
import { InstallerPaymentInstallerWiseReportComponent } from './report/installerpaymentinstallerwise/installerpaymentinstallerwise.component';
import { TransportCostComponent } from './transportcost/transportcost.component';
import { CheckDepositReceivedComponent } from './checkDepositReceived/checkDepositReceived.component';
import { CheckActiveComponent } from './checkActives/chcekActives/checkActive.component';
import { CheckApplicationComponent } from './chcekApplications/checkApplications/check-application.component';
import { StockOrderStatusComponent } from './quickstock/stockorderstatus/stockorderstatus.component';
import { DeliveryTypeComponent } from './quickstock/deliverytype/deliverytype.component';
import {PurchaseCompanyComponent} from './quickstock/PurchaseCompany/purchase-company.component';
import { TransportcompanyComponent } from './transportcompany/transportcompany.component';
import { PaymentStatusComponent } from './quickstock/paymentstatus/paymentstatus.component';
import { PaymentMethodComponent } from './quickstock/paymentmethod/paymentmethod.component';
import { PaymentTypeComponent } from './quickstock/paymenttype/paymenttype.component';
import { CurrencyComponent } from './quickstock/currency/currency.component';
import { FreightCompanyComponent } from './quickstock/freightcompany/freightcompany.component';
import { StockFromComponent } from './quickstock/stockfrom/stockfrom.component';
import { StockOrderForComponent } from './quickstock/stockorderfor/stockorderfor.component';
import { PurchaseDocumentListComponent } from './quickstock/purchasedocumentlist/purchasedocumentlist.component';
import { JobCostMonthWiseComponent } from './report/jobcostmonthwise/jobcostmonthwise.component';
import {vendorComponent } from './quickstock/vendors/vendor.component';
import { EcommerceSubscriberComponent } from './wholesale/ecommerce/subcribers/subscriber.component';
import {StockOrderComponent } from './quickstock/stockorder/stock-order.component';
import {StockTransfersComponent} from './quickstock/stockTransfers/stock-transfers.component';
import{IncoTermComponent} from './quickstock/incoTerm/incoterm.component';
import { WholeSaleLeadHeaderResultsComponent } from './wholesaleheaderresult/wholesaleheaderresult.component';
import { ViewResultWholeSaleLeadComponent } from './wholesaleheaderresult/view-wholesaleheaderresult.component';
import { JobVariationReportComponent } from './report/jobvariation/jobvariation.component';
import { MonthlyComparisonReportComponent } from './report/monthlycomparison/monthlycomparison.component';
import { SeriesComponent } from './wholesale/data-vaults/series/series.component';
import { PropertyTypeComponent } from './wholesale/data-vaults/propery-type/propertyTypes.component';
import { WholesaleJobStatusComponent } from './wholesale/data-vaults/jobStatus/jobStatus.component';
import { PVDStatusComponent } from './wholesale/data-vaults/pvd-status/pvdStatus.component';
import { RequestToWholeSaleLeadTransferComponent } from './wholesale/requesttotransferlead/requesttotransferlead.component';
import {WholeSaleDataVaultActivityLogComponent} from './wholesale/data-vaults/activity/wholesale-datavault-activitylog.component';
import {EcommerceDataVaultActivityLogComponent} from './wholesale/ecommerce/activity/ecommerce-datavault-activitylog.component';
import {EssentialTrackerComponent} from './jobs/essentialTracker/eseentialTracker.component';
import { WholesaleSmsReplyComponent } from './wholesale/reply/wholesale-sms-reply.component';
import { EcommerceProductTypeComponent } from './wholesale/ecommerce/producttype/ecommerceproducttype.component';
import { EcommerceSpecificationComponent } from './wholesale/ecommerce/specification/specification.component';
import{LeadSourceComparisonReportComponent} from './report/leadSourceComparison/leadSourceComparison-report.component';
import { RescheduleInstallationReportComponent } from './report/rescheduleinstallation/rescheduleinstallation.component';
import { JobCostFixExpenseComponent } from './installation-cost/jobcostfixexpense/jobcostfixexpense.component';
import{StockPaymentComponent} from './inventory/stock-payment/stock-payment.component';
import { CIMETLeadTrackerComponent } from './leads/leads/cimet-tracker.component';
import { StockPaymentTrackerComponent } from './inventory/stock-payment-tracker/stock-payment-tracker.component';
import { StockVariationsComponent } from './quickstock/stock-variation/stock-variation.component';
import{EcommerceStcRegisterComponent} from './wholesale/ecommerce/stcregister/stcregister.component';
import { EcommerceStcDealPriceComponent } from './wholesale/ecommerce/stcDealPrice/stcdealprice.component';
import { InventoryDataVaultActivityLogComponent } from './inventory/data-vaults/activitylog/inventorydatavaultactivitylog.Component';
import{ReviewReportComponent} from './report/reviewReport/reviewreport.component';
import{StockOrderReceivedComponent} from './inventory/reports/stockOrderReceivedReport.component';
import{leadMonthlyComparison} from './report/leadmonthlycomparison/lead-monthly-comparison.component';
import{EmployeeProfitReportComponent} from './report/employeeProfitReport/employeeProfitReport.component';
import {SerialNoStatusComponent} from './inventory/data-vaults/serialnostatuses/serialnoStatus.component';
import {SerialNoHistoryReportComponent} from './inventory/reports/serialNoHistoryReport.component';
import {ApplicationFeeTrackerComponent} from './jobs/applicationfeeTracker/applicationFeeTracker.component';
import{VoucherComponent}from './vouchers/voucher.component';
@NgModule({
    imports: [
        EmailEditorModule,
        RouterModule.forChild([
            {
                path: '',
                children: [
                    { path: 'smsTemplates/smsTemplates', component: SmsTemplatesComponent, data: { permission: 'Pages.SmsTemplates' } },
                    { path: 'leadSources/leadSources/createOrEdit', component: CreateOrEditLeadSourceComponent, data: { permission: 'Pages.LeadSources.Create' } },
                    { path: 'leadSources/leadSources/view', component: ViewLeadSourceComponent, data: { permission: 'Pages.LeadSources' } },
                    { path: 'jobs/freebieTransports', component: FreebieTransportsComponent, data: { permission: 'Pages.FreebieTransports' } },
                    { path: 'invoices/invoiceStatuses', component: InvoiceStatusesComponent, data: { permission: 'Pages.InvoiceStatuses' } },
                    { path: 'jobs/jobs/jobActiveTracker', component: JobActiveRequestComponent, data: { permission: 'Pages.Tracker.JobActiveTracker' } },
                    { path: 'jobs/financeTracker/financeTracker', component: FinanceTrackerComponent, data: { permission: 'Pages.Tracker.FinanceTracker' } },
                    { path: 'jobs/stctracker/stctracker', component: StcTrackerComponent, data: { permission: 'Pages.Tracker.STCTracker' } },
                    { path: 'referral/referraltracker', component: ReferraltrackerComponent, data: { permission: 'Pages.Tracker.Referral' } },
                    { path: 'warranty/warrantytracker', component: WarrantytrackerComponent, data: { permission: 'Pages.Tracker.Warranty' } },

                    { path: 'jobs/jobcancellationreason', component: JobCancellationReasonComponent, data: { permission: 'Pages.Tenant.datavault.JobCancellationReason' } },
                    { path: 'holdReasons/holdReasons', component: HoldReasonsComponent, data: { permission: 'Pages.HoldReasons' } },
                    { path: 'documentTypes/documenttypes', component: DocumentTypesComponent, data: { permission: 'Pages.Tenant.datavault.DocumentType' } },
                    { path: 'documentLibrary/documentlibrary', component: DocumentLibrarysComponent, data: { permission: 'Pages.Tenant.datavault.DocumentLibrary' } },
                    { path: 'myinstaller/myinstaller', component: MyinstallerComponent, data: { permission: 'Pages.MyInstaller' } },
                    { path: 'installation/installation/pendinginstallation', component: PendingInstallationComponent, data: { permission: 'Pages.PendingInstallation' } },
                    { path: 'installation/installation/pendinginstallationdetail', component: PendingInstallationDetailComponent, data: { permission: 'Pages.PendingInstallation' } },
                    { path: 'installation/jobBooking', component: JobBookingComponent, data: { permission: 'Pages.JobBooking' } },
                    // { path: 'documenttypes/createOrEdit', component: CreateOrEditDocumentTypeComponent },
                    { path: 'gridconnectiontracker/gridconnectiontracker', component: GridConnectionTrackerComponent, data: { permission: 'Pages.Tracker.Gridconnectiontracker' } },
                    { path: 'gridconnectiontracker/gridconnectiondetail', component: GridConnectionDetailComponent, data: { permission: 'Pages.Tracker.Gridconnectiontracker.Detail' } },
                    { path: 'emailtemplates/emailtemplates', component: EmailTemplateComponent },
                    { path: 'emailtemplates/createOrEdit', component: CreateOrEditEmailTemplateComponent },
                    { path: 'jobs/refundReasons', component: RefundReasonsComponent, data: { permission: 'Pages.RefundReasons' } },
                    { path: 'jobs/jobRefunds', component: JobRefundsComponent, data: { permission: 'Pages.Tracker.RefundTracker' } },
                    { path: 'installation/installation', component: InstallationComponent, data: { permission: 'Pages.Installation' } },
                    { path: 'calenders/calenders', component: CalendersComponent, data: { permission: 'Pages.Calenders' } },
                    { path: 'intaller-map/intaller-map', component: IntallerMapComponent, data: { permission: 'Pages.Map' } },
                    { path: 'jobs/variations', component: VariationsComponent, data: { permission: 'Pages.Variations' } },
                    { path: 'jobs/jobPromotions/freebies', component: FreebiesComponent, data: { permission: 'Pages.Tracker.FreebiesTracker' } },
                    { path: 'invoices/invoicePaymentMethods', component: InvoicePaymentMethodsComponent, data: { permission: 'Pages.InvoicePaymentMethods' } },
                    { path: 'invoices/invoicePayments', component: InvoicePaymentsComponent, data: { permission: 'Pages.Tracker.InvoiceTracker' } },
                    { path: 'invoices/invoiceIssued', component: InvoiceIssuedComponent, data: { permission: 'Pages.Tracker.InvoiceIssuedTracker' } },
                    { path: 'invoices/new-invoice-issued', component: NewInvoiceIssuedComponent, data: {} },
                    { path: 'invoices/invoice-payway', component: InvoicePayWayComponent, data: { permission: 'Pages.InvoicePayWay' } },
                    { path: 'invoices/installerInvoiceFileList', component: InstallerInvoiceFileListComponent, data: {permission: 'Pages.InvoiceFileList'} },
                    { path: 'invoices/invoicePayments/createOrEdit', component: CreateOrEditInvoicePaymentComponent, data: { permission: 'Pages.InvoicePayments.Create' } },
                    { path: 'jobs/jobs/jobslist', component: JobsListComponent, data: { permission: 'Pages.Tracker.ApplicationTracker' } },
                    { path: 'jobs/jobGrid/jobGrid', component: jobGridComponent, data: { permission: 'Pages.JobGrid' } },
                    { path: 'jobs/holdjobs/holdJobs', component: HoldJobsComponent, data: { permission: 'Pages.HoldJobTracker' } },
                    { path: 'jobs/jobs/jobDetail', component: JobDetailModalComponent, data: { permission: 'Pages.Jobs' } },
                    { path: 'jobs/promotionMasters', component: PromotionMastersComponent, data: { permission: 'Pages.PromotionMasters' } },
                    { path: 'jobs/financeOptions', component: FinanceOptionsComponent, data: { permission: 'Pages.FinanceOptions' } },
                    { path: 'jobs/houseTypes', component: HouseTypesComponent, data: { permission: 'Pages.HouseTypes' } },
                    { path: 'cancelReasons/cancelReasons', component: CancelReasonsComponent, data: { permission: 'Pages.CancelReasons' } },
                    { path: 'rejectReasons/rejectReasons', component: RejectReasonsComponent, data: { permission: 'Pages.RejectReasons' } },
                    { path: 'stcPvdStatus/stcpvdstatus', component: Stcpvdstatuscomponent, data: { permission: 'Pages.StcPvdStatus' } },
                    { path: 'serviceCategory/serviceCategory', component: ServiceCategoryComponent, data: { permission: 'Pages.ServiceCategory' } },
                    { path: 'serviceSources/serviceSources', component: ServiceSourcesComponent, data: { permission: 'Pages.ServiceSources' } },
                    { path: 'serviceStatus/serviceStatus', component: ServiceStatusComponent, data: { permission: 'Pages.ServiceStatus' } },
                    { path: 'serviceType/serviceType', component: ServiceTypeComponent, data: { permission: 'Pages.ServiceType' } },
                    { path: 'serviceSubCategory/serviceSubCategory', component: ServiceSubCategoryComponent, data: { permission: 'Pages.ServiceSubCategory' } },
                    { path: 'reviewType/reviewType', component: ReviewTypeComponent, data: { permission: 'Pages.ReviewType' } },
                    { path: 'jobs/meterPhases', component: MeterPhasesComponent, data: { permission: 'Pages.MeterPhases' } },
                    { path: 'jobs/meterUpgrades', component: MeterUpgradesComponent, data: { permission: 'Pages.MeterUpgrades' } },
                    { path: 'jobs/elecRetailers', component: ElecRetailersComponent, data: { permission: 'Pages.ElecRetailers' } },
                    { path: 'jobs/depositOptions', component: DepositOptionsComponent, data: { permission: 'Pages.DepositOptions' } },
                    { path: 'jobs/paymentOptions', component: PaymentOptionsComponent, data: { permission: 'Pages.PaymentOptions' } },
                    { path: 'jobs/jobStatuses', component: JobStatusesComponent, data: { permission: 'Pages.JobStatuses' } },
                    { path: 'jobs/productItems', component: ProductItemsComponent, data: { permission: 'Pages.ProductItems' } },
                    { path: 'jobs/productItems/createOrEdit', component: CreateOrEditProductItemComponent, data: { permission: 'Pages.ProductItems.Create' } },
                    { path: 'jobs/productItems/view', component: ViewProductItemComponent, data: { permission: 'Pages.ProductItems' } },
                    { path: 'jobs/elecDistributors', component: ElecDistributorsComponent, data: { permission: 'Pages.ElecDistributors' } },
                    { path: 'jobs/roofAngles', component: RoofAnglesComponent, data: { permission: 'Pages.RoofAngles' } },
                    { path: 'jobs/roofTypes', component: RoofTypesComponent, data: { permission: 'Pages.RoofTypes' } },
                    { path: 'jobs/productTypes', component: ProductTypesComponent, data: { permission: 'Pages.ProductTypes' } },
                    { path: 'jobs/jobTypes', component: JobTypesComponent, data: { permission: 'Pages.JobTypes' } },
                    { path: 'jobs/jobTypes/createOrEdit', component: CreateOrEditJobTypeComponent, data: { permission: 'Pages.JobTypes.Create' } },
                    { path: 'jobs/jobTypes/view', component: ViewJobTypeComponent, data: { permission: 'Pages.JobTypes' } },
                    { path: 'promotions/promotionUsers', component: PromotionUsersComponent, data: { permission: 'Pages.PromotionUsers' } },
                    { path: 'promotions/promotions', component: PromotionsComponent, data: { permission: 'Pages.Promotions' } },
                    { path: 'promotions/promotions/createOrEdit', component: CreateOrEditPromotionComponent, data: { permission: 'Pages.Promotions.Create' } },
                    { path: 'departments/departments', component: DepartmentsComponent, data: { permission: 'Pages.Departments' } },
                    { path: 'userDashboard', component: UserDashboardComponent, data: { permission: 'Pages.User.Dashboard' } },
                    { path: 'expense/leadExpenses', component: LeadExpensesComponent, data: { permission: 'Pages.LeadExpenses' } },
                    { path: 'expense/leadExpenses/createOrEdit', component: CreateOrEditLeadExpenseModalComponent, data: { permission: 'Pages.LeadExpenses.Create' } },
                    { path: 'expense/leadExpenses/view', component: ViewLeadExpenseModalComponent, data: { permission: 'Pages.LeadExpenses' } },
                    { path: 'theSolarDemo/userTeams', component: UserTeamsComponent, data: { permission: 'Pages.UserTeams' } },
                    { path: 'theSolarDemo/userTeams/createOrEdit', component: CreateOrEditUserTeamComponent, data: { permission: 'Pages.UserTeams.Create' } },
                    { path: 'theSolarDemo/userTeams/view', component: ViewUserTeamComponent, data: { permission: 'Pages.UserTeams' } },
                    { path: 'theSolarDemo/categories', component: CategoriesComponent, data: { permission: 'Pages.Categories' } },
                    { path: 'theSolarDemo/categories/createOrEdit', component: CreateOrEditCategoryModalComponent, data: { permission: 'Pages.Categories.Create' } },
                    { path: 'theSolarDemo/categories/view', component: ViewCategoryModalComponent, data: { permission: 'Pages.Categories' } },
                    { path: 'theSolarDemo/teams', component: TeamsComponent, data: { permission: 'Pages.Teams' } },
                    
                    { path: 'installation-cost/other-charges', component: OtherChargesComponent, data: { permission: 'Pages.Tenant.DataVaults.InstallationCost.OtherCharges' } },
                    { path: 'installation-cost/statewiseCost', component: StateWiseCostComponent, data: { permission: 'Pages.Tenant.DataVaults.InstallationCost.StateWiseInstallationCost' } },
                    { path: 'installation-cost/installationItemList', component: InstallationItemComponent, data: { permission: 'Pages.Tenant.DataVaults.InstallationCost.InstallationItemList' } },
                    { path: 'installation-cost/jobcostfixexpense', component: JobCostFixExpenseComponent, data: { permission: 'Pages.Tenant.DataVaults.InstallationCost.JobCostFixExpense' } },
                    { path: 'installation-cost/fixCostPrice', component: FixCostPriceComponent, data: { permission: 'Pages.Tenant.DataVaults.InstallationCost.FixedCostPrice' } },
                    { path: 'installation-cost/extraInstallationCharge', component: ExtraInstallatIonChargeComponent, data: { permission: 'Pages.Tenant.DataVaults.InstallationCost.ExtraInstallationCharges' } },
                    { path: 'installation-cost/stcCost', component: STCCostComponent, data: { permission: 'Pages.Tenant.DataVaults.InstallationCost.STCCost' } },
                    { path: 'installation-cost/batteryInstallationCost', component: BatteryInstallationCostComponent, data: { permission: 'Pages.Tenant.DataVaults.InstallationCost.BatteryInstallationCost' } },
                    { path: 'installation-cost/categoryInstallationItem', component: CategoryInstallationItemComponent, data: { permission: '' } },
                    { path: 'installation-cost/postcodecost', component: postCodeWiseCostComponent, data: { permission: 'Pages.Tenant.DataVaults.InstallationCost.PostCodeCost' } },

                    { path: 'theSolarDemo/teams/createOrEdit', component: CreateOrEditTeamComponent, data: { permission: 'Pages.Teams.Create' } },
                    { path: 'theSolarDemo/teams/view', component: ViewTeamComponent, data: { permission: 'Pages.Teams' } },
                    { path: 'priceItemLists/priceItemLists', component: PriceItemListsComponent, data: { permission: 'Pages.PriceItemLists' } },
                    { path: 'leads/leads', component: LeadsComponent, data: { permission: 'Pages.ManageLeads' } },
                    { path: 'leads/leads/createOrEdit', component: CreateOrEditLeadComponent, data: { permission: 'Pages.Leads' } },
                    { path: 'leads/leads/view', component: ViewLeadComponent, data: { permission: 'Pages.Leads' } },
                    { path: 'leads/leads/activityLog', component: ActivityLogLeadComponent, data: { permission: 'Pages.Leads' } },
                    { path: 'leads/leads/assignOrTransfer', component: AssignOrTransferLeadComponent, data: { permission: 'Pages.Leads' } },
                    { path: 'leads/leads/changeLeadStatus', component: ChangeStatusLeadComponent, data: { permission: 'Pages.Leads' } },
                    { path: 'myleads/myleads', component: MyLeadsComponent, data: { permission: 'Pages.MyLeads' } },
                    { path: 'myleads/myleads/myview', component: ViewMyLeadComponent, data: { permission: 'Pages.Leads' } },
                    { path: 'myleads/myleads/myactivityLog', component: ActivityLogMyLeadComponent, data: { permission: 'Pages.Leads' } },
                    { path: 'duplicatelead/duplicatelead', component: DuplicateleadsComponent, data: { permission: 'Pages.Lead.Duplicate' } },
                    { path: 'closedlead/closedlead', component: ClosedleadsComponent, data: { permission: 'Pages.Lead.Close' } },
                    { path: 'closedlead/closedlead/viewclosedlead', component: ViewClosedLeadComponent, data: { permission: 'Pages.Lead.Close' } },
                    { path: 'cancelleads/cancelleads', component: CancelleadsComponent, data: { permission: 'Pages.Lead.Calcel' } },
                    { path: 'rejectleads/rejectleads', component: RejectleadsComponent, data: { permission: 'Pages.Lead.Rejects' } },
                    { path: 'installer/newinstaller', component: NewinstallerComponent, data: { permission: 'Pages.Installer.New' } },
                    { path: 'services/manageservices/manageservice', component: ManageServicesComponent, data: { permission: 'Pages.Lead.ManageService' } },
                    { path: 'services/myservices/myservice', component: MyServicesComponent, data: { permission: 'Pages.Service.MyService' } },
                    { path: 'services/service-map/service-map', component: ServiceMapComponent, data: { permission: 'Pages.Service.ServiceMap' } },
                    { path: 'services/serviceInstallation/serviceInstallation', component: ServiceInstallationComponent, data: { permission: 'Pages.Service.ServiceInstallation' } },
                    { path: 'review/review', component: ReviewComponent, data: { permission: 'Pages.Review' } },
                    { path: 'installer/invoiceinstaller', component: InvoiceinstallerComponent, data: { permission: 'Pages.Installer.Invoice' } },
                    { path: 'installer/radytoPayInstaller', component: ReadytoPayinstallerComponent, data: { permission: 'Pages.Installer.ReadyToPay' } },
                    { path: 'result/result', component: ResultsComponent, data: { permission: 'Pages.Leads' } },
                    { path: 'result/result/viewResult', component: ViewResultLeadComponent, data: { permission: 'Pages.Leads' } },
                    { path: 'leadSources/leadSources', component: LeadSourcesComponent, data: { permission: 'Pages.LeadSources' } },
                    { path: 'leadSources/leadSources/createOrEdit', component: CreateOrEditLeadSourceModalComponent, data: { permission: 'Pages.LeadSources.Create' } },
                    { path: 'leadSources/leadSources/view', component: ViewLeadSourceModalComponent, data: { permission: 'Pages.LeadSources' } },
                    { path: 'postalTypes/postalTypes', component: PostalTypesComponent, data: { permission: 'Pages.PostalTypes' } },
                    { path: 'leads/leads/leadtracker', component: LeadTrackerComponent, data: { permission: 'Pages.LeadTracker' } },
                    { path: 'postalTypes/postalTypes/createOrEdit', component: CreateOrEditPostalTypeModalComponent, data: { permission: 'Pages.PostalTypes.Create' } },
                    { path: 'postalTypes/postalTypes/view', component: ViewPostalTypeModalComponent, data: { permission: 'Pages.PostalTypes' } },
                    { path: 'postCodes/postCodes', component: PostCodesComponent, data: { permission: 'Pages.PostCodes' } },
                    { path: 'postCodes/postCodes/createOrEdit', component: CreateOrEditPostCodeModalComponent, data: { permission: 'Pages.PostCodes.Create' } },
                    { path: 'postCodes/postCodes/view', component: ViewPostCodeModalComponent, data: { permission: 'Pages.PostCodes' } },
                    { path: 'streetNames/streetNames', component: StreetNamesComponent, data: { permission: 'Pages.StreetNames' } },
                    { path: 'streetNames/streetNames/createOrEdit', component: CreateOrEditStreetNameModalComponent, data: { permission: 'Pages.StreetNames.Create' } },
                    { path: 'streetNames/streetNames/view', component: ViewStreetNameModalComponent, data: { permission: 'Pages.StreetNames' } },
                    { path: 'streetTypes/streetTypes', component: StreetTypesComponent, data: { permission: 'Pages.StreetTypes' } },
                    { path: 'streetTypes/streetTypes/createOrEdit', component: CreateOrEditStreetTypeModalComponent, data: { permission: 'Pages.StreetTypes.Create' } },
                    { path: 'streetTypes/streetTypes/view', component: ViewStreetTypeModalComponent, data: { permission: 'Pages.StreetTypes' } },
                    { path: 'unitTypes/unitTypes', component: UnitTypesComponent, data: { permission: 'Pages.UnitTypes' } },
                    { path: 'unitTypes/unitTypes/createOrEdit', component: CreateOrEditUnitTypeModalComponent, data: { permission: 'Pages.UnitTypes.Create' } },
                    { path: 'unitTypes/unitTypes/view', component: ViewUnitTypeModalComponent, data: { permission: 'Pages.UnitTypes' } },
                    { path: 'reply/smsReply', component: SmsReplyComponent, data: { permission: 'Pages.Sms' } },
                    { path: 'reply/emailReply', component: EmailReplyComponent, data: { permission: 'Pages.Email' } },
                    { path: 'services/warrentyClaim/warrentyClaim', component: WarrentyClaimComponent, data: { permission: 'Pages.Service.WarrantyClaim' } },
                    { path: 'services/warrentyClaim/serviceInvoice', component: ServiceInvoiceModelComponent, data: { permission: 'Pages.Service.ServiceInvoice' } },
                    // { path: 'services/serviceDocumentTypes/serviceDocumentTypes', component: ServiceDocumentTypesComponent, data: { permission: 'Pages.Tenant.Services.ServiceDocumentType' } },
                    
                    //Report Section
                    
                    { path: 'report/activityreport', component: ActivityreportComponent, data: { permission: 'Pages.ActivityReport' } },
                    { path: 'report/leadexpensereport/leadexpense-report', component: LeadexpenseReportComponent, data: { permission: 'Pages.LeadexpenseReport' } },
                    { path: 'report/todo-activity-list', component: TodoActivityListComponent, data: { permission: 'Pages.ToDoActivityReport' } },
                    { path: 'report/outstandingreport', component: OutStandingReportComponent, data: { permission: 'Pages.OutStandingReport' } },
                    { path: 'report/jobcost', component: JobCostComponent, data: { permission: 'Pages.Report.JobCost' } },
                    { path: 'report/callhistory/usercallhistory', component: UserCallHistoryComponent, data: { permission: 'Pages.CallHistory.UserCallHistory' } },
                    { path: 'report/callhistory/statewisecallhistory', component: StateWiseCallHistoryComponent, data: { permission: 'Pages.CallHistory.StateWiseCallHistory' } },
                    { path: 'report/callQueue', component: CxCallQueueComponent, data: { permission: 'Pages.CallHistory.CallFlowQueueCallHistory' } },
                    { path: 'report/leadassign', component: LeadAssignReportComponent, data: { permission: 'Pages.Report.LeadAssign' } },                    
                    { path: 'report/leadsold', component: LeadSoldReportComponent, data: { permission: 'Pages.Report.LeadSold' } },                    
                    { path: 'report/employeeJobCost', component: EmployeeJobCostComponent, data: { permission: 'Pages.Report.EmpJobCost' } },
                    { path: 'report/productSold', component: ProductSoldReportComponent, data: { permission: '' } },                    
                    { path: 'report/smscountreport', component: SmsCountReportComponent, data: { permission: '' } },                    
                    { path: 'report/callQueueweekly', component: CxCallQueueWeeklyComponent, data: { permission: 'Pages.CallHistory.CallFlowQueueWeeklyCallHistory' } },
                    { path: 'report/callQueueUserwise', component: CxCallQueueUserWiseComponent, data: { permission: 'Pages.CallHistory.CallFlowQueueUserWiseCallHistory' } },
                    { path: 'report/installerinvoicepayment', component: InstallerInvoicePaymentReportComponent, data: { permission: 'Pages.Report.InstallerInvoicePayment' } },
                    { path: 'report/installerpaymentinstallerwise', component: InstallerPaymentInstallerWiseReportComponent, data: { permission: 'Pages.Report.InstallerWiseInvoicePayment' } },

                    { path: 'dashboard', component: DashboardComponent, data: { permission: 'Pages.Tenant.Dashboard' } },
                    { path: 'dashboard-invoice', component: DashboardInvoiceComponent, data: { permission: 'Pages.Tenant.Dashboard' } },                    
                    { path: 'dashboard-sales', component: DashboardSalesComponent, data: { permission: 'Pages.Tenant.Dashboard' } },                    
                    { path: 'dashboard-service', component: DashboardServiceComponent, data: { permission: 'Pages.Tenant.Dashboard' } },                    
                    { path: 'quotationtemplate/quotationtemplate', component: QuotationTemplateComponent, data: { permission: 'Pages.QuotationTemplate' } },
                    { path: 'quotationtemplate/quotationtemplate/createOrEdit', component: CreateOrEditQuotationTemplateModalComponent },
                    { path: 'postcoderange/postcoderange', component: PostCodeRangeComponent, data: { permission: 'Pages.PostCodeRange' } },
                    { path: 'warehouselocation', component: WarehouseLocationComponent, data: { permission: 'Pages.WarehouseLocation' } },
                    { path: 'productpackage', component: ProductPackageComponent, data: { permission: 'Pages.ProductPackages' } },
                    { path: 'callflowqueue', component: CallFlowQueueComponent, data: { permission: 'Pages.CallFlowQueues' } },

                    { path: 'my-lead-generation', component: MyLeadGenerationComponent, data: { permission: 'Pages.LeadGeneration.MyLeadsGeneration' } },
                    { path: 'lead-generation-map', component: LeadGenMapComponent, data: { permission: 'Pages.LeadGeneration.Map' } },
                    { path: 'lead-generation-installation', component: LeadGenInstallationComponent, data: { permission: 'Pages.LeadGeneration.Installation' } },
                    { path: 'dashboard-wholesale', component: DashboardWholesaleComponent, data: { permission: '' } },
                    { path: 'wholesale/leads', component: WholesaleLeadsComponent, data: { permission: '' } },  
                    { path: 'wholesale/leads/createEditWholesaleLead', component: CreateEditWholesaleLeadComponent, data: { permission: '' } },
                    { path: 'wholesale/promotions/promotions', component: WholesalePromotionsComponent, data: { permission: '' } },
                    { path: 'wholesale/promotions/promotions/app-create-or-edit-promotion', component: CreateOrEditWhlPromotionComponent, data: { permission: '' } },
                    { path: 'wholesale/promotions/promotionUsers', component: PromotionUsersWholesaleComponent, data: { permission: '' } },                  
                    { path: 'wholesale/invoice', component: WholesaleInvoiceComponent, data: { permission: '' } },
                    { path: 'wholesale/invoice/createEditWholesaleInvoice', component: createEditWholesaleInvoiceComponent, data: { permission: '' } },
                    { path: 'wholesale/data-vaults/wholesale-type', component: WholesaleStatusTypesComponent, data: { permission: '' } },
                    { path: 'wholesale/data-vaults/invoice-type', component: InvoiceTypesComponent, data: { permission: '' } },
                    { path: 'wholesale/data-vaults/wholesale-bdm', component: WholesaleBDMComponent, data: { permission: '' } },
                    { path: 'wholesale/data-vaults/delivery-options', component: DeliveryOptionsComponent, data: { permission: '' } },
                    { path: 'wholesale/data-vaults/job-type', component: WholesaleJobTypesComponent, data: { permission: '' } },
                    { path: 'wholesale/data-vaults/transport-type', component: TransportTypesComponent, data: { permission: '' } },
                    { path: 'wholesale/data-vaults/wholesalelead-documenttype', component: WholeSaleLeadDocumentTypesComponent, data: { permission: '' } },
                    { path: 'wholesale/data-vaults/smsTemplates', component: WholeSaleSmsTemplatesComponent, data: { permission: '' } },
                    { path: 'wholesale/data-vaults/emailTemplates', component: WholeSaleEmailTemplatesComponent, data: { permission: '' } },
                    { path: 'wholesale/data-vaults/emailTemplates/createOrEdit', component: CreateOrEditWholeSaleEmailTemplateModalComponent },
                    { path: 'dashboard2', component: Dashboard2Component, data: { permission: 'Pages.Tenant.Dashboard' } },                            
                    { path: 'inventory/dashboard', component: DashboardInventoryComponent, data: { permission: '' } },
                    // { path: 'inventory/data-vaults/vendors', component: VendorsComponent, data: { permission: '' } },
                    // { path: 'inventory/data-vaults/transport-company', component: TransportCompanyComponent, data: { permission: '' } },
                    // { path: 'inventory/data-vaults/stock-order-status', component: StockOrderStatusComponent, data: { permission: '' } },
                    // { path: 'inventory/data-vaults/payment-method', component: PaymentMethodComponent, data: { permission: '' } },
                    // { path: 'inventory/data-vaults/delivery-type', component: DeliveryTypeComponent, data: { permission: '' } },  
                    // { path: 'inventory/data-vaults/stock-order', component: StockOrderComponent, data: { permission: '' } },  
                    // { path: 'inventory/data-vaults/stock-currency', component: StockCurrencyComponent, data: { permission: '' } },  
                    // { path: 'inventory/data-vaults/stock-from', component: StockFromComponent, data: { permission: '' } },  
                    // { path: 'inventory/data-vaults/purchase-company', component: PurchaseCompanyComponent, data: { permission: '' } },  
                    { path: 'inventory/stock-transfer', component: StockTransferComponent, data: { permission: '' } },
                    { path: 'inventory/purchase-order-new', component: PurchaseOrderComponent, data: { permission: '' } },
                    { path: 'inventory/purchase-order-new/StockOrderDetailModal', component: StockOrderDetailModal, data: { permission: '' } },
                    { path: 'serviceDocumentTypes/serviceDocumenttypes', component: ServiceDocumentTypesComponent, data: { permission: 'Pages.Tenant.datavault.ServiceDocumentType' } },
                    { path: 'wholesale/dashboard-kanban', component: WholesaleDashboardComponent, data: { permission: '' } },                            
                    { path: 'commission', component: CommissionComponent, data: { permission: 'Pages.LeadGeneration.Commission' } },
                    { path: 'report/jobcommission', component: JobCommissionComponent, data: { permission: 'Pages.Report.JobCommission' } },
                    { path: 'report/jobcommissionpaid', component: JobCommissionPaidComponent, data: { permission: 'Pages.Report.JobCommissionPaid' } },
                    { path: 'report/jobvariation', component: JobVariationReportComponent, data: { permission: 'Pages.Report.JobVariation' } },
                    { path: 'commission-range', component: CommissionRangeComponent, data: { permission: '' } },
                    { path: 'commission-range', component: createOrEditCommissionRangeModalComponent },
                    { path: 'commission-range', component: viewcommissionRangeModalComponent },
                    { path: 'dashboard-message', component: DashboardMessageComponent },
                    { path: 'wholesale/ecommerce/ecommerceslider', component: ECommerceSliderComponent, data: { permission: 'Pages.Tenant.WholeSale.ECommerce.Slider' } },
                    { path: 'wholesale/ecommerce/add-product', component: WholesaleProductComponent, data: { permission: '' } },
                    { path: 'wholesale/ecommerce/add-product', component: CreateOrEditProductModalComponent, data: { permission: '' } },
                    { path: 'wholesale/ecommerce/add-product', component: ViewProductModalComponent, data: { permission: '' } },
                    { path: 'wholesale/ecommerce/special-offers', component: SpecialOffersComponent, data: { permission: '' } },
                    { path: 'wholesale/ecommerce/special-offers', component: CreateOrEditSpecialOfferModalComponent, data: { permission: '' } },
                    { path: 'wholesale/ecommerce/special-offers', component: ViewSpecialOfferModalComponent, data: { permission: '' } },
                    { path: 'wholesale/credit/credit-applications', component: CreditApplicationsComponent, data: { permission: '' } },
                    { path: 'wholesale/credit/credit-applications', component: ViewCreditApplicationModalComponent, data: { permission: '' } },
                    { path: 'wholesale/ecommerce/brandingpartner', component: BrandingPartnerComponent, data: { permission: 'Pages.Tenant.WholeSale.ECommerce.BrandingPartner' } },
                    { path: 'wholesale/ecommerce/userrequest', component: UserRequestComponent, data: { permission: 'Pages.Tenant.WholeSale.ECommerce.UserRequest' } },
                    { path: 'wholesale/ecommerce/solarpackage', component: EcommerceSolarPackagePackageComponent, data: { permission: 'Pages.Tenant.WholeSale.ECommerce.EcommerceSolarPackages' } },
                    { path: 'wholesale/ecommerce/contactus', component: ContactUsComponent, data: { permission: 'Pages.Tenant.WholeSale.ECommerce.EcommerceContactUs' } },
                    { path: 'report/userdetail', component: UserDetailReportComponent, data: { permission: 'Pages.Report.UserDetails' } },
                    { path: 'report/comparison', component: ComparisonReportComponent, data: { permission: 'Pages.Report.Comparison' } },
                    { path: 'report/monthlycomparison', component: MonthlyComparisonReportComponent, data: { permission: 'Pages.Report.MonthlyComparison' } },
                    { path: 'leads/leads/3rdpartyleadtracker', component: ThirdrdPartyLeadTrackerComponent, data: { permission: 'Pages.3rdPartyLeadTracker' } },
                    { path: 'leads/leads/cimettracker', component: CIMETLeadTrackerComponent, data: { permission: 'Pages.Tracker.CIMETTracker' } },
                    { path: 'datavaultactivitylog/datavaultactivitylog', component: DataVaultActivityLogComponent, data: { permission: 'Pages.Tenant.datavault.ActivityLog' } },
                    { path: 'state', component: StateComponent, data: { permission: 'Pages.DataVaults.State' } },

                    { path: 'transportcost/transportcost', component: TransportCostComponent, data: { permission: 'Pages.TransportCost' } },
                    
                    { path: 'checkDepositReceived', component: CheckDepositReceivedComponent, data: { permission: 'Pages.Tenant.DataVaults.CheckDepositReceived' } },
                    { path: 'checkActives/chcekActives', component: CheckActiveComponent,data: { permission: 'Pages.CheckActive' } },
                    { path: 'chcekApplications/checkApplications', component: CheckApplicationComponent,data: { permission: 'Pages.CheckApplication' } },
                    { path: 'quickstock/PurchaseCompany', component: PurchaseCompanyComponent,data: { permission: 'Pages.Tenant.QuickStock.DataVaults.PurchaseCompany' } },
                    { path: 'quickstock/stockorderstatus', component: StockOrderStatusComponent, data : {Permissions : 'Pages.Tenant.QuickStock.DataVaults.StockOrderStatus'} },
                    { path: 'quickstock/deliverytype', component: DeliveryTypeComponent, data : {Permissions : 'Pages.Tenant.QuickStock.DataVaults.DeliveryTypes'} },
                    { path: 'quickstock/paymentstatus', component: PaymentStatusComponent, data : {Permissions : 'Pages.Tenant.QuickStock.DataVaults.PaymentStatus'} },
                    { path: 'quickstock/paymentmethod', component: PaymentMethodComponent, data : {Permissions : 'Pages.Tenant.QuickStock.DataVaults.PaymentMethod'} },
                    { path: 'transportcompany', component: TransportcompanyComponent, data : {Permissions : 'Pages.Tenant.QuickStock.DataVaults.TransportCompanies'} },
                    { path: 'quickstock/paymenttype', component: PaymentTypeComponent, data : {Permissions : 'Pages.Tenant.QuickStock.DataVaults.PaymentType'} },
                    { path: 'quickstock/currency', component: CurrencyComponent, data : {Permissions : 'Pages.Tenant.QuickStock.DataVaults.Currencies'} },
                    { path: 'quickstock/freightcompany', component: FreightCompanyComponent, data : {Permissions : 'Pages.Tenant.QuickStock.DataVaults.FreightCompany'} },
                    { path: 'quickstock/stockfrom', component: StockFromComponent, data : {Permissions : 'Pages.Tenant.QuickStock.DataVaults.StockFroms'} },
                    { path: 'quickstock/stockorderfor', component: StockOrderForComponent, data : {Permissions : 'Pages.Tenant.QuickStock.DataVaults.StockOrderFors'} },
                    { path: 'quickstock/purchasedocumentlist', component: PurchaseDocumentListComponent, data : {Permissions : 'Pages.Tenant.QuickStock.DataVaults.PurchaseDocumentList'} },
                    { path: 'report/jobcostmonthwise', component: JobCostMonthWiseComponent, data: { permission: 'Pages.Report.JobCostMonthWise' } },
                    { path: 'quickstock/incoTerm', component: IncoTermComponent, data : {Permissions : 'Pages.Tenant.QuickStock.DataVaults.IncoTerm'} },
                    { path: 'quickstock/vendors', component: vendorComponent, data : {Permissions : 'Pages.Tenant.QuickStock.DataVaults.Vendor'} },
                    { path: 'wholesale/ecommerce/subcribers', component: EcommerceSubscriberComponent, data: { permission: 'Pages.Tenant.WholeSale.ECommerce.Subscribe' } },
                    { path: 'quickstock/purchase-order-new', component: StockOrderComponent, data : {Permissions : 'Pages.Tenant.QuickStock.PurchaseOrder'} },

                    { path: 'inventory/stockorder', component: PurchaseOrderComponent, data : {Permissions : 'Pages.Tenant.QuickStock.PurchaseOrder'} },
                    { path: 'quickstock/stockTransfers', component: StockTransfersComponent, data : {Permissions : 'Pages.Tenant.QuickStock.StockTransfer'} },

                    { path: 'wholesaleheaderresult', component: WholeSaleLeadHeaderResultsComponent, data: { permission: 'Pages.Tenant.WholeSale' } },
                    { path: 'wholesaleheaderresult/viewResult', component: ViewResultWholeSaleLeadComponent, data: { permission: 'Pages.Tenant.WholeSale' } },

                    { path: 'wholesale/data-vaults/series', component: SeriesComponent, data: { permission: 'Pages.Tenant.WholeSale.DataVaults.Serieses' } },

                    { path: 'wholesale/data-vaults/propery-type', component: PropertyTypeComponent, data: { permission: '' } },
                    { path: 'wholesale/data-vaults/pvd-status', component: PVDStatusComponent, data: { permission: '' } },
                    { path: 'wholesale/data-vaults/jobStatus', component: WholesaleJobStatusComponent, data: { permission: '' } },

                    { path: 'wholesale/requesttotransferlead', component: RequestToWholeSaleLeadTransferComponent, data: { permission: 'Pages.WholeSale.RequestToTransferLeads' } },
                    { path: 'wholesale/data-vaults/activity', component: WholeSaleDataVaultActivityLogComponent, data: { permission: 'Pages.Tenant.WholeSale.datavault.ActivityLog' } },
                    { path: 'wholesale/ecommerce/activity', component: EcommerceDataVaultActivityLogComponent, data: { permission: 'Pages.Tenant.Ecommerce.datavault.ActivityLog' } },
                    { path: 'jobs/essentialTracker', component: EssentialTrackerComponent, data: { permission: 'Pages.Tracker.EssentialTracker' } },
                    { path: 'wholesale/reply', component: WholesaleSmsReplyComponent, data: { permission: 'Pages.Tenant.WholeSale.Sms' } },
                    
                    { path: 'wholesale/ecommerce/producttype', component: EcommerceProductTypeComponent, data: { permission: 'Pages.Tenant.WholeSale.DataVaults.ProductType' } },
                    { path: 'wholesale/ecommerce/specification', component: EcommerceSpecificationComponent, data: { permission: '' }},

                    { path: 'report/leadSources', component: LeadSourceComparisonReportComponent, data: { permission: 'Pages.Report.LeadComparison' } },
                    { path: 'report/rescheduleinstallation', component: RescheduleInstallationReportComponent, data: { permission: 'Pages.Report.RescheduleInstallation' } },
                    { path: 'inventory/stock-payment', component: StockPaymentComponent, data : {Permissions : 'Pages.Tenant.QuickStock.StockPayment'} },
                    { path: 'inventory/stock-payment-tracker', component: StockPaymentTrackerComponent, data : {Permissions : 'Pages.Tenant.QuickStock.StockPaymentTracker'} },
                    { path: 'report/progessReport', component: ProgressReportComponent, data: { permission: 'Pages.Report.ProgressReport' } },
                    { path: 'quickstock/stock-variation', component: StockVariationsComponent, data: { permission: 'Pages.Tenant.QuickStock.DataVaults.StockVariations' } },
                    { path: 'wholesale/ecommerce/stcregister', component: EcommerceStcRegisterComponent, data: { permission: 'Pages.Tenant.WholeSale.ECommerce.EcommerceStcRegister' } },
                    { path: 'wholesale/ecommerce/stcDealPrice', component: EcommerceStcDealPriceComponent, data: { permission: 'Pages.Tenant.WholeSale.ECommerce.EcommerceStcDealPrice' } },
                    { path: 'inventory/data-vaults/activitylog', component: InventoryDataVaultActivityLogComponent, data: { permission: 'Pages.Tenant.QuickStock.DataVaults.ActivityLog' } },
                    { path: 'inventory/reports/stockOrderReceivedReport', component: StockOrderReceivedComponent, data: { permission: 'Pages.Tenant.QuickStock.Reports.StockReceivedReport' } },
                    { path: 'report/reviewReport', component: ReviewReportComponent, data: { permission: 'Pages.Report.ReviewReport' } },
                    { path: 'report/leadmonthlycomparison', component: leadMonthlyComparison, data: { permission: '' } },
                    { path: 'report/employeeProfitReport', component: EmployeeProfitReportComponent, data: { permission: 'Pages.Report.EmployeeProfit' } },
                    { path: 'inventory/data-vaults/serialnostatuses', component: SerialNoStatusComponent, data : {Permissions : 'Pages.Tenant.QuickStock.DataVaults.SerialNoStatus'} },
                    { path: 'inventory/reports/serialNoHistoryReport', component: SerialNoHistoryReportComponent, data: { permission: 'Pages.Tenant.QuickStock.Reports.SerialNoHistoryReport' } },
                    { path: 'jobs/applicationfeeTracker', component: ApplicationFeeTrackerComponent, data: { permission: 'Pages.Tracker.ApplicationFeeTracker' } },
                    { path: 'vouchers', component: VoucherComponent, data: { permission: 'Pages.Tracker.ApplicationFeeTracker' } },
                    { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
                    { path: '**', redirectTo: 'dashboard' },
                    
                    
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class MainRoutingModule { }
