import { Component, Injector, ViewEncapsulation } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DashboardCustomizationConst } from '@app/shared/common/customizable-dashboard/DashboardCustomizationConsts';
import { Title } from '@angular/platform-browser';
import { DatePipe } from '@angular/common';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
@Component({
    templateUrl: './dashboard-wholesale.component.html',
    styleUrls: ['./dashboard-wholesale.component.less'],
    encapsulation: ViewEncapsulation.None ,
    // styles: ['.kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper { padding-top:75px !important; }']
})

export class DashboardWholesaleComponent extends AppComponentBase {
    dashboardName = DashboardCustomizationConst.dashboardNames.defaultTenantDashboard;
    pipe = new DatePipe('en-US');
    today = this.pipe.transform(new Date(), 'MMM dd');

    constructor(
        injector: Injector,
        private titleService: Title,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Wholesale Dashboard");
    }
    showchild: boolean = true;
    shouldShow: boolean = false;
    addTaskShow: boolean = false;
    ngOnInit(): void {
       
       
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Wholesale Dashboard';
        log.section = 'Wholesale Dashboard';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
    }
}
