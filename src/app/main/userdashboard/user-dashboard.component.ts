import { Component,  Injector, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import { UserDashboardServiceProxy, GetLeadForDashboardDto, GetLeadForChangeStatusOutput, LeadsServiceProxy, GetLeadForAssignOrTransferOutput 
					} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
//import { TransferLeadModalComponent } from './transfer-lead.component';
import { DashboardCustomizationConst } from '@app/shared/common/customizable-dashboard/DashboardCustomizationConsts';


@Component({
    selector: 'userDashboard',
    templateUrl: './user-dashboard.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class UserDashboardComponent extends AppComponentBase implements OnInit {

    userdashboard = DashboardCustomizationConst.dashboardNames.defaultTenantDashboard;
    //@ViewChild('transferLeadModal', { static: true }) transferLeadModal: TransferLeadModalComponent;
    userLead:GetLeadForDashboardDto[];
					
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,       
        private _leadsServiceProxy: LeadsServiceProxy, 
        private _UserDashboardServiceProxy: UserDashboardServiceProxy
    ) {
        super(injector);
    }

     ngOnInit(): void {
         this.show(this._activatedRoute.snapshot.queryParams['id']);
     }

    show(leadId?: number): void {        
            this._UserDashboardServiceProxy.getUserLeadForDashboard().subscribe(result => {
                this.userLead = result;
            });
    }

    reject(id):void{
        let status :GetLeadForChangeStatusOutput = new GetLeadForChangeStatusOutput();
        status.id = id;
        status.leadStatusID = 7;

        this.message.confirm('',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._leadsServiceProxy.changeStatus(status)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('LeadRejectedSuccessfully'));
                        });
                }
            }
        );
    }

    unHandled(id):void{
        let status :GetLeadForChangeStatusOutput = new GetLeadForChangeStatusOutput();
        status.id = id;
        status.leadStatusID = 2;

        this.message.confirm('',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._leadsServiceProxy.changeStatus(status)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('LeadStatusChangedSuccessfully'));
                        });
                }
            }
        );
    }

    transfer(lead:GetLeadForDashboardDto):void{
        
    }

    reloadPage(): void {
        //this.paginator.changePage(this.paginator.getPage());
        window.location.reload();
    }
}
