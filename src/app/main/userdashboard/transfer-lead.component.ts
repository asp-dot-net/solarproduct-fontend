// import { AfterViewChecked, Component, ElementRef, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
// import { AppConsts } from '@shared/AppConsts';
// import { AppComponentBase } from '@shared/common/app-component-base';
// import { CreateOrUpdateUserInput, OrganizationUnitDto, PasswordComplexitySetting, ProfileServiceProxy, UserEditDto, UserRoleDto, UserServiceProxy, GetUserForEditOutput } from '@shared/service-proxies/service-proxies';
// import { ModalDirective } from 'ngx-bootstrap/modal';
// import * as _ from 'lodash';
// import { finalize } from 'rxjs/operators';

// @Component({
//     selector: 'transferLeadModal',
//     templateUrl: './transfer-lead.component.html'
// })
// export class TransferLeadModalComponent extends AppComponentBase {

//     @ViewChild('transferLeadModal', { static: true }) modal: ModalDirective;

//     @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

//     active = false;
//     saving = false;
//     canChangeUserName = true;
//     isTwoFactorEnabled: boolean = this.setting.getBoolean('Abp.Zero.UserManagement.TwoFactorLogin.IsEnabled');
//     isLockoutEnabled: boolean = this.setting.getBoolean('Abp.Zero.UserManagement.UserLockOut.IsEnabled');
//     passwordComplexitySetting: PasswordComplexitySetting = new PasswordComplexitySetting();

//     user: UserEditDto = new UserEditDto();
//     roles: UserRoleDto[];
//     sendActivationEmail = true;
//     setRandomPassword = true;
//     passwordComplexityInfo = '';
//     profilePicture: string;

//     allOrganizationUnits: OrganizationUnitDto[];
//     memberedOrganizationUnits: string[];
//     userPasswordRepeat = '';

//     constructor(
//         injector: Injector,
//         private _userService: UserServiceProxy,
//         private _profileService: ProfileServiceProxy
//     ) {
//         super(injector);
//     }

//     onShown(): void {
        
//     }

//     save(): void {
        
//     }

//     close(): void {
//         this.active = false;
//         this.userPasswordRepeat = '';
//         this.modal.hide();
//     }
// }
