import { Component, Injector, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DashboardCustomizationConst } from '@app/shared/common/customizable-dashboard/DashboardCustomizationConsts';
import { Title } from '@angular/platform-browser';
import { DatePipe } from '@angular/common';
import { CommonLookupServiceProxy, DashboardSalesServiceProxy, GetHeaderValue, GetLeadStatusDetailsDto, LeadsServiceProxy, OrganizationUnitDto, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { assign, result } from 'lodash';
import * as moment from 'moment';
import { start } from 'repl';
import { ViewSalesModalComponent } from './view-sales.component';
import { MessageService } from 'abp-ng2-module';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './dashboard-sales.component.html',
    styleUrls: ['./dashboard-sales.component.less'],
    encapsulation: ViewEncapsulation.None 
})

export class DashboardSalesComponent extends AppComponentBase implements OnInit {
    dashboardName = DashboardCustomizationConst.dashboardNames.defaultTenantDashboard;

    pipe = new DatePipe('en-US');
    today = new Date();
    date = this.pipe.transform(new Date(), 'MMM dd');
    StartDate: moment.Moment = moment(this.today);
    EndDate: moment.Moment = moment(this.today);
    
    @ViewChild('viewSalesModal', { static: true }) viewSalesModal: ViewSalesModalComponent;

    constructor(
        injector: Injector,
        private titleService: Title,
        private _commonLookupService : CommonLookupServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _dashboardSalesServiceProxy : DashboardSalesServiceProxy
        ,private _leadServiceProxy : LeadsServiceProxy) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Dashboard Sales");
        this.today = new Date();
        this.StartDate = moment(this.today);
        this.EndDate = moment(this.today);
    }
    
    showchild: boolean = true;
    shouldShow: boolean = false;
    addTaskShow: boolean = false;
    shouldShowDatefilter: boolean = false;
    shouldShowCustom: boolean = false;

    users : any[];
    userId : number;
    usersLength = 0;
    datetype = "Today";
    Assigned = 0;
    Cold = 0;
    Warm = 0;
    Hot = 0;
    UnHandled = 0;
    Canceled = 0;
    TotalLeadData = 0;
    TotalJobs = 0;
    Planned = 0;
    active = 0;
    JobBook = 0;
    Installed = 0;
    Can_Hold = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnitlength = 0;
    organizationUnit = 0;
    NSW = 0;
    VIC = 0;
    SA = 0;
    QLD = 0;
    Missed = 0;
    Inbound = 0;
    Outbound = 0;
    NotAnswerd  = 0;
    headerDto : GetHeaderValue = new GetHeaderValue();
    rank ="";
    role = "";
    LeadStatusList : GetLeadStatusDetailsDto[] ;
    MessageBoardList: string[];
    totalStatewiseSale = 0;
    totalCallHitory = 0;

    ngOnInit(): void {
        this.StartDate = moment(this.today);
        this.EndDate = moment(this.today);
        this._leadServiceProxy.getCurrentUserRole().subscribe(r =>{
            this.role = r;

            this._commonLookupService.getOrganizationUnit().subscribe(output => {
                this.allOrganizationUnits = output;
                this.organizationUnit = this.allOrganizationUnits[0].id;
                this.organizationUnitlength = this.allOrganizationUnits.length;
                    
                this.bindusers();
                this.getMessageBoard();
            });
        });
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open';
        log.section = 'Dashboard Sales';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {
        });
    }

    bindusers():void{
        debugger;
        
        this._dashboardSalesServiceProxy.getUsers(this.organizationUnit).subscribe(result => {
            this.users = result;
            this.usersLength = result.length;
            if(result.length > 1){
             this.userId = 0;
            }
            else{
            this.userId = result[0].id;
             }
             this.getData();
            

         });
    }

    getLeadData() :void{
        this._dashboardSalesServiceProxy.getLeadOverView(this.userId,this.StartDate,this.EndDate,this.organizationUnit).subscribe(result=>{
            this.Assigned = result.assigned;
            this.Cold = result.cold;
            this.Warm = result.warm;
            this.Hot = result.hot;
            this.UnHandled = result.unHandled;
            this.Canceled = result.canceled;
            this.TotalLeadData = result.assigned +result.cold +result.warm + result.unHandled + result.canceled;
            this.shouldShowDatefilter = false;
        })
    }

    getSalesData() :void{
        this._dashboardSalesServiceProxy.getSalesOverView(this.userId,this.StartDate,this.EndDate,this.organizationUnit).subscribe(result=>{
            this.TotalJobs = result.totalJobs;
            this.Planned = result.planned;
            this.active = result.active;
            this.JobBook = result.jobBook;
            this.Installed = result.installed;
            this.Can_Hold = result.can_Hold;
        })
    }

    getStateWiseSales() :void{
        this._dashboardSalesServiceProxy.getStateWiseSales(this.userId,this.StartDate,this.EndDate,this.organizationUnit).subscribe(result=>{
            this.NSW = result.nsw;
            this.QLD = result.qld;
            this.SA = result.sa;
            this.VIC = result.vic;
            this.totalStatewiseSale = result.nsw + result.qld + result.sa + result.vic;
        })
    }

    getCallHistory() :void{
        this._dashboardSalesServiceProxy.getCallHistory(this.userId,this.StartDate,this.EndDate,this.organizationUnit).subscribe(result=>{
            this.Inbound = result.inbound;
            this.Outbound = result.outbound;
            this.NotAnswerd = result.notAnswerd;
            this.Missed = result.missed;
            this.totalCallHitory = result.inbound + result.outbound + result.notAnswerd + result.missed;
        })
    }

    getHeader() : void{
        debugger;

        this._dashboardSalesServiceProxy.getHeaderValue(this.userId,this.StartDate,this.EndDate,this.organizationUnit).subscribe(result => {
            this.headerDto = result;
            if(result.name != ""){
                if(result.rank == 11 || result.rank == 12 || result.rank == 13){
                    this.rank =result.rank.toString()+"th"
                }
                else if(result.rank % 10 == 1){
                    this.rank  =result.rank.toString()+"st"
                }
                else if(result.rank % 10 == 2){
                    this.rank =result.rank.toString()+"nd"
                }
                else if(result.rank % 10 == 3){
                    this.rank =result.rank.toString()+"rd"
                }
                else{
                    this.rank =result.rank.toString()+"th"
                }
            }
            

        })
    }

    getLeadstatusDetail() :void{
        this._dashboardSalesServiceProxy.getLeadStatusDetail(this.userId,this.StartDate,this.EndDate,this.organizationUnit).subscribe(result =>{
            this.LeadStatusList =result;
        })
    }

    getMessageBoard() : void{
        this._dashboardSalesServiceProxy.getMessageBoardDataList(this.organizationUnit).subscribe(r => {
            this.MessageBoardList = r;
        })
    }

    clear() : void
    {
        this.StartDate = moment(this.today);
        this.EndDate = moment(this.today);
        this.getData();

    }

    onClick(e) {
        debugger;
        this.datetype = e.target.innerHTML;
        if(e.target.innerHTML == "Today"){
            this.StartDate = moment(this.today);
            this.EndDate = moment(this.today);
            this.date = this.pipe.transform(new Date(), 'MMM dd');
        }

        if(e.target.innerHTML == "Yesterday"){
           
            var Yesterday = new Date();
            Yesterday.setDate(new Date().getDate() - 1)
            this.StartDate = moment(Yesterday);
            this.EndDate = moment(Yesterday);
            this.date = this.pipe.transform(Yesterday, 'MMM dd');
        }

        if(e.target.innerHTML == "Last 7 Days"){
            var mindate = new Date();
            mindate.setDate(new Date().getDate() - 7)

            this.StartDate = moment(mindate);
            this.EndDate = moment(this.today);

            this.date = this.pipe.transform(mindate, 'MMM dd') + '-' + this.pipe.transform(new Date(), 'MMM dd');
        }
        if(e.target.innerHTML == "Last 30 Days"){
            var mindate = new Date();
            mindate.setDate(new Date().getDate() - 30)

            this.StartDate = moment(mindate);
            this.EndDate = moment(this.today);

            this.date = this.pipe.transform(mindate, 'MMM dd') + '-' + this.pipe.transform(new Date(), 'MMM dd');
        }
        if(e.target.innerHTML == "This Month"){
            var date = new Date();
            var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
            var lastDay = new Date(date.getFullYear(), date.getMonth()+1, 0);

            this.StartDate = moment(firstDay);
            this.EndDate = moment(lastDay);

            this.date = this.pipe.transform(firstDay, 'MMM dd') + '-' + this.pipe.transform(lastDay, 'MMM dd');
        }
        if(e.target.innerHTML == "Last Month"){
            var date = new Date();
            var firstDay = new Date(date.getFullYear(), date.getMonth()-1, 1);
            var lastDay = new Date(date.getFullYear(), date.getMonth(), 0);

            this.StartDate = moment(firstDay);
            this.EndDate = moment(lastDay);

            this.date = this.pipe.transform(firstDay, 'MMM dd') + '-' + this.pipe.transform(lastDay, 'MMM dd');
        }
        if(e.target.innerHTML == "Custom Range"){
            this.shouldShowCustom =true;
            this.date = "";
        }
        else{
            this.shouldShowCustom =false;
            this.getSearchedData();
            
        }
            
    }

        getData() : void{
            this.getHeader();
            this.getLeadData();
            this.getSalesData();
            this.getStateWiseSales();
            this.getCallHistory();
            this.getLeadstatusDetail();
            this.shouldShow = false;
        }

    getSearchedData() : void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Search By Filter';
        log.section = 'Dashboard Sales';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        this.getData();

        });
    }

    viewSale() :void{
        this.viewSalesModal.show(this.userId,this.StartDate,this.EndDate,this.organizationUnit);
    }
}
