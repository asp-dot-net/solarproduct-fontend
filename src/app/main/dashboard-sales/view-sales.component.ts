import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { DashboardSalesServiceProxy, GetLeadSalesListDto, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { result } from 'lodash';

@Component({
    selector: 'viewSalesModal',
    templateUrl: './view-sales.component.html'
})
export class ViewSalesModalComponent extends AppComponentBase {

    @ViewChild('viewSalesModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    list : GetLeadSalesListDto[]
    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _dashboardSalesServiceProxy : DashboardSalesServiceProxy
 
    ) {
        super(injector);
       
    }

    show(userId :number,Sdate : moment.Moment , edate : moment.Moment, organizationUnit : number): void {
        this._dashboardSalesServiceProxy.getLeadSalesList(userId,Sdate,edate,organizationUnit).subscribe(result => {
            debugger;
            this.list = result;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId =79;
            log.actionNote ='View Sales';
            log.section = 'Dashboard Sales';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
            });
        })
    }


    close(): void {
        this.modal.hide();
    }
}
