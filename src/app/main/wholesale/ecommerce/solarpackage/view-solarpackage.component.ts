import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { CreateOrEditEcommerceSolarPackageDto, CreateOrEditWholeSaleLeadDocumentTypeDto, EcommerceSolarPackageServiceProxy, SpecialOfferDto, SpecialOfferServiceProxy, WholeSaleLeadDocumentTypeServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { EcommerceSolarPackagePackageComponent } from './solarpackage.component';
import { AppConsts } from '@shared/AppConsts';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'ViewEcommerceSolarPackageModal',
    templateUrl: './view-solarpackage.component.html'
})
export class ViewEcommerceSolarPackageModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    EcommerceSolarPackage: CreateOrEditEcommerceSolarPackageDto = new CreateOrEditEcommerceSolarPackageDto();
    imgurl ='';

    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _ecommerceSolarPackageServiceProxy : EcommerceSolarPackageServiceProxy,
    ) {
        super(injector);
    }

    show(WholeSaleLeadDocumenttypeId?: number): void {

            this._ecommerceSolarPackageServiceProxy.getEcommerceSolarPackageForView(WholeSaleLeadDocumenttypeId).subscribe(result => {
                this.EcommerceSolarPackage = result.ecommerceSolarPackage;
                 this.imgurl = AppConsts.docUrl  + this.EcommerceSolarPackage.banner;
                this.active = true;
                this.modal.show();

        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Open ECommerce Solar Package View : ' + this.EcommerceSolarPackage.name;
        log.section = 'ECommerce Solar Package';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {            
         });
            });
        
    }


   
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
