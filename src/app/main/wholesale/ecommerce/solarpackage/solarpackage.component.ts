import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { Title } from '@angular/platform-browser';
import {
    EcommerceSolarPackageServiceProxy, 
    EcommerceSolarPackageDto,
    CreateOrEditEcommerceSolarPackageDto
} from '@shared/service-proxies/service-proxies';
import { CreateOrEditEcommerceSolarPackageModalComponent } from './create-or-edit-solarpackage-modal.component';
import { ViewEcommerceSolarPackageModalComponent } from './view-solarpackage.component';

@Component({
    templateUrl: './solarpackage.component.html',
    styleUrls: ['./solarpackage.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class EcommerceSolarPackagePackageComponent extends AppComponentBase {

    @ViewChild('createOrEditEcommerceSolarPackageModal', { static: true }) createOrEditEcommerceSolarPackageModal: CreateOrEditEcommerceSolarPackageModalComponent;
    @ViewChild('ViewEcommerceSolarPackageModal', { static: true }) ViewEcommerceSolarPackageModal: ViewEcommerceSolarPackageModalComponent;

    show: boolean = true;
    showchild: boolean = true;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    
    advancedFiltersAreShown = false;
    filterText = '';
    firstrowcount = 0;
    last = 0;
   
    // productPackage: CreateOrEditEcommerceSolarPackageDto = new CreateOrEditEcommerceSolarPackageDto();
    public screenHeight: any;  
    testHeight = 250;
    constructor(
        injector: Injector,
        private _ecommerceSolarPackageServiceProxy: EcommerceSolarPackageServiceProxy,
        private titleService: Title,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Ecommerce SolarPackage");
    }
    searchLog() : void {
        
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'ECommerce Solar Package';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
       
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open ECommerce Solar Package';
        log.section = 'ECommerce Solar Package';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
    }
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }
    getEcommerceSolarPackage(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._ecommerceSolarPackageServiceProxy.getAll(
            this.filterText,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();

        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    deleteEcommerceSolarPackage(productPackage: EcommerceSolarPackageDto): void {
        this.message.confirm(
            this.l('AreYouSureWanttoDelete'),
            this.l('Delete'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._ecommerceSolarPackageServiceProxy.delete(productPackage.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete ECommerce Solar Package: ' + productPackage.name;
                            log.section = 'ECommerce Solar Package';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });

                        });
                }
            }
        );
    }
}
