import { Component, ViewChild, Injector, Output, EventEmitter, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { EcommerceSolarPackageServiceProxy, CreateOrEditEcommerceSolarPackageDto, CreateOrEditEcommerceSolarPackagesItemDto, CommonLookupServiceProxy, CommonLookupDto, LeadStateLookupTableDto, OrganizationUnitDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { AppConsts } from '@shared/AppConsts';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { FileUploader, FileUploaderOptions, FileItem } from 'ng2-file-upload';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'createOrEditEcommerceSolarPackageModal',
    templateUrl: './create-or-edit-solarpackage-modal.component.html'
})
export class CreateOrEditEcommerceSolarPackageModalComponent extends AppComponentBase implements OnInit {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    ecommerceSolarPackage: CreateOrEditEcommerceSolarPackageDto = new CreateOrEditEcommerceSolarPackageDto();
    allOrganizationUnits: OrganizationUnitDto[];
    allStates: LeadStateLookupTableDto[];

    productList: any[];

    EcommerceProductItems: any[];
    productItems: any[];
    productTypes: CommonLookupDto[];
    productItemSuggestions: any[];
    public uploader: FileUploader;
    public maxfileBytesUserFriendlyValue = 5;
    private _uploaderOptions: FileUploaderOptions = {};
    selectedFile : any;

    filetoken ='';
    constructor(
        injector: Injector,
        private _ecommerceSolarPackageServiceProxy: EcommerceSolarPackageServiceProxy,
        private _commonLookupService : CommonLookupServiceProxy,
        private _tokenService: TokenService,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        // private _jobServiceProxy: JobsServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void { 
        this._commonLookupService.getAllProductTypeForTableDropdown().subscribe(result => {
            this.productTypes = result;
        });
        // this._commonLookupService.getAllEcommerceSolarItemForTableDropdownForEdit().subscribe(result => {
        //     this.productItems = result;
        // });
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;           
        });

    }

    show(ecommerceSolarPackageId?: number): void {
        this.filetoken = "";
        if (!ecommerceSolarPackageId) {
            this.ecommerceSolarPackage = new CreateOrEditEcommerceSolarPackageDto();
            this.ecommerceSolarPackage.id = ecommerceSolarPackageId;

            this.EcommerceProductItems = [];
            var jobEcommerceSolarCreateOrEdit = { id: 0, productItemId: 0, productItemName: "", quantity: 0, model: "", productItems: [], productTypeId: 0 }
            jobEcommerceSolarCreateOrEdit.productItems = this.productItems;
            this.EcommerceProductItems.push(jobEcommerceSolarCreateOrEdit);

            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New ECommerce Solar Package';
            log.section = 'ECommerce Solar Package';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._ecommerceSolarPackageServiceProxy.getEcommerceSolarPackageForEdit(ecommerceSolarPackageId).subscribe(result => {
                this.ecommerceSolarPackage = result.ecommerceSolarPackage;
                this.EcommerceProductItems = [];
                this.ecommerceSolarPackage.ecommerceSolarPackageItem.map((item) => {
                    var ecommerceSolarPackageCreateOrEdit = { id: 0, productItemId: 0, productItemName: "", quantity: 0, model: "", productItems: [], productTypeId: 0 }
                    ecommerceSolarPackageCreateOrEdit.productItemId = item.productItemId;
                    ecommerceSolarPackageCreateOrEdit.productTypeId = item.productTypeId;
                    ecommerceSolarPackageCreateOrEdit.productItemName = item.productItem;

                    ecommerceSolarPackageCreateOrEdit.quantity = item.quantity;
                    ecommerceSolarPackageCreateOrEdit.id = item.id;
                    ecommerceSolarPackageCreateOrEdit.model = item.model;
                    this.EcommerceProductItems.push(ecommerceSolarPackageCreateOrEdit);
                    
                    // let pitemname = this.productItems.find(x => x.id == item.productItemId);
                    // ecommerceSolarPackageCreateOrEdit.productItemName = pitemname.displayName;
                    // this.JobEcommerceSolars.push(ecommerceSolarPackageCreateOrEdit);
                })

                if (this.ecommerceSolarPackage.ecommerceSolarPackageItem.length == 0) {
                    var ecommerceSolarPackageCreateOrEdit = { id: 0, productItemId: 0, productItemName: "", quantity: 0, model: "", productItems: [], productTypeId: 0 }
                    ecommerceSolarPackageCreateOrEdit.productItems = this.productItems;
                    this.EcommerceProductItems.push(ecommerceSolarPackageCreateOrEdit);
                }

                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit ECommerce Solar Package : ' + this.ecommerceSolarPackage.name;
                log.section = 'ECommerce Solar Package';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });

            });
        }
        this.initFileUploader();

    }

    onShown(): void {
       document.getElementById('Name').focus();
    }
   
    save(): void {
        this.saving = true;
        this.ecommerceSolarPackage.fileToken = this.filetoken ?this.filetoken : ""; 

        this.ecommerceSolarPackage.banner = this.ecommerceSolarPackage.banner ? this.ecommerceSolarPackage.banner :"";
 
             if(!this.ecommerceSolarPackage.fileToken && this.ecommerceSolarPackage.banner.trim() == ""){
                 if( this.ecommerceSolarPackage.fileToken.trim() == ""){
                     this.notify.warn("Select Image");
                     this.saving = false;
                     return;
                 }
             }
        this.ecommerceSolarPackage.ecommerceSolarPackageItem = [];
        this.EcommerceProductItems.map((item) => {
            let createOrEditEcommerceSolarPackageItemDto = new CreateOrEditEcommerceSolarPackagesItemDto();
            createOrEditEcommerceSolarPackageItemDto.productItemId = item.productItemId;
            createOrEditEcommerceSolarPackageItemDto.quantity = item.quantity;
            createOrEditEcommerceSolarPackageItemDto.id = item.id;
            if (item.productTypeId != undefined && item.productItemId != undefined && item.quantity != undefined && item.model != undefined) {
                this.ecommerceSolarPackage.ecommerceSolarPackageItem.push(createOrEditEcommerceSolarPackageItemDto);
            }
        });

        this._ecommerceSolarPackageServiceProxy.createOrEdit(this.ecommerceSolarPackage).pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.ecommerceSolarPackage.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='ECommerce Solar Package Updated : '+ this.ecommerceSolarPackage.name;
                    log.section = 'ECommerce Solar Package';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='ECommerce Solar Package Created : '+ this.ecommerceSolarPackage.name;
                    log.section = 'ECommerce Solar Package';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
            },
            error => {
                this.saving = false;
            }
        );
        
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
    // changeEcommerceSolarType(i) : void {
    //     this.installationItemList[i].productItemId = "";
    //     this.installationItemList[i].productItemName = "";
    //     this.installationItemList[i].size = 0;
    //     this.installationItemList[i].pricePerWatt = 0;
    //     this.installationItemList[i].unitPrice = 0;
    // }
    filterProductItem(item, i): void {
        //this.JobEcommerceSolars[i].productItems = this.productItems.filter(x => x.productTypeId == item.productTypeId);
        this.EcommerceProductItems[i].productItemId = "";
        this.EcommerceProductItems[i].productItemName = "";
        this.EcommerceProductItems[i].modal = "";
        this.EcommerceProductItems[i].size = 0;
        this.EcommerceProductItems[i].pricePerWatt = 0;
        this.EcommerceProductItems[i].unitPrice = 0;
    }

    filterProductIteams(event, i): void {
        let Id = this.EcommerceProductItems[i].productTypeId;

        this._ecommerceSolarPackageServiceProxy.getPicklistProductItemList(Id, event.query).subscribe(output => {
            //this.productItemSuggestions = output;
            debugger;
            var items =this.EcommerceProductItems.map(i=>i.productItemName+'');
            this.productItemSuggestions = output.filter(x=> !items.includes(x.productItem))
        });
    }

    getProductTypeId(i) {
        let pItem = this.productItems.find(x => x.displayName == this.EcommerceProductItems[i].productItemName)
        this.EcommerceProductItems[i].productItemId = pItem.id;
        this.EcommerceProductItems[i].model = pItem.model;
    }

    check(event) {
        if (event.target.value < 1) {
            event.target.value = '';
        }
    }

    removeEcommerceSolarPackageItem(JobEcommerceSolar): void {
        if (this.EcommerceProductItems.length == 1)
            return;
        
        if (this.EcommerceProductItems.indexOf(JobEcommerceSolar) === -1) {
            
        } else {
            this.EcommerceProductItems.splice(this.EcommerceProductItems.indexOf(JobEcommerceSolar), 1);
        }
    }

    addEcommerceSolarPackageItem(): void {
        var jobEcommerceSolarCreateOrEdit = { id: 0, productItemId: 0, productItemName: "", quantity: 0, model: "", productItems: [], productTypeId: 0 }
        this.EcommerceProductItems.push(jobEcommerceSolarCreateOrEdit);
    }

    selectEcommerceSolarItem(event, i) {
        debugger;
        this.EcommerceProductItems[i].productItemId = event.id;
        this.EcommerceProductItems[i].productItemName = event.productItem;
        this.EcommerceProductItems[i].size = event.size;
        this.EcommerceProductItems[i].model = event.productModel;
        //this.calculateUnitPrice(i);
    }

    initFileUploader(): void {
        this.uploader = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Profile/UploadFile' });
        this._uploaderOptions.autoUpload = false;
        this._uploaderOptions.authToken = 'Bearer ' + this._tokenService.getToken();
        this._uploaderOptions.removeAfterUpload = true;
        this.uploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

        this.uploader.onBuildItemForm = (fileItem: FileItem, form: any) => {
            form.append('FileType', fileItem.file.type);
            form.append('FileName', fileItem.file.name);
            form.append('FileToken', this.guid());
        };

        this.uploader.onSuccessItem = (item, response, status) => {
            const resp = <IAjaxResponse>JSON.parse(response);
            if (resp.success) {
                this.filetoken = resp.result.fileToken;
                //this.instfilename.push(resp.result.fileName);
            } else {
                this.message.error(resp.error.message);
            }
        };

        this.uploader.setOptions(this._uploaderOptions);
    }

    imgheight = 0;
    imgWidth = 0;

    fileChangeEvent(event: any): void {
        debugger;
        var url = URL.createObjectURL(event.target.files[0]);
        var img = new Image;

        img.onload = () =>{
            var w = img.width;
            var h = img.height;
            //alert(w + "X"+ h )
        //     if(w != 50 || h != 80){
        //         this.notify.warn("Select Image That Contain Size 1920 X 1000");
        //         this.selectedFile = null;
        //    }
        };
        img.src = url;

        this.uploader.clearQueue();
        this.uploader.addToQueue([<File>event.target.files[0]]);
        this.uploader.uploadAll();

    }
     

    guid(): string {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }

    downloadfile(file): void {
        let FileName = AppConsts.docUrl + file;

        window.open(FileName, "_blank");

    }
}
