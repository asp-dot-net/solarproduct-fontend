import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {  EcommerceProductTypeDto, EcommerceProductTypeServiceProxy} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { FileUploader} from 'ng2-file-upload';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
@Component({
    selector: 'CreateOrEditEcommerceProductTypeModal',
    templateUrl: './createoredit-ecommerceproducttype.component.html'
})
export class CreateOrEditEcommerceProductTypeModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    selectedFile : any;

    EcommerceProductType: EcommerceProductTypeDto = new EcommerceProductTypeDto();

    constructor(
        injector: Injector,
        private _eCommerceSliderServiceProxy: EcommerceProductTypeServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,

    ) {
        super(injector);
    }
    public uploader: FileUploader;
    public maxfileBytesUserFriendlyValue = 5;
    filetoken ='';

    show(EcommerceProductTypeId?: number): void {
        

        if (!EcommerceProductTypeId) {
            this.EcommerceProductType = new EcommerceProductTypeDto();
            this.EcommerceProductType.id = EcommerceProductTypeId;

            this.active = true;
            this.modal.show();
            
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Wholesale Product Type';
            log.section = 'Wholesale Product Type';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._eCommerceSliderServiceProxy.getLeadSourceForEdit(EcommerceProductTypeId).subscribe(result => {
                this.EcommerceProductType = result;
                this.active = true;
                this.modal.show();
                
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Wholesale Product Type : ' +this.EcommerceProductType.type;
                log.section = 'Wholesale Product Type';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
    }

    onShown(): void {
        
        //document.getElementById('Department_Name').focus();
    }

    save(): void {
        debugger;

          

        this._eCommerceSliderServiceProxy.createOrEdit(this.EcommerceProductType)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.filetoken ='';
                this.close();
                this.modalSave.emit(null);
                
                if(this.EcommerceProductType.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Wholesale Product Type Updated : '+ this.EcommerceProductType.type;
                    log.section = 'Wholesale Product Type';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Wholesale Product Type Created : '+ this.EcommerceProductType.type;
                    log.section = 'Wholesale Product Type';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
            });
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }


     

    
    


}
