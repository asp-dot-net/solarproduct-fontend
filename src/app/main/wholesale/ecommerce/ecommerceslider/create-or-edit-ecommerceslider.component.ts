import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize, map, take } from 'rxjs/operators';
import { CreateOrEditECommerceSliderDto, ECommerceSliderServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { AppConsts } from '@shared/AppConsts';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { FileUploader, FileItem, FileUploaderOptions } from 'ng2-file-upload';
import { Observable, fromEvent } from 'rxjs';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
@Component({
    selector: 'CreateOrEditECommerceSliderModal',
    templateUrl: './create-or-edit-ecommerceslider.component.html'
})
export class CreateOrEditECommerceSliderModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    selectedFile : any;

    ECommerceSlider: CreateOrEditECommerceSliderDto = new CreateOrEditECommerceSliderDto();

    constructor(
        injector: Injector,
        private _eCommerceSliderServiceProxy: ECommerceSliderServiceProxy,
        private _tokenService: TokenService,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,


    ) {
        super(injector);
    }
    public uploader: FileUploader;
    public maxfileBytesUserFriendlyValue = 5;
    private _uploaderOptions: FileUploaderOptions = {};
    filetoken ='';
    show(ECommerceSliderId?: number): void {

        if (!ECommerceSliderId) {
            this.ECommerceSlider = new CreateOrEditECommerceSliderDto();
            this.ECommerceSlider.id = ECommerceSliderId;

            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New ECommerceSlider';
            log.section = 'ECommerce Slider';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });

        } else {
            this._eCommerceSliderServiceProxy.getECommerceSliderForEdit(ECommerceSliderId).subscribe(result => {
                this.ECommerceSlider = result.createOrEditECommerceSlider;
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit ECommerceSlider: ' + this.ECommerceSlider.name;
                log.section = 'ECommerce Slider';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });

            });
        }
        this.initFileUploader();
    }

    onShown(): void {
        
        //document.getElementById('Department_Name').focus();
    }

    save(): void {
        debugger;
        this.saving = true;
        
       this.ECommerceSlider.img = this.filetoken ?this.filetoken : ""; 

       this.ECommerceSlider.imgPath = this.ECommerceSlider.imgPath ? this.ECommerceSlider.imgPath :"";

            if(!this.ECommerceSlider.img && this.ECommerceSlider.imgPath.trim() == ""){
                if( this.ECommerceSlider.img.trim() == ""){
                    this.notify.warn("Select Image");
                    this.saving = false;
                    return;
                }
            }
          

        this._eCommerceSliderServiceProxy.createOrEdit(this.ECommerceSlider)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.filetoken ='';
                this.close();
                this.modalSave.emit(null);
                
                if(this.ECommerceSlider.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='ECommerce Slider Updated : '+ this.ECommerceSlider.name;
                    log.section = 'ECommerce Slider';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='ECommerce Slider Created : '+ this.ECommerceSlider.name;
                    log.section = 'ECommerce Slider';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
            });
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }

    initFileUploader(): void {
        this.uploader = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Profile/UploadFile' });
        this._uploaderOptions.autoUpload = false;
        this._uploaderOptions.authToken = 'Bearer ' + this._tokenService.getToken();
        this._uploaderOptions.removeAfterUpload = true;
        this.uploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

        this.uploader.onBuildItemForm = (fileItem: FileItem, form: any) => {
            form.append('FileType', fileItem.file.type);
            form.append('FileName', fileItem.file.name);
            form.append('FileToken', this.guid());
        };

        this.uploader.onSuccessItem = (item, response, status) => {
            const resp = <IAjaxResponse>JSON.parse(response);
            if (resp.success) {
                this.filetoken = resp.result.fileToken;
                //this.instfilename.push(resp.result.fileName);
            } else {
                this.message.error(resp.error.message);
            }
        };

        this.uploader.setOptions(this._uploaderOptions);
    }

    imgheight = 0;
    imgWidth = 0;

    fileChangeEvent(event: any): void {
        debugger
        // // if (event.target.files[0].size > 5242880) { //5MB
        // //     this.message.warn(this.l('ProfilePicture_Warn_SizeLimit', this.maxfileBytesUserFriendlyValue));
        // //     return;
        // // }
        // let image = <ImageBitmap>event.target.file[0];
        //    if(image.width != 1920 || image.height != 1000){
        //         this.notify.warn("Select Image That Contain Size 1920 X 1000");
        //         this.selectedFile = null;
        //         return;
        //    }
        // // let width = event.target.files[0].width;
        // // let height = event.target.files[0].height;
        // let mapLoadedImage = (event): ISize => {
        //     return {
        //         width: event.target.width,
        //         height: event.target.height
        //     };
        // }

        //  var image = new Image();
        // // image = event.target.files[0];
        // let $loadedImg = fromEvent(image, "load").pipe(take(1), map(mapLoadedImage));

        var url = URL.createObjectURL(event.target.files[0]);
        var img = new Image;

        img.onload = () =>{
            var w = img.width;
            var h = img.height;
            //alert(w + "X"+ h )
            if(w != 2032 || h != 665){
                this.notify.warn("Select Image That Contain Size 2032 X 665");
                this.selectedFile = null;
           }
        };
        img.src = url;

        this.uploader.clearQueue();
        this.uploader.addToQueue([<File>event.target.files[0]]);
        this.uploader.uploadAll();

    }
     

    guid(): string {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }

    downloadfile(file): void {
        let FileName = AppConsts.oldDocUrl + file;

        window.open(FileName, "_blank");

    }
    


}
