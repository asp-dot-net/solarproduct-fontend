import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import {  ECommerceSliderDto, ECommerceSliderServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AppConsts } from '@shared/AppConsts';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
@Component({
    selector: 'viewEcommerceSliderModal',
    templateUrl: './view-ecommerceslider.component.html'
})
export class ViewEcommerceSliderModalComponent extends AppComponentBase {

    @ViewChild('viewEcommerceSlider', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    ECommerceSlider: ECommerceSliderDto = new ECommerceSliderDto();
    imgurl ='';

    constructor(
        injector: Injector,
        private _eCommerceSliderServiceProxy: ECommerceSliderServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,

    ) {
        super(injector);
    }

    show(ECommerceSliderId: number): void {
        debugger;
        this.active = true;
        this._eCommerceSliderServiceProxy.getECommerceSliderForView(ECommerceSliderId).subscribe(result => {
            this.ECommerceSlider = result.eCommerceSliders;
            this.imgurl = AppConsts.docUrl  + this.ECommerceSlider.imgPath;
            this.active = true;
            this.modal.show();

        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Opened ECommerce Slider View : ' + this.ECommerceSlider.name;
        log.section = 'ECommerce Slider';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {            
         });
        });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
