import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize, map, take } from 'rxjs/operators';
import {  BrandingPartnerDto, BrandingPartnerServiceProxy, CommonLookupDto, CommonLookupServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { AppConsts } from '@shared/AppConsts';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { FileUploader, FileItem, FileUploaderOptions } from 'ng2-file-upload';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'CreateOrEditBrandingPartnerModal',
    templateUrl: './create-or-edit-brandingpartner.component.html'
})
export class CreateOrEditBrandingPartnerModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    selectedFile : any;

    BrandingPartner: BrandingPartnerDto = new BrandingPartnerDto();

    constructor(
        injector: Injector,
        private _eCommerceSliderServiceProxy: BrandingPartnerServiceProxy,
        private _tokenService: TokenService,
        private _CommonLookupServiceProxy : CommonLookupServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,

    ) {
        super(injector);
    }
    public uploader: FileUploader;
    public maxfileBytesUserFriendlyValue = 5;
    private _uploaderOptions: FileUploaderOptions = {};
    filetoken ='';
    productTypes : CommonLookupDto[] ;
    show(BrandingPartnerId?: number): void {
        this._CommonLookupServiceProxy.getAllProductTypeForTableDropdown().subscribe(result => {
            this.productTypes =result;
        });

        if (!BrandingPartnerId) {
            this.BrandingPartner = new BrandingPartnerDto();
            this.BrandingPartner.id = BrandingPartnerId;

            this.active = true;
            this.modal.show();
            
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New ECommerce Branding Partner';
            log.section = 'ECommerce Branding Partner';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._eCommerceSliderServiceProxy.getBusinessForEdit(BrandingPartnerId).subscribe(result => {
                this.BrandingPartner = result;
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit ECommerce Branding Partner : ' + this.BrandingPartner.brandName;
                log.section = 'ECommerce Branding Partner';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        this.initFileUploader();
    }

    onShown(): void {
        
        //document.getElementById('Department_Name').focus();
    }

    save(): void {
        debugger;
        this.saving = true;
        
       this.BrandingPartner.fileToken = this.filetoken ?this.filetoken : ""; 

       this.BrandingPartner.logo = this.BrandingPartner.logo ? this.BrandingPartner.logo :"";

            if(!this.BrandingPartner.fileToken && this.BrandingPartner.logo.trim() == ""){
                if( this.BrandingPartner.fileToken.trim() == ""){
                    this.notify.warn("Select Image");
                    this.saving = false;
                    return;
                }
            }
          

        this._eCommerceSliderServiceProxy.createOrEdit(this.BrandingPartner)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.filetoken ='';
                this.close();
                this.modalSave.emit(null);
                if(this.BrandingPartner.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='ECommerce Branding Partner Updated : '+ this.BrandingPartner.brandName;
                    log.section = 'ECommerce Branding Partner';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='ECommerce Branding Partner Created : '+ this.BrandingPartner.brandName;
                    log.section = 'ECommerce Branding Partner';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
            });
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }

    initFileUploader(): void {
        this.uploader = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Profile/UploadFile' });
        this._uploaderOptions.autoUpload = false;
        this._uploaderOptions.authToken = 'Bearer ' + this._tokenService.getToken();
        this._uploaderOptions.removeAfterUpload = true;
        this.uploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

        this.uploader.onBuildItemForm = (fileItem: FileItem, form: any) => {
            form.append('FileType', fileItem.file.type);
            form.append('FileName', fileItem.file.name);
            form.append('FileToken', this.guid());
        };

        this.uploader.onSuccessItem = (item, response, status) => {
            const resp = <IAjaxResponse>JSON.parse(response);
            if (resp.success) {
                this.filetoken = resp.result.fileToken;
                //this.instfilename.push(resp.result.fileName);
            } else {
                this.message.error(resp.error.message);
            }
        };

        this.uploader.setOptions(this._uploaderOptions);
    }

    imgheight = 0;
    imgWidth = 0;

    fileChangeEvent(event: any): void {
        debugger;
        var url = URL.createObjectURL(event.target.files[0]);
        var img = new Image;

        img.onload = () =>{
            var w = img.width;
            var h = img.height;
            //alert(w + "X"+ h )
        //     if(w != 50 || h != 80){
        //         this.notify.warn("Select Image That Contain Size 1920 X 1000");
        //         this.selectedFile = null;
        //    }
        };
        img.src = url;

        this.uploader.clearQueue();
        this.uploader.addToQueue([<File>event.target.files[0]]);
        this.uploader.uploadAll();

    }
     

    guid(): string {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }

    downloadfile(file): void {
        debugger;
        let FileName = AppConsts.docUrl + file;

        window.open(FileName, "_blank");

    }
    


}
