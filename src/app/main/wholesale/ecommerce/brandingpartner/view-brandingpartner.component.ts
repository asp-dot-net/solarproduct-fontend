import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import {  BrandingPartnerDto, BrandingPartnerServiceProxy, ECommerceSliderDto, ECommerceSliderServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AppConsts } from '@shared/AppConsts';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
@Component({
    selector: 'viewBrandingPartnerModal',
    templateUrl: './view-brandingpartner.component.html'
})
export class ViewBrandingPartnerModalComponent extends AppComponentBase {

    @ViewChild('viewBrandingPartner', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    BrandingPartner: BrandingPartnerDto = new BrandingPartnerDto();
    imgurl ='';

    constructor(
        injector: Injector,
        private _brandingPartnerServiceProxy: BrandingPartnerServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }

    show(BrandingPartnerId: number): void {
        debugger;
        this.active = true;
        this._brandingPartnerServiceProxy.getBusinessForEdit(BrandingPartnerId).subscribe(result => {
            this.BrandingPartner = result;
            this.imgurl = AppConsts.docUrl  + this.BrandingPartner.logo;
            this.active = true;
            this.modal.show();
            
let log = new UserActivityLogDto();
log.actionId = 79;
log.actionNote ='Opened ECommerce Branding Partner View : ' + this.BrandingPartner.brandName;
log.section = 'ECommerce Branding Partner';
this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
.subscribe(() => {            
 });
        });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
