import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { CommonLookupServiceProxy, ECommerceUserRegisterRequestServiceProxy, EcommerceUserRegisterRequestDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { result } from 'lodash';
import { FileItem, FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { AppConsts } from '@shared/AppConsts';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';

@Component({
    selector: 'ApproveUserReuestModal',
    templateUrl: './approveuserrequest-modal.component.html'
})
export class ApproveUserReuestModalComponent extends AppComponentBase {

    // @ViewChild('approveuserreuestmodal', { static: true }) modal: ModalDirective;
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    organizationUnitId: any;
    organizationUnit: any;

    active = false;
    saving = false;

    userRegisterRequest: EcommerceUserRegisterRequestDto = new EcommerceUserRegisterRequestDto();

    constructor(
        injector: Injector,
        private _tokenService: TokenService,
        private _commonLookupServiceProxy: CommonLookupServiceProxy,
        private _ecommerceUserRegisterRequestServiceProxy: ECommerceUserRegisterRequestServiceProxy,
       
    ) {
        super(injector);
    }

    show(request: EcommerceUserRegisterRequestDto): void {
        this.showMainSpinner();
        this.userRegisterRequest = new EcommerceUserRegisterRequestDto();
        this.userRegisterRequest = request;

        this._commonLookupServiceProxy.getOrganizationUnit().subscribe(result => {
            this.organizationUnit = result;
            this.hideMainSpinner();
            this.modal.show();
        }, err => { this.hideMainSpinner(); });
    }

    save(): void {
        this.showMainSpinner();
        this._ecommerceUserRegisterRequestServiceProxy.approveUserRequest(this.userRegisterRequest.id, this.organizationUnitId).subscribe(result => {
            if(result == "Success")
            {
                this.active = false;
                this.hideMainSpinner(); 
                this.modal.hide();
                this.notify.info(this.l('SavedSuccessfully'));
            }
            else 
            {
                this.hideMainSpinner();
                this.notify.info("Email Address Already Exists");
            }
        }, err => { this.hideMainSpinner(); });
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }
    
}
