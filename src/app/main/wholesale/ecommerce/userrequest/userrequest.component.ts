import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { WholeSaleLeadDocumentTypeServiceProxy, WholeSaleLeadDocumentTypeDto, SpecialOfferServiceProxy, EcommerceUserRegisterRequestDto, ECommerceUserRegisterRequestServiceProxy, EntityDto } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';

import { Title } from '@angular/platform-browser';
import { ApproveUserReuestModalComponent } from './approveuserrequest-modal.component';
import { CreateOrEditProductModalComponent } from '../add-product/create-or-edit-product-modal.component';

@Component({
    templateUrl: './userrequest.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class UserRequestComponent extends AppComponentBase {

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('ApproveUserReuestModal', { static: true }) ApproveUserReuestModal: ApproveUserReuestModalComponent;

    advancedFiltersAreShown = false;
    filterText = '';
    firstrowcount = 0;
    last = 0;
    public screenHeight: any;  
    testHeight = 250;
    IsApprove = 'All';

    constructor(
        injector: Injector,
        private _ECommerceUserRegisterRequestServiceProxy : ECommerceUserRegisterRequestServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private titleService: Title
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  UserRequest");
    }
    searchLog() : void {
        
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'ECommerce User Request';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
       
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open ECommerce User Request';
        log.section = 'ECommerce User Request';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
    }
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }
    getUserRequests(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._ECommerceUserRegisterRequestServiceProxy.getAll(
            this.filterText,
            this.IsApprove,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }
    
    deleteUserRequest(EcommerceUserRegisterRequestDto :EcommerceUserRegisterRequestDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._ECommerceUserRegisterRequestServiceProxy.delete(EcommerceUserRegisterRequestDto.id).subscribe(() => {
                        this.notify.info(this.l('DeletedSuccessfully'));
                        
                        let log = new UserActivityLogDto();
                        log.actionId = 83;
                        log.actionNote ='Delete ECommerce User Request: ' + EcommerceUserRegisterRequestDto.userName;
                        log.section = 'ECommerce User Request';
                        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                            .subscribe(() => {
                        });

                        this.getUserRequests();
                    });
                }
            }
        );
    }
}
