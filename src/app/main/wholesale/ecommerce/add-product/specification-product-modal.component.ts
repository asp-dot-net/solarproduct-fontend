import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { CommonLookupServiceProxy, CreateOrEditWholeSaleLeadDocumentTypeDto, ECommerceProductItemsServiceProxy, EcommerceProductItemSpecificationDto, WholeSaleLeadDocumentTypeServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { AppConsts } from '@shared/AppConsts';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { FileUploader, FileItem, FileUploaderOptions } from 'ng2-file-upload';

@Component({
    selector: 'ProductSpecificationModal',
    templateUrl: './specification-product-modal.component.html'
})
export class ProductSpecificationModalComponent extends AppComponentBase {

    @ViewChild('productSpecificationModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    id = 0;
    value: string;
    name: string;
    filenName = [];
    SpecificationName : any[];
    specificationId: any;
    ecommerceProductItemId: any;

    ecommerceProductItemSpecification: EcommerceProductItemSpecificationDto = new EcommerceProductItemSpecificationDto();

    ecommerceProductItemSpecifications: EcommerceProductItemSpecificationDto[] = [];


    //WholeSaleLeadDocumenttype: CreateOrEditWholeSaleLeadDocumentTypeDto = new CreateOrEditWholeSaleLeadDocumentTypeDto();

    constructor(
        injector: Injector,
        private  _eCommerceProductItemSpecificationServiceProxy: ECommerceProductItemsServiceProxy,
        private _commonLookupServiceProxy : CommonLookupServiceProxy,
        private _tokenService: TokenService,
    ) {
        super(injector);
    }
    
     show(productid?: number,typeId?: number): void {
       // debugger;

          if (productid) {
              this._commonLookupServiceProxy.getSpecification(typeId).subscribe(result => {
                  this.SpecificationName= result;
              });
              this.id = productid;
              this.ecommerceProductItemId = productid;
              //this.name = name;

              this.getproductspeicification(productid);
              this.active = true;
              this.modal.show();
          } 

     }

    onShown(): void {
        
        //document.getElementById('Department_Name').focus();
    }

    
    

    getproductspeicification(id :number): void{
        this._eCommerceProductItemSpecificationServiceProxy.getAllEcommerceProductItemSpecification(id).subscribe(result => {
            this.ecommerceProductItemSpecifications = result;
            //this.ecommerceProductItemSpecifications.
        })
    }

    save(): void {
        if (this.specificationId == undefined) {
            this.notify.warn(this.l('Selectname'));
            return;
        }
          
            if (this.ecommerceProductItemSpecification.value == undefined) {
                 this.notify.warn(this.l('Selectvalue'));
                 return;
            }
         
         this.saving = true;
        
        // this.eCommerceProductItem.rating =this.rating;
        // this.eCommerceProductItem.productDescription =this.productDiscription;
         
        this.ecommerceProductItemSpecification.specificationId = this.specificationId;
        this.ecommerceProductItemSpecification.ecommerceProductItemId = this.id;
        //this.ecommerceProductItemSpecification.name = this.name;
        this._eCommerceProductItemSpecificationServiceProxy.saveSpecification(this.ecommerceProductItemSpecification)
        .pipe(finalize(() => {
            this.saving = false;
            //this.spinner.hide();
        }))
        .subscribe(() => {
            this.ecommerceProductItemSpecification = new EcommerceProductItemSpecificationDto();
            this.getproductspeicification(this.id);
            //this.ecommerceProductItemSpecification.value
            //this.ecommerceProductItemSpecification.value
            this.notify.info(this.l('SavedSuccessfully'));

        });
    }

    deleteSpecifications(id : number) : void{
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._eCommerceProductItemSpecificationServiceProxy.deleteSpecification(id)
                        .subscribe(() => {
                            this.getproductspeicification(this.id);
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }




    close(): void {
        this.active = false;
        this.modal.hide();
    }
   
   
}
