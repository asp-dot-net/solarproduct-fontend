import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { CommonLookupServiceProxy,  ECommerceProductItemsServiceProxy,EcommerceProductItemImageDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { AppConsts } from '@shared/AppConsts';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { FileUploader, FileItem, FileUploaderOptions } from 'ng2-file-upload';

@Component({
    selector: 'ProductImageModal',
    templateUrl: './image-product-modal.component.html'
})
export class ProductImageModalComponent extends AppComponentBase {

    @ViewChild('productImageModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    public maxfileBytesUserFriendlyValue = 5;
    public uploader: FileUploader;
    private _uploaderOptions: FileUploaderOptions = {};
    ecommerceProductItemImage:  EcommerceProductItemImageDto = new  EcommerceProductItemImageDto();
    DocumentType : any[];
    documentTypeId: any;
    ecommerceProductItemImages:  EcommerceProductItemImageDto[] = [];
    id = 0;
    filenName = [];

    //WholeSaleLeadDocumenttype: CreateOrEditWholeSaleLeadDocumentTypeDto = new CreateOrEditWholeSaleLeadDocumentTypeDto();

    constructor(
        injector: Injector,
        private  _eCommerceProductItemsServiceProxy: ECommerceProductItemsServiceProxy,
        private _commonLookupServiceProxy : CommonLookupServiceProxy,
        private _tokenService: TokenService,
    ) {
        super(injector);
    }

     show(productid?: number): void {

         if (productid) {
            
             this.id = productid;
             this. getImages(productid);

             this.active = true;
            this.initFileUploader();
             this.modal.show();
         } 

     }

    onShown(): void {
        
        //document.getElementById('Department_Name').focus();
    }

    // save(): void {
    //     this.saving = true;


    //     this._WholeSaleLeadDocumentTypeServiceProxy.createOrEdit(this.WholeSaleLeadDocumenttype)
    //         .pipe(finalize(() => { this.saving = false; }))
    //         .subscribe(() => {
    //             this.notify.info(this.l('SavedSuccessfully'));
    //             this.close();
    //             this.modalSave.emit(null);
    //         });
    //}
    close(): void {
        this.active = false;
        this.modal.hide();
    }

    fileChangeEvent(event: any): void {
        debugger
        if (event.target.files[0].size > 5242880) { //5MB
            this.message.warn(this.l('ProfilePicture_Warn_SizeLimit', this.maxfileBytesUserFriendlyValue));
            return;
        }

        this.uploader.clearQueue();
        this.uploader.addToQueue([<File>event.target.files[0]]);
    }

    saveimage(): void {
        debugger;
        //this.ecommerceProductItemDocument.documentTypeId = this.ecommerceProductItemDocument.documentTypeId ? this.ecommerceProductItemDocument.documentTypeId : 0;
        // if (this.ecommerceProductItemDocument.documentTypeId) {
        //     this.notify.warn(this.l('SelectDocumentType'));
        //     return;
        // }
        
        if (this.uploader.queue.length == 0) {
            this.notify.warn(this.l('SelectFileToUpload'));
            return;
        }
        this.uploader.uploadAll();
    };

    guid(): string {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }

    initFileUploader(): void {
        debugger
        this.uploader = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Profile/UploadFile' });
        this._uploaderOptions.autoUpload = false;
        this._uploaderOptions.authToken = 'Bearer ' + this._tokenService.getToken();
        this._uploaderOptions.removeAfterUpload = true;
        this.uploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

        this.uploader.onBuildItemForm = (fileItem: FileItem, form: any) => {
            form.append('FileType', fileItem.file.type);
            form.append('FileName', fileItem.file.name);
            form.append('FileToken', this.guid());
            this.filenName.push(fileItem.file.name);

        };
        
        this.uploader.onSuccessItem = (item, response, status) => {
            const resp = <IAjaxResponse>JSON.parse(response);
            if (resp.success) {
                this.updateFile(resp.result.fileToken, resp.result.fileName, resp.result.fileType, resp.result.filePath);
            } else {
                this.message.error(resp.error.message);
            }
        };
        this.uploader.setOptions(this._uploaderOptions);
    }

    updateFile(fileToken: string, fileName: string, fileType: string, filePath: string): void {
        // const input = new EcommerceProductItemDocumentDto();
        this.ecommerceProductItemImage.fileToken = fileToken;
        this.ecommerceProductItemImage.ecommerceProductItemId = this.id;
        //this.ecommerceProductItemDocument = this.documentTypeId;
        this.ecommerceProductItemImage.fileName = fileName;
       // this.ecommerceProductItemImage.imageName
       // this.ecommerceProductItemDocument.documentTypeId = this.documentTypeId;
        // this.ecommerceProductItemDocument.fileType = fileType;
        // this.ecommerceProductItemDocument.filePath = filePath;
        //input.sectionName = "Quotation";
        //this.saving = true;
        //this.spinner.show();
        //input.sectionId =this.sectionId;
        this._eCommerceProductItemsServiceProxy.saveImage(this.ecommerceProductItemImage)
            .pipe(finalize(() => {
                this.saving = false;
                //this.spinner.hide();
            }))
            .subscribe(() => {
                this.ecommerceProductItemImage = new EcommerceProductItemImageDto();
                this.getImages(this.id);

                this.notify.info(this.l('SavedSuccessfully'));

            });
    }

     getImages(id :number): void{
         this._eCommerceProductItemsServiceProxy.getAllEcommerceProductItemImages(id).subscribe(result => {
             this. ecommerceProductItemImages = result;
            
         })
     }

    
     downloadfile(file): void {
        debugger;
        let FileName = AppConsts.oldDocUrl + file;
              
        window.open(FileName, "_blank");
        

    }

    deleteimage(id : number) : void{
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._eCommerceProductItemsServiceProxy.deleteImage(id)
                        .subscribe(() => {
                            this.getImages(this.id);
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }
}
