import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { CreateOrEditWholeSaleLeadDocumentTypeDto, ECommerceProductItemsServiceProxy, EcommerceProductItemDto, WholeSaleLeadDocumentTypeServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { AppConsts } from '@shared/AppConsts';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
@Component({
    selector: 'ViewProductModal',
    templateUrl: './view-product-modal.component.html'
})
export class ViewProductModalComponent extends AppComponentBase {

    @ViewChild('viewModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    ecommerceProductItem: EcommerceProductItemDto = new EcommerceProductItemDto();

    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private  _eCommerceProductItemsServiceProxy: ECommerceProductItemsServiceProxy,
    ) {
        super(injector);
    }

    show(WholeSaleLeadDocumenttypeId?: number): void {

        this._eCommerceProductItemsServiceProxy.getEcommerceProductItemForView(WholeSaleLeadDocumenttypeId).subscribe(result => {
            this.ecommerceProductItem = result;
            this.active = true;
            this.modal.show();

            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Opened ECommerce Products View : ' + this.ecommerceProductItem.productName;
            log.section = 'ECommerce Products';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {            
             });
        });
    }

    onShown(): void {
        
        //document.getElementById('Department_Name').focus();
    }

    // save(): void {
    //     this.saving = true;


    //     this._WholeSaleLeadDocumentTypeServiceProxy.createOrEdit(this.WholeSaleLeadDocumenttype)
    //         .pipe(finalize(() => { this.saving = false; }))
    //         .subscribe(() => {
    //             this.notify.info(this.l('SavedSuccessfully'));
    //             this.close();
    //             this.modalSave.emit(null);
    //         });
    // }
    close(): void {
        this.active = false;
        this.modal.hide();
    }
    
    downloadfile(file): void {
        let FileName = AppConsts.oldDocUrl + file;

        window.open(FileName, "_blank");

    }
}
