import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {  CommonLookupDto, CommonLookupServiceProxy, ECommerceProductItemsServiceProxy, EcommerceProductItemDocumentDto, EcommerceProductItemDto, EcommerceProductItemPriceCategoryDto, ProductItemsServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { result } from 'lodash';
import { FileItem, FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { AppConsts } from '@shared/AppConsts';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
@Component({
    selector: 'CreateOrEditProductModal',
    templateUrl: './create-or-edit-product-modal.component.html'
})
export class CreateOrEditProductModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    eCommerceProductItem: EcommerceProductItemDto = new EcommerceProductItemDto();
    ProductCategory : any[];
    PriceCategory : any[];
    DocumentType : any[];
    specialStatues : any[];
    ecommerceProductItemDocuments: EcommerceProductItemDocumentDto[] = [];
    ecommerceProductItemDocument: EcommerceProductItemDocumentDto = new EcommerceProductItemDocumentDto();
    public maxfileBytesUserFriendlyValue = 5;
    public uploader: FileUploader;
    private _uploaderOptions: FileUploaderOptions = {};
    filenName = [];
    documentTypeId: any;
    productDiscription ='';
    specialStatus = 0;
    rating = 0;
    brandingPartners : CommonLookupDto[];
    seriesList : CommonLookupDto[];
    ecommerceProductTypeList : CommonLookupDto[];

    constructor(
        injector: Injector,
        private  _eCommerceProductItemsServiceProxy: ECommerceProductItemsServiceProxy,
        private _commonLookupServiceProxy : CommonLookupServiceProxy,
        private _tokenService: TokenService,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }

    show(eCommerceProductItemId?: number, productCategoryId? : number ): void {

        this._commonLookupServiceProxy.getBrandingPartnersDropdown(productCategoryId).subscribe(result => {
            this.brandingPartners = result;           
        });

        this._commonLookupServiceProxy.getSeriesDropdown(productCategoryId).subscribe(result => {
            this.seriesList = result;           
        });

        this._commonLookupServiceProxy.getEcommercePriceCategoryDropdown().subscribe(result => {
            this.PriceCategory = result;
        });
        

        this._commonLookupServiceProxy.getSpecialStatusDropdown().subscribe(result => {
            this.specialStatues = result;
            
        });

        this._commonLookupServiceProxy.getEcommerceProductTypesDropdown().subscribe(result => {
            this.ecommerceProductTypeList = result;           
        });

        this.ecommerceProductItemDocument.documentName

        // if (!eCommerceProductItemId) {
        //     this.eCommerceProductItem = new EcommerceProductItemDto();
        //     this.eCommerceProductItem.id = eCommerceProductItemId;
        //     this.active = true;
        //     this.modal.show();
        // } else {
            this._eCommerceProductItemsServiceProxy.getEcommerceProductItemForEdit(eCommerceProductItemId).subscribe(result => {
                this.eCommerceProductItem = result;
                result.ecommerceProductItemPriceCategories = !result.ecommerceProductItemPriceCategories ? [] :result.ecommerceProductItemPriceCategories ;
                result.ecommerceProductItemDocuments = !result.ecommerceProductItemDocuments ? [] :result.ecommerceProductItemDocuments ;
                if(result.ecommerceProductItemPriceCategories.length <= 0){
                    //this.eCommerceProductItem.ecommerceProductItemPriceCategories = [];
                    let installationItemCreateOrEdit = new EcommerceProductItemPriceCategoryDto();
                    this.eCommerceProductItem.ecommerceProductItemPriceCategories.push(installationItemCreateOrEdit);
                }
                this.ecommerceProductItemDocuments = result.ecommerceProductItemDocuments;
                // this.rating = result.rating;
                // this.specialStatus = result.specialStatusId;
                // this.productDiscription = result.productDescription;


                this.active = true;
                this.modal.show();
            });
        //}
        this.initFileUploader();
    }

    onShown(): void {
        
        document.getElementById('Department_Name').focus();
    }

    save(): void {
        this.saving = true;
        // this.eCommerceProductItem.rating =this.rating;
        // this.eCommerceProductItem.productDescription =this.productDiscription;
        // this.eCommerceProductItem.specialStatusId =this.specialStatus;


        this._eCommerceProductItemsServiceProxy.createOrEdit(this.eCommerceProductItem)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.eCommerceProductItem.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='ECommerce Products Updated : '+ this.eCommerceProductItem.productName;
                    log.section = 'ECommerce Products';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='ECommerce Products Created : '+ this.eCommerceProductItem.productName;
                    log.section = 'ECommerce Products';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
            });
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }


    removeInstallationItem(installationItem): void {
        if (this.eCommerceProductItem.ecommerceProductItemPriceCategories.length == 1)
            return;
       
        if (this.eCommerceProductItem.ecommerceProductItemPriceCategories.indexOf(installationItem) === -1) {
            
        } else {
            this.eCommerceProductItem.ecommerceProductItemPriceCategories.splice(this.eCommerceProductItem.ecommerceProductItemPriceCategories.indexOf(installationItem), 1);
        }
    }

    addInstallationItem(): void {
        let installationItemCreateOrEdit = new EcommerceProductItemPriceCategoryDto();
        this.eCommerceProductItem.ecommerceProductItemPriceCategories.push(installationItemCreateOrEdit);
    }

    fileChangeEvent(event: any): void {
        debugger
        if (event.target.files[0].size > 5242880) { //5MB
            this.message.warn(this.l('ProfilePicture_Warn_SizeLimit', this.maxfileBytesUserFriendlyValue));
            return;
        }

        this.uploader.clearQueue();
        this.uploader.addToQueue([<File>event.target.files[0]]);
    }

    savedocument(): void {
        debugger;
        //this.ecommerceProductItemDocument.documentTypeId = this.ecommerceProductItemDocument.documentTypeId ? this.ecommerceProductItemDocument.documentTypeId : 0;
        // if (this.ecommerceProductItemDocument.documentTypeId) {
        //     this.notify.warn(this.l('SelectDocumentType'));
        //     return;
        // }
        if (this.documentTypeId == undefined) {
            this.notify.warn(this.l('SelectDocumentType'));
            return;
        }
        if (this.uploader.queue.length == 0) {
            this.notify.warn(this.l('SelectFileToUpload'));
            return;
        }
        this.uploader.uploadAll();
    };

    guid(): string {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }

    initFileUploader(): void {
        debugger
        this.uploader = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Profile/UploadFile' });
        this._uploaderOptions.autoUpload = false;
        this._uploaderOptions.authToken = 'Bearer ' + this._tokenService.getToken();
        this._uploaderOptions.removeAfterUpload = true;
        this.uploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

        this.uploader.onBuildItemForm = (fileItem: FileItem, form: any) => {
            form.append('FileType', fileItem.file.type);
            form.append('FileName', fileItem.file.name);
            form.append('FileToken', this.guid());
            this.filenName.push(fileItem.file.name);

        };
        
        this.uploader.onSuccessItem = (item, response, status) => {
            const resp = <IAjaxResponse>JSON.parse(response);
            if (resp.success) {
                this.updateFile(resp.result.fileToken, resp.result.fileName, resp.result.fileType, resp.result.filePath);
            } else {
                this.message.error(resp.error.message);
            }
        };
        this.uploader.setOptions(this._uploaderOptions);
    }

    updateFile(fileToken: string, fileName: string, fileType: string, filePath: string): void {
        // const input = new EcommerceProductItemDocumentDto();
        this.ecommerceProductItemDocument.fileToken = fileToken;
        this.ecommerceProductItemDocument.ecommerceProductItemId = this.eCommerceProductItem.id;
        //this.ecommerceProductItemDocument = this.documentTypeId;
        this.ecommerceProductItemDocument.fileName = fileName;
        this.ecommerceProductItemDocument.documentTypeId = this.documentTypeId;
        // this.ecommerceProductItemDocument.fileType = fileType;
        // this.ecommerceProductItemDocument.filePath = filePath;
        //input.sectionName = "Quotation";
        //this.saving = true;
        //this.spinner.show();
        //input.sectionId =this.sectionId;
        this._eCommerceProductItemsServiceProxy.saveDocument(this.ecommerceProductItemDocument)
            .pipe(finalize(() => {
                this.saving = false;
                //this.spinner.hide();
            }))
            .subscribe(() => {
                this.ecommerceProductItemDocument = new EcommerceProductItemDocumentDto();
                this.getDocuments();
                this.notify.info(this.l('SavedSuccessfully'));

            });
    }

    getDocuments(): void{
        this._eCommerceProductItemsServiceProxy.getAllEcommerceProductItemDocuments(this.eCommerceProductItem.id).subscribe(result => {
            this.ecommerceProductItemDocuments = result;
        })
    }

    
    
}
