import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { CommonLookupServiceProxy, CreateOrEditWholeSaleLeadDocumentTypeDto, ECommerceProductItemsServiceProxy, EcommerceProductItemDocumentDto, WholeSaleLeadDocumentTypeServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { AppConsts } from '@shared/AppConsts';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { FileUploader, FileItem, FileUploaderOptions } from 'ng2-file-upload';

@Component({
    selector: 'ProductDocumentModal',
    templateUrl: './document-product-modal.component.html'
})
export class ProductDocumentModalComponent extends AppComponentBase {

    @ViewChild('productDocumentModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    public maxfileBytesUserFriendlyValue = 5;
    public uploader: FileUploader;
    private _uploaderOptions: FileUploaderOptions = {};
    ecommerceProductItemDocument: EcommerceProductItemDocumentDto = new EcommerceProductItemDocumentDto();
    DocumentType : any[];
    documentTypeId: any;
    ecommerceProductItemDocuments: EcommerceProductItemDocumentDto[] = [];
    id = 0;
    filenName = [];

    //WholeSaleLeadDocumenttype: CreateOrEditWholeSaleLeadDocumentTypeDto = new CreateOrEditWholeSaleLeadDocumentTypeDto();

    constructor(
        injector: Injector,
        private  _eCommerceProductItemsServiceProxy: ECommerceProductItemsServiceProxy,
        private _commonLookupServiceProxy : CommonLookupServiceProxy,
        private _tokenService: TokenService,
    ) {
        super(injector);
    }

    show(productid?: number): void {

        if (productid) {
            this._commonLookupServiceProxy.getEcommerceDocumentTypeDropdown().subscribe(result => {
                this.DocumentType = result;
            });
            this.id = productid;
            this.getDocuments(productid);

            this.active = true;
            this.initFileUploader();
            this.modal.show();
        } 

    }

    onShown(): void {
        
        //document.getElementById('Department_Name').focus();
    }

    // save(): void {
    //     this.saving = true;


    //     this._WholeSaleLeadDocumentTypeServiceProxy.createOrEdit(this.WholeSaleLeadDocumenttype)
    //         .pipe(finalize(() => { this.saving = false; }))
    //         .subscribe(() => {
    //             this.notify.info(this.l('SavedSuccessfully'));
    //             this.close();
    //             this.modalSave.emit(null);
    //         });
    //}
    close(): void {
        this.active = false;
        this.modal.hide();
    }

    fileChangeEvent(event: any): void {
        debugger
        if (event.target.files[0].size > 5242880) { //5MB
            this.message.warn(this.l('ProfilePicture_Warn_SizeLimit', this.maxfileBytesUserFriendlyValue));
            return;
        }

        this.uploader.clearQueue();
        this.uploader.addToQueue([<File>event.target.files[0]]);
    }

    savedocument(): void {
        debugger;
        //this.ecommerceProductItemDocument.documentTypeId = this.ecommerceProductItemDocument.documentTypeId ? this.ecommerceProductItemDocument.documentTypeId : 0;
        // if (this.ecommerceProductItemDocument.documentTypeId) {
        //     this.notify.warn(this.l('SelectDocumentType'));
        //     return;
        // }
        if (this.documentTypeId == undefined) {
            this.notify.warn(this.l('SelectDocumentType'));
            return;
        }
        if (this.uploader.queue.length == 0) {
            this.notify.warn(this.l('SelectFileToUpload'));
            return;
        }
        this.uploader.uploadAll();
    };

    guid(): string {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }

    initFileUploader(): void {
        debugger
        this.uploader = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Profile/UploadFile' });
        this._uploaderOptions.autoUpload = false;
        this._uploaderOptions.authToken = 'Bearer ' + this._tokenService.getToken();
        this._uploaderOptions.removeAfterUpload = true;
        this.uploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

        this.uploader.onBuildItemForm = (fileItem: FileItem, form: any) => {
            form.append('FileType', fileItem.file.type);
            form.append('FileName', fileItem.file.name);
            form.append('FileToken', this.guid());
            this.filenName.push(fileItem.file.name);

        };
        
        this.uploader.onSuccessItem = (item, response, status) => {
            const resp = <IAjaxResponse>JSON.parse(response);
            if (resp.success) {
                this.updateFile(resp.result.fileToken, resp.result.fileName, resp.result.fileType, resp.result.filePath);
            } else {
                this.message.error(resp.error.message);
            }
        };
        this.uploader.setOptions(this._uploaderOptions);
    }

    updateFile(fileToken: string, fileName: string, fileType: string, filePath: string): void {
        // const input = new EcommerceProductItemDocumentDto();
        this.ecommerceProductItemDocument.fileToken = fileToken;
        this.ecommerceProductItemDocument.ecommerceProductItemId = this.id;
        //this.ecommerceProductItemDocument = this.documentTypeId;
        this.ecommerceProductItemDocument.fileName = fileName;
        this.ecommerceProductItemDocument.documentTypeId = this.documentTypeId;
        // this.ecommerceProductItemDocument.fileType = fileType;
        // this.ecommerceProductItemDocument.filePath = filePath;
        //input.sectionName = "Quotation";
        //this.saving = true;
        //this.spinner.show();
        //input.sectionId =this.sectionId;
        this._eCommerceProductItemsServiceProxy.saveDocument(this.ecommerceProductItemDocument)
            .pipe(finalize(() => {
                this.saving = false;
                //this.spinner.hide();
            }))
            .subscribe(() => {
                this.ecommerceProductItemDocument = new EcommerceProductItemDocumentDto();
                this.getDocuments(this.id);
                this.notify.info(this.l('SavedSuccessfully'));

            });
    }

    getDocuments(id :number): void{
        this._eCommerceProductItemsServiceProxy.getAllEcommerceProductItemDocuments(id).subscribe(result => {
            this.ecommerceProductItemDocuments = result;
        })
    }

    
    downloadfile(file): void {
        let FileName = AppConsts.oldDocUrl + file;

        window.open(FileName, "_blank");

    }

    deleteDoc(id : number) : void{
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._eCommerceProductItemsServiceProxy.deleteDocument(id)
                        .subscribe(() => {
                            this.getDocuments(this.id);
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }
}
