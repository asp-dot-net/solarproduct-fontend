import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {  CommonLookupServiceProxy, ECommerceProductItemsServiceProxy, EcommerceProductItemDto, ProductItemProductTypeLookupTableDto } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import { Title } from '@angular/platform-browser';
import { CreateOrEditProductModalComponent } from './create-or-edit-product-modal.component';
import { ViewProductModalComponent } from './view-product-modal.component';
import { ProductDocumentModalComponent } from './document-product-modal.component';
import { ProductImageModalComponent } from './image-product-modal.component';
import { ProductSpecificationModalComponent } from './specification-product-modal.component';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
@Component({
    templateUrl: './add-product.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class WholesaleProductComponent extends AppComponentBase {

    @ViewChild('CreateOrEditProductModal', { static: true }) CreateOrEditProductModal: CreateOrEditProductModalComponent;
    @ViewChild('ViewProductModal', { static: true }) ViewProductModal: ViewProductModalComponent;
    @ViewChild('productDocumentModal', { static: true }) ProductDocumentModal : ProductDocumentModalComponent;
    @ViewChild('productImageModal', { static: true }) ProductImageModal : ProductImageModalComponent;
    @ViewChild('productSpecificationModal', { static: true }) ProductSpecificationModal : ProductSpecificationModalComponent;


    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    firstrowcount = 0;
    last = 0;
    public screenHeight: any;  
    testHeight = 250;
    allProductTypes: ProductItemProductTypeLookupTableDto[];
    productCategoryId = 0;
    activeFilter = "All";
    constructor(
        injector: Injector,
        private _eCommerceProductItemsServiceProxy: ECommerceProductItemsServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService,
        private titleService: Title,
        private _commonLookupService: CommonLookupServiceProxy

    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  ECommerce Product");
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        this._commonLookupService.getAllProductTypeForTableDropdown().subscribe(result => {
            this.allProductTypes = result;
        });
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open ECommerce Products';
        log.section = 'ECommerce Products';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
    }
    searchLog() : void {
        
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'ECommerce Products';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }
    
    geteCommerceProductItem(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._eCommerceProductItemsServiceProxy.getAll(
            this.filterText,
            this.productCategoryId,
            this.activeFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    deleteeCommerceProductItem(eCommerceProductItem: EcommerceProductItemDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._eCommerceProductItemsServiceProxy.delete(eCommerceProductItem.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));

                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete ECommerce Products: ' + eCommerceProductItem.productName;
                            log.section = 'ECommerce Products';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });
                        });
                }
            }
        );
    }


    createProduct(): void {
        this.CreateOrEditProductModal.show();
    }
    viewProduct(): void {
        this.ViewProductModal.show();
    }

}
