import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import {EcommerceSpecificationDto,EcommerceSpecificationServiceProxy,CommonLookupDto, CommonLookupServiceProxy, CreateOrEditSpecificationDto } from '@shared/service-proxies/service-proxies';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { finalize, map, take } from 'rxjs/operators';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AuditLogListDto } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
@Component({
    selector: 'CreateOrEditSpecificationModal',
    templateUrl: './create-or-edit-specification-modal.html',
})
export class CreateOrEditSpecificationModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    saving = false;
    id? :number;
    selectedProductTypes: CommonLookupDto[] = []; 
    productType: CommonLookupDto[] = [];
    ecommercespecification:CreateOrEditSpecificationDto= new CreateOrEditSpecificationDto();
    //ecospecification: EcommerceSpecificationDto = new EcommerceSpecificationDto();

    constructor(
        injector: Injector,
        private _EcommerceSpecificationServiceProxy: EcommerceSpecificationServiceProxy ,
        private _CommonLookupServiceProxy : CommonLookupServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
       
    }
    
   

    ngOnInit(): void {
        debugger
        this._CommonLookupServiceProxy.getAllProductTypeForTableDropdown().subscribe(result => {
            this.productType  = result;
        });
        
    }

    show(specificationid?: number): void {
      
        if (!specificationid) {
            this.ecommercespecification = new CreateOrEditSpecificationDto();
            this.ecommercespecification.id =specificationid;
            this.ecommercespecification.productType = []; 
            this.selectedProductTypes = []; 
            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New ECommerce Specification';
            log.section = 'ECommerce Specification';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });

        } else {
            this._EcommerceSpecificationServiceProxy.getEcommerceSpecificationForEdit(specificationid).subscribe(result => {
                this.ecommercespecification= result.specification;
                this.active = true;
                this.modal.show();
                this.selectedProductTypes = this.productType.filter(pt => this.ecommercespecification.productType.includes(pt.id.toString()));
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit ECommerce Specification : ' + this.ecommercespecification.name;
                log.section = 'ECommerce Specification';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        
    }
    onShown(): void {
        
        document.getElementById('Name').focus();
    }
    save(): void {
        debugger;
        const productTypeDisplayNames = this.selectedProductTypes.map(pt => pt.id.toString());
    
    this.ecommercespecification.productType = productTypeDisplayNames; 
        this.saving = true;
        this._EcommerceSpecificationServiceProxy.createOrEdit(this.ecommercespecification)
         .pipe(finalize(() => { this.saving = false;}))
         .subscribe(() => {
            this.notify.info(this.l('SavedSuccessfully'));
            // result.items[0].wholesalesjobtypes.IsActive,
            this.close();
            this.modalSave.emit(null);
            if(this.ecommercespecification.id){
                let log = new UserActivityLogDto();
                log.actionId = 82;
                log.actionNote ='ECommerce Specification Updated : '+ this.ecommercespecification.name;
                log.section = 'ECommerce Specification';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }else{
                let log = new UserActivityLogDto();
                log.actionId = 81;
                log.actionNote ='ECommerce Specification Created : '+ this.ecommercespecification.name;
                log.section = 'ECommerce Specification';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }

         });

    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}