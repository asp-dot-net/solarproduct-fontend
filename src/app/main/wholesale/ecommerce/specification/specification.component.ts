import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { EcommerceSpecificationServiceProxy, CreateOrEditSpecificationDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto,EcommerceSpecificationDto} from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import * as _ from 'lodash';
import { CreateOrEditSpecificationModalComponent } from './create-or-edit-specification-modal.component';
import { Title } from '@angular/platform-browser';
@Component({
    templateUrl: './specification.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class EcommerceSpecificationComponent extends AppComponentBase {

    @ViewChild('CreateOrEditSpecificationModal', { static: true }) CreateOrEditSpecificationModal: CreateOrEditSpecificationModalComponent;

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;


    advancedFiltersAreShown = false;
    filterText = '';
    firstrowcount = 0;
    last = 0;
    public screenHeight: any;  
    testHeight = 250;
    IsActive = 'All';
    ProductType = [];
    constructor(
        injector: Injector,
        private _EcommerceSpecificationServiceProxy:  EcommerceSpecificationServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,

        private titleService: Title
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Specification");
    }
    searchLog() : void {
        
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'ECommerce Specification';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
       
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open ECommerce Specification';
        log.section = 'ECommerce Specification';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }
    getSpecification(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._EcommerceSpecificationServiceProxy.getAll(
            this.filterText,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            //result.items[0].specification.productType
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createJobType(): void {
        // this._router.navigate(['/app/main/jobs/jobTypes/createOrEdit']);        

        //this.createOrEditJobTypeModal.show();
    }

   
    createSpecification(): void {
        this.CreateOrEditSpecificationModal.show();
    }      

     deleteSpecification(EcommerceSpecificationDto: EcommerceSpecificationDto): void {
        this.message.confirm(
            this.l('AreYouSureWanttoDelete'),
            this.l('Delete'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._EcommerceSpecificationServiceProxy.delete(EcommerceSpecificationDto.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));

                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete ECommerce Specification: ' + EcommerceSpecificationDto.name;
                            log.section = 'ECommerce Specification';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });

                        });
                }
            }
        );
    }

    // filterLeadStatus(event): void {
    //     this.EcommerceSpecificationServiceProxy.promotionGetAllLeadStatus(event.query).subscribe(result => {
    //         this.filteredLeadStatus = result;
    //     });
    // }




    // exportToExcel(): void {
    //     this._transportTypesServiceProxy.getJobTypesToExcel(
    //         this.filterText,
    //         this.nameFilter,
    //     )
    //         .subscribe(result => {
    //             this._fileDownloadService.downloadTempFile(result);
    //         });
    // }
}
