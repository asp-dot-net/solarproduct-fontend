import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EcommerceSubscriberServiceProxy, TeamDto } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { Title } from '@angular/platform-browser';
import { SubscriberEmailModalComponent } from './sendemail-subscriber-modal.component';

@Component({
    templateUrl: './subscriber.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class EcommerceSubscriberComponent extends AppComponentBase {

    show: boolean = true;
    showchild: boolean = true;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('SubscriberEmailModal', { static: true }) SubscriberEmailModal: SubscriberEmailModalComponent;

    advancedFiltersAreShown = false;
    filterText = '';
    nameFilter = '';
    firstrowcount = 0;
    last = 0;
    count = 0;
    emailList : string[];
    shouldShow = false;

    public screenHeight: any;  
    testHeight = 250;

    constructor(
        injector: Injector,
        private _ecommerceSubscriberServiceProxy: EcommerceSubscriberServiceProxy,
        private titleService: Title,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        ) {
            super(injector);
            this.titleService.setTitle(this.appSession.tenancyName + " |  EcommerceSubscriber");
    }
  

 searchLog() : void {
        
    let log = new UserActivityLogDto();
        log.actionId =80;
        log.actionNote ='Searched by Filters';
        log.section = 'ECommerce Subcriber';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
}
ngOnInit(): void {
    this.screenHeight = window.innerHeight; 
   
    let log = new UserActivityLogDto();
    log.actionId =79;
    log.actionNote ='Open ECommerce Subcriber';
    log.section = 'ECommerce Subcriber';
    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {
    });
}
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    getEcommerceSubscriber(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._ecommerceSubscriberServiceProxy.getAll(
            this.filterText,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();

        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }


    isAllChecked() {
        if (this.primengTableHelper.records)
            return this.primengTableHelper.records.every(_ => _.isSelected);
    }

    oncheckboxCheck() {

        
        this.count = 0;
        this.primengTableHelper.records.forEach(item => {

            if (item.isSelected == true) {
                this.count = this.count + 1;
            }
        })
    }

    checkAll(ev) {

        this.primengTableHelper.records.forEach(x => x.isSelected = ev.target.checked);
        this.oncheckboxCheck();
    }

    sendEmail() : void{
        let selectedids = [];

        if(this.count > 0){
            this.primengTableHelper.records.forEach(function (item) {
                debugger;
                if (item.isSelected ){
                    selectedids.push(item.subscriberEmail)
    
                }
            });
            this.SubscriberEmailModal.show(selectedids);
        }

        
    }   
    testHeightSize () {
        if (this.shouldShow == true) {
            this.testHeight = this.testHeight + 82 ;
        }

        else {
            this.testHeight = this.testHeight - 82 ;
        }
    }
}
