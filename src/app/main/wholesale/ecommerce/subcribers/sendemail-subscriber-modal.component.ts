import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { LeadsServiceProxy, WholeSaleActivityServiceProxy, WholeSaleSmsEmailDto, WholeSaleLeadServiceProxy, CommonLookupDto, CreateOrEditWholeSaleLeadDto, GetEcommerceSubscribeEmailDto, EcommerceSubscriberServiceProxy } from '@shared/service-proxies/service-proxies';
import { EmailEditorComponent } from 'angular-email-editor';
import * as _ from 'lodash';
import { result } from 'lodash';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'SubscriberEmailModal',
    templateUrl: './sendemail-subscriber-modal.component.html',
})
export class SubscriberEmailModalComponent extends AppComponentBase {
    @ViewChild('subscriberEmailModal', { static: true }) modal: ModalDirective;
    
    @ViewChild(EmailEditorComponent)
    // private emailEditor: EmailEditorComponent;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    ExpandedViewApp: boolean = true;
    active = false;    
    activityLog: GetEcommerceSubscribeEmailDto = new GetEcommerceSubscribeEmailDto();
    saving = false;
    role: string = '';
    total = 0;
    credit = 0;
    // allSMSTemplates: CommonLookupDto[];
    // leadCompanyName: any;
    // activityName = '';
    // fromemails: CommonLookupDto[];
    // emailFrom : any;
    // allEmailTemplates: CommonLookupDto[];
    // wholeSalelead : CreateOrEditWholeSaleLeadDto;
    // ccbox = false;
    // bccbox = false;
    emailData = '';
    // smsData = '';
    // templateBody : any;
    @ViewChild(EmailEditorComponent)
    private emailEditor: EmailEditorComponent;

    constructor(injector: Injector        
        ,private _ecommerceSubscriberServiceProxy: EcommerceSubscriberServiceProxy
        , private _wholeSaleLeadServiceProxy : WholeSaleLeadServiceProxy,
        private sanitizer: DomSanitizer
        ) {
        super(injector);
        this.activityLog = new GetEcommerceSubscribeEmailDto();
    }

    show(emailList : string[]) {
     
        this.activityLog.body = "";
        
        this.activityLog.emailFrom = 'info@arisesolar.com.au';
        this.activityLog.emailList = emailList;
        // this._wholeSaleLeadServiceProxy.getOrgWiseDefultandownemailadd(wholeSaleLeadID).subscribe(result => {
        //   debugger;
        //   this.fromemails = result;
        // });

        this.modal.show();        
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;                
    }
    expandApp() {
        this.ExpandedViewApp = false;
    }

    save(){
      this.saving = true;
      this._ecommerceSubscriberServiceProxy.sendEmailBluk(this.activityLog)
      .pipe(finalize(() => { this.saving = false; }))
      .subscribe(() => {
        this.notify.info(this.l('EmailSendSuccessfully'));
        this.modal.hide();
        this.modalSave.emit(null);
        this.activityLog.body = "";
        this.activityLog.subject = '';
      });
      
    }
    
    countCharcters(): void {

        if (this.role != 'Admin') {
          this.total = this.activityLog.body.length;
          this.credit = Math.ceil(this.total / 160);
          if (this.total > 320) {
            this.notify.warn(this.l('You Can Not Add more than 320 characters'));
          }
        }
        else {
          this.total = this.activityLog.body.length;
          this.credit = Math.ceil(this.total / 160);
        }
    }

    saveHTML(data) {
      let htmlTemplate = this.getEmailTemplate(data);
      this.activityLog.body = htmlTemplate;
      this.save();
  }

  getEmailTemplate(emailHTML) {
      let myTemplateStr = emailHTML;

      // interpolate
      let compiled = _.template(myTemplateStr);
      let myTemplateCompiled = compiled();
      return myTemplateCompiled;
  }



sendEmail() {
    this.saving = true;
    debugger;
    this.emailEditor.editor.exportHtml((data) =>
        this.saveHTML(data.html)
    );
}

}
