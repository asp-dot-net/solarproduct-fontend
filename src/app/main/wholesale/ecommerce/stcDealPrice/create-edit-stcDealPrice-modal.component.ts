import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {  CreateOrEditStcDealPriceDto, CheckActiveServiceProxy,CreateOrEditCheckApplicationDto,UserActivityLogDto ,UserActivityLogServiceProxy, StcDealPriceServiceProxy} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';



@Component({
    selector: 'createOrEditStcDealPriceModal',
    templateUrl: './create-edit-stcDealPrice-modal.component.html'
})
export class CreateOrEditStcDealPriceModalComponent extends AppComponentBase {
   
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    saving = false;

    stcDealPrice: CreateOrEditStcDealPriceDto = new CreateOrEditStcDealPriceDto();
    constructor(
        injector: Injector,
        private _StcDealPriceServiceProxy: StcDealPriceServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }
    
    show(Id?: number): void {
    
        if (!Id) {
            this.stcDealPrice = new CreateOrEditStcDealPriceDto();
            this.stcDealPrice.id = Id;
            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New STC Deal Price';
            log.section = 'STC Deal Price';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._StcDealPriceServiceProxy.getStcDeakPriceForEdit(Id).subscribe(result => {
                this.stcDealPrice = result.stcDealPrice;
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit STC Deal Price : ' + this.stcDealPrice.value;
                log.section = 'STC Deal Price';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            });
        }
        
    }
    onShown(): void {
        
        document.getElementById('CheckActives_CheckActives').focus();
    }
    save(): void {
            this.saving = true;
            this._StcDealPriceServiceProxy.createOrEdit(this.stcDealPrice)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.stcDealPrice.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='STC Deal Price Updated : '+ this.stcDealPrice.value;
                    log.section = 'STC Deal Price';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='STC Deal Price Created : '+ this.stcDealPrice.value;
                    log.section = 'STC Deal Price';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }



    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
