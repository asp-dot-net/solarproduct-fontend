import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { EcommerceSpecificationServiceProxy, CreateOrEditSpecificationDto, StcDealPriceServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto,StcDealPriceDto} from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import * as _ from 'lodash';
 import { CreateOrEditStcDealPriceModalComponent } from './create-edit-stcDealPrice-modal.component';
import { Title } from '@angular/platform-browser';
@Component({
    templateUrl: './stcdealprice.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class EcommerceStcDealPriceComponent extends AppComponentBase {

     @ViewChild('createOrEditStcDealPriceModal', { static: true }) createOrEditStcDealPriceModal: CreateOrEditStcDealPriceModalComponent;

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;


    advancedFiltersAreShown = false;
    filterText = '';
    firstrowcount = 0;
    last = 0;
    public screenHeight: any;  
    testHeight = 250;
    IsActive = 'All';
    ProductType = [];
    constructor(
        injector: Injector,
        private _StcDealPriceServiceProxy:  StcDealPriceServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,

        private titleService: Title
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Specification");
    }
    searchLog() : void {
        
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'STC Deal Price';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
       
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open ECommerce STC Deal Price';
        log.section = 'STC Deal Price';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }
    getStcDealPrice(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._StcDealPriceServiceProxy.getAll(
            this.filterText,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            //result.items[0].specification.productType
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createJobType(): void {
        // this._router.navigate(['/app/main/jobs/jobTypes/createOrEdit']);        

        //this.createOrEditJobTypeModal.show();
    }

   
    createStcDealPrice(): void {
        this.createOrEditStcDealPriceModal.show();
    }      

     deleteStcDealPrice(StcDealPriceDto: StcDealPriceDto): void {
        this.message.confirm(
            this.l('AreYouSureWanttoDelete'),
            this.l('Delete'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._StcDealPriceServiceProxy.delete(StcDealPriceDto.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));

                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete ECommerce STC Deal Price: ' + StcDealPriceDto.value;
                            log.section = 'STC Deal Price';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });

                        });
                }
            }
        );
    }

    // filterLeadStatus(event): void {
    //     this.EcommerceSpecificationServiceProxy.promotionGetAllLeadStatus(event.query).subscribe(result => {
    //         this.filteredLeadStatus = result;
    //     });
    // }




    // exportToExcel(): void {
    //     this._transportTypesServiceProxy.getJobTypesToExcel(
    //         this.filterText,
    //         this.nameFilter,
    //     )
    //         .subscribe(result => {
    //             this._fileDownloadService.downloadTempFile(result);
    //         });
    // }
}
