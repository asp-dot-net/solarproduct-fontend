import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize, retry } from 'rxjs/operators';
import { CreateOrEditWholeSaleLeadDocumentTypeDto, SpecialOfferDto, SpecialOfferProductDto, SpecialOfferServiceProxy, WholeSaleLeadDocumentTypeServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'CreateOrEditSpecialOfferModal',
    templateUrl: './create-or-edit-offer-modal.component.html'
})
export class CreateOrEditSpecialOfferModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    SpecialOffer: SpecialOfferDto = new SpecialOfferDto();
    SpecialOfferProduct : SpecialOfferProductDto = new SpecialOfferProductDto();
    searchResult: any [];

    constructor(
        injector: Injector,
        private _specialOfferServiceProxy : SpecialOfferServiceProxy,       
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }

    show(SpecialOfferId?: number): void {

        if (!SpecialOfferId) {
            this.SpecialOffer = new SpecialOfferDto();
            this.SpecialOffer.id = SpecialOfferId;
            this.SpecialOffer.specialOfferProducts = [];

            this.active = true;
            this.modal.show();
            
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New ECommerce Special Offers';
            log.section = 'ECommerce Special Offers';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._specialOfferServiceProxy.getSpecialOfferForEdit(SpecialOfferId).subscribe(result => {
                this.SpecialOffer = result;
                if(result.specialOfferProducts.length > 0){
                    //this.eCommerceProductItem.ecommerceProductItemPriceCategories = [];
                    //let productEdit = new SpecialOfferProductDto();
                    this.SpecialOffer.specialOfferProducts = result.specialOfferProducts;
                }
                else{
                    this.SpecialOffer.specialOfferProducts = [];

                }
                this.active = true;
                this.modal.show();
                
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit ECommerce Special Offers : ' + this.SpecialOffer.offerTitle;
                log.section = 'ECommerce Special Offers';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });

            });
        }
    }

    onShown(): void {
        
        // document.getElementById('Department_Name').focus();
    }

    save(): void {
        this.saving = true;

        if(this.SpecialOffer.specialOfferProducts.length > 0){
            this._specialOfferServiceProxy.createOrEdit(this.SpecialOffer)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                
                if(this.SpecialOffer.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='ECommerce Special Offers Updated : '+ this.SpecialOffer.offerTitle;
                    log.section = 'ECommerce Special Offers';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='ECommerce Special Offers Created : '+ this.SpecialOffer.offerTitle;
                    log.section = 'ECommerce Special Offers';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
            });
        }
        else{
            this.notify.warn("Add Product")
            return; 
        }
        
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }

    AddProduct() : void{
        debugger;
        if(!this.SpecialOfferProduct.productItemId){
            this.notify.warn("Select Product")
            return;
        }
        if(!this.SpecialOfferProduct.newPrice){
            this.notify.warn("Enter The Offer Price")
            return;
        }
        this.SpecialOffer.specialOfferProducts.push(this.SpecialOfferProduct);
        this.SpecialOfferProduct = new SpecialOfferProductDto();
    }

    filterProductame(event): void {
        this._specialOfferServiceProxy.getEcommerceProductsForDropdown(event.query).subscribe(result => {
            this.searchResult = result;
            var items =this.SpecialOffer.specialOfferProducts.map(i=>i.productName+'');
            this.searchResult = result.filter(x=> !items.includes(x.productName))
        });
      }

      OnSelectDetails(event): void {
        debugger;
        this.SpecialOfferProduct.productName = event.productName;
        this.SpecialOfferProduct.productItemId = event.id;
        this.SpecialOfferProduct.oldPrice = event.price;
        this.SpecialOfferProduct.brand = event.brand;
        this.SpecialOfferProduct.productCategory = event.productCategory;
      }

      removeProduct(proItem): void {
        
        if (this.SpecialOffer.specialOfferProducts.indexOf(proItem) === -1) {
            
        } else {
            this.message.confirm(
                '',
                this.l('AreYouSure'),
                (isConfirmed) => {
                    if (isConfirmed) {
                        this.SpecialOffer.specialOfferProducts.splice(this.SpecialOffer.specialOfferProducts.indexOf(proItem), 1);
                        this.notify.success(this.l('SuccessfullyDeleted'));
                    }
                }
            );
        }
    }
}
