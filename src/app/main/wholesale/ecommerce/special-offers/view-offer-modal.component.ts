import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { CreateOrEditWholeSaleLeadDocumentTypeDto, SpecialOfferDto, SpecialOfferServiceProxy, WholeSaleLeadDocumentTypeServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'ViewSpecialOfferModal',
    templateUrl: './view-offer-modal.component.html'
})
export class ViewSpecialOfferModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    SpecialOffer: SpecialOfferDto = new SpecialOfferDto();

    constructor(
        injector: Injector,
        private _specialOfferServiceProxy : SpecialOfferServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }

    show(WholeSaleLeadDocumenttypeId?: number): void {

            this._specialOfferServiceProxy.getSpecialOfferForEdit(WholeSaleLeadDocumenttypeId).subscribe(result => {
                this.SpecialOffer = result;
                this.active = true;
                this.modal.show();

        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Opened ECommerce Special Offers View : ' + this.SpecialOffer.offerTitle;
        log.section = 'ECommerce Special Offers';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {            
         });
            });
        
    }


   
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
