import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { CommonLookupServiceProxy, EcommerceContactUsDto, EcommerceContactUsServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { result } from 'lodash';
import { FileItem, FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { AppConsts } from '@shared/AppConsts';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';

@Component({
    selector: 'AddtoLaedContactUsModal',
    templateUrl: './addtolead-modal.component.html'
})
export class AddtoLaedContactUsModalComponent extends AppComponentBase {

    // @ViewChild('approveuserreuestmodal', { static: true }) modal: ModalDirective;
    @ViewChild('AddtoLaedContactUsModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    organizationUnitId: any;
    organizationUnit: any;

    active = false;
    saving = false;

    contactDto : EcommerceContactUsDto = new EcommerceContactUsDto();

    constructor(
        injector: Injector,
        private _tokenService: TokenService,
        private _commonLookupServiceProxy: CommonLookupServiceProxy,
        private _ecommerceContactUsServiceProxy: EcommerceContactUsServiceProxy
    ) {
        super(injector);
    }

    show(data : EcommerceContactUsDto): void {
        this.showMainSpinner();
        this.contactDto = data;
        this._commonLookupServiceProxy.getOrganizationUnit().subscribe(result => {
            this.organizationUnit = result;
            this.hideMainSpinner();
            this.modal.show();
        }, err => { this.hideMainSpinner(); });
    }

    save(): void {
        this.showMainSpinner();
        this._ecommerceContactUsServiceProxy.addtoLead(this.contactDto.id, this.organizationUnitId).subscribe(result => {
            if(result == "Success")
            {
                this.active = false;
                this.hideMainSpinner(); 
                this.modal.hide();
                this.notify.info(this.l('SavedSuccessfully'));
            }
            else 
            {
                this.hideMainSpinner();
                this.notify.info("Email Address Already Exists");
            }
        }, err => { this.hideMainSpinner(); });
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }
    
}
