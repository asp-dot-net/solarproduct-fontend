import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize, map, take } from 'rxjs/operators';
import { CreateOrEditECommerceSliderDto, ECommerceSliderServiceProxy, EcommerceContactUsDto, EcommerceContactUsServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'CreateOrEditECommerceContactUsModal',
    templateUrl: './create-or-edit-contactus.component.html'
})
export class CreateOrEditECommerceContactUsModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    selectedFile : any;

    ContactUsDto: EcommerceContactUsDto = new EcommerceContactUsDto();

    constructor(
        injector: Injector,
        private _ecommerceContactUsServiceProxy: EcommerceContactUsServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }
   
    show(id?: number): void {

        if (!id) {
            this.ContactUsDto = new EcommerceContactUsDto();
            this.ContactUsDto.id = id;

            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Contact Us';
            log.section = 'Contact Us';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._ecommerceContactUsServiceProxy.getEcommerceContactUsForEdit(id).subscribe(result => {
                this.ContactUsDto = result;
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Contact Us : ' + this.ContactUsDto.firstName;
                log.section = 'Contact Us';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });

            });
        }
    }

    onShown(): void {
        
        //document.getElementById('Department_Name').focus();
    }

    save(): void {
        debugger;
        this.saving = true;

        this._ecommerceContactUsServiceProxy.edit(this.ContactUsDto)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.ContactUsDto.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Contact Us Updated : '+ this.ContactUsDto.firstName;
                    log.section = 'Contact Us';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Contact Us Created : '+ this.ContactUsDto.firstName;
                    log.section = 'Contact Us';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
            });
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }
    


}
