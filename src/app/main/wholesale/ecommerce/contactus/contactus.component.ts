import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { EcommerceContactUsServiceProxy } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import { UserActivityLogServiceProxy, UserActivityLogDto,EcommerceContactUsDto} from '@shared/service-proxies/service-proxies';
import { Title } from '@angular/platform-browser';
import { finalize } from 'rxjs/operators';
import { CreateOrEditECommerceContactUsModalComponent } from './create-or-edit-contactus.component';
import { ContactUSSMSEmailModalComponent } from './contactus-sms-email-modal.component';
import { AddtoLaedContactUsModalComponent } from './addtolead-modal.component';

@Component({
    templateUrl: './contactus.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ContactUsComponent extends AppComponentBase {


    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    @ViewChild('CreateOrEditECommerceContactUsModal', { static: true }) CreateOrEditECommerceContactUsModal: CreateOrEditECommerceContactUsModalComponent;
    @ViewChild('AddtoLaedContactUsModal', { static: true }) AddtoLaedContactUsModal: AddtoLaedContactUsModalComponent;
    @ViewChild('ContactUSSMSEmailModal', { static: true }) ContactUSSMSEmailModal: ContactUSSMSEmailModalComponent;

    advancedFiltersAreShown = false;
    filterText = '';
    firstrowcount = 0;
    last = 0;
    public screenHeight: any;  
    testHeight = 250;
    IsActive = 'All';
    constructor(
        injector: Injector,
        private _ecommerceContactUsServiceProxy: EcommerceContactUsServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private titleService: Title
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Contact Us");
    }
    searchLog() : void {
        
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Contact Us';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight;

        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Contact Us';
        log.section = 'Contact Us';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
    }
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }
    getContactUs(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._ecommerceContactUsServiceProxy.getAll(
            this.filterText,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    deletecontact(EcommerceContactUsDto:EcommerceContactUsDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._ecommerceContactUsServiceProxy.delete(EcommerceContactUsDto.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete Contact Us: ' + EcommerceContactUsDto.firstName;
                            log.section = 'Contact Us';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });
                        });
                }
            }
        );
    }


}
