import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { LeadsServiceProxy, WholeSaleActivityServiceProxy, WholeSaleSmsEmailDto, WholeSaleLeadServiceProxy, CommonLookupDto, CreateOrEditWholeSaleLeadDto, EcommerceContactUsServiceProxy } from '@shared/service-proxies/service-proxies';
import { EmailEditorComponent } from 'angular-email-editor';
import * as _ from 'lodash';
import { result } from 'lodash';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'ContactUSSMSEmailModal',
    templateUrl: './contactus-sms-email-modal.component.html',
})
export class ContactUSSMSEmailModalComponent extends AppComponentBase {
    @ViewChild('ContactUSSMSEmailModal', { static: true }) modal: ModalDirective;
    
    @ViewChild(EmailEditorComponent)
    private emailEditor: EmailEditorComponent;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    ExpandedViewApp: boolean = true;
    active = false;    
    activityLog: WholeSaleSmsEmailDto = new WholeSaleSmsEmailDto();
    saving = false;
    //role: string = '';
    total = 0;
    credit = 0;
    //allSMSTemplates: CommonLookupDto[];
    //leadCompanyName: any;
    activityName = '';
    //fromemails: CommonLookupDto[];
    //emailFrom : any;
    //allEmailTemplates: CommonLookupDto[];
    //wholeSalelead : CreateOrEditWholeSaleLeadDto;
    //ccbox = false;
    //bccbox = false;
    //emailData = '';
    //smsData = '';
    //templateBody : any;

    constructor(injector: Injector
        , private _dateTimeService: DateTimeService,
        private _ecommerceContactUsServiceProxy: EcommerceContactUsServiceProxy,
       
        private sanitizer: DomSanitizer
        ) {
        super(injector);
       // this.wholeSalelead = new CreateOrEditWholeSaleLeadDto();
        this.activityLog = new WholeSaleSmsEmailDto();
        this.activityLog.emailTemplateId = 0;
    }

    show(type : number, tomailorphone : string) {
        //this.activityLog.wholeSaleLeadId = wholeSaleLeadID;
        //this.activityLog.sectionId = sectionId;
        this.activityLog.body = "";
        this.activityLog.emailFrom = 'info@arisesolar.com.au';
        this.activityLog.emailTo = tomailorphone;
        this.activityLog.phoneNo = tomailorphone;
        
        if (type == 1) {
          this.activityName = "SMS";
        }
        else if (type == 2) {
          this.activityName = "Email";
        }


        this.modal.show();        
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;                
    }
    expandApp() {
        this.ExpandedViewApp = false;
    }

    save(){
      this.saving = true;
      if(this.activityName == "SMS"){
        this._ecommerceContactUsServiceProxy.sendSMS(this.activityLog)
        .pipe(finalize(() => { this.saving = false; }))
        .subscribe(() => {
          this.modal.hide();
          this.notify.info(this.l('SmsSendSuccessfully'));
          this.modalSave.emit(null);
          this.activityLog.body = "";
          this.activityLog.emailTemplateId = 0;
          this.activityLog.smsTemplateId = 0;
          this.activityLog.customeTagsId = 0;
          this.activityLog.subject = '';
        });
      }
      else if(this.activityName == "Email"){
        this._ecommerceContactUsServiceProxy.sendEmail(this.activityLog)
        .pipe(finalize(() => { this.saving = false; }))
        .subscribe(() => {
          this.notify.info(this.l('EmailSendSuccessfully'));
          this.modal.hide();
          this.modalSave.emit(null);
          this.activityLog.body = "";
          this.activityLog.emailTemplateId = 0;
          this.activityLog.smsTemplateId = 0;
          this.activityLog.customeTagsId = 0;
          this.activityLog.subject = '';
        });
      }
      
    }
    
    countCharcters(): void {

        this.total = this.activityLog.body.length;
        this.credit = Math.ceil(this.total / 160);
    }

    

    

    // setsmsHTML(smsHTML) {
    //   let htmlTemplate = smsHTML;
    //   this.activityLog.body = htmlTemplate;
    //   this.countCharcters();
    // }
  
    

    // saveDesign() {
    //   this.saving = true;
      
    // }
  
    // setHTML(emailHTML) {
    //   let htmlTemplate = this.getEmailTemplate(emailHTML);
    //   this.activityLog.body = htmlTemplate;
    //   this.save();
    // }
}
