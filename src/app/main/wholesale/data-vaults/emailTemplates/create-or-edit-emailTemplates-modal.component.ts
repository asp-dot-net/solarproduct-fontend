import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { WholeSaleEmailTamplateServiceProxy,UserActivityLogDto, CreateOrEditWholeSaleEmailTemplateDto, CommonLookupServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { Title } from '@angular/platform-browser';
import { UserActivityLogServiceProxy} from '@shared/service-proxies/service-proxies';
@Component({
    selector: 'createOrEditQuote',
    templateUrl: './create-or-edit-emailTemplates-modal.component.html',
    styleUrls: ['./create-or-edit-emailTemplates-modal.component.less'],
})
export class CreateOrEditWholeSaleEmailTemplateModalComponent extends AppComponentBase {

    saving = false;

    WholeSaleEmailTemplate: CreateOrEditWholeSaleEmailTemplateDto = new CreateOrEditWholeSaleEmailTemplateDto();
    
    allOrganizationUnits = [];
    templateType = [];

    htmlContentbody: any = '';

    constructor(
        injector: Injector,
        private _WholeSaleEmailTemplateServiceProxy: WholeSaleEmailTamplateServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        
        private _router: Router,
        private titleService: Title
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Html Template");
    }

    ngOnInit(): void {

        this._activatedRoute.queryParams.subscribe(params => {
            this.WholeSaleEmailTemplate.id = params['Id'];
        });

        if (this.WholeSaleEmailTemplate.id != null) {
            //this.editorLoaded();
            if (this.WholeSaleEmailTemplate.id !== null && this.WholeSaleEmailTemplate.id !== undefined) {
                this._WholeSaleEmailTemplateServiceProxy.getWholeSaleEmailTemplateForEdit(this.WholeSaleEmailTemplate.id).subscribe(result => {
                    this.WholeSaleEmailTemplate = result.emailTemplateDto;
                    this.htmlContentbody = this.WholeSaleEmailTemplate.viewHtml;
                    let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Edit Wholesale Email Template : ' + this.WholeSaleEmailTemplate.templateName;
            log.section = 'Wholesale Email Template';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
                });
            }
            
        }
        else{
            this.WholeSaleEmailTemplate = new CreateOrEditWholeSaleEmailTemplateDto();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Wholesale Email Template';
            log.section = 'Wholesale Email Template';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        }

        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
        });

        this._commonLookupService.getTemplateTypeForTableDropdown().subscribe(result => {
            this.templateType = result;
        });

        
    }
   
    
    cancel(): void {
        this._router.navigate(['app/main/wholesale/data-vaults/emailTemplates']);
    }

    config: AngularEditorConfig = {
        editable: true,
        spellcheck: true,
        height: "45rem",
        minHeight: "15rem",
        placeholder: "Enter text here...",
        translate: "no",
        defaultParagraphSeparator: "p",
        defaultFontName: "Arial",
        toolbarHiddenButtons: [["bold"]],
        sanitize: false,
        customClasses: [
            {
                name: "quote",
                class: "quote"
            },
            {
                name: "redText",
                class: "redText"
            },
            {
                name: "titleText",
                class: "titleText",
                tag: "h1"
            }
        ],
        uploadUrl: 'v1/image',
        //upload: (file: File) => { ... }
        uploadWithCredentials: false,
        //sanitize: true,
        toolbarPosition: 'top',
        // toolbarHiddenButtons: [
        //     ['bold', 'italic'],
        //     ['fontSize']
        // ]
    };

    editorLoaded() {
        if (this.WholeSaleEmailTemplate.id !== null && this.WholeSaleEmailTemplate.id !== undefined) {
            this._WholeSaleEmailTemplateServiceProxy.getWholeSaleEmailTemplateForEdit(this.WholeSaleEmailTemplate.id).subscribe(result => {
                this.WholeSaleEmailTemplate = result.emailTemplateDto;
                this.htmlContentbody = this.WholeSaleEmailTemplate.viewHtml;
            });
        }
    }
   
    saveDesign(): void {
        this.saving = true;
        // this.WholeSaleEmailTemplate.viewHtml = this.htmlContentbody;
        this._WholeSaleEmailTemplateServiceProxy.createOrEdit(this.WholeSaleEmailTemplate)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this._router.navigate(['app/main/wholesale/data-vaults/emailTemplates']);
                if(this.WholeSaleEmailTemplate.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Wholesale Email Template Updated : '+ this.WholeSaleEmailTemplate.templateName;
                    log.section = 'Wholesale Email Template';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Wholesale Email Template Created : '+ this.WholeSaleEmailTemplate.templateName;
                    log.section = 'Wholesale Email Template';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
                // this.close();
                // this.modalSave.emit(null);
            });
    }

}
