import {AppConsts} from '@shared/AppConsts';
import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute , Router} from '@angular/router';
import { WholeSaleEmailTamplateServiceProxy, WholeSaleEmailTamplateDto, OrganizationUnitDto, CommonLookupServiceProxy } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { Title } from '@angular/platform-browser';


@Component({
    templateUrl: './emailTemplates.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class WholeSaleEmailTemplatesComponent extends AppComponentBase {
   
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    allOrganizationUnits: OrganizationUnitDto[];

    organizationUnit = 0;
    organizationUnitlength = 0;
    firstrowcount = 0;
    last = 0;
    public screenHeight: any;  
    testHeight = 250;
    constructor(
        injector: Injector,
        private _wholeSaleEmailTemplatesServiceProxy: WholeSaleEmailTamplateServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private titleService: Title,
		private _router: Router,

    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Email Templates");
    }
   
    searchLog() : void {
        
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Wholesale Email Template';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.getEmailTemplates();
        });
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Wholesale Email Template';
        log.section = 'Wholesale Email Template';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
    }
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }
    getEmailTemplates(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._wholeSaleEmailTemplatesServiceProxy.getAll(
            this.filterText,
            this.organizationUnit,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
            
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createEmailTemplate(val = 0): void {
        if(val == 0) {
            this._router.navigate(['/app/main/wholesale/data-vaults/emailTemplates/createOrEdit']);
        }
        else{
            this._router.navigate(['/app/main/wholesale/data-vaults/emailTemplates/createOrEdit'], { queryParams: {Id : val} });
        }
    }


    deleteEmailTemplate(emailTemplate: WholeSaleEmailTamplateDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._wholeSaleEmailTemplatesServiceProxy.delete(emailTemplate.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete Wholesale Email Template: ' + emailTemplate.templateName;
                            log.section = 'Wholesale Email Template';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });
                        });
                }
            }
        );
    }  
}
