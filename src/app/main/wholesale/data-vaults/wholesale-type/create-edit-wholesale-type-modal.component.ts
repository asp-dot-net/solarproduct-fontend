import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AuditLogListDto, WholeSaleStatusDto, WholeSaleStatuseServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'CreateEditWholesaleTypeModal',
    templateUrl: './create-edit-wholesale-type-modal.html',
})
export class CreateEditWholesaleTypeModal extends AppComponentBase {
    @ViewChild('CreateEditWholesaleTypeModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    ExpandedViewApp: boolean = true;
    active = false;    
    saving = false;
    wholeSaleStatus : WholeSaleStatusDto = new WholeSaleStatusDto()
    wholeSaleStatusName = '';
    constructor(injector: Injector,private _userActivityLogServiceProxy : UserActivityLogServiceProxy, private _dateTimeService: DateTimeService,
        private _wholeSaleStatuseServiceProxy : WholeSaleStatuseServiceProxy) {
        super(injector);
        this.wholeSaleStatus = new WholeSaleStatusDto();
    }


    show(statusid? : number) {

        if(statusid > 0) {
            this._wholeSaleStatuseServiceProxy.getLeadSourceForEdit(statusid).subscribe(result => {
                this.wholeSaleStatus = result;
            });
            
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Wholesale Status';
            log.section = 'Wholesale Status';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        }
        else{
            this.wholeSaleStatus = new WholeSaleStatusDto();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Edit Wholesale Status : ' + this.wholeSaleStatus.name;
            log.section = 'Wholesale Status';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        }

        this.modal.show();        
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;                
    }
    expandApp() {
        this.ExpandedViewApp = false;
    }
    save(): void {
        debugger;
        this.saving = true; 
        this.wholeSaleStatus.name = this.wholeSaleStatusName;
        this._wholeSaleStatuseServiceProxy.createOrEdit(this.wholeSaleStatus)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.close();
                this.modalSave.emit(null);
                this.notify.info(this.l('SavedSuccessfully'));
                
                if(this.wholeSaleStatus.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Wholesale Status Updated : '+ this.wholeSaleStatus.name;
                    log.section = 'Wholesale Status';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Wholesale Status Created : '+ this.wholeSaleStatus.name;
                    log.section = 'Wholesale Status';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
            });
    }
}
