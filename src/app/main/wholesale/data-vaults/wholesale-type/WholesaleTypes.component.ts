﻿import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { WholeSaleStatuseServiceProxy, WholeSaleStatusDto } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
// import { CreateOrEditJobTypeModalComponent } from './create-or-edit-jobType-modal.component';
import { WholesaleTypeModal } from './wholesale-type-modal.component';
import { CreateEditWholesaleTypeModal } from './create-edit-wholesale-type-modal.component';
import { Title } from '@angular/platform-browser';
@Component({
    templateUrl: './WholesaleTypes.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class WholesaleTypesComponent extends AppComponentBase {

    // @ViewChild('createOrEditJobTypeModal', { static: true }) createOrEditJobTypeModal: CreateOrEditJobTypeModalComponent;
   // @ViewChild('ViewJobTypeModalComponent', { static: true }) viewWholesaleTypeModal: ViewJobTypeModalComponent;
   @ViewChild('WholesaleTypeModal', { static: true }) WholesaleTypeModal: WholesaleTypeModal;
   @ViewChild('CreateEditWholesaleTypeModal', { static: true }) CreateEditWholesaleTypeModal: CreateEditWholesaleTypeModal;

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    nameFilter = '';
    firstrowcount = 0;
    last = 0;
    public screenHeight: any;  
    testHeight = 250;
    constructor(
        injector: Injector,
        private _wholeSaleStatuseServiceProxy : WholeSaleStatuseServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService,
        private _router: Router,
        private titleService: Title
        ) {
            super(injector);
            this.titleService.setTitle(this.appSession.tenancyName + " |  Wholesale Status");
    }
    searchLog() : void {
        
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Wholesale Status';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
       
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Wholesale Status';
        log.section = 'Wholesale Status';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
    }
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }

    getWholeSaleStatus(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._wholeSaleStatuseServiceProxy.getAll(
            this.filterText,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createJobType(): void {
        // this._router.navigate(['/app/main/jobs/jobTypes/createOrEdit']);        

        //this.createOrEditJobTypeModal.show();
    }

    showWholesaleModal(): void {
        this.WholesaleTypeModal.show();
     }
     createWholesaleTypeModal(id : number): void {
        this.CreateEditWholesaleTypeModal.show(id);
     }     

    deleteStatus(WholeSaleStatusDto: WholeSaleStatusDto): void {
        this.message.confirm(
            this.l('AreYouSureWanttoDelete'),
            this.l('Delete'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._wholeSaleStatuseServiceProxy.delete(WholeSaleStatusDto.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));

                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete Wholesale Status: ' + WholeSaleStatusDto.name;
                            log.section = 'Wholesale Status';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });

                        });
                }
            }
        );
    }

    // exportToExcel(): void {
    //     this._jobTypesServiceProxy.getJobTypesToExcel(
    //         this.filterText,
    //         this.nameFilter,
    //     )
    //         .subscribe(result => {
    //             this._fileDownloadService.downloadTempFile(result);
    //         });
    // }
}
