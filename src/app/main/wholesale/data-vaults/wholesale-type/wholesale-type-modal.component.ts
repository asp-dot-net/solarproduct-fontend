import { Component, Injector, ViewChild } from '@angular/core';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AuditLogListDto } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
@Component({
    selector: 'WholesaleTypeModal',
    templateUrl: './wholesale-type-modal.html',
})
export class WholesaleTypeModal extends AppComponentBase {
    @ViewChild('WholesaleTypeModal', { static: true }) modal: ModalDirective;

    ExpandedViewApp: boolean = true;
    active = false;    

    constructor(injector: Injector, private _dateTimeService: DateTimeService) {
        super(injector);
    }

    show() {
        this.modal.show();        
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;                
    }
    expandApp() {
        this.ExpandedViewApp = false;
    }
}
