import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { CreateOrEditWholeSaleLeadDocumentTypeDto, WholeSaleLeadDocumentTypeServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'CreateOrEditWholeSaleLeadDocumentTypeModal',
    templateUrl: './create-or-edit-wholeSaleLeadDocumentType-modal.component.html'
})
export class CreateOrEditWholeSaleLeadDocumentTypeModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    WholeSaleLeadDocumenttype: CreateOrEditWholeSaleLeadDocumentTypeDto = new CreateOrEditWholeSaleLeadDocumentTypeDto();

    constructor(
        injector: Injector,
        private _WholeSaleLeadDocumentTypeServiceProxy: WholeSaleLeadDocumentTypeServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }

    show(WholeSaleLeadDocumenttypeId?: number): void {

        if (!WholeSaleLeadDocumenttypeId) {
            this.WholeSaleLeadDocumenttype = new CreateOrEditWholeSaleLeadDocumentTypeDto();
            this.WholeSaleLeadDocumenttype.id = WholeSaleLeadDocumenttypeId;

            this.active = true;
            this.modal.show();
            
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Wholesale Document Type';
            log.section = 'Wholesale Document Type';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._WholeSaleLeadDocumentTypeServiceProxy.getWholeSaleLeadDocumentTypeForEdit(WholeSaleLeadDocumenttypeId).subscribe(result => {
                this.WholeSaleLeadDocumenttype = result.createorEditWholeSaleLeadDocumentTypeDto;
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Wholesale Document Type : ' + this.WholeSaleLeadDocumenttype.title;
                log.section = 'Wholesale Document Type';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });

            });
        }
    }

    onShown(): void {
        
        document.getElementById('Department_Name').focus();
    }

    save(): void {
        this.saving = true;


        this._WholeSaleLeadDocumentTypeServiceProxy.createOrEdit(this.WholeSaleLeadDocumenttype)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.WholeSaleLeadDocumenttype.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Wholesale Document Type Updated : '+ this.WholeSaleLeadDocumenttype.title;
                    log.section = 'Wholesale Document Type';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Wholesale Document Type Created : '+ this.WholeSaleLeadDocumenttype.title;
                    log.section = 'Wholesale Document Type';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
            });
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
