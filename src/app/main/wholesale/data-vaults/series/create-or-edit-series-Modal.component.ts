import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize, map, take } from 'rxjs/operators';
import {  SeriesDto, SeriesesServiceProxy, CommonLookupDto, CommonLookupServiceProxy, CreateOrEditSeriesDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { AppConsts } from '@shared/AppConsts';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'CreateOrEditSeriesModal',
    templateUrl:'./create-or-edit-series-Modal.component.html'
})
export class CreateOrEditSeriesModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    selectedFile : any;

    Series:  SeriesDto = new  SeriesDto();

    constructor(
        injector: Injector,
        private _seriesServiceProxy: SeriesesServiceProxy,
        private _tokenService: TokenService,
        private _CommonLookupServiceProxy : CommonLookupServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,

    ) {
        super(injector);
    }
    productTypes : CommonLookupDto[] ;
    show(seriesid?: number): void {
        this._CommonLookupServiceProxy.getAllProductTypeForTableDropdown().subscribe(result => {
            this.productTypes =result;
        });
        if (!seriesid) {
            
            this.Series = new CreateOrEditSeriesDto();
            this.Series.id = seriesid;
            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Wholesale Series';
            log.section = 'Wholesale Series';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });

        } else {
            this._seriesServiceProxy.getSeriesForEdit(seriesid).subscribe(result => {
                this.Series = result.series;
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Wholesale Series : ' + this.Series.seriesName;
                log.section = 'Wholesale Series';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        
    }
    onShown(): void {
        
        document.getElementById('Name').focus();
    }
   
    save(): void {
        this.saving = true;
        this._seriesServiceProxy.createOrEdit(this.Series)
         .pipe(finalize(() => { this.saving = false;}))
         .subscribe(() => {
            this.notify.info(this.l('SavedSuccessfully'));
            // result.items[0].currency.name,
            this.close();
            this.modalSave.emit(null);
            
            if(this.Series.id){
                let log = new UserActivityLogDto();
                log.actionId = 82;
                log.actionNote ='Wholesale Series Updated : '+ this.Series.seriesName;
                log.section = 'Wholesale Series';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }else{
                let log = new UserActivityLogDto();
                log.actionId = 81;
                log.actionNote ='Wholesale Series Created : '+ this.Series.seriesName;
                log.section = 'Wholesale Series';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }
         });

    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }


}
