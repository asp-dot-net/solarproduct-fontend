import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import {WholesaleJobStatusServiceProxy, CreateOrEditWholesaleJobStatusDto } from '@shared/service-proxies/service-proxies';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { finalize, map, take } from 'rxjs/operators';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AuditLogListDto } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
@Component({
    selector: 'CreateOrEditJobStatusModal',
    templateUrl: './create-or-edit-job-status-modal.html',
})
export class CreateOrEditWholesaleJobStatusModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    saving = false;
    id? :number;
    wholesalesjobstatus:CreateOrEditWholesaleJobStatusDto= new CreateOrEditWholesaleJobStatusDto();
    constructor(
        injector: Injector,
        private _WholesaleJobStatusServiceProxy: WholesaleJobStatusServiceProxy ,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }
    
    show(jobstatusid?: number): void {
        if (!jobstatusid) {
            this.wholesalesjobstatus = new CreateOrEditWholesaleJobStatusDto();
            this.wholesalesjobstatus.id =jobstatusid;
            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Wholesale Job Status';
            log.section = 'Wholesale Job Status';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._WholesaleJobStatusServiceProxy.getWholesaleJobStatusForEdit(jobstatusid).subscribe(result => {
                this.wholesalesjobstatus= result.jobStatus;
                this.active = true;
                this.modal.show();
                
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Wholesale Job Status : ' + this.wholesalesjobstatus.name;
                log.section = 'Wholesale Job Status';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        
    }
    onShown(): void {
        
        document.getElementById('Name').focus();
    }
    save(): void {
        this.saving = true;
        this._WholesaleJobStatusServiceProxy.createOrEdit(this.wholesalesjobstatus)
         .pipe(finalize(() => { this.saving = false;}))
         .subscribe(() => {
            this.notify.info(this.l('SavedSuccessfully'));
            // result.items[0].wholesalesjobtypes.IsActive,
            this.close();
            this.modalSave.emit(null);
            if(this.wholesalesjobstatus.id){
                let log = new UserActivityLogDto();
                log.actionId = 82;
                log.actionNote ='Wholesale Job Status Updated : '+ this.wholesalesjobstatus.name;
                log.section = 'Wholesale Job Status';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }else{
                let log = new UserActivityLogDto();
                log.actionId = 81;
                log.actionNote ='Wholesale Job Status Created : '+ this.wholesalesjobstatus.name;
                log.section = 'Wholesale Job Status';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }
         });

    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}