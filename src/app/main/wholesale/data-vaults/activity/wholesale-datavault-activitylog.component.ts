import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DepartmentsServiceProxy, DepartmentDto, DocumentTypeServiceProxy, DocumentTypeDto, DataVaultActivityLogServiceProxy, GetAllActivityLogDto, GetActivityLogHistoryDto, WholeSaleDataVaultActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';



import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';21
import * as moment from 'moment';
import { Title } from '@angular/platform-browser';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
@Component({
    templateUrl: './wholesale-datavault-activitylog.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class WholeSaleDataVaultActivityLogComponent extends AppComponentBase {

    // @ViewChild('createOrEditDocumentTypeModal', { static: true }) createOrEditDocumentTypeModal: CreateOrEditDocumentTypeModalComponent;
    // @ViewChild('viewDocumentTypeModal', { static: true }) viewDocumentTypeModal: ViewDocumentTypeModalComponent;

    @ViewChild('detailActivityModal', { static: true }) modal: ModalDirective;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    firstrowcount = 0;
    last = 0;
    public screenHeight: any;  
    testHeight = 250;
    DataVaultHistoryList : GetActivityLogHistoryDto[];
    DataVaultList : GetAllActivityLogDto[];
    ExpandedView: boolean = true;

    constructor(
        injector: Injector,
        private _dataVaultActivityLogServiceProxy: WholeSaleDataVaultActivityLogServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService,
        private titleService: Title
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Activity Log");
    }
    
    searchLog() : void {
        
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Wholesale Data Vaults Activity';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
        this.getActivity();
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Wholesale Data Vaults Activity';
        log.section = 'Wholesale Data Vaults Activity';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
    }
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }
    getActivity(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._dataVaultActivityLogServiceProxy.getAllWholeSaleDataVaultActivityLog(
            0,
            undefined,
            undefined,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event) > 0 ? this.primengTableHelper.getMaxResultCount(this.paginator, event) : 25
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    // deleteDocumenttype(documenttype: DocumentTypeDto): void {
    //     this.message.confirm(
    //         '',
    //         this.l('AreYouSure'),
    //         (isConfirmed) => {
    //             if (isConfirmed) {
    //                 this._documenttypeServiceProxy.delete(documenttype.id)
    //                     .subscribe(() => {
    //                         this.reloadPage();
    //                         this.notify.success(this.l('SuccessfullyDeleted'));
    //                     });
    //             }
    //         }
    //     );
    // }


    DataVaultloghistory(DataVaultHistoryId){
        this._dataVaultActivityLogServiceProxy.getWholeSaleDataVaultActivityLogHistory(DataVaultHistoryId).subscribe(result => {
        
            this.DataVaultHistoryList = result;
        }, error => { this.spinnerService.hide() });
        this.modal.show();
    }

    expandGrid() {
        this.ExpandedView = true;
    }

    navigateToLeadDetail(dataVaultSectionid): void {
        
        this.ExpandedView = !this.ExpandedView;
        this.ExpandedView = false;
        this.GetDataVaultSectionWiseLogList(dataVaultSectionid);
        
    }
    setctionName:string = '';
    GetDataVaultSectionWiseLogList(sectionId){
        this._dataVaultActivityLogServiceProxy.getWholeSaleDataVaultActivityLogDetail(sectionId).subscribe(result => {
            this.setctionName = result[0].sectionName;
            this.DataVaultList = result;
        }, error => { this.spinnerService.hide() });
    }
    close(): void {
        this.modal.hide();
    }

}
