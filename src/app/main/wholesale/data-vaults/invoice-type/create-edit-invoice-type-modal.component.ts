import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize, map, take } from 'rxjs/operators';
import { CreateOrEditWholesaleinvoicetypesDto , WholesaleinvoicetypeServiceProxy } from '@shared/service-proxies/service-proxies';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AuditLogListDto } from '@shared/service-proxies/service-proxies';
import * as moment from 'moment';
import { AppConsts } from '@shared/AppConsts';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { FileUploader, FileItem, FileUploaderOptions } from 'ng2-file-upload';
import { Observable, fromEvent } from 'rxjs';
import { result } from 'lodash';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'CreateEditInvoiceTypeModal',
    templateUrl: './create-edit-invoice-type-modal.component.html',
})
export class CreateEditInvoiceTypeModal extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    ExpandedViewApp: boolean = true;
    active = false;   
    saving = false;
    id? :number;
    wholesaleinvoice:CreateOrEditWholesaleinvoicetypesDto= new  CreateOrEditWholesaleinvoicetypesDto();
    constructor(
        injector: Injector,
        private _wholesaleinvoicetypeServiceProxy: WholesaleinvoicetypeServiceProxy ,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }

    show(invoiceid?: number): void {
        if (!invoiceid) {
            this.  wholesaleinvoice = new CreateOrEditWholesaleinvoicetypesDto();
            this.  wholesaleinvoice.id =invoiceid;
            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Wholesale Invoice Type';
            log.section = 'Wholesale Invoice Type';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._wholesaleinvoicetypeServiceProxy.getWholesaleinvoicetypeForEdit(invoiceid).subscribe(result => {
                this.wholesaleinvoice= result.invoice;
                this.active = true;
                this.modal.show();
                
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Wholesale Invoice Type : ' + this.wholesaleinvoice.name;
                log.section = 'Wholesale Invoice Type';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        
    }
        
    

    onShown(): void {
        
        document.getElementById('Name').focus();
    }
    save(): void {
        this.saving = true;
        this._wholesaleinvoicetypeServiceProxy.createOrEdit(this.wholesaleinvoice)
         .pipe(finalize(() => { this.saving = false;}))
         .subscribe(() => {
            this.notify.info(this.l('SavedSuccessfully'));
             //result.items[0].stockfroms,
            this.close();
            this.modalSave.emit(null);

            if(this.wholesaleinvoice.id){
                let log = new UserActivityLogDto();
                log.actionId = 82;
                log.actionNote ='Wholesale Invoice Type Updated : '+ this.wholesaleinvoice.name;
                log.section = 'Wholesale Invoice Type';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }else{
                let log = new UserActivityLogDto();
                log.actionId = 81;
                log.actionNote ='Wholesale Invoice Type Created : '+ this.wholesaleinvoice.name;
                log.section = 'Wholesale Invoice Type';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }
         });

    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
