﻿import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { WholesaleDeliveryOptionServiceProxy, DeliveryOptionDto } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
 import { DeliveryOptionModal } from './delivery-options-modal.component';
 import { CreateEditDeliveryOptionModal } from './create-edit-delivery-option-modal.component';

import { Title } from '@angular/platform-browser';
@Component({
    templateUrl: './delivery-options.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class DeliveryOptionsComponent extends AppComponentBase {

    @ViewChild('DeliveryOptionModal', { static: true }) DeliveryOptionModal: DeliveryOptionModal;
    @ViewChild('CreateEditDeliveryOptionModal', { static: true }) CreateEditdeliveryoptionmodal: CreateEditDeliveryOptionModal;

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    nameFilter = '';
    firstrowcount = 0;
    last = 0;
    public screenHeight: any;  
    testHeight = 250;
    constructor(
        injector: Injector,
        private _deliveryoptionServiceProxy: WholesaleDeliveryOptionServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService,
        private _router: Router,
        private titleService: Title
        ) {
            super(injector);
            this.titleService.setTitle(this.appSession.tenancyName + " |  Delivery Options");
    }
    searchLog() : void {
        
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Wholesale Delivery Options';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
       
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Wholesale Delivery Options';
        log.section = 'Wholesale Delivery Options';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
    }
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }
    getDeliveryOption(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._deliveryoptionServiceProxy.getAll(
            this.filterText,
           // this.nameFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            // result.items[0].deliveryoption.name
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    //createDeliveryOption(): void {
        // this._router.navigate(['/app/main/jobs/jobTypes/createOrEdit']);        

        //this.createOrEditJobTypeModal.show();
   // }

    showDeliveryModal(): void {
        this.DeliveryOptionModal.show();
     }
     createDeliveryOption(): void {
        this.CreateEditdeliveryoptionmodal.show();
     }     

     deleteDeliveryOption(DeliveryOption: DeliveryOptionDto): void {
        this.message.confirm(
            this.l('AreYouSureWanttoDelete'),
            this.l('Delete'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._deliveryoptionServiceProxy.delete(DeliveryOption.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete Wholesale Delivery Options: ' + DeliveryOption.name;
                            log.section = 'Wholesale Delivery Options';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });
                        });
                }
            }
        );
    }

    exportToExcel(): void {
        this._deliveryoptionServiceProxy.getDeliveryoptionToExcel(
            this.filterText,
            //this.nameFilter,
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }
}
