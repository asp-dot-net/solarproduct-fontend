 import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { WholesaleDeliveryOptionServiceProxy, CreateOrEditWholesaleDeliveryOptionDto } from '@shared/service-proxies/service-proxies';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { finalize, map, take } from 'rxjs/operators';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AuditLogListDto } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
@Component({
    selector: 'CreateEditDeliveryOptionModal',
    templateUrl: './create-edit-delivery-option-modal.html',
})
export class CreateEditDeliveryOptionModal extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    id? :number;
    wholesaledelivery:CreateOrEditWholesaleDeliveryOptionDto= new CreateOrEditWholesaleDeliveryOptionDto();
    constructor(
        injector: Injector,
        private _wholesaledeliveryServiceProxy: WholesaleDeliveryOptionServiceProxy ,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,

    ) {
        super(injector);
    }
    
    show(deliveryid?: number): void {
        if (!deliveryid) {
            this.wholesaledelivery = new CreateOrEditWholesaleDeliveryOptionDto();
            this.wholesaledelivery.id =deliveryid;

            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Wholesale Delivery Options';
            log.section = 'Wholesale Delivery Options';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            
        } else {
            this._wholesaledeliveryServiceProxy.getWholesaleDeliveryoptionForEdit(deliveryid).subscribe(result => {
                this.wholesaledelivery= result.deliveryoption;
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Wholesale Delivery Options : ' + this.wholesaledelivery.name;
                log.section = 'Wholesale Delivery Options';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        
    }
    onShown(): void {
        
        document.getElementById('Name').focus();
    }
    save(): void {
        this.saving = true;
        this._wholesaledeliveryServiceProxy.createOrEdit(this.wholesaledelivery)
         .pipe(finalize(() => { this.saving = false;}))
         .subscribe(() => {
            this.notify.info(this.l('SavedSuccessfully'));
             //result.items[0].stockfroms,
            this.close();
            this.modalSave.emit(null);
            if(this.wholesaledelivery.id){
                let log = new UserActivityLogDto();
                log.actionId = 82;
                log.actionNote ='Wholesale Delivery Options Updated : '+ this.wholesaledelivery.name;
                log.section = 'Wholesale Delivery Options';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }else{
                let log = new UserActivityLogDto();
                log.actionId = 81;
                log.actionNote ='Wholesale Delivery Options Created : '+ this.wholesaledelivery.name;
                log.section = 'Wholesale Delivery Options';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }

         });

    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
