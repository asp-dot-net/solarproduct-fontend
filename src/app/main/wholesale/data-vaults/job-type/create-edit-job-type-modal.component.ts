import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import {WholesaleJobTypeServiceProxy, CreateOrEditWholesaleJobTypeDto } from '@shared/service-proxies/service-proxies';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { finalize, map, take } from 'rxjs/operators';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AuditLogListDto } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
@Component({
    selector: 'CreateEditWholesaleJobTypeModal',
    templateUrl: './create-edit-job-type-modal.html',
})
export class CreateEditWholesaleJobTypeModal extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    saving = false;
    id? :number;
    wholesalesjobtypes:CreateOrEditWholesaleJobTypeDto= new CreateOrEditWholesaleJobTypeDto();
    constructor(
        injector: Injector,
        private _wholesalejobtypeServiceProxy: WholesaleJobTypeServiceProxy ,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }
    
    show(jobtypeid?: number): void {
        if (!jobtypeid) {
            this.wholesalesjobtypes = new CreateOrEditWholesaleJobTypeDto();
            this.wholesalesjobtypes.id =jobtypeid;
            this.active = true;
            this.modal.show();
            
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Wholesale Job Type';
            log.section = 'Wholesale Job Type';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._wholesalejobtypeServiceProxy.getWholesaleJobTypeForEdit(jobtypeid).subscribe(result => {
                this.wholesalesjobtypes= result.jobType;
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Wholesale Job Type : ' + this.wholesalesjobtypes.name;
                log.section = 'Wholesale Job Type';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });

            });
        }
        
    }
    onShown(): void {
        
        document.getElementById('Name').focus();
    }
    save(): void {
        this.saving = true;
        this._wholesalejobtypeServiceProxy.createOrEdit(this.wholesalesjobtypes)
         .pipe(finalize(() => { this.saving = false;}))
         .subscribe(() => {
            this.notify.info(this.l('SavedSuccessfully'));
            // result.items[0].wholesalesjobtypes.IsActive,
            this.close();
            this.modalSave.emit(null);
            if(this.wholesalesjobtypes.id){
                let log = new UserActivityLogDto();
                log.actionId = 82;
                log.actionNote ='Wholesale Job Type Updated : '+ this.wholesalesjobtypes.name;
                log.section = 'Wholesale Job Type';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }else{
                let log = new UserActivityLogDto();
                log.actionId = 81;
                log.actionNote ='Wholesale Job Type Created : '+ this.wholesalesjobtypes.name;
                log.section = 'Wholesale Job Type';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }
         });

    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}