import {AppConsts} from '@shared/AppConsts';
import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute , Router} from '@angular/router';
import { WholeSaleSmsTemplateDto, OrganizationUnitDto, UserServiceProxy, CommonLookupServiceProxy, WholeSaleSmsTemplatesServiceProxy  } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { CreateOrEditWholeSaleSmsTemplateModalComponent } from './create-or-edit-smsTemplate-modal.component';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { ViewWholeSaleSmsTemplateModalComponent } from './view-smsTemplate-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { Title } from '@angular/platform-browser';


@Component({
    templateUrl: './smsTemplates.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class WholeSaleSmsTemplatesComponent extends AppComponentBase {
    
    @ViewChild('createOrtWholeSaleEditSmsTemplateModal', { static: true }) createOrtWholeSaleEditSmsTemplateModal: CreateOrEditWholeSaleSmsTemplateModalComponent;
    @ViewChild('viewWholeSaleSmsTemplateModalComponent', { static: true }) viewWholeSaleSmsTemplateModal: ViewWholeSaleSmsTemplateModalComponent;   
    
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    allOrganizationUnits: OrganizationUnitDto[];

    organizationUnit = 0;
    organizationUnitlength = 0;
    firstrowcount = 0;
    last = 0;
    public screenHeight: any;  
    testHeight = 250;
    constructor(
        injector: Injector,
        private _wholeSalesmsTemplatesServiceProxy: WholeSaleSmsTemplatesServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService,
        private _userServiceProxy: UserServiceProxy,
        private titleService: Title
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  SMS Templates");
    }
   
    searchLog() : void {
        
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Wholesale Sms Template';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.getSmsTemplates();
        });
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Wholesale Sms Template';
        log.section = 'Wholesale Sms Template';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }
    getSmsTemplates(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._wholeSalesmsTemplatesServiceProxy.getAll(
            this.filterText,
            this.organizationUnit,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createSmsTemplate(): void {
        this.createOrtWholeSaleEditSmsTemplateModal.show();        
    }


    deleteSmsTemplate(smsTemplate: WholeSaleSmsTemplateDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._wholeSalesmsTemplatesServiceProxy.delete(smsTemplate.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete Wholesale Sms Template: ' + smsTemplate.name;
                            log.section = 'Wholesale Sms Template';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });

                        });
                }
            }
        );
    }

    exportToExcel(): void {
        this._wholeSalesmsTemplatesServiceProxy.getWholeSaleSmsTemplatesToExcel(
        this.filterText,
        )
        .subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
         });
    }
    
    
    
    
    
}
