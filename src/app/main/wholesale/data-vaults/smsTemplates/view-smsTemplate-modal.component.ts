import {AppConsts} from "@shared/AppConsts";
import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetSmsTemplateForViewDto, GetWholeSaleSmsTemplateForViewDto, SmsTemplateDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
@Component({
    selector: 'viewWholeSaleSmsTemplateModal',
    templateUrl: './view-smsTemplate-modal.component.html'
})
export class ViewWholeSaleSmsTemplateModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetWholeSaleSmsTemplateForViewDto;


    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
        this.item = new GetWholeSaleSmsTemplateForViewDto();
        this.item.wholeSaleSmsTemplate = new SmsTemplateDto();
    }

    show(item: GetWholeSaleSmsTemplateForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();

        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Opened Wholesale Sms Template View : ' + this.item.wholeSaleSmsTemplate.name;
        log.section = 'Wholesale Sms Template';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {            
         });
    }
    
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
