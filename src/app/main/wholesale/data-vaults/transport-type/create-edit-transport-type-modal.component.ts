import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { WholesaleTransportTypeServiceProxy, CreateOrEditWholesaleTransportTypeDto } from '@shared/service-proxies/service-proxies';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { finalize, map, take } from 'rxjs/operators';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AuditLogListDto } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
@Component({
    selector: 'CreateEditTransportTypeModal',
    templateUrl: './create-edit-transport-type-modal.html',
})
export class CreateEditTransportTypeModal extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    id? :number;
    wholesaletransporttype:CreateOrEditWholesaleTransportTypeDto= new CreateOrEditWholesaleTransportTypeDto();
    constructor(
        injector: Injector,
        private _wholesaletransporttypeServiceProxy: WholesaleTransportTypeServiceProxy ,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }
    
    show(transportid?: number): void {
        if (!transportid) {
            this.wholesaletransporttype = new CreateOrEditWholesaleTransportTypeDto();
            this.wholesaletransporttype.id =transportid;

            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Wholesale Transport Type';
            log.section = 'Wholesale Transport Type';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._wholesaletransporttypeServiceProxy.getWholesaleTransportTypeForEdit(transportid).subscribe(result => {
                this.wholesaletransporttype= result.transportType;
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Wholesale Transport Type : ' + this.wholesaletransporttype.name;
                log.section = 'Wholesale Transport Type';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        
    }
    onShown(): void {
        
        document.getElementById('Name').focus();
    }
    save(): void {
        this.saving = true;
        this._wholesaletransporttypeServiceProxy.createOrEdit(this.wholesaletransporttype)
         .pipe(finalize(() => { this.saving = false;}))
         .subscribe(() => {
            this.notify.info(this.l('SavedSuccessfully'));
             //result.items[0].stockfroms,
            this.close();
            this.modalSave.emit(null);
            if(this.wholesaletransporttype.id){
                let log = new UserActivityLogDto();
                log.actionId = 82;
                log.actionNote ='Wholesale Transport Type Updated : '+ this.wholesaletransporttype.name;
                log.section = 'Wholesale Transport Type';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }else{
                let log = new UserActivityLogDto();
                log.actionId = 81;
                log.actionNote ='Wholesale Transport Type Created : '+ this.wholesaletransporttype.name;
                log.section = 'Wholesale Transport Type';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }
         });

    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
