import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import {WholesalePVDStatusServiceProxy, CreateOrEditWholesalePVDStatusDto } from '@shared/service-proxies/service-proxies';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { finalize, map, take } from 'rxjs/operators';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
    selector: 'CreateOrEditPVDStatusModal',
    templateUrl: './create-or-edit-pvd-status-modal.html',
})
export class CreateOrEditPVDStatusModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    saving = false;
    id? :number;
    wholesalepvdstatus:CreateOrEditWholesalePVDStatusDto= new CreateOrEditWholesalePVDStatusDto();
    constructor(
        injector: Injector,
        private _wholesalespropertypeServiceProxy: WholesalePVDStatusServiceProxy ,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }
    
    show(pvdstatusid?: number): void {
        if (!pvdstatusid) {
            this.wholesalepvdstatus = new CreateOrEditWholesalePVDStatusDto();
            this.wholesalepvdstatus.id =pvdstatusid;
            
            

            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Wholesale PVD Status';
            log.section = 'Wholesale PVD Status';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });

        } else {
            this._wholesalespropertypeServiceProxy.getWholesalePVDStatusForEdit(pvdstatusid).subscribe(result => {
                this.wholesalepvdstatus= result.pvdStatus;
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Wholesale PVD Status : ' + this.wholesalepvdstatus.name;
                log.section = 'Wholesale PVD Status';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        
    }
    onShown(): void {
        
        document.getElementById('Name').focus();
    }
    save(): void {
        this.saving = true;
        this._wholesalespropertypeServiceProxy.createOrEdit(this.wholesalepvdstatus)
         .pipe(finalize(() => { this.saving = false;}))
         .subscribe(() => {
            this.notify.info(this.l('SavedSuccessfully'));
            // result.items[0].wholesalesjobtypes.IsActive,
            this.close();
            this.modalSave.emit(null);
            if(this.wholesalepvdstatus.id){
                let log = new UserActivityLogDto();
                log.actionId = 82;
                log.actionNote ='Wholesale PVD Status Updated : '+ this.wholesalepvdstatus.name;
                log.section = 'Wholesale PVD Status';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }else{
                let log = new UserActivityLogDto();
                log.actionId = 81;
                log.actionNote ='Wholesale PVD Status Created : '+ this.wholesalepvdstatus.name;
                log.section = 'Wholesale PVD Status';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }
         });

    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}