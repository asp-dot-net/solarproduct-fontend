import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import {WholesalePropertyTypeServiceProxy, CreateOrEditWholesalePropertyTypeDto } from '@shared/service-proxies/service-proxies';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { finalize, map, take } from 'rxjs/operators';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AuditLogListDto } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'CreateOrEditPropertyTypeModal',
    templateUrl: './create-or-edit-property-type-modal.html',
})
export class CreateOrEditPropertyModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    saving = false;
    id? :number;
    wholesalespropertytypes:CreateOrEditWholesalePropertyTypeDto= new CreateOrEditWholesalePropertyTypeDto();
    constructor(
        injector: Injector,
        private _wholesalespropertypeServiceProxy: WholesalePropertyTypeServiceProxy ,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }
    
    show(propertytypeid?: number): void {
        if (!propertytypeid) {
            this.wholesalespropertytypes = new CreateOrEditWholesalePropertyTypeDto();
            this.wholesalespropertytypes.id =propertytypeid;
            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Wholesale Property Type';
            log.section = 'Wholesale Property Type';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._wholesalespropertypeServiceProxy.getWholesalePropertyTypeForEdit(propertytypeid).subscribe(result => {
                this.wholesalespropertytypes= result.propertyType;
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Wholesale Property Type : ' + this.wholesalespropertytypes.name;
                log.section = 'Wholesale Property Type';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });

            });
        }
        
    }
    onShown(): void {
        
        document.getElementById('Name').focus();
    }
    save(): void {
        this.saving = true;
        this._wholesalespropertypeServiceProxy.createOrEdit(this.wholesalespropertytypes)
         .pipe(finalize(() => { this.saving = false;}))
         .subscribe(() => {
            this.notify.info(this.l('SavedSuccessfully'));
            // result.items[0].wholesalesjobtypes.IsActive,
            this.close();
            this.modalSave.emit(null);
            if(this.wholesalespropertytypes.id){
                let log = new UserActivityLogDto();
                log.actionId = 82;
                log.actionNote ='Wholesale Property Type Updated : '+ this.wholesalespropertytypes.name;
                log.section = 'Wholesale Property Type';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }else{
                let log = new UserActivityLogDto();
                log.actionId = 81;
                log.actionNote ='Wholesale Property Type Created : '+ this.wholesalespropertytypes.name;
                log.section = 'Wholesale Property Type';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }
         });

    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}