import { Component, Injector, ViewChild, ViewEncapsulation, HostListener } from '@angular/core';
import {  CommonLookupServiceProxy, LeadsServiceProxy,   OrganizationUnitDto, SmsReplyDto, TokenAuthServiceProxy, UserServiceProxy, WholeSaleActivityServiceProxy } from '@shared/service-proxies/service-proxies';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import * as _ from 'lodash';
import * as moment from 'moment';
import { HttpClient } from '@angular/common/http';
import { OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { Title } from '@angular/platform-browser';
import { ViewMyLeadComponent } from '@app/main/myleads/myleads/view-mylead.component';
import { ViewWholesaleLeadComponent } from '../leads/view-wholesalelead.component';

@Component({
    selector: 'app-sms-reply',
    templateUrl: './wholesale-sms-reply.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class WholesaleSmsReplyComponent extends AppComponentBase implements OnInit {
    
    saving = false;
    show: boolean = true;
    readunreadsmstag = 0;
    showchild: boolean = true;
    shouldShow: boolean = false;
    organizationUnitlength: number=0;
    excelorcsvfile = 0;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    // @ViewChild('viewsmsreposnceBodymodel', { static: true }) viewsmsreposnceBodymodel: ViewSMSBodyComponent;
    @ViewChild('ViewWholesaleLeadDetail', { static: true }) ViewWholesaleLeadDetail: ViewWholesaleLeadComponent;

    advancedFiltersAreShown = false;
    filter = '';
    tableRecords: any;
    copanyNameFilter = '';
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnit = 0;
    sampleDateRange: moment.Moment[] = [moment().add(-7, 'days').endOf('day'), moment().startOf('day')];
    dateNameFilter = '';
    StartDate: moment.Moment;
    EndDate: moment.Moment;
    markstatusid = 2;
    firstrowcount = 0;
    last = 0;
    olddataresult: SmsReplyDto[];
    datalenght = 0;
    FiltersData = false;
    ExpandedView: boolean = true;
    SMSExpandedView: boolean = true;
    LeadExpandedView: boolean = true;
    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 250;
    toggle: boolean = true;
    filterName = "MobileNo";
    orgCode = '';

    change() {
        this.toggle = !this.toggle;
      }
    constructor(
        injector: Injector,
        private _wholeSaleActivityServiceProxy: WholeSaleActivityServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _httpClient: HttpClient,
        private _router: Router,
        private titleService: Title
        ) {
            super(injector);
            this.titleService.setTitle(this.appSession.tenancyName + " |  SMS");
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            debugger;
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.orgCode = this.allOrganizationUnits[0].code;
            this.getSmsReply();

        });
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Wholesale SMS';
        log.section = 'Wholesale SMS';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
    }

    changeOrgCode() {
        this.orgCode = this.allOrganizationUnits.find(x => x.id == this.organizationUnit).code;
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + 82 ;
        }
        else {
            this.testHeight = this.testHeight - 82 ;
        }
    }

    clear() {
        this.filter = '';
        this.sampleDateRange = [moment().add(-7, 'days').endOf('day'), moment().startOf('day')];
        this.getSmsReply();
    }

   

    getSmsReply(event?: LazyLoadEvent) {
        debugger;
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.StartDate = this.sampleDateRange[0];
        this.EndDate = this.sampleDateRange[1];
        this.organizationUnit,
        this.primengTableHelper.showLoadingIndicator();
        var filterText_ = this.filter;
        // if(this.filterName == 'JobNumber' &&  this.filter != null && this.filter.trim() != ''){
        //     filterText_ = this.orgCode + this.filter.trim();
        // }
        this._wholeSaleActivityServiceProxy.getSmsReplyList(
            this.filter,
            this.StartDate,
            this.EndDate,
            this.organizationUnit,
            this.markstatusid,
            filterText_,
            this.dateNameFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.tableRecords = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            this.shouldShow = false;
            
        });
    }

    getOldSmsReply(id,leadid)
    {
        
        this.primengTableHelper.showLoadingIndicator();
        this._wholeSaleActivityServiceProxy.getOldSmsReplyList(id,leadid,this.organizationUnit).subscribe(result => {
           
            this.primengTableHelper.hideLoadingIndicator();
            this.LeadExpandedView = true;
            this.olddataresult = result;
            this.datalenght = result.length;
            
            this.ExpandedView = false;
            this.SMSExpandedView = !this.SMSExpandedView;
            this.SMSExpandedView = false;

            this.shouldShow = true;
            
            // this.ExpandedView = !this.ExpandedView;
            
            // this.ExpandedView1 = true;
        }, e => {
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    expandGrid() {
        this.ExpandedView = true;
        this.SMSExpandedView = true;
        this.LeadExpandedView = true;
        // this.ExpandedView2 = true;
    }

    exportToExcel(): void {
      
    }

    mark(id :number): void {
        this.message.confirm(
            this.l('AreYouSureWanttoMark'),
            this.l('Mark'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._wholeSaleActivityServiceProxy.markReadSms(id)
                        .subscribe(() => {
                            this.getSmsReply()
                            this.notify.success(this.l('SuccessfullyMark'));
                        });
                }
            }
        );
    }

    reloadPage(event): void {
        this.ExpandedView = true;
        this.SMSExpandedView = true;
        this.paginator.changePage(this.paginator.getPage());
    }

    checkAll(ev) {
        
        this.tableRecords.forEach(x => x.isSelected = ev.target.checked);
    }

    isAllChecked() {
        if (this.tableRecords)
            return this.tableRecords.every(_ => _.isSelected);
    }

    submit(): void {
        
        let selectedids = [];
        this.saving = true;
        this.primengTableHelper.records.forEach(function (record) {
            if (record.isSelected) {
                selectedids.push(record.leadId);
            }
        });
        if (selectedids.length == 0) {
            this.notify.warn(this.l('NoDataSelected'));
            this.saving = false;
            
            return;
        }
        if (this.readunreadsmstag == 0) {
            this.notify.warn("PleaseSelectValue");
            this.saving = false;
            return;
        }
        this._wholeSaleActivityServiceProxy.markReadSmsInBulk(this.readunreadsmstag, selectedids)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.readunreadsmstag = 0;
                this.reloadPage(true);
                this.saving = false;
                this.notify.info(this.l('SuccessfullyMark'));
        });
    }

    SelectedLeadId: number = 0;
   

    navigateToLeadDetail(leadid): void {
        this.ExpandedView = false;

        this.SMSExpandedView = true;

        this.SelectedLeadId = leadid;
        this.ViewWholesaleLeadDetail.showDetail(leadid, "wholesaleLead", 13);
        this.LeadExpandedView = false;
    }
}
