import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CreditSMSEmailModalComponent } from './credit-sms-email-modal.component';

describe('WholeSaleSMSEmailModalComponent', () => {
  let component: CreditSMSEmailModalComponent;
  let fixture: ComponentFixture<CreditSMSEmailModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreditSMSEmailModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditSMSEmailModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
