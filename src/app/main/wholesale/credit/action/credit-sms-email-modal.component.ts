import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { LeadsServiceProxy, WholeSaleActivityServiceProxy, WholeSaleSmsEmailDto, WholeSaleLeadServiceProxy, CommonLookupDto, CreateOrEditWholeSaleLeadDto } from '@shared/service-proxies/service-proxies';
import { EmailEditorComponent } from 'angular-email-editor';
import * as _ from 'lodash';
import { result } from 'lodash';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'wholeSaleSMSEmailModal',
    templateUrl: './sms-email-modal.component.html',
})
export class CreditSMSEmailModalComponent extends AppComponentBase {
    @ViewChild('LeadSMSModal', { static: true }) modal: ModalDirective;
    
    @ViewChild(EmailEditorComponent)
    private emailEditor: EmailEditorComponent;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    ExpandedViewApp: boolean = true;
    active = false;    
    activityLog: WholeSaleSmsEmailDto = new WholeSaleSmsEmailDto();
    saving = false;
    role: string = '';
    total = 0;
    credit = 0;
    //allSMSTemplates: CommonLookupDto[];
    leadCompanyName: any;
    activityName = '';
    fromemails: CommonLookupDto[];
    emailFrom : any;
    allEmailTemplates: CommonLookupDto[];
    wholeSalelead : CreateOrEditWholeSaleLeadDto;
    ccbox = false;
    bccbox = false;
    emailData = '';
    smsData = '';
    templateBody : any;

    constructor(injector: Injector
        , private _dateTimeService: DateTimeService
        , private _wholeSaleActivityServiceProxy : WholeSaleActivityServiceProxy
        , private _leadsServiceProxy : LeadsServiceProxy
        , private _wholeSaleLeadServiceProxy : WholeSaleLeadServiceProxy,
        private sanitizer: DomSanitizer
        ) {
        super(injector);
        this.wholeSalelead = new CreateOrEditWholeSaleLeadDto();
        this.activityLog = new WholeSaleSmsEmailDto();
        this.activityLog.emailTemplateId = 0;
    }

    show(applicationId : number,  type : number) {
        this.activityLog.wholeSaleLeadId = applicationId;
        //this.activityLog.sectionId = sectionId;
        this.activityLog.body = "";
        // this._wholeSaleLeadServiceProxy.getallSMSTemplates(wholeSaleLeadID).subscribe(result => {
        //     this.allSMSTemplates = result;
        //   });

        // this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
        //     this.role = result;
        //   });

        // this._wholeSaleLeadServiceProxy.getWholeSaleLeadForEdit(wholeSaleLeadID).subscribe(result => {
        //     this.wholeSalelead = result;
            
        // });
        // this._wholeSaleLeadServiceProxy.getallEmailTemplates(wholeSaleLeadID).subscribe(result => {
        //   this.allEmailTemplates = result;
        // });
    
        if (type == 1) {
          this.activityName = "SMS";
        }
        else if (type == 2) {
          this.activityName = "Email";
        }

        // this._wholeSaleLeadServiceProxy.getOrgWiseDefultandownemailadd(wholeSaleLeadID).subscribe(result => {
        //   debugger;
        //   this.fromemails = result;
        //   this.activityLog.emailFrom = this.fromemails[0].displayName;
        // });

        this.modal.show();        
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;                
    }
    expandApp() {
        this.ExpandedViewApp = false;
    }

    save(){
      this.saving = true;
      if(this.activityName == "SMS"){
        if (this.role != 'Admin') {
          if (this.total > 320) {
            this.notify.warn(this.l('You Can Not Add more than 320 characters'));
            this.saving = false;
          } else {
            this._wholeSaleActivityServiceProxy.sendSms(this.activityLog)
              .pipe(finalize(() => { this.saving = false; }))
              .subscribe(() => {
                this.modal.hide();
                this.notify.info(this.l('SmsSendSuccessfully'));
                this.modalSave.emit(null);
                this.activityLog.body = "";
                this.activityLog.emailTemplateId = 0;
                this.activityLog.smsTemplateId = 0;
                this.activityLog.customeTagsId = 0;
                this.activityLog.subject = '';
              });
          }
        }
        else {
          this._wholeSaleActivityServiceProxy.sendSms(this.activityLog)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
              this.modal.hide();
              this.notify.info(this.l('SmsSendSuccessfully'));
              this.modalSave.emit(null);
              this.activityLog.body = "";
              this.activityLog.emailTemplateId = 0;
              this.activityLog.smsTemplateId = 0;
              this.activityLog.customeTagsId = 0;
              this.activityLog.subject = '';
            });
        }
      }
      else if(this.activityName == "Email"){
        this._wholeSaleActivityServiceProxy.sendEmail(this.activityLog)
        .pipe(finalize(() => { this.saving = false; }))
        .subscribe(() => {
          this.notify.info(this.l('EmailSendSuccessfully'));
          this.modal.hide();
          this.modalSave.emit(null);
          this.activityLog.body = "";
          this.activityLog.emailTemplateId = 0;
          this.activityLog.smsTemplateId = 0;
          this.activityLog.customeTagsId = 0;
          this.activityLog.subject = '';
        });
      }
      
    }
    
    countCharcters(): void {

        if (this.role != 'Admin') {
          this.total = this.activityLog.body.length;
          this.credit = Math.ceil(this.total / 160);
          if (this.total > 320) {
            this.notify.warn(this.l('You Can Not Add more than 320 characters'));
          }
        }
        else {
          this.total = this.activityLog.body.length;
          this.credit = Math.ceil(this.total / 160);
        }
    }

    opencc(): void {
      this.ccbox = !this.ccbox;
    }
    openbcc(): void {
      this.bccbox = !this.bccbox;
    }
  
    // onTagChange(event): void {
  
    //   const id = event.target.value;
    //   if (id == 1) {
    //     if (this.activityLog.body != null) {
    //       this.activityLog.body = this.activityLog.body + " " + "{{Customer.Name}}";
    //     } else {
    //       this.activityLog.body = "{{Customer.Name}}";
    //     }
  
    //   } else if (id == 2) {
    //     if (this.activityLog.body != null) {
    //       this.activityLog.body = this.activityLog.body + " " + "{{Customer.Mobile}}";
    //     } else {
    //       this.activityLog.body = "{{Customer.Mobile}}";
    //     }
    //   } else if (id == 3) {
    //     if (this.activityLog.body != null) {
    //       this.activityLog.body = this.activityLog.body + " " + "{{Customer.Phone}}";
    //     } else {
    //       this.activityLog.body = "{{Customer.Phone}}";
    //     }
    //   } else if (id == 4) {
    //     if (this.activityLog.body != null) {
    //       this.activityLog.body = this.activityLog.body + " " + "{{Customer.Email}}";
    //     } else {
    //       this.activityLog.body = "{{Customer.Email}}";
    //     }
    //   } else if (id == 5) {
  
    //     if (this.activityLog.body != null) {
    //       this.activityLog.body = this.activityLog.body + " " + "{{Customer.Address}}";
    //     } else {
    //       this.activityLog.body = "{{Customer.Address}}";
    //     }
    //   } else if (id == 6) {
    //     if (this.activityLog.body != null) {
    //       this.activityLog.body = this.activityLog.body + " " + "{{Sales.Name}}";
    //     } else {
    //       this.activityLog.body = "{{Sales.Name}}";
    //     }
    //   } else if (id == 7) {
    //     if (this.activityLog.body != null) {
    //       this.activityLog.body = this.activityLog.body + " " + "{{Sales.Mobile}}";
    //     } else {
    //       this.activityLog.body = "{{Sales.Mobile}}";
    //     }
    //   } else if (id == 8) {
    //     if (this.activityLog.body != null) {
    //       this.activityLog.body = this.activityLog.body + " " + "{{Sales.Email}}";
    //     } else {
    //       this.activityLog.body = "{{Sales.Email}}";
    //     }
    //   }
    //   else if (id == 9) {
    //     if (this.activityLog.body != null) {
    //       this.activityLog.body = this.activityLog.body + " " + "{{Quote.ProjectNo}}";
    //     } else {
    //       this.activityLog.body = "{{Quote.ProjectNo}}";
    //     }
    //   }
    //   else if (id == 10) {
    //     if (this.activityLog.body != null) {
    //       this.activityLog.body = this.activityLog.body + " " + "{{Quote.SystemCapacity}}";
    //     } else {
    //       this.activityLog.body = "{{Quote.SystemCapacity}}";
    //     }
    //   }
    //   else if (id == 11) {
    //     if (this.activityLog.body != null) {
    //       this.activityLog.body = this.activityLog.body + " " + "{{Quote.AllproductItem}}";
    //     } else {
    //       this.activityLog.body = "{{Quote.AllproductItem}}";
    //     }
    //   }
    //   else if (id == 12) {
    //     if (this.activityLog.body != null) {
    //       this.activityLog.body = this.activityLog.body + " " + "{{Quote.TotalQuoteprice}}";
    //     } else {
    //       this.activityLog.body = "{{Quote.TotalQuoteprice}}";
    //     }
    //   }
    //   else if (id == 13) {
    //     if (this.activityLog.body != null) {
    //       this.activityLog.body = this.activityLog.body + " " + "{{Quote.InstallationDate}}";
    //     } else {
    //       this.activityLog.body = "{{Quote.InstallationDate}}";
    //     }
    //   }
    //   else if (id == 14) {
    //     if (this.activityLog.body != null) {
    //       this.activityLog.body = this.activityLog.body + " " + "{{Quote.InstallerName}}";
    //     } else {
    //       this.activityLog.body = "{{Quote.InstallerName}}";
    //     }
    //   }
    //   else if (id == 15) {
    //     if (this.activityLog.body != null) {
    //       this.activityLog.body = this.activityLog.body + " " + "{{Freebies.TransportCompany}}";
    //     } else {
    //       this.activityLog.body = "{{Freebies.TransportCompany}}";
    //     }
    //   }
    //   else if (id == 16) {
    //     if (this.activityLog.body != null) {
    //       this.activityLog.body = this.activityLog.body + " " + "{{Freebies.TransportLink}}";
    //     } else {
    //       this.activityLog.body = "{{Freebies.TransportLink}}";
    //     }
    //   }
    //   else if (id == 17) {
    //     if (this.activityLog.body != null) {
    //       this.activityLog.body = this.activityLog.body + " " + "{{Freebies.DispatchedDate}}";
    //     } else {
    //       this.activityLog.body = "{{Freebies.DispatchedDate}}";
    //     }
    //   }
    //   else if (id == 18) {
    //     if (this.activityLog.body != null) {
    //       this.activityLog.body = this.activityLog.body + " " + "{{Freebies.TrackingNo}}";
    //     } else {
    //       this.activityLog.body = "{{Freebies.TrackingNo}}";
    //     }
    //   }
    //   else if (id == 19) {
    //     if (this.activityLog.body != null) {
    //       this.activityLog.body = this.activityLog.body + " " + "{{Freebies.PromoType}}";
    //     } else {
    //       this.activityLog.body = "{{Freebies.PromoType}}";
    //     }
    //   }
    //   else if (id == 20) {
    //     if (this.activityLog.body != null) {
    //       this.activityLog.body = this.activityLog.body + " " + "{{Invoice.UserName}}";
    //     } else {
    //       this.activityLog.body = "{{Invoice.UserName}}";
    //     }
    //   }
    //   else if (id == 21) {
    //     if (this.activityLog.body != null) {
    //       this.activityLog.body = this.activityLog.body + " " + "{{Invoice.UserMobile}}";
    //     } else {
    //       this.activityLog.body = "{{Invoice.UserMobile}}";
    //     }
    //   }
    //   else if (id == 22) {
    //     if (this.activityLog.body != null) {
    //       this.activityLog.body = this.activityLog.body + " " + "{{Invoice.UserEmail}}";
    //     } else {
    //       this.activityLog.body = "{{Invoice.UserEmail}}";
    //     }
    //   }
    //   else if (id == 23) {
    //     if (this.activityLog.body != null) {
    //       this.activityLog.body = this.activityLog.body + " " + "{{Invoice.Owning}}";
    //     } else {
    //       this.activityLog.body = "{{Invoice.Owning}}";
    //     }
    //   }
    //   else if (id == 24) {
    //     if (this.activityLog.body != null) {
    //       this.activityLog.body = this.activityLog.body + " " + "{{Organization.orgName}}";
    //     } else {
    //       this.activityLog.body = "{{Organization.orgName}}";
    //     }
    //   }
    //   else if (id == 25) {
    //     if (this.activityLog.body != null) {
    //       this.activityLog.body = this.activityLog.body + " " + "{{Organization.orgEmail}}";
    //     } else {
    //       this.activityLog.body = "{{Organization.orgEmail}}";
    //     }
    //   }
    //   else if (id == 26) {
    //     if (this.activityLog.body != null) {
    //       this.activityLog.body = this.activityLog.body + " " + "{{Organization.orgMobile}}";
    //     } else {
    //       this.activityLog.body = "{{Organization.orgMobile}}";
    //     }
    //   }
    //   else if (id == 27) {
    //     if (this.activityLog.body != null) {
    //       this.activityLog.body = this.activityLog.body + " " + "{{Organization.orglogo}}";
    //     } else {
    //       this.activityLog.body = "{{Organization.orglogo}}";
    //     }
    //   }
    //   else if (id == 28) {
    //     if (this.activityLog.body != null) {
    //       this.activityLog.body = this.activityLog.body + " " + "{{Review.link}}";
    //     } else {
    //       this.activityLog.body = "{{Review.link}}";
    //     }
    //   }
    // }
smseditorLoaded() {

    this.activityLog.body = "";
    if ((this.activityLog.smsTemplateId != 0 && this.activityLog.smsTemplateId !== null && this.activityLog.smsTemplateId !== undefined)) {
      this._wholeSaleLeadServiceProxy.getSmsTemplateForEditForSms(this.activityLog.smsTemplateId).subscribe(result => {
        this.smsData = result.wholeSaleSmsTemplate.text;
        if (this.smsData != "") {
          this.setsmsHTML(this.smsData)
        }
      });
    }

  }
    editorLoaded() {

      // this.activityLog.body = "";
      // if ((this.activityLog.emailTemplateId != 0 && this.activityLog.emailTemplateId !== null && this.activityLog.emailTemplateId !== undefined)) {
      //   this._wholeSaleLeadServiceProxy.getEmailTemplateForEditForEmail(this.activityLog.emailTemplateId).subscribe(result => {
      //     //this.activityLog.subject = result.emailTemplate.subject;
      //     this.emailData = result.emailTemplateDto.viewHtml;
      //     if (this.emailData != "") {
      //       this.emailData = this.getEmailTemplate(this.emailData);
      //       this.emailEditor.editor.loadDesign(JSON.parse(this.emailData));
      //     }
      //   });
      // }
      if ((this.activityLog.emailTemplateId != 0 && this.activityLog.emailTemplateId !== null && this.activityLog.emailTemplateId !== undefined)) {
        this._wholeSaleLeadServiceProxy.getEmailTemplateForEditForEmail(this.activityLog.emailTemplateId).subscribe(result => {


          let htmlTemplate = this.getEmailTemplate(result.emailTemplateDto.viewHtml)
          this.templateBody = htmlTemplate;
          this.templateBody = this.sanitizer.bypassSecurityTrustHtml(this.templateBody);
        });
      }
      
  
    }

    setsmsHTML(smsHTML) {
      let htmlTemplate = this.getsmsTemplate(smsHTML);
      this.activityLog.body = htmlTemplate;
      this.countCharcters();
    }
  
    getsmsTemplate(smsHTML) {
      //let myTemplateStr = "Hello {{user.name}}, you are {{user.age.years}} years old";
      let myTemplateStr = smsHTML;
      let addressValue = this.wholeSalelead.postalAddress +  ", " + this.wholeSalelead.postalState + "-" + this.wholeSalelead.postalPostCode;
      let myVariables = {
        Customer: {
          Name: this.wholeSalelead.firstName + " " +this.wholeSalelead.lastName ,
          CompanyName : this.wholeSalelead.companyName,
          Address: addressValue,
          Mobile: this.wholeSalelead.mobile,
          Email: this.wholeSalelead.email,
          Phone: this.wholeSalelead.talephone
        },
        Organization: {
          orgName: this.wholeSalelead.orgName,
          orgEmail: this.wholeSalelead.orgEmail,
          orgMobile: this.wholeSalelead.orgMobile,
          orglogo: AppConsts.docUrl + "/" + this.wholeSalelead.orgLogo,
        },
        Sales:{
          Name : this.wholeSalelead.salesRapName,
          Mobile : this.wholeSalelead.salesRapMobile,
          Email :  this.wholeSalelead.salesRapEmail,
        },
        Invoice : {
          UserName : this.wholeSalelead.userName,
          UserMobile : this.wholeSalelead.userMobile,
          UserEmail : this.wholeSalelead.userEmail,
          Owning : this.wholeSalelead.owning,
        },
        // Review: {
        //   link: 'https://g.page/r/cw-phi-vkpsjeag/review',
        // },
      }
  
      // use custom delimiter {{ }}
      _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;
  
      // interpolate
      let compiled = _.template(myTemplateStr);
      let myTemplateCompiled = compiled(myVariables);
      return myTemplateCompiled;
    }

    getEmailTemplate(emailHTML) {
      //let myTemplateStr = "Hello {{user.name}}, you are {{user.age.years}} years old";
      let myTemplateStr = emailHTML;
      let addressValue = this.wholeSalelead.postalAddress +  ", " + this.wholeSalelead.postalState + "-" + this.wholeSalelead.postalPostCode;

      let myVariables = {
        Customer: {
          Name: this.wholeSalelead.firstName + " " +this.wholeSalelead.lastName ,
          CompanyName : this.wholeSalelead.companyName,
          Address: addressValue,
          Mobile: this.wholeSalelead.mobile,
          Email: this.wholeSalelead.email,
          Phone: this.wholeSalelead.talephone
        },
        Organization: {
          orgName: this.wholeSalelead.orgName,
          orgEmail: this.wholeSalelead.orgEmail,
          orgMobile: this.wholeSalelead.orgMobile,
          orglogo: AppConsts.docUrl + "/" + this.wholeSalelead.orgLogo,
        },
        Sales:{
          Name : this.wholeSalelead.salesRapName,
          Mobile : this.wholeSalelead.salesRapMobile,
          Email :  this.wholeSalelead.salesRapEmail,
        },
        Invoice : {
          UserName : this.wholeSalelead.userName,
          UserMobile : this.wholeSalelead.userMobile,
          UserEmail : this.wholeSalelead.userEmail,
          Owning : this.wholeSalelead.owning,
        },
        // Review: {
        //   link: 'https://g.page/r/cw-phi-vkpsjeag/review',
        // },
      }
  
      // use custom delimiter {{ }}
      _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;
  
      // interpolate
      let compiled = _.template(myTemplateStr);
      let myTemplateCompiled = compiled(myVariables);
      return myTemplateCompiled;
    }

    saveDesign() {
      this.saving = true;
      if (this.activityLog.emailTemplateId == 0) {
        const emailHTML = this.activityLog.body;
        this.setHTML(emailHTML)
      }
      else {
        this.activityLog.body = this.templateBody.changingThisBreaksApplicationSecurity;
        this.save();
      }
    }
  
    setHTML(emailHTML) {
      let htmlTemplate = this.getEmailTemplate(emailHTML);
      this.activityLog.body = htmlTemplate;
      this.save();
    }
}
