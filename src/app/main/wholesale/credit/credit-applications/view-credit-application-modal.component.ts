import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { BuisnessServiceProxy, BusinessDto, CreateOrEditWholeSaleLeadDocumentTypeDto, WholeSaleLeadDocumentTypeServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
@Component({
    selector: 'ViewCreditApplicationModal',
    templateUrl: './view-credit-application-modal.component.html',
})
export class ViewCreditApplicationModalComponent extends AppComponentBase {

    activeTabIndex: number = (abp.clock.provider.supportsMultipleTimezone) ? 0 : 1;

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    Business: BusinessDto = new BusinessDto();

    constructor(
        injector: Injector,
        private _buisnessServiceProxy : BuisnessServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,

    ) {
        super(injector);
    }

    show(BusinessId?: number,type? : string): void {
        debugger;
        if (!BusinessId) {
            this.Business = new BusinessDto();
            this.Business.id = BusinessId;
            this.active = true;
            this.modal.show();
        } else {
            this._buisnessServiceProxy.getBusinessForView(BusinessId,type).subscribe(result => {
                this.Business = result;
                this.active = true;
                this.modal.show();
            });
        }
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Opened Credit Applications View : ' + this.Business.buisnessStructure;
        log.section = 'Credit Applications';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {            
         });
    }

    onShown(): void {
        
        //document.getElementById('Department_Name').focus();
    }

    // save(): void {
    //     this.saving = true;


    //     this._WholeSaleLeadDocumentTypeServiceProxy.createOrEdit(this.Business)
    //         .pipe(finalize(() => { this.saving = false; }))
    //         .subscribe(() => {
    //             this.notify.info(this.l('SavedSuccessfully'));
    //             this.close();
    //             this.modalSave.emit(null);
    //         });
    // }
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
