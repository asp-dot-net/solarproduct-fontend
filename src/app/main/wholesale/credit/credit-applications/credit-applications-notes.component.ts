import { EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { BuisnessServiceProxy, BusinessDto, CreateOrEditJobInstallerInvoiceDto, JobInstallerInvoicesServiceProxy } from '@shared/service-proxies/service-proxies';
import * as moment from 'moment';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/internal/operators/finalize';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
@Component({
  selector: 'CreditApplicationNotesModal',
  templateUrl: './credit-applications-notes.component.html',
//   styleUrls: ['./readyTopay.component.css']
})
export class CreditApplicationNotesModalComponent extends AppComponentBase {


    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    saving = false;
    distAppliedDate: moment.Moment;
    MeterNumber = "";
    ApprovalRefNo = "";
    item: BusinessDto = new BusinessDto();
    prorityId = 0;
    comment = "";
    remarks: string;
    date: moment.Moment;

    constructor(
        injector: Injector,
        private _buisnessServiceProxy: BuisnessServiceProxy,
        private _router: Router
    ) {
        super(injector);
    }
    
    


    show(InstallerInvoiceId?: number, sectionId?: number): void {
        
        this.showMainSpinner();
        const date = new Date();
        this._buisnessServiceProxy.getBusinessForEdit(InstallerInvoiceId).subscribe(result => {
            this.item = new BusinessDto();
            this.item = result;
            //this.item.approedDate = moment(date);
            // this.prorityId = this.item.priorityId;
            // this.item.sectionId = sectionId;
            this.modal.show();
            this.hideMainSpinner();
        }, e => {
            this.hideMainSpinner();
        });
    }

    save(): void {
        this.saving = true;
        // this.item.isPaid = true;
        // this.item.priorityId = this.prorityId;
       
        this._buisnessServiceProxy.approveAmount(this.item)
        .pipe(finalize(() => { this.saving = false;}))
        .subscribe(() => {
            this.modal.hide();
            // this.remarks = "";
            // this.date = null;
            this.notify.info(this.l('SavedSuccessfully'));
            this.modalSave.emit(null);
            
        });
    }

    close(): void {
        this.modal.hide();
    }
}
