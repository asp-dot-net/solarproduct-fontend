﻿import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PromotionsServiceProxy, PromotionDto, JobsServiceProxy, UserServiceProxy, OrganizationUnitDto, PromotionUsersServiceProxy, PromotionUserPromotionLookupTableDto, CommonLookupServiceProxy, WholeSalePromotionsServiceProxy } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { CreateOrEditWhlPromotionComponent } from './create-or-edit-promotion.component';
import { ViewWholeSalePromotionModalComponent } from './view-promotion-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { Title } from '@angular/platform-browser';
import { WholeSaleActivityLogDetailComponent } from '../../leads/wholesale-activity-log-detail.component';

@Component({
    templateUrl: './promotions.component.html',
    styleUrls: ['./promotions.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class WholesalePromotionsComponent extends AppComponentBase {
    FiltersData = false;
    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    organizationUnitlength: number = 0;
    promotype = 0;

    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    toggle: boolean = true;

    change() {
        this.toggle = !this.toggle;
      }

    @ViewChild('CreateOrEditWhlPromotionComponent', { static: true }) CreateOrEditWhlPromotionComponent: CreateOrEditWhlPromotionComponent;
     @ViewChild('viewWholeSalePromotionModal', { static: true }) viewWholeSalePromotionModal: ViewWholeSalePromotionModalComponent;
     @ViewChild('wholeSaleactivityLogDetail', { static: true }) wholeSaleactivityLogDetail: WholeSaleActivityLogDetailComponent;

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    titleFilter = '';
    promoChargeFilter = "0";
    descriptionFilter = '';
    promotionTypeNameFilter = '';
    FromDateFilter: moment.Moment;
    ToDateFilter: moment.Moment;

    totalPromotion = 0;
    interestedCount = 0;
    notInterestedCount = 0;
    otherCount = 0;
    unhandle = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnit = 0;
    promotionSold = 0;
    promotionProject = 0;
    noReplay = 0;
    sampleDateRange: moment.Moment[];
    promotionTitleFilter = 0;
    totalSendPromotion = 0;
    allPromotions: PromotionUserPromotionLookupTableDto[];
    pertotalPromotion = 0;
    perinterestedCount = 0;
    pernotInterestedCount = 0;
    perotherCount = 0;
    perunhandle =  0;
    perpromotionSold =  0;
    perpromotionProject = 0;
    pernoReplay =  0;
    firstrowcount = 0;
    last = 0;
    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 330;
    date = new Date();
    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);
    constructor(
        injector: Injector,
        private _promotionsServiceProxy: WholeSalePromotionsServiceProxy,
        private _notifyService: NotifyService,
        private _commonLookupService: CommonLookupServiceProxy,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService,     
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _router: Router,
        private _promotionUsersServiceProxy: PromotionUsersServiceProxy,
        private titleService: Title
        ) {
            super(injector);
            this.titleService.setTitle(this.appSession.tenancyName + " |  Promotions");
    }

   
    searchLog() : void {
        
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Wholesale Promotions';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.getPromotions();
            // this.getCount(this.organizationUnit);
        });
        this._promotionUsersServiceProxy.getAllPromotionForTableDropdown().subscribe(result => {
            this.allPromotions = result;
        });
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Wholesale Promotions';
        log.section = 'Wholesale Promotions';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }

        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }

    createOrEditPromotion(organizationUnit): void {
        this._router.navigate(['/app/main/wholesale/promotions/promotions/app-create-or-edit-promotion']);
    }
    getPromotions(event?: LazyLoadEvent) {
        debugger;
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._promotionsServiceProxy.getAll(
            this.filterText,
            this.titleFilter,
            undefined,
            this.descriptionFilter,
            undefined,
            undefined,
            this.startDate,
            this.endDate,
            this.promotionTypeNameFilter,
            this.organizationUnit, this.promotionTitleFilter, this.promotype,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            this.shouldShow = false;
            if (result.totalCount > 0) {
                debugger;
                this.totalPromotion = result.items[0].totalWholeSalePromotion;
                this.interestedCount = result.items[0].summaryInterestedCount;
                this.notInterestedCount = result.items[0].summaryNotInterestedCount;
                this.otherCount = result.items[0].summaryOtherCount;
                this.unhandle = result.items[0].unhandle;
                this.promotionSold = result.items[0].wholeSalePromotionSold;
                this.promotionProject = result.items[0].wholeSalePromotionProject;
                this.noReplay = result.items[0].noReplay;
                this.totalSendPromotion = result.items[0].totalSendWholeSalePromotion;


                this.pertotalPromotion = Math.floor((this.totalPromotion / this.totalSendPromotion)*100);
                this.perinterestedCount = Math.floor((this.interestedCount / this.totalPromotion)*100);
                this.pernotInterestedCount = Math.floor((this.notInterestedCount / this.totalPromotion)*100);
                this.perotherCount = Math.floor((this.otherCount / this.totalPromotion)*100);
                this.perunhandle = Math.floor((this.unhandle / this.totalPromotion)*100);
                this.perpromotionSold = Math.floor((this.promotionSold / this.totalPromotion)*100);
                this.perpromotionProject = Math.floor((this.promotionProject / this.totalPromotion)*100);
                this.pernoReplay = Math.floor((this.noReplay / this.totalPromotion)*100);

                this.perinterestedCount = this.perinterestedCount>0 ?this.perinterestedCount:0;
                this.pernotInterestedCount =this.pernotInterestedCount>0 ?this.pernotInterestedCount:0;
                this.perotherCount =this.perotherCount>0 ?this.perotherCount:0;
                this.pertotalPromotion =this.pertotalPromotion>0 ?this.pertotalPromotion:0;

            } else {
                this.totalPromotion = 0;
                this.interestedCount = 0;
                this.notInterestedCount = 0;
                this.otherCount = 0;
                this.unhandle = 0;
                this.promotionSold = 0;
                this.promotionProject = 0;
                this.noReplay = 0;
                this.totalSendPromotion = 0;
            }
        },
        err => {
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createPromotion(): void {
        //this.CreateOrEditWhlPromotionComponent.show(this.organizationUnit);
    }


    deletePromotion(PromotionDto: PromotionDto): void {
        debugger;
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._promotionsServiceProxy.delete(PromotionDto.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete Wholesale Promotions: ' + PromotionDto.title;
                            log.section = 'Wholesale Promotions';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });

                        });
                }
            }
        );
    }
    clear() {
        this.titleFilter = '';
        this.startDate = moment(this.date);
        this.endDate = moment(this.date);
    }
    exportToExcel(): void {
        this._promotionsServiceProxy.getWholeSalePromotionsToExcel(
            this.filterText,
            this.titleFilter,
            undefined,
            this.descriptionFilter,
            undefined,
            undefined,
           this.startDate,
           this.endDate,
            this.promotionTypeNameFilter,
            this.organizationUnit, this.promotionTitleFilter, this.promotype,
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }
}
