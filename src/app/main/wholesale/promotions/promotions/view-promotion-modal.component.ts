import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetWholeSalePromotionForViewDto, WholeSalePromotionDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
@Component({
    selector: 'viewWholeSalePromotionModal',
    templateUrl: './view-promotion-modal.component.html'
})
export class ViewWholeSalePromotionModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetWholeSalePromotionForViewDto;


    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
        this.item = new GetWholeSalePromotionForViewDto();
        this.item.wholeSalePromotion = new WholeSalePromotionDto();
    }

    show(item: GetWholeSalePromotionForViewDto): void {
        debugger;
        this.item = item;
        this.active = true;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Opened Wholesale Promotions View : ' + this.item.wholeSalePromotion.title;
        log.section = 'Wholesale Promotions';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {            
         });
        this.modal.show();

      
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
