﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {
    PromotionsServiceProxy, CreateOrEditPromotionDto, PromotionPromotionTypeLookupTableDto,
    LeadsServiceProxy, LeadStateLookupTableDto,
    LeadSourcesServiceProxy, LeadSourceLookupTableDto,
    TeamDto, TeamsServiceProxy, NameValueOfString, NameValueOfInt32,
    LeadStatusLookupTableDto,
    CreateOrEditEmailTemplateDto,
    EmailTemplateServiceProxy,
    JobsServiceProxy,
    CommonLookupServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { debug } from 'console';
import { EmailEditorComponent } from 'angular-email-editor';
import { ActivatedRoute } from '@angular/router';
import * as _ from 'lodash';
import { FileDownloadService } from '@shared/utils/file-download.service';

@Component({
    selector: 'WholesalecreateOrEditPromotionModal',
    templateUrl: './create-or-edit-promotion-modal.component.html'
})
export class WholesaleCreateOrEditPromotionModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    showjobstatus = false;
    total = 0;
    credit = 0;
    promotion: CreateOrEditPromotionDto = new CreateOrEditPromotionDto();

    promotionTypeName = '';

    allPromotionTypes: PromotionPromotionTypeLookupTableDto[];
    team: TeamDto;
    teams: TeamDto[] = [];
    _teamid = '';

    sampleDateRange: moment.Moment[];
    allLeadStatus: LeadStatusLookupTableDto[];
    allStates: LeadStateLookupTableDto[];
    allLeadSources: LeadSourceLookupTableDto[];

    SelectedPromotionTypeId: number;
    SelectedStateId: number;
    SelectedLeadSourceId: number;
    SelectedLeadStatusId: number;
    SelectedJobStatusId: number;
    SelectedTeamId: number;
    name: string;
    organizationunit: any;
    TotalLeads: number;
    FilteredLeadId_CSV: string;

    filteredState: NameValueOfString[];
    filteredLeadSource: NameValueOfString[];
    filteredLeadStatus: NameValueOfString[];
    filteredJobStatus: NameValueOfString[];
    filteredTeams: NameValueOfString[];

    // teamids: NameValueOfInt32[] = new Array<NameValueOfInt32>();
    // leadStatusids: NameValueOfInt32[] = new Array<NameValueOfInt32>();
    // leadSourceids: NameValueOfInt32[] = new Array<NameValueOfInt32>();
    // jobStatusids: NameValueOfInt32[] = new Array<NameValueOfInt32>();
    // stateids: NameValueOfInt32[] = new Array<NameValueOfInt32>();
    leadStatusids = [];
    leadSourceids=[];
    jobStatusids =[];
    stateids=[];
    teamids=[];
    email: CreateOrEditEmailTemplateDto = new CreateOrEditEmailTemplateDto();
    emailData = '';
    saveortest = 0;
    areaNameFilter = '';
    typeNameFilter = '';
    constructor(
        injector: Injector,
        private _promotionsServiceProxy: PromotionsServiceProxy,
        private _teamService: TeamsServiceProxy,
        private _leadSourceServiceProxy: LeadSourcesServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _emailTemplateServiceProxy: EmailTemplateServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _jobsServiceProxy: JobsServiceProxy
    ) {

        super(injector);
        this.SelectedPromotionTypeId = 0;
        this.SelectedStateId = 0;
        this.SelectedLeadSourceId = 0;
        this.SelectedJobStatusId = 0;
        this.SelectedLeadStatusId = 0;
        this.SelectedTeamId = 0;
        this.total = 0;
        this.credit = 0;
        this.TotalLeads = 0;
        this.FilteredLeadId_CSV = '';

        this.getAllPromotionType();
        this.getAllTeams();
        this.getAllStates();
        this.getAllLeadSources();
        this._activatedRoute.queryParams.subscribe(params => {
            this.email.id = params['eid'];
        });
    }
    @ViewChild(EmailEditorComponent)
    private emailEditor: EmailEditorComponent;

    filterLeadSource(event): void {
        this._leadsServiceProxy.promotionGetAllLeadSource(event.query).subscribe(result => {
            this.filteredLeadSource = result;
        });
    }


    filterState(event): void {
        this._leadsServiceProxy.promotionGetAllState(event.query).subscribe(result => {
            this.filteredState = result;
        });
    }
    filterLeadStatus(event): void {
        this._leadsServiceProxy.promotionGetAllLeadStatus(event.query).subscribe(result => {
            this.filteredLeadStatus = result;
        });
    }

    filterJobStatus(event): void {
        this._leadsServiceProxy.promotionGetAllJobStatus(event.query).subscribe(result => {
            this.filteredJobStatus = result;

        });
    }

    filterTeams(event): void {
        this._teamService.getAllTeams(event.query).subscribe(teams => {
            this.filteredTeams = teams;
        });
    }

    getAllLeadStatus() {

        //Get LeadStatus
        this._leadsServiceProxy.promotionGetAllLeadStatus("").subscribe(result => {
            // this.allLeadStatus = result;
            this.filteredLeadStatus = result;
            //this.countLeads();
        });
    }

    getAllPromotionType() {

        // this._promotionsServiceProxy.getAllWholeSalePromotionTypeForTableDropdown().subscribe(result => {
        //     this.allPromotionTypes = result;
        // });

    }
    getAllTeams() {
        //Get Teams
        this.teams = [];
        this._teamService.getAll(undefined, undefined, undefined, undefined, undefined)
            .subscribe(result => {
                result.items.forEach(element => {
                    this.teams.push(element.team);
                });
            });
    }

    getAllStates() {
        //Get States
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
    }

    getAllLeadSources() {
        //Get LeadSources
        this._commonLookupService.getAllLeadSourceForTableDropdown().subscribe(result => {
            this.allLeadSources = result;
        });
    }

    show(organizationid: number): void {
        //this.countLeads();
        this.promotion = new CreateOrEditPromotionDto();
        this.organizationunit = organizationid;
        this.active = true;
        this.modal.show();
        // if (!promotionId) {
        //     this.promotion = new CreateOrEditPromotionDto();
        //     this.promotion.id = promotionId;
        //     this.promotionTypeName = '';

        //     this.active = true;
        //     this.modal.show();
        // } else {
        //     this._promotionsServiceProxy.getPromotionForEdit(promotionId).subscribe(result => {
        //         this.promotion = result.promotion;

        //         this.promotionTypeName = result.promotionTypeName;

        //         this.active = true;
        //         this.modal.show();
        //     });
        // }

    }

    RefreshCountLeads(): void {
        this.countLeads();
    }

    countLeads(): void {
        this.TotalLeads = 0;
        //Get LeadCount

        const exist = this.leadStatusids.some(function (item) {
            return item.name.includes("Upgrade");
        });
        if (exist == true) {
            this.showjobstatus = true;
        } else {
            this.showjobstatus = false;
        }

        this._leadsServiceProxy.getTotalLeadsCountForPromotion(
            this.SelectedLeadStatusId == 0 ? undefined : this.SelectedLeadStatusId,
            this.SelectedLeadSourceId == 0 ? undefined : this.SelectedLeadSourceId,
            this.sampleDateRange != undefined && this.sampleDateRange.length > 0 ? this.sampleDateRange[0] : undefined,
            this.sampleDateRange != undefined && this.sampleDateRange.length > 1 ? this.sampleDateRange[1] : undefined,
            this.SelectedStateId == 0 ? undefined : this.SelectedStateId,
            this.SelectedTeamId == 0 ? undefined : this.SelectedTeamId,
            this.leadStatusids,
            this.leadSourceids,
            this.stateids,
            this.teamids,
            this.showjobstatus ? this.jobStatusids : [],
            this.areaNameFilter,
            this.typeNameFilter,
            this.organizationunit,
            '',
            [],
            undefined,
            undefined,
            undefined
        ).subscribe(result => {
            this.FilteredLeadId_CSV = '';
            result.forEach(element => {
                this.FilteredLeadId_CSV = this.FilteredLeadId_CSV + element + ',';
            });
            this.TotalLeads = result.length;
            this.promotion.leadCount = result.length;
        });
    }

    sendEmail() {
        this.saving = true;
        this.saveortest = 1;
        debugger;
        this.saveDesign();
    }

    saveDesign() {
        this.saving = true;
        this.emailEditor.editor.exportHtml((data) =>
            this.saveHTML(data.html)
        );
    }

    saveHTML(data) {
        let htmlTemplate = this.getEmailTemplate(data);
        this.promotion.description = htmlTemplate;
        this.save();
    }

    getEmailTemplate(emailHTML) {
        let myTemplateStr = emailHTML;

        // interpolate
        let compiled = _.template(myTemplateStr);
        let myTemplateCompiled = compiled();
        return myTemplateCompiled;
    }

    Testing(): void {
        this.saving = true;
        if (this.promotion.promotionTypeId == 1) {
            if (this.promotion.mobileNos != null) {
                this.saveortest = 2;
                this.save();
            } else {
                this.notify.warn('Plase Enter Mobile NO');
            }
        } else {
            if (this.promotion.emails != null) {
                this.saveortest = 2;
                this.saveDesign();
            } else {
                this.notify.warn('Plase Enter Email');
            }
        }
    }

    savesms(): void {
        this.saving = true;
        this.saveortest = 1;
        this.save();
    }

    save(): void {
        this.saving = true;
        this.promotion.saveOrTest = this.saveortest;
        this.promotion.organizationID = this.organizationunit;
        this.promotion.promotionTypeId = this.SelectedPromotionTypeId;
        this.promotion.selectedLeadIdsForPromotion = this.FilteredLeadId_CSV;
        if (this.TotalLeads > 0) {
            this._promotionsServiceProxy.createOrEdit(this.promotion)
                .pipe(finalize(() => { this.saving = false; }))
                .subscribe(() => {
                    this.notify.info(this.l('SavedSuccessfully'));
                    this.FilteredLeadId_CSV = '';
                    if (this.promotion.saveOrTest == 1) {
                        this.close();
                    }
                    this.modalSave.emit(null);
                });
        } else {
            this.notify.error("No leads selected.");
            this.saving = false;
        }
    }

    countCharcters(): void {
        this.total = this.promotion.description.length;
        this.credit = Math.ceil(this.total / 160);
    }

    close(): void {
        this.active = false;
        this.total = 0;
        this.credit = 0;
        this.SelectedPromotionTypeId = 0;
        this.SelectedStateId = 0;
        this.SelectedLeadSourceId = 0;
        this.SelectedJobStatusId = 0;
        this.SelectedLeadStatusId = 0;
        this.SelectedTeamId = 0;
        this.total = 0;
        this.credit = 0;
        this.TotalLeads = 0;
        this.FilteredLeadId_CSV = '';
        this.areaNameFilter = '';
        this.typeNameFilter = '';
        this.jobStatusids = new Array<NameValueOfInt32>();;
        this.TotalLeads = 0;
        this.promotion.leadCount = 0;
        this.modal.hide();
    }

    exportExcel() {
        this.saving = true;
        this.promotion.saveOrTest = 1;
        this.promotion.organizationID = this.organizationunit;
        this.promotion.startDateFilter = this.sampleDateRange[0];
        this.promotion.endDateFilter = this.sampleDateRange[1];
        this.promotion.leadStatusIdsFilter = this.leadStatusids;
        this.promotion.leadSourceIdsFilter = this.leadSourceids;
        this.promotion.stateIdsFilter = this.stateids;
        this.promotion.teamIdsFilter = this.teamids;
        this.showjobstatus ? this.promotion.jobStatusIdsFilter = this.jobStatusids : [];
        this.promotion.areaNameFilter = this.areaNameFilter;
        this.promotion.typeNameFilter = this.typeNameFilter;
        this.promotion.organizationID = this.organizationunit;
        this.promotion.promotionTypeId = this.SelectedPromotionTypeId;
        this.promotion.selectedLeadIdsForPromotion = this.FilteredLeadId_CSV;
        
        if (this.TotalLeads > 0) {
            this._promotionsServiceProxy.createWithExcel(this.promotion)
                .pipe(finalize(() => { this.saving = false; }))
                .subscribe((result) => {
                    this.notify.info(this.l('SavedSuccessfully'));
                    this._fileDownloadService.downloadTempFile(result);
                    this.FilteredLeadId_CSV = '';
                    if (this.promotion.saveOrTest == 1) {
                        this.close();
                    }
                    this.modalSave.emit(null);
                });
        } else {
            this.notify.error("No leads selected.");
            this.saving = false;
        }
    }
}
