﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { PromotionResponseStatusesServiceProxy, CreateOrEditPromotionResponseStatusDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';

@Component({
    selector: 'createOrEditPromotionResponseStatusModal',
    templateUrl: './create-or-edit-promotionResponseStatus-modal.component.html'
})
export class CreateOrEditPromotionResponseStatusModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    promotionResponseStatus: CreateOrEditPromotionResponseStatusDto = new CreateOrEditPromotionResponseStatusDto();



    constructor(
        injector: Injector,
        private _promotionResponseStatusesServiceProxy: PromotionResponseStatusesServiceProxy
    ) {
        super(injector);
    }

    show(promotionResponseStatusId?: number): void {

        if (!promotionResponseStatusId) {
            this.promotionResponseStatus = new CreateOrEditPromotionResponseStatusDto();
            this.promotionResponseStatus.id = promotionResponseStatusId;

            this.active = true;
            this.modal.show();
        } else {
            this._promotionResponseStatusesServiceProxy.getPromotionResponseStatusForEdit(promotionResponseStatusId).subscribe(result => {
                this.promotionResponseStatus = result.promotionResponseStatus;


                this.active = true;
                this.modal.show();
            });
        }
        
    }

    save(): void {
            this.saving = true;

			
            this._promotionResponseStatusesServiceProxy.createOrEdit(this.promotionResponseStatus)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
             });
    }







    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
