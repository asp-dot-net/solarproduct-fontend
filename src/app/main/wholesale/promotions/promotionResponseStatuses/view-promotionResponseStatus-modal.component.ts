﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetPromotionResponseStatusForViewDto, PromotionResponseStatusDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewPromotionResponseStatusModal',
    templateUrl: './view-promotionResponseStatus-modal.component.html'
})
export class ViewPromotionResponseStatusModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetPromotionResponseStatusForViewDto;


    constructor(
        injector: Injector
    ) {
        super(injector);
        this.item = new GetPromotionResponseStatusForViewDto();
        this.item.promotionResponseStatus = new PromotionResponseStatusDto();
    }

    show(item: GetPromotionResponseStatusForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
