﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetPromotionUserForViewDto, GetWholeSalePromotionUserForViewDto, PromotionUserDto, WholeSalePromotionUserDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
@Component({
    selector: 'viewPromotionUserModal',
    templateUrl: './view-promotionUser-modal.component.html'
})
export class ViewPromotionUserModalWholesaleComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetWholeSalePromotionUserForViewDto;


    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
        this.item = new GetWholeSalePromotionUserForViewDto();
        this.item.wholeSalePromotionUser = new WholeSalePromotionUserDto();
    }

    show(item: GetWholeSalePromotionUserForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();

        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Opened Wholesale Promotion Tracker View : ' + this.item.wholeSalePromotionTitle;
        log.section = 'Wholesale Promotion Tracker';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {            
         });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
