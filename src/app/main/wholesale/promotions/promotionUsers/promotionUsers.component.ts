﻿import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
    PromotionUsersServiceProxy, PromotionUserDto,
    PromotionsServiceProxy, GetPromotionForViewDto,
    PromotionUserPromotionLookupTableDto,
    PromotionUserPromotionResponseStatusLookupTableDto,
    PromotionPromotionTypeLookupTableDto,
    LeadsServiceProxy,
    LeadUsersLookupTableDto,
    JobStatusTableDto,
    JobsServiceProxy,
    OrganizationUnitDto,
    UserServiceProxy,
    CommonLookupServiceProxy,
    WholeSalePromotionUsersServiceProxy,
    WholeSalePromotionsServiceProxy,
} from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
//import { CreateOrEditPromotionUserModalComponent } from './create-or-edit-promotionUser-modal.component';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
//import { ViewPromotionUserModalWholesaleComponent } from './view-promotionUser-modal.component';
//import { ViewPromotionModalComponent } from '.././promotions/view-promotion-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { ViewMyLeadComponent } from '@app/main/myleads/myleads/view-mylead.component';
import { finalize } from 'rxjs/operators';
import { Title } from '@angular/platform-browser';

@Component({
    templateUrl: './promotionUsers.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class PromotionUsersWholesaleComponent extends AppComponentBase {
    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    organizationUnitlength: number = 0;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    toggle: boolean = true;

    change() {
        this.toggle = !this.toggle;
      }
      
    // @ViewChild('createOrEditPromotionUserModal', { static: true }) createOrEditPromotionUserModal: CreateOrEditPromotionUserModalComponent;
    //@ViewChild('ViewPromotionUserModalWholesaleComponent', { static: true }) viewPromotionUserModal: ViewPromotionUserModalWholesaleComponent;
    // @ViewChild('viewPromotionModalComponent', { static: true }) viewPromotionModalComponent: ViewPromotionModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    // @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
    excelorcsvfile = 0;
    readunreadsmstag = 0;
    advancedFiltersAreShown = false;
    filterText = '';
    saving = false;
    maxResponseDateFilter: moment.Moment;
    minResponseDateFilter: moment.Moment;
    responseMessageFilter = '';
    promotionTitleFilter = 0;
    tableRecords: any;
    leadCopanyNameFilter = '';
    promotionResponseStatusNameFilter = '';
    promotionResponseStatusIdFilter = 0;
    allPromotionResponseStatuss: PromotionUserPromotionResponseStatusLookupTableDto[];
    allPromotions: PromotionUserPromotionLookupTableDto[];
    //sampleDateRange: moment.Moment[];
    FiltersData = true;
    allPromotionTypes: PromotionPromotionTypeLookupTableDto[];
    filteredTeams: LeadUsersLookupTableDto[];
    filteredManagers: LeadUsersLookupTableDto[];
    filteredReps: LeadUsersLookupTableDto[];
    alljobstatus: JobStatusTableDto[];
    responseType = 0;
    salesRepId = 0;
    teamId: number = 0;
    jobStatusIDFilter = [];
    date = new Date();
    firstDay = moment(new Date(this.date.getFullYear(), this.date.getMonth(), 1));
    sampleDateRange: moment.Moment[] = [this.firstDay, moment().add(0, 'days').endOf('day')];
    responseDateFilter = 'Responce';
    // startDate: moment.Moment;
    // endDate: moment.Moment;
    totalPromotion = 0;
    interestedCount = 0;
    notInterestedCount = 0;
    otherCount = 0;
    unhandle = 0;
    perinterestedCount = 0;
    pernotInterestedCount = 0;
    perotherCount = 0;
    perunhandle = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnit: number = 0;
    promotionSold = 0;
    promotionProject = 0;
    noReplay = 0;
    perpromotionSold = 0;
    perpromotionProject = 0;
    pernoReplay = 0;
    ExpandedView: boolean = true;
    SelectedLeadId: number = 0;
    promotype = 0;
    readunreadsms = 2;
    firstrowcount = 0;
    last = 0;
    role: string = '';
    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 230;
    filterName = "CompanyName";
    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);

    constructor(
        injector: Injector,
        private _promotionUsersServiceProxy: WholeSalePromotionUsersServiceProxy,
        private _promotionsServiceProxy: WholeSalePromotionsServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _jobsServiceProxy: JobsServiceProxy,
        private _userServiceProxy: UserServiceProxy,
        private titleService: Title
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Promotion Tracker");
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
            this.role = result;
        });

        this._commonLookupService.getTeamForFilter().subscribe(teams => {
            this.filteredTeams = teams;
            //this.teamId = teams[0].id;
        });

        this._promotionUsersServiceProxy.getAllWholeSalePromotionResponseStatusForTableDropdown().subscribe(result => {
            this.allPromotionResponseStatuss = result;
        });

        this._promotionUsersServiceProxy.getAllWholeSalePromotionForTableDropdown().subscribe(result => {
            this.allPromotions = result;
        });

        this._commonLookupService.getAllJobStatusTableDropdown().subscribe(result => {
            this.alljobstatus = result;
        });

        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;

            // this._leadsServiceProxy.getSalesRepForFilter(this.allOrganizationUnits[0].id, this.teamId).subscribe(rep => {
            //     this.filteredReps = rep;
            // });

            this.bindSalesRep();

            this.getPromotionUsers();
            // this.getCount(this.organizationUnit);
        });
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Wholesale Promotion Tracker';
        log.section = 'Wholesale Promotion Tracker';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
    }
 searchLog() : void {
        
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Wholesale Promotion Tracker';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }

        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }

    bindSalesRep()
    {
        this._leadsServiceProxy.getSalesRepForFilter(this.organizationUnit, this.teamId).subscribe(rep => {
            this.filteredReps = rep;
        });
    }
    // getCount(organizationUnit: number): void {
    //     this._jobsServiceProxy.getAllApplicationTrackerCount(this.organizationUnit).subscribe(result => {
    //         this.totalPromotion = result.totalPromotion;
    //         this.interestedCount = result.interestedCount;
    //         this.notInterestedCount = result.notInterestedCount;
    //         this.otherCount = result.otherCount;
    //         this.unhandle = result.unhandle;
    //         this.promotionSold = result.promotionSold;
    //         this.promotionProject = result.promotionProject;
    //         this.noReplay = result.noReplay;
    //     });
    // }

    getAllPromotionType() {
        this._promotionsServiceProxy.getAllWholeSalePromotionTypeForTableDropdown().subscribe(result => {
            this.allPromotionTypes = result;
        });
    }

    updateResponseStatusFor(promotionUserId, promomotionResponseStatusId) {
        this.message.confirm(
            '',
            'Are you sure you want to change the response status?',
            (isConfirmed) => {
                if (isConfirmed) {
                    this._promotionUsersServiceProxy.updateWholeSalePromotionResponseStatus(
                        promotionUserId,
                        promomotionResponseStatusId)
                        .subscribe(result => {
                            this.notify.info('Response updated successfully');
                            this.getPromotionUsers();
                        });
                }
            }
        );
    }

    getPromotionUsers(event?: LazyLoadEvent) {
        debugger;
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        // if (this.sampleDateRange != null) {
        //     this.startDate = this.sampleDateRange[0];
        //     this.endDate = this.sampleDateRange[1];
        // } else {
        //     this.startDate = null;
        //     this.endDate = null;
        // }


        this.primengTableHelper.showLoadingIndicator();

        this._promotionUsersServiceProxy.getAll(
            this.filterName,
            this.filterText,
            this.startDate,
            this.endDate,
            this.organizationUnit,
            this.responseMessageFilter,
            this.promotionTitleFilter,
            this.leadCopanyNameFilter,
            this.promotionResponseStatusNameFilter,
            this.promotionResponseStatusIdFilter != 0 ? this.promotionResponseStatusIdFilter : undefined,
            this.jobStatusIDFilter,
            this.teamId,
            this.salesRepId,
            this.responseDateFilter,
            this.promotype,
            this.readunreadsms,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            debugger;
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.tableRecords = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            if (result.totalCount > 0) {
                this.getLeadSummaryCount();
            } else {
                this.totalPromotion = 0;
                this.interestedCount = 0;
                this.notInterestedCount = 0;
                this.otherCount = 0;
                this.unhandle = 0;
                this.promotionSold = 0;
                this.promotionProject = 0;
                this.noReplay = 0;
            }

            this.shouldShow = false;
            // if (result.totalCount > 0) {
            //     this.totalPromotion = result.items[0].totalPromotion;
            //     this.interestedCount = result.items[0].interestedCount;
            //     this.notInterestedCount = result.items[0].notInterestedCount;
            //     this.otherCount = result.items[0].otherCount;
            //     this.unhandle = result.items[0].unhandle;
            //     this.promotionSold = result.items[0].promotionSold;
            //     this.promotionProject = result.items[0].promotionProject;
            //     this.noReplay = result.items[0].noReplay;


            //     this.perinterestedCount = Math.floor((result.items[0].interestedCount / this.totalPromotion) * 100);
            //     this.pernotInterestedCount = Math.floor((result.items[0].notInterestedCount / this.totalPromotion) * 100);
            //     this.perotherCount = Math.floor((result.items[0].otherCount / this.totalPromotion) * 100);
            //     this.perunhandle = Math.floor((result.items[0].unhandle / this.totalPromotion) * 100);
            //     this.perpromotionSold = Math.floor((result.items[0].promotionSold / this.totalPromotion) * 100);
            //     this.perpromotionProject = Math.floor((result.items[0].promotionProject / this.totalPromotion) * 100);
            //     this.pernoReplay = Math.floor((result.items[0].noReplay / this.totalPromotion) * 100);

            // } else {
            //     this.totalPromotion = 0;
            //     this.interestedCount = 0;
            //     this.notInterestedCount = 0;
            //     this.otherCount = 0;
            //     this.unhandle = 0;
            //     this.promotionSold = 0;
            //     this.promotionProject = 0;
            //     this.noReplay = 0;
            // }
        });
    }
    getLeadSummaryCount() {
        debugger;

        this._promotionUsersServiceProxy.getAllCount(
            this.filterName,
            this.filterText,
            this.startDate,
            this.endDate,
            this.organizationUnit,
            this.responseMessageFilter,
            this.promotionTitleFilter,
            this.leadCopanyNameFilter,
            this.promotionResponseStatusNameFilter,
            this.promotionResponseStatusIdFilter != 0 ? this.promotionResponseStatusIdFilter : undefined,
            this.jobStatusIDFilter,
            this.teamId,
            this.salesRepId,
            this.responseDateFilter,
            this.promotype,
            this.readunreadsms,
            undefined,
            undefined,
            undefined,
        ).subscribe(result => {
            debugger;
            if (result) {
                this.totalPromotion = result.totalWholeSalePromotion;
                this.interestedCount = result.interestedCount;
                this.notInterestedCount = result.notInterestedCount;
                this.otherCount = result.otherCount;
                this.unhandle = result.unhandle;
                this.promotionSold = result.wholeSalePromotionSold;
                this.promotionProject = result.wholeSalePromotionProject;
                this.noReplay = result.noReplay;


                this.perinterestedCount = Math.floor((result.interestedCount / this.totalPromotion) * 100);
                this.pernotInterestedCount = Math.floor((result.notInterestedCount / this.totalPromotion) * 100);
                this.perotherCount = Math.floor((result.otherCount / this.totalPromotion) * 100);
                this.perunhandle = Math.floor((result.unhandle / this.totalPromotion) * 100);
                this.perpromotionSold = Math.floor((result.wholeSalePromotionSold / this.totalPromotion) * 100);
                this.perpromotionProject = Math.floor((result.wholeSalePromotionProject / this.totalPromotion) * 100);
                this.pernoReplay = Math.floor((result.noReplay / this.totalPromotion) * 100);

            }
        });
    }
    mark(id: number): void {
        this.message.confirm(
            this.l('AreYouSureWanttoMark'),
            this.l('Mark'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._promotionUsersServiceProxy.markAsReadWholeSalePromotionSms(id)
                        .subscribe(() => {
                            this.getPromotionUsers()
                            this.notify.success(this.l('SuccessfullyMark'));
                        });
                }
            }
        );
    }

    reloadPage(event): void {
        this.ExpandedView = true;
        this.paginator.changePage(this.paginator.getPage());
    }

    createPromotionUser(): void {
        //this.createOrEditPromotionUserModal.show();
    }

    deletePromotionUser(promotionUser: PromotionUserDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._promotionUsersServiceProxy.delete(promotionUser.id)
                        .subscribe(() => {
                            this.reloadPage(event);
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete Wholesale Promotion Tracker: ' + promotionUser.responseDate;
                            log.section = 'Wholesale Promotion Tracker';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });

                        });
                }
            }
        );
    }

    exportToExcel(excelorcsv): void {
        this.excelorcsvfile = excelorcsv
        this._promotionUsersServiceProxy.getWholeSalePromotionUsersToExcel(
            this.filterName,
            this.filterText,
            this.startDate,
            this.endDate,
            this.organizationUnit,
            this.responseMessageFilter,
            this.promotionTitleFilter,
            this.leadCopanyNameFilter,
            this.promotionResponseStatusNameFilter,
            this.promotionResponseStatusIdFilter != 0 ? this.promotionResponseStatusIdFilter : undefined,
            this.jobStatusIDFilter,
            this.teamId,
            this.salesRepId,
            this.responseDateFilter,
            this.promotype,
            this.readunreadsms,
            this.excelorcsvfile)
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            }
        );
    }

    expandGrid() {
        this.ExpandedView = true;
    }

    navigateToLeadDetail(leadid): void {
        // this.ExpandedView = !this.ExpandedView;
        // this.ExpandedView = false;
        // this.SelectedLeadId = leadid;
        // this.viewLeadDetail.showDetail(leadid, null, 15);
    }

    checkAll(ev) {
        debugger;
        this.tableRecords.forEach(x => x.isSelected = ev.target.checked);
    }

    isAllChecked() {
        if (this.tableRecords)
            return this.tableRecords.every(_ => _.isSelected);
    }


    submit(): void {
        debugger;
        let selectedids = [];
        this.saving = true;
        this.primengTableHelper.records.forEach(function (record) {
            if (record.isSelected) {
                selectedids.push(record.promotionUser.id);
            }
        });
        if (selectedids.length == 0) {
            this.notify.warn(this.l('NoDataSelected'));
            this.saving = false;
            return;
        }
        if (this.readunreadsmstag == 0) {
            this.notify.warn("PleaseSelectValue");
            this.saving = false;
            return;
        }
        this._promotionUsersServiceProxy.markAsReadWholeSalePromotionSmsInBulk(this.readunreadsmstag, selectedids)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.readunreadsmstag = 0;
                this.reloadPage(true);
                this.saving = false;
                this.notify.info(this.l('SuccessfullyMark'));
            });

    }

}
