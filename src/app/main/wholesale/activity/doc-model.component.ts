import { Component, ElementRef, Injector, ViewChild } from '@angular/core';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AuditLogListDto, CommonLookupDto, GetWholeSaleLeadDoc, WholeSaleLeadServiceProxy } from '@shared/service-proxies/service-proxies';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { FileItem, FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'wholeSaleDocumentUploadModal',
    templateUrl: './doc-model.component.html',
})
export class WholeSaleDocumentUploadModalComponent extends AppComponentBase {
    @ViewChild('DocumentUploadModal', { static: true }) modal: ModalDirective;
    @ViewChild('myInput') myInputVariable: ElementRef;

    ExpandedViewApp: boolean = true;
    active = false;    
    public uploader: FileUploader;
    public maxfileBytesUserFriendlyValue = 5;
    private _uploaderOptions: FileUploaderOptions = {};
    wholesaleLeadId = 0;
    filenName = [];
    filetokens=[];
    fileNames = [];
    wholeSaleLeaddocs: GetWholeSaleLeadDoc[];
    sectionId =0;
    fileToken : any;
    fileName : any;
    saving = false;
    DocumentTypes: CommonLookupDto[];
    documentTypeId= 0;

    constructor(injector: Injector
        , private _dateTimeService: DateTimeService
        , private _tokenService: TokenService
        ,public _wholeSaleLeadServiceProxy :WholeSaleLeadServiceProxy
        ) {
        super(injector);
    }

    show(wholesaleLeadId : number, sectionId :number) {
        this.wholesaleLeadId = wholesaleLeadId;
        this.sectionId = sectionId;
        this._wholeSaleLeadServiceProxy.getallWholeSaleDocument().subscribe(result => {
            this.DocumentTypes = result;
        });
        this.getallWholeSaledoc(this.wholesaleLeadId);
        this.initializeModal();
        this.modal.show();
          
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;                
    }
    expandApp() {
        this.ExpandedViewApp = false;
    }

    initializeModal(): void {
        // this.active = true;
        this.initFileUploader();
      }
    
    // upload completed event
    fileChangeEvent(event: any): void {
        if (event.target.files[0].size > 5242880) { //5MB
            this.message.warn(this.l('ProfilePicture_Warn_SizeLimit', this.maxfileBytesUserFriendlyValue));
            return;
        }
        this.uploader.clearQueue();
        this.uploader.addToQueue([<File>event.target.files[0]]);
        this.uploader.uploadAll();
    }

    initFileUploader(): void {
        debugger;
        this.uploader = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Profile/UploadFile' });
        this._uploaderOptions.autoUpload = false;
        this._uploaderOptions.authToken = 'Bearer ' + this._tokenService.getToken();
        this._uploaderOptions.removeAfterUpload = true;
        this.uploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };
        this.uploader.onBuildItemForm = (fileItem: FileItem, form: any) => {
            form.append('FileType', fileItem.file.type);
            form.append('FileName', fileItem.file.name);
            form.append('FileToken', this.guid());
            this.filenName.push(fileItem.file.name);

        };
        this.uploader.onSuccessItem = (item, response, status) => {
            debugger;
            const resp = <IAjaxResponse>JSON.parse(response);
            if (resp.success) {
                this.fileToken = resp.result.fileToken;
                this.fileName = resp.result.fileName;
                //this.savedocument(resp.result.fileToken, resp.result.fileName, this.wholesaleLeadId);
                // this.filetokens.push(resp.result.fileToken);
                // this.fileNames.push(resp.result.fileName);
            } else {
                this.message.error(resp.error.message);
            }
        };
        this.uploader.setOptions(this._uploaderOptions);

    }
    
    guid(): string {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }

    savedocument(): void {
        this.active = true;
        if (this.documentTypeId == 0) {
            this.notify.warn(this.l('SelectDocumentType'));
            this.active = false;
            return;
        }
        if (!this.fileToken) {
            this.notify.warn(this.l('SelectFileToUpload'));
            this.active = false;
            return;
        }
        debugger;
        this.notify.info("File Uploading Start");
        this.active = true;
        this._wholeSaleLeadServiceProxy.saveWholeSaleDocument(this.fileToken,this.fileName,this.wholesaleLeadId,this.documentTypeId,this.sectionId)
            .pipe(finalize(() => { this.active = false; }))
            .subscribe(() => {
                this.getallWholeSaledoc(this.wholesaleLeadId);
                this.documentTypeId = 0;
                this.fileToken = null;
                this.fileName = "";  
            });
            
    }

    downloadfile(file): void {

        let FileName = AppConsts.docUrl + "/" + file.filePath + file.fileName;
        window.open(FileName, "_blank");
    }

    deletefile(wholesaleDocId : number){
        this.message.confirm(
            this.l('AreYouSureWanttoDelete'),
            this.l('Delete'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._wholeSaleLeadServiceProxy.deleteWholeSaleDoc(wholesaleDocId,this.wholesaleLeadId,this.sectionId).subscribe(result => {
                        this.notify.info(this.l('SuccessfullyDeleted'));
                        this.getallWholeSaledoc(this.wholesaleLeadId);
                    }); 
                }
            }
        );
      
    }
    getallWholeSaledoc(wholesaleLeadId :number) : void{
        this._wholeSaleLeadServiceProxy.getallWholeSaledoc(this.wholesaleLeadId).subscribe(result => {
            this.wholeSaleLeaddocs = result;
        });
    }


}
