import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AuditLogListDto, CreateOrEditWholeSaleLeadDto,  WholeSaleActivityServiceProxy, WholeSaleActivitylogInput, WholeSaleLeadServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
@Component({
    selector: 'wholeSaleNotifyCommentModal',
    templateUrl: './notify-comment-modal.component.html',
})
export class WholeSaleNotifyCommentModalComponent extends AppComponentBase {
    @ViewChild('NotifyCommentModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    ExpandedViewApp: boolean = true;
    active = false;    
    wholeSalelead : CreateOrEditWholeSaleLeadDto;
    activitytype : number;
    activityLog : WholeSaleActivitylogInput
    saving = false;

    constructor(injector: Injector
        , private _wholeSaleActivityServiceProxy : WholeSaleActivityServiceProxy
        , private _dateTimeService: DateTimeService
        , private _userActivityLogServiceProxy : UserActivityLogServiceProxy
        , private _wholeSaleLeadServiceProxy : WholeSaleLeadServiceProxy
        ) {
        super(injector);
        this.wholeSalelead = new CreateOrEditWholeSaleLeadDto();
        this.activityLog = new WholeSaleActivitylogInput();

    }
    sectionName = '';
    show(wholeSaleLeadId: number, sectionId:number, type :number,section: string) {

        this.activitytype = type;
        this._wholeSaleLeadServiceProxy.getWholeSaleLeadForEdit(wholeSaleLeadId).subscribe(result => {
            this.wholeSalelead = result;
        });
        this.activityLog = new WholeSaleActivitylogInput();
        this.activityLog.wholeSaleLeadId = wholeSaleLeadId;
        this.activityLog.sectionId = sectionId;
        this.modal.show();  
        
        let log = new UserActivityLogDto();
        log.actionId = 79;
        if (type === 1) {
            log.actionNote = 'Open Notify';
        } else if (type === 2) {
            log.actionNote = 'Open Comment';
        }
        log.section = section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });

    }

    save(){
        this.saving = true;
        if(this.activitytype==1){
            this._wholeSaleActivityServiceProxy.addNotifyActivityLog(this.activityLog)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
              this.notify.info(this.l('NotificationSentSuccessfully'));
              this.modal.hide();
              this.modalSave.emit(null);
              let log = new UserActivityLogDto();
            log.actionId = 9;
            log.actionNote ='Notify';
            log.section = this.sectionName;
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            });
        }
        else{
            this._wholeSaleActivityServiceProxy.addCommentActivityLog(this.activityLog)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
              this.notify.info(this.l('SavedSuccessfully'));
              this.modal.hide();
              this.modalSave.emit(null);
              let log = new UserActivityLogDto();
            log.actionId = 24;
            log.actionNote ='Comment Added';
            log.section = this.sectionName;
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            });
        }
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;                
    }
    expandApp() {
        this.ExpandedViewApp = false;
    }
}
