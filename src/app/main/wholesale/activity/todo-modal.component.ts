import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AuditLogListDto, CommonLookupDto, CreateOrEditWholeSaleLeadDto, JobsServiceProxy, WholeSaleActivityServiceProxy, WholeSaleActivitylogInput, WholeSaleLeadServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
@Component({
    selector: 'wholeSaletodoModal',
    templateUrl: './todo-modal.component.html',
})
export class WholeSaletodoModalComponent extends AppComponentBase {
    @ViewChild('todoModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    ExpandedViewApp: boolean = true;
    active = false;    
    wholeSalelead : CreateOrEditWholeSaleLeadDto;
    activitytype : number;
    activityLog : WholeSaleActivitylogInput
    saving = false;   
    userName = '';
    allUsers: CommonLookupDto[];
    
    constructor(injector: Injector
        , private _dateTimeService: DateTimeService
        , private _wholeSaleActivityServiceProxy : WholeSaleActivityServiceProxy
        , private _wholeSaleLeadServiceProxy : WholeSaleLeadServiceProxy
        , private _jobsServiceProxy :JobsServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy) {
        super(injector);
        this.wholeSalelead = new CreateOrEditWholeSaleLeadDto();
        this.activityLog = new WholeSaleActivitylogInput();
    }
    sectionName=""
    show(wholeSaleLeadId: number, sectionId:number,section:string) {
        this.activityLog.wholeSaleLeadId = wholeSaleLeadId;
        this.activityLog.sectionId = sectionId;
        this._wholeSaleLeadServiceProxy.getWholeSaleLeadForEdit(wholeSaleLeadId).subscribe(result => {
            this.wholeSalelead = result;
        });
        this._jobsServiceProxy.getUsersListForToDo().subscribe(result => {
            this.allUsers = result;
        });
        this.modal.show();  
        this.sectionName=section;  
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote = 'Open ToDo ';
        log.section = this.sectionName;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });    
    }
    
    save(){
        this.saving = true;
        this._wholeSaleActivityServiceProxy.addToDoActivityLog(this.activityLog)
        .pipe(finalize(() => { this.saving = false; }))
        .subscribe(() => {
          this.notify.info(this.l('SavedSuccessfully'));
          this.modal.hide();
          this.modalSave.emit(null);
          let log = new UserActivityLogDto();
              log.actionId =25;
              log.actionNote ='To Do';
              log.section = this.sectionName;
              this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                  .subscribe(() => {
              });
        });
    }
    close(): void {
        this.saving = false;
        this.modal.hide();
    }

    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;                
    }
    expandApp() {
        this.ExpandedViewApp = false;
    }
}
