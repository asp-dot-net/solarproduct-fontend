import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AuditLogListDto, CreateOrEditWholeSaleLeadDto, LeadsServiceProxy, WholeSaleActivityServiceProxy, WholeSaleActivitylogInput, WholeSaleLeadServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
@Component({
    selector: 'wholeSaleReminderModal',
    templateUrl: './reminder-modal.component.html',
})
export class WholeSaleReminderModalComponent extends AppComponentBase {
    @ViewChild('ReminderModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    ExpandedViewApp: boolean = true;
    active = false;    
    wholeSalelead : CreateOrEditWholeSaleLeadDto;
    activitytype : number;
    activityLog : WholeSaleActivitylogInput
    saving = false;   
    userName = '';

    constructor(injector: Injector
        , private _wholeSaleActivityServiceProxy : WholeSaleActivityServiceProxy
        , private _wholeSaleLeadServiceProxy : WholeSaleLeadServiceProxy
        , private _leadsServiceProxy : LeadsServiceProxy
        , private _userActivityLogServiceProxy : UserActivityLogServiceProxy
        )
         {
            super(injector);
            this.wholeSalelead = new CreateOrEditWholeSaleLeadDto();
            this.activityLog = new WholeSaleActivitylogInput();
    }
    sectionName=""
    show(wholeSaleLeadId: number, sectionId:number, section: string) {
            this.activityLog = new WholeSaleActivitylogInput();
            this.activityLog.wholeSaleLeadId = wholeSaleLeadId;
        this.activityLog.sectionId = sectionId;

        this._wholeSaleLeadServiceProxy.getWholeSaleLeadForEdit(wholeSaleLeadId).subscribe(result => {
            this.wholeSalelead = result;
        });
        this._leadsServiceProxy.getCurrentUserIdName().subscribe(result => {
            this.userName = result;

        });
        this.modal.show(); 
        this.sectionName=section;  
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote = 'Open Reminder ';
        log.section = this.sectionName;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });          
    }

    close(): void {
        this.saving = false;
        this.modal.hide();
    }

    save(){
        this.saving = true;
        this._wholeSaleActivityServiceProxy.addReminderActivityLog(this.activityLog)
        .pipe(finalize(() => { this.saving = false; }))
        .subscribe(() => {
          this.notify.info(this.l('SavedSuccessfully'));
          this.modal.hide();
          this.modalSave.emit(null);
          let log = new UserActivityLogDto();
              log.actionId =8;
              log.actionNote ='Reminder Sent';
              log.section = this.sectionName;
              this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                  .subscribe(() => {
              });
            
        });
    }

    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;                
    }
    expandApp() {
        this.ExpandedViewApp = false;
    }
}
