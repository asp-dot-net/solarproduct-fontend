import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WholeSaleSMSEmailModalComponent } from './sms-email-modal.component';

describe('WholeSaleSMSEmailModalComponent', () => {
  let component: WholeSaleSMSEmailModalComponent;
  let fixture: ComponentFixture<WholeSaleSMSEmailModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WholeSaleSMSEmailModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WholeSaleSMSEmailModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
