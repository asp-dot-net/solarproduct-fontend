﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit, ElementRef, Directive, Optional } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {
    LeadsServiceProxy
    , LeadStateLookupTableDto
    , LeadSourceLookupTableDto
    , LeadStatusLookupTableDto,
    WholesaleInvoiceDto,
    CheckExistLeadDto,
    GetDuplicateLeadPopupDto,
    CommonLookupDto,
    CommonLookupServiceProxy,
    WholeSaleInvoiceServiceProxy,
    JobsServiceProxy,
    OrganizationUnitDto,
    PostCodeRangeServiceProxy,
    JobInstallerInvoicesServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { FileUpload } from 'primeng/fileupload';
import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AgmCoreModule } from '@agm/core';
import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';
import PlaceResult = google.maps.places.PlaceResult;
import { Appearance, GermanAddress, Location } from '@angular-material-extensions/google-maps-autocomplete';
//import { ViewDuplicatePopUpModalComponent } from './duplicate-lead-popup.component';
import { ViewMyLeadComponent } from '@app/main/myleads/myleads/view-mylead.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'createEditWholesaleInvoice',
    templateUrl: './create-edit-invoice.component.html',
    styleUrls: ['./create-edit-invoice.component.less']
})
export class createEditWholesaleInvoiceComponent extends AppComponentBase implements OnInit {
    @ViewChild("myNameElem") myNameElem: ElementRef;
    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @ViewChild('SampleDatePicker', { static: true }) sampleDatePicker: ElementRef;
    //@ViewChild('duplicatepopupModal', { static: true }) duplicatepopupModal: ViewDuplicatePopUpModalComponent;
    @ViewChild('createEditWholesaleInvoiceComponent', { static: true }) createEditWholesaleInvoiceComponent: createEditWholesaleInvoiceComponent;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    installationItemList: any[];
    //installationItemList: any[] = [];
    invoiceDto:  WholesaleInvoiceDto = new  WholesaleInvoiceDto();

    active = false;
    saving = false;
    
    postCodeSuburb = '';
    stateName = '';
    leadSourceName = '';
    LeadStatusName = '';
    latitude = '';
    longitude = '';
    leadStatus: any;
    allStates: LeadStateLookupTableDto[];
    allLeadSources: LeadSourceLookupTableDto[];
    allLeadStatus: LeadStatusLookupTableDto[];
    ShowAddress: boolean = true;
    unitType: any;
    streetType: any;
    streetName: any;
    suburb: any;
    postalUnitType: any;
    postalStreetType: any;
    postalStreetName: any;
    postalSuburb: any;
    filteredunitTypes: LeadStatusLookupTableDto[];
    filteredstreettypes: LeadStatusLookupTableDto[];
    filteredstreetnames: LeadStatusLookupTableDto[];
    filteredsuburbs: LeadStatusLookupTableDto[];
    output: LeadStatusLookupTableDto[] = new Array<LeadStatusLookupTableDto>();
    unitTypesSuggestions: string[];
    streetTypesSuggestions: string[];
    streetNamesSuggestions: string[];
    suburbSuggestions: string[];
    postalunitTypesSuggestions: string[];
    postalstreetTypesSuggestions: string[];
    postalstreetNamesSuggestions: string[];
    postalsuburbSuggestions: string[];
    selectAddress: boolean = true;
    selectAddress2: boolean = true;
    from: string;
    item: GetDuplicateLeadPopupDto[];
    role: string = '';
    JobProducts: any[];
    disbaleifsalerep = false;

    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnitlength: number=0;

    productTypes : CommonLookupDto[];
    JobTypes = [];
    //JobTypes : CommonLookupDto[];
    DeliveryOptions : CommonLookupDto[];
    InvoiceTypes : CommonLookupDto[] ;
    JobStatuses : CommonLookupDto[];
    TransportTypes : CommonLookupDto[];
    PropertyTypes : CommonLookupDto[];
    PVDStatuses : CommonLookupDto[];
    productItemSuggestions: any[];


    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _commonLookupServiceProxy: CommonLookupServiceProxy,
        private _jobServiceProxy: JobsServiceProxy,
        private _wholeSaleInvoiceServiceProxy : WholeSaleInvoiceServiceProxy,

        private _router: Router,
        private _el: ElementRef,
        private spinner: NgxSpinnerService,
        private _postCodeRangeServiceProxy: PostCodeRangeServiceProxy,
        private _jobInstallerInvoiceServiceProxy: JobInstallerInvoicesServiceProxy,
        @Optional() private viewLeadDetail?: ViewMyLeadComponent,
        
    ) {
        super(injector);
        this.invoiceDto = new WholesaleInvoiceDto();
        this._el.nativeElement.setAttribute('autocomplete', 'off');
        this._el.nativeElement.setAttribute('autocorrect', 'off');
        this._el.nativeElement.setAttribute('autocapitalize', 'none');
        this._el.nativeElement.setAttribute('spellcheck', 'false');
    }

    ngOnInit(): void {
        debugger;
        this._commonLookupServiceProxy.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
            // this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
        });

        // this._commonLookupServiceProxy.getAllProductTypeForTableDropdown().subscribe(result => {
        //     this.productTypes = result;
        // });
        this.getProductType();
        this.getInvoiceType();
        this.getJobType();
        this.getPropertyTypes();
        this.getDeliveryOptions();
        this.getJobStatuses();
        this.getTransportTypes();
        this.getPVDStatuses();

        this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
            this.role = result;
        });
    }

    show(invoiceId?: number) { 
        //  if (this.installationItemList.length == 1)
        //      return;
        if (!invoiceId) {
            this.invoiceDto = new WholesaleInvoiceDto();
            this.invoiceDto.id = invoiceId;
            this.installationItemList = [];
            let installationItemCreateOrEdit = { id: "", categoryId: "", ProductName: "", size: 0, model: 0, qty: 0,price: 0 }
            this.installationItemList.push(installationItemCreateOrEdit);

            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Wholesale Invoice';
            log.section = 'Wholesale Invoice';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });

        } else {
            this._wholeSaleInvoiceServiceProxy.getWholeSaleLeadForEdit(invoiceId).subscribe(result => {
                this.invoiceDto = result;
                this.installationItemList = [];
                this.invoiceDto.productDetailDto.map((item) => {
                    let InvoiceItemCreateOrEdit = { id: 0, categoryId: 0, ecommerceProductItemId: 0, productName: "", model: "", qty: 0,price: 0  }
                    InvoiceItemCreateOrEdit.id = item.id;
                    InvoiceItemCreateOrEdit.categoryId = item.categoryId;
                    InvoiceItemCreateOrEdit.ecommerceProductItemId = item.ecommerceProductItemId;
                    // InvoiceItemCreateOrEdit.productName = item.productName;
                    InvoiceItemCreateOrEdit.model = item.model;
                    InvoiceItemCreateOrEdit.qty = item.qty;
                    InvoiceItemCreateOrEdit.price = item.price;
                    this.installationItemList.push(InvoiceItemCreateOrEdit);
                });

                this.active = true;
                this.modal.show();
                
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Wholesale Invoice : ' + this.invoiceDto.name;
                log.section = 'Wholesale Invoice';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }

             
    }

   
    // show(invoiceId?:number) { 
    //     // if (this.installationItemList.length == 1)
    //     //     return;
    //     if (!invoiceId) {
    //         this.invoiceDto = new WholesaleInvoiceDto();
    //         this.invoiceDto.id = invoiceId;
    //         this.installationItemList = [];
    //         let installationItemCreateOrEdit = { id: "", categoryId: "", ProductName: "", size: 0, model: 0, qty: 0,price: 0 }
    //         this.installationItemList.push(installationItemCreateOrEdit);

    //         this.active = true;
    //         this.modal.show();
    //     } else {
    //         this._wholeSaleInvoiceServiceProxy.getWholeSaleLeadForEdit(invoiceId).subscribe(result => {
    //             this.invoiceDto = result;
    //             this.active = true;
    //             this.modal.show();
    //         });
    //     }

             
    // }


    getProductType(){
        this._commonLookupServiceProxy.getAllProductTypeForTableDropdown().subscribe(result =>{
            this.productTypes = result;
        })
    }
    
    getInvoiceType(){
        this._commonLookupServiceProxy.getWholesaleInvoicetypeDropdown().subscribe(result =>{
            this.InvoiceTypes = result;
        })
    }
    
    getJobType(){
        this._commonLookupServiceProxy.getWholesaleJobTypeDropdown().subscribe(result =>{
            this.JobTypes = result;
        })
    }
    
    getDeliveryOptions(){
        this._commonLookupServiceProxy.getWholesaleDelivaryOptionDropdown().subscribe(result =>{
            this.DeliveryOptions= result;
           // result.items[0].invoiceDto.nam,
        })
    }
    
    getJobStatuses(){
        this._commonLookupServiceProxy.getWholesaleJobStatusDropdown().subscribe(result =>{
            this.JobStatuses = result;
        })
    }
    
    getTransportTypes(){
        this._commonLookupServiceProxy.getWholesaleTransportTypeDropdown().subscribe(result =>{
            this.TransportTypes = result;
        })
    }
    
    getPropertyTypes(){
        this._commonLookupServiceProxy.getWholesalePropertyTypeDropdown().subscribe(result =>{
            this.PropertyTypes = result;
        })
    }
    
    getPVDStatuses(){
        this._commonLookupServiceProxy.getWholesalePVDStatusDropdown().subscribe(result =>{
            this.PVDStatuses = result;
        })
    }


    onShown(){}


    addInstallationItem(): void {
        let installationItemCreateOrEdit = { id: "", categoryId: "", ProductName: "", size: 0, model: 0, qty: 0,price: 0}
        this.installationItemList.push(installationItemCreateOrEdit);
    }

    removeInstallationItem(installationItem): void {
        debugger;
         if (this.installationItemList.length == 1)
             return;
       
         if (this.installationItemList.indexOf(installationItem) === -1) {
            
         } else {
             this.installationItemList.splice(this.installationItemList.indexOf(installationItem), 1);
         }
     }


     changeProductType(i) : void {
        this.installationItemList[i].categoryId = "";
        this.installationItemList[i].ecommerceProductItemId = "";
        this.installationItemList[i].productName = "";
        this.installationItemList[i].model = "";
        this.installationItemList[i].qty = 0;
        this.installationItemList[i].price = 0;
    }

    filterProductIteams(event, i): void {
        let Id = this.installationItemList[i].productTypeId;

        this._jobServiceProxy.getPicklistProductItemList(Id, event.query).subscribe(output => {
            //this.productItemSuggestions = output;
            debugger;
            var items =this.installationItemList.map(i=>i.productItemName+'');
            this.productItemSuggestions = output.filter(x=> !items.includes(x.productItem))
        });
    }


    selectProductItem(event, i) {

        this.installationItemList[i].categoryId = event.id;
        this.installationItemList[i].ProductName = event.productItem;
        this.installationItemList[i].model = event.model;
        this.installationItemList[i].expiryDate = moment(event.expiryDate);
        // this.calculateUnitPrice(i);
    }
    

    // get UnitType
    filterunitTypes(event): void {
        this._leadsServiceProxy.getAllUnitType(event.query).subscribe(output => {
            this.unitTypesSuggestions = output;
        });
    }

    // get StreetType
    filterstreettypes(event): void {
        this._leadsServiceProxy.getAllStreetType(event.query).subscribe(output => {
            this.streetTypesSuggestions = output;
        });
    }

    // get StreetName
    filterstreetnames(event): void {
        this._leadsServiceProxy.getAllStreetName(event.query).subscribe(output => {
            this.streetNamesSuggestions = output;
        });
    }

    // get Suburb
    filtersuburbs(event): void {
        this._leadsServiceProxy.getAllSuburb(event.query).subscribe(output => {
            this.suburbSuggestions = output;
        });
    }
    //get state & Postcode
    fillstatepostcode(): void {
        var splitted = this.suburb.split("||");
        this.bindArea();
    }






    close(): void {
        this.Reset();
        this.modal.hide();
        this.saving = false;
    }

    cancel(): void {
        this.Reset();
        this._router.navigate(['/app/main/wholesale/invoice']);     
        
    }

    bindArea(): void {
        
        // if(this.lead.postCode != null && this.lead.postCode != "" && this.lead.postCode != undefined)
        // {
        //     this._postCodeRangeServiceProxy.getAreaByPostCodeRange(this.lead.postCode).subscribe((result) => 
        //     {
        //         this.lead.area = result;
        //     });
        // }
    }
    
     OnSelectDetails(event): void {
         // alert(event.leadId);
        //  this.lead.referralLeadId = event.leadId;
        //  this.lead.referralName = event.customerName;
     }
     // End Referral Details Code
 
     referal: boolean = false;
     LeadSourceOnChange() {
        //  if(this.lead.leadSource == 'Referral'){
 
        //      this.referal = true;
        //  }
        //  else{
        //      this.referal = false;
        //  }
     }

     Reset(): void {
        this.selectAddress = true;
        // this.lead.companyName = undefined;
        // this.lead.leadSource = "";
        // this.lead.area = "";
        // this.lead.type = "";
        // this.lead.email = undefined;
        // this.lead.phone = undefined;
        // this.lead.mobile = undefined;
        // this.lead.altPhone = undefined;
        // this.lead.requirements = undefined;
        // this.lead.solarType = undefined;
        // this.lead.address = undefined;
        // this.lead.address = null;
        // this.lead.unitNo = undefined;
        // this.unitType = undefined;
        // this.lead.streetNo = undefined;
        // this.streetName = undefined;
        // this.streetType = undefined;
        // this.suburb = undefined;
        // this.lead.state = "";
        // this.lead.postCode = undefined;
        // this.lead.country = undefined;
        // this.lead.abn = undefined;
        // this.lead.fax = undefined;
        // this.lead.isGoogle = "Google";
    }

    save(): void {
        this.saving = true;

        // this.installationItemPeriod.startDate = this.startDate;
        // this.installationItemPeriod.endDate = this.endDate;
        
        this.invoiceDto.productDetailDto = [];
        this.installationItemList.map((item) => {
                    let InvoiceItemCreateOrEdit = { id: 0, categoryId: 0, ecommerceProductItemId: 0, productName: "", model: "", qty: 0,price: 0  }
                    InvoiceItemCreateOrEdit.id = item.id;
                    InvoiceItemCreateOrEdit.categoryId = item.categoryId;
                    InvoiceItemCreateOrEdit.ecommerceProductItemId = item.ecommerceProductItemId;
                    InvoiceItemCreateOrEdit.productName = item.productName;
                    InvoiceItemCreateOrEdit.model = item.model;
                    InvoiceItemCreateOrEdit.qty = item.qty;
                    InvoiceItemCreateOrEdit.price = item.price;

                    this.installationItemList.push(InvoiceItemCreateOrEdit);
        });
       
                this._wholeSaleInvoiceServiceProxy.createOrEdit(this.invoiceDto)
                .pipe(finalize(() => { this.saving = false;}))
                .subscribe(() => {
                    this.notify.info(this.l('SavedSuccessfully'));
                    this.close();
                    this.modalSave.emit(null);
                    
                if(this.invoiceDto.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Wholesale Invoice Updated : '+ this.invoiceDto.name;
                    log.section = 'Wholesale Invoice';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Wholesale Invoice Created : '+ this.invoiceDto.name;
                    log.section = 'Wholesale Invoice';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
                });
                
    }
} 
   
