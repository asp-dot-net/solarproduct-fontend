import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize, map, take } from 'rxjs/operators';
import { AuditLogListDto,
     CommonLookupDto, CommonLookupServiceProxy, 
     WholesaleInvoiceDto, WholeSaleInvoiceServiceProxy,
     EcommerceCartProductItemDto,
     OrganizationUnitDto,
     JobsServiceProxy,
    CommonLookupInvoice
    
     } from '@shared/service-proxies/service-proxies';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { filter, result } from 'lodash';
import * as moment from 'moment';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
// import { LazyLoadEvent, Paginator, Table } from 'primeng';
import { id } from '@swimlane/ngx-charts';
import { Paginator } from 'primeng';


@Component({
    selector: 'EditInvoiceModal',
    templateUrl: './edit-invoice-modal.html',
})
export class EditInvoiceModal extends AppComponentBase {
    @ViewChild('EditInvoiceModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>()


    installationItemList: any[];
    productDetailDto: any[];
    productItemSuggestions: any[];
    ExpandedViewApp: boolean = true;
    active = false;  
    saving = false;  
    allOrganizationUnits: OrganizationUnitDto[];
    invoiceDto : WholesaleInvoiceDto = new WholesaleInvoiceDto();
    productTypes : CommonLookupDto[];
    JobTypes = [];
    //JobTypes : CommonLookupDto[];
    DeliveryOptions : CommonLookupDto[];
    InvoiceTypes : CommonLookupDto[] ;
    JobStatuses : CommonLookupDto[];
    TransportTypes : CommonLookupDto[];
    PropertyTypes : CommonLookupDto[];
    PVDStatuses : CommonLookupDto[];
    allLocation : CommonLookupDto[];
    organizationUnitlength: number=0;
    organizationUnit = 0;
    customerId: number;
    customername = "";
    filterName = '';
    filterText = '';
    stateId : number = 0;
    postCodePostalCode2Filter = '';
    addressFilter = '';
   
    leadId = 0;

    firstrowcount = 0;
    last = 0;
    date = new Date();
    firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    invoiceDateNameFilter = 'Creation';
    startDate = moment(this.firstDay);
    endDate = moment().endOf('day');
     @ViewChild('paginator', { static: true }) paginator: Paginator;
   
    constructor(injector: Injector
        , private _dateTimeService: DateTimeService
        ,private _userActivityLogServiceProxy : UserActivityLogServiceProxy
        , private _wholeSaleInvoiceServiceProxy : WholeSaleInvoiceServiceProxy
        ,private _jobServiceProxy: JobsServiceProxy
        , private _commonLookupServiceProxy : CommonLookupServiceProxy

    ) {
        super(injector);
        this.getInvoiceType();
    }


    ngOnInit(): void {
        // debugger;
        this._commonLookupServiceProxy.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
            // this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
        });
        this._commonLookupServiceProxy.getLocationDropdown().subscribe(result => {
            this.allLocation = result;
        })

        this._commonLookupServiceProxy.getAllProductTypeForTableDropdown().subscribe(result => {
            this.productTypes = result;
        });
        //  this.bindInvoice();
         //this.getInvoiceIssued();
    }

   
    show(invoiceId?: number) { 
        debugger;
        if (!invoiceId) {
            this.invoiceDto = new WholesaleInvoiceDto();
            this.invoiceDto.id = invoiceId;

            // this.invoiceDto.organizationUnit = this.allOrganizationUnits[0].id;

            this.installationItemList = [];
                let InvoiceItemCreateOrEdit = {id: "", categoryId: "", ecommerceProductItemId: "", productInvoiceName: "", model: "", qty: 0,price: 0 }
                this.installationItemList.push(InvoiceItemCreateOrEdit);
            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Wholesale Invoice';
            log.section = 'Wholesale Invoice';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });

        } else {
            this._wholeSaleInvoiceServiceProxy.getWholeSaleLeadForEdit(invoiceId).subscribe(result => {
                           this.invoiceDto = result;
                           this.customername = result.customerName;
                          

                this.installationItemList = [];
                this.invoiceDto.productitemDetailDto.map((item) => {
                    let InvoiceItemCreateOrEdit = { id: 0, categoryId: 0, ecommerceProductItemId: 0, productInvoiceName: "", model: "", qty: 0,price: 0  }
                    InvoiceItemCreateOrEdit.id = item.id;
                    InvoiceItemCreateOrEdit.categoryId = item.categoryId;
                    InvoiceItemCreateOrEdit.ecommerceProductItemId = item.ecommerceProductItemId;
                    InvoiceItemCreateOrEdit.productInvoiceName = item.productName;
                    InvoiceItemCreateOrEdit.model = item.model;
                    InvoiceItemCreateOrEdit.qty = item.qty;
                    InvoiceItemCreateOrEdit.price = item.price;
                    

                    this.installationItemList.push(InvoiceItemCreateOrEdit);
                });
                this.caculateTotalamount();
                this.active = true;
                this.modal.show();

                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Wholesale Invoice : ' + this.invoiceDto.name;
                log.section = 'Wholesale Invoice';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });

            
        }

        // this.getProductType();
        this.getInvoiceType();
        this.getJobType();
        this.getPropertyTypes();
        this.getDeliveryOptions();
        this.getJobStatuses();
        this.getTransportTypes();
        this.getPVDStatuses();
        this.bindInvoice();

    }



    save(): void {
        debugger;
        this.saving = true;

        // this.installationItemPeriod.startDate = this.startDate;
        // this.installationItemPeriod.endDate = this.endDate;
        this.invoiceDto.productitemDetailDto = [];
        this.installationItemList.map((item) => {
            let installationItemListDto = new EcommerceCartProductItemDto();
            installationItemListDto.id = item.id;
            installationItemListDto.categoryId = item.categoryId;
            installationItemListDto.ecommerceProductItemId = item.ecommerceProductItemId;
            installationItemListDto.productName = item.productInvoiceName;
            installationItemListDto.model = item.model;
            installationItemListDto.qty = item.qty;
            installationItemListDto.price = item.price;
            // installationItemListDto.expir = item.price;

             this.invoiceDto.productitemDetailDto.push(installationItemListDto);
        });
       
        this._wholeSaleInvoiceServiceProxy.createOrEdit(this.invoiceDto)
         .pipe(finalize(() => { this.saving = false;}))
         .subscribe(() => {
            this.notify.info(this.l('SavedSuccessfully'));
            this.close();
            this.modalSave.emit(null);
            if(this.invoiceDto.id){
                let log = new UserActivityLogDto();
                log.actionId = 82;
                log.actionNote ='Wholesale Invoice Updated : '+ this.invoiceDto.name;
                log.section = 'Wholesale Invoice';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }else{
                let log = new UserActivityLogDto();
                log.actionId = 81;
                log.actionNote ='Wholesale Invoice Created : '+ this.invoiceDto.name;
                log.section = 'Wholesale Invoice';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }
           

         });

    }

   


    close(): void {
        this.active = false;
        this.modal.hide();
    }

     removeInstallationItem(installationItem): void {
        debugger;
         if (this.installationItemList.length == 1)
             return;
       
         if (this.installationItemList.indexOf(installationItem) === -1) {
            
         } else {
             this.installationItemList.splice(this.installationItemList.indexOf(installationItem), 1);
         }
     }


    addInstallationItem(): void {
        debugger;
        let InvoiceItemCreateOrEdit = {id: "", categoryId: "",ecommerceProductItemId: "", productInvoiceName: "", model: "", qty: 0, price: 0}
        this.installationItemList.push(InvoiceItemCreateOrEdit);
    }

    changeProductType(i) : void {
        this.installationItemList[i].ecommerceProductItemId = "";
        this.installationItemList[i].productInvoiceName = "";
        this.installationItemList[i].model = "";
        this.installationItemList[i].size = 0;
        this.installationItemList[i].qty = 0;
        this.installationItemList[i].price = 0;
    }



    filterProductIteams(event, i): void {
        let Id = this.installationItemList[i].categoryId;

        this._commonLookupServiceProxy.getEcommerceProductItemList(Id, event.query,this.invoiceDto.wholesaleLeadId ).subscribe(output => {
            //this.productItemSuggestions = output;

            debugger;
            var items =this.installationItemList.map(i=>i.productInvoiceName+'');
            this.productItemSuggestions = output.filter(x=> !items.includes(x.productItem))
        });
    }


    selectProductItem(event, i) {

        this.installationItemList[i].ecommerceProductItemId = event.id;
        this.installationItemList[i].productInvoiceName = event.productItem;
        this.installationItemList[i].model = event.productModel;
        this.installationItemList[i].qty =  this.installationItemList[i].qty > 1 ?  this.installationItemList[i].qty : 1
        this.installationItemList[i].price = event.price > 0 ?  this.installationItemList[i].qty * event.price : 0;
        this.caculateTotalamount();

        // this.installationItemList[i].expiryDate = moment(event.expiryDate);
        // this.calculateUnitPrice(i);
    }

    invoiceSearchResult: any [];
    filterCustomername(event): void {
        
        if(!event.query){
            this.customerId = 0;
            this.customername = "";
            // this.getInvoiceIssued();
        }
        else{

            this.invoiceSearchResult = Object.assign([], this.InsvoiceList).filter(
                item => item.customerName.toLowerCase().indexOf(event.query.toLowerCase()) > -1  || item.customerName?.toLowerCase().indexOf(event.query.toLowerCase())>-1
            )
        }
        
    }

    InsvoiceList: CommonLookupInvoice[];
    bindInvoice(){
        this.InsvoiceList = [];
        this._wholeSaleInvoiceServiceProxy.getAllInvoiceWithFullName().subscribe(result => {
            this.InsvoiceList = result;
        });
    }
   

    selectInvoice(event): void {
        debugger;
        this.invoiceDto.wholesaleLeadId = event.id;
        this.customername = event.customerName;
        // this.getInvoiceIssued();
    }

   


    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;                
    }
    expandApp() {
        this.ExpandedViewApp = false;
    }

    // getProductType(){
    //     this._commonLookupServiceProxy.getAllProductTypeForTableDropdown().subscribe(result =>{
    //         this.productTypes = result;
    //     })
    // }
    
    getInvoiceType(){
        this._commonLookupServiceProxy.getWholesaleInvoicetypeDropdown().subscribe(result =>{
            this.InvoiceTypes = result;
        })
    }
    
    getJobType(){
        this._commonLookupServiceProxy.getWholesaleJobTypeDropdown().subscribe(result =>{
            this.JobTypes = result;
        })
    }
    
    getDeliveryOptions(){
        this._commonLookupServiceProxy.getWholesaleDelivaryOptionDropdown().subscribe(result =>{
            this.DeliveryOptions= result;
           // result.items[0].invoiceDto.nam,
        })
    }
    
    getJobStatuses(){
        this._commonLookupServiceProxy.getWholesaleJobStatusDropdown().subscribe(result =>{
            this.JobStatuses = result;
        })
    }
    
    getTransportTypes(){
        this._commonLookupServiceProxy.getWholesaleTransportTypeDropdown().subscribe(result =>{
            this.TransportTypes = result;
        })
    }
    
    getPropertyTypes(){
        this._commonLookupServiceProxy.getWholesalePropertyTypeDropdown().subscribe(result =>{
            this.PropertyTypes = result;
        })
    }
    
    getPVDStatuses(){
        this._commonLookupServiceProxy.getWholesalePVDStatusDropdown().subscribe(result =>{
            this.PVDStatuses = result;
        })
    }

    caculateTotalamount() {
        this.invoiceDto.invoiceAmount = 0;
        this.invoiceDto.invoiceAmountWithGST = 0;

        this.installationItemList.forEach(item => {     
            this.invoiceDto.invoiceAmount =  this.invoiceDto.invoiceAmount + ( item.price > 0 ? item.price : 0);
         });

         this.invoiceDto.invoiceAmountWithGST = this.invoiceDto.invoiceAmount + 40;
    }

    calculatePrice(i) {
        this.installationItemList[i].qty =  this.installationItemList[i].qty > 1 ?  this.installationItemList[i].qty : 1
        this.installationItemList[i].price = this.installationItemList[i].price > 0 ?  this.installationItemList[i].qty * this.installationItemList[i].price : 0;
        // this.installationItemList[i].expiryDate = moment(event.expiryDate);
        // this.calculateUnitPrice(i);
    }

    
   
}
