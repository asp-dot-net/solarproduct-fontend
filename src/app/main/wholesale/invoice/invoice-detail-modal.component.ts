import { Component, Injector, ViewChild } from '@angular/core';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
    selector: 'InvoiceDetailModal',
    templateUrl: './invoice-detail-modal.html',
})
export class InvoiceDetailModal extends AppComponentBase {
    @ViewChild('InvoiceDetailModal', { static: true }) modal: ModalDirective;

    ExpandedViewApp: boolean = true;
    active = false;    

    constructor(injector: Injector,private _userActivityLogServiceProxy : UserActivityLogServiceProxy, private _dateTimeService: DateTimeService) {
        super(injector);
    }

    show() {
        this.modal.show();
        // let log = new UserActivityLogDto();
        // log.actionId = 79;
        // log.actionNote ='Opened Wholesale Promotions View : ' + this.item.wholeSalePromotion.title;
        // log.section = 'Wholesale Promotions';
        // this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        // .subscribe(() => {            
        //  });        
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;                
    }
    expandApp() {
        this.ExpandedViewApp = false;
    }
}
