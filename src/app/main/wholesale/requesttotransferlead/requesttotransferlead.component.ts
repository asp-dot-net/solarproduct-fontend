import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { WholeSaleLeadServiceProxy, LeadDto, LeadStatusLookupTableDto, OrganizationUnitDto, UserServiceProxy, LeadStateLookupTableDto, JobStatusTableDto, LeadSourceLookupTableDto, CommonLookupServiceProxy, AssignOrDeleteWholeSaleLeadInput, WholeSaleLeadsSummaryCount} from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import * as _ from 'lodash';
import * as moment from 'moment';
import { OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ViewWholesaleLeadComponent } from '../leads/view-wholesalelead.component';
import { CreateEditWholesaleLeadComponent } from '../leads/create-edit-wholesalelead.component';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';

@Component({
    templateUrl: './requesttotransferlead.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
})
export class RequestToWholeSaleLeadTransferComponent extends AppComponentBase implements OnInit {

    show: boolean = true;
    FiltersData = false;
    showchild: boolean = true;
    shouldShow: boolean = false;
    flag = true;
    organizationUnitlength: number = 0;
    count = 0
    tableRecords: any;
    checkbox:any;
    isSelected: boolean;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };
    excelorcsvfile = 0;

    toggle: boolean = true;

    change() {
        this.toggle = !this.toggle;
      }

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('ViewWholesaleLeadDetail', { static: true }) ViewWholesaleLeadDetail: ViewWholesaleLeadComponent;
    @ViewChild('createEditWholesaleLead', { static: true }) createEditWholesaleLead: CreateEditWholesaleLeadComponent;
        
    
   
    
    StartDate = moment().startOf('month').add(1, 'days');
    EndDate = moment().add(0, 'days').endOf('day');
    PageNo: number;
    ExpandedView: boolean = true;
    // flafValue:boolean = true;
    SelectedLeadId: number = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnit = 0;
    role: string = '';
    leadName: string;
    //type: string;
   // approve: string = '';
    activeFilter ='Transfer';
    approveFilter='Approve';
    wholeSaleLeaIds: number[] = []
    allSalesRap : any[];
    allMove: any[];
    saving = false;
    action = 0;
    firstrowcount = 0;
    changeOrganization = 0;
    SalesRapid = 0;
    last = 0;
    ids: number[] = []
    selectedids: number[] = []
    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 330;
    leadSummary : WholeSaleLeadsSummaryCount[]= [];
    Var_Org = 0;
    constructor(
       
        injector: Injector,
        private _commonLookupService: CommonLookupServiceProxy,
        private titleService: Title,       
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _wholeSaleLeadServiceProxy : WholeSaleLeadServiceProxy
        ) {
            super(injector);
            this.titleService.setTitle(this.appSession.tenancyName + " |  Request To Transfer");
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
        
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.Var_Org = this.allOrganizationUnits[0].id;
            
            this._commonLookupService.getAllUsersByRoleNameTableDropdown('Wholesale SalesRep',this.allOrganizationUnits[0].id).subscribe(result => {
                this.allSalesRap = result;
            })
            // this.bindLeadSource(this.organizationUnit)
            this.getLeads();
        });
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allMove = result;
        });

        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Wholesale Leads Request To Transfer';
        log.section = 'Wholesale Leads Request To Transfer';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
    }

    searchLog() : void {
        
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Wholesale Leads Request To Transfer';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }

        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }



    expandGrid() {
        this.ExpandedView = true;
    }
    clear() {
        
       
        this.StartDate = moment().add(0, 'days').endOf('day');
        this.EndDate = moment().add(0, 'days').endOf('day');
        this.getLeads();
    }


    getLeads(event?: LazyLoadEvent) {
        this.leadSummary = [];
        this.PageNo = this.primengTableHelper.getSkipCount(this.paginator, event);

        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();
        this._wholeSaleLeadServiceProxy.getAllRequestToWholeSaleLeadTransfer(           
            this.StartDate,
            this.EndDate,
            this.organizationUnit,
            this.activeFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            this.shouldShow = false;
            this.tableRecords = result.items;

        },
        err => {
            this.primengTableHelper.hideLoadingIndicator();
        });
    }
    onchange() : void{

        this._commonLookupService.getAllUsersByRoleNameTableDropdown('Wholesale SalesRep',this.Var_Org).subscribe(result => {
            this.allSalesRap = result;
        })

     }
    
   
    reloadPage(flafValue: boolean): void {
        this.ExpandedView = true;
        this.flag = flafValue;
        this.paginator.changePage(this.paginator.getPage());
        if (!flafValue) {
            this.navigateToLeadDetail(this.SelectedLeadId);
        }
    }


    navigateToLeadDetail(leadid): void {
        this.ExpandedView = !this.ExpandedView;
        this.ExpandedView = false;
        this.SelectedLeadId = leadid;
        this.leadName = "wholesaleLead";
        this.ViewWholesaleLeadDetail.showDetail(leadid, this.leadName, 13);
    }

    
    //   Approved(): void{
    //      this._wholeSaleLeadServiceProxy.requestToTransferApproveNotApprove("Approve",this.ids)
    //      .pipe(finalize(() => {  }))
    //          .subscribe(() => {
    //          // this.viewLeadDetail.reloadLead.emit(false);
            
    //          this.notify.info("Successfully Request Approved");
    //          });
        
    
    //  }
     
    //  NotApproved():void{
    //     this._wholeSaleLeadServiceProxy.requestToTransferApproveNotApprove("NotApprove",this.ids)
    //         .pipe(finalize(() => {  }))
    //         .subscribe(() => {
    //             // this.viewLeadDetail.reloadLead.emit(false);
               
    //             this.notify.info("Successfully Request Rejected");
    //         });
    //  }

   
    submit() : void {
        this.ids = [];
        this.saving = true;
           // let transferPermission = this.permission.isGranted("Pages.Leads.LeadTracker.Action.TransferLeftSalesRepLead");
           
           let action = this.action;
           
               this.tableRecords.forEach(item =>{

                   if (item.isSelected == true) {
                    this.ids.push(item.id)
                    //alert('Checkbox must be checked to submit the form');
                       
                   }
               })
               if(this.ids.length == 0)
                {
                    this.notify.warn(this.l('NoLeadsSelected'));
                    this.saving = false;
                    return;
                }

               if (this.activeFilter=='Transfer') {
                   if (this.approveFilter=='Approve') {
                       this._wholeSaleLeadServiceProxy.requestToTransferApproveNotApprove("Approve",this.ids)
                       .pipe(finalize(() => {  }))
                           .subscribe(() => {
                           // this.viewLeadDetail.reloadLead.emit(false);
                          
                           this.notify.info("Successfully Request Approved");
                           });
               
                     // Additional logic for approval
                   } else {
                       this._wholeSaleLeadServiceProxy.requestToTransferApproveNotApprove("UnApproved",this.ids)
                       .pipe(finalize(() => {  }))
                       .subscribe(() => {
                           // this.viewLeadDetail.reloadLead.emit(false);
                          
                           this.notify.info("Request NotApprove");
                       });
              
                     // Additional logic for not approval
                   }
   
               }
               else{
   
                   if (this.approveFilter=='Approve')
                       {
                           this._wholeSaleLeadServiceProxy.requestToMoveSalesRap("Approve",this.organizationUnit,this.SalesRapid,this.ids)
                           .pipe(finalize(() => {  }))
                               .subscribe(() => {
                               // this.viewLeadDetail.reloadLead.emit(false);
                           
                               this.notify.info("Successfully Request Approved");
                               });
                   
                       // Additional logic for approval

                       } else {
                           this._wholeSaleLeadServiceProxy.requestToMoveSalesRap("UnApproved",this.organizationUnit,this.SalesRapid,this.ids)
                           .pipe(finalize(() => {  }))
                           .subscribe(() => {
                               // this.viewLeadDetail.reloadLead.emit(false);
                           
                               this.notify.info("Request NotApprove for requestToMoveSalesRap");
                           });
                       } 
               }    
              
                 // Additional logic for not approval
         
        }    
      
                
    
      oncheckboxCheck() {
      this.count = 0;
          this.tableRecords.forEach(item => {

              if (item.isSelected == true) {
                 this.count = this.count + 1;
              }
          })
      }

     isAllChecked() {
         if (this.tableRecords)
             return this.tableRecords.every(_ => _.isSelected);
     }

     checkAll(ev) {

         this.tableRecords.forEach(x => x.isSelected = ev.target.checked);
         this.oncheckboxCheck();
     }

   
     
}
