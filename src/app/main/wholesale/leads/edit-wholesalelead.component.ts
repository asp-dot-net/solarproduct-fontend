import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {  CommonLookupDto, CommonLookupServiceProxy, CreateOrEditWholeSaleLeadDto, ECommerceProductItemsServiceProxy, EcommerceProductItemDocumentDto, EcommerceProductItemDto, EcommerceProductItemPriceCategoryDto, LeadStateLookupTableDto, LeadsServiceProxy, NameValueOfString, ProductItemsServiceProxy, WholeSaleLeadContactDto, WholeSaleLeadServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import PlaceResult = google.maps.places.PlaceResult;
import { Appearance, GermanAddress, Location } from '@angular-material-extensions/google-maps-autocomplete';
import { ViewDuplicateWholesaleLeadPopUpModalComponent } from './duplicate-wholesalelead.component';

@Component({
    selector: 'EditWholesaleLeadModal',
    templateUrl: './edit-wholesalelead.component.html'
})
export class EditWholesaleLeadModalComponent extends AppComponentBase {

    @ViewChild('editWholesaleLeadModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('ViewDuplicateWholesaleLeadPopUpModal', { static: true }) ViewDuplicateWholesaleLeadPopUpModal: ViewDuplicateWholesaleLeadPopUpModalComponent;

    active = false;
    saving = false;
    
    postalStateName = '';
    delivaryStateName = '';
    
    allStates : LeadStateLookupTableDto[];
    allBDMs : any[];
    allSalesRap : any[];
   
    selectAddress: boolean = true;
    selectAddressDelivery: boolean = true;
    selectAddress2: boolean = true;
    
    DelAddress = true;
    wholeSaleLead : CreateOrEditWholeSaleLeadDto = new CreateOrEditWholeSaleLeadDto();
    allWholesaleStatus : CommonLookupDto[];
    role : string[];

    isBDM = false;
    isSalesRap = false;
    customerTypes =[];
    filteredcustomerType: NameValueOfString[];
    PriceCategory : any[];
    TradingSTCs : any[];
    suburbSuggestions: string[];
    suburb = "";
    IsAdmin = false;
    delivarySuburbSuggestions: string[];
    delivarySuburb = "";

    constructor(
        injector: Injector,
        private  _eCommerceProductItemsServiceProxy: ECommerceProductItemsServiceProxy,
        private _commonLookupServiceProxy : CommonLookupServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _wholeSaleLeadServiceProxy : WholeSaleLeadServiceProxy,
        private _el: ElementRef,
        private _leadsServiceProxy: LeadsServiceProxy,
    ) {
        super(injector);
        this._el.nativeElement.setAttribute('autocomplete', 'off');
        this._el.nativeElement.setAttribute('autocorrect', 'off');
        this._el.nativeElement.setAttribute('autocapitalize', 'none');
        this._el.nativeElement.setAttribute('spellcheck', 'false');
    }

    show(id? : number, oid? :number ): void {

        this._commonLookupServiceProxy.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        })
        this._commonLookupServiceProxy.getWholesaleStatusDropdown().subscribe(result => {
            this.allWholesaleStatus = result;
        })
        if(id > 0){
            this._wholeSaleLeadServiceProxy.getWholeSaleLeadForEdit(id).subscribe(result => {
                this.wholeSaleLead = result;
                this.suburb = result.postalSuburb;
                this.delivarySuburb = result.delivarySuburb;

                if(result.wholeSaleLeadContacts.length <= 0){
                    //this.eCommerceProductItem.ecommerceProductItemPriceCategories = [];
                    let contact = new WholeSaleLeadContactDto();
                    this.wholeSaleLead.wholeSaleLeadContacts.push(contact);
                }
            })
          
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Edit Wholesale Leads : ' + this.wholeSaleLead.firstName;
            log.section = 'Wholesale Leads';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
           
        }
        else{
            this.wholeSaleLead = new CreateOrEditWholeSaleLeadDto();
            this.wholeSaleLead.organizationId = oid;
            this.wholeSaleLead.wholeSaleLeadContacts = [];
            let contact = new WholeSaleLeadContactDto();
            this.wholeSaleLead.wholeSaleLeadContacts.push(contact);
            
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Wholesale Leads';
            log.section = 'Wholesale Leads';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
        }
        this._commonLookupServiceProxy.getAllUsersByRoleNameTableDropdown('Wholesale Manager',oid).subscribe(result => {
            this.allBDMs = result;
        })
        this._commonLookupServiceProxy.getAllUsersByRoleNameTableDropdown('Wholesale SalesRep',oid).subscribe(result => {
            this.allSalesRap = result;
        })
        this._commonLookupServiceProxy.getCurrentUserRoles().subscribe(result => {
            this.role = result;
            this.IsAdmin =result.includes('Admin') ? true : false;
            this.isBDM = result.includes('Wholesale Manager') ? true : false;
            this.isSalesRap = result.includes('Wholesale SalesRep') ? true : false;
            if(this.isBDM == true){
                this.wholeSaleLead.bdmId = this.appSession.userId;
            }
            if(this.isSalesRap == true){
                this.wholeSaleLead.salesRapId = this.appSession.userId;
            }
            
            // this._leadsServiceProxy.getCurrentUserId().subscribe(r => {
            //     if(this.isBDM == true){
            //         this.wholeSaleLead.bdmId = r;
            //     }
            //     if(this.isSalesRap == true){
            //         this.wholeSaleLead.salesRapId =r;
            //     }
            // });
        });
        this._commonLookupServiceProxy.getEcommercePriceCategoryDropdown().subscribe(result => {
            this.PriceCategory = result;
        });
        this._commonLookupServiceProxy.getTradingSTCWhomDropdown().subscribe(result => {
            this.TradingSTCs = result;
        });

        if(this.wholeSaleLead.customerType){
            var ctype = this.wholeSaleLead.customerType.split(',');
            this.customerTypes = [];
            ctype.forEach(element => {
                debugger;
                this.customerTypes.push(element);
            });
        }
        
        this.active = true;
        this.modal.show();
    }

    filtercustomerTypes(event): void {
        this._commonLookupServiceProxy.getCustomerTypes(event.query).subscribe(result => {
            this.filteredcustomerType = result;
        });
    }
    filtersuburbs(event): void {
        this._leadsServiceProxy.getAllSuburb(event.query).subscribe(output => {
            this.suburbSuggestions = output;
        });
    }
    fillstatepostcode(): void {
        var splitted = this.suburb.split("||");
        this.wholeSaleLead.postalSuburb = splitted[0];
        this.wholeSaleLead.postalState = splitted[1].trim();
        this.wholeSaleLead.postalPostCode = splitted[2].trim();
        //this.wholeSaleLead.postalPostCode = "AUSTRALIA";
        // this._leadsServiceProxy.getareaBysuburbPostandstate(this.lead.suburb, this.lead.state, this.lead.postCode).subscribe(result => {
        //     if (result != null) {
        //         this.lead.area = result;
        //     }
        // });

       // this.bindArea();
    }
    delivaryfiltersuburbs(event): void {
        this._leadsServiceProxy.getAllSuburb(event.query).subscribe(output => {
            this.delivarySuburbSuggestions = output;
        });
    }
    delivaryfillstatepostcode(): void {
        var splitted = this.suburb.split("||");
        this.wholeSaleLead.delivarySuburb = splitted[0];
        this.wholeSaleLead.delivaryState = splitted[1].trim();
        this.wholeSaleLead.delivaryPostCode = splitted[2].trim();
        //this.wholeSaleLead.postalPostCode = "AUSTRALIA";
        // this._leadsServiceProxy.getareaBysuburbPostandstate(this.lead.suburb, this.lead.state, this.lead.postCode).subscribe(result => {
        //     if (result != null) {
        //         this.lead.area = result;
        //     }
        // });

       // this.bindArea();
    }
    onShown(): void {
        
        document.getElementById('wLead_abn').focus();
    }
    removeContactItem(contactItem): void {
        if (this.wholeSaleLead.wholeSaleLeadContacts.length == 1)
            return;
       
        if (this.wholeSaleLead.wholeSaleLeadContacts.indexOf(contactItem) === -1) {
            
        } else {
            this.wholeSaleLead.wholeSaleLeadContacts.splice(this.wholeSaleLead.wholeSaleLeadContacts.indexOf(contactItem), 1);
        }
    }

    addContactItem(): void {
        let newcontact = new WholeSaleLeadContactDto();
        this.wholeSaleLead.wholeSaleLeadContacts.push(newcontact);
    }

    close(): void {
        this.Reset();
        this.modal.hide();
        this.saving = false;
    }

    cancel(): void {
        this.Reset();
        this.modal.hide();
        this.saving = false;
        
    }

    bindArea(): void {
        
        // if(this.wholeSaleLead.postCode != null && this.wholeSaleLead.postCode != "" && this.wholeSaleLead.postCode != undefined)
        // {
        //     this._postCodeRangeServiceProxy.getAreaByPostCodeRange(this.wholeSaleLead.postCode).subscribe((result) => 
        //     {
        //         this.wholeSaleLead.area = result;
        //     });
        // }
    }

     OnSelectDetails(event): void {
         // alert(event.leadId);
        //  this.wholeSaleLead.referralLeadId = event.leadId;
        //  this.wholeSaleLead.referralName = event.customerName;
     }
     // End Referral Details Code
 
     referal: boolean = false;
     LeadSourceOnChange() {
        //  if(this.wholeSaleLead.leadSource == 'Referral'){
 
        //      this.referal = true;
        //  }
        //  else{
        //      this.referal = false;
        //  }
     }


    Reset(): void {
        this.wholeSaleLead = new CreateOrEditWholeSaleLeadDto();
    }

    google(): void {
        debugger;
        this.selectAddress = true;
    }

    database(): void {
        this.selectAddress = false;
    }

    googleDelivery(): void {
        debugger;
        this.selectAddressDelivery = true;
    }

    databaseDelivery(): void {
        this.selectAddressDelivery = false;
    }

    onAutocompleteSelected(result: PlaceResult) {
        debugger;
        if (result.address_components.length == 7) {
            this.wholeSaleLead.postalUnitNo = "";
          
            this.wholeSaleLead.postalUnitType = "";
            this.wholeSaleLead.postalStreetNo = "";
            this.postalStateName = "";
            this.wholeSaleLead.postalStateId = 0;
            this.wholeSaleLead.postalPostCode = "";
            this.wholeSaleLead.postalAddress = "";
            this.wholeSaleLead.postalSuburb ="";
            this.wholeSaleLead.postalSuburbId = 0
            this.suburb ="";

            // this.wholeSaleLead.country = "";

            this.wholeSaleLead.postalStreetNo = result.address_components[0].long_name.toUpperCase();
            var str = result.address_components[1].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.wholeSaleLead.postalStreetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.wholeSaleLead.postalStreetType = splitted[3].toUpperCase().trim();
            }
            else if (splitted.length == 3) {
                this.wholeSaleLead.postalStreetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.wholeSaleLead.postalStreetType = splitted[2].toUpperCase().trim();
            }
            else {
                this.wholeSaleLead.postalStreetName = splitted[0].toUpperCase().trim();
                this.wholeSaleLead.postalStreetType = splitted[1].toUpperCase().trim();
            }
            this.suburb = result.address_components[2].long_name.toUpperCase().trim();
            this.postalStateName = result.address_components[4].short_name;
            this._leadsServiceProxy.getSuburbId(result.address_components[6].long_name).subscribe(result => {
                this.wholeSaleLead.postalSuburbId = result;
            });
            this._leadsServiceProxy.stateId(result.address_components[4].short_name).subscribe(result => {
                this.wholeSaleLead.postalStateId = result;
            });
            this.wholeSaleLead.postalPostCode = result.address_components[6].long_name.toUpperCase();
            this.wholeSaleLead.postalAddress = this.wholeSaleLead.postalUnitNo + " " + this.wholeSaleLead.postalUnitType + " " + this.wholeSaleLead.postalStreetNo + " " + this.wholeSaleLead.postalStreetName + " " + this.wholeSaleLead.postalStreetType;  //result.formatted_address;
            // this.wholeSaleLead.country = result.address_components[5].long_name.toUpperCase();
        }
        else if (result.address_components.length == 6) {
            this.wholeSaleLead.postalUnitNo = "";
            this.wholeSaleLead.postalUnitType = "";
           
            this.wholeSaleLead.postalStreetNo = "";
            this.wholeSaleLead.postalStreetName = "";
            this.wholeSaleLead.postalStreetType = "";
            
            this.wholeSaleLead.postalSuburb ="";
            this.wholeSaleLead.postalSuburbId = 0
            this.suburb ="";
            
            this.postalStateName= "";
            this.wholeSaleLead.postalStateId = 0;
            this.wholeSaleLead.postalPostCode = "";
            this.wholeSaleLead.postalAddress = "";
            // this.wholeSaleLead.country = "";

            this.wholeSaleLead.postalStreetNo = result.address_components[0].long_name.toUpperCase();
            var str = result.address_components[1].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.wholeSaleLead.postalStreetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.wholeSaleLead.postalStreetType = splitted[3].toUpperCase().trim();
            }
            else if (splitted.length == 3) {
                this.wholeSaleLead.postalStreetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.wholeSaleLead.postalStreetType = splitted[2].toUpperCase().trim();
            }
            else {
                this.wholeSaleLead.postalStreetName = splitted[0].toUpperCase().trim();
                this.wholeSaleLead.postalStreetType = splitted[1].toUpperCase().trim();
            }
            this.suburb = result.address_components[2].long_name.toUpperCase().trim();
            this.postalStateName = result.address_components[3].short_name;
            this._leadsServiceProxy.getSuburbId(result.address_components[5].long_name).subscribe(result => {
                this.wholeSaleLead.postalSuburbId = result;
            });
            this._leadsServiceProxy.stateId(result.address_components[3].short_name).subscribe(result => {
                this.wholeSaleLead.postalStateId = result;
            });
            this.wholeSaleLead.postalPostCode = result.address_components[5].long_name.toUpperCase();
            this.wholeSaleLead.postalAddress = this.wholeSaleLead.postalUnitNo + " " + this.wholeSaleLead.postalUnitType + " " + this.wholeSaleLead.postalStreetNo + " " + this.wholeSaleLead.postalStreetName + " " + this.wholeSaleLead.postalStreetType;  //result.formatted_address;
            // this.wholeSaleLead.country = result.address_components[4].long_name.toUpperCase();
        }
        else {
            this.wholeSaleLead.postalUnitNo = "";
            this.wholeSaleLead.postalUnitType = "";
            this.wholeSaleLead.postalStreetNo = "";
            this.wholeSaleLead.postalStreetName = "";
            this.wholeSaleLead.postalStreetType = "";
            this.postalStateName = "";
            this.wholeSaleLead.postalStateId = 0;
            this.wholeSaleLead.postalPostCode = "";
            this.wholeSaleLead.postalAddress = "";
            
            this.wholeSaleLead.postalSuburb ="";
            this.wholeSaleLead.postalSuburbId = 0
            this.suburb ="";

            var str1 = result.address_components[0].long_name.toUpperCase();
            var splitted1 = str1.split(" ");
            if (splitted1.length == 1) {
                this.wholeSaleLead.postalUnitNo = splitted1[0].toUpperCase().trim();
            }
            else {
                this.wholeSaleLead.postalUnitNo = splitted1[1].toUpperCase().trim();
                // this.wholeSaleLead.postalUnitType = splitted1[0].toUpperCase().trim();
                this.wholeSaleLead.postalUnitType = splitted1[0].toUpperCase().trim();
            }
            this.wholeSaleLead.postalStreetNo = result.address_components[1].long_name.toUpperCase();
            var str = result.address_components[2].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.wholeSaleLead.postalStreetNo = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.wholeSaleLead.postalStreetType = splitted[3].toUpperCase().trim();
            }
            else if (splitted.length == 3) {
                this.wholeSaleLead.postalStreetNo = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.wholeSaleLead.postalStreetType = splitted[2].toUpperCase().trim();
            }
            else {
                this.wholeSaleLead.postalStreetNo = splitted[0].toUpperCase().trim();
                this.wholeSaleLead.postalStreetType = splitted[1].toUpperCase().trim();
            }
             this.suburb = result.address_components[3].long_name.toUpperCase().trim();
            this.postalStateName= result.address_components[5].short_name;
            this._leadsServiceProxy.getSuburbId(result.address_components[7].long_name).subscribe(result => {
                this.wholeSaleLead.postalSuburbId = result;
            });
            this._leadsServiceProxy.stateId(result.address_components[5].short_name).subscribe(result => {
                this.wholeSaleLead.postalStateId = result;
            });
            this.wholeSaleLead.postalPostCode = result.address_components[7].long_name.toUpperCase();
            this.wholeSaleLead.postalAddress = this.wholeSaleLead.postalUnitNo + " " + this.wholeSaleLead.postalUnitType + " " + this.wholeSaleLead.postalStreetNo + " " + this.wholeSaleLead.postalStreetName + " " + this.wholeSaleLead.postalStreetType;  //result.formatted_address;
            // this.wholeSaleLead.country = result.address_components[6].long_name.toUpperCase();
        }
        this.bindArea();
    }

    onLocationSelected(location: Location) {
        debugger;
        this.wholeSaleLead.postalLatitude = location.latitude.toString();
        this.wholeSaleLead.postalLongitude = location.longitude.toString();
    }

    onAutocompleteSelectedDel(result: PlaceResult) {
        debugger;
        if (result.address_components.length == 7) {
            this.wholeSaleLead.delivaryUnitNo = "";
          
            this.wholeSaleLead.delivaryUnitType = "";
            this.wholeSaleLead.delivaryStreetNo = "";
            this.delivaryStateName = "";
            this.wholeSaleLead.delivaryStateId = 0;
            this.wholeSaleLead.delivaryPostCode = "";
            this.wholeSaleLead.delivaryAddress = "";
            // this.wholeSaleLead.country = "";

            this.wholeSaleLead.delivarySuburb ="";
            this.wholeSaleLead.delivarySuburbId = 0
            this.delivarySuburb ="";

            this.wholeSaleLead.delivaryStreetNo = result.address_components[0].long_name.toUpperCase();
            var str = result.address_components[1].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.wholeSaleLead.delivaryStreetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.wholeSaleLead.delivaryStreetType = splitted[3].toUpperCase().trim();
            }
            else if (splitted.length == 3) {
                this.wholeSaleLead.delivaryStreetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.wholeSaleLead.delivaryStreetType = splitted[2].toUpperCase().trim();
            }
            else {
                this.wholeSaleLead.delivaryStreetName = splitted[0].toUpperCase().trim();
                this.wholeSaleLead.delivaryStreetType = splitted[1].toUpperCase().trim();
            }
             this.delivarySuburb = result.address_components[2].long_name.toUpperCase().trim();
            this.delivaryStateName = result.address_components[4].short_name;
            this._leadsServiceProxy.getSuburbId(result.address_components[6].long_name).subscribe(result => {
                this.wholeSaleLead.delivarySuburbId = result;
            });
            this._leadsServiceProxy.stateId(result.address_components[4].short_name).subscribe(result => {
                this.wholeSaleLead.delivaryStateId = result;
            });
            this.wholeSaleLead.delivaryPostCode = result.address_components[6].long_name.toUpperCase();
            this.wholeSaleLead.delivaryAddress = this.wholeSaleLead.delivaryUnitNo + " " + this.wholeSaleLead.delivaryUnitType + " " + this.wholeSaleLead.delivaryStreetNo + " " + this.wholeSaleLead.delivaryStreetName + " " + this.wholeSaleLead.delivaryStreetType;  //result.formatted_address;
            // this.wholeSaleLead.country = result.address_components[5].long_name.toUpperCase();
        }
        else if (result.address_components.length == 6) {
            this.wholeSaleLead.delivaryUnitNo = "";
            this.wholeSaleLead.delivaryUnitType = "";
           
            this.wholeSaleLead.delivaryStreetNo = "";
            this.wholeSaleLead.delivaryStreetName = "";
            this.wholeSaleLead.delivaryStreetType = "";
            
            this.delivaryStateName= "";
            this.wholeSaleLead.delivaryStateId = 0;
            this.wholeSaleLead.delivaryPostCode = "";
            this.wholeSaleLead.delivaryAddress = "";
            // this.wholeSaleLead.country = "";

            this.wholeSaleLead.delivarySuburb ="";
            this.wholeSaleLead.delivarySuburbId = 0
            this.delivarySuburb ="";

            this.wholeSaleLead.delivaryStreetNo = result.address_components[0].long_name.toUpperCase();
            var str = result.address_components[1].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.wholeSaleLead.delivaryStreetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.wholeSaleLead.delivaryStreetType = splitted[3].toUpperCase().trim();
            }
            else if (splitted.length == 3) {
                this.wholeSaleLead.delivaryStreetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.wholeSaleLead.delivaryStreetType = splitted[2].toUpperCase().trim();
            }
            else {
                this.wholeSaleLead.delivaryStreetName = splitted[0].toUpperCase().trim();
                this.wholeSaleLead.delivaryStreetType = splitted[1].toUpperCase().trim();
            }
            this.delivarySuburb = result.address_components[2].long_name.toUpperCase().trim();
            this.delivaryStateName = result.address_components[3].short_name;
            this._leadsServiceProxy.getSuburbId(result.address_components[5].long_name).subscribe(result => {
                this.wholeSaleLead.delivarySuburbId = result;
            });
            this._leadsServiceProxy.stateId(result.address_components[3].short_name).subscribe(result => {
                this.wholeSaleLead.delivaryStateId = result;
            });
            this.wholeSaleLead.delivaryPostCode = result.address_components[5].long_name.toUpperCase();
            this.wholeSaleLead.delivaryAddress = this.wholeSaleLead.delivaryUnitNo + " " + this.wholeSaleLead.delivaryUnitType + " " + this.wholeSaleLead.delivaryStreetNo + " " + this.wholeSaleLead.delivaryStreetName + " " + this.wholeSaleLead.delivaryStreetType;  //result.formatted_address;
            // this.wholeSaleLead.country = result.address_components[4].long_name.toUpperCase();
        }
        else {
            this.wholeSaleLead.delivaryUnitNo = "";
            this.wholeSaleLead.delivaryUnitType = "";
            this.wholeSaleLead.delivaryStreetNo = "";
            this.wholeSaleLead.delivaryStreetName = "";
            this.wholeSaleLead.delivaryStreetType = "";
            this.delivaryStateName = "";
            this.wholeSaleLead.delivaryStateId = 0;
            this.wholeSaleLead.delivaryPostCode = "";
            this.wholeSaleLead.delivaryAddress = "";

            this.wholeSaleLead.delivarySuburb ="";
            this.wholeSaleLead.delivarySuburbId = 0
            this.delivarySuburb ="";

            var str1 = result.address_components[0].long_name.toUpperCase();
            var splitted1 = str1.split(" ");
            if (splitted1.length == 1) {
                this.wholeSaleLead.delivaryUnitNo = splitted1[0].toUpperCase().trim();
            }
            else {
                this.wholeSaleLead.delivaryUnitNo = splitted1[1].toUpperCase().trim();
                // this.wholeSaleLead.delivaryUnitType = splitted1[0].toUpperCase().trim();
                this.wholeSaleLead.delivaryUnitType = splitted1[0].toUpperCase().trim();
            }
            this.wholeSaleLead.delivaryStreetNo = result.address_components[1].long_name.toUpperCase();
            var str = result.address_components[2].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.wholeSaleLead.delivaryStreetNo = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.wholeSaleLead.delivaryStreetType = splitted[3].toUpperCase().trim();
            }
            else if (splitted.length == 3) {
                this.wholeSaleLead.delivaryStreetNo = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.wholeSaleLead.delivaryStreetType = splitted[2].toUpperCase().trim();
            }
            else {
                this.wholeSaleLead.delivaryStreetNo = splitted[0].toUpperCase().trim();
                this.wholeSaleLead.delivaryStreetType = splitted[1].toUpperCase().trim();
            }
            this.suburb = result.address_components[3].long_name.toUpperCase().trim();
            this.delivaryStateName= result.address_components[5].short_name;
            this._leadsServiceProxy.getSuburbId(result.address_components[7].long_name).subscribe(result => {
                this.wholeSaleLead.delivarySuburbId = result;
            });
            this._leadsServiceProxy.stateId(result.address_components[5].short_name).subscribe(result => {
                this.wholeSaleLead.delivaryStateId = result;
            });
            this.wholeSaleLead.delivaryPostCode = result.address_components[7].long_name.toUpperCase();
            this.wholeSaleLead.delivaryAddress = this.wholeSaleLead.delivaryUnitNo + " " + this.wholeSaleLead.delivaryUnitType + " " + this.wholeSaleLead.delivaryStreetNo + " " + this.wholeSaleLead.delivaryStreetName + " " + this.wholeSaleLead.delivaryStreetType;  //result.formatted_address;
            // this.wholeSaleLead.country = result.address_components[6].long_name.toUpperCase();
        }
        this.bindArea();
    }

    onLocationSelectedDel(location: Location) {
        debugger;
        this.wholeSaleLead.delivaryLatitude = location.latitude.toString();
        this.wholeSaleLead.delivaryLongitude = location.longitude.toString();
    }
    save(): void {
        this.saving = true;
        
        this.wholeSaleLead.customerType = "";
        this.customerTypes.forEach(element => {
            debugger;
            this.wholeSaleLead.customerType = this.wholeSaleLead.customerType + element.name +","
        });

        this.wholeSaleLead.isDelivery = this.DelAddress;
        this.wholeSaleLead.postalSuburb = this.suburb;

        this._wholeSaleLeadServiceProxy.checkExistWholeSaleLeadsList(this.wholeSaleLead)
        .pipe(finalize(() => {this.saving = false;}))
        .subscribe(dupresult => {
            debugger;
            if (dupresult.length > 0) {
                // this.message.confirm('Do You Still Want to Proceed',
                // "Looks Like Duplicate Record",
                // (isConfirmed) => {
                //      if (isConfirmed) {
                //         this._wholeSaleLeadServiceProxy.createOrEdit(this.wholeSaleLead)
                //         .pipe(finalize(() => { this.saving = false; }))
                //         .subscribe(() => {
                //              // this.viewLeadDetail.reloadLead.emit(false);
                //              this.saving = false; 
                //              this.notify.info(this.l('SavedSuccessfully'));
                //              this.close()
                //             });
                //     }
                //     else {
                //         this.saving = false;
                //     }
                // }
                // );
                this.ViewDuplicateWholesaleLeadPopUpModal.show(dupresult,this.wholeSaleLead,this.IsAdmin);

            }
            else{
                this.ViewDuplicateWholesaleLeadPopUpModal.savedata = true;
                this.createoredit()
                // this._wholeSaleLeadServiceProxy.createOrEdit(this.wholeSaleLead)
                // .pipe(finalize(() => { this.saving = false; }))
                // .subscribe(() => {
                //     // this.viewLeadDetail.reloadLead.emit(false);
                    
                //     this.saving = false; 
                //     this.notify.info(this.l('SavedSuccessfully'));
                //     this.close()
                // });
            }
        })
    }

createoredit() :void{
    debugger;
    if(this.ViewDuplicateWholesaleLeadPopUpModal.savedata == true){
        this._wholeSaleLeadServiceProxy.createOrEdit(this.wholeSaleLead)
        .pipe(finalize(() => { this.saving = false; }))
        .subscribe(() => {
            // this.viewLeadDetail.reloadLead.emit(false);
            
            this.saving = false; 
            this.notify.info(this.l('SavedSuccessfully'));
            if(this.wholeSaleLead.id){
                let log = new UserActivityLogDto();
                log.actionId = 82;
                log.actionNote ='Wholesale Leads Updated : '+ this.wholeSaleLead.firstName;
                log.section = 'Wholesale Leads';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }else{
                let log = new UserActivityLogDto();
                log.actionId = 81;
                log.actionNote ='Wholesale Leads Created : '+ this.wholeSaleLead.firstName;
                log.section = 'Wholesale Leads';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }
            this.close()
        });
    }
    
}
    
    
}
