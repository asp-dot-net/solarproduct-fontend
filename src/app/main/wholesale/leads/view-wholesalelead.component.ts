﻿import { Component, ViewChild, Injector, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { LeadsServiceProxy, GetWholeSaleLeadForViewDto, LeadDto,WholeSaleInvoiceServiceProxy, GetActivityLogViewDto, GetLeadForChangeStatusOutput, OrganizationUnitServiceProxy, OrganizationUnitMapDto, GetCallHistoryViewDto, CallHistoryServiceProxy, CommonLookupServiceProxy, WholeSaleLeadServiceProxy, CreateOrEditWholeSaleLeadDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
//import { AddActivityModalComponent } from './add-activity-model.component';
import * as moment from 'moment';
import { JobsComponent } from '@app/main/jobs/jobs/jobs.component';
import { AgmCoreModule } from '@agm/core';
import PlaceResult = google.maps.places.PlaceResult;
import { Appearance, GermanAddress, Location } from '@angular-material-extensions/google-maps-autocomplete';
import { LazyLoadEvent } from 'primeng/public_api';
import { CreateEditWholesaleLeadComponent } from '@app/main/wholesale/leads/create-edit-wholesalelead.component';
import { PromotionStopResponceComponent } from '@app/main/promotions/promotionUsers/stop-responce.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { DatePipe } from '@angular/common';
import { WholeSaleActivityLogComponent } from './wholesale-activity-log.component';
import { WholeSaleActivityLogDetailComponent } from './wholesale-activity-log-detail.component';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, WholesaleInvoiceDto} from '@shared/service-proxies/service-proxies';
@Component({
    selector: 'ViewWholesaleLeadDetail',
    templateUrl: './view-wholesalelead.component.html',
    //styleUrls: ['./myleads.component.less'],
    animations: [appModuleAnimation()]
})
export class ViewWholesaleLeadComponent extends AppComponentBase implements OnInit {


    //@ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    //@ViewChild('jobCreateOrEdit', { static: true }) jobCreateOrEdit: JobsComponent;
    @ViewChild('createEditLead', { static: true }) createEditLead: CreateEditWholesaleLeadComponent;
    @ViewChild('wholeSaleactivityLog', { static: true }) wholeSaleactivityLog: WholeSaleActivityLogComponent;
    @ViewChild('wholeSaleactivityDetailLog', { static: true }) wholeSaleactivityDetailLog: WholeSaleActivityLogDetailComponent;
    @ViewChild('promoStopRespmodel', { static: true }) promot: PromotionStopResponceComponent;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    @Input() SelectedLeadId: number = 0;
    @Output() reloadLead = new EventEmitter<boolean>();
    active = false;
    saving = false;
    item: GetWholeSaleLeadForViewDto;
    activityLog: GetActivityLogViewDto[];
    activeTabIndex: number = 0;
    activeTabIndexForPromotion: number = 0;
    activityDatabyDate: any[] = [];
    actionId: number = 0;
    activity: boolean = false;
    leadActivityList: GetActivityLogViewDto[];
    lat: number;
    lng: number;
    zoom: 20;
    showforresult = false;
    name: string;
    role: string = '';
    mainsearch = false;
    sectionId = 0;
    showsectionwise = false;
    leadId = 0;
    OrganizationMapDto: OrganizationUnitMapDto[];
    mapSrc: string = "";
   WholesaleInvoiceDto: WholesaleInvoiceDto[] = [];
    selectedMap: string;

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _wholeSaleleadsServiceProxy: WholeSaleLeadServiceProxy,
        private _leadsServiceProxy : LeadsServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private spinner: NgxSpinnerService,
        private _organizationUnitServiceProxy : OrganizationUnitServiceProxy,
        public datepipe: DatePipe,
        private _callHistoryService: CallHistoryServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _wholeSaleInvoiceServiceProxy : WholeSaleInvoiceServiceProxy
    ) {
        super(injector);
        this.item = new GetWholeSaleLeadForViewDto();
        this.item.createOrEditWholeSaleLeadDto = new CreateOrEditWholeSaleLeadDto();
    }

    ngOnInit(): void {
        this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
            this.role = result;
        });

        if (this.SelectedLeadId > 0)
            this.showDetail(this.SelectedLeadId);
        this.registerToEvents();
    }

    getParsedDate(strDate) {//get date formate
        if (strDate == "" || strDate == null || strDate == undefined) {
            return;
        }
        let month_names = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

        var strSplitDate = String(strDate._i).split('T');

        var dateArray = strSplitDate[0].split('-');
        let monthint = parseInt(dateArray[1]);
        let date = month_names[monthint - 1] + " " + dateArray[2];
        return date;

    }

    showDetail(leadId: number, LeadName?: string, sectionId?: number): void {
        debugger;
        this.leadId = leadId;
        this.SelectedLeadId =leadId;
        if (sectionId == 15) {
            // this.activeTabIndexForPromotion = 0;
            this.activeTabIndex = 1;
        }
        else if (sectionId == 28) 
        {
            this.activeTabIndex = 1;
        } else {
            this.activeTabIndex = 0;
            // this.activeTabIndexForPromotion = 1;
        }

        this.sectionId = sectionId;
        if (this.sectionId == 0 || this.sectionId > 12) {
            this.showsectionwise = true;
        }

        if (sectionId == 30) {
            this.mainsearch = true;
            this.showsectionwise = false;
        }

        this.name = LeadName;
        let that = this;
        if (leadId != null) {
            this.spinner.show();
            this._wholeSaleleadsServiceProxy.getWholeSaleLeadForView(leadId).subscribe(result => {
                this.item = result;
                this.lng = parseFloat(result.longitude);
                this.lat = parseFloat(result.latitude);

                this._commonLookupService.getOrganizationMapById(result.createOrEditWholeSaleLeadDto.organizationId).subscribe((result) => {
                    this.OrganizationMapDto = result;
                    this.selectedMap = 'GoogleMap';

                    if(this.selectedMap != 'GoogleMap')
                    {
                        this.bindMap();
                    }
                });

                //this.jobCreateOrEdit.getJobDetailByLeadId(this.item.createOrEditWholeSaleLeadDto, this.sectionId);
                this.wholeSaleactivityLog.show(this.item.createOrEditWholeSaleLeadDto.id, 0, this.sectionId);

                if(this.permission.isGranted('Pages.LeadDetails.CallHistory'))
                {
                    this.getCallHistory();
                }

                this.spinner.hide();
            }, err => {
                this.spinner.hide();
            }
            );

        }
        
        this._wholeSaleInvoiceServiceProxy.getWholesaleInvoicesByLeadId(this.leadId).subscribe((invoiceResult) => {
            this.WholesaleInvoiceDto = invoiceResult;
        }, err => {
            console.error('Error fetching wholesale invoices:', err);
        });
            
    }

    deleteLead(lead: CreateOrEditWholeSaleLeadDto): void {
        // this.message.confirm(
        //     '',
        //     this.l('AreYouSure'),
        //     (isConfirmed) => {
        //         if (isConfirmed) {
        //             this._wholeSaleleadsServiceProxy.deleteLeads(lead.id)
        //                 .subscribe(() => {
        //                     this.notify.success(this.l('SuccessfullyDeleted'));
        //                     this.reloadLead.emit(true);
        //                 });
        //         }
        //     }
        // );
    }

    myHistory: boolean = false;
    callType: any = '';
    callHistories: GetCallHistoryViewDto[];

    getCallHistory()
    {
        if(this.permission.isGranted("Pages.LeadDetails.CallHistory"))
        {
            this._callHistoryService.getWholeSaleCallHistory(this.item.createOrEditWholeSaleLeadDto.id, this.callType, this.myHistory).subscribe((result) => {
                this.callHistories = result;
            });
        }
    }

    warm(lead: CreateOrEditWholeSaleLeadDto): void {
        let status: GetLeadForChangeStatusOutput = new GetLeadForChangeStatusOutput();
        status.id = lead.id;
        status.leadStatusID = 4;
        this.message.confirm('',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._wholeSaleleadsServiceProxy.changeStatus(status)
                        .subscribe(() => {
                            this.notify.success(this.l('LeadStatusChangedToWarm'));
                            //this._router.navigate(['/app/main/myleads/myleads']);
                            this.modalSave.emit(null);
                            this.showDetail(lead.id);
                            this.reloadLead.emit(false);
                        });
                }
            }
        );
    }

    cold(lead: CreateOrEditWholeSaleLeadDto): void {
        let status: GetLeadForChangeStatusOutput = new GetLeadForChangeStatusOutput();
        status.id = lead.id;
        status.leadStatusID = 3;
        this.message.confirm('',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._wholeSaleleadsServiceProxy.changeStatus(status)
                        .subscribe(() => {
                            this.notify.success(this.l('LeadStatusChangedToCold'));
                            //this._router.navigate(['/app/main/myleads/myleads']);
                            this.modalSave.emit(null);
                            this.showDetail(lead.id);
                            this.reloadLead.emit(false);
                        });
                }
            }
        );
    }

    hot(lead: CreateOrEditWholeSaleLeadDto): void {
        let status: GetLeadForChangeStatusOutput = new GetLeadForChangeStatusOutput();
        status.id = lead.id;
        status.leadStatusID = 5;
        this.message.confirm('',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._wholeSaleleadsServiceProxy.changeStatus(status)
                        .subscribe(() => {
                            this.notify.success(this.l('LeadStatusChangedToHot'));
                            //this._router.navigate(['/app/main/myleads/myleads']);
                            this.modalSave.emit(null);
                            this.showDetail(lead.id);
                            this.reloadLead.emit(false);
                        });
                }
            }
        );
    }

    cancel(lead: CreateOrEditWholeSaleLeadDto): void {

        abp.event.trigger('app.show.cancelLeadModal', lead.id);
        
    }

    reject(id): void {
        abp.event.trigger('app.show.rejectLeadModal', id);
    }

    registerToEvents() {

        abp.event.on('app.onCancelModalSaved', () => {
            debugger;
            this.showDetail(this.SelectedLeadId);
        });
    }

    addActivitySuccess() {

        this.showDetail(this.SelectedLeadId);
    }

    changeActivity(lead: LeadDto) {
        let that = this;

        this._wholeSaleleadsServiceProxy.getWholeSaleLeadActivityLog(lead.id, this.actionId, this.sectionId, false, false, 0, false, 0).subscribe(result => {
            if (result.length > 0) {
                this.notify.success(this.l('ActivityFiltered'));
            }
            else {
                this.notify.success(this.l('NoActivityFound'));
            }
            let lastdatetime !: moment.Moment;
            this.activityLog = result;
            this.leadActivityList = result;
            let obj = {
                activityDate: "",
                DatewiseActivity: []
            }
            this.activityDatabyDate = [];
            this.activityLog.forEach(function (log) {
                if (that.getParsedDate(log.creationTime) == that.getParsedDate(lastdatetime)) {
                    log.logDate = "";
                    let item1 = this.activityDatabyDate.find(activityDate => activityDate.activityDate === that.getParsedDate(lastdatetime));
                    item1.DatewiseActivity.push({ actionNote: log.actionNote });
                }
                else {
                    log.logDate = that.getParsedDate(log.creationTime);
                    obj.activityDate = log.logDate;
                    obj.DatewiseActivity.push(log.actionNote);
                    this.activityDatabyDate.push({
                        activityDate: log.logDate,
                        DatewiseActivity: [{ actionNote: log.actionNote }]
                    });
                }
                lastdatetime = log.creationTime;
            }.bind(that));

        });
    }

    myActivity(lead: LeadDto) {
        let that = this;

        this._wholeSaleleadsServiceProxy.getWholeSaleLeadActivityLog(lead.id, this.actionId, this.sectionId, false, this.activity, 0, false, 0).subscribe(result => {
            if (result.length > 0) {
                this.notify.success(this.l('ActivityFiltered'));
            }
            else {
                this.notify.success(this.l('NoActivityFound'));
            }
            let lastdatetime !: moment.Moment;
            this.activityLog = result;
            this.leadActivityList = result;
            let obj = {
                activityDate: "",
                DatewiseActivity: []
            }
            this.activityDatabyDate = [];
            this.activityLog.forEach(function (log) {
                if (that.getParsedDate(log.creationTime) == that.getParsedDate(lastdatetime)) {
                    log.logDate = "";
                    let item1 = this.activityDatabyDate.find(activityDate => activityDate.activityDate === that.getParsedDate(lastdatetime));
                    item1.DatewiseActivity.push({ actionNote: log.actionNote });
                }
                else {
                    log.logDate = that.getParsedDate(log.creationTime);
                    obj.activityDate = log.logDate;
                    obj.DatewiseActivity.push(log.actionNote);
                    this.activityDatabyDate.push({
                        activityDate: log.logDate,
                        DatewiseActivity: [{ actionNote: log.actionNote }]
                    });
                }
                lastdatetime = log.creationTime;
            }.bind(that));

        });
    }

    bindMap() {
        console.log(this.OrganizationMapDto);
        
        if(this.selectedMap != "GoogleMap")
        {
            let orgMap = this.OrganizationMapDto.find(x => x.mapProvider == this.selectedMap);

            let Src = "";
            if(orgMap != null && orgMap != undefined)
            {
                if(orgMap.mapProvider == "GoogleMap") {
                    //this.selectedMap
                    // Src = "https://maps.googleapis.com/maps/api/staticmap?center="+ this.lat + "," + this.lng +"&zoom=19&scale=2&size=800x300&maptype=satellite&key="+ orgMap.mapApiKey +"&markers=size:tiny|color:red|label:|" + this.lat + "," + this.lng;
                }
                else if (orgMap.mapProvider == "NearMap") {
                    let date = new Date();
                    let latest_date =this.datepipe.transform(date, 'yyyyMMdd');
    
                    console.log(latest_date);
    
                    Src = "https://au0.nearmap.com/staticmap?center="+ this.lat + "," + this.lng +"&zoom=20&size=600x1270&maptype=satellite&httpauth=false&date="+ latest_date +"&apikey="+ orgMap.mapApiKey;
                
                    //https://au0.nearmap.com/staticmap?center=-27.238610,153.039820&size=800x800&zoom=20&date=20200205&httpauth=false&apikey=MzA4YzMwYjItZTg3Yy00Yzg3LTg3ZWItYWRkZTA4NDI4ZDky
                }
                else if (orgMap.mapProvider == "MetroMap") {
                    Src = "api.metromap.com.au/metromapkey/getlayers?lng="+ this.lng  + "&lat=" + this.lat +"&type=point&zoom=19&scale=2&size=800x300&maptype=satellite&key="+ orgMap.mapApiKey +"&markers=size:tiny|color:red|label:|" + this.lat + "," + this.lng;
                
                    //https://api.metromap.com.au/metromapkey/getlayers?key={api-key}&lng={lng}&lat={lat}&type=point
                }
            }
    
            this.mapSrc = Src;
        }
    }


}
