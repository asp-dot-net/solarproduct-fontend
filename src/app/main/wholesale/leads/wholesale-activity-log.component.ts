import { Component, EventEmitter, Injector, Input, OnInit, Output, ViewChild } from '@angular/core';
import {
    WholeSaleLeadServiceProxy, GetActivityLogViewDto, GetLeadForViewDto, GetLeadForChangeStatusOutput, LeadDto, CommonLookupDto, LeadsServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import PlaceResult = google.maps.places.PlaceResult;
import { JobsComponent } from '@app/main/jobs/jobs/jobs.component';
import { PromotionStopResponceComponent } from '@app/main/promotions/promotionUsers/stop-responce.component';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { WholeSaleActivityLogHistoryComponent } from './activity-log-history.component';
import { WholeSaleActivityLogDetailComponent } from './wholesale-activity-log-detail.component';

import { finalize } from 'rxjs/operators';
import * as moment from 'moment';
import { AgmCoreModule } from '@agm/core';


@Component({
    selector: 'wholeSaleactivityLog',
    templateUrl: './wholesale-activity-log.component.html',
    animations: [appModuleAnimation()]
})
export class WholeSaleActivityLogComponent extends AppComponentBase implements OnInit {

    @ViewChild('wholesaleactivityloghistory', { static: true }) wholesaleactivityloghistory: WholeSaleActivityLogHistoryComponent;
    @ViewChild('wholeSaleactivityLogDetail', { static: true }) wholeSaleactivityLogDetail: WholeSaleActivityLogDetailComponent;

    @Input() SelectedLeadId: number = 0;
    @Output() reloadLead = new EventEmitter();
    active = false;
    saving = false;
    item: GetLeadForViewDto;
    activeTabIndex: number = 0;
    activityDatabyDate: any[] = [];
    actionId: number = 0;
    activity: boolean = false;
    leadActivityList: GetActivityLogViewDto[];
    leadActionList: CommonLookupDto[];
    leadId: number = 0;
    id: number = 0;
    role: string = '';
    sectionId: number = 0;
    showsectionwise = false;
    currentactivity: boolean = false;
    allActivity: boolean = false;
    serviceId : number =0 ;
    wholesaleId = 0;

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _wholeSaleleadsServiceProxy: WholeSaleLeadServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _router: Router,
        private spinner: NgxSpinnerService
    ) {
        super(injector);
        this.item = new GetLeadForViewDto();
        this.item.lead = new LeadDto();
    }


    ngOnInit(): void {
        //this.show(this._activatedRoute.snapshot.queryParams['id']);
        // this._wholeSaleleadsServiceProxy.getAllLeadAction().subscribe(result => {
        //     this.leadActionList = result;
        // });

        this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
            this.role = result;
            if (this.role == 'Sales Rep') {
                this.activity = true;
            }
        });
    }

    show(leadId?: number, param?: number, sectionId?: number, service? : number): void {
        debugger;
        this.sectionId = sectionId;
        this.activity = false;
        this.allActivity = false;
        this.serviceId = service;
        this.wholesaleId = leadId;
        if (this.sectionId == 0 || this.sectionId > 12) {
            this.showsectionwise = true;
        } 
        //console.log(this.sectionId);
        if (this.sectionId == 30) {
            this.showsectionwise = false;
        } 
        if (param != null) {
            this.id = param;
        }
        this.leadId = leadId;
        if(this.sectionId == 23 || this.sectionId == 24 || this.sectionId == 25)
        {
            this.currentactivity =  true;
        }
        if (this.sectionId == 15) {
            this.actionId = 11;
            this._wholeSaleleadsServiceProxy.getWholeSaleLeadActivityLog(leadId, this.actionId, this.sectionId, this.currentactivity, this.activity, 0, this.allActivity, this.serviceId).subscribe(result => {
              debugger;
                this.leadActivityList = result;
            });
        }
        else if (this.sectionId == 28) {
            this.actionId = 6;
            this._wholeSaleleadsServiceProxy.getWholeSaleLeadActivityLog(leadId, this.actionId, this.sectionId, this.currentactivity, this.activity, 0, this.allActivity, this.serviceId).subscribe(result => {
              debugger;
                this.leadActivityList = result;
            });
        }
         else {
            //this.actionId = 1;
            this._wholeSaleleadsServiceProxy.getWholeSaleLeadActivityLog(leadId, this.actionId, this.sectionId, this.currentactivity, this.activity, 0, this.allActivity, this.serviceId).subscribe(result => {
                debugger;
                this.leadActivityList = result;
            });
        }
    }

    showDetail(leadId: number): void {
        let that = this;
        debugger
        this._wholeSaleleadsServiceProxy.getWholeSaleLeadActivityLog(leadId, 0, this.sectionId,this.currentactivity, false, 0, this.allActivity, this.serviceId).subscribe(result => {
            this.leadActivityList = result;
        });
    }

    registerToEvents() {
        abp.event.on('app.onCancelModalSaved', () => {
            this.showDetail(this.leadId);
        });
    }

    addActivitySuccess() {
        this.showDetail(this.leadId);
    }

    changeActivity(leadId: number) {
        let that = this;
        this.spinner.show();
        debugger
        this._wholeSaleleadsServiceProxy.getWholeSaleLeadActivityLog(leadId, this.actionId, this.sectionId,this.currentactivity, false, 0, this.allActivity, this.serviceId).subscribe(result => {
            if (result.length > 0) {
                this.notify.success(this.l('ActivityFiltered'));
               
            }
            else {
                this.notify.success(this.l('NoActivityFound'));
            }
            this.leadActivityList = result;
            this.spinner.hide();
        });
    }
    navigateToLeadHistory(leadid): void {

        // this.wholesaleactivityloghistory.show(leadid);
    }

    myActivity(leadId: number) {
        let that = this;
        this.spinner.show();
        this._wholeSaleleadsServiceProxy.getWholeSaleLeadActivityLog(leadId, this.actionId, this.sectionId,this.currentactivity, this.activity, 0, this.allActivity, this.serviceId).subscribe(result => {
            if (result.length > 0) {
                this.notify.success(this.l('ActivityFiltered'));
            }
            else {
                this.notify.success(this.l('NoActivityFound'));
            }
            this.leadActivityList = result;
            this.spinner.hide();
        });
    }
}
