﻿import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { WholeSaleLeadServiceProxy, LeadDto, LeadStatusLookupTableDto, OrganizationUnitDto, CreateOrEditWholeSaleLeadDto, LeadStateLookupTableDto, JobStatusTableDto, LeadSourceLookupTableDto, CommonLookupServiceProxy, AssignOrDeleteWholeSaleLeadInput, WholeSaleLeadsSummaryCount, LeadsServiceProxy } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { AppConsts } from '@shared/AppConsts';
import { ViewWholesaleLeadComponent } from './view-wholesalelead.component';
import { OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { finalize } from 'rxjs/operators';
import { FileUpload } from 'primeng/fileupload';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { LeadDetailModal } from './lead-detail-modal.component';
import { CreateEditWholesaleLeadComponent } from './create-edit-wholesalelead.component';

import { LeadNewContactModal } from './add-contact-modal.component';
import { WholeSaleSMSEmailModalComponent } from '../activity/sms-email-modal.component';
import { WholeSaleNotifyCommentModalComponent } from '../activity/notify-comment-modal.component';
import { WholeSaleReminderModalComponent } from '../activity/reminder-modal.component';
import { WholeSaletodoModalComponent } from '../activity/todo-modal.component';
import { WholeSaleDocumentUploadModalComponent } from '../activity/doc-model.component';
import { EditWholesaleLeadModalComponent } from './edit-wholesalelead.component';
import { ViewCreditApplicationModalComponent } from '../credit/credit-applications/view-credit-application-modal.component';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';

@Component({
    templateUrl: './wholesaleleads.component.html',
    styleUrls: ['./wholesaleleads.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
})
export class WholesaleLeadsComponent extends AppComponentBase implements OnInit {

    show: boolean = true;
    FiltersData = false;
    showchild: boolean = true;
    shouldShow: boolean = false;
    flag = true;
    organizationUnitlength: number = 0;
    count = 0
    tableRecords: any;

    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };
    excelorcsvfile = 0;

    toggle: boolean = true;

    change() {
        this.toggle = !this.toggle;
      }

    @ViewChild('ExcelFileUpload', { static: false }) excelFileUpload: FileUpload;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('ViewWholesaleLeadDetail', { static: true }) ViewWholesaleLeadDetail: ViewWholesaleLeadComponent;
    @ViewChild('LeadDetailModal', { static: true }) LeadDetailModal: LeadDetailModal;
    @ViewChild('wholeSaleSMSEmailModal', { static: true }) wholeSaleSMSEmailModal: WholeSaleSMSEmailModalComponent;
    @ViewChild('wholeSaletodoModal', { static: true }) wholeSaletodoModal: WholeSaletodoModalComponent;
    @ViewChild('wholeSaleDocumentUploadModal', { static: true }) wholeSaleDocumentUploadModal: WholeSaleDocumentUploadModalComponent;
    @ViewChild('createEditWholesaleLead', { static: true }) createEditWholesaleLead: CreateEditWholesaleLeadComponent;
    @ViewChild('wholeSaleReminderModal', { static: true }) wholeSaleReminderModal: WholeSaleReminderModalComponent;
    @ViewChild('wholeSaleNotifyCommentModal', { static: true }) wholeSaleNotifyCommentModal: WholeSaleNotifyCommentModalComponent;
    @ViewChild('LeadNewContactModal', { static: true }) LeadNewContactModal: LeadNewContactModal;
    @ViewChild('EditWholesaleLeadModal', { static: true }) EditWholesaleLeadModal: EditWholesaleLeadModalComponent;
    @ViewChild('ViewCreditApplicationModal', { static: true }) ViewCreditApplicationModal: ViewCreditApplicationModalComponent; 
        
    
    uploadUrl: string;
    advancedFiltersAreShown = false;
    filterText = '';
    copanyNameFilter = '';
    emailFilter = '';
    phoneFilter = '';
    mobileFilter = '';
    addressFilter = '';
    requirementsFilter = '';
    postCodeSuburbFilter = '';
    stateId : number = 0;
    stateNameFilter = '';
    streetNameFilter = '';
    postCodePostalCode2Filter = '';
    leadSourceNameFilter = '';
    //leadSubSourceNameFilter = '';
    leadStatusName = '';
    typeNameFilter = '';
    areaNameFilter = '';
    leadStatus: any;
    allLeadStatus: LeadStatusLookupTableDto[];
    alljobstatus: JobStatusTableDto[];
    leadStatusId: number = 0;
    jobStatusIDFilter = 0;
    allLeadSources: LeadSourceLookupTableDto[];
    // StartDate: moment.Moment;
    // EndDate: moment.Moment;
    StartDate = moment().startOf('month').add(1, 'days');
    EndDate = moment().add(0, 'days').endOf('day');
    PageNo: number;
    ExpandedView: boolean = true;
    // flafValue:boolean = true;
    SelectedLeadId: number = 0;
    //sampleDateRange: moment.Moment[] = [moment().add(-4, 'days').endOf('day'), moment().add(0, 'days').endOf('day')];
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnit = 0;
    leadSourceIdFilter = [];
    allStates: LeadStateLookupTableDto[];
    suburbSuggestions: string[];
    dateFilterType = 'Creation';
    changeView: boolean = true;
    role: string[] = [];
    projectNumberFilter = '';
    leadName: string;
    totalLeads = 0;
    new = 0;
    unHandledLeads = 0;
    upgrade = 0;
    canceled = 0;
    cold = 0;
    warm = 0;
    hot = 0;
    closed = 0;
    assigned = 0;
    reAssigned = 0;
    existing = 0;
    dnc = 0;
    sold = 0;
    rejected = 0;
    jobStatusID = [];
    upgrateshow = false;
    frozenValue: [];
    frozenCols: any[];
    allBDMs : any[];
    allSalesRap : any[];
    allWholesaleStatus : any[];
    filterDBMId = 0;
    filterSalesRapId = 0;
    statusId = 0;
    filterStatusId = 0;
    changeOrganization = 0;
    action = 0;
    assignUserId = 0;
    assignManagerId = 0;
    PlaceHolder = '';
    assignUser: any = [];
    assignManager: any = [];
    saving = false;
    filterName ="MobileNo";
    uploadUrlstc: string;
   mylead = false;
   
   isCreateValid = false;
   LastActivityDate: moment.Moment;

    leadstatuss = [
        {value: 2, name: 'UnHandled'},
        {value: 3, name: 'Cold'},
        {value: 4, name: 'Warm'},
        {value: 5, name: 'Hot'},
        {value: 6, name: 'Upgrade'},
        {value: 10, name: 'Assigned'},
        {value: 11, name: 'ReAssigned'},
    ];
    createby = 0;
    createByList = [];

    leadStatusIDS = [];
    firstrowcount = 0;
    last = 0;

    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 330;
    istransfer = 0;
    leadSummary : WholeSaleLeadsSummaryCount[]= [];
    
    constructor(
        injector: Injector,
        private _notifyService: NotifyService,
        private _commonLookupService: CommonLookupServiceProxy,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _httpClient: HttpClient,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _tokenService: TokenService,
        private titleService: Title,
        private _wholeSaleLeadServiceProxy : WholeSaleLeadServiceProxy,
        ) {
            super(injector);
            this.titleService.setTitle(this.appSession.tenancyName + " |  Wholesale Leads");
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/Users/ImportLeadFromExcel';
        this.uploadUrlstc = AppConsts.remoteServiceBaseUrl + '/Users/ImportWholeSaleLeadFromExcel';
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Wholesale Leads';
        log.section = 'Wholesale Leads';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
        this.StartDate = null;
        this.EndDate = null;
        this._commonLookupService.getCurrentUserRoles().subscribe(result => {
            this.role = result;
            
            if(result.includes('Wholesale SalesRep')){
                this._wholeSaleLeadServiceProxy.getLeadCountByUserId().subscribe(count => {
                    this._wholeSaleLeadServiceProxy.getLeadCountByUserIdFromUser().subscribe(Limit => {
                        if(count > Limit){
                            this.isCreateValid = false;
                        }
                        else{
                            this.isCreateValid = true;
                        }
                    })
                })
            }
            else{
                this.isCreateValid = true;
            }
        });
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });

        this.frozenCols = [];

        this._commonLookupService.getAllJobStatusTableDropdown().subscribe(result => {
            this.alljobstatus = result;
        });

        this._commonLookupService.getWholesaleStatusDropdown().subscribe(result => {
            this.allWholesaleStatus = result;
        })
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            
            this._commonLookupService.getAllUsersByRoleNameTableDropdown('Wholesale Manager',this.allOrganizationUnits[0].id).subscribe(result => {
                this.allBDMs = result;

                this._commonLookupService.getAllUsersByRoleNameTableDropdown('Wholesale SalesRep',this.allOrganizationUnits[0].id).subscribe(sales => {
                    this.allSalesRap = sales;
                })
            })
            
            // this.bindLeadSource(this.organizationUnit)
            this.getLeads();
        });

        this._wholeSaleLeadServiceProxy.getCreateUsers().subscribe(output => {
            this.createByList = output;
        })
    }
    searchLog() : void {
        
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Wholesale Leads';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
  
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }

        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }

    bindLeadSource(id) {
        this._commonLookupService.getAllLeadSourceForTableDropdownOnlyOrg(id).subscribe(result => {
            this.allLeadSources = result;
        });
    }

    expandGrid() {
        this.ExpandedView = true;
    }
    clear() {
        this.filterText = '';
        this.addressFilter = '';
        this.postCodeSuburbFilter = '';
        this.stateNameFilter = '';
        this.stateId = 0;
        this.postCodePostalCode2Filter = '';
        this.leadSourceNameFilter = '';
        this.typeNameFilter = '';
        this.jobStatusIDFilter = 0;
        this.areaNameFilter = '';
        this.dateFilterType = 'Creation';
        //this.sampleDateRange = [moment().add(-7, 'days').endOf('day'), moment().add(0, 'days').startOf('day')];
        this.StartDate = moment().add(0, 'days').endOf('day');
        this.EndDate = moment().add(0, 'days').endOf('day');
        this.jobStatusID = [];
        this.LastActivityDate = null;
        this.getLeads();
    }

    filtersuburbs(event): void {
        this._commonLookupService.getOnlySuburb(event.query).subscribe(output => {
            this.suburbSuggestions = output;
        });
    }

    leadstatuschange()
    {
        debugger;
        if (this.leadStatusIDS.includes(6)) {
            this.upgrateshow = true;
        } else {
            this.upgrateshow = false;
            this.jobStatusID = [];
        } 
    }

    uploadExcelstc(data: { files: File }): void {
        
        var headers_object = new HttpHeaders().set("Authorization", "Bearer " + this._tokenService.getToken());

        const httpOptions = {
            headers: headers_object
        };
        const formData: FormData = new FormData();
        const file = data.files[0];
        formData.append('file' + ',' + this.organizationUnit, file, file.name);
        this._httpClient
            .post<any>(this.uploadUrlstc, formData, httpOptions)
            .pipe(finalize(() => this.excelFileUpload.clear()))
            .subscribe(response => {
                if (response.success) {
                    this.notify.success(this.l('ImportInstallerInvoicePaymentProcessStart'));
                } else if (response.error != null) {
                    this.notify.error(this.l('ImportInstallerInvoicePaymentUploadFailed'));
                }
            });
    }

    onUploadExcelErrorstc(): void {
        this.notify.error(this.l('ImportInstallerInvoicePaymentUploadFailed'));
    }
    getLeads(event?: LazyLoadEvent) {
        debugger;
        this.leadSummary = [];
        this.PageNo = this.primengTableHelper.getSkipCount(this.paginator, event);

        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();
        
        this._wholeSaleLeadServiceProxy.getAll(
            this.filterName,
            this.filterText,
            this.stateId,
            this.postCodePostalCode2Filter,
            this.addressFilter,
            this.dateFilterType,
            this.StartDate,
            this.EndDate,
            this.organizationUnit,
            this.filterDBMId,
            this.filterSalesRapId,
            this.statusId,
            this.mylead,
            this.createby,
            this.LastActivityDate,
            this.istransfer,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            this.shouldShow = false;
            this.tableRecords = result.items;

            if (result.totalCount > 0) {
                //this.getLeadSummaryCount();
                this.leadSummary = result.items[0].summaryCount;
                

            } else {
                // this.totalLeads = 0;
                // this.existing = 0;
                // this.dnc = 0;
                // this.cold = 0;
                // this.warm = 0;
                // this.hot = 0;
                
            }
           
        },
        err => {
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    
    filterCountries(event): void {
        this._commonLookupService.getAllLeadStatusForTableDropdown(event.query).subscribe(result => {
            this.allLeadStatus = result;
        });
    }

    reloadPage(flafValue: boolean): void {
        this.ExpandedView = true;
        this.flag = flafValue;
        this.paginator.changePage(this.paginator.getPage());
        if (!flafValue) {
            this.navigateToLeadDetail(this.SelectedLeadId);
        }
    }

    createLead(lId : number): void {
        // this._router.navigate(['/app/main/wholesale/leads/createEditWholesaleLead'], { queryParams: { id: lId, oid : this.organizationUnit } });
        this.EditWholesaleLeadModal.show(lId,this.organizationUnit);
    }

    navigateToLeadDetail(leadid): void {
        this.ExpandedView = !this.ExpandedView;
        this.ExpandedView = false;
        this.SelectedLeadId = leadid;
        this.leadName = "wholesaleLead";
        this.ViewWholesaleLeadDetail.showDetail(leadid, this.leadName, 13);
    }

    deleteLead(CreateOrEditWholeSaleLeadDto : CreateOrEditWholeSaleLeadDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._wholeSaleLeadServiceProxy.delete(CreateOrEditWholeSaleLeadDto.id).subscribe(result => {
                        this.getLeads();
                        let log = new UserActivityLogDto();
                        log.actionId = 83;
                        log.actionNote ='Delete Wholesale Leads: ' + CreateOrEditWholeSaleLeadDto.firstName;
                        log.section = 'Wholesale Leads';
                        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                            .subscribe(() => {
                        });
                    });
                }
            }
        );
    }

    exportToExcel(excelorcsv): void {
       
        this.excelorcsvfile = excelorcsv;
        this._wholeSaleLeadServiceProxy.getLaedsForExport(
            this.filterName,
            this.filterText,
            this.stateId,
            this.postCodePostalCode2Filter,
            this.addressFilter,
            this.dateFilterType,
            this.StartDate,
            this.EndDate,
            this.organizationUnit,
            this.filterDBMId,
            this.filterSalesRapId,
            this.statusId,
            this.mylead,
            this.createby
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }
    uploadExcel(data: { files: File }): void {
        const formData: FormData = new FormData();
        const file = data.files[0];
        formData.append('file', file, file.name);

        this._httpClient
            .post<any>(this.uploadUrl, formData)
            .pipe(finalize(() => this.excelFileUpload.clear()))
            .subscribe(response => {
                if (response.success) {
                    this.notify.success(this.l('ImportLeadsProcessStart'));
                } else if (response.error != null) {
                    this.notify.error(this.l('ImportLeadUploadFailed'));
                }
            });
    }

    onUploadExcelError(): void {
        this.notify.error(this.l('ImportLeadUploadFailed'));
    }

    showLeadDetails(): void {
        this.LeadDetailModal.show();
     }

    addNewContact(): void {
        this.LeadNewContactModal.show();
    }
    
     showLeadDocUpload(wholeSaleId : number): void {
        this.wholeSaleDocumentUploadModal.show(wholeSaleId,35);
     }

     showLeadSMS(wholeSaleId : number, type :number): void {
        this.wholeSaleSMSEmailModal.show(wholeSaleId,35,type, "Wholesale Leads");
     }

     showLeadToDo(wholeSaleId : number): void {
        this.wholeSaletodoModal.show(wholeSaleId,35,"Wholesale Leads");
     }
     showLeadReminder(wholeSaleId : number): void {
        this.wholeSaleReminderModal.show(wholeSaleId , 35, "Wholesale Leads");
        //this.wholeSaleReminderModal.clea
     }
     showLeadNotifyComment(wholeSaleId : number, type : number): void {
        this.wholeSaleNotifyCommentModal.show(wholeSaleId,35,type,"Wholesale Leads");
     }  

     onchange() : void{
        this._commonLookupService.getAllUsersByRoleNameTableDropdown('Wholesale Manager',this.organizationUnit).subscribe(result => {
            this.allBDMs = result;
        })
        this._commonLookupService.getAllUsersByRoleNameTableDropdown('Wholesale SalesRep',this.organizationUnit).subscribe(result => {
            this.allSalesRap = result;
        })

        this.getLeads();
     }

     changeAction() : void {
        //debugger;
        if(this.action == 2 || this.action == 5)
        {
            this.PlaceHolder = 'Sales Rep';

            this.spinnerService.show();
            this.assignUser = [];
            this.assignUserId = 0;
            this._commonLookupService.getAllUsersByRoleNameTableDropdown('Wholesale SalesRep', this.changeOrganization).subscribe(result => {
                this.assignUser = result;
                this.spinnerService.hide();
            }, e => { this.spinnerService.hide(); });
        }
        else {
            this.PlaceHolder = 'Sales Manager';

            this.assignUserId = 0;
            
            this.spinnerService.show();
            this._commonLookupService.getAllUsersByRoleNameTableDropdown('Wholesale Manager', this.changeOrganization).subscribe(result => {
                this.assignUser = result;
                this.spinnerService.hide();
            }, e => { this.spinnerService.hide(); });
        }
        
    }


    submit() : void {
        let selectedids = [];
        this.saving = true;
        debugger
        // let transferPermission = this.permission.isGranted("Pages.Leads.LeadTracker.Action.TransferLeftSalesRepLead");

        let action = this.action;

        this.primengTableHelper.records.forEach(function (lead) {
            
                if (lead.createOrEditWholeSaleLeadDto.isSelected ) {
                    selectedids.push(lead.createOrEditWholeSaleLeadDto.id);
                }
        });
            
        let leads: AssignOrDeleteWholeSaleLeadInput = new AssignOrDeleteWholeSaleLeadInput();
        leads.assignToUserID = this.assignUserId;
        leads.wholeSaleLeadIds = selectedids;
        leads.organizationID = this.changeOrganization;
        leads.leadActionId = this.action;
        leads.sectionId = 35;
        
        if (this.changeOrganization == 0 && this.action != 3) {
            this.notify.warn(this.l('PleaseSelectOrganization'));
            this.saving = false;
            return;
        }

        if(this.assignUserId == 0 && this.action != 3)
        {
            this.notify.warn("Please Select " + this.PlaceHolder);
            this.saving = false;
            return;
        }

        if(selectedids.length == 0)
        {
            this.notify.warn(this.l('NoLeadsSelected'));
            this.saving = false;
            return;
        }

        this._wholeSaleLeadServiceProxy.asssignOrTransferLeads(leads)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.getLeads();
                this.saving = false;
                this.notify.info(this.l('AssignedSuccessfully'));
            }, e => { this.saving = false; });
    }

    oncheckboxCheck() {

        
        this.count = 0;
        this.tableRecords.forEach(item => {

            if (item.createOrEditWholeSaleLeadDto.isSelected == true) {
                this.count = this.count + 1;
            }
        })
    }

    isAllChecked() {
        if (this.tableRecords)
            return this.tableRecords.every(_ => _.createOrEditWholeSaleLeadDto.isSelected);
    }

    checkAll(ev) {

        this.tableRecords.forEach(x => x.createOrEditWholeSaleLeadDto.isSelected = ev.target.checked);
        this.oncheckboxCheck();
    }

    AddtoUserRequest(id : number): void {
        this.primengTableHelper.showLoadingIndicator();

        this._wholeSaleLeadServiceProxy.addtoUserRequest(id).subscribe(result => {
            if(result == "Success")
            {
                this.primengTableHelper.hideLoadingIndicator(); 
                this.notify.info(this.l('SavedSuccessfully'));
            }
            else 
            {
                this.primengTableHelper.hideLoadingIndicator(); 

                this.notify.info("Email Address Already Exists");
            }
        }, err => { this.primengTableHelper.hideLoadingIndicator(); });
    }
    requestToMoveLead(leadid): void{
        this._wholeSaleLeadServiceProxy.requestToMove(leadid,35)
        .pipe(finalize(() => { this.saving = false; }))
        .subscribe(() => {
            // this.viewLeadDetail.reloadLead.emit(false);
            
            this.saving = false; 
            this.notify.info("Successfully Requested");
        });
    }
     
}
