﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit, ElementRef, Directive, Optional } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {
    LeadsServiceProxy, LeadtrackerHistoryDto, WholeSaleLeadServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AgmCoreModule } from '@agm/core';
import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';
import PlaceResult = google.maps.places.PlaceResult;
import { Appearance, GermanAddress, Location } from '@angular-material-extensions/google-maps-autocomplete';

@Component({
    selector: 'wholesaleactivityloghistory',
    templateUrl: './activity-log-history.component.html',
})
export class WholeSaleActivityLogHistoryComponent extends AppComponentBase {
    @ViewChild("myNameElem") myNameElem: ElementRef;
    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @ViewChild('SampleDatePicker', { static: true }) sampleDatePicker: ElementRef;
    hidePreviousValue: boolean = false;

    leadtrackerHistory: LeadtrackerHistoryDto[];
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _wholeSaleLeadsServiceProxy: WholeSaleLeadServiceProxy,
        private _router: Router,
        private _el: ElementRef

    ) {
        super(injector);
        this._el.nativeElement.setAttribute('autocomplete', 'off');
        this._el.nativeElement.setAttribute('autocorrect', 'off');
        this._el.nativeElement.setAttribute('autocapitalize', 'none');
        this._el.nativeElement.setAttribute('spellcheck', 'false');
    }

    show(leadId?: number,leadorjob?: number): void {
       debugger;
                if(leadorjob ==1 ){
                    this._wholeSaleLeadsServiceProxy.getWholeSaleLeadActivityLogHistory(leadId, 2, 0, false, false, 0, false,0).subscribe(result => {
                    this.leadtrackerHistory = result;
                    this.hidePreviousValue = false;
                       this.modal.show();
                       });
                }
                if (leadorjob == 2) {
           
                    this._wholeSaleLeadsServiceProxy.getWholeSaleLeadActivityLogHistory(leadId, 2, 0, false, false, 0, false,0).subscribe(result => {
                        this.leadtrackerHistory = result;
                        this.hidePreviousValue = true;
                        this.modal.show();
                    });
                }
               
    }
    close(): void {

        this.modal.hide();
    }
}
