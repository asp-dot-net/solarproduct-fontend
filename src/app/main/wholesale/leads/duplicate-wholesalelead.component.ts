import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { CreateOrEditWholeSaleLeadDto, GetDuplicateLeadPopupDto, GetWholeSaleLeadForViewDto, WholeSaleLeadServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
    selector: 'ViewDuplicateWholesaleLeadPopUpModal',
    templateUrl: './duplicate-wholesalelead.component.html'
})
export class ViewDuplicateWholesaleLeadPopUpModalComponent extends AppComponentBase {

    @ViewChild('duplicateLeadPOPModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    saving = false;
    result: GetWholeSaleLeadForViewDto[];
    wholesalelead : CreateOrEditWholeSaleLeadDto;
    savedata =true;
    isadmin = false;
    constructor(
        private _wholeSaleLeadServiceProxy:WholeSaleLeadServiceProxy,
        private _router: Router,
        injector: Injector
    ) {
        super(injector);
    }

    show(item: GetWholeSaleLeadForViewDto[], lead : CreateOrEditWholeSaleLeadDto,isadmin = false): void {       
        this.isadmin = isadmin;
        this.savedata =true;
        this.result = item;
        this.wholesalelead = lead;
        this.modal.show();
    }

    close(): void {
        this.savedata =false;
        this.modal.hide();
    }

    save(): void{
        this.savedata =true;
        this.modal.hide();
        this.modalSave.emit(this.savedata);
        // this._wholeSaleLeadServiceProxy.createOrEdit(this.wholesalelead)
        // .pipe(finalize(() => { this.saving = false; }))
        // .subscribe(() => {
        //      this.modal.hide();
        //      this.saving = false; 
        //      this.notify.info(this.l('SavedSuccessfully'));
        //      this._router.navigate(['/app/main/wholesale/leads']); 
        //     });
    }
}
