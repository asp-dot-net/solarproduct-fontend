﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit, ElementRef, Directive, Optional } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {
    LeadsServiceProxy
    , LeadStateLookupTableDto,
    CommonLookupServiceProxy,
    CreateOrEditWholeSaleLeadDto, WholeSaleLeadServiceProxy, WholeSaleStatuseServiceProxy, CommonLookupDto, NameValueOfString, WholeSaleLeadContactDto
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { FileUpload } from 'primeng/fileupload';
import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AgmCoreModule } from '@agm/core';
import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';
import PlaceResult = google.maps.places.PlaceResult;
import { Appearance, GermanAddress, Location } from '@angular-material-extensions/google-maps-autocomplete';
//import { ViewDuplicatePopUpModalComponent } from './duplicate-lead-popup.component';
import { ViewMyLeadComponent } from '@app/main/myleads/myleads/view-mylead.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { result } from 'lodash';
import { ViewDuplicateWholesaleLeadPopUpModalComponent } from './duplicate-wholesalelead.component';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'createEditWholesaleLead',
    templateUrl: './create-edit-wholesalelead.component.html',
    styleUrls: ['./create-edit-wholesalelead.component.less']
})
export class CreateEditWholesaleLeadComponent extends AppComponentBase implements OnInit {
    @ViewChild("myNameElem") myNameElem: ElementRef;
    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @ViewChild('SampleDatePicker', { static: true }) sampleDatePicker: ElementRef;
    //@ViewChild('duplicatepopupModal', { static: true }) duplicatepopupModal: ViewDuplicatePopUpModalComponent;
    // @ViewChild('CreateEditWholesaleLeadComponent', { static: true }) CreateEditWholesaleLeadComponent: CreateEditWholesaleLeadComponent;
    @ViewChild('ViewDuplicateWholesaleLeadPopUpModal', { static: true }) ViewDuplicateWholesaleLeadPopUpModal: ViewDuplicateWholesaleLeadPopUpModalComponent;

    active = false;
    saving = false;
    
    postalStateName = '';
    delivaryStateName = '';
    
    allStates : LeadStateLookupTableDto[];
    allBDMs : any[];
    allSalesRap : any[];
   
    selectAddress: boolean = true;
    selectAddressDelivery: boolean = true;
    selectAddress2: boolean = true;
    
    DelAddress = true;
    wholeSaleLead : CreateOrEditWholeSaleLeadDto = new CreateOrEditWholeSaleLeadDto();
    allWholesaleStatus : CommonLookupDto[];
    role : string[];

    isBDM = false;
    isSalesRap = false;
    customerTypes =[];
    filteredcustomerType: NameValueOfString[];
    PriceCategory : any[];
    TradingSTCs : any[];

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _router: Router,
        private _el: ElementRef,
        private _wholeSaleLeadServiceProxy:WholeSaleLeadServiceProxy,
        private _wholeSaleStatuseServiceProxy : WholeSaleStatuseServiceProxy,
        @Optional() private viewLeadDetail?: ViewMyLeadComponent,
        
    ) {
        super(injector);
        this._el.nativeElement.setAttribute('autocomplete', 'off');
        this._el.nativeElement.setAttribute('autocorrect', 'off');
        this._el.nativeElement.setAttribute('autocapitalize', 'none');
        this._el.nativeElement.setAttribute('spellcheck', 'false');
    }
 
    ngOnInit(): void {
       
        this.show(this._activatedRoute.snapshot.queryParams['id'], this._activatedRoute.snapshot.queryParams['oid']);

        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        })
        this._commonLookupService.getWholesaleStatusDropdown().subscribe(result => {
            this.allWholesaleStatus = result;
        })
         
    }
    onShown(){}

    show(id? : number, oid? :number) : void{
        debugger;
        if(id > 0){
            this._wholeSaleLeadServiceProxy.getWholeSaleLeadForEdit(id).subscribe(result => {
                this.wholeSaleLead = result;
                if(result.wholeSaleLeadContacts.length <= 0){
                    //this.eCommerceProductItem.ecommerceProductItemPriceCategories = [];
                    let contact = new WholeSaleLeadContactDto();
                    this.wholeSaleLead.wholeSaleLeadContacts.push(contact);
                }
            })
        }
        else{
            this.wholeSaleLead = new CreateOrEditWholeSaleLeadDto();
            this.wholeSaleLead.organizationId = oid;
            this.wholeSaleLead.wholeSaleLeadContacts = [];
            let contact = new WholeSaleLeadContactDto();
            this.wholeSaleLead.wholeSaleLeadContacts.push(contact);
        }
        this._commonLookupService.getAllUsersByRoleNameTableDropdown('Wholesale Manager',oid).subscribe(result => {
            this.allBDMs = result;
        })
        this._commonLookupService.getAllUsersByRoleNameTableDropdown('Wholesale SalesRep',oid).subscribe(result => {
            this.allSalesRap = result;
        })
        this._commonLookupService.getCurrentUserRoles().subscribe(result => {
            this.role = result;
            this.isBDM = result.includes('Wholesale Manager') ? true : false;
            this.isSalesRap = result.includes('Wholesale SalesRep') ? true : false;
            if(this.isBDM == true){
                this.wholeSaleLead.bdmId = this.appSession.userId;
            }
            if(this.isSalesRap == true){
                this.wholeSaleLead.salesRapId = this.appSession.userId;
            }
            
            // this._leadsServiceProxy.getCurrentUserId().subscribe(r => {
            //     if(this.isBDM == true){
            //         this.wholeSaleLead.bdmId = r;
            //     }
            //     if(this.isSalesRap == true){
            //         this.wholeSaleLead.salesRapId =r;
            //     }
            // });
        });
        this._commonLookupService.getEcommercePriceCategoryDropdown().subscribe(result => {
            this.PriceCategory = result;
        });
        this._commonLookupService.getTradingSTCWhomDropdown().subscribe(result => {
            this.TradingSTCs = result;
        });

        if(this.wholeSaleLead.customerType){
            var ctype = this.wholeSaleLead.customerType.split(',');
            this.customerTypes = [];
            ctype.forEach(element => {
                debugger;
                this.customerTypes.push(element);
            });
        }
        
    }
    filtercustomerTypes(event): void {
        this._commonLookupService.getCustomerTypes(event.query).subscribe(result => {
            this.filteredcustomerType = result;
        });
    }
    // // get StreetName
    // filterstreetnames(event): void {
    //     this._leadsServiceProxy.getAllStreetName(event.query).subscribe(output => {
    //         this.streetNamesSuggestions = output;
    //     });
    // }

    // // get Suburb
    // filtersuburbs(event): void {
    //     this._leadsServiceProxy.getAllSuburb(event.query).subscribe(output => {
    //         this.suburbSuggestions = output;
    //     });
    // }
    // //get state & Postcode
    // fillstatepostcode(): void {
    //     var splitted = this.suburb.split("||");
    //     // this.wholeSaleLead.suburb = splitted[0];
    //     // this.wholeSaleLead.state = splitted[1].trim();
    //     // this.wholeSaleLead.postCode = splitted[2].trim();
    //     // this.wholeSaleLead.country = "AUSTRALIA";

    //     this.bindArea();
    // }
    removeContactItem(contactItem): void {
        if (this.wholeSaleLead.wholeSaleLeadContacts.length == 1)
            return;
       
        if (this.wholeSaleLead.wholeSaleLeadContacts.indexOf(contactItem) === -1) {
            
        } else {
            this.wholeSaleLead.wholeSaleLeadContacts.splice(this.wholeSaleLead.wholeSaleLeadContacts.indexOf(contactItem), 1);
        }
    }

    addContactItem(): void {
        let contact = new WholeSaleLeadContactDto();
        this.wholeSaleLead.wholeSaleLeadContacts.push(contact);
    }

    close(): void {
        this.Reset();
        this.modal.hide();
        this.saving = false;
    }

    cancel(): void {
        this.Reset();
        this._router.navigate(['/app/main/wholesale/leads']);     
        
    }

    bindArea(): void {
        
        // if(this.wholeSaleLead.postCode != null && this.wholeSaleLead.postCode != "" && this.wholeSaleLead.postCode != undefined)
        // {
        //     this._postCodeRangeServiceProxy.getAreaByPostCodeRange(this.wholeSaleLead.postCode).subscribe((result) => 
        //     {
        //         this.wholeSaleLead.area = result;
        //     });
        // }
    }

     OnSelectDetails(event): void {
         // alert(event.leadId);
        //  this.wholeSaleLead.referralLeadId = event.leadId;
        //  this.wholeSaleLead.referralName = event.customerName;
     }
     // End Referral Details Code
 
     referal: boolean = false;
     LeadSourceOnChange() {
        //  if(this.wholeSaleLead.leadSource == 'Referral'){
 
        //      this.referal = true;
        //  }
        //  else{
        //      this.referal = false;
        //  }
     }

     Reset(): void {
        this.wholeSaleLead = new CreateOrEditWholeSaleLeadDto();
    }

    google(): void {
        debugger;
        this.selectAddress = true;
    }

    database(): void {
        this.selectAddress = false;
    }

    googleDelivery(): void {
        debugger;
        this.selectAddressDelivery = true;
    }

    databaseDelivery(): void {
        this.selectAddressDelivery = false;
    }

    onAutocompleteSelected(result: PlaceResult) {
        debugger;
        if (result.address_components.length == 7) {
            this.wholeSaleLead.postalUnitNo = "";
          
            this.wholeSaleLead.postalUnitType = "";
            this.wholeSaleLead.postalStreetNo = "";
            this.postalStateName = "";
            this.wholeSaleLead.postalStateId = 0;
            this.wholeSaleLead.postalPostCode = "";
            this.wholeSaleLead.postalAddress = "";
            // this.wholeSaleLead.country = "";

            this.wholeSaleLead.postalStreetNo = result.address_components[0].long_name.toUpperCase();
            var str = result.address_components[1].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.wholeSaleLead.postalStreetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.wholeSaleLead.postalStreetType = splitted[3].toUpperCase().trim();
            }
            else if (splitted.length == 3) {
                this.wholeSaleLead.postalStreetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.wholeSaleLead.postalStreetType = splitted[2].toUpperCase().trim();
            }
            else {
                this.wholeSaleLead.postalStreetName = splitted[0].toUpperCase().trim();
                this.wholeSaleLead.postalStreetType = splitted[1].toUpperCase().trim();
            }
            // this.suburb = result.address_components[2].long_name.toUpperCase().trim();
            this.postalStateName = result.address_components[4].short_name;
            // this._leadsServiceProxy.getSuburbId(result.address_components[6].long_name).subscribe(result => {
            //     this.wholeSaleLead.suburbId = result;
            // });
            this._leadsServiceProxy.stateId(result.address_components[4].short_name).subscribe(result => {
                this.wholeSaleLead.postalStateId = result;
            });
            this.wholeSaleLead.postalPostCode = result.address_components[6].long_name.toUpperCase();
            this.wholeSaleLead.postalAddress = this.wholeSaleLead.postalUnitNo + " " + this.wholeSaleLead.postalUnitType + " " + this.wholeSaleLead.postalStreetNo + " " + this.wholeSaleLead.postalStreetName + " " + this.wholeSaleLead.postalStreetType;  //result.formatted_address;
            // this.wholeSaleLead.country = result.address_components[5].long_name.toUpperCase();
        }
        else if (result.address_components.length == 6) {
            this.wholeSaleLead.postalUnitNo = "";
            this.wholeSaleLead.postalUnitType = "";
           
            this.wholeSaleLead.postalStreetNo = "";
            this.wholeSaleLead.postalStreetName = "";
            this.wholeSaleLead.postalStreetType = "";
            
            this.postalStateName= "";
            this.wholeSaleLead.postalStateId = 0;
            this.wholeSaleLead.postalPostCode = "";
            this.wholeSaleLead.postalAddress = "";
            // this.wholeSaleLead.country = "";

            this.wholeSaleLead.postalStreetNo = result.address_components[0].long_name.toUpperCase();
            var str = result.address_components[1].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.wholeSaleLead.postalStreetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.wholeSaleLead.postalStreetType = splitted[3].toUpperCase().trim();
            }
            else if (splitted.length == 3) {
                this.wholeSaleLead.postalStreetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.wholeSaleLead.postalStreetType = splitted[2].toUpperCase().trim();
            }
            else {
                this.wholeSaleLead.postalStreetName = splitted[0].toUpperCase().trim();
                this.wholeSaleLead.postalStreetType = splitted[1].toUpperCase().trim();
            }
            // this.suburb = result.address_components[2].long_name.toUpperCase().trim();
            this.postalStateName = result.address_components[3].short_name;
            // this._leadsServiceProxy.getSuburbId(result.address_components[5].long_name).subscribe(result => {
            //     this.wholeSaleLead.suburbId = result;
            // });
            this._leadsServiceProxy.stateId(result.address_components[3].short_name).subscribe(result => {
                this.wholeSaleLead.postalStateId = result;
            });
            this.wholeSaleLead.postalPostCode = result.address_components[5].long_name.toUpperCase();
            this.wholeSaleLead.postalAddress = this.wholeSaleLead.postalUnitNo + " " + this.wholeSaleLead.postalUnitType + " " + this.wholeSaleLead.postalStreetNo + " " + this.wholeSaleLead.postalStreetName + " " + this.wholeSaleLead.postalStreetType;  //result.formatted_address;
            // this.wholeSaleLead.country = result.address_components[4].long_name.toUpperCase();
        }
        else {
            this.wholeSaleLead.postalUnitNo = "";
            this.wholeSaleLead.postalUnitType = "";
            this.wholeSaleLead.postalStreetNo = "";
            this.wholeSaleLead.postalStreetName = "";
            this.wholeSaleLead.postalStreetType = "";
            this.postalStateName = "";
            this.wholeSaleLead.postalStateId = 0;
            this.wholeSaleLead.postalPostCode = "";
            this.wholeSaleLead.postalAddress = "";

            var str1 = result.address_components[0].long_name.toUpperCase();
            var splitted1 = str1.split(" ");
            if (splitted1.length == 1) {
                this.wholeSaleLead.postalUnitNo = splitted1[0].toUpperCase().trim();
            }
            else {
                this.wholeSaleLead.postalUnitNo = splitted1[1].toUpperCase().trim();
                // this.wholeSaleLead.postalUnitType = splitted1[0].toUpperCase().trim();
                this.wholeSaleLead.postalUnitType = splitted1[0].toUpperCase().trim();
            }
            this.wholeSaleLead.postalStreetNo = result.address_components[1].long_name.toUpperCase();
            var str = result.address_components[2].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.wholeSaleLead.postalStreetNo = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.wholeSaleLead.postalStreetType = splitted[3].toUpperCase().trim();
            }
            else if (splitted.length == 3) {
                this.wholeSaleLead.postalStreetNo = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.wholeSaleLead.postalStreetType = splitted[2].toUpperCase().trim();
            }
            else {
                this.wholeSaleLead.postalStreetNo = splitted[0].toUpperCase().trim();
                this.wholeSaleLead.postalStreetType = splitted[1].toUpperCase().trim();
            }
            // this.suburb = result.address_components[3].long_name.toUpperCase().trim();
            this.postalStateName= result.address_components[5].short_name;
            // this._leadsServiceProxy.getSuburbId(result.address_components[7].long_name).subscribe(result => {
            //     this.wholeSaleLead.suburbId = result;
            // });
            this._leadsServiceProxy.stateId(result.address_components[5].short_name).subscribe(result => {
                this.wholeSaleLead.postalStateId = result;
            });
            this.wholeSaleLead.postalPostCode = result.address_components[7].long_name.toUpperCase();
            this.wholeSaleLead.postalAddress = this.wholeSaleLead.postalUnitNo + " " + this.wholeSaleLead.postalUnitType + " " + this.wholeSaleLead.postalStreetNo + " " + this.wholeSaleLead.postalStreetName + " " + this.wholeSaleLead.postalStreetType;  //result.formatted_address;
            // this.wholeSaleLead.country = result.address_components[6].long_name.toUpperCase();
        }
        this.bindArea();
    }

    onLocationSelected(location: Location) {
        debugger;
        this.wholeSaleLead.postalLatitude = location.latitude.toString();
        this.wholeSaleLead.postalLongitude = location.longitude.toString();
    }

    onAutocompleteSelectedDel(result: PlaceResult) {
        debugger;
        if (result.address_components.length == 7) {
            this.wholeSaleLead.delivaryUnitNo = "";
          
            this.wholeSaleLead.delivaryUnitType = "";
            this.wholeSaleLead.delivaryStreetNo = "";
            this.delivaryStateName = "";
            this.wholeSaleLead.delivaryStateId = 0;
            this.wholeSaleLead.delivaryPostCode = "";
            this.wholeSaleLead.delivaryAddress = "";
            // this.wholeSaleLead.country = "";

            this.wholeSaleLead.delivaryStreetNo = result.address_components[0].long_name.toUpperCase();
            var str = result.address_components[1].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.wholeSaleLead.delivaryStreetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.wholeSaleLead.delivaryStreetType = splitted[3].toUpperCase().trim();
            }
            else if (splitted.length == 3) {
                this.wholeSaleLead.delivaryStreetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.wholeSaleLead.delivaryStreetType = splitted[2].toUpperCase().trim();
            }
            else {
                this.wholeSaleLead.delivaryStreetName = splitted[0].toUpperCase().trim();
                this.wholeSaleLead.delivaryStreetType = splitted[1].toUpperCase().trim();
            }
            // this.suburb = result.address_components[2].long_name.toUpperCase().trim();
            this.delivaryStateName = result.address_components[4].short_name;
            // this._leadsServiceProxy.getSuburbId(result.address_components[6].long_name).subscribe(result => {
            //     this.wholeSaleLead.suburbId = result;
            // });
            this._leadsServiceProxy.stateId(result.address_components[4].short_name).subscribe(result => {
                this.wholeSaleLead.delivaryStateId = result;
            });
            this.wholeSaleLead.delivaryPostCode = result.address_components[6].long_name.toUpperCase();
            this.wholeSaleLead.delivaryAddress = this.wholeSaleLead.delivaryUnitNo + " " + this.wholeSaleLead.delivaryUnitType + " " + this.wholeSaleLead.delivaryStreetNo + " " + this.wholeSaleLead.delivaryStreetName + " " + this.wholeSaleLead.delivaryStreetType;  //result.formatted_address;
            // this.wholeSaleLead.country = result.address_components[5].long_name.toUpperCase();
        }
        else if (result.address_components.length == 6) {
            this.wholeSaleLead.delivaryUnitNo = "";
            this.wholeSaleLead.delivaryUnitType = "";
           
            this.wholeSaleLead.delivaryStreetNo = "";
            this.wholeSaleLead.delivaryStreetName = "";
            this.wholeSaleLead.delivaryStreetType = "";
            
            this.delivaryStateName= "";
            this.wholeSaleLead.delivaryStateId = 0;
            this.wholeSaleLead.delivaryPostCode = "";
            this.wholeSaleLead.delivaryAddress = "";
            // this.wholeSaleLead.country = "";

            this.wholeSaleLead.delivaryStreetNo = result.address_components[0].long_name.toUpperCase();
            var str = result.address_components[1].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.wholeSaleLead.delivaryStreetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.wholeSaleLead.delivaryStreetType = splitted[3].toUpperCase().trim();
            }
            else if (splitted.length == 3) {
                this.wholeSaleLead.delivaryStreetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.wholeSaleLead.delivaryStreetType = splitted[2].toUpperCase().trim();
            }
            else {
                this.wholeSaleLead.delivaryStreetName = splitted[0].toUpperCase().trim();
                this.wholeSaleLead.delivaryStreetType = splitted[1].toUpperCase().trim();
            }
            // this.suburb = result.address_components[2].long_name.toUpperCase().trim();
            this.delivaryStateName = result.address_components[3].short_name;
            // this._leadsServiceProxy.getSuburbId(result.address_components[5].long_name).subscribe(result => {
            //     this.wholeSaleLead.suburbId = result;
            // });
            this._leadsServiceProxy.stateId(result.address_components[3].short_name).subscribe(result => {
                this.wholeSaleLead.delivaryStateId = result;
            });
            this.wholeSaleLead.delivaryPostCode = result.address_components[5].long_name.toUpperCase();
            this.wholeSaleLead.delivaryAddress = this.wholeSaleLead.delivaryUnitNo + " " + this.wholeSaleLead.delivaryUnitType + " " + this.wholeSaleLead.delivaryStreetNo + " " + this.wholeSaleLead.delivaryStreetName + " " + this.wholeSaleLead.delivaryStreetType;  //result.formatted_address;
            // this.wholeSaleLead.country = result.address_components[4].long_name.toUpperCase();
        }
        else {
            this.wholeSaleLead.delivaryUnitNo = "";
            this.wholeSaleLead.delivaryUnitType = "";
            this.wholeSaleLead.delivaryStreetNo = "";
            this.wholeSaleLead.delivaryStreetName = "";
            this.wholeSaleLead.delivaryStreetType = "";
            this.delivaryStateName = "";
            this.wholeSaleLead.delivaryStateId = 0;
            this.wholeSaleLead.delivaryPostCode = "";
            this.wholeSaleLead.delivaryAddress = "";

            var str1 = result.address_components[0].long_name.toUpperCase();
            var splitted1 = str1.split(" ");
            if (splitted1.length == 1) {
                this.wholeSaleLead.delivaryUnitNo = splitted1[0].toUpperCase().trim();
            }
            else {
                this.wholeSaleLead.delivaryUnitNo = splitted1[1].toUpperCase().trim();
                // this.wholeSaleLead.delivaryUnitType = splitted1[0].toUpperCase().trim();
                this.wholeSaleLead.delivaryUnitType = splitted1[0].toUpperCase().trim();
            }
            this.wholeSaleLead.delivaryStreetNo = result.address_components[1].long_name.toUpperCase();
            var str = result.address_components[2].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.wholeSaleLead.delivaryStreetNo = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.wholeSaleLead.delivaryStreetType = splitted[3].toUpperCase().trim();
            }
            else if (splitted.length == 3) {
                this.wholeSaleLead.delivaryStreetNo = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.wholeSaleLead.delivaryStreetType = splitted[2].toUpperCase().trim();
            }
            else {
                this.wholeSaleLead.delivaryStreetNo = splitted[0].toUpperCase().trim();
                this.wholeSaleLead.delivaryStreetType = splitted[1].toUpperCase().trim();
            }
            // this.suburb = result.address_components[3].long_name.toUpperCase().trim();
            this.delivaryStateName= result.address_components[5].short_name;
            // this._leadsServiceProxy.getSuburbId(result.address_components[7].long_name).subscribe(result => {
            //     this.wholeSaleLead.suburbId = result;
            // });
            this._leadsServiceProxy.stateId(result.address_components[5].short_name).subscribe(result => {
                this.wholeSaleLead.delivaryStateId = result;
            });
            this.wholeSaleLead.delivaryPostCode = result.address_components[7].long_name.toUpperCase();
            this.wholeSaleLead.delivaryAddress = this.wholeSaleLead.delivaryUnitNo + " " + this.wholeSaleLead.delivaryUnitType + " " + this.wholeSaleLead.delivaryStreetNo + " " + this.wholeSaleLead.delivaryStreetName + " " + this.wholeSaleLead.delivaryStreetType;  //result.formatted_address;
            // this.wholeSaleLead.country = result.address_components[6].long_name.toUpperCase();
        }
        this.bindArea();
    }

    onLocationSelectedDel(location: Location) {
        debugger;
        this.wholeSaleLead.delivaryLatitude = location.latitude.toString();
        this.wholeSaleLead.delivaryLongitude = location.longitude.toString();
    }

    save() :void{
        debugger;
        this.saving = true;
        
        this.wholeSaleLead.customerType = "";
        this.customerTypes.forEach(element => {
            debugger;
            this.wholeSaleLead.customerType = this.wholeSaleLead.customerType + element.name +","
        });

        this.wholeSaleLead.isDelivery = this.DelAddress;
        this._wholeSaleLeadServiceProxy.checkExistWholeSaleLeadsList(this.wholeSaleLead)
        .pipe(finalize(() => {this.saving = false;}))
        .subscribe(dupresult => {
            debugger;
            if (dupresult.length > 0) {
                // this.message.confirm('Do You Still Want to Proceed',
                // "Looks Like Duplicate Record",
                // (isConfirmed) => {
                //      if (isConfirmed) {
                //         this._wholeSaleLeadServiceProxy.createOrEdit(this.wholeSaleLead)
                //         .pipe(finalize(() => { this.saving = false; }))
                //         .subscribe(() => {
                //              // this.viewLeadDetail.reloadLead.emit(false);
                //              this.Reset();
                //              this.saving = false; 
                //              this.notify.info(this.l('SavedSuccessfully'));
                //              this._router.navigate(['/app/main/wholesale/leads']); 
                //             });
                //     }
                //     else {
                //         this.saving = false;
                //     }
                // }
                // );
                this.ViewDuplicateWholesaleLeadPopUpModal.show(dupresult,this.wholeSaleLead);
            }
            else{
                this._wholeSaleLeadServiceProxy.createOrEdit(this.wholeSaleLead)
                .pipe(finalize(() => { this.saving = false; }))
                .subscribe(() => {
                    // this.viewLeadDetail.reloadLead.emit(false);
                    this.Reset();
                    this.saving = false; 
                    this.notify.info(this.l('SavedSuccessfully'));
                    this._router.navigate(['/app/main/wholesale/leads']);
                });
            }
        })

    }

}
