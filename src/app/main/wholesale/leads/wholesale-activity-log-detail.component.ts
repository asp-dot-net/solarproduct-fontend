import { Component, EventEmitter, Injector, Input, OnInit, Output, ViewChild } from '@angular/core';
import {
    WholeSaleLeadServiceProxy, GetActivityLogViewDto, GetLeadForViewDto, GetLeadForChangeStatusOutput, LeadDto, CommonLookupDto, LeadsServiceProxy, LeadtrackerHistoryDto, WholeSalePromotionResponseStatusesServiceProxy, WholeSalePromotionsServiceProxy, GetPromotionHistorybyPromotionIdOutPut
} from '@shared/service-proxies/service-proxies';

import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import PlaceResult = google.maps.places.PlaceResult;
import { JobsComponent } from '@app/main/jobs/jobs/jobs.component';
import { PromotionStopResponceComponent } from '@app/main/promotions/promotionUsers/stop-responce.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { WholeSaleActivityLogHistoryComponent } from './activity-log-history.component';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import * as moment from 'moment';
import { AgmCoreModule } from '@agm/core';
import * as _ from 'lodash';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'wholeSaleactivityLogDetail',
    templateUrl: './wholesale-activity-log-detail.component.html',
    animations: [appModuleAnimation()]
})
export class WholeSaleActivityLogDetailComponent extends AppComponentBase implements OnInit {

    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @ViewChild('wholesaleactivityloghistory', { static: true }) wholesaleactivityloghistory: WholeSaleActivityLogHistoryComponent;
    //@ViewChild('wholesaleactivitylogdetailhistory', { static: true }) wholesaleactivitylogdetailhistory: WholeSaleActivityLogHistoryComponent;


    @Input() SelectedLeadId: number = 0;
    @Output() reloadLead = new EventEmitter();
    active = false;
    saving = false;
    item: GetLeadForViewDto;
    activeTabIndex: number = 0;
    activityDatabyDate: any[] = [];
    actionId: number = 0;
    activity: boolean = false;
    leadActivityList: GetActivityLogViewDto[];
    leadActionList: CommonLookupDto[];
    leadId: number = 0;
    id: number = 0;
    role: string = '';
    sectionId: number = 0;
    showsectionwise = false;
    currentactivity: boolean = false;
    allActivity: boolean = false;
    serviceId : number =0 ;
    promotionHistory : GetPromotionHistorybyPromotionIdOutPut;
    leadtrackerHistory: LeadtrackerHistoryDto[];
    type = "";
    responseMessage = "";
    viewType = 'promotion';
    constructor(
        injector: Injector,        
        private sanitizer: DomSanitizer,
        private _wholeSalePromotionsServiceProxy: WholeSalePromotionsServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _router: Router,
        private spinner: NgxSpinnerService,
    ) {
        super(injector);
        this.item = new GetLeadForViewDto();
        this.item.lead = new LeadDto();
        this.promotionHistory = new GetPromotionHistorybyPromotionIdOutPut();

    }


    ngOnInit(): void {
        //this.show(this._activatedRoute.snapshot.queryParams['id']);
        // this._wholeSaleleadsServiceProxy.getAllLeadAction().subscribe(result => {
        //     this.leadActionList = result;
        // });

        this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
            this.role = result;
            if (this.role == 'Sales Rep') {
                this.activity = true;
            }
        });
    }

    show(promotionId : number , responce? : string,type : string = 'promotion'): void {
        debugger;
        this.responseMessage = responce;
        // this.sectionId = sectionId;
        this.activity = false;
        this.allActivity = false;
        this.viewType = type;
        // this.serviceId = service;
        if(type == 'activityEmail'){
            this.promotionHistory.description = responce;
        }else{
            this._wholeSalePromotionsServiceProxy.getPromotionHistorybyPromotionId(promotionId).subscribe(result => {
                this.promotionHistory = result;
                this.type = result.promotionSendingId == 1 ? "SMS" : "Email";
                if(!result.id || result.id == 0){
                            this._wholeSalePromotionsServiceProxy.getWholeSalePromotionForView(promotionId).subscribe(r => {
                                this.promotionHistory.description = r.wholeSalePromotion.description;
                                this.type = r.wholeSalePromotionTypeName;
                                this.promotionHistory.charges =r.wholeSalePromotion.promoCharge;
                                this.promotionHistory.title = r.wholeSalePromotion.title;
                            })
                }
                
                
            });
        }
        
        this.setHtml(this.promotionHistory.description);
        this.modal.show();
    }

    close(): void {
        this.modal.hide();
    }

    htmlContentBody: any;

    setHtml(emailHTML) {
        let compiled = _.template(emailHTML);
        let myTemplateCompiled = compiled();
        this.htmlContentBody = myTemplateCompiled;
        this.htmlContentBody = this.sanitizer.bypassSecurityTrustHtml(this.htmlContentBody);

        // if(htmlTemplate != '')
        // {
        //     this.spinner.hide();
        //     this.modal.show();
        // }
    }

   

    
    navigateToLeadHistory(leadid): void {

        // this.wholesaleactivityloghistory.show(leadid);
    }

    

    
}


