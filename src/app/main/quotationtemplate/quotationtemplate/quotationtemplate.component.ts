import { Component, Injector, ViewEncapsulation, ViewChild, OnInit, HostListener } from '@angular/core';
import {  Router} from '@angular/router';
import { CommonLookupServiceProxy, OrganizationUnitDto, QuotationTamplateServiceProxy,UserActivityLogServiceProxy,UserActivityLogDto , QuotationTemplateDto  } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import * as _ from 'lodash';
import { Title } from '@angular/platform-browser';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './quotationtemplate.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class QuotationTemplateComponent extends AppComponentBase implements OnInit {
    
    show: boolean = true;
    showchild: boolean = true;
    toggleBlock(){
        this.show = !this.show;
    };
    toggleBlockChild(){
        this.showchild = !this.showchild;
    };
       
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    advancedFiltersAreShown = false;
    filterText = '';
    organizationUnit = 0;
    templateType = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    AllTemplateType: any = [];
    public screenHeight: any;  
    testHeight = 250;
    constructor(
        injector: Injector,
        private _quotationTemplateServiceProxy: QuotationTamplateServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
		private _router: Router,
        private titleService: Title,
        private _commonLookupService: CommonLookupServiceProxy,
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Html Template");
    }
    
    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Html Templates';
        log.section = 'Html Templates';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
        this._commonLookupService.getTemplateTypeForTableDropdown().subscribe(result => {
            this.AllTemplateType = result;
        });
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
        });
    }
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }
    getQuotationTemplates(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._quotationTemplateServiceProxy.getAll(
            this.filterText,
            this.organizationUnit,
            this.templateType,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
        });
        
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createQuotationTemplate(val = 0): void {
		//this.createOrEditQuotationTemplateModal.show();  
        if(val == 0) {
            this._router.navigate(['/app/main/quotationtemplate/quotationtemplate/createOrEdit']);
        }
        else{
            this._router.navigate(['/app/main/quotationtemplate/quotationtemplate/createOrEdit'], { queryParams: {Id : val} });
        }
        
    }
    searchLog() : void {
        debugger;
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Html Templates';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    deleteQuotationTemplate(quotationTemplate: QuotationTemplateDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._quotationTemplateServiceProxy.delete(quotationTemplate.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete Html Templates: ' + quotationTemplate.templateName;
                            log.section = 'Html Templates';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });
                        });
                }
            }
        );
    }
}
