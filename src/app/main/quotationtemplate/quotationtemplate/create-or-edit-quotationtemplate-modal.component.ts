import { Component,  Injector} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { QuotationTamplateServiceProxy, CreateOrEditQuotationTemplateDto, CommonLookupServiceProxy,UserActivityLogServiceProxy,UserActivityLogDto  } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { Title } from '@angular/platform-browser';

@Component({
    selector: 'createOrEditQuote',
    templateUrl: './create-or-edit-quotationtemplate-modal.component.html',
    styleUrls: ['./create-or-edit-quotationtemplate-modal.component.less'],
})
export class CreateOrEditQuotationTemplateModalComponent extends AppComponentBase {
    saving = false;
    quotationTemplate: CreateOrEditQuotationTemplateDto = new CreateOrEditQuotationTemplateDto();
    allOrganizationUnits = [];
    templateType = [];
    htmlContentbody: any = '';
    constructor(
        injector: Injector,
        private _quotationTemplateServiceProxy: QuotationTamplateServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private titleService: Title,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Html Template");
    }

    ngOnInit(): void {
    debugger;
        this._activatedRoute.queryParams.subscribe(params => {
            this.quotationTemplate.id = params['Id'];
        });

        if (this.quotationTemplate.id != null) {
           
            if (this.quotationTemplate.id !== null && this.quotationTemplate.id !== undefined) {
                this._quotationTemplateServiceProxy.getQuotationTemplateForEdit(this.quotationTemplate.id).subscribe(result => {
                    this.quotationTemplate = result.quotationTemplate;
                    this.htmlContentbody = this.quotationTemplate.viewHtml;
                    let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Edit Html Templates : ' + this.quotationTemplate.templateName;
            log.section = 'Html Templates';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
                });
            }
            
        }
        else{
            this.quotationTemplate = new CreateOrEditQuotationTemplateDto();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Html Templates';
            log.section = 'Html Templates';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
        }
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
        });

        this._commonLookupService.getTemplateTypeForTableDropdown().subscribe(result => {
            this.templateType = result;
        });
    }

    cancel(): void {
        this._router.navigate(['app/main/quotationtemplate/quotationtemplate']);
    }

    config: AngularEditorConfig = {
        editable: true,
        spellcheck: true,
        height: "45rem",
        minHeight: "15rem",
        placeholder: "Enter text here...",
        translate: "no",
        defaultParagraphSeparator: "p",
        defaultFontName: "Arial",
        toolbarHiddenButtons: [["bold"]],
        sanitize: false,
        customClasses: [
            {
                name: "quote",
                class: "quote"
            },
            {
                name: "redText",
                class: "redText"
            },
            {
                name: "titleText",
                class: "titleText",
                tag: "h1"
            }
        ],
        uploadUrl: 'v1/image',
        uploadWithCredentials: false,
        toolbarPosition: 'top',
      
    };

    editorLoaded() {
        if (this.quotationTemplate.id !== null && this.quotationTemplate.id !== undefined) {
            this._quotationTemplateServiceProxy.getQuotationTemplateForEdit(this.quotationTemplate.id).subscribe(result => {
                this.quotationTemplate = result.quotationTemplate;
                this.htmlContentbody = this.quotationTemplate.viewHtml;
            });
        }
    }
   
    saveDesign(): void {
        this.saving = true;
        
        this._quotationTemplateServiceProxy.createOrEdit(this.quotationTemplate)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                if(this.quotationTemplate.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Html Templates Updated : '+ this.quotationTemplate.templateName;
                    log.section = 'Html Templates';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Html Templates Created : '+ this.quotationTemplate.templateName;
                    log.section = 'Html Templates';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
                this._router.navigate(['app/main/quotationtemplate/quotationtemplate']);
                // this.close();
                // this.modalSave.emit(null);
            });
    }

}
