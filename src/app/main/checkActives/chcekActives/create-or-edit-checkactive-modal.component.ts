import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {  CreateOrEditCheckActiveDto, CheckActiveServiceProxy,CreateOrEditCheckApplicationDto,UserActivityLogDto ,UserActivityLogServiceProxy} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';



@Component({
    selector: 'createOrEditCheckActiveModal',
    templateUrl: './create-or-edit-checkactive-modal.component.html'
})
export class CreateOrEditCheckActiveModalComponent extends AppComponentBase {
   
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    saving = false;

    checkActive: CreateOrEditCheckActiveDto = new CreateOrEditCheckActiveDto();
    constructor(
        injector: Injector,
        private _checkActiveServiceProxy: CheckActiveServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }
    
    show(checkActiveId?: number): void {
    
        if (!checkActiveId) {
            this.checkActive = new CreateOrEditCheckApplicationDto();
            this.checkActive.id = checkActiveId;
            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Check Active';
            log.section = 'Check Active';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._checkActiveServiceProxy.getCheckActiveForEdit(checkActiveId).subscribe(result => {
                this.checkActive = result.checkActive;
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Check Active : ' + this.checkActive.name;
                log.section = 'Check Active';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            });
        }
        
    }
    onShown(): void {
        
        document.getElementById('CheckActives_CheckActives').focus();
    }
    save(): void {
            this.saving = true;
            this._checkActiveServiceProxy.createOrEdit(this.checkActive)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.checkActive.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Check Active Updated : '+ this.checkActive.name;
                    log.section = 'Check Active';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Check Active Created : '+ this.checkActive.name;
                    log.section = 'Check Active';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }



    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
