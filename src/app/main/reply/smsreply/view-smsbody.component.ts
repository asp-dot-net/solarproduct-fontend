﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { LeadsServiceProxy, SmsReplyDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
    selector: 'viewsmsreposnceBodymodel',
    templateUrl: './view-smsbody.component.html'
})
export class ViewSMSBodyComponent extends AppComponentBase {
    @ViewChild('viewmodel', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item : SmsReplyDto;
    responcebody :  any;
    body :any;
    constructor(
        injector: Injector,
        private _leadsServiceProxy: LeadsServiceProxy
    ) {
        super(injector);
    }

    show(Id: number): void {
        
        this._leadsServiceProxy.getLeadActivitybyID(Id).subscribe(result => {      
            this.item = result;
            this.responcebody = this.item.responceBody;
            this.body = this.item.body;
           this.modal.show();
        });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
