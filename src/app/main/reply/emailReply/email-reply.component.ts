import { Component, Injector, ViewChild, ViewEncapsulation, HostListener } from '@angular/core';
import { CommonLookupServiceProxy, EmailReplyDto, LeadsServiceProxy, OrganizationUnitDto, TokenAuthServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy, UserServiceProxy } from '@shared/service-proxies/service-proxies';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import * as _ from 'lodash';
import * as moment from 'moment';
import { HttpClient } from '@angular/common/http';
import { OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ViewEmailBodyComponent } from './view-emailbody.component';
import { Title } from '@angular/platform-browser';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'app-email-reply',
    templateUrl: './email-reply.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class EmailReplyComponent extends AppComponentBase implements OnInit {

    @ViewChild('viewEmailBodymodel', { static: true }) viewEmailBodymodel: ViewEmailBodyComponent;
    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    organizationUnitlength: number=0;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    SelectedLeadId: number = 0;
    ComposeBox = false;
    emailDetailView = false;
    advancedFiltersAreShown = false;
    filterText = '';
    copanyNameFilter = '';
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnit = 0;
    sampleDateRange: moment.Moment[] = [moment().add(-7, 'days').endOf('day'), moment().startOf('day')];
    dateFilterType = 'Creation';
    StartDate: moment.Moment;
    EndDate: moment.Moment;
    ExpandedView: boolean = true;
    firstrowcount = 0;
    last = 0;
    datalenght = 0;
    olddataresult: EmailReplyDto[];
    FiltersData = false;
    filterName = "";
    
    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 110;

    constructor(
        injector: Injector,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _userServiceProxy: UserServiceProxy,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _httpClient: HttpClient,
        private _router: Router,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private titleService: Title
        ) {
            super(injector);
            this.titleService.setTitle(this.appSession.tenancyName + " |  Email");
    }
    ngOnInit(): void {

        this.screenWidth = window.innerWidth;  
        this.screenHeight = window.innerHeight; 

        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Email';
            log.section = 'Email';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.getEmailReply();

        });
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) {  
        this.screenWidth = window.innerWidth;  
        this.screenHeight = window.innerHeight;
    }  

    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + 90 ;
        }

        else {
            this.testHeight = this.testHeight - 90 ;
        }
    }

    clear() {
        this.filterText = '';
        this.sampleDateRange = [moment().add(-7, 'days').endOf('day'), moment().startOf('day')];
        this.getEmailReply();
    }
    onShown(): void {
        document.getElementById('filterText').focus();
    }

    emailListData: any [];


    getEmailReply(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.StartDate = this.sampleDateRange[0];
        this.EndDate = this.sampleDateRange[1];
        this.primengTableHelper.showLoadingIndicator();

        this._leadsServiceProxy.getEmailReplyList(
            this.filterName,
            this.StartDate,
            this.EndDate,
            this.organizationUnit,
            this.filterText,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.emailListData = result.items;
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            this.shouldShow = false;
        });
    }


    getOldemailReply(leadid)
    {
        this._leadsServiceProxy.getOldEmailReplyList(leadid,this.organizationUnit).subscribe(result => {
           //debugger;
            this.olddataresult = result;
            this.datalenght = result.length;
            this.shouldShow = true;
        });
    }
    exportToExcel(): void {
    }

    expandGrid() {
        this.ExpandedView = true;
    }

    navigateEmailBodymodel(id): void {
        this.ExpandedView = !this.ExpandedView;
        this.ExpandedView = false;
        this.SelectedLeadId = id;
        this.viewEmailBodymodel.show(id);
    }

    reloadPage($event): void {
        this.ExpandedView = true;
     
        this.paginator.changePage(this.paginator.getPage());
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Email';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Email';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }
}
