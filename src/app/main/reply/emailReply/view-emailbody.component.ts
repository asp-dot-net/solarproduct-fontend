﻿import { Component, ViewChild, Injector, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { CreateOrEditEmailTemplateDto, EmailTemplateServiceProxy, LeadsServiceProxy, SmsReplyDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { EmailEditorComponent } from 'angular-email-editor';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
    selector: 'viewEmailBodymodel',
    templateUrl: './view-emailbody.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ViewEmailBodyComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    emailData = '';
    email: CreateOrEditEmailTemplateDto = new CreateOrEditEmailTemplateDto();
    item: SmsReplyDto;
    responcebody: any;
    templateid: any;
    constructor(
        injector: Injector,
        private _leadsServiceProxy: LeadsServiceProxy,
    ) {
        super(injector);
    }
    @ViewChild(EmailEditorComponent)
    private emailEditor: EmailEditorComponent;

    show(Id: number): void {

        this._leadsServiceProxy.getLeadActivitybyID(Id).subscribe(result => {
            debugger;
            this.item = result;
            this.responcebody = this.item.responceBody;
            this.templateid = this.item.templateId;
        });
    }
    
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
