﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { HoldReasonsServiceProxy, CreateOrEditHoldReasonDto, CreateOrEditServiceCategoryDto, ServiceCategorysServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';


@Component({
    selector: 'createOrEditserviceCategoryModal',
    templateUrl: './create-or-edit-serviceCategory-modal.component.html'
})
export class CreateOrEditServiceCategoryModalComponent extends AppComponentBase {
   
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    serviceCategory: CreateOrEditServiceCategoryDto = new CreateOrEditServiceCategoryDto();



    constructor(
        injector: Injector,
        private _serviceCategoryServiceProxy: ServiceCategorysServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }
    
    show(serviceCategoryId?: number): void {
    

        if (!serviceCategoryId) {
            this.serviceCategory = new CreateOrEditServiceCategoryDto();
            this.serviceCategory.id = serviceCategoryId;

            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Service Category';
            log.section = 'Service Category';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._serviceCategoryServiceProxy.getServiceCategoryForEdit(serviceCategoryId).subscribe(result => {
                this.serviceCategory = result.serviceCategorys;


                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Service Category : ' + this.serviceCategory.name;
                log.section = 'Service Category';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        
    }
    onShown(): void {
        
        document.getElementById('ServiceCategory_ServiceCategory').focus();
    }
    save(): void {
            this.saving = true;
            this._serviceCategoryServiceProxy.createOrEdit(this.serviceCategory)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.serviceCategory.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Service Category Updated : '+ this.serviceCategory.name;
                    log.section = 'Service Category';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Service Category Created : '+ this.serviceCategory.name;
                    log.section = 'Service Category';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
