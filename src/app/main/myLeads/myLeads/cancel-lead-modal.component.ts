import { Component, ElementRef, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { LeadsServiceProxy, GetLeadForViewDto, LeadDto,ActivityLogInput, GetLeadForChangeStatusOutput, LookupTableDto } from '@shared/service-proxies/service-proxies';
import * as _ from 'lodash';
import * as moment from 'moment';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
    selector: 'cancelLeadModel',
    templateUrl: './cancel-lead-modal.component.html',
})
export class CancelLeadModelComponent extends AppComponentBase{

    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    CancelReason:string;
    CancelReasonId : number;
    LeadId:number;	
    active = false;
    saving = false;
    allCancelReasons: LookupTableDto[];
    constructor(
        injector: Injector,      
        private _leadsServiceProxy: LeadsServiceProxy
    ) {
        super(injector);
    }

     show(LeadId?: number): void {
        this.LeadId = LeadId;
        this.modal.show();
        this._leadsServiceProxy.getAllCancelReasonForTableDropdown().subscribe(result => {						
            this.allCancelReasons = result;
        });
     }

     close(): void {
        this.active = false;
        this.CancelReason = '';
        this.modal.hide();
     }

     save(): void{
        let status :GetLeadForChangeStatusOutput = new GetLeadForChangeStatusOutput();
        status.id = this.LeadId;
        status.leadStatusID = 8;
        status.rejectReason = this.CancelReason;
        status.cancelReasonId = this.CancelReasonId;
        
        this._leadsServiceProxy.changeStatus(status)
            .subscribe(() => {
                this.notify.success(this.l('LeadCanceledSuccessfully'));
                this.CancelReason = '';
                this.modal.hide();
                this.modalSave.emit(null);
            });
    }
}
