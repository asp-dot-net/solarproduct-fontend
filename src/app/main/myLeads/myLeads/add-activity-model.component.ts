import { Component, ElementRef, EventEmitter, Injectable, Injector, OnInit, Optional, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { LeadsServiceProxy, GetLeadForViewDto, LeadDto, ActivityLogInput, CommonLookupDto, EmailTemplateServiceProxy, SmsTemplatesServiceProxy, JobsServiceProxy, JobPromotionsServiceProxy, JobPromotionPromotionMasterLookupTableDto, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import * as _ from 'lodash';
import * as moment from 'moment';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';
import { EmailEditorComponent } from 'angular-email-editor';
import { AppConsts } from '@shared/AppConsts';
import { ViewMyLeadComponent } from './view-mylead.component';
import { isMoment } from 'moment';
import { Editor } from 'primeng/editor';
import { OWL_DATE_TIME_FORMATS } from 'ng-pick-datetime';
import { NgxSpinnerService } from 'ngx-spinner';
declare var Quill: any;

@Component({
    selector: 'addActivityModal',
    templateUrl: './add-activity-model.component.html'
})

export class AddActivityModalComponent extends AppComponentBase implements OnInit {
    
    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @ViewChild('SampleDatePicker', { static: true }) sampleDatePicker: ElementRef;
    @ViewChild("myNameElem") myNameElem: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @Output() reloadLead = new EventEmitter<boolean>();

    saving = false;
    sampleDate: moment.Moment = moment().add(1, 'days').startOf('day');
    item: GetLeadForViewDto;
    activityLog: ActivityLogInput = new ActivityLogInput();
    activityType: number = 6;
    priorityType: number = 1;
    activityName = "";
    actionNote = "";
    id = 0;
    allEmailTemplates: CommonLookupDto[];
    allSMSTemplates: CommonLookupDto[];
    emailData = '';
    smsData = '';
    emailTemplate: number;
    uploadUrl: string;
    uploadedFiles: any[] = [];
    myDate = new Date();
    date = moment(this.myDate);
    total = 0;
    credit = 0;
    customeTagsId = 0;
    role: string = '';
    subject = '';
    userName: string;
    leadCompanyName: any;
    pageid = 0;
    allUsers: CommonLookupDto[];
    referanceId = 0;
    CurrentDate = new Date();
    freebieTransport: JobPromotionPromotionMasterLookupTableDto[];
    emailfrom = "";
    fromemails: CommonLookupDto[];
    leadid: number;
    criteriaavaibaleornot = false;
    smsid = false;
    serviceId = 0;
    sectionName = '';
    constructor(
        injector: Injector,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _router: Router,
        private _jobsServiceProxy: JobsServiceProxy,
        private _jobPromotionsServiceProxy: JobPromotionsServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private spinner: NgxSpinnerService,
        @Optional() private viewLeadDetail?: ViewMyLeadComponent
    ) {
        super(injector);
        this.item = new GetLeadForViewDto();
        this.item.lead = new LeadDto();
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/DemoUiComponents/UploadAttachments';

        // var fontSizeStyle = Quill.import('attributors/style/size');
        // fontSizeStyle.whitelist = ['24px', '48px', '100px', '200px'];
        // Quill.register(fontSizeStyle, true);

        // var FontAttributor = Quill.import('formats/font');
        // var fonts = ['impact', 'courier', 'comic'];

        // FontAttributor.whitelist = fonts;
        // Quill.register(FontAttributor, true);

        // var quill = new Quill('#container', {
        // modules: {
        //     toolbar: {container: "#toolbar-container"}
        // },
        // placeholder: 'Type something here',
        // theme: 'snow'  // 'snow' or 'bubble'
        // });
    }

    @ViewChild(EmailEditorComponent)
    private emailEditor: EmailEditorComponent;

    ngOnInit(): void {
        this.activityLog.emailTemplateId = 0;
        this.activityLog.smsTemplateId = 0;

    }

    show(leadId: number, id?: number, pageid?: number, sectionId?: number, refundfreebiesid?: number,serviceId? :number,section? : string): void {
        this.sectionName = section;
        
        this.activityLog.activityNote = "";
        this.activityLog.emailTemplateId = 0;
        this.activityLog.smsTemplateId = 0;
        this.activityLog.referanceId = 0;
        this.activityLog.body = "";
        this.pageid = pageid;
        this.activityLog.sectionId = sectionId;
        this.activityLog.referanceId = refundfreebiesid;
        this.emailfrom = "info@solarbridge.com.au";
        this.leadid = leadId;
        this.serviceId = serviceId;
        if (id == 9) {
            this.id = id;
            this.activityType = 9;
        }
        else if (id == 8) {
            this.id = id;
            this.activityType = 8;
        }
        else if (id == 24) {
            this.id = id;
            this.activityType = 24;
        }
        else if (id == 25) {
            this.id = id;
            this.activityType = 25;
        }
        else {
            if(sectionId == 28)
            {
                this.activityType = 6;
            }
            else{
                this.activityType = 8;
            }
        }

        this.spinner.show();
        this._leadsServiceProxy.getOrgWiseDefultandownemailadd(leadId).subscribe(result => {
            debugger;
            this.fromemails = result;
            this.activityLog.emailFrom = this.fromemails[0].displayName;
        });
        this._leadsServiceProxy.getLeadForView(leadId, 0).subscribe(result => {
            debugger;
            this.item = result;
            this.leadCompanyName = result.lead.companyName;
            this.selection();
            this.modal.show();

            this.spinner.hide();
        });
        this._leadsServiceProxy.getallEmailTemplates(leadId).subscribe(result => {
            this.allEmailTemplates = result;
        });

        this._leadsServiceProxy.getallSMSTemplates(leadId).subscribe(result => {
            this.allSMSTemplates = result;
        });

        this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
            this.role = result;
        });
        this._jobsServiceProxy.getUsersListForToDo().subscribe(result => {
            this.allUsers = result;
        });
        this._leadsServiceProxy.getCurrentUserIdName().subscribe(result => {
            this.userName = result;

        });
        this._jobPromotionsServiceProxy.getAllFreebieTransportRepositoryForTableDropdown().subscribe(result => {
            this.freebieTransport = result;
        });
    }

    selection(): void {
        this.activityLog.body = '';
        this.activityLog.emailTemplateId = 0;
        this.activityLog.customeTagsId = 0;
        this.activityLog.subject = '';
        if (this.activityType == 6) {
            this.activityName = "SMS To ";
            this.actionNote = " SMS Sent";
            this._leadsServiceProxy.checkorgwisephonedynamicavaibleornot(this.leadid).subscribe(result => {
                this.criteriaavaibaleornot = result;
                if (!this.criteriaavaibaleornot) {
                    this.message.error("authentication issue");
                }
            });
        }
        else if (this.activityType == 7) {
            this.activityName = "Email To ";
            this.actionNote = " Email Sent";
        }
        else if (this.activityType == 8) {
            this.activityName = "Reminder To ";
            this.actionNote = " Reminder Set";
        }
        else if (this.activityType == 9) {
            this.activityName = "Notify To ";
            this.actionNote = " Notification";
        }
        else if (this.activityType == 24) {
            this.activityName = "Comment ";
            this.actionNote = "Comment";
        }
        else if (this.activityType == 25) {
            this.activityName = "ToDo";
            this.actionNote = "ToDo";
        }
        let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Opend '+ this.actionNote + ' For ' + this.leadCompanyName + this.item.jobNumber;
            log.section = this.sectionName;
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    onTagChange(event): void {

        const id = event.target.value;
        if (id == 1) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Customer.Name}}";
            } else {
                this.activityLog.body = "{{Customer.Name}}";
            }

        } else if (id == 2) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Customer.Mobile}}";
            } else {
                this.activityLog.body = "{{Customer.Mobile}}";
            }
        } else if (id == 3) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Customer.Phone}}";
            } else {
                this.activityLog.body = "{{Customer.Phone}}";
            }
        } else if (id == 4) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Customer.Email}}";
            } else {
                this.activityLog.body = "{{Customer.Email}}";
            }
        } else if (id == 5) {

            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Customer.Address}}";
            } else {
                this.activityLog.body = "{{Customer.Address}}";
            }
        } else if (id == 6) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Sales.Name}}";
            } else {
                this.activityLog.body = "{{Sales.Name}}";
            }
        } else if (id == 7) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Sales.Mobile}}";
            } else {
                this.activityLog.body = "{{Sales.Mobile}}";
            }
        } else if (id == 8) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Sales.Email}}";
            } else {
                this.activityLog.body = "{{Sales.Email}}";
            }
        }
        else if (id == 9) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Quote.ProjectNo}}";
            } else {
                this.activityLog.body = "{{Quote.ProjectNo}}";
            }
        }
        else if (id == 10) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Quote.SystemCapacity}}";
            } else {
                this.activityLog.body = "{{Quote.SystemCapacity}}";
            }
        }
        else if (id == 11) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Quote.AllproductItem}}";
            } else {
                this.activityLog.body = "{{Quote.AllproductItem}}";
            }
        }
        else if (id == 12) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Quote.TotalQuoteprice}}";
            } else {
                this.activityLog.body = "{{Quote.TotalQuoteprice}}";
            }
        }
        else if (id == 13) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Quote.InstallationDate}}";
            } else {
                this.activityLog.body = "{{Quote.InstallationDate}}";
            }
        }
        else if (id == 14) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Quote.InstallerName}}";
            } else {
                this.activityLog.body = "{{Quote.InstallerName}}";
            }
        }
        else if (id == 15) {
            if (this.activityLog.body != null) {
              this.activityLog.body = this.activityLog.body + " " + "{{Freebies.TransportCompany}}";
            } else {
              this.activityLog.body = "{{Freebies.TransportCompany}}";
            }
          }
          else if (id == 16) {
            if (this.activityLog.body != null) {
              this.activityLog.body = this.activityLog.body + " " + "{{Freebies.TransportLink}}";
            } else {
              this.activityLog.body = "{{Freebies.TransportLink}}";
            }
          }
          else if (id == 17) {
            if (this.activityLog.body != null) {
              this.activityLog.body = this.activityLog.body + " " + "{{Freebies.DispatchedDate}}";
            } else {
              this.activityLog.body = "{{Freebies.DispatchedDate}}";
            }
          }
          else if (id == 18) {
            if (this.activityLog.body != null) {
              this.activityLog.body = this.activityLog.body + " " + "{{Freebies.TrackingNo}}";
            } else {
              this.activityLog.body = "{{Freebies.TrackingNo}}";
            }
          }
          else if (id == 19) {
            if (this.activityLog.body != null) {
              this.activityLog.body = this.activityLog.body + " " + "{{Freebies.PromoType}}";
            } else {
              this.activityLog.body = "{{Freebies.PromoType}}";
            }
          }
          else if (id == 20) {
            if (this.activityLog.body != null) {
              this.activityLog.body = this.activityLog.body + " " + "{{Invoice.UserName}}";
            } else {
              this.activityLog.body = "{{Invoice.UserName}}";
            }
          }
          else if (id == 21) {
            if (this.activityLog.body != null) {
              this.activityLog.body = this.activityLog.body + " " + "{{Invoice.UserMobile}}";
            } else {
              this.activityLog.body = "{{Invoice.UserMobile}}";
            }
          }
          else if (id == 22) {
            if (this.activityLog.body != null) {
              this.activityLog.body = this.activityLog.body + " " + "{{Invoice.UserEmail}}";
            } else {
              this.activityLog.body = "{{Invoice.UserEmail}}";
            }
          }
          else if (id == 23) {
            if (this.activityLog.body != null) {
              this.activityLog.body = this.activityLog.body + " " + "{{Invoice.Owning}}";
            } else {
              this.activityLog.body = "{{Invoice.Owning}}";
            }
          }
          else if (id == 24) {
            if (this.activityLog.body != null) {
              this.activityLog.body = this.activityLog.body + " " + "{{Organization.orgName}}";
            } else {
              this.activityLog.body = "{{Organization.orgName}}";
            }
          }
          else if (id == 25) {
            if (this.activityLog.body != null) {
              this.activityLog.body = this.activityLog.body + " " + "{{Organization.orgEmail}}";
            } else {
              this.activityLog.body = "{{Organization.orgEmail}}";
            }
          }
          else if (id == 26) {
            if (this.activityLog.body != null) {
              this.activityLog.body = this.activityLog.body + " " + "{{Organization.orgMobile}}";
            } else {
              this.activityLog.body = "{{Organization.orgMobile}}";
            }
          }
          else if (id == 27) {
            if (this.activityLog.body != null) {
              this.activityLog.body = this.activityLog.body + " " + "{{Organization.orglogo}}";
            } else {
              this.activityLog.body = "{{Organization.orglogo}}";
            }
          }
          else if (id == 28) {
            if (this.activityLog.body != null) {
              this.activityLog.body = this.activityLog.body + " " + "{{Review.link}}";
            } else {
              this.activityLog.body = "{{Review.link}}";
            }
          }
    }



    saveDesign() {

        if (this.activityLog.emailTemplateId == 0) {
            this.saving = true;
            const emailHTML = this.activityLog.body;
            // if (emailHTML != "") {
            //     this.emailEditor.editor.loadDesign(JSON.parse(emailHTML));
            //     this.emailEditor.editor.exportHtml((data) =>
            this.setHTML(emailHTML)
            // );
        }
        // let htmlTemplate = this.getEmailTemplate(emailHTML);
        // this.activityLog.body = htmlTemplate;
        // this.save();
        // }
        else {
            this.saving = true;
            this.emailEditor.editor.exportHtml((data) =>
                this.setHTML(data.html)
            );
        }
    }

    save(): void {
        debugger;
        if (this.activityType == 8) {
            if (this.myNameElem.nativeElement.value != '') {
                let reminderdate = moment(this.myNameElem.nativeElement.value, "DD/MM/YYYY");
                
                let date = moment(this.CurrentDate, "DD/MM/YYYY");
                // let date = moment().format("dd/MM/yyyy, hh:mm");
                if (reminderdate > date) {
                    this.activityLog.activityDate = reminderdate;
                    this.activityLog.body = this.myNameElem.nativeElement.value.toString();
                } else {
                    this.notify.warn(this.l('Please Select Date Greater Date than Current Date'));
                    return;
                }

            }
            else {
                this.notify.warn(this.l('Please Select Date'));
                return;
            }
        }
        if (this.activityType == 25) {
            this.activityLog.todopriorityId = this.priorityType;
            if (this.priorityType == 1) { this.activityLog.todopriority = "Low"; }
            if (this.priorityType == 2) { this.activityLog.todopriority = "Heigh"; }
            if (this.priorityType == 3) { this.activityLog.todopriority = "Medium"; }
            if (this.activityLog.referanceId != 0) {
                this.activityLog.userId = this.activityLog.referanceId;
            }
            else {
                this.notify.warn(this.l('Please Select User'));
                return;
            }
        }
        this.activityLog.leadId = this.item.lead.id;
        this.activityLog.actionId = this.activityType;
        this.activityLog.actionNote = this.actionNote;
        this.activityLog.serviceId = this.serviceId;

        this.saving = true;

        let log = new UserActivityLogDto();
                    log.actionId = this.activityType;
                    log.actionNote = this.actionNote;
                    log.section = this.sectionName;
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
        if (this.pageid == 1) {
            this._leadsServiceProxy.addActivityLogWithNotification(this.activityLog)
                .pipe(finalize(() => { this.saving = false; }))
                .subscribe(() => {
                    

                    this.modal.hide();
                    this.notify.info(this.l('NotificationSentSuccessfully'));
                    //this.viewLeadDetail.reloadLead.emit(false);
                    this.modalSave.emit(false);
                    this.activityType = 9;
                    this.activityName = "";
                    this.actionNote = "";
                    this.activityLog.body = "";
                    this.activityLog.activityNote = "";
                });

        }
        else {
            if (this.activityType == 6 && this.role != 'Admin') {
                if (this.total > 320) {
                    this.notify.warn(this.l('You Can Not Add more than 320 characters'));
                    this.saving = false;
                } else {
                    this._leadsServiceProxy.addActivityLog(this.activityLog)
                        .pipe(finalize(() => { this.saving = false; }))
                        .subscribe(() => {
                            this.modal.hide();
                            this.notify.info(this.l('SavedSuccessfully'));
                            //this.viewLeadDetail.reloadLead.emit(false);
                            this.modalSave.emit(false);
                            this.activityType = 6;
                            this.activityName = "";
                            this.actionNote = "";
                            this.activityLog.body = "";
                            this.activityLog.activityNote = "";
                        });
                }
            } else if (this.activityType == 6 && this.role != 'Admin') {
                this._leadsServiceProxy.addActivityLog(this.activityLog)
                    .pipe(finalize(() => { this.saving = false; }))
                    .subscribe(() => {
                        this.modal.hide();
                        this.notify.info(this.l('SavedSuccessfully'));
                        this.modalSave.emit(false);
                        //this.viewLeadDetail.reloadLead.emit(false);
                        this.activityType = 6;
                        this.activityName = "";
                        this.actionNote = "";
                        this.activityLog.body = "";
                        this.activityLog.activityNote = "";
                    });
            }
            else if (this.activityType == 26) {
                this._leadsServiceProxy.addActivityLog(this.activityLog)
                    .pipe(finalize(() => { this.saving = false; }))
                    .subscribe(() => {
                        this.modal.hide();
                        this.notify.info(this.l('SavedSuccessfully'));
                        this.modalSave.emit(false);
                        //this.viewLeadDetail.reloadLead.emit(false);
                        this.activityType = 8;
                        this.activityName = "";
                        this.actionNote = "";
                        this.activityLog.body = "";
                        this.activityLog.activityNote = "";
                    });
            }
            else {
                this._leadsServiceProxy.addActivityLog(this.activityLog)
                    .pipe(finalize(() => { this.saving = false; }))
                    .subscribe(() => {
                        this.modal.hide();
                        this.notify.info(this.l('SavedSuccessfully'));
                        this.modalSave.emit(false);
                        //this.viewLeadDetail.reloadLead.emit(false);
                        this.activityType = 8;
                        this.activityName = "";
                        this.actionNote = "";
                        this.activityLog.body = "";
                        this.activityLog.activityNote = "";
                    });
            }
        }
    }

    countCharcters(): void {

        if (this.role != 'Admin') {
            this.total = this.activityLog.body.length;
            this.credit = Math.ceil(this.total / 160);
            if (this.total > 320) {
                this.notify.warn(this.l('You Can Not Add more than 320 characters'));
            }
        }
        else {
            this.total = this.activityLog.body.length;
            this.credit = Math.ceil(this.total / 160);
        }


    }


    close(): void {
        this.modal.hide();
    }

    setHTML(emailHTML) {

        let htmlTemplate = this.getEmailTemplate(emailHTML);
        this.activityLog.body = htmlTemplate;
        this.save();
    }

    getEmailTemplate(emailHTML) {

        //let myTemplateStr = "Hello {{user.name}}, you are {{user.age.years}} years old";
        let myTemplateStr = emailHTML;
        let addressValue = this.item.lead.address + " " + this.item.lead.suburb + ", " + this.item.lead.state + "-" + this.item.lead.postCode;
        let myVariables = {
            Customer: {
                Name: this.item.lead.companyName,
                Address: addressValue,
                Mobile: this.item.lead.mobile,
                Email: this.item.lead.email,
                Phone: this.item.lead.phone,
                SalesRep: this.item.currentAssignUserName
            },
            Sales: {
                Name: this.item.currentAssignUserName,
                Mobile: this.item.currentAssignUserMobile,
                Email: this.item.currentAssignUserEmail
            },
            Quote: {
                ProjectNo: this.item.jobNumber,
                SystemCapacity: this.item.systemCapacity != null ? this.item.systemCapacity + " " + 'KW' : this.item.systemCapacity,
                AllproductItem: this.item.qunityAndModelList,
                TotalQuoteprice: this.item.totalQuotaion,
                InstallationDate: this.item.installationDate,
                InstallerName: this.item.installerName,
            },
            Freebies: {
                DispatchedDate: this.item.dispatchedDate,
                TrackingNo: this.item.trackingNo,
                TransportCompany: this.item.transportCompanyName,
                TransportLink: this.item.transportLink,
                PromoType: this.item.freebiesPromoType,
            },
            Invoice: {
                UserName: this.item.userName,
                UserMobile: this.item.userPhone,
                UserEmail: this.item.userEmail,
                Owning: this.item.owning,
            },
            Organization: {
                orgName: this.item.orgName,
                orgEmail: this.item.orgEmail,
                orgMobile: this.item.orgMobile,
                orglogo: AppConsts.docUrl + "/" + this.item.orgLogo,
            },
            Review: {
               link: this.item.reviewlink,
              },
        }

        // use custom delimiter {{ }}
        _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;

        // interpolate
        let compiled = _.template(myTemplateStr);
        let myTemplateCompiled = compiled(myVariables);
        return myTemplateCompiled;
    }

    editorLoaded() {

        this.activityLog.body = "";
        if ((this.activityLog.emailTemplateId != 0 && this.activityLog.emailTemplateId !== null && this.activityLog.emailTemplateId !== undefined)) {
            this._jobsServiceProxy.getEmailTemplateForEditForEmail(this.activityLog.emailTemplateId).subscribe(result => {
                this.activityLog.subject = result.emailTemplate.subject;
                this.emailData = result.emailTemplate.body;
                if (this.emailData != "") {
                    this.emailData = this.getEmailTemplate(this.emailData);
                    this.emailEditor.editor.loadDesign(JSON.parse(this.emailData));
                    // this.emailEditor.editor.exportHtml((data) =>
                    //     this.setHTML(data.html)
                    // );
                }
            });
        }
    }


    ////// Sms ////
    smseditorLoaded() {

        this.activityLog.body = "";
        if ((this.activityLog.smsTemplateId != 0 && this.activityLog.smsTemplateId !== null && this.activityLog.smsTemplateId !== undefined)) {
            this._jobsServiceProxy.getSmsTemplateForEditForSms(this.activityLog.smsTemplateId).subscribe(result => {
                this.smsData = result.smsTemplate.text;
                if (this.smsData != "") {
                    this.setsmsHTML(this.smsData)
                }
            });
        }
    }


    setsmsHTML(smsHTML) {
        let htmlTemplate = this.getsmsTemplate(smsHTML);
        this.activityLog.body = htmlTemplate;
        this.countCharcters();
    }

    getsmsTemplate(smsHTML) {
        let myTemplateStr = smsHTML;
        let addressValue = this.item.lead.address + " " + this.item.lead.suburb + ", " + this.item.lead.state + "-" + this.item.lead.postCode;
        let myVariables = {
            Customer: {
                Name: this.item.lead.companyName,
                Address: addressValue,
                Mobile: this.item.lead.mobile,
                Email: this.item.lead.email,
                Phone: this.item.lead.phone,
                SalesRep: this.item.currentAssignUserName
            },
            Sales: {
                Name: this.item.currentAssignUserName,
                Mobile: this.item.currentAssignUserMobile,
                Email: this.item.currentAssignUserEmail
            },
            Quote: {
                ProjectNo: this.item.jobNumber,
                SystemCapacity: this.item.systemCapacity != null ? this.item.systemCapacity + " " + 'KW' : this.item.systemCapacity,
                AllproductItem: this.item.qunityAndModelList,
                TotalQuoteprice: this.item.totalQuotaion,
                InstallationDate: this.item.installationDate,
                InstallerName: this.item.installerName,
            },
            Freebies: {
                DispatchedDate: this.item.dispatchedDate,
                TrackingNo: this.item.trackingNo,
                TransportCompany: this.item.transportCompanyName,
                TransportLink: this.item.transportLink,
                PromoType: this.item.freebiesPromoType,
            },
            Invoice: {
                UserName: this.item.userName,
                UserMobile: this.item.userPhone,
                UserEmail: this.item.userEmail,
                Owning: this.item.owning,
            },
            Organization: {
                orgName: this.item.orgName,
                orgEmail: this.item.orgEmail,
                orgMobile: this.item.orgMobile,
                orglogo: AppConsts.docUrl + "/" + this.item.orgLogo,
            },
            Review: {
                link: this.item.reviewlink,
               },
        }

        // use custom delimiter {{ }}
        _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;

        // interpolate
        let compiled = _.template(myTemplateStr);
        let myTemplateCompiled = compiled(myVariables);
        return myTemplateCompiled;
    }
    // upload completed event
    onUpload(event): void {
        for (const file of event.files) {
            this.uploadedFiles.push(file);
        }
    }

    onBeforeSend(event): void {
        event.xhr.setRequestHeader('Authorization', 'Bearer ' + abp.auth.getToken());
    }
}