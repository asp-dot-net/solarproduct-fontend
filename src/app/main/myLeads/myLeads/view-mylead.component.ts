﻿import { Component, ViewChild, Injector, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { LeadsServiceProxy, GetLeadForViewDto, LeadDto, GetActivityLogViewDto, GetLeadForChangeStatusOutput, OrganizationUnitServiceProxy, OrganizationUnitMapDto, GetCallHistoryViewDto, CallHistoryServiceProxy, CommonLookupServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
// import { AddActivityModalComponent } from './add-activity-model.component';
import * as moment from 'moment';
import { JobsComponent } from '@app/main/jobs/jobs/jobs.component';
import { AgmCoreModule } from '@agm/core';
import PlaceResult = google.maps.places.PlaceResult;
import { Appearance, GermanAddress, Location } from '@angular-material-extensions/google-maps-autocomplete';
import { LazyLoadEvent } from 'primeng/public_api';
import { ActivityLogMyLeadComponent } from './activity-log-mylead.component';
import { CreateEditLeadComponent } from '@app/main/leads/leads/create-edit-lead.component';
import { PromotionStopResponceComponent } from '@app/main/promotions/promotionUsers/stop-responce.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { DatePipe } from '@angular/common';
import { finalize } from 'rxjs/operators';
import { stringToKeyValue } from '@angular/flex-layout/extended/typings/style/style-transforms';

@Component({
    selector: 'viewLeadDetail',
    templateUrl: './view-mylead.component.html',
    styleUrls: ['./myleads.component.less'],
    animations: [appModuleAnimation()]
})
export class ViewMyLeadComponent extends AppComponentBase implements OnInit {
    @ViewChild('detailActivityModal', { static: true }) modal: ModalDirective;
    // @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    @ViewChild('jobCreateOrEdit', { static: true }) jobCreateOrEdit: JobsComponent;
    @ViewChild('createEditLead', { static: true }) createEditLead: CreateEditLeadComponent;
    @ViewChild('activityLog', { static: true }) activityLogLead: ActivityLogMyLeadComponent;
    @ViewChild('promoStopRespmodel', { static: true }) promot: PromotionStopResponceComponent;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    @Input() SelectedLeadId: number = 0;
    @Output() reloadLead = new EventEmitter<boolean>();
    active = false;
    saving = false;
    item: GetLeadForViewDto;
    activityLog: GetActivityLogViewDto[];
    activeTabIndex: number = 0;
    activeTabIndexForPromotion: number = 0;
    activityDatabyDate: any[] = [];
    actionId: number = 0;
    activity: boolean = false;
    leadActivityList: GetActivityLogViewDto[];
    lat: number;
    lng: number;
    zoom: 20;
    showforresult = false;
    name: string;
    role: string = '';
    mainsearch = false;
    sectionId = 0;
    showsectionwise = false;
    ExpandedView: boolean = true;
    OrganizationMapDto: OrganizationUnitMapDto[];
    mapSrc: string = "";
   
    selectedMap: string;
    serviceId : number = 0;
    showTab = true;
    SectionName = '';
    date = new Date();
    leadId: number = 0;
  projectOpenDate  =null;
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _router: Router,
        private spinner: NgxSpinnerService,
        private _organizationUnitServiceProxy : OrganizationUnitServiceProxy,
        public datepipe: DatePipe,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _callHistoryService: CallHistoryServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy
    ) {
        super(injector);
        this.item = new GetLeadForViewDto();

        this.item.lead = new LeadDto();
    }


    // lt = 22.3003;
    // long = 73.2051;

    // markers = [
    //     {
    //         lt: 21.1594627,
    //         long: 72.6822083,
    //         label: 'Surat'
    //     },
    //     {
    //         lt: 23.0204978,
    //         long: 72.4396548,
    //         label: 'Ahmedabad'
    //     },
    //     {
    //         lt: 22.2736308,
    //         long: 70.7512555,
    //         label: 'Rajkot'
    //     }
    // ];


    ngOnInit(): void {
        this.showTab = true;
        this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
            this.role = result;
        });

        if (this.SelectedLeadId > 0)
            this.showDetail(this.SelectedLeadId);
        this.registerToEvents();
        
    }

    getParsedDate(strDate) {//get date formate
        if (strDate == "" || strDate == null || strDate == undefined) {
            return;
        }
        let month_names = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

        var strSplitDate = String(strDate._i).split('T');

        var dateArray = strSplitDate[0].split('-');
        let monthint = parseInt(dateArray[1]);
        let date = month_names[monthint - 1] + " " + dateArray[2];
        return date;

    }

    showDetail(leadId: number, LeadName?: string, sectionId?: number, service : number = 0,section?: string): void {
        this.SectionName = section;
        this.showTab = true;
       

        this.serviceId = service;
        if (sectionId == 15) {
            // this.activeTabIndexForPromotion = 0;
            this.activeTabIndex = 1;
        }
        else if (sectionId == 28 || sectionId == 22) 
        {
            this.activeTabIndex = 1;
        } else {
            this.activeTabIndex = 0;
            // this.activeTabIndexForPromotion = 1;
        }

        this.sectionId = sectionId;
        if (this.sectionId == 0 || this.sectionId > 12) {
            this.showsectionwise = true;
        }

        if (sectionId == 30) {
            this.mainsearch = true;
            this.showsectionwise = false;
        }


        this.name = LeadName;
        let that = this;
        if (leadId != null) {
            this.spinner.show();
            this._leadsServiceProxy.getLeadForView(leadId, 0).subscribe(result => {
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open Detail By Click on ' + result.lead.companyName;
                log.section = this.SectionName;
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
                this.item = result;
              
                this.lng = parseFloat(result.longitude);
                this.lat = parseFloat(result.latitude);

                this._commonLookupService.getOrganizationMapById(result.lead.organizationId).subscribe((result) => {
                    this.OrganizationMapDto = result;
                });

                this.selectedMap = 'GoogleMap';

                    if(this.selectedMap != 'GoogleMap')
                    {
                        that.bindMap();
                    }
                debugger;
                if(LeadName == "3rdpartyleads"){
                    this.jobCreateOrEdit.getJobDetailByLeadId(this.item.lead, sectionId,true,section);

            
                }
                else{
                    this.jobCreateOrEdit.getJobDetailByLeadId(this.item.lead, sectionId,false,section);

            
                }
                if(sectionId != 28){
                   
                    this.showTab = true;

                }
                else{

                    this.showTab = false;
                }
                this.activityLogLead.show(this.item.lead.id, 0, sectionId, service,section);


                
                // if(this.permission.isGranted('Pages.LeadDetails.CallHistory'))
                // {
                //     this.getCallHistory();
                // }

                this.spinner.hide();

            }, err => {
                this.spinner.hide();
            }
            );
            

        }
      
    }

    // createLead(): void {
    //     debugger
    //     this._router.navigate(['/app/main/leads/leads/createOrEdit'], { queryParams: this.name});
    // }

    deleteLead(lead: LeadDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._leadsServiceProxy.deleteLeads(lead.id)
                        .subscribe(() => {
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote = 'Lead Deleted';
                            log.section = this.SectionName;
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            }); 
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            this.reloadLead.emit(true);
                        });
                }
            }
        );
    }

    myHistory: boolean = false;
    callType: any = '';
    callHistories: GetCallHistoryViewDto[];

    getCallHistory()
    {
        if(this.permission.isGranted("Pages.LeadDetails.CallHistory"))
        {
            this._callHistoryService.getCallHistory(this.item.lead.id, this.callType, this.myHistory).subscribe((result) => {
                this.callHistories = result;
            });
        }
    }

    warm(lead: LeadDto): void {
        let status: GetLeadForChangeStatusOutput = new GetLeadForChangeStatusOutput();
        status.id = lead.id;
        status.leadStatusID = 4;
        status.section = this.SectionName;
        this.message.confirm('',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._leadsServiceProxy.changeStatus(status)
                        .subscribe(() => {
                            this.notify.success(this.l('LeadStatusChangedToWarm'));
                            //this._router.navigate(['/app/main/myleads/myleads']);
                            this.modalSave.emit(null);
                            this.showDetail(lead.id);
                            this.reloadLead.emit(false);
                        });
                }
            }
        );
    }

    cold(lead: LeadDto): void {
        let status: GetLeadForChangeStatusOutput = new GetLeadForChangeStatusOutput();
        status.id = lead.id;
        status.leadStatusID = 3;
        status.section = this.SectionName;
        this.message.confirm('',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._leadsServiceProxy.changeStatus(status)
                        .subscribe(() => {
                            this.notify.success(this.l('LeadStatusChangedToCold'));
                            //this._router.navigate(['/app/main/myleads/myleads']);
                            this.modalSave.emit(null);
                            this.showDetail(lead.id);
                            this.reloadLead.emit(false);
                        });
                }
            }
        );
    }

    hot(lead: LeadDto): void {
        let status: GetLeadForChangeStatusOutput = new GetLeadForChangeStatusOutput();
        status.id = lead.id;
        status.leadStatusID = 5;
        status.section = this.SectionName;
        this.message.confirm('',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._leadsServiceProxy.changeStatus(status)
                        .subscribe(() => {
                            this.notify.success(this.l('LeadStatusChangedToHot'));
                            //this._router.navigate(['/app/main/myleads/myleads']);
                            this.modalSave.emit(null);
                            this.showDetail(lead.id);
                            this.reloadLead.emit(false);
                        });
                }
            }
        );
    }

    cancel(lead: LeadDto): void {

        abp.event.trigger('app.show.cancelLeadModal', lead.id);
        // let status :GetLeadForChangeStatusOutput = new GetLeadForChangeStatusOutput();
        // status.id = lead.id;
        // status.leadStatusID = 8;
        // this.message.confirm('',
        //     this.l('AreYouSure'),
        //     (isConfirmed) => {
        //         if (isConfirmed) {
        //             this._leadsServiceProxy.changeStatus(status)
        //                 .subscribe(() => {
        //                     this.notify.success(this.l('LeadStatusChangedToCancel'));
        //                     this._router.navigate(['/app/main/myleads/myleads']);
        //                 });
        //         }
        //     }
        // );
    }

    reject(id): void {
        abp.event.trigger('app.show.rejectLeadModal', id);
    }
    close(): void {
        this.modal.hide();
    }
    navigateToProjectOpen(leadId,sectionId): void {
        this.leadId = leadId;
        this.projectOpenDate =new Date();
        this.modal.show();
        this.sectionId=sectionId;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Open Project Open Date' ;
        log.section = this.SectionName;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }
    SaveProjectOpenDate(){
        this._leadsServiceProxy.updateProjectOpenDate(this.leadId,this.projectOpenDate,this.sectionId)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.modal.hide();
                this.modalSave.emit(null);
                let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Project Open Date Updated ';
                    log.section = this.SectionName;
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    });
            })

    }
    registerToEvents() {

        abp.event.on('app.onCancelModalSaved', () => {
            debugger;
            this.showDetail(this.SelectedLeadId);
        });
    }

    addActivitySuccess() {

        this.showDetail(this.SelectedLeadId);
    }

    changeActivity(lead: LeadDto) {
        let that = this;

        this._leadsServiceProxy.getLeadActivityLog(lead.id, this.actionId, this.sectionId, false, false, 0, false, this.serviceId).subscribe(result => {
            if (result.length > 0) {
                this.notify.success(this.l('ActivityFiltered'));
            }
            else {
                this.notify.success(this.l('NoActivityFound'));
            }
            let lastdatetime !: moment.Moment;
            this.activityLog = result;
            this.leadActivityList = result;
            let obj = {
                activityDate: "",
                DatewiseActivity: []
            }
            this.activityDatabyDate = [];
            this.activityLog.forEach(function (log) {
                if (that.getParsedDate(log.creationTime) == that.getParsedDate(lastdatetime)) {
                    log.logDate = "";
                    let item1 = this.activityDatabyDate.find(activityDate => activityDate.activityDate === that.getParsedDate(lastdatetime));
                    item1.DatewiseActivity.push({ actionNote: log.actionNote });
                }
                else {
                    log.logDate = that.getParsedDate(log.creationTime);
                    obj.activityDate = log.logDate;
                    obj.DatewiseActivity.push(log.actionNote);
                    this.activityDatabyDate.push({
                        activityDate: log.logDate,
                        DatewiseActivity: [{ actionNote: log.actionNote }]
                    });
                }
                lastdatetime = log.creationTime;
            }.bind(that));

        });
    }

    myActivity(lead: LeadDto) {
        let that = this;

        this._leadsServiceProxy.getLeadActivityLog(lead.id, this.actionId, this.sectionId, false, this.activity, 0, false, this.serviceId).subscribe(result => {
            if (result.length > 0) {
                this.notify.success(this.l('ActivityFiltered'));
            }
            else {
                this.notify.success(this.l('NoActivityFound'));
            }
            let lastdatetime !: moment.Moment;
            this.activityLog = result;
            this.leadActivityList = result;
            let obj = {
                activityDate: "",
                DatewiseActivity: []
            }
            this.activityDatabyDate = [];
            this.activityLog.forEach(function (log) {
                if (that.getParsedDate(log.creationTime) == that.getParsedDate(lastdatetime)) {
                    log.logDate = "";
                    let item1 = this.activityDatabyDate.find(activityDate => activityDate.activityDate === that.getParsedDate(lastdatetime));
                    item1.DatewiseActivity.push({ actionNote: log.actionNote });
                }
                else {
                    log.logDate = that.getParsedDate(log.creationTime);
                    obj.activityDate = log.logDate;
                    obj.DatewiseActivity.push(log.actionNote);
                    this.activityDatabyDate.push({
                        activityDate: log.logDate,
                        DatewiseActivity: [{ actionNote: log.actionNote }]
                    });
                }
                lastdatetime = log.creationTime;
            }.bind(that));

        });
    }

    bindMap() {
        // console.log(this.OrganizationMapDto);
        
        if(this.selectedMap != "GoogleMap")
        {
            let orgMap = this.OrganizationMapDto.find(x => x.mapProvider == this.selectedMap);

            let Src = "";
            if(orgMap != null && orgMap != undefined)
            {
                if(orgMap.mapProvider == "GoogleMap") {
                    //this.selectedMap
                    // Src = "https://maps.googleapis.com/maps/api/staticmap?center="+ this.lat + "," + this.lng +"&zoom=19&scale=2&size=800x300&maptype=satellite&key="+ orgMap.mapApiKey +"&markers=size:tiny|color:red|label:|" + this.lat + "," + this.lng;
                }
                else if (orgMap.mapProvider == "NearMap") {
                    let date = new Date();
                    let latest_date =this.datepipe.transform(date, 'yyyyMMdd');
    
                    console.log(latest_date);
    
                    Src = "https://au0.nearmap.com/staticmap?center="+ this.lat + "," + this.lng +"&zoom=20&size=600x1270&maptype=satellite&httpauth=false&date="+ latest_date +"&apikey="+ orgMap.mapApiKey;
                
                    //https://au0.nearmap.com/staticmap?center=-27.238610,153.039820&size=800x800&zoom=20&date=20200205&httpauth=false&apikey=MzA4YzMwYjItZTg3Yy00Yzg3LTg3ZWItYWRkZTA4NDI4ZDky
                }
                else if (orgMap.mapProvider == "MetroMap") {
                    Src = "api.metromap.com.au/metromapkey/getlayers?lng="+ this.lng  + "&lat=" + this.lat +"&type=point&zoom=19&scale=2&size=800x300&maptype=satellite&key="+ orgMap.mapApiKey +"&markers=size:tiny|color:red|label:|" + this.lat + "," + this.lng;
                
                    //https://api.metromap.com.au/metromapkey/getlayers?key={api-key}&lng={lng}&lat={lat}&type=point
                }
                else if (orgMap.mapProvider == "Pylon") {
                    Src = this.item.pylonUrl;
                }
                else if (orgMap.mapProvider == "BingMap") {
                    Src = "https://dev.virtualearth.net/REST/v1/Imagery/Map/AerialWithLabels/"+ this.lat + "," + this.lng +"/20?mapSize=1270,600&pp="+ this.lat + "," + this.lng +";66&key="+ orgMap.mapApiKey;
                }
            }
    
            this.mapSrc = Src;
        }
    }
}
