﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit, ElementRef, Directive, Optional } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {
    LeadsServiceProxy, LeadtrackerHistoryDto,
    UserActivityLogDto,
    UserActivityLogServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AgmCoreModule } from '@agm/core';
import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';
import PlaceResult = google.maps.places.PlaceResult;
import { Appearance, GermanAddress, Location } from '@angular-material-extensions/google-maps-autocomplete';

@Component({
    selector: 'activityloghistory',
    templateUrl: './activity-log-history.component.html',
})
export class ActivityLogHistoryComponent extends AppComponentBase {
    @ViewChild("myNameElem") myNameElem: ElementRef;
    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @ViewChild('SampleDatePicker', { static: true }) sampleDatePicker: ElementRef;
    hidePreviousValue: boolean = false;

    leadtrackerHistory: LeadtrackerHistoryDto[];
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _router: Router,
        private _el: ElementRef

    ) {
        super(injector);
        this._el.nativeElement.setAttribute('autocomplete', 'off');
        this._el.nativeElement.setAttribute('autocorrect', 'off');
        this._el.nativeElement.setAttribute('autocapitalize', 'none');
        this._el.nativeElement.setAttribute('spellcheck', 'false');
    }

    show(leadId?: number, leadorjob?: number,section = ''): void {
        let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open Activity Log History';
                log.section = section;
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
        if (leadorjob == 1) {
            this._leadsServiceProxy.getLeadActivityLogHistory(leadId, 2, 0, false, false, 0, false,0).subscribe(result => {
                this.leadtrackerHistory = result;
                this.hidePreviousValue = false;
                this.modal.show();
            });
        }
        if (leadorjob == 2) {
            this._leadsServiceProxy.getJobActivityLogHistory(leadId, 2, 0, false, false, 0, false,0).subscribe(result => {
                this.leadtrackerHistory = result;
                this.hidePreviousValue = false;
                this.modal.show();
            });
        }
        if (leadorjob == 3) {
           
            this._leadsServiceProxy.getLeadActivityLogHistory(leadId, 2, 0, false, false, 0, false,0).subscribe(result => {
                this.leadtrackerHistory = result;
                this.hidePreviousValue = true;
                this.modal.show();
            });
        }
        if (leadorjob == 4) {
            this._leadsServiceProxy.getJobActivityLogHistory(leadId, 2, 0, false, false, 0, false,0).subscribe(result => {
                this.leadtrackerHistory = result;
                this.hidePreviousValue = true;
                this.modal.show();
            });
        }
    }
    close(): void {

        this.modal.hide();
    }
}
