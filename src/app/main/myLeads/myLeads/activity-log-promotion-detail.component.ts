import { Component, EventEmitter, Injector, Input, OnInit, Output, ViewChild } from '@angular/core';
import {
    GetActivityLogViewDto, GetLeadForViewDto,  LeadDto, CommonLookupDto, LeadsServiceProxy, LeadtrackerHistoryDto,   GetPromotionHistorybyPromotionIdOutPut, PromotionsServiceProxy,
    UserActivityLogDto,
    UserActivityLogServiceProxy
} from '@shared/service-proxies/service-proxies';

import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { NgxSpinnerService } from 'ngx-spinner';
import { ModalDirective } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { DomSanitizer } from '@angular/platform-browser';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'activityLogPromotionDetail',
    templateUrl: './activity-log-promotion-detail.component.html',
    animations: [appModuleAnimation()]
})
export class ActivityLogPromotionDetailComponent extends AppComponentBase implements OnInit {

    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    
    @Output() reloadLead = new EventEmitter();
    active = false;
    saving = false;
    item: GetLeadForViewDto;
    activeTabIndex: number = 0;
    activityDatabyDate: any[] = [];
    actionId: number = 0;
    activity: boolean = false;
    leadActivityList: GetActivityLogViewDto[];
    leadActionList: CommonLookupDto[];
    leadId: number = 0;
    id: number = 0;
    role: string = '';
    sectionId: number = 0;
    showsectionwise = false;
    currentactivity: boolean = false;
    allActivity: boolean = false;
    serviceId : number =0 ;
    promotionHistory : GetPromotionHistorybyPromotionIdOutPut;
    leadtrackerHistory: LeadtrackerHistoryDto[];
    type = "";
    viewType = "";
    responseMessage = "";
    constructor(
        injector: Injector,        
        private sanitizer: DomSanitizer,
        private _promotionsServiceProxy: PromotionsServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _router: Router,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private spinner: NgxSpinnerService,
    ) {
        super(injector);
        this.item = new GetLeadForViewDto();
        this.item.lead = new LeadDto();
        this.promotionHistory = new GetPromotionHistorybyPromotionIdOutPut();

    }


    ngOnInit(): void {
        //this.show(this._activatedRoute.snapshot.queryParams['id']);
        // this._wholeSaleleadsServiceProxy.getAllLeadAction().subscribe(result => {
        //     this.leadActionList = result;
        // });

        this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
            this.role = result;
            if (this.role == 'Sales Rep') {
                this.activity = true;
            }
        });
    }

    show(promotionId : number, responce? : string, type : string = 'promotion',section = ''): void {
        debugger;
        let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open Activity Log Promotion History';
                log.section = section;
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
        // this.sectionId = sectionId;
        this.activity = false;
        this.allActivity = false;
        this.responseMessage = responce;
        // this.serviceId = service;
        this.viewType = type;

        if(type == 'activityEmail'){
            this.promotionHistory.description = responce;
        }
        else{
            
        this._promotionsServiceProxy.getPromotionHistorybyPromotionId(promotionId).subscribe(result => {
            this.promotionHistory = result;
            this.type = result.promotionSendingId == 1 ? "SMS" : "Email";
            if(!result.id || result.id == 0){
                this._promotionsServiceProxy.getPromotionForView(promotionId).subscribe(r => {
                    this.promotionHistory.description = r.promotion.description;
                    this.type = r.promotionTypeName;
                    this.promotionHistory.charges =r.promotion.promoCharge;
                    this.promotionHistory.title = r.promotion.title;
                })
            }
           
        });
        }
        this.setHtml(this.promotionHistory.description);
        this.modal.show();
    }

    close(): void {
        this.modal.hide();
    }

    htmlContentBody: any;

    setHtml(emailHTML) {
        let compiled = _.template(emailHTML);
        let myTemplateCompiled = compiled();
        this.htmlContentBody = myTemplateCompiled;
        this.htmlContentBody = this.sanitizer.bypassSecurityTrustHtml(this.htmlContentBody);

        // if(htmlTemplate != '')
        // {
        //     this.spinner.hide();
        //     this.modal.show();
        // }
    }

   

    
    navigateToLeadHistory(leadid): void {

        // this.wholesaleactivityloghistory.show(leadid);
    }

    

    
}


