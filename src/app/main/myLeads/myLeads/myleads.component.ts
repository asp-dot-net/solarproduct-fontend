﻿import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LeadsServiceProxy, LeadDto, LeadStatusLookupTableDto, OrganizationUnitDto, UserServiceProxy, LeadStateLookupTableDto, JobStatusTableDto, LeadSourceLookupTableDto, CommonLookupServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { AppConsts } from '@shared/AppConsts';
import { HttpClient } from '@angular/common/http';
import { finalize } from 'rxjs/operators';
import { FileUpload } from 'primeng/fileupload';
import { ViewMyLeadComponent } from './view-mylead.component';
import { OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AddActivityModalComponent } from './add-activity-model.component';
import { ReminderModalComponent } from '@app/main/activitylog/reminder-modal.component';

@Component({
    templateUrl: './myleads.component.html',
    styleUrls: ['./myleads.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
})
export class MyLeadsComponent extends AppComponentBase implements OnInit {

    show: boolean = true;
    FiltersData = false;
    showchild: boolean = true;
    shouldShow: boolean = false;
    flag = true;
    organizationUnitlength: number = 0;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };
    excelorcsvfile = 0;

    @ViewChild('ExcelFileUpload', { static: false }) excelFileUpload: FileUpload;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
    //@ViewChild('ViewMyLeadComponent') viewleaddetail:ViewMyLeadComponent;
    @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    @ViewChild('ReminderModal', { static: true }) ReminderModal: ReminderModalComponent;

    uploadUrl: string;
    advancedFiltersAreShown = false;
    filterText = '';
    filterName = 'CompanyName';
    copanyNameFilter = '';
    emailFilter = '';
    phoneFilter = '';
    mobileFilter = '';
    addressFilter = '';
    requirementsFilter = '';
    postCodeSuburbFilter = '';
    stateNameFilter = '';
    streetNameFilter = '';
    postCodePostalCode2Filter = '';
    leadSourceNameFilter = '';
    //leadSubSourceNameFilter = '';
    leadStatusName = '';
    typeNameFilter = '';
    areaNameFilter = '';
    leadStatus: any;
    allLeadStatus: LeadStatusLookupTableDto[];
    alljobstatus: JobStatusTableDto[];
    leadStatusId: number = 0;
    jobStatusIDFilter = 0;
    allLeadSources: LeadSourceLookupTableDto[];
    
    //StartDate: moment().startOf('day');
    // EndDate: moment.Moment;
    //StartDate = moment().add(0, 'days').endOf('day');
    //EndDate = moment().add(0, 'days').endOf('day');

    date = new Date();
    
    // filterText = 'AS821831';
    // firstDay = new Date(this.date.getFullYear() - 5, this.date.getMonth(), 1);
    
    //firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    //StartDate = moment(this.firstDay);
    //EndDate = moment().endOf('day');

    //firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    StartDate: moment.Moment = moment(this.date);
    EndDate: moment.Moment = moment(this.date);
    
    PageNo: number;
    ExpandedView: boolean = true;
    SelectedLeadId: number = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnit = 0;
    leadSourceIdFilter = [];
    allStates: LeadStateLookupTableDto[];
    suburbSuggestions: string[];
    dateFilterType = 'Creation';
    changeView: boolean = true;
    role: string = '';
    projectNumberFilter = '';
    leadName: string;
    totalLeads = 0;
    new = 0;
    unHandledLeads = 0;
    upgrade = 0;
    canceled = 0;
    cold = 0;
    warm = 0;
    hot = 0;
    closed = 0;
    assigned = 0;
    reAssigned = 0;
    sold = 0;
    rejected = 0;
    jobStatusID = [];
    upgrateshow = false;
    frozenValue: [];
    frozenCols: any[];
   
    leadstatuss = [
        {value: 2, name: 'UnHandled'},
        {value: 3, name: 'Cold'},
        {value: 4, name: 'Warm'},
        {value: 5, name: 'Hot'},
        {value: 6, name: 'Upgrade'},
        {value: 10, name: 'Assigned'},
        {value: 11, name: 'ReAssigned'},
    ];

    leadStatusIDS = [];
    firstrowcount = 0;
    last = 0;

    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 330;
    toggle: boolean = true;
    toggle1: boolean = true;
    orgCode = 'AS';
    change() {
        this.toggle = !this.toggle;
    }

    changeFilter() {
        this.toggle1 = !this.toggle1;
    }
      
    constructor(
        injector: Injector,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _notifyService: NotifyService,
        private _commonLookupService: CommonLookupServiceProxy,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _httpClient: HttpClient,
        private _userServiceProxy: UserServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _router: Router,
        private titleService: Title
        ) {
            super(injector);
            this.titleService.setTitle(this.appSession.tenancyName + " |  My Leads");
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/Users/ImportLeadFromExcel';
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });

        // this._commonLookupService.getAllLeadStatusForTableDropdown("").subscribe(result => {
        //     debugger;
        //     this.leadstatuss = result;
        // });

        // this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
        //     this.role = result;
        //     if (this.role == 'Sales Rep') {
        //         this.leadStatusId = 10;
        //     }
        // });

        // this._commonLookupService.getAllLeadSourceDropdown().subscribe(result => {
        //     this.allLeadSources = result;
        // });
        this.frozenCols = [];

        this._commonLookupService.getAllJobStatusTableDropdown().subscribe(result => {
            this.alljobstatus = result;
        });

        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open My Lead';
            log.section = 'My Lead';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.orgCode = this.allOrganizationUnits[0].code;

            this.bindLeadSource(this.organizationUnit)
            this.getLeads();
        });
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }

        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }

    bindLeadSource(id) {
        this._commonLookupService.getAllLeadSourceForTableDropdownOnlyOrg(id).subscribe(result => {
            this.allLeadSources = result;
        });
    }

    expandGrid() {
        this.ExpandedView = true;
    }
    clear() {
        this.filterText = '';
        this.addressFilter = '';
        this.postCodeSuburbFilter = '';
        this.stateNameFilter = '';
        this.postCodePostalCode2Filter = '';
        this.leadSourceNameFilter = '';
        this.typeNameFilter = '';
        this.jobStatusIDFilter = 0;
        this.areaNameFilter = '';
        this.dateFilterType = 'Creation';
        this.StartDate = moment(this.date);
        this.EndDate = moment(this.date);
        this.jobStatusID = [];
        this.getLeads();
    }
    changeOrgCode() {
        this.orgCode = this.allOrganizationUnits.find(x => x.id == this.organizationUnit).code;
    }
    filtersuburbs(event): void {
        this._commonLookupService.getOnlySuburb(event.query).subscribe(output => {
            this.suburbSuggestions = output;
        });
    }

    leadstatuschange()
    {
        debugger;
        if (this.leadStatusIDS.includes(6)) {
            this.upgrateshow = true;
        } else {
            this.upgrateshow = false;
            this.jobStatusID = [];
        } 
    }

    getLeads(event?: LazyLoadEvent) {

        this.PageNo = this.primengTableHelper.getSkipCount(this.paginator, event);

        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this.primengTableHelper.showLoadingIndicator();
        this._leadsServiceProxy.getAllForMyLead(
            this.filterName,
            filterText_,
            this.copanyNameFilter,
            this.emailFilter,
            this.phoneFilter,
            this.mobileFilter,
            this.addressFilter,
            this.requirementsFilter,
            this.postCodeSuburbFilter,
            this.stateNameFilter,
            this.streetNameFilter,
            this.postCodePostalCode2Filter,
            this.leadSourceNameFilter,
            this.leadSourceIdFilter,
            //this.leadSubSourceNameFilter,
            this.leadStatusName,
            this.typeNameFilter,
            this.areaNameFilter,
            (this.leadStatusId == 0) ? undefined : this.leadStatusId,
            0,
            this.StartDate,
            this.EndDate,
            this.organizationUnit == 0 ? undefined : this.organizationUnit,
            undefined,
            undefined,
            undefined,
            this.dateFilterType,
            undefined,
            undefined,
            this.leadStatusIDS.includes(6) ? this.jobStatusIDFilter == 0 ? undefined : this.jobStatusIDFilter : undefined,
            undefined,
            undefined,
            this.projectNumberFilter,
            undefined,
            this.jobStatusID,
            this.leadStatusIDS,
            undefined,
            0,
            "",
            undefined,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            this.shouldShow = false;
            if(result.totalCount > 0)
            {
                this.totalLeads = parseInt(result.items[0].summaryCount.total);
                this.new = parseInt(result.items[0].summaryCount.new);
                this.unHandledLeads = parseInt(result.items[0].summaryCount.unHandled);
                this.cold = parseInt(result.items[0].summaryCount.cold);
                this.warm = parseInt(result.items[0].summaryCount.warm);
                this.hot = parseInt(result.items[0].summaryCount.hot);
                this.upgrade = parseInt(result.items[0].summaryCount.upgrade);
                this.rejected = parseInt(result.items[0].summaryCount.rejected);
                this.canceled = parseInt(result.items[0].summaryCount.cancelled);
                this.closed = parseInt(result.items[0].summaryCount.closed);
                this.assigned = parseInt(result.items[0].summaryCount.assigned);
                this.reAssigned = parseInt(result.items[0].summaryCount.reAssigned);
                this.sold = parseInt(result.items[0].summaryCount.sold);
            } else {
                this.totalLeads = 0;
                this.new = 0;
                this.unHandledLeads = 0;
                this.cold = 0;
                this.warm = 0;
                this.hot = 0;
                this.upgrade = 0;
                this.rejected = 0;
                this.canceled = 0;
                this.closed = 0;
                this.assigned = 0;
                this.reAssigned = 0;
                this.sold = 0;
            }
            
            //this.ExpandedView = true;
            if (this.flag == false && result.totalCount != 0 && this.ExpandedView == false) {
                this.navigateToLeadDetail(this.SelectedLeadId)
            }
            else {
                if (this.ExpandedView == false && result.totalCount != 0) {
                    this.navigateToLeadDetail(result.items[0].lead.id)
                }
                else {
                    this.ExpandedView = true;
                }
            }

            if (this.ExpandedView == false && result.totalCount != 0) {
                this.navigateToLeadDetail(result.items[0].lead.id)
            }
            else {
                this.ExpandedView = true;
            }
        },
        err => {
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    filterCountries(event): void {
        this._commonLookupService.getAllLeadStatusForTableDropdown(event.query).subscribe(result => {
            this.allLeadStatus = result;
        });
    }

    reloadPage(flafValue: boolean): void {
        this.ExpandedView = true;
        this.flag = flafValue;
        this.paginator.changePage(this.paginator.getPage());
        if (!flafValue) {
            this.navigateToLeadDetail(this.SelectedLeadId);
        }
    }

    createLead(): void {
        this._router.navigate(['/app/main/leads/leads/createOrEdit'], { queryParams: { OId: this.organizationUnit, from: "myleads" ,SectionId : 13, Section : 'My Lead'} });
    }

    navigateToLeadDetail(leadid): void {
        this.ExpandedView = !this.ExpandedView;
        this.ExpandedView = false;
        this.SelectedLeadId = leadid;
        this.leadName = "myleads";
        this.viewLeadDetail.showDetail(leadid, this.leadName, 13,0,'My Lead');
    }

    deleteLead(lead: LeadDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    // this._leadsServiceProxy.delete(lead.id)
                    //     .subscribe(() => {
                    //         this.reloadPage();
                    //         this.notify.success(this.l('SuccessfullyDeleted'));
                    //     });
                }
            }
        );
    }

    exportToExcel(excelorcsv): void {
        // if (this.sampleDateRange != null) {
        //     this.StartDate = this.sampleDateRange[0];
        //     this.EndDate = this.sampleDateRange[1];
        // } else {
        //     this.StartDate = this.sampleDateRange[0];
        //     this.EndDate = this.sampleDateRange[1];
        // }
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this.excelorcsvfile = excelorcsv;
        this._leadsServiceProxy.getMyLeadToExcel(
            this.filterName,
            filterText_,
            this.copanyNameFilter,
            this.emailFilter,
            this.phoneFilter,
            this.mobileFilter,
            this.addressFilter,
            this.requirementsFilter,
            this.postCodeSuburbFilter,
            this.stateNameFilter,
            this.streetNameFilter,
            this.postCodePostalCode2Filter,
            this.leadSourceNameFilter,
            undefined,
            //this.leadSubSourceNameFilter,
            this.leadStatusName,
            this.typeNameFilter,
            this.areaNameFilter,
            (this.leadStatusId == 0) ? undefined : this.leadStatusId,
            0,
            this.StartDate,
            this.EndDate,
            this.organizationUnit == 0 ? undefined : this.organizationUnit,
            undefined,
            undefined,
            undefined,
            this.dateFilterType,
            undefined,
            undefined,
            undefined,
            undefined,
            this.leadStatusId == 6 ? this.jobStatusIDFilter == 0 ? undefined : this.jobStatusIDFilter : undefined, 
            this.excelorcsvfile,
            this.jobStatusID,
            this.leadStatusIDS,
            undefined
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }
    uploadExcel(data: { files: File }): void {
        const formData: FormData = new FormData();
        const file = data.files[0];
        formData.append('file', file, file.name);

        this._httpClient
            .post<any>(this.uploadUrl, formData)
            .pipe(finalize(() => this.excelFileUpload.clear()))
            .subscribe(response => {
                if (response.success) {
                    this.notify.success(this.l('ImportLeadsProcessStart'));
                } else if (response.error != null) {
                    this.notify.error(this.l('ImportLeadUploadFailed'));
                }
            });
    }

    onUploadExcelError(): void {
        this.notify.error(this.l('ImportLeadUploadFailed'));
    }

    onReminderHide(){
        debugger;
        if(this.primengTableHelper.records.find(e => e.lead.id == this.ReminderModal.activityLog.leadId).reminderTime.value != this.ReminderModal.activityLog.activityDate){
            this.primengTableHelper.records.find(e => e.lead.id == this.ReminderModal.activityLog.leadId).isChangedFollowup = true;
            var date = moment( this.ReminderModal.activityLog.activityDate.toString()).format('DD-MM-yyyy hh:mm:ss');;
            this.primengTableHelper.records.find(e => e.lead.id == this.ReminderModal.activityLog.leadId).reminderTime = date ;
            this.primengTableHelper.records.find(e => e.lead.id == this.ReminderModal.activityLog.leadId).activityDescription = this.ReminderModal.activityLog.activityNote;
        }
        
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'My Lead';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'My Lead';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }

}