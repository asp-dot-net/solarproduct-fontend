import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { CreateOrEditVendorsDto, VendorContactDto,CreateOrEditPurchaseCompanyDto, VendorServiceProxy, LeadsServiceProxy, LeadStateLookupTableDto, CommonLookupServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import PlaceResult = google.maps.places.PlaceResult;
import { Appearance, GermanAddress, Location } from '@angular-material-extensions/google-maps-autocomplete';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';


@Component({
    selector: 'createOrEditVendorModal',
    templateUrl: './createoredit-vendor-modal.component.html'
})
export class CreateOrEditVendorModalComponent extends AppComponentBase {
   
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    vendors: CreateOrEditVendorsDto = new CreateOrEditVendorsDto();
    unitType: any;
    streetType: any;
    streetName: any;
    suburb: any;
    suburbSuggestions: string[];
    selectAddress: boolean = true;
    allStates: LeadStateLookupTableDto[];
    streetTypesSuggestions: string[];
    streetNamesSuggestions: string[];
    unitTypesSuggestions: string[];

    constructor(
        injector: Injector,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _vendorServiceProxy: VendorServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }

    onAutocompleteSelected(result: PlaceResult) {
        debugger;
        if (result.address_components.length == 7) {
            this.vendors.unitNo = "";
            this.unitType = "";
            this.vendors.unitType = "";
            this.vendors.streetNo = "";
            this.streetName = "";
            this.streetType = "";
            this.suburb = "";
            this.vendors.state = "";
            this.vendors.suburbId = 0;
            this.vendors.stateId = 0;
            this.vendors.postCode = "";
            this.vendors.address = "";

            this.vendors.streetNo = result.address_components[0].long_name.toUpperCase();
            var str = result.address_components[1].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.streetType = splitted[3].toUpperCase().trim();
            }
            else if (splitted.length == 3) {
                this.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.streetType = splitted[2].toUpperCase().trim();
            }
            else {
                this.streetName = splitted[0].toUpperCase().trim();
                this.streetType = splitted[1].toUpperCase().trim();
            }
            this.suburb = result.address_components[2].long_name.toUpperCase().trim();
            this.vendors.state = result.address_components[4].short_name;
            this._leadsServiceProxy.getSuburbId(result.address_components[6].long_name).subscribe(result => {
                this.vendors.suburbId = result;
            });
            this._leadsServiceProxy.stateId(result.address_components[4].short_name).subscribe(result => {
                this.vendors.stateId = result;
            });
            this.vendors.postCode = result.address_components[6].long_name.toUpperCase();
            this.vendors.address = this.vendors.unitNo + " " + this.vendors.unitType + " " + this.vendors.streetNo + " " + this.streetName + " " + this.streetType;  //result.formatted_address;
        }
        else if (result.address_components.length == 6) {
            this.vendors.unitNo = "";
            this.unitType = "";
            this.vendors.unitType = "";
            this.vendors.streetNo = "";
            this.streetName = "";
            this.streetType = "";
            this.suburb = "";
            this.vendors.state = "";
            this.vendors.suburbId = 0;
            this.vendors.stateId = 0;
            this.vendors.postCode = "";
            this.vendors.address = "";

            this.vendors.streetNo = result.address_components[0].long_name.toUpperCase();
            var str = result.address_components[1].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.streetType = splitted[3].toUpperCase().trim();
            }
            else if (splitted.length == 3) {
                this.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.streetType = splitted[2].toUpperCase().trim();
            }
            else {
                this.streetName = splitted[0].toUpperCase().trim();
                this.streetType = splitted[1].toUpperCase().trim();
            }
            this.suburb = result.address_components[2].long_name.toUpperCase().trim();
            this.vendors.state = result.address_components[3].short_name;
            this._leadsServiceProxy.getSuburbId(result.address_components[5].long_name).subscribe(result => {
                this.vendors.suburbId = result;
            });
            this._leadsServiceProxy.stateId(result.address_components[3].short_name).subscribe(result => {
                this.vendors.stateId = result;
            });
            this.vendors.postCode = result.address_components[5].long_name.toUpperCase();
            this.vendors.address = this.vendors.unitNo + " " + this.vendors.unitType + " " + this.vendors.streetNo + " " + this.streetName + " " + this.streetType;  //result.formatted_address;
        }
        else {
            this.vendors.unitNo = "";
            this.unitType = "";
            this.vendors.unitType = "";
            this.vendors.streetNo = "";
            this.streetName = "";
            this.streetType = "";
            this.suburb = "";
            this.vendors.state = "";
            this.vendors.suburbId = 0;
            this.vendors.stateId = 0;
            this.vendors.postCode = "";
            this.vendors.address = "";

            var str1 = result.address_components[0].long_name.toUpperCase();
            var splitted1 = str1.split(" ");
            if (splitted1.length == 1) {
                this.vendors.unitNo = splitted1[0].toUpperCase().trim();
            }
            else {
                this.vendors.unitNo = splitted1[1].toUpperCase().trim();
                this.unitType = splitted1[0].toUpperCase().trim();
                this.vendors.unitType = splitted1[0].toUpperCase().trim();
            }
            this.vendors.streetNo = result.address_components[1].long_name.toUpperCase();
            var str = result.address_components[2].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.streetType = splitted[3].toUpperCase().trim();
            }
            else if (splitted.length == 3) {
                this.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.streetType = splitted[2].toUpperCase().trim();
            }
            else {
                this.streetName = splitted[0].toUpperCase().trim();
                this.streetType = splitted[1].toUpperCase().trim();
            }
            this.suburb = result.address_components[3].long_name.toUpperCase().trim();
            this.vendors.state = result.address_components[5].short_name;
            this._leadsServiceProxy.getSuburbId(result.address_components[7].long_name).subscribe(result => {
                this.vendors.suburbId = result;
            });
            this._leadsServiceProxy.stateId(result.address_components[5].short_name).subscribe(result => {
                this.vendors.stateId = result;
            });
            this.vendors.postCode = result.address_components[7].long_name.toUpperCase();
            this.vendors.address = this.vendors.unitNo + " " + this.vendors.unitType + " " + this.vendors.streetNo + " " + this.streetName + " " + this.streetType;  //result.formatted_address;
        }
    }

    onLocationSelected(location: Location) {
        debugger;
        this.vendors.latitude = location.latitude.toString();
        this.vendors.longitude = location.longitude.toString();
    }

    filtersuburbs(event): void {
        this._leadsServiceProxy.getAllSuburb(event.query).subscribe(output => {
            this.suburbSuggestions = output;
        });
    }
    //get state & Postcode
    fillstatepostcode(): void {
        var splitted = this.suburb.split("||");
        this.vendors.suburb = splitted[0];
        this.vendors.state = splitted[1].trim();
        this.vendors.postCode = splitted[2].trim();

    }
    
    show(id? : number) : void{
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
        if(id > 0)
            {
            this._vendorServiceProxy.getVendorsForEdit(id).subscribe(result => {
                 this.vendors = result;
                 this.suburb = result.suburb;
                 if (this.vendors.isGoogle == "Google") {
                    this.selectAddress = true;
                }
                else {
                    this.selectAddress = false;
                }
                if(this.vendors.vendorContacts.length <= 0){
                    
                    let contact = new VendorContactDto();
                    this.vendors.vendorContacts.push(contact);
                   
                }
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Edit Inventory Vendors : ' + this.vendors.companyName;
            log.section = 'Inventory Vendors';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            })
            
          
        }
        else{
            this.vendors = new CreateOrEditVendorsDto();
          
            this.vendors.vendorContacts = [];
            this.vendors.isGoogle = "Google";

            let contact = new VendorContactDto();
            this.vendors.vendorContacts.push(contact);
           
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Inventory Vendors';
            log.section = 'Inventory Vendors';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        }
       
        this.active = true;
        this.modal.show();   
    }

    removeContactItem(contactItem): void {
        if (this.vendors.vendorContacts.length == 1)
            return;
       
        if (this.vendors.vendorContacts.indexOf(contactItem) === -1) {
            
        } else {
            this.vendors.vendorContacts.splice(this.vendors.vendorContacts.indexOf(contactItem), 1);
        }
    }

    addContactItem(): void {
        let contact = new VendorContactDto();
        this.vendors.vendorContacts.push(contact);
    }
    onShown(): void {
        
        document.getElementById('vendors_vendors').focus();
    }
    save(): void {
            this.saving = true;
            this.vendors.unitType = this.unitType;
            this.vendors.suburb = this.suburb;
            this.vendors.streetName = this.streetName;
            this.vendors.streetType = this.streetType;
            this._vendorServiceProxy.createOrEdit(this.vendors)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.vendors.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Inventory Vendors Updated : '+ this.vendors.companyName;
                    log.section = 'Inventory Vendors';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Inventory Vendors Created : '+ this.vendors.companyName;
                    log.section = 'Inventory Vendors';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }

    google(): void {
        debugger;
        this.selectAddress = true;
    }

    database(): void {
        this.selectAddress = false;
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    filterunitTypes(event): void {
        this._leadsServiceProxy.getAllUnitType(event.query).subscribe(output => {
            this.unitTypesSuggestions = output;
        });
    }

    // get StreetType
    filterstreettypes(event): void {
        this._leadsServiceProxy.getAllStreetType(event.query).subscribe(output => {
            this.streetTypesSuggestions = output;
        });
    }

    // get StreetName
    filterstreetnames(event): void {
        this._leadsServiceProxy.getAllStreetName(event.query).subscribe(output => {
            this.streetNamesSuggestions = output;
        });
    }
}
