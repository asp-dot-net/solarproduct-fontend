import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { CreateOrEditVendorsDto, VendorContactDto,CreateOrEditPurchaseCompanyDto, VendorServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';


@Component({
    selector: 'viewVendorModal',
    templateUrl: './viewvendor.modal.component.html'
})
export class ViewVendorModalComponent extends AppComponentBase {
   
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    vendors: CreateOrEditVendorsDto = new CreateOrEditVendorsDto();



    constructor(
        injector: Injector,
        private _vendorServiceProxy: VendorServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }
    
    show(id? : number) : void{
        debugger;
        if(id > 0){
            this._vendorServiceProxy.getVendorsForEdit(id).subscribe(result => {
                 this.vendors = result;
                if(this.vendors.vendorContacts.length <= 0){
                    
                    let contact = new VendorContactDto();
                    this.vendors.vendorContacts.push(contact);
                   
                }

            })
        }
        else{
            this.vendors = new CreateOrEditVendorsDto();
            // this.vendors.id = oid;
            this.vendors.vendorContacts = [];
            let contact = new VendorContactDto();
            this.vendors.vendorContacts.push(contact);
            
        }
        this.active = true;
        this.modal.show();
        

        
        
    }
    removeContactItem(contactItem): void {
        if (this.vendors.vendorContacts.length == 1)
            return;
       
        if (this.vendors.vendorContacts.indexOf(contactItem) === -1) {
            
        } else {
            this.vendors.vendorContacts.splice(this.vendors.vendorContacts.indexOf(contactItem), 1);
        }
    }

    addContactItem(): void {
        let contact = new VendorContactDto();
        this.vendors.vendorContacts.push(contact);
    }
    onShown(): void {
        
        document.getElementById('vendors_vendors').focus();
    }
    save(): void {
            this.saving = true;
            this._vendorServiceProxy.createOrEdit(this.vendors)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
             });
    }



    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
