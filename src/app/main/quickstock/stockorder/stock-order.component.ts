import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TeamsServiceProxy, TeamDto, CommonLookupServiceProxy, StockOrderServiceProxy } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { Title } from '@angular/platform-browser';
import { CreateOrEditStockOrderModalComponent } from './create-or-edit-stock-order-modal.component';
import {SMSModalComponent} from '@app/main/inventory/Activity/sms-modal-component';
import {EmailModalComponent} from '@app/main/inventory/Activity/email-modal-component';
import {RemindersModalComponent} from '@app/main/inventory/Activity/reminders-modal-component';
import{CommentsModalComponent} from '@app/main/inventory/Activity/comments-modal.component';
import { Moment } from 'moment';
@Component({
    templateUrl: './stock-order.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class StockOrderComponent extends AppComponentBase {

    show: boolean = true;
    showchild: boolean = true;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    @ViewChild('createOrEditStockOrderModal', { static: true }) createOrEditStockOrderModal: CreateOrEditStockOrderModalComponent;
    @ViewChild('SMSModal', { static: true }) SMSModal: SMSModalComponent;
    @ViewChild('EmailModal', { static: true }) EmailModal: EmailModalComponent;
    @ViewChild('RemindersModal', { static: true }) RemindersModal: RemindersModalComponent;
    @ViewChild('CommentsModal', { static: true }) CommentsModal: CommentsModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    StockOrderStatusIds = [];
    WarehouseLocationIdFiletr = [];
    PaymentStatusIdFiletr = [];
    PaymentTypeIdFiletr = [];
    VendorIdFiletr = [];
    DeliveryTypeIdFiletr = [];
    StockOrderIdFiletr = [];
    TransportCompanyIdFiletr = [];
    GstTypeIdFilter= 0;
    advancedFiltersAreShown = false;
    filterText = '';
    nameFilter = '';
    firstrowcount = 0;
    last = 0;

    public screenHeight: any;
    testHeight = 250;

    constructor(
        injector: Injector,
        private _fileDownloadService: FileDownloadService,
        private _stockOrderServiceProxy: StockOrderServiceProxy,
        private titleService: Title
        ) {
            super(injector);
            this.titleService.setTitle(this.appSession.tenancyName + " |  Stock Order");
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight;

    }
    @HostListener('window:resize', ['$event'])
    onResize(event) {
        this.screenHeight = window.innerHeight;
    }

    getStockOrder(event?: LazyLoadEvent) {
        // if (this.primengTableHelper.shouldResetPaging(event)) {
        //     this.paginator.changePage(0);
        //     return;
        // }

        // this.primengTableHelper.showLoadingIndicator();

        // this._stockOrderServiceProxy.getAll(
        //     this.filterText,
        //     this.GstTypeIdFilter,
        //     this.TransportCompanyIdFiletr,
        //     this.StockOrderStatusIds,
        //     this.WarehouseLocationIdFiletr,
        //     this.PaymentStatusIdFiletr,
        //     this.PaymentTypeIdFiletr,
        //     this.VendorIdFiletr,
        //     this.DeliveryTypeIdFiletr,
        //     this.StockOrderIdFiletr,
        //     this.primengTableHelper.getSorting(this.dataTable),
        //     this.primengTableHelper.getSkipCount(this.paginator, event),
        //     this.primengTableHelper.getMaxResultCount(this.paginator, event)
        // ).subscribe(result => {
        //     this.primengTableHelper.totalRecordsCount = result.totalCount;
        //     this.primengTableHelper.records = result.items;
        //      //result.items[0].amtDiscountwithOutGST
        //     //  result.items.forEach(item => {

        //     //     item.expDeliveryDate = moment(item.expDeliveryDate, 'YYYY-MM-DD') as Moment;
        //     // });
        //     const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
        //     this.firstrowcount =  totalrows + 1;
        //     this.last = totalrows + result.items.length;
        //     this.primengTableHelper.hideLoadingIndicator();
        // });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createStockOrder(): void {
         this.createOrEditStockOrderModal.show();
    }


    deleteStockOrder(id: number): void {
        // this.message.confirm(
        //     this.l('AreYouSureWanttoDelete'),
        //     this.l('Delete'),
        //     (isConfirmed) => {
        //         if (isConfirmed) {
        //             this._stockOrderServiceProxy.delete(id)
        //                 .subscribe(() => {
        //                     this.reloadPage();
        //                     this.notify.success(this.l('SuccessfullyDeleted'));
        //                 });
        //         }
        //     }
        // );
    }

    exportToExcel(): void {
        // this._stockOrderServiceProxy.getStockOrderToExcel(
        //     this.filterText,

        //     )
        //     .subscribe(result => {
        //         this._fileDownloadService.downloadTempFile(result);
        //      });
    }
}


