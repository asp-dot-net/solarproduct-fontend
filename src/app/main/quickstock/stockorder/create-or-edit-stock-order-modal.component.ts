import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {CreateOrEditStockOrderDto, CommonLookupDto, StockOrderServiceProxy ,CommonLookupServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';


@Component({
    selector: 'createOrEditStockOrderModal',
    templateUrl: './create-or-edit-stock-order-modal.component.html'
})
export class CreateOrEditStockOrderModalComponent extends AppComponentBase {
   
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    allPurchaseCompany: CommonLookupDto[];
    allStockOrderStatus: CommonLookupDto[];
    allPaymentType: CommonLookupDto[];
    allPaymentMethod: CommonLookupDto[];
    allDeliveryType: CommonLookupDto[];
    allLocation: CommonLookupDto[];
    allStockOrderfor: CommonLookupDto[];
    allCurrency: CommonLookupDto[];
    allStockFrom :CommonLookupDto[];
    allVendors :CommonLookupDto[];
    allPaymentStatus:CommonLookupDto[];
    stockorder: CreateOrEditStockOrderDto = new CreateOrEditStockOrderDto();
    date = new Date();
    targetArrivalDate = moment(new Date(this.date.getFullYear(), this.date.getMonth(), 1));
    eta = moment(new Date(this.date.getFullYear(), this.date.getMonth(), 1));
    etd = moment(new Date(this.date.getFullYear(), this.date.getMonth(), 1));
    physicalDeliveryDate = moment(new Date(this.date.getFullYear(), this.date.getMonth(), 1));
    
    constructor(
        injector: Injector,
        private _stockOrderServiceProxy: StockOrderServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
       
        this._commonLookupService.getPurchaseCompanyDropdown().subscribe(result => {
            this.allPurchaseCompany = result;
        })
        this._commonLookupService.getStockOrderStatusDropdown().subscribe(result => {
            this.allStockOrderStatus = result;
        })
        this._commonLookupService.getpaymentTypeDropdown().subscribe(result => {
            this.allPaymentType = result;
        })
        this._commonLookupService.getStockOrderPaymentStatusDropdown().subscribe(result => {
            this.allPaymentStatus = result;
        })
        this._commonLookupService.getpaymentMethodDropdown().subscribe(result => {
            this.allPaymentMethod = result;
        })
        this._commonLookupService.getDeliveryTypeDropdown().subscribe(result => {
            this.allDeliveryType = result;
        })
        this._commonLookupService.getcurrencyDropdown().subscribe(result => {
            this.allCurrency = result;
        })
        this._commonLookupService.getLocationDropdown().subscribe(result => {
            this.allLocation = result;
        })
        this._commonLookupService.getStockorderfoDropdown().subscribe(result => {
            this.allStockOrderfor = result;
        })
        this._commonLookupService.getStockOrderFromOutsideDropdown().subscribe(result => {
            this.allStockFrom = result;
        })
        this._commonLookupService.getAllVendorDropdown().subscribe(result => {
            this.allVendors = result;
        })
         
    }
    removeContactItem(contactItem): void {
        // if (this.stockorder.productInfo.length == 1)
        //     return;
       
        // if (this.stockorder.productInfo.indexOf(contactItem) === -1) {
            
        // } else {
        //     this.stockorder.productInfo.splice(this.stockorder.productInfo.indexOf(contactItem), 1);
        // }
    }

    addContactItem(): void {
        // let contact = new ProductsItem();
        // this.stockorder.productInfo.push(contact);
    }
    show(stockorderId?: number): void {
    

        if (!stockorderId) {
            this.stockorder = new CreateOrEditStockOrderDto();
            this.stockorder.id = stockorderId;
            // this.stockorder.productInfo = [];
            // let contact = new ProductsItem();
            // this.stockorder.productInfo.push(contact);
           
        } else {
            // this._stockOrderServiceProxy.getPaymentTypeForEdit(paymentTypeId).subscribe(result => {
            //     this.paymentType = result.paymentType;


            //     this.active = true;
            //     this.modal.show();
            // });
        }
        this.active = true;
        this.modal.show();
    }
    onShown(): void {
        
        document.getElementById('stockorder_stockorder').focus();
    }
    
    save(): void {
        debugger;
            this.saving = true;
            this.stockorder.targetArrivalDate = this.targetArrivalDate; 
             this.stockorder.eta = this.eta;
             this.stockorder.etd = this.etd;
             this.stockorder.physicalDeliveryDate = this.physicalDeliveryDate;
            this._stockOrderServiceProxy.createOrEdit(this.stockorder)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
             });
    }



    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
