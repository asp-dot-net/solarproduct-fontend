import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {CreateOrEditPaymentTypeDto, PaymentTypeServiceProxy  } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';


@Component({
    selector: 'createOrEditPaymentTypeModal',
    templateUrl: './createoredit-paymenttype-modal.component.html'
})
export class CreateOrEditPaymentTypeModalComponent extends AppComponentBase {
   
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    saving = false;
    paymentType: CreateOrEditPaymentTypeDto = new CreateOrEditPaymentTypeDto();

    constructor(
        injector: Injector,
        private _paymentTypeServiceProxy: PaymentTypeServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }
    
    show(paymentTypeId?: number): void {

        if (!paymentTypeId) {
            this.paymentType = new CreateOrEditPaymentTypeDto();
            this.paymentType.id = paymentTypeId;
            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Inventory Payment Type';
            log.section = 'Inventory Payment Type';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._paymentTypeServiceProxy.getPaymentTypeForEdit(paymentTypeId).subscribe(result => {
                this.paymentType = result.paymentType;
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Inventory Payment Type : ' + this.paymentType.name;
                log.section = 'Inventory Payment Type';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        
    }
    onShown(): void {
        
        document.getElementById('paymentType_paymentType').focus();
    }
    save(): void {
            this.saving = true;
            this._paymentTypeServiceProxy.createOrEdit(this.paymentType)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.paymentType.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Inventory Payment Type Updated : '+ this.paymentType.name;
                    log.section = 'Inventory Payment Type';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Inventory Payment Type Created : '+ this.paymentType.name;
                    log.section = 'Inventory Payment Type';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
