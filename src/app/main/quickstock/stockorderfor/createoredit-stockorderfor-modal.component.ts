import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { StockOrderForsServiceProxy, CreateOrEditStockOrderForsDto } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {  } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'createOrEditStockOrderForModal',
    templateUrl: './createoredit-stockorderfor-modal.component.html'
})
export class CreateOrEditStockOrderForModalComponent extends AppComponentBase {
   
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    stockorderfor: CreateOrEditStockOrderForsDto  = new CreateOrEditStockOrderForsDto ();
    id? :number;
    constructor(
        injector: Injector,
        private _stockOrderForsServiceProxy: StockOrderForsServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }
    
    show(stockOrderForsid?: number): void {
        if (!stockOrderForsid) {
            this.stockorderfor = new CreateOrEditStockOrderForsDto ();
            this. stockorderfor.id = stockOrderForsid;

            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Inventory Stock Order For';
            log.section = 'Inventory Stock Order For';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._stockOrderForsServiceProxy.getStockOrderForsForEdit(stockOrderForsid).subscribe(result => {
                this.stockorderfor= result.stockorderfors;
                this.active = true;
                this.modal.show();
                
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Inventory Stock Order For : ' + this.stockorderfor.name;
                log.section = 'Inventory Stock Order For';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        
    }

    
    onShown(): void {
        
        document.getElementById('Name').focus();
    }
    save(): void {
        debugger;
        this.saving = true;
        this._stockOrderForsServiceProxy.createOrEdit(this.stockorderfor)
         .pipe(finalize(() => { this.saving = false;}))
         .subscribe(() => {
            this.notify.info(this.l('SavedSuccessfully'));
            this.close();
            this.modalSave.emit(null);
            
            if(this.stockorderfor.id){
                let log = new UserActivityLogDto();
                log.actionId = 82;
                log.actionNote ='Inventory Stock Order For Updated : '+ this.stockorderfor.name;
                log.section = 'Inventory Stock Order For';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }else{
                let log = new UserActivityLogDto();
                log.actionId = 81;
                log.actionNote ='Inventory Stock Order For Created : '+ this.stockorderfor.name;
                log.section = 'Inventory Stock Order For';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }
         });

    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
