import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { CreateOrEditIncoTermDto, FreightCompanyServiceProxy, IncoTermServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';


@Component({
    selector: 'createOrEditIncoTermModal',
    templateUrl: './create-or-edit-incoterm-modal.component.html'
})
export class CreateOrEditIncoTermModalComponent extends AppComponentBase {
   
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    incoTerm: CreateOrEditIncoTermDto = new CreateOrEditIncoTermDto();

    constructor(
        injector: Injector,
        private _incoTermServiceProxy: IncoTermServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }
    
    show(incotermId?: number): void {
    

        if (!incotermId) {
            this.incoTerm = new CreateOrEditIncoTermDto();
            this.incoTerm.id = incotermId;

            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New  Inco Term';
            log.section = 'Inventory Inco Term';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._incoTermServiceProxy.getIncoTermForEdit(incotermId).subscribe(result => {
                this.incoTerm = result.incoTerm;
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit  Inco Term : ' + this.incoTerm.name;
                log.section = 'Inventory Inco Term';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        
    }
    onShown(): void {
        
        document.getElementById('incoTerm_incoTerm').focus();
    }
    save(): void {
            this.saving = true;
            this._incoTermServiceProxy.createOrEdit(this.incoTerm)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.incoTerm.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Inco Term Updated : '+ this.incoTerm.name;
                    log.section = 'Inventory Inco Term';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Inco Term Created : '+ this.incoTerm.name;
                    log.section = 'Inventory Inco Term';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
