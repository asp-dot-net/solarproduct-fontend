import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IncoTermDto, IncoTermServiceProxy } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { Title } from '@angular/platform-browser';
import { CreateOrEditIncoTermModalComponent } from './create-or-edit-incoterm-modal.component';

@Component({
    templateUrl: './incoterm.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class IncoTermComponent extends AppComponentBase {

    show: boolean = true;
    showchild: boolean = true;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    @ViewChild('createOrEditIncoTermModal', { static: true }) createOrEditIncoTermModal: CreateOrEditIncoTermModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    nameFilter = '';
    firstrowcount = 0;
    last = 0;

    public screenHeight: any;  
    testHeight = 250;

    constructor(
        injector: Injector,
        private _incoTermServiceProxy: IncoTermServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private titleService: Title
        ) {
            super(injector);
            this.titleService.setTitle(this.appSession.tenancyName + " |  Inco Term");
    }
    searchLog() : void {
        
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Inventory Inco Term';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
       
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Inco Term';
        log.section = 'Inventory Inco Term';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
    }
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  

    getIncoTerm(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();
        this._incoTermServiceProxy.getAll(
            this.filterText,
            //this.nameFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            //result.items[0].incoTerm.isActive
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }


    createIncoTerm(): void {
        this.createOrEditIncoTermModal.show();        
    }

    deleteIncoTerm(IncoTermDto: IncoTermDto): void {
        this.message.confirm(
            this.l('AreYouSureWanttoDelete'),
            this.l('Delete'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._incoTermServiceProxy.delete(IncoTermDto.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete  Inco Term: ' + IncoTermDto.name;
                            log.section = 'Inventory Inco Term';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });
                        });
                }
            }
        );
    }

    exportToExcel(): void {
        this._incoTermServiceProxy.getIncoTermToExcel(
            this.filterText,
               
            )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
             });
    }
}
