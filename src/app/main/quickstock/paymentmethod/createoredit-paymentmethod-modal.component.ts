import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {CreateOrEditPaymentMethod, PaymentMethodServiceProxy  } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';


@Component({
    selector: 'createOrEditPaymentMethodModal',
    templateUrl: './createoredit-paymentmethod-modal.component.html'
})
export class CreateOrEditPaymentMethodModalComponent extends AppComponentBase {
   
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    saving = false;
    paymentMethod: CreateOrEditPaymentMethod = new CreateOrEditPaymentMethod();

    constructor(
        injector: Injector,
        private _paymentStatusServiceProxy: PaymentMethodServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }
    
    show(paymentMethodId?: number): void {
    

        if (!paymentMethodId) {
            this.paymentMethod = new CreateOrEditPaymentMethod();
            this.paymentMethod.id = paymentMethodId;
            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Inventory Payment Method';
            log.section = 'Inventory Payment Method';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._paymentStatusServiceProxy.getPaymentMethodForEdit(paymentMethodId).subscribe(result => {
                this.paymentMethod = result.paymentMethod;
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Inventory Payment Method : ' + this.paymentMethod.name;
                log.section = 'Inventory Payment Method';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });

            });
        }
        
    }
    onShown(): void {
        
        document.getElementById('paymentMethod_paymentMethod').focus();
    }
    save(): void {
            this.saving = true;
            this._paymentStatusServiceProxy.createOrEdit(this.paymentMethod)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                
                if(this.paymentMethod.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Inventory Payment Method Updated : '+ this.paymentMethod.name;
                    log.section = 'Inventory Payment Method';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Inventory Payment Method Created : '+ this.paymentMethod.name;
                    log.section = 'Inventory Payment Method';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }



    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
