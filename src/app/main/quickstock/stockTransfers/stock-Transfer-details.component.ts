import { Component, ViewChild, Injector, Input, Output, EventEmitter, OnInit,NgModule  } from '@angular/core';
import { GetStockTransferForViewDto, GetPurchaseOrderActivityLogViewDto, CallHistoryServiceProxy, GetActivityLogViewDto, StockOrderServiceProxy, StockOrderDto, OrganizationUnitMapDto, GetCallHistoryViewDto, GetStockOrderForViewDto, CommonLookupServiceProxy, WholeSaleLeadServiceProxy, CreateOrEditWholeSaleLeadDto, StockTransferServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import PlaceResult = google.maps.places.PlaceResult;
import { NgxSpinnerService } from 'ngx-spinner';
import { DatePipe } from '@angular/common';
import { AppConsts } from '@shared/AppConsts';
import {  TokenService } from 'abp-ng2-module';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { finalize } from 'rxjs/operators';
import {StockTranferActivityLogComponent} from './stock-transfer-activity-log.component';
@Component({
    selector: 'viewStockTranferDetail',
    templateUrl: './stock-Transfer-details.component.html',
    animations: [appModuleAnimation()]
})
export class StockTranferDetailComponent extends AppComponentBase implements OnInit {
    @ViewChild('stockTransferactivityLog', { static: true }) stockTransferactivityLog: StockTranferActivityLogComponent;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @Input() SelectedStockTransferId: number = 0;
    @Output() reloadLead = new EventEmitter<boolean>();
    active = false;
    saving = false;
    activeTabIndex: number = 0;
    actionId: number = 0;
    activity: boolean = false;
    zoom: 20;
    showforresult = false;
    name: string;
    role: string = '';
    mainsearch = false;
    sectionId = 0;
    showsectionwise = false;
   StockTransferId = 0;
   stockTransfer: GetStockTransferForViewDto = new GetStockTransferForViewDto();
   StockTransferItemList: any[];
    constructor(
        injector: Injector,
        private _stockTransferServiceProxy: StockTransferServiceProxy,
        private spinner: NgxSpinnerService,
        public datepipe: DatePipe,
      
    ) {
        super(injector);
      
    }
    ngOnInit(): void {
        if (this.SelectedStockTransferId > 0)
            this.showDetail(this.SelectedStockTransferId);
        
    }

    showDetail(stockTransferId: number, StockTransferName?: string, sectionId?: number): void {
        debugger;
        this.StockTransferId = stockTransferId;
        this.SelectedStockTransferId = stockTransferId;
        this.sectionId = sectionId;
        this.name = StockTransferName;
    
        if (stockTransferId != null) {
            this.refreshStockTransferData();
        }
    } 
    refreshStockTransferData(): void {
        this.spinner.show();
        this._stockTransferServiceProxy.getStockTransferById(this.StockTransferId).subscribe(result => {
            this.stockTransfer = result;
            
            this.StockTransferItemList = [];
            this.stockTransfer.productItemInfo.map((item) => {
                let StockTransferItemCreateOrEdit = { id: 0, productTypeId: 0, productType: " ", productItemId: 0, productItemName: "", quantity: 0, size: 0,   productModel: "",};
                StockTransferItemCreateOrEdit.id = item.id;
                StockTransferItemCreateOrEdit.productTypeId = item.productTypeId;
                StockTransferItemCreateOrEdit.productItemId = item.productItemId;
                StockTransferItemCreateOrEdit.productItemName = item.productItem;
                StockTransferItemCreateOrEdit.productType = item.productType;
                StockTransferItemCreateOrEdit.quantity = item.quantity;
                StockTransferItemCreateOrEdit.size = item.size;
                StockTransferItemCreateOrEdit.productModel=item.modelNo;
                
               
                this.StockTransferItemList.push(StockTransferItemCreateOrEdit);
         });
        this.stockTransferactivityLog.show(this.StockTransferId, 0, this.sectionId);
      
        this.spinner.hide();
          });

        
    }

    registerToEvents() {
        abp.event.on('app.onCancelModalSaved', () => {
            debugger;
            this.showDetail(this.SelectedStockTransferId);
        });
    }
}
    


