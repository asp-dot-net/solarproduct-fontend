import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {CreateOrEditStockTransferDto, CommonLookupDto, JobsServiceProxy ,CommonLookupServiceProxy , StockTransferServiceProxy,OrganizationUnitDto,ProductsItemDto} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'createOrEditStockTransferModal',
    templateUrl: './create-or-edit-stock-transfer-modal.componenet.html'
})
export class CreateOrEditStockTransferModalComponent extends AppComponentBase {
   
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    productItemSuggestions: any[];
    productTypes: CommonLookupDto[];
    StockTransferItemList: any[];
    active = false;
    saving = false;
    allTransportCompany: CommonLookupDto[];
    organizationUnit = 0;
    organizationUnitlength: number = 0;
    allLocation: CommonLookupDto[];
    filteredLocation: CommonLookupDto[];
    allOrganizationUnits: OrganizationUnitDto[];
    stocktransfer: CreateOrEditStockTransferDto = new CreateOrEditStockTransferDto();
    date = new Date();
    targetArrivalDate = moment(new Date(this.date.getFullYear(), this.date.getMonth(), 1));
    eta = moment(new Date(this.date.getFullYear(), this.date.getMonth(), 1));
    etd = moment(new Date(this.date.getFullYear(), this.date.getMonth(), 1));
    physicalDeliveryDate = moment(new Date(this.date.getFullYear(), this.date.getMonth(), 1));
    
    constructor(
        injector: Injector,
        private _stockTransferServiceProxy: StockTransferServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _jobServiceProxy: JobsServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }

    ngOnInit(): void {
       
        this._commonLookupService.getTransportCompanyDropdown().subscribe(result => {
            this.allTransportCompany = result;
        })
        this._commonLookupService.getAllProductTypeForTableDropdown().subscribe(result => {
            this.productTypes = result;
        });
        this._commonLookupService.getLocationDropdown().subscribe(result => {
            this.allLocation = result;
            this.filteredLocation = [...this.allLocation];
        })
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;   
        }); 
    }
    filterToLocation() {
        if (this.stocktransfer.warehouseLocationFromId) {
            this.filteredLocation = [...this.allLocation]; 
    
           
            const index = this.filteredLocation.findIndex(location => 
                location.id === Number(this.stocktransfer.warehouseLocationFromId));
    
            if (index > -1) {
                this.filteredLocation.splice(index, 1); 
            }
        } else {
            this.filteredLocation = [...this.allLocation];
        }
    }
    
    
    filterProductIteams(event, i): void {
        let Id = this.StockTransferItemList[i].productTypeId;

        this._jobServiceProxy.getPicklistProductItemList(Id, event.query).subscribe(output => {
            
            debugger;
            var items =this.StockTransferItemList.map(i=>i.productItemName+'');
            this.productItemSuggestions = output.filter(x=> !items.includes(x.productItem))
        });
    }
     selectProductItem(event, i) {
        this.StockTransferItemList[i].productItemId = event.id;
        this.StockTransferItemList[i].productItemName = event.productItem;
        this.StockTransferItemList[i].productModel = event.productModel;
        this.StockTransferItemList[i].size = event.size;
    }
    removeProductItem(installationItem): void {
        if (this.StockTransferItemList.length == 1)
            return;
       
        if (this.StockTransferItemList.indexOf(installationItem) === -1) {
            
        } else {
            this.StockTransferItemList.splice(this.StockTransferItemList.indexOf(installationItem), 1);
        }     
    }
    addProductItem(): void {
        let PurchaseOrderItemCreateOrEdit = { id: 0, productTypeId: 0, productItemId: 0, productItemName: "",size:0, quantity:0, productModel: "", }
        this.StockTransferItemList.push(PurchaseOrderItemCreateOrEdit);  
    }
    changeProductType(i) : void {
        this.StockTransferItemList[i].productItemId = "";
        this.StockTransferItemList[i].productItemName = "";
        this.StockTransferItemList[i].quantity = 0;
        this.StockTransferItemList[i].size = 0;
        this.StockTransferItemList[i].productModel = ""; 
    }
    show(stockorderId?: number,OrgId?: number): void {
        if (stockorderId == 0) {
            this.stocktransfer = new CreateOrEditStockTransferDto();
            this._stockTransferServiceProxy.getLastTransferNo(OrgId).subscribe((LastTransferNo) => {
                this.stocktransfer.transferNo = LastTransferNo; 
            this.stocktransfer.organizationId=OrgId;
            this.stocktransfer.id = stockorderId;
            this.filteredLocation = [...this.allLocation];
                    this.StockTransferItemList = [{
                        id: 0,
                        productTypeId: 0,
                        productItemId: 0,
                        productItemName: "",
                        productModel: "",
                        size: 0,
                        quantity: 0,

                    }];
           
                    let log = new UserActivityLogDto();
                    log.actionId = 79;
                    log.actionNote ='Open For Create New Stock Transfer';
                    log.section = 'Stock Transfer';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    });
                })
        } 
        else {
            this._stockTransferServiceProxy.getStockTransferForEdit(stockorderId).subscribe(result => {
                this.stocktransfer = result.stockTransfer;
                this.StockTransferItemList = result.stockTransfer.productItemInfo.map((item) => ({
                    id: item.id,
                    productTypeId: item.productTypeId,
                    productItemId: item.productItemId,
                    productItemName: item.productItem,
                    quantity: item.quantity,
                    productModel: item.modelNo,
                    size : item.size || 0
                }));
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Edit Stock Transfer : ' + this.stocktransfer.vehicalNo;
            log.section = 'Stock Transfer';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            });
            

        }
        this.active = true;
        this.modal.show();
    }
    onShown(): void {
        
        document.getElementById('stocktransfer_stocktransfer').focus();
    }
    save(): void {
        debugger;
            this.saving = true;
            //  this.stocktransfer.transferByDate=moment();
            //  this.stocktransfer.transferd=2;
            this.stocktransfer.productItemInfo = [];
             this.StockTransferItemList.map((item) => {
                 let StockTransferItemListDto = new ProductsItemDto();
                 StockTransferItemListDto.id = item.id;
                 StockTransferItemListDto.productItemId = item.productItemId;
                 StockTransferItemListDto.quantity = item.quantity;
                 this.stocktransfer.productItemInfo.push(StockTransferItemListDto);
             });
            this._stockTransferServiceProxy.createOrEdit(this.stocktransfer)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.stocktransfer.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Stock Transfer Updated : '+ this.stocktransfer.vehicalNo;
                    log.section = 'Stock Transfer';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Stock Transfer Created : '+ this.stocktransfer.vehicalNo;
                    log.section = 'Stock Transfer';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
