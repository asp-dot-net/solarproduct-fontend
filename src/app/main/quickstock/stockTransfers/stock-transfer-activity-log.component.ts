import { Component, EventEmitter, Injector, Input, OnInit, Output, ViewChild } from '@angular/core';
import {
    StockOrderServiceProxy, GetStockTransferActivityLogViewDto, StockOrderDto, GetStockOrderForViewDto, LeadDto, CommonLookupDto, LeadsServiceProxy,
    StockTransferServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import PlaceResult = google.maps.places.PlaceResult;
import { JobsComponent } from '@app/main/jobs/jobs/jobs.component';
import { PromotionStopResponceComponent } from '@app/main/promotions/promotionUsers/stop-responce.component';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { StockTransferActivityLogHistoryComponent } from './stock-Transfer-activity-log-histroy.component';

@Component({
    selector: 'stockTransferactivityLog',
    templateUrl: './stock-transfer-activity-log.component.html',
    animations: [appModuleAnimation()]
})
export class StockTranferActivityLogComponent extends AppComponentBase implements OnInit {

     @ViewChild('stockTransferActivityLogHistory', { static: true }) stockTransferActivityLogHistory: StockTransferActivityLogHistoryComponent;
    @Input() SelectedStockTransferId: number = 0;
    @Output() reloadLead = new EventEmitter();
    active = false;
    saving = false;
    purchaseOrder: GetStockOrderForViewDto = new GetStockOrderForViewDto();
    activeTabIndex: number = 0;
    activityDatabyDate: any[] = [];
    actionId: number = 0;
    activity: boolean = false;
    StockOrderActivityList: GetStockTransferActivityLogViewDto[];
    leadActionList: CommonLookupDto[];
    PurchaseOrderId: number = 0;
    id: number = 0;
    role: string = '';
    sectionId: number = 0;
    showsectionwise = false;
    currentactivity: boolean = false;
    allActivity: boolean = false;
    serviceId : number =0 ;
    PurchaseId = 0;

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _stockTransferServiceProxy: StockTransferServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _router: Router,
        private spinner: NgxSpinnerService
    ) {
        super(injector);
        this.purchaseOrder= new GetStockOrderForViewDto();
         this.purchaseOrder.purchaseOrder = new StockOrderDto();
    }


    ngOnInit(): void {
    
        // this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
        //     this.role = result;
        //     if (this.role == 'Sales Rep') {
        //         this.activity = true;
        //     }
        // });
    }

    show(stockTransferId?: number, param?: number, sectionId?: number, service? : number): void {
        debugger;
        this.sectionId = sectionId;
        this.activity = false;
        this.allActivity = false;
        this.serviceId = service;
        this.PurchaseId = stockTransferId;
        if (this.sectionId == 0 || this.sectionId > 12) {
            this.showsectionwise = true;
        } 
        if (param != null) {
            this.id = param;
        }
        this.PurchaseOrderId = stockTransferId;
        this.spinner.show();
            //this.actionId = 1;
            this._stockTransferServiceProxy.getStockTransferActivityLog(stockTransferId, this.actionId, this.sectionId, this.currentactivity, this.allActivity).subscribe(result => {
                debugger;
                  this.StockOrderActivityList = result;
                  this.spinner.hide();   
              });
        
    }

    showDetail(stockTransferId: number): void {
        let that = this;
        debugger

        this._stockTransferServiceProxy.getStockTransferActivityLog(stockTransferId, this.actionId, this.sectionId, this.currentactivity, this.allActivity).subscribe(result => {
           
              this.StockOrderActivityList = result;
          });
    }

    registerToEvents() {
        abp.event.on('app.onCancelModalSaved', () => {
            this.showDetail(this.PurchaseOrderId);
        });
    }

    addActivitySuccess() {
        this.showDetail(this.PurchaseOrderId);
    }

    changeActivity(stockTransferId: number) {
        let that = this;
        this.spinner.show();
        debugger
        this._stockTransferServiceProxy.getStockTransferActivityLog(stockTransferId, this.actionId, this.sectionId, this.currentactivity, this.allActivity).subscribe(result => {
            if (result.length > 0) {
                this.notify.success(this.l('ActivityFiltered'));
            }
            else {
                this.notify.success(this.l('NoActivityFound'));
            }
            this.StockOrderActivityList = result;
            this.spinner.hide();
        });
    }
    navigateToLeadHistory(leadid): void {

        // this.wholesaleactivityloghistory.show(leadid);
    }

    myActivity(StockId: number) {
        let that = this;
        this.spinner.show();
        this._stockTransferServiceProxy.getStockTransferActivityLog(StockId, this.actionId, this.sectionId, this.currentactivity, this.allActivity).subscribe(result => {
            if (result.length > 0) {
                this.notify.success(this.l('ActivityFiltered'));
            }
            else {
                this.notify.success(this.l('NoActivityFound'));
            }
            this.StockOrderActivityList = result;
            this.spinner.hide();
        });
    }
}
