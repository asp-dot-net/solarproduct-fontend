import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OrganizationUnitDto, TeamDto, CommonLookupServiceProxy, CommonLookupDto, StockTransferServiceProxy } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { GetStockTransferForViewDto } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { Title } from '@angular/platform-browser';
import { CreateOrEditStockTransferModalComponent } from './create-or-edit-stock-transfer-modal.componenet';
import { StockTranferDetailComponent } from './stock-Transfer-details.component';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';

@Component({
    templateUrl: './stock-transfers.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class StockTransfersComponent extends AppComponentBase {

    show: boolean = true;
    showchild: boolean = true;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };
    allTransportCompany: CommonLookupDto[];
    allLocation: CommonLookupDto[];
    @ViewChild('createOrEditStockTransferModal', { static: true }) createOrEditStockTransferModal: CreateOrEditStockTransferModalComponent;
    @ViewChild('viewStockTranferDetail', { static: true }) viewStockTranferDetail: StockTranferDetailComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    date = new Date();
    filterName ="TransportCompany";
    StartDate = moment().startOf('month').add(1, 'days');
    EndDate = moment().add(0, 'days').endOf('day');
    dateFilterType  = undefined;
    filterText = '';
    advancedFiltersAreShown = false;
    nameFilter = '';
    firstrowcount = 0;
    last = 0;
    flag = true;
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnit = 0;
    organizationUnitlength: number = 0;
    shouldShow: boolean = false;
    warehouseLocationFromIdFilter:number = undefined;
    WarehouseLocationToIdFilter:number = undefined;
    TransportCompanyFilter:number = undefined;
    public screenHeight: any;  
    testHeight = 250;
    toggle: boolean = true;
    ExpandedView: boolean = true;
    SelectedStockTransferId: number = 0;
    StockTransferName: string;
    constructor(
        injector: Injector,
        private _fileDownloadService: FileDownloadService,
        private _stockTransferServiceProxy: StockTransferServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private titleService: Title
        ) {
            super(injector);
            this.titleService.setTitle(this.appSession.tenancyName + " |  Stock Transfer");
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
        this._commonLookupService.getTransportCompanyDropdown().subscribe(result => {
            this.allTransportCompany = result;
        })
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            
            this.getStockTransfer();
        });
        this._commonLookupService.getLocationDropdown().subscribe(result => {
            this.allLocation = result;
        })
        
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Stock Transfer';
        log.section = 'Stock Transfer';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
    }
    searchLog() : void {
        
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Stock Transfer';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    

    getStockTransfer(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._stockTransferServiceProxy.getAll(
            this.filterName,
            this.filterText,
            this.warehouseLocationFromIdFilter,
            this.WarehouseLocationToIdFilter,
            this.TransportCompanyFilter,
            this.organizationUnit,
            this.dateFilterType,
            this.StartDate,
            this.EndDate,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            debugger
            this.primengTableHelper.records = result.items;
             //result.items[0].notes
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            this.closeModal();
        });
    }
    change() {
        this.toggle = !this.toggle;
      }
      @HostListener('window:resize', ['$event'])  
      onResize(event) { 
          this.screenHeight = window.innerHeight;        
      }  
      
    reloadPage(flafValue: boolean): void {
        
        this.ExpandedView = true;
        this.flag = flafValue;
        this.paginator.changePage(this.paginator.getPage());
        if (!flafValue) {
            this.navigateToStockTransferDetail(this.SelectedStockTransferId);
        }
    }
    navigateToStockTransferDetail(stockTransferId): void {
        debugger;
        this.ExpandedView = !this.ExpandedView;
        this.ExpandedView = false;
        this.SelectedStockTransferId = stockTransferId;
        this.StockTransferName = "StockTransferName";
        this.viewStockTranferDetail.showDetail(stockTransferId, this.StockTransferName, 1);
    }

    createStockTransfer(): void {
         this.createOrEditStockTransferModal.show(0,this.organizationUnit);        
    }
clear(){}
expandGrid() {
    this.ExpandedView = true;
}
closeModal() {
    this.shouldShow = false; // Hides the modal
}
    deleteStockTransfer(GetStockTransferForViewDto: GetStockTransferForViewDto): void {
        this.message.confirm(
            this.l('AreYouSureWanttoDelete'),
            this.l('Delete'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._stockTransferServiceProxy.delete(GetStockTransferForViewDto.id)
                        .subscribe(() => {
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            this.getStockTransfer();
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete Stock Transfer: ' + GetStockTransferForViewDto.transportCompany;
                            log.section = 'Stock Transfer';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });
                        });
                }
            }
        );
    }

    exportToExcel(): void {
        this._stockTransferServiceProxy.getStockTransferToExcel(
            this.filterName,
            this.filterText,
            this.warehouseLocationFromIdFilter,
            this.WarehouseLocationToIdFilter,
            this.TransportCompanyFilter,
            this.organizationUnit,
            this.dateFilterType,
            this.StartDate,
            this.EndDate,
            )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
             });
    }
}


