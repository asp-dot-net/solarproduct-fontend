import { Component, ViewChild, Injector, Output, EventEmitter, OnInit, ElementRef, Directive, Optional } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {
    LeadsServiceProxy, StockTransferHistoryDto, StockOrderServiceProxy, StockTransferServiceProxy, WholeSaleLeadServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AgmCoreModule } from '@agm/core';
import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';
import PlaceResult = google.maps.places.PlaceResult;
import { Appearance, GermanAddress, Location } from '@angular-material-extensions/google-maps-autocomplete';

@Component({
    selector: 'stockTransferActivityLogHistory',
    templateUrl: './stock-Transfer-activity-log-histroy.component.html',
})
export class StockTransferActivityLogHistoryComponent extends AppComponentBase {
    @ViewChild("myNameElem") myNameElem: ElementRef;
    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @ViewChild('SampleDatePicker', { static: true }) sampleDatePicker: ElementRef;


    StockTransfer: StockTransferHistoryDto[];
    constructor(
        injector: Injector,
        private _stockTransferServiceProxy: StockTransferServiceProxy,
        private _el: ElementRef

    ) {
        super(injector);
        this._el.nativeElement.setAttribute('autocomplete', 'off');
        this._el.nativeElement.setAttribute('autocorrect', 'off');
        this._el.nativeElement.setAttribute('autocapitalize', 'none');
        this._el.nativeElement.setAttribute('spellcheck', 'false');
    }

    show(stockTransferId?: number): void {
        debugger
        this._stockTransferServiceProxy.getStockTransferActivityLogHistory(stockTransferId, 0, 0, false, false).subscribe(result => {
                    this.StockTransfer = result;
                    this.modal.show();
                });
    }
    close(): void {

        this.modal.hide();
    }
}
