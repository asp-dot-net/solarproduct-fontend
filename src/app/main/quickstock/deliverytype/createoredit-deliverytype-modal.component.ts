import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { CreateOrEditDeliveryTypesDto, DeliveryTypesServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';


@Component({
    selector: 'createOrEditDeliveryTypeModal',
    templateUrl: './createoredit-deliverytype-modal.component.html'
})
export class CreateOrEditDeliveryTypeModalComponent extends AppComponentBase {
   
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    deliverytype: CreateOrEditDeliveryTypesDto = new CreateOrEditDeliveryTypesDto();
    id? :number;

    constructor(
        injector: Injector,
        private _deliverytypeServiceProxy: DeliveryTypesServiceProxy ,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
    }
    
    show(deliverytypeid?: number): void {
        if (!deliverytypeid) {
            this. deliverytype = new CreateOrEditDeliveryTypesDto();
            this. deliverytype.id = deliverytypeid;
            this.active = true;
            this.modal.show();
            
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Inventory Delivery Type';
            log.section = 'Inventory Delivery Type';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._deliverytypeServiceProxy.getDeliveryTypesForEdit(deliverytypeid).subscribe(result => {
                this.deliverytype = result.deliveryTypes;
                this.active = true;
                this.modal.show(); 
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit  Delivery Type : ' + this.deliverytype.name;
                log.section = 'Inventory Delivery Type';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        
    }
    onShown(): void {
        
        document.getElementById('name').focus();
    }
    save(): void {
            this.saving = true;
            this._deliverytypeServiceProxy.createOrEdit(this.deliverytype)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                 //result.items[0].deliveryTypes,
                this.close();
                this.modalSave.emit(null);
                if(this.deliverytype.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Delivery Type Updated : '+ this.deliverytype.name;
                    log.section = 'Inventory Delivery Type';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Delivery Type Created : '+ this.deliverytype.name;
                    log.section = 'Inventory Delivery Type';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
