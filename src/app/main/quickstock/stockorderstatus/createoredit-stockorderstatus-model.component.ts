import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import {  CreateOrEditStockOrderStatusDto, StockOrderStatusServiceProxy } from '@shared/service-proxies/service-proxies';
import * as moment from 'moment';


@Component({
    selector: 'createOrEditStockOrderStatusModal',
    templateUrl: './createoredit-stockorderstatus-model.component.html'
})
export class CreateOrEditStockOrderStatusModalComponent extends AppComponentBase {
   
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    stockOrderStatus: CreateOrEditStockOrderStatusDto = new CreateOrEditStockOrderStatusDto();

    constructor(
        injector: Injector,
        private _stockOrderStatusServiceProxy: StockOrderStatusServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,

    ) {
        super(injector);
    }
    
    show(stockOrderStatusId?: number): void {
    

        if (!stockOrderStatusId) {
            this.stockOrderStatus = new CreateOrEditStockOrderStatusDto();
            this.stockOrderStatus.id = stockOrderStatusId;

            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Inventory Stock Order Status';
            log.section = 'Inventory Stock Order Status';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._stockOrderStatusServiceProxy.getStockOrderStatusForEdit(stockOrderStatusId).subscribe(result => {
                this.stockOrderStatus = result.stockOrderStatus;


                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Inventory Stock Order Status : ' + this.stockOrderStatus.name;
                log.section = 'Inventory Stock Order Status';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        
    }
    onShown(): void {
        
        document.getElementById('stockOrderStatus_StockOrderStatus').focus();
    }
    save(): void {
            this.saving = true;
            this._stockOrderStatusServiceProxy.createOrEdit(this.stockOrderStatus)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.stockOrderStatus.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Inventory Stock Order Status Updated : '+ this.stockOrderStatus.name;
                    log.section = 'Inventory Stock Order Status';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Inventory Stock Order Status Created : '+ this.stockOrderStatus.name;
                    log.section = 'Inventory Stock Order Status';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }



    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
