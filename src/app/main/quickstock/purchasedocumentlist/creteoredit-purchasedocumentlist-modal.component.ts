import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { CreateOrEditPurchaseDocumentListDto, PurchaseDocumentListServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'createOrEditPurchaseDocumentListModal',
    templateUrl: './creteoredit-purchasedocumentlist-modal.component.html'
})
export class CreateOrEditPurchaseDocumentListModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    allAreas = [];
    purchaseDocumentList: CreateOrEditPurchaseDocumentListDto = new CreateOrEditPurchaseDocumentListDto();

    availableFormats = ['PDF', 'JPG', 'PNG','ZIP','Excel'];
    selectedFormats: string[] = [];

    constructor(
        injector: Injector,
        private _purchaseDocumentListServiceProxy: PurchaseDocumentListServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }

    show(PurchaseDocumentListId?: number): void {
        if (!PurchaseDocumentListId) {
            this.purchaseDocumentList = new CreateOrEditPurchaseDocumentListDto();
            this.purchaseDocumentList.id = PurchaseDocumentListId;
            this.selectedFormats = []; 
            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Inventory Purchase Document List';
            log.section = 'Inventory Purchase Document List';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });

        } else {
            this._purchaseDocumentListServiceProxy.getPurchaseDocumentListForEdit(PurchaseDocumentListId).subscribe(result => {
                this.purchaseDocumentList = result.purchaseDocumentList;
                this.selectedFormats = this.purchaseDocumentList.formats; // Populate selectedFormats with formats from purchase document list
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Inventory Purchase Document List : ' + this.purchaseDocumentList.name;
                log.section = 'Inventory Purchase Document List';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });

            });
        }
    }

    onShown(): void {
        document.getElementById('purchaseDocumentList_purchaseDocumentList').focus();
    }

    save(): void {
        this.purchaseDocumentList.formats = this.selectedFormats;
        this.saving = true;
        this._purchaseDocumentListServiceProxy.createOrEdit(this.purchaseDocumentList)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.purchaseDocumentList.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Inventory Purchase Document List Updated : '+ this.purchaseDocumentList.name;
                    log.section = 'Inventory Purchase Document List';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 

                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Inventory Purchase Document List Created : '+ this.purchaseDocumentList.name;
                    log.section = 'Inventory Purchase Document List';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
