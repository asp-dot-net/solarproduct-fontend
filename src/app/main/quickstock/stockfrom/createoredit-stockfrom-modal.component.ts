import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { CreateOrEditStockFromsDto , StockFromsServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {  } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'createOrEditStockFromModal',
    templateUrl: './createoredit-stockfrom-modal.component.html'
})
export class CreateOrEditStockFromModalComponent extends AppComponentBase {
   
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    id? :number;
    stockfrom:CreateOrEditStockFromsDto= new CreateOrEditStockFromsDto();
    constructor(
        injector: Injector,
        private _stockFromServiceProxy: StockFromsServiceProxy ,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }
    
    show(stockforsmid?: number): void {
        if (!stockforsmid) {
            this.  stockfrom = new CreateOrEditStockFromsDto();
            this.  stockfrom.id =stockforsmid;

            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Inventory Stock From';
            log.section = 'Inventory Stock From';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._stockFromServiceProxy.getStockFromsForEdit(stockforsmid).subscribe(result => {
                this. stockfrom= result.stockfroms;
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Inventory Stock From : ' + this.stockfrom.name;
                log.section = 'Inventory Stock From';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        
    }
    onShown(): void {
        
        document.getElementById('Name').focus();
    }
    save(): void {
        this.saving = true;
        this._stockFromServiceProxy.createOrEdit(this. stockfrom)
         .pipe(finalize(() => { this.saving = false;}))
         .subscribe(() => {
            this.notify.info(this.l('SavedSuccessfully'));
             //result.items[0].stockfroms,
            this.close();
            this.modalSave.emit(null);
            
            if(this.stockfrom.id){
                let log = new UserActivityLogDto();
                log.actionId = 82;
                log.actionNote ='Inventory Stock From Updated : '+ this.stockfrom.name;
                log.section = 'Inventory Stock From';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }else{
                let log = new UserActivityLogDto();
                log.actionId = 81;
                log.actionNote ='Inventory Stock From Created : '+ this.stockfrom.name;
                log.section = 'Inventory Stock From';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }
         });

    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
