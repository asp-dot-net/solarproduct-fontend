import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { CreateOrEditPaymentStatusDto, PaymentStatusServiceProxy,CreateOrEditPurchaseCompanyDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'createOrEditPaymentStatusModal',
    templateUrl: './creteoredit-paymentstatus-modal.component.html'
})
export class CreateOrEditPaymentStatusModalComponent extends AppComponentBase {
   
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    paymentStatus: CreateOrEditPaymentStatusDto = new CreateOrEditPaymentStatusDto();

    constructor(
        injector: Injector,
        private _paymentStatusServiceProxy: PaymentStatusServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }
    
    show(paymentStatusId?: number): void {
    

        if (!paymentStatusId) {
            this.paymentStatus = new CreateOrEditPurchaseCompanyDto();
            this.paymentStatus.id = paymentStatusId;

            this.active = true;
            this.modal.show();
            
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Inventory Payment Status';
            log.section = 'Inventory Payment Status';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });

        } else {
            this._paymentStatusServiceProxy.getPaymentStatusForEdit(paymentStatusId).subscribe(result => {
                this.paymentStatus = result.paymentStatus;


                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Inventory Payment Status : ' + this.paymentStatus.name;
                log.section = 'Inventory Payment Status';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        
    }
    onShown(): void {
        
        document.getElementById('paymentStatus_paymentStatus').focus();
    }
    save(): void {
            this.saving = true;
            this._paymentStatusServiceProxy.createOrEdit(this.paymentStatus)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);

                if(this.paymentStatus.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Inventory Payment Status Updated : '+ this.paymentStatus.name;
                    log.section = 'Inventory Payment Status';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Inventory Payment Status Created : '+ this.paymentStatus.name;
                    log.section = 'Inventory Payment Status';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }



    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
