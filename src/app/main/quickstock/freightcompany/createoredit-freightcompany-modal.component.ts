import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { CreateOrEditFreightCompanyDto, FreightCompanyServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'createOrEditFreightCompanyModal',
    templateUrl: './createoredit-freightcompany-modal.component.html'
})
export class CreateOrEditFreightCompanyModalComponent extends AppComponentBase {
   
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    saving = false;
    freightCompany: CreateOrEditFreightCompanyDto = new CreateOrEditFreightCompanyDto();

    constructor(
        injector: Injector,
        private _freightCompanyServiceProxy: FreightCompanyServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }
    
    show(freightCompanyId?: number): void {
    

        if (!freightCompanyId) {
            this.freightCompany = new CreateOrEditFreightCompanyDto();
            this.freightCompany.id = freightCompanyId;
            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New  Freight Company';
            log.section = 'Inventory Freight Company';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._freightCompanyServiceProxy.getFreightCompanyForEdit(freightCompanyId).subscribe(result => {
                this.freightCompany = result.freightCompany;
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit  Freight Company : ' + this.freightCompany.name;
                log.section = 'Inventory Freight Company';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        
    }
    onShown(): void {
        
        document.getElementById('freightCompany_freightCompany').focus();
    }
    save(): void {
            this.saving = true;
            this._freightCompanyServiceProxy.createOrEdit(this.freightCompany)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.freightCompany.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Freight Company Updated : '+ this.freightCompany.name;
                    log.section = 'Inventory Freight Company';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Freight Company Created : '+ this.freightCompany.name;
                    log.section = 'Inventory Freight Company';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }



    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
