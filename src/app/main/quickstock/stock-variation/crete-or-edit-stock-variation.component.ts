import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { StockVariationsServiceProxy, StockVariationDto,UserActivityLogServiceProxy, UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';

@Component({
    selector: 'createOrEditStockVariationModal',
    templateUrl: './crete-or-edit-stock-variation.component.html'
})
export class CreateOrEditStockVariationModalComponent extends AppComponentBase {
   
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    variation: StockVariationDto = new StockVariationDto();

    constructor(
        injector: Injector,
        private _variationsServiceProxy: StockVariationsServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
    }

    show(variationId?: number): void {

        if (!variationId) {
            this.variation = new StockVariationDto();
            this.variation.id = variationId;
            this.variation.action = "Plus";
            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto(); 
            log.actionId = 79;
            log.actionNote ='Open For Create New Variation';
            log.section = 'Variation';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._variationsServiceProxy.getVariationForEdit(variationId).subscribe(result => {
                this.variation = result;
                

                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Variation : ' + this.variation.name;
                log.section = 'Variation';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        
    }

    onShown(): void {
        
        document.getElementById('StockVariation_Name').focus();
    }

    save(): void {
            this.saving = true;
            this._variationsServiceProxy.createOrEdit(this.variation)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);

                if(this.variation.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Variation Updated : '+ this.variation.name;
                    log.section = 'Variation';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Variation Created : '+ this.variation.name;
                    log.section = 'Variation';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }

             });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
