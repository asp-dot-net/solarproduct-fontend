import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute , Router} from '@angular/router';
import { HoldReasonsServiceProxy, PurchaseCompanyDto, ServiceCategorysServiceProxy, ServiceCategoryDto, ServiceStatusesServiceProxy, ServiceStatusDto, CheckApplicationsServiceProxy, ChcekApplicationDto, PurchaseCompanyServiceProxy  } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { Title } from '@angular/platform-browser';

import {CreateOrEditPurchaseCompanyModalComponent} from './create-or-edit-purchase-company-modal.component';
import {ViewPurchaseCompanyModalComponent} from './view-purchase-company-modal.component';


@Component({
    templateUrl: './purchase-company.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class PurchaseCompanyComponent extends AppComponentBase {
    
    @ViewChild('createOrEditPurchaseCompanyModal', { static: true }) createOrEditPurchaseCompanyModal: CreateOrEditPurchaseCompanyModalComponent;
    @ViewChild('viewPurchaseCompanyModal', { static: true }) viewVendorModal: ViewPurchaseCompanyModalComponent;
    
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    // holdReasonFilter = '';
    firstrowcount = 0;
    last = 0;
    public screenHeight: any;  
    testHeight = 250;
    constructor(
        injector: Injector,
        private _purchaseCompanyServiceProxy: PurchaseCompanyServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,     
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService,
        private titleService: Title
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Purchase Company");
    }
    
 searchLog() : void {
        
    let log = new UserActivityLogDto();
        log.actionId =80;
        log.actionNote ='Searched by Filters';
        log.section = 'Inventory Purchase Company';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
}
ngOnInit(): void {
    this.screenHeight = window.innerHeight; 
   
    let log = new UserActivityLogDto();
    log.actionId =79;
    log.actionNote ='Open Inventory Purchase Company';
    log.section = 'Inventory Purchase Company';
    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {
    });
}
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }
    getpurchasecompany(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._purchaseCompanyServiceProxy.getAll(
            this.filterText,
            // this.holdReasonFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            //  result.items[0].purchaseCompany.name
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createPurchaseCompany(): void {
        this.createOrEditPurchaseCompanyModal.show();        
    }


    deletePurchaseCompany(PurchaseCompanyDto:PurchaseCompanyDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._purchaseCompanyServiceProxy.delete(PurchaseCompanyDto.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete Inventory Purchase Company: ' + PurchaseCompanyDto.name;
                            log.section = 'Inventory Purchase Company';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });

                        });
                }
            }
        );
    }

    exportToExcel(): void {
        this._purchaseCompanyServiceProxy.getPurchaseCompanyToExcel(
        this.filterText,
           
        )
        .subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
         });
    }
    
    
    
    
}
