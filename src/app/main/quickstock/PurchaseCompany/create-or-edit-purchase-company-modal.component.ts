
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {  CreateOrEditPurchaseCompanyDto, CommonLookupServiceProxy, PurchaseCompanyServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { AppConsts } from '@shared/AppConsts';
import { FileUploader, FileItem, FileUploaderOptions } from 'ng2-file-upload';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
@Component({
    selector: 'createOrEditPurchaseCompanyModal',
    templateUrl: './create-or-edit-purchase-company-modal.component.html'
})
export class CreateOrEditPurchaseCompanyModalComponent extends AppComponentBase {
   
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    selectedFile : any;
    purchaseCompany: CreateOrEditPurchaseCompanyDto = new CreateOrEditPurchaseCompanyDto();

    constructor(
        injector: Injector,
        private _purchaseCompanyServiceProxy: PurchaseCompanyServiceProxy,
        private _tokenService: TokenService,  
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _CommonLookupServiceProxy : CommonLookupServiceProxy
    ) {
        super(injector);
    }
    public uploader: FileUploader;
    public maxfileBytesUserFriendlyValue = 5;
    private _uploaderOptions: FileUploaderOptions = {};
    filetoken ='';
    show(purchaseCompanyId?: number): void {
    

        if (!purchaseCompanyId) {
            this.purchaseCompany = new CreateOrEditPurchaseCompanyDto();
            this.purchaseCompany.id = purchaseCompanyId;

            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Inventory Purchase Company';
            log.section = 'Inventory Purchase Company';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._purchaseCompanyServiceProxy.getPurchaseCompanyForEdit(purchaseCompanyId).subscribe(result => {
                this.purchaseCompany = result.purchaseCompany;
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Inventory Purchase Company : ' + this.purchaseCompany.name;
                log.section = 'Inventory Purchase Company';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        this.initFileUploader();
    }
    onShown(): void {
        
        document.getElementById('purchaseCompanys_purchaseCompanys').focus();
    }
    save(): void {
            this.saving = true;
            this.purchaseCompany.fileToken = this.filetoken ?this.filetoken : ""; 

       this.purchaseCompany.logo = this.purchaseCompany.logo ? this.purchaseCompany.logo :"";

            if(!this.purchaseCompany.fileToken && this.purchaseCompany.logo.trim() == ""){
                if( this.purchaseCompany.fileToken.trim() == ""){
                    this.notify.warn("Select Image");
                    this.saving = false;
                    return;
                }
            }
            this._purchaseCompanyServiceProxy.createOrEdit(this.purchaseCompany)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.purchaseCompany.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Inventory Purchase Company Updated : '+ this.purchaseCompany.name;
                    log.section = 'Inventory Purchase Company';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Inventory Purchase Company Created : '+ this.purchaseCompany.name;
                    log.section = 'Inventory Purchase Company';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }

    initFileUploader(): void {
        this.uploader = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Profile/UploadFile' });
        this._uploaderOptions.autoUpload = false;
        this._uploaderOptions.authToken = 'Bearer ' + this._tokenService.getToken();
        this._uploaderOptions.removeAfterUpload = true;
        this.uploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

        this.uploader.onBuildItemForm = (fileItem: FileItem, form: any) => {
            form.append('FileType', fileItem.file.type);
            form.append('FileName', fileItem.file.name);
            form.append('FileToken', this.guid());
        };

        this.uploader.onSuccessItem = (item, response, status) => {
            const resp = <IAjaxResponse>JSON.parse(response);
            if (resp.success) {
                this.filetoken = resp.result.fileToken;
                //this.instfilename.push(resp.result.fileName);
            } else {
                this.message.error(resp.error.message);
            }
        };

        this.uploader.setOptions(this._uploaderOptions);
    }
    imgheight = 0;
    imgWidth = 0;

    fileChangeEvent(event: any): void {
        debugger;
        var url = URL.createObjectURL(event.target.files[0]);
        var img = new Image;

        img.onload = () =>{
            var w = img.width;
            var h = img.height;
            //alert(w + "X"+ h )
        //     if(w != 50 || h != 80){
        //         this.notify.warn("Select Image That Contain Size 1920 X 1000");
        //         this.selectedFile = null;
        //    }
        };
        img.src = url;

        this.uploader.clearQueue();
        this.uploader.addToQueue([<File>event.target.files[0]]);
        this.uploader.uploadAll();

    }
    guid(): string {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }
    downloadfile(file): void {
        let FileName = AppConsts.docUrl + file;

        window.open(FileName, "_blank");

    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}