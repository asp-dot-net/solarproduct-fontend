import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { CreateOrEditCurrenciesDto, CurrenciesServiceProxy} from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import {  } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';


@Component({
    selector: 'createOrEditCurrencyModal',
    templateUrl: './createoredit-currency-Modal.component.html'
})
export class CreateOrEditCurrencyModalComponent extends AppComponentBase {
   
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    currency: CreateOrEditCurrenciesDto = new CreateOrEditCurrenciesDto();

    id? :number;
    constructor(
        injector: Injector,
        private _currrencyServiceProxy: CurrenciesServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,

    ) {
        super(injector);
    }
    
    show(cuurencyid?: number): void {
        if (!cuurencyid) {
            this.currency = new CreateOrEditCurrenciesDto();
            this. currency.id = cuurencyid;

            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Currency';
            log.section = 'Inventory Currency';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        }
         else {
            this._currrencyServiceProxy.getCurrenciesForEdit(cuurencyid).subscribe(result => {
                this.currency = result.currency;
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Currency : ' + this.currency.name;
                log.section = 'Inventory Currency';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
 

            });
        }
        
    }
    onShown(): void {
        
        document.getElementById('Name').focus();
    }
    save(): void {
        this.saving = true;
        this._currrencyServiceProxy.createOrEdit(this.currency)
         .pipe(finalize(() => { this.saving = false;}))
         .subscribe(() => {
            this.notify.info(this.l('SavedSuccessfully'));
            // result.items[0].currency.name,
            this.close();
            this.modalSave.emit(null);

            if(this.currency.id){
                let log = new UserActivityLogDto();
                log.actionId = 82;
                log.actionNote ='Currency Updated : '+ this.currency.name;
                log.section = 'Inventory Currency';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }else{
                let log = new UserActivityLogDto();
                log.actionId = 81;
                log.actionNote ='Currency Created : '+ this.currency.name;
                log.section = 'Inventory Currency';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }

         });

    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
