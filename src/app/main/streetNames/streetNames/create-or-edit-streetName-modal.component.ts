﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { StreetNamesServiceProxy, CreateOrEditStreetNameDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'createOrEditStreetNameModal',
    templateUrl: './create-or-edit-streetName-modal.component.html'
})
export class CreateOrEditStreetNameModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    streetName: CreateOrEditStreetNameDto = new CreateOrEditStreetNameDto();



    constructor(
        injector: Injector,
        private _streetNameServiceProxy: StreetNamesServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }

    onShown(): void {
        document.getElementById('streetName_Name').focus();
    } 

    show(streetNameId?: number): void {

        if (!streetNameId) {
            this.streetName = new CreateOrEditStreetNameDto();
            this.streetName.id = streetNameId;

            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Street Names';
            log.section = 'Street Names';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._streetNameServiceProxy.getStreetNameForEdit(streetNameId).subscribe(result => {
                this.streetName = result.streetName;


                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Street Names : ' + this.streetName.name;
                log.section = 'Street Names';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });

            });
        }
        
    }

    save(): void {
            this.saving = true;

			
            this._streetNameServiceProxy.createOrEdit(this.streetName)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.streetName.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Street Names Updated : '+ this.streetName.name;
                    log.section = 'Street Names';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Street Names Created : '+ this.streetName.name;
                    log.section = 'Street Names';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
