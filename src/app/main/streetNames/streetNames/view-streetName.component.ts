﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { StreetNamesServiceProxy, GetStreetNameForViewDto, StreetNameDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
    selector: 'viewStreetName',
    templateUrl: './view-streetName.component.html',
    animations: [appModuleAnimation()]
})
export class ViewStreetNameComponent extends AppComponentBase implements OnInit {

    active = false;
    saving = false;

    item: GetStreetNameForViewDto;


    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
         private _streetNamesServiceProxy: StreetNamesServiceProxy
    ) {
        super(injector);
        this.item = new GetStreetNameForViewDto();
        this.item.streetName = new StreetNameDto();        
    }

    ngOnInit(): void {
        this.show(this._activatedRoute.snapshot.queryParams['id']);
    }

    show(streetNameId: number): void {
      this._streetNamesServiceProxy.getStreetNameForView(streetNameId).subscribe(result => {      
                 this.item = result;
                this.active = true;
            });       
    }
}
