﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { StreetNamesServiceProxy, CreateOrEditStreetNameDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { ActivatedRoute } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
    selector: 'createOrEditStreetName',
    templateUrl: './create-or-edit-streetName.component.html',
    animations: [appModuleAnimation()]
})
export class CreateOrEditStreetNameComponent extends AppComponentBase implements OnInit {
    active = false;
    saving = false;
    
    streetName: CreateOrEditStreetNameDto = new CreateOrEditStreetNameDto();



    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,        
        private _streetNamesServiceProxy: StreetNamesServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.show(this._activatedRoute.snapshot.queryParams['id']);
    }

    show(streetNameId?: number): void {

        if (!streetNameId) {
            this.streetName = new CreateOrEditStreetNameDto();
            this.streetName.id = streetNameId;

            this.active = true;
        } else {
            this._streetNamesServiceProxy.getStreetNameForEdit(streetNameId).subscribe(result => {
                this.streetName = result.streetName;


                this.active = true;
            });
        }
        
    }

    save(): void {
            this.saving = true;

			
            this._streetNamesServiceProxy.createOrEdit(this.streetName)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
             });
    }







}
