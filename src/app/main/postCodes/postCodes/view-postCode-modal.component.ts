﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetPostCodeForViewDto, PostCodeDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewPostCodeModal',
    templateUrl: './view-postCode-modal.component.html'
})
export class ViewPostCodeModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetPostCodeForViewDto;


    constructor(
        injector: Injector
    ) {
        super(injector);
        this.item = new GetPostCodeForViewDto();
        this.item.postCode = new PostCodeDto();
    }

    show(item: GetPostCodeForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
