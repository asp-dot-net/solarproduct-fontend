﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { PostCodesServiceProxy, GetPostCodeForViewDto, PostCodeDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
    selector: 'viewPostCode',
    templateUrl: './view-postCode.component.html',
    animations: [appModuleAnimation()]
})
export class ViewPostCodeComponent extends AppComponentBase implements OnInit {

    active = false;
    saving = false;

    item: GetPostCodeForViewDto;


    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
         private _postCodesServiceProxy: PostCodesServiceProxy
    ) {
        super(injector);
        this.item = new GetPostCodeForViewDto();
        this.item.postCode = new PostCodeDto();        
    }

    ngOnInit(): void {
        this.show(this._activatedRoute.snapshot.queryParams['id']);
    }

    show(postCodeId: number): void {
      this._postCodesServiceProxy.getPostCodeForView(postCodeId).subscribe(result => {      
                 this.item = result;
                this.active = true;
            });       
    }
}
