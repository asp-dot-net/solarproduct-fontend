﻿import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute , Router} from '@angular/router';
import { PostCodesServiceProxy, PostCodeDto,PostCodeStateLookupTableDto  } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { CreateOrEditPostCodeModalComponent } from './create-or-edit-postCode-modal.component';
import { ViewPostCodeModalComponent } from './view-postCode-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { Title } from '@angular/platform-browser';

@Component({
    templateUrl: './postCodes.component.html',
    styleUrls: ['./postCodes.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class PostCodesComponent extends AppComponentBase {
    
    
    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    toggleBlock(){
        this.show = !this.show;
    };
    toggleBlockChild(){
        this.showchild = !this.showchild;
    };
    @ViewChild('createOrEditPostCodeModal', { static: true }) createOrEditPostCodeModal: CreateOrEditPostCodeModalComponent;
    @ViewChild('viewPostCodeModalComponent', { static: true }) viewPostCodeModal: ViewPostCodeModalComponent;   
      
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    postalCodeFilter = '';
    suburbFilter = '';
    poBoxesFilter = '';
    areaFilter = '';
    stateNameFilter = '';
    allStates: PostCodeStateLookupTableDto[];
    public screenHeight: any;  
    testHeight = 250;

    constructor(
        injector: Injector,
        private _postCodesServiceProxy: PostCodesServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService,
			private _router: Router,
            private titleService: Title
        ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Post Code");
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
    }
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }
    getPostCodes(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._postCodesServiceProxy.getAll(
            this.filterText,
            this.postalCodeFilter,
            this.suburbFilter,
            this.poBoxesFilter,
            this.areaFilter,
            this.stateNameFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    // createPostCode(): void {
    //     // this._router.navigate(['/app/main/postCodes/postCodes/createOrEdit']);        
		
    //     this.createOrEditPostCodeModal.show();   
    // }


    deletePostCode(postCode: PostCodeDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._postCodesServiceProxy.delete(postCode.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    exportToExcel(): void {
        this._postCodesServiceProxy.getPostCodesToExcel(
        this.filterText,
            this.postalCodeFilter,
            this.suburbFilter,
            this.poBoxesFilter,
            this.areaFilter,
            this.stateNameFilter,
        )
        .subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
         });
    }
}
