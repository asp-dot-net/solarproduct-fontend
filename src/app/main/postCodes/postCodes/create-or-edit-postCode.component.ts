﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { PostCodesServiceProxy, CreateOrEditPostCodeDto ,PostCodeStateLookupTableDto
					} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { ActivatedRoute } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
    selector: 'createOrEditPostCode',
    templateUrl: './create-or-edit-postCode.component.html',
    animations: [appModuleAnimation()]
})
export class CreateOrEditPostCodeComponent extends AppComponentBase implements OnInit {
    active = false;
    saving = false;
    
    postCode: CreateOrEditPostCodeDto = new CreateOrEditPostCodeDto();

    stateName = '';

	allStates: PostCodeStateLookupTableDto[];
					
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,        
        private _postCodesServiceProxy: PostCodesServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.show(this._activatedRoute.snapshot.queryParams['id']);
    }

    show(postCodeId?: number): void {

        if (!postCodeId) {
            this.postCode = new CreateOrEditPostCodeDto();
            this.postCode.id = postCodeId;
            this.stateName = '';

            this.active = true;
        } else {
            this._postCodesServiceProxy.getPostCodeForEdit(postCodeId).subscribe(result => {
                this.postCode = result.postCode;

                this.stateName = result.stateName;

                this.active = true;
            });
        }
        this._postCodesServiceProxy.getAllStateForTableDropdown().subscribe(result => {						
						this.allStates = result;
					});
					
    }

    save(): void {
            this.saving = true;

			
            this._postCodesServiceProxy.createOrEdit(this.postCode)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
             });
    }







}
