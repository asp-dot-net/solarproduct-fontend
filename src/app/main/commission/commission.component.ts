import { Component, Injector, ViewEncapsulation, ViewChild, HostListener, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { JobsServiceProxy, LeadsServiceProxy,   CommonLookupDto,  OrganizationUnitDto, CommonLookupServiceProxy, LeadGenerationServiceProxy, UserActivityLogServiceProxy, UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { EntityTypeHistoryModalComponent } from '@app/shared/common/entityHistory/entity-type-history-modal.component';
import * as _ from 'lodash';
import * as moment from 'moment';
import { finalize } from 'rxjs/operators';
// import { AddActivityModalComponent } from '../myleads/myleads/add-activity-model.component';
// import { ViewMyLeadComponent } from '../myleads/myleads/view-mylead.component';
import { Title } from '@angular/platform-browser';
// import { ViewApplicationModelComponent } from '../jobs/jobs/view-application-model/view-application-model.component';
// import { CreateEditMyLeadGenModal } from './create-edit-myLeadGeneration-modal.component';
// import { ItemsList } from '@ng-select/ng-select/lib/items-list';
// import { JobSmsEmailModelComponent } from '../jobs/jobs/job-sms-email-model/job-sms-email-model.component';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { CreateEditcommissionModel } from './create-commission.component';
import { ViewApplicationModelComponent } from '../jobs/jobs/view-application-model/view-application-model.component';

@Component({
    templateUrl: './commission.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class CommissionComponent extends AppComponentBase {

    sectionId: number = 32;
    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    assignLead = 0;
    ExpandedView: boolean = true;
    SelectedLeadId: number = 0;
    filterName = "JobNumber";

    toggle: boolean = true;
    @Output() reloadmyLeadGeneration = new EventEmitter<boolean>();
    change() {
        this.toggle = !this.toggle;
      }

    
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    orgCode = '';

    //@ViewChild('entityTypeHistoryModal', { static: true }) entityTypeHistoryModal: EntityTypeHistoryModalComponent;
    //@ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    // @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
    //@ViewChild('jobBookingsmsemail', { static: true }) jobBookingsmsemail: JobBookingSmsEmailComponent;
    //@ViewChild('viewApplicationModal', { static: true }) viewApplicationModal: ViewApplicationModelComponent;
    // @ViewChild('CreateEditMyLeadGenModal', { static: true }) CreateEditMyLeadGenModal: CreateEditMyLeadGenModal;
    @ViewChild('viewApplicationModal', { static: true }) viewApplicationModal: ViewApplicationModelComponent;

    @ViewChild('createEditcommissionModel', { static: true }) createEditcommissionModel: CreateEditcommissionModel;

    advancedFiltersAreShown = false;
    
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnit = 0;
    organizationUnitlength: number = 0;
    userId : number;
    appointmentFor : number;
    users: CommonLookupDto[];
    appointmentForList: CommonLookupDto[];

    filterText = '';
    dateType = 'InstallDate'
    // StartDate = moment().add(0, 'days').endOf('day');
    // EndDate = moment().add(0, 'days').endOf('day');
    currRoleName = '';
    date = new Date();
    StartDate: moment.Moment = moment(this.date);
    EndDate: moment.Moment = moment(this.date);
    firstrowcount = 0;
    last = 0;

    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 250;
    FiltersData = false;

    selectedRecords = 0;
    assignUserId = 0;
    assignUser: CommonLookupDto[];
    saving = false;
    batteryFilter = 0;
    
    constructor(
        injector: Injector,
        private _commonLookupService: CommonLookupServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _jobsServiceProxy: JobsServiceProxy,
        private titleService: Title,
        private _leadGenerationServiceProxy: LeadGenerationServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _fileDownloadService: FileDownloadService,

    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Commission");
    }

    roleName = ['Leadgen Manager', 'Leadgen SalesRep'];
    

    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
       
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Commission';
            log.section = 'Commission';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.orgCode = this.allOrganizationUnits[0].code;
            // this._commonLookupService.getAllUsersByRoleNameContainsTableDropdown(this.roleName, this.organizationUnit).subscribe(result => {
            //     this.users = result;
            // });
            let assignUserRole = ['Leadgen SalesRep'];

            this._commonLookupService.getAllUsersByRoleNameContainsTableDropdown(assignUserRole,this.allOrganizationUnits[0].id).subscribe(result => {
                this.users = result;
            });
            this.bindUsers(this.allOrganizationUnits[0].id);

            this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
                this.currRoleName = result;
                if(result == "Admin")
                {
                    this.userId = 0;
                }
                else
                {
                    this.userId = this.appSession.user.id;
                }
            });

            this.getLeads();
        });
    }

    bindUsers(orgId) : void {
        let assignUserRole = ['Leadgen SalesRep'];
        this._commonLookupService.getAllUsersByRoleNameContainsTableDropdown(assignUserRole,orgId).subscribe(result => {
            this.users = result;
        });
        this._commonLookupService.getAllUsersByRoleNameContainsTableDropdown(assignUserRole, orgId).subscribe(result => {
            this.assignUser = result;
        });
        this._commonLookupService.getAllUsersByRoleNameContainsTableDropdown(assignUserRole,orgId).subscribe(result => {
            this.users = result;
        });
        this._commonLookupService.getAllUsersForAppointment(orgId).subscribe(result => {
            this.appointmentForList = result;
        });
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }

    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + 82 ;
        }
        else {
            this.testHeight = this.testHeight - 82 ;
        }
    }

    // private setIsEntityHistoryEnabled(): boolean {
    //     let customSettings = (abp as any).custom;
    //     return this.isGrantedAny('Pages.Administration.AuditLogs') && customSettings.EntityHistory && customSettings.EntityHistory.isEnabled && _.filter(customSettings.EntityHistory.enabledEntities, entityType => entityType === this._entityTypeFullName).length === 1;
    // }

    changeOrgCode() {
        this.orgCode = this.allOrganizationUnits.find(x => x.id == this.organizationUnit).code;
    }

    getLeads(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._leadGenerationServiceProxy.getCommissionJobs(
            this.filterName,
            this.organizationUnit,
            filterText_,
            this.userId,
            this.dateType,
            this.StartDate,
            this.EndDate,
            this.appointmentFor,
            this.batteryFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            // this.tableRecords = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            this.shouldShow = false;
        });
    }

    reloadPage(event): void {
        this.ExpandedView = true;
        this.paginator.changePage(this.paginator.getPage());
    }



    
   
    expandGrid() {
        this.ExpandedView = true;
    }
    
    navigateToLeadDetail(leadid): void {
        // debugger;
        // this.ExpandedView = !this.ExpandedView;
        // this.ExpandedView = false;
        // this.SelectedLeadId = leadid;
        // this.viewLeadDetail.showDetail(leadid,null,12);
    }
    
    CreateAppontmentModal(leadId): void {
        // this.CreateEditMyLeadGenModal.show(leadId,this.organizationUnit,32);
     }

    clear() : void {
        this.dateType = 'Assign'
        this.StartDate = moment(this.date);
        this.EndDate = moment(this.date);

        // this.getLeads();
    }

    count = 0;
    oncheckboxCheck() {
        this.count = 0;
        this.primengTableHelper.records.forEach(item => {

            if (item.isSelected == true) {
                this.count = this.count + 1;
            }
        })
    }

    checkAll(ev) {
        this.primengTableHelper.records.forEach(x => x.isSelected = ev.target.checked);
        this.oncheckboxCheck();
    }

    isAllChecked() {
        if (this.primengTableHelper.records)
            return this.primengTableHelper.records.every(_ => _.isSelected);
    }

    submit(): void {
        
        let selectedids: number[] = [];
        
        if (this.userId == 0) {
            this.notify.warn(this.l('PleaseSelectUser'));
            return;
        }

        this.primengTableHelper.records.forEach(function (lead) {
            if (lead.isSelected) {
                selectedids.push(lead.id);
            }
        });
        if (selectedids.length == 0) {
            this.notify.warn('Please Select Apointment');
            return;
        }
        this._leadGenerationServiceProxy.getCommissionAmountByUserId(this.userId).subscribe(result => {
            this.createEditcommissionModel.show(result,selectedids,37,'Commission')

        })
        // if (selectedids.length == 0) {
        //     this.notify.warn(this.l('NoLeadsSelected'));
        //     return;
        // }

        // this.saving = true;
        // this._leadGenerationServiceProxy.transferToSalesRep(this.assignUserId, this.sectionId, selectedids).pipe(finalize(() => { this.saving = false; })).subscribe(() => {
        //     this.notify.success(this.l('AssignedSuccessfully'));
        //     this.getLeads();
        // }, error => {
        //     this.saving = false;
        // });
    }

    exportToExcel(excelorcsv): void {
        // if (this.sampleDateRange != null) {
        //     this.StartDate = this.sampleDateRange[0];
        //     this.EndDate = this.sampleDateRange[1];
        // } else {
        //     this.StartDate = this.sampleDateRange[0];
        //     this.EndDate = this.sampleDateRange[1];
        // }
        //this.excelorcsvfile = excelorcsv;
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._leadGenerationServiceProxy.getCommissionJobsExcel(
            this.filterName,
            this.organizationUnit,
            filterText_,
            this.userId,
            this.dateType,
            this.StartDate,
            this.EndDate,
            this.appointmentFor,
            excelorcsv
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Commission';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Commission';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }

}
