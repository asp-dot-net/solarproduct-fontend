import { Component, ElementRef, Injector, Optional, ViewChild } from '@angular/core';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import {  LeadGenerationServiceProxy ,   UserServiceProxy, CommonLookupServiceProxy , CommonLookupDto, LeadsServiceProxy, CreateCommissionDto, UserActivityLogDto, UserActivityLogServiceProxy} from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import * as moment from 'moment';
import { result } from 'lodash';
@Component({
    selector: 'createEditcommissionModel',
    templateUrl: './create-commission.component.html',
})
export class CreateEditcommissionModel extends AppComponentBase {
    @ViewChild('createEditcommissionModel', { static: true }) modal: ModalDirective;
    @ViewChild("myNameElem") myNameElem: ElementRef;

    ExpandedViewApp: boolean = true;
    active = false; 
    saving = true;  
    leadId = 0;
    leadGenAppointment : CreateCommissionDto = new CreateCommissionDto();
    userList: CommonLookupDto[];
    sectionId : number;
    // roleName = ['Leadgen manager', 'Leadgen SalesRep'];
    //currRoleName = '';
    CommissionDate = moment().add(0, 'days').endOf('day');

    constructor(injector: Injector
        , private _dateTimeService: DateTimeService
        , private _leadGenerationServiceProxy : LeadGenerationServiceProxy
        ,private _userServiceProxy : UserServiceProxy
        ,private _userActivityLogServiceProxy : UserActivityLogServiceProxy
        , private _commonLookupService : CommonLookupServiceProxy
        , private _leadsServiceProxy : LeadsServiceProxy) {
        super(injector);
    }

  SectionName = '';
  show(amount : number, appointmentIds : number[], sectionId? : number, section = '') {
        debugger;
        this.SectionName = section;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Open For Create Appointment';
        log.section = section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
        this.leadGenAppointment = new CreateCommissionDto();
        this.leadGenAppointment.note = "";
        this.leadGenAppointment.commitionDate = moment().add(0, 'days').endOf('day');
        this.leadGenAppointment.amount = amount;
        this.leadGenAppointment.appointmentIds = appointmentIds;
        // this.myNameElem.nativeElement.value = '';

        // this.leadId = leadId;
        this.leadGenAppointment.sectionId = sectionId; 
        // let assignUserRole = ['Leadgen SalesRep'];
        // this._commonLookupService.getAllUsersForAppointment(orgId).subscribe(result => {
        //     this.userList = result;
        // });

       
        this.modal.show();        
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;                
    }

    expandApp() {
        this.ExpandedViewApp = false;
    }

    appointmentSelect(){
        // this.leadGenAppointment.appointmentDate = this.myNameElem.nativeElement.value.toString();
        // this._leadGenerationServiceProxy.checkAppointmentExist(this.leadGenAppointment.appointmentDate, this.leadGenAppointment.appointmentFor).subscribe(result => {
        //     if(result) {
        //         this.notify.warn("Appointment Is Already Exist");
        //         this.leadGenAppointment.appointmentFor= null;
        //         document.getElementById('appointmentFor').focus();
        //     }
        // });
    }

    save(): void {
        this.saving = true;
        // this.leadGenAppointment.leadId = this.leadId;
        // this.leadGenAppointment.sectionId = this.sectionId;
        // this.leadGenAppointment.appointmentDate = this.myNameElem.nativeElement.value.toString();
        // this.leadGenAppointment.commitionDate = this.CommissionDate;
        
        this._leadGenerationServiceProxy.createCommission(this.leadGenAppointment)
            .pipe(finalize(() => { this.saving = false;}))
            .subscribe(() => {
                let log = new UserActivityLogDto();
                log.actionId = 64;
                log.actionNote ='Commission Created';
                log.section = this.SectionName;
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            this.notify.info(this.l('SavedSuccessfully'));
            this.close();
            // this.viewLeadDetail.reloadmyLeadGeneration.emit(null);
        });
    }
}