﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { LeadSourcesServiceProxy, CreateOrEditLeadSourceDto, OrganizationUnitDto, CommonLookupServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';

@Component({
    selector: 'createOrEditLeadSourceModal',
    templateUrl: './create-or-edit-leadSource-modal.component.html'
})
export class CreateOrEditLeadSourceModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    
    isActive = false;
    active = false;
    saving = false;

    LeadSource: CreateOrEditLeadSourceDto = new CreateOrEditLeadSourceDto();

    organizationUnit = [];
    allOrganizationUnits: OrganizationUnitDto[];

    constructor(
        injector: Injector,
        private _leadSourceServiceProxy: LeadSourcesServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy
    ) {
        super(injector);
    }

    show(leadSourceId?: number): void { 
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
        });

        if (!leadSourceId) {
            this.LeadSource = new CreateOrEditLeadSourceDto();
            this.LeadSource.id = leadSourceId;
            this.active = true;
            this.isActive = true;
            setTimeout  (() => {
                this.modal.show();
            },2000);
        } else {
            this._leadSourceServiceProxy.getLeadSourceForEdit(leadSourceId).subscribe(result => {
                this.LeadSource = result.leadSource;
                this.active = true;
                this.isActive = true;
                this.modal.show();
            });
        }    
        
        console.log(this.LeadSource);
    }

    save(): void {
        this.saving = true;

        this._leadSourceServiceProxy.createOrEdit(this.LeadSource)
            .pipe(finalize(() => { this.saving = false;}))
            .subscribe(() => {
            this.notify.info(this.l('SavedSuccessfully'));
            this.close();
            this.modalSave.emit(null);
        });
    }

    onShown(): void {
        
        document.getElementById('LeadSource_Name').focus();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
