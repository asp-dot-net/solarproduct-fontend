﻿import {AppConsts} from '@shared/AppConsts';
import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute , Router} from '@angular/router';
import { LeadSourcesServiceProxy, LeadSourceDto, CommonLookupServiceProxy, OrganizationUnitDto  } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';


import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { CreateOrEditLeadSourceModalComponent } from './create-or-edit-leadSource-modal.component';
import { ViewLeadSourceModalComponent } from './view-leadSource-modal.component';
import { Title } from '@angular/platform-browser';


@Component({
    templateUrl: './leadSources.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class LeadSourcesComponent extends AppComponentBase {
    
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('createOrEditLeadSourceModal', { static: true }) createOrEditLeadSourceModal: CreateOrEditLeadSourceModalComponent;
    @ViewChild('viewLeadSourceModal', { static: true }) viewLeadSourceModal: ViewLeadSourceModalComponent;
    
    advancedFiltersAreShown = false;
    filterText = '';
    nameFilter = '';
    firstrowcount = 0;
    last = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnit = 0;
    organizationUnitlength: number;
    
    public screenHeight: any;  
    testHeight = 250;

    constructor(
        injector: Injector,
        private _leadSourcesServiceProxy: LeadSourcesServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService,
			private _router: Router,
            private titleService: Title,
        private _commonLookupService: CommonLookupServiceProxy,
        ) {
                super(injector);
                this.titleService.setTitle(this.appSession.tenancyName + " |  Lead Sources");
    }

    ngOnInit(): void {
        this._commonLookupService.getOrganizationUnit().subscribe(output => {

            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            
            this.getLeadSources();
        });
        this.screenHeight = window.innerHeight; 
    }
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  

    getLeadSources(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._leadSourcesServiceProxy.getAll(
            this.organizationUnit,
            this.filterText,
            this.nameFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    // createLeadSource(): void {
    //     this._router.navigate(['/app/main/leadSources/leadSources/createOrEdit']);        
    // }


    deleteLeadSource(leadSource: LeadSourceDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._leadSourcesServiceProxy.delete(leadSource.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    exportToExcel(): void {
        this._leadSourcesServiceProxy.getLeadSourcesToExcel(
        this.filterText,
            this.nameFilter,
        )
        .subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
         });
    }
    
    
    
    
    
}
