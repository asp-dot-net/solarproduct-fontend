﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { LeadSourcesServiceProxy, CreateOrEditLeadSourceDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Observable } from "@node_modules/rxjs";





@Component({
    templateUrl: './create-or-edit-leadSource.component.html',
    animations: [appModuleAnimation()]
})
export class CreateOrEditLeadSourceComponent extends AppComponentBase implements OnInit {
    active = false;
    saving = false;

    leadSource: CreateOrEditLeadSourceDto = new CreateOrEditLeadSourceDto();






    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _leadSourcesServiceProxy: LeadSourcesServiceProxy,
        private _router: Router
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.show(this._activatedRoute.snapshot.queryParams['id']);

    }

    show(leadSourceId?: number): void {

        if (!leadSourceId) {
            this.leadSource = new CreateOrEditLeadSourceDto();
            this.leadSource.id = leadSourceId;


            this.active = true;
        } else {
            this._leadSourcesServiceProxy.getLeadSourceForEdit(leadSourceId).subscribe(result => {
                this.leadSource = result.leadSource;



                //this.active = true;
            });
        }


    }

    save(): void {
        
        if (this.leadSource.name != null) {
            if (this.leadSource.isActive != false || this.leadSource.isActive != null) {
                this.saving = true;
                this._leadSourcesServiceProxy.createOrEdit(this.leadSource)
                    .pipe(finalize(() => {
                        this.saving = false;
                    }))
                    .subscribe(x => {
                        this.saving = false;
                        this.notify.info(this.l('SavedSuccessfully'));
                        this._router.navigate(['/app/main/leadSources/leadSources']);
                    })
            } else {
                this.notify.info(this.l('Please Select ISActive'));
                this.saving = false;
            }
        } else {
            this.notify.info(this.l('Please Enter Name'));
            this.saving = false;
        }
    }

    saveAndNew(): void {
        if (this.leadSource.name != null) {
            if (this.leadSource.isActive != false || this.leadSource.isActive != null) {
                this.saving = true;
                this._leadSourcesServiceProxy.createOrEdit(this.leadSource)
                    .pipe(finalize(() => {
                        this.saving = false;
                    }))
                    .subscribe(x => {
                        this.saving = false;
                        this.notify.info(this.l('SavedSuccessfully'));
                        this.leadSource = new CreateOrEditLeadSourceDto();
                    });
            } else {
                this.notify.info(this.l('Please Select ISActive'));
                this.saving = false;
            }
        } else {
            this.notify.info(this.l('Please Enter Name'));
            this.saving = false;
        }
    }













}
