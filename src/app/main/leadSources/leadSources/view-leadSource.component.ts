﻿import {AppConsts} from "@shared/AppConsts";
import { Component, ViewChild, Injector, Output, EventEmitter, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { LeadSourcesServiceProxy, GetLeadSourceForViewDto, LeadSourceDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
    templateUrl: './view-leadSource.component.html',
    animations: [appModuleAnimation()]
})
export class ViewLeadSourceComponent extends AppComponentBase implements OnInit {

    active = false;
    saving = false;

    item: GetLeadSourceForViewDto;


    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
         private _leadSourcesServiceProxy: LeadSourcesServiceProxy
    ) {
        super(injector);
        this.item = new GetLeadSourceForViewDto();
        this.item.leadSource = new LeadSourceDto();        
    }

    ngOnInit(): void {
        this.show(this._activatedRoute.snapshot.queryParams['id']);
    }

    show(leadSourceId: number): void {
      this._leadSourcesServiceProxy.getLeadSourceForView(leadSourceId).subscribe(result => {      
                 this.item = result;
                this.active = true;
            });       
    }
    
    
}
