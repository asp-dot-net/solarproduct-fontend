﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { PostCodeRangeServiceProxy, CreateOrEditPostCodeRangeDto ,UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';


@Component({
    selector: 'createOrEditPostCodeRangeModal',
    templateUrl: './create-or-edit-postcoderange-modal.component.html'
})
export class CreateOrEditPostCodeRangeModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    postCodeRange: CreateOrEditPostCodeRangeDto = new CreateOrEditPostCodeRangeDto();

    constructor(
        injector: Injector,
        private _postCodeRangeServiceProxy: PostCodeRangeServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
    }

    show(postCodeRangeId?: number): void {
        if (!postCodeRangeId) {
            this.postCodeRange = new CreateOrEditPostCodeRangeDto();
            this.postCodeRange.id = postCodeRangeId;
            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Post Code Range';
            log.section = 'Post Code Range';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            // setTimeout  (() => {
            //     this.modal.show();
            // },2000);
        } else {
            this._postCodeRangeServiceProxy.getPostCodeRangeForEdit(postCodeRangeId).subscribe(result => {
                this.postCodeRange = result.postCodeRange;
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Post Code Range : ' + this.postCodeRange.area;
                log.section = 'Post Code Range';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        // this.active = true;
        // this.modal.show();
    }

    onShown(): void {
       document.getElementById('PostCodeRange_StartPostCode').focus();
    }
   
    save(): void {
        this.saving = true;
        this._postCodeRangeServiceProxy.createOrEdit(this.postCodeRange).pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.postCodeRange.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Post Code Range Updated : '+ this.postCodeRange.area;
                    log.section = 'Post Code Range';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Post Code Range Created : '+ this.postCodeRange.area;
                    log.section = 'Post Code Range';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
            },
            error => {
                this.saving = false;
            }
        );
        
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
