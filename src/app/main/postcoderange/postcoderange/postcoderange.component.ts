﻿import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy,UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { CreateOrEditPostCodeRangeModalComponent } from './create-or-edit-postcoderange-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { Title } from '@angular/platform-browser';
import {
    PostCodeRangeServiceProxy, 
    PostCodeRangeDto,
    CreateOrEditPostCodeRangeDto
} from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
@Component({
    templateUrl: './postcoderange.component.html',
    styleUrls: ['./postcoderange.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class PostCodeRangeComponent extends AppComponentBase {

    @ViewChild('createOrEditPostCodeRangeModal', { static: true }) createOrEditPostCodeRangeModal: CreateOrEditPostCodeRangeModalComponent;

    show: boolean = true;
    showchild: boolean = true;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    
    advancedFiltersAreShown = false;
    filterText = '';
    firstrowcount = 0;
    last = 0;
    public screenHeight: any;  
    testHeight = 280;
    postCodeRange: CreateOrEditPostCodeRangeDto = new CreateOrEditPostCodeRangeDto();

    constructor(
        injector: Injector,
        private _postCodeRangeServiceProxy: PostCodeRangeServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private titleService: Title
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Post Code Range");
    }
    searchLog() : void {
        debugger;
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Post Code Range';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Post Code Range';
        log.section = 'Post Code Range';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
    }
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }
    getPostCodeRange(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._postCodeRangeServiceProxy.getAll(
            this.filterText,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();

        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    deletePostCodeRange(postCodeRange: PostCodeRangeDto): void {
        this.message.confirm(
            this.l('AreYouSureWanttoDelete'),
            this.l('Delete'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._postCodeRangeServiceProxy.delete(postCodeRange.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete Post Code Range: ' + postCodeRange.area;
                            log.section = 'Post Code Range';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });
                        });
                }
            }
        );
    }
}
