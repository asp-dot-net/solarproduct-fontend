import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { UserActivityLogDto, UserActivityLogServiceProxy, CreateorEditDocumentTypeDto, DocumentTypeServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'createOrEditDocumentTypeModal',
    templateUrl: './create-or-edit-documenttype-modal.component.html'
})
export class CreateOrEditDocumentTypeModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    saving = false;

    documenttype: CreateorEditDocumentTypeDto = new CreateorEditDocumentTypeDto();

    constructor(
        injector: Injector,
        private _documenttypeServiceProxy: DocumentTypeServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
    }

    show(documenttypeId?: number): void {

        if (!documenttypeId) {
            this.documenttype = new CreateorEditDocumentTypeDto();
            this.documenttype.id = documenttypeId;
            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Document Type';
            log.section = 'Document Type';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
        } else {
            this._documenttypeServiceProxy.getDepartmentForEdit(documenttypeId).subscribe(result => {
                this.documenttype = result.documenttypeDto;
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Document Type : ' + this.documenttype.title;
                log.section = 'Document Type';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            });
        }
    }

    onShown(): void {
        
        document.getElementById('Department_Name').focus();
    }

    save(): void {
        this.saving = true;


        this._documenttypeServiceProxy.createOrEdit(this.documenttype)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.documenttype.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Document Type Updated : '+ this.documenttype.title;
                    log.section = 'Document Type';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Document Type Created : '+ this.documenttype.title;
                    log.section = 'Document Type';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
            });
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
