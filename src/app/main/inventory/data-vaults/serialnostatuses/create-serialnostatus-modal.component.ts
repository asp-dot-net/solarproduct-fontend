import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {  CreateOrEditSerialNoStatusDto, SerialNoStatusServiceProxy,UserActivityLogServiceProxy,UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'CreateOrEditSerialNoModal',
    templateUrl: './create-serialnostatus-modal.component.html'
})
export class CreateOrEditSerialNoModalComponent extends AppComponentBase {
   
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    SerialNoStatus: CreateOrEditSerialNoStatusDto = new CreateOrEditSerialNoStatusDto();

    constructor(
        injector: Injector,
        private _SerialNoStatusServiceProxy: SerialNoStatusServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }
    
    show(Id?: number): void {
    

       
            this._SerialNoStatusServiceProxy.getSerialNoStatusForEdit(Id).subscribe(result => {
                this.SerialNoStatus = result.serialNoStatus;
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit SerialNo Status : ' + this.SerialNoStatus.status;
                log.section = 'SerialNo Status';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 

            });
        }
        onShown(): void {
        
            document.getElementById('CheckApplications_CheckApplications').focus();
        }
        save(): void {
                this.saving = true;
                this._SerialNoStatusServiceProxy.createOrEdit(this.SerialNoStatus)
                 .pipe(finalize(() => { this.saving = false;}))
                 .subscribe(() => {
                    this.notify.info(this.l('SavedSuccessfully'));
                    this.close();
                    this.modalSave.emit(null);
                   
                        let log = new UserActivityLogDto();
                        log.actionId = 82;
                        log.actionNote ='SerialNo Status Updated : '+ this.SerialNoStatus.status;
                        log.section = 'SerialNo Status';
                        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                            .subscribe(() => {
                        }); 
                    
                 });
        }
    
    
        close(): void {
            this.active = false;
            this.modal.hide();
        }   
}
   

