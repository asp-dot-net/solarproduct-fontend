import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserActivityLogDto, CreateOrEditSerialNoStatusDto, SerialNoStatusServiceProxy, SerialNoStatusDto ,UserActivityLogServiceProxy} from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { Title } from '@angular/platform-browser';
import { CreateOrEditSerialNoModalComponent } from './create-serialnostatus-modal.component';
import { finalize } from 'rxjs/operators';
@Component({
    templateUrl: './serialnoStatus.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class SerialNoStatusComponent extends AppComponentBase {
   
  
  @ViewChild('CreateOrEditSerialNoModal', { static: true }) CreateOrEditSerialNoModal: CreateOrEditSerialNoModalComponent;

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    saving = false;
    advancedFiltersAreShown = false;
    filterText = '';
    nameFilter = '';
    firstrowcount = 0;
    last = 0;
    public screenHeight: any;  
    testHeight = 250;
     SerialNoStatus: CreateOrEditSerialNoStatusDto = new CreateOrEditSerialNoStatusDto();
    constructor(
        injector: Injector,
        private _SerialNoStatusServiceProxy: SerialNoStatusServiceProxy,
         private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        
        private titleService: Title
        ) {
            super(injector);
            this.titleService.setTitle("SalesDrive | SerialNo Status");
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        let log = new UserActivityLogDto();
            log.actionId =79;
            log.actionNote ='Open SerialNo Status';
            log.section = 'SerialNo Status';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }
 
    searchLog() : void {
        debugger;
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'SerialNo Status';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    getSerialNoStatus(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._SerialNoStatusServiceProxy.getAll(
            this.filterText,
           
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
             // result.items[0].serialNoStatus.id
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }
    edit()
    {

    }
    delete(SerialNoStatusDto: SerialNoStatusDto): void {
        this.message.confirm(
            this.l('AreYouSureWanttoDelete'),
            this.l('Delete'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._SerialNoStatusServiceProxy.delete(SerialNoStatusDto.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            let log = new UserActivityLogDto();
                                                        log.actionId = 83;
                                                        log.actionNote ='Delete SerialNo Status: ' + SerialNoStatusDto.status;
                                                        log.section = 'SerialNo Status';
                                                        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                                            .subscribe(() => {
                                                        }); 
                        });
                }
            }
        );
    }

   
}
