import { Component, Injector, ViewChild } from '@angular/core';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AuditLogListDto } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
    selector: 'CreateStockOrderForModal',
    templateUrl: './create-stock-order-modal.html',
})
export class CreateStockOrderForModal extends AppComponentBase {
    @ViewChild('CreateStockOrderForModal', { static: true }) modal: ModalDirective;

    ExpandedViewApp: boolean = true;
    active = false;    

    constructor(injector: Injector, private _dateTimeService: DateTimeService) {
        super(injector);
    }

    show() {
        this.modal.show();        
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;                
    }
    expandApp() {
        this.ExpandedViewApp = false;
    }
}
