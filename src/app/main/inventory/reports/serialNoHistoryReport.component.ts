
import { Component, Injector, ViewEncapsulation, ViewChild, OnInit, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { StockOrderServiceProxy,CommonLookupDto,CommonLookupServiceProxy ,GetAllSerialNoHistoryReportDto,OrganizationUnitDto,GetAllStockOrderOutputDto, StockOrderReceivedReportServiceProxy, SerialNoHistoryReportServiceProxy} from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';

import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { DomSanitizer, SafeHtml, Title } from '@angular/platform-browser';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';

@Component({
    templateUrl: './serialNoHistoryReport.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class SerialNoHistoryReportComponent extends AppComponentBase implements OnInit {
   
    
    FiltersData = false;
    testHeight = 330;
    shouldShow: boolean = false;
    organizationUnitlength: number = 0;
    flag = true;
    show: boolean = true;
    showchild: boolean = true;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    toggle: boolean = true;
    showCancel: boolean = false;
    ExpandedView: boolean = true;
    change() {
        this.toggle = !this.toggle;
      }
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
   
   
    advancedFiltersAreShown = false;
    SerialNoStatusIds = [];
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnit = 0;
    changeOrganization = 0;
    date = new Date();
    firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    StartDate = moment().startOf('month').add(1, 'days');
    EndDate = moment().add(0, 'days').endOf('day');
    dateFilterType  = undefined;
    filterText = '';
    
    filterName ="ItemName";
    firstrowcount = 0;
    last = 0;
    summary: GetAllSerialNoHistoryReportDto[];
    allSerialNoStatus: CommonLookupDto[];
    allPurchaseCompany: CommonLookupDto[];
    allStockFrom :CommonLookupDto[];
    allLocation: CommonLookupDto[];
    productTypes: CommonLookupDto[];
    public screenWidth: any;  
    public screenHeight: any;  
    

    constructor(
        injector: Injector,
        private _serialNoHistoryReportServiceProxy: SerialNoHistoryReportServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private titleService: Title,
       
    ) {
        super(injector);
        
    }
    searchLog() : void {
        
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'SerialNo History Report';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
  
   
    ngOnInit(): void {
     
        this.screenHeight = window.innerHeight;
        this.titleService.setTitle(this.appSession.tenancyName + " |  SerialNo History Report");
  
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Open SerialNo History Report';
        log.section = 'SerialNo History Report';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
       

        this._commonLookupService.getAllSerialNoStatus().subscribe(result => {
            this.allSerialNoStatus = result;
        })
       
    }
          
  
    expandGrid() {
        this.ExpandedView = true;
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    
    
    getStockOrder(event?: LazyLoadEvent) {
        debugger;
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
      
        this.primengTableHelper.showLoadingIndicator();

        this._serialNoHistoryReportServiceProxy.getAll(
            
            this.filterName,
            this.filterText,
            this.SerialNoStatusIds,
            this.dateFilterType,
            this.StartDate,
            this.EndDate,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.summary=result.items;
            
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            
            
        });
    }
    
   

}
