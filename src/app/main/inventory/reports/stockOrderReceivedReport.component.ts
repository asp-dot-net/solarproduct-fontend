
import { Component, Injector, ViewEncapsulation, ViewChild, OnInit, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { StockOrderServiceProxy,CommonLookupDto,CommonLookupServiceProxy ,GetAllStockOrderReceivedReportDto,OrganizationUnitDto,GetAllStockOrderOutputDto, StockOrderReceivedReportServiceProxy} from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';

import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { DomSanitizer, SafeHtml, Title } from '@angular/platform-browser';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';

@Component({
    templateUrl: './stockOrderReceivedReport.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class StockOrderReceivedComponent extends AppComponentBase implements OnInit {
   
    
    FiltersData = false;
    testHeight = 330;
    shouldShow: boolean = false;
    organizationUnitlength: number = 0;
    flag = true;
    show: boolean = true;
    showchild: boolean = true;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    toggle: boolean = true;
    showCancel: boolean = false;
    ExpandedView: boolean = true;
    change() {
        this.toggle = !this.toggle;
      }
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
   
   
    advancedFiltersAreShown = false;
    StockOrderStatusIds = [];
    StateFitlerIds = [];
    PurchaseCompanyIdFiletr  :number = undefined;
    OrderTypeIdFiletr :number = undefined;
    ProductTypeIdFiletr  :number = undefined;
   
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnit = 0;
    changeOrganization = 0;
    date = new Date();
    firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    StartDate = moment().startOf('month').add(1, 'days');
    EndDate = moment().add(0, 'days').endOf('day');
    dateFilterType  = undefined;
    filterText = '';
    
    filterName ="ItemName";
    firstrowcount = 0;
    last = 0;
    summary: GetAllStockOrderReceivedReportDto[];
    allStockOrderStatus: CommonLookupDto[];
    allPurchaseCompany: CommonLookupDto[];
    allStockFrom :CommonLookupDto[];
    allLocation: CommonLookupDto[];
    productTypes: CommonLookupDto[];
    public screenWidth: any;  
    public screenHeight: any;  
    

    constructor(
        injector: Injector,
        private _StockOrderReceivedReportServiceProxy: StockOrderReceivedReportServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private titleService: Title,
       
    ) {
        super(injector);
        
    }
    searchLog() : void {
        
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Stock Received Report';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
  
   
    ngOnInit(): void {
     
        this.screenHeight = window.innerHeight;
        this.titleService.setTitle(this.appSession.tenancyName + " |  Stock Order Received Report");
     
        
        
        this._commonLookupService.getStockOrderStatusDropdown().subscribe(result => {
            this.allStockOrderStatus = result;
        })
        this._commonLookupService.getPurchaseCompanyDropdown().subscribe(result => {
            this.allPurchaseCompany = result;
        })
        this._commonLookupService.getStockOrderFromOutsideDropdown().subscribe(result => {
            this.allStockFrom = result;
        })
        this._commonLookupService.getLocationDropdown().subscribe(result => {
            this.allLocation = result;
            // this.StateFitlerIds = this.allLocation.map(location => location.id);
            this.StateFitlerIds=[1,2,4,5]
            this.getStockOrder();
        })
        this._commonLookupService.getAllProductTypeForTableDropdown().subscribe(result => {
            this.productTypes = result;
        });
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            
            this.getStockOrder();
        });
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Open Stock Received Report';
        log.section = 'Stock Received Report';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
    }
  
    expandGrid() {
        this.ExpandedView = true;
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    
    isStateSelected(stateId: number): boolean {
        return this.StateFitlerIds.includes(stateId);
    }
    getStockOrder(event?: LazyLoadEvent) {
        debugger;
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
      
        this.primengTableHelper.showLoadingIndicator();

        this._StockOrderReceivedReportServiceProxy.getAll(
            
            this.filterName,
            this.filterText,
            this.PurchaseCompanyIdFiletr,
            this.ProductTypeIdFiletr,
            this.StockOrderStatusIds,
            this.OrderTypeIdFiletr,
            this.StateFitlerIds,
            this.organizationUnit,
            this.dateFilterType,
            this.StartDate,
            this.EndDate,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.summary=result.items;
            
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            
            
        });
    }
    
    // checkColumnVisibility(records: GetAllStockOrderReceivedReportDto[]) {
    //     // Reset all flags
    //     this.showBrisbane = this.showMelbourne = this.showSydney = 
    //     this.showPerth = this.showDarwin = this.showHobart = 
    //     this.showAdelaide = this.showCanberra = true;
    
    //     if (records && records.length) {
    //         this.showBrisbane = !records.every(record => 
    //             record.stockOrderReceivedReport.every(r => r.brisbane === 0)
    //         );
    //         this.showMelbourne = !records.every(record => 
    //             record.stockOrderReceivedReport.every(r => r.melbourne === 0)
    //         );
    //         this.showSydney = !records.every(record => 
    //             record.stockOrderReceivedReport.every(r => r.sydeny === 0)
    //         );
    //         this.showPerth = !records.every(record => 
    //             record.stockOrderReceivedReport.every(r => r.perth === 0)
    //         );
    //         this.showDarwin = !records.every(record => 
    //             record.stockOrderReceivedReport.every(r => r.darwin === 0)
    //         );
    //         this.showHobart = !records.every(record => 
    //             record.stockOrderReceivedReport.every(r => r.hobart === 0)
    //         );
    //         this.showAdelaide = !records.every(record => 
    //             record.stockOrderReceivedReport.every(r => r.adelaide === 0)
    //         );
    //         this.showCanberra = !records.every(record => 
    //             record.stockOrderReceivedReport.every(r => r.canberra === 0)
    //         );
    //     }
    // }
    

}
