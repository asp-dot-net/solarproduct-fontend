import { Component, ViewChild, Injector, Output, EventEmitter, OnInit} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { QuotationsServiceProxy, StockOrderDto, CommonLookupServiceProxy,GetStockOrderForViewDto, UserActivityLogDto, UserActivityLogServiceProxy, StockOrderServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import * as _ from 'lodash';

@Component({
    selector: 'viewPoModel',
    templateUrl: './view-po.component.html'
})
export class ViewPoModalComponent extends AppComponentBase implements OnInit {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    purchaseOrder: GetStockOrderForViewDto;
    saving = false;
    purchaseOrderId:number = 0;
    quoteId:number = 0;
    leadId:number = 0;
    ngOnInit(): void {
      }
    constructor(
        injector: Injector,
        private _stockorderServiceProxy: StockOrderServiceProxy,
        private sanitizer: DomSanitizer,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _commonLookupServiceProxy: CommonLookupServiceProxy
    ) {
        super(injector);
        this.purchaseOrder = new GetStockOrderForViewDto();
        this.purchaseOrder.purchaseOrder = new StockOrderDto();

    }

    htmlContentBody: any;
    SectionName = '';

    show(purchaseOrderId:number,Section = ''): void {
        this.purchaseOrderId = purchaseOrderId;
       
        this.spinnerService.show();
        this.SectionName = Section;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote = 'View PO';
        log.section = Section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
        this._stockorderServiceProxy.getPoDataByPurchaseOrderId(purchaseOrderId).subscribe(result => {
             
            console.log(result);
            this.purchaseOrder = result;

            this.setHtml(this.purchaseOrder.purchaseOrder.viewHtml);
            //this.setHtml(test);

            this.spinnerService.hide();
            this.modal.show();
        },
        err => {
            this.spinnerService.hide();
        });
        
    }
    setHtml(emailHTML) {
        let htmlTemplate = this.getPO(emailHTML)
        this.htmlContentBody = htmlTemplate;
        this.htmlContentBody = this.sanitizer.bypassSecurityTrustHtml(this.htmlContentBody)
        
    }
    getPO(emailHTML) {
        // debugger;
        let myTemplateStr = emailHTML;
        let TableList = [];
        let subtotal = 0; 
        if (this.purchaseOrder.purchaseOrderItemList != null) {
            this.purchaseOrder.purchaseOrderItemList.forEach(obj => {
               
                TableList.push("<tr><td>" + obj.productType + "</td><td>" + obj.productItem + "</td><td>" + obj.modelNo + "</td><td>" + obj.expiryDatee  + "</td><td>" + obj.quantity  + "</td><td>" + obj.pricePerWatt + "</td><td>" + obj.unitRate + "</td><td>" + obj.amount + "</td></tr>");
                subtotal += obj.amount; 
            })
        }
        let gstAmount = (subtotal * 0.10).toFixed(2);
        let fTotal = (subtotal + parseFloat(gstAmount)).toFixed(2);
        let myVariables = {
            R: {
                OrderNo: this.purchaseOrder.purchaseOrder.orderNo,
                Date: this.purchaseOrder.purchaseOrder.orderDatee,
                AddtionalNote: this.purchaseOrder.purchaseOrder.additionalNotes,
                TransportCompany: this.purchaseOrder.purchaseOrder.purchaseCompany,
                WareHouseLocation : this.purchaseOrder.purchaseOrder.warehouseLocation,
                FullAddress:this.purchaseOrder.purchaseOrder.vendorAddress,
                PaymentTerm: this.purchaseOrder.purchaseOrder.paymentTerm,
                Currency: this.purchaseOrder.purchaseOrder.currency,
                CompanyName:this.purchaseOrder.purchaseOrder.vendor,
                ProductItemList: TableList.toString().replace(/,/g, ''),
                Subtotal  : subtotal ,
                GstAmount : gstAmount,
                Ftotal    : fTotal
                
            }
        }
        _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;
    
        
        let compiled = _.template(myTemplateStr);
        let myTemplateCompiled = compiled(myVariables);
        return myTemplateCompiled;
    }

    close(): void {
        this.modal.hide();
    }

    htmlstr: string = '';
    generatePdf(): void {
     debugger
        this.saving = true;
        
        //this.spinner.show();
        this.spinnerService.show();
        this.htmlstr = this.htmlContentBody.changingThisBreaksApplicationSecurity;
        this._commonLookupServiceProxy.downloadPdf(this.purchaseOrder.purchaseOrder.orderNo.toString(),"PurchaseOrder", this.htmlstr).subscribe(result => {
            let FileName = result;
            this.saving = false;
            this.spinnerService.hide();
            window.open(result, "_blank");
        },
        err => {
            this.spinnerService.hide();
        });
    }

    transform(value:string): string {
        let first = value.substr(0,1).toUpperCase();
        return first + value.substr(1); 
    }
}