import { Component, ViewChild, Injector, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { LeadsServiceProxy, GetPurchaseOrderActivityLogViewDto, CallHistoryServiceProxy, GetActivityLogViewDto, StockOrderServiceProxy, StockOrderDto, OrganizationUnitMapDto, GetCallHistoryViewDto, GetStockOrderForViewDto, CommonLookupServiceProxy, WholeSaleLeadServiceProxy, CreateOrEditWholeSaleLeadDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import PlaceResult = google.maps.places.PlaceResult;
import { NgxSpinnerService } from 'ngx-spinner';
import { DatePipe } from '@angular/common';
import { AppConsts } from '@shared/AppConsts';
import {  TokenService } from 'abp-ng2-module';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { FileUpload } from 'primeng/fileupload';
import{PurchaseOrderActivityLogComponent} from  './purchase-order-activity-log.component';
@Component({
    selector: 'ViewPurchaseOrderDetail',
    templateUrl: './purchase-order-details.component.html',
    animations: [appModuleAnimation()],
    styleUrls: ['./purchase-order-details.component.css']
})
export class ViewPurchaseOrderDetailComponent extends AppComponentBase implements OnInit {
  @ViewChild('purchaseOrderactivityLog', { static: true }) purchaseOrderactivityLog: PurchaseOrderActivityLogComponent;
    @ViewChild('ExcelFileUpload', { static: false }) excelFileUpload: FileUpload;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @Input() SelectedPurtchaseOrderId: number = 0;
    @Output() reloadLead = new EventEmitter<boolean>();
    active = false;
    saving = false;
    purchaseOrder: GetStockOrderForViewDto = new GetStockOrderForViewDto();
    activityLog: GetPurchaseOrderActivityLogViewDto[];
    activeTabIndex: number = 0;
    activityDatabyDate: any[] = [];
    actionId: number = 0;
    activity: boolean = false;
    PurchaseOrderActivityList: GetPurchaseOrderActivityLogViewDto[];
    lat: number;
    lng: number;
    zoom: 20;
    showforresult = false;
    name: string;
    role: string = '';
    mainsearch = false;
    sectionId = 0;
    showsectionwise = false;
    PurchaseOrderId = 0;
    OrganizationMapDto: OrganizationUnitMapDto[];
    mapSrc: string = "";
    PurchaseOrderItemList: any[];
    selectedMap: string;
    path = AppConsts.docUrl;
    uploadUrlstc: string;
    organizationUnit = 0;
    isSubmit:boolean;
    constructor(
        injector: Injector,
        private _stockOrderServiceProxy: StockOrderServiceProxy,
        private spinner: NgxSpinnerService,
        public datepipe: DatePipe,
        private _tokenService: TokenService,
        private _httpClient: HttpClient,
        private _callHistoryService: CallHistoryServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
         //this.item = new GetWholeSaleLeadForViewDto();
        // this.item.createOrEditWholeSaleLeadDto = new CreateOrEditWholeSaleLeadDto();
          this.purchaseOrder= new GetStockOrderForViewDto();
         this.purchaseOrder.purchaseOrder = new StockOrderDto();
         this.uploadUrlstc = AppConsts.remoteServiceBaseUrl + '/Users/ImportPurchaseOrderItemFromExcel';
    }

    ngOnInit(): void {
        if (this.SelectedPurtchaseOrderId > 0)
            this.showDetail(this.SelectedPurtchaseOrderId);
        // this.registerToEvents();
    }

    showDetail(purchaseOrderId: number, PurchaseOrderName?: string, sectionId?: number): void {
        debugger;
        this.PurchaseOrderId = purchaseOrderId;
        this.SelectedPurtchaseOrderId = purchaseOrderId;
        this.sectionId = sectionId;
        this.name = PurchaseOrderName;
    
        if (purchaseOrderId != null) {
            this.refreshPurchaseOrderData();
        }
         
    }
    
    refreshPurchaseOrderData(): void {
        this.spinner.show();
        this._stockOrderServiceProxy.getStockOrderById(this.PurchaseOrderId).subscribe(result => {
            this.purchaseOrder = result;
            this.PurchaseOrderItemList = [];
            this.purchaseOrder.purchaseOrderItemList.map((item) => {
                let PurchaseOrderItemCreateOrEdit = { id: 0, productTypeId: 0, productType: " ", productItemId: 0, productItemName: "", quantity: 0, pendingQty: 0,uploadedQty: 0, pricePerWatt: 0, unitRate: 0, estRate: 0, amount: 0, productModel: "", expiryDate: null, isApp: false };
                PurchaseOrderItemCreateOrEdit.id = item.id;
                
                PurchaseOrderItemCreateOrEdit.productTypeId = item.productTypeId;
                PurchaseOrderItemCreateOrEdit.productItemId = item.productItemId;
                PurchaseOrderItemCreateOrEdit.productItemName = item.productItem;
                PurchaseOrderItemCreateOrEdit.productType = item.productType;
                PurchaseOrderItemCreateOrEdit.quantity = item.quantity;
                PurchaseOrderItemCreateOrEdit.pendingQty = item.pendingQuantity;
                PurchaseOrderItemCreateOrEdit.uploadedQty=item.uploadedQuantity;
                PurchaseOrderItemCreateOrEdit.pricePerWatt = item.pricePerWatt;
                PurchaseOrderItemCreateOrEdit.unitRate = item.unitRate;
                PurchaseOrderItemCreateOrEdit.amount = item.amount;
                PurchaseOrderItemCreateOrEdit.estRate = item.estRate;
                PurchaseOrderItemCreateOrEdit.productModel = item.modelNo;
                PurchaseOrderItemCreateOrEdit.expiryDate = item.expiryDate;
                PurchaseOrderItemCreateOrEdit.isApp = item.isApp;
                this.PurchaseOrderItemList.push(PurchaseOrderItemCreateOrEdit);
            });
            this.purchaseOrderactivityLog.show(this.PurchaseOrderId, 0, this.sectionId);
            this.spinner.hide();

            let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Open Detail By Click on ' + result.purchaseOrder.orderNo;
        log.section = "Stock Order";
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
        });

    }
    
    myHistory: boolean = false;
    callType: any = '';
    callHistories: GetCallHistoryViewDto[];

    cancel(lead: CreateOrEditWholeSaleLeadDto): void {

        abp.event.trigger('app.show.cancelLeadModal', lead.id);  
    }
    reject(id): void {
        abp.event.trigger('app.show.rejectLeadModal', id);
    }
    registerToEvents() {
        abp.event.on('app.onCancelModalSaved', () => {
            debugger;
            this.showDetail(this.SelectedPurtchaseOrderId);
        });
    }
    getCallHistory()
    {
        
            this._callHistoryService.getPurchaseOrderCallHistory(this.PurchaseOrderId, this.callType, this.myHistory).subscribe((result) => {
                this.callHistories = result;
            });
       
    }
    addActivitySuccess() {

        this.showDetail(this.SelectedPurtchaseOrderId);
    }
    
    
    uploadExcelstc(Files : any, id: number, warehouseLocationId: number): void {
        debugger;
        var headers_object = new HttpHeaders().set("Authorization", "Bearer " + this._tokenService.getToken());

        const httpOptions = {
            headers: headers_object
        };
        const formData: FormData = new FormData();
        const file = <File>Files.target.files[0];
        formData.append('file' + ',' + this.organizationUnit, file, file.name);
        formData.append('PurchaseOrderId', this.PurchaseOrderId.toString());
        formData.append('ProductItemId', id.toString());
        formData.append('WarehouseLocationId', warehouseLocationId.toString());
        this._httpClient
            .post<any>(this.uploadUrlstc, formData, httpOptions)
            // .pipe(finalize(() => this.excelFileUpload.clear()))
            .subscribe(response => {
                if (response.success) {
                    this.notify.success(this.l('ImportSerialNoProcessStart'));
                } else if (response.error != null) {
                    this.notify.error(this.l('ImportSerialNoUploadFailed'));
                }
            });
    }
    revertAllSerialNumbers(purchaseOrderId: number, productItemId: number): void {
        this.spinner.show();
        
        this._stockOrderServiceProxy.revertAllSerialNo(purchaseOrderId, productItemId)
            .pipe(finalize(() => this.spinner.hide()))
            .subscribe(() => {
                this.notify.success(this.l('RevertedAllSerialNumbersSuccessfully'));
                this.refreshPurchaseOrderData();  
            }, (error) => {
                this.notify.error(this.l('RevertFailed'));
            });
    }
    onUploadExcelErrorstc(): void {
        this.notify.error(this.l('ImportSerialNoUploadFailed'));
    }

}
