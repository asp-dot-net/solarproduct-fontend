import { Component, Injector, ViewEncapsulation, ViewChild, OnInit, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { StockOrderServiceProxy,CommonLookupDto,CommonLookupServiceProxy ,GetAllStockOrderDto,OrganizationUnitDto,GetAllStockOrderOutputDto} from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { DomSanitizer, SafeHtml, Title } from '@angular/platform-browser';
import { ViewAllUploadsModal } from './view-uploads-modal.component';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import {CreateOrEditPurchaseOrderModalComponent} from './create-or-edit-purchase-order-modal.component';
import{ViewPurchaseOrderDetailComponent} from './purchase-order-details.component';
import {SMSModalComponent} from '@app/main/inventory/Activity/sms-modal-component' ;
import {EmailModalComponent} from '@app/main/inventory/Activity/email-modal-component';
import{RemindersModalComponent} from '@app/main/inventory/Activity/reminders-modal-component';
import{CommentsModalComponent} from '@app/main/inventory/Activity/comments-modal.component';
import {StockOrderToDoModalComponent} from '@app/main/inventory/Activity/todo-modal.component';
import {ViewPoModalComponent} from '@app/main/inventory/purchase-order-new/view-po.component';
@Component({
    templateUrl: './purchase-order-new.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class PurchaseOrderComponent extends AppComponentBase implements OnInit {
    @ViewChild('ViewAllUploadsModal', { static: true }) ViewAllUploadsModal: ViewAllUploadsModal;      
    @ViewChild('CreateOrEditPurchaseOrderModal', { static: true }) CreateOrEditPurchaseOrderModal: CreateOrEditPurchaseOrderModalComponent;
    @ViewChild('ViewPurchaseOrderDetail', { static: true }) ViewPurchaseOrderDetail: ViewPurchaseOrderDetailComponent;
    @ViewChild('SMSModal', { static: true }) SMSModal: SMSModalComponent;
    @ViewChild('EmailModal', { static: true }) EmailModal: EmailModalComponent;
    @ViewChild('RemindersModal', { static: true }) RemindersModal: RemindersModalComponent;
    @ViewChild('CommentsModal', { static: true }) CommentsModal: CommentsModalComponent;
    @ViewChild('todosModal', { static: true }) todosModal: StockOrderToDoModalComponent;
    @ViewChild('viewPoModel', { static: true }) viewPoModel: ViewPoModalComponent;
    
    FiltersData = false;
   
    shouldShow: boolean = false;
    organizationUnitlength: number = 0;
    flag = true;
    show: boolean = true;
    showchild: boolean = true;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    toggle: boolean = true;
    showCancel: boolean = false;
    ExpandedView: boolean = true;
    change() {
        this.toggle = !this.toggle;
      }
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    StockOrderStatusIds = [];
    WarehouseLocationIdFiletr = [];
    PaymentStatusIdFiletr  :number = undefined;
    PaymentTypeIdFiletr = [];
    TransportCompanyIdFiletr :number = undefined;
    VendorIdFiletr  :number = undefined;
    DeliveryTypeIdFiletr :number = undefined;
    StockOrderIdFiletr :number = undefined;
    GstTypeIdFilter :number = undefined;
    advancedFiltersAreShown = false;
    allLocation: CommonLookupDto[];
    allPaymentStatus:CommonLookupDto[];
    allStockOrderStatus: CommonLookupDto[];
    allPaymentType: CommonLookupDto[];
    allPurchaseCompany: CommonLookupDto[];
    allVendors: CommonLookupDto[];
    allDeliveryType :CommonLookupDto[];
    allStockOrderfor:CommonLookupDto[];
    allTransportCompany:CommonLookupDto[];
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnit = 0;
    changeOrganization = 0;
    date = new Date();
    firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    StartDate = moment().startOf('month').add(1, 'days');
    EndDate = moment().add(0, 'days').endOf('day');
    dateFilterType  = undefined;
    filterText = '';
    areaNameFilter = '';
    filterName ="OrderNo";
   
    excelorcsvfile = 0;
    firstrowcount = 0;
    last = 0;
    vicrebate = "All";
    solarRebateStatus = 0;
   
    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 330;
    allBDMs : any[];
    allSalesRap : any[];
    
    SelectedPurtchaseOrderId: number = 0;
    PurchaseOrderName: string;
    summary: GetAllStockOrderDto[];

    constructor(
        injector: Injector,
        private _stockOrderServiceProxy: StockOrderServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _commonLookupService: CommonLookupServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _router: Router,
        private titleService: Title,
       
    ) {
        super(injector);
        
    }
    searchLog() : void {
        
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Stock Order';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
  
   
    ngOnInit(): void {
        this.StartDate =moment().startOf('month').add(1, 'days');
        this.EndDate = moment().add(0, 'days').endOf('day');
        this.screenHeight = window.innerHeight;
        this.titleService.setTitle(this.appSession.tenancyName + " |  Purchase Order");
     
        debugger;
        this._commonLookupService.getStockOrderStatusDropdown().subscribe(result => {
            this.allStockOrderStatus = result;
        })
        this._commonLookupService.getStockOrderPaymentStatusDropdown().subscribe(result => {
            this.allPaymentStatus = result;
        })
        this._commonLookupService.getLocationDropdown().subscribe(result => {
            this.allLocation = result;
        })
        this._commonLookupService.getpaymentTypeDropdown().subscribe(result => {
            this.allPaymentType = result;
        })
        this._commonLookupService.getAllVendorDropdown().subscribe(result => {
            this.allVendors = result;
        });
        this._commonLookupService.getPurchaseCompanyDropdown().subscribe(result => {
            this.allPurchaseCompany = result;
        })
        this._commonLookupService.getDeliveryTypeDropdown().subscribe(result => {
            this.allDeliveryType = result;
        })
        this._commonLookupService.getStockorderfoDropdown().subscribe(result => {
            this.allStockOrderfor = result;
        })
        this._commonLookupService.getTransportCompanyDropdown().subscribe(result => {
            this.allTransportCompany = result;
        })
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            
            this.getStockOrder();
        });
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Open Stock Order';
        log.section = 'Stock Order';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
    }

    
    stockOrderDetail(orderId?: number): void {
        this._router.navigate(['/app/main/inventory/purchase-order-new/StockOrderDetailModal'], { queryParams: { OId: orderId } });
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    
    closeModal() {
        this.shouldShow = false; // Hides the modal
    }
    getStockOrder(event?: LazyLoadEvent) {
        debugger;
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
      
        this.primengTableHelper.showLoadingIndicator();

        this._stockOrderServiceProxy.getAll(
                  
            this.filterName,
            this.filterText,
            this.GstTypeIdFilter,
            this.TransportCompanyIdFiletr,
            this.StockOrderStatusIds,
            this.WarehouseLocationIdFiletr,
            this.PaymentStatusIdFiletr,
            this.PaymentTypeIdFiletr,
            this.VendorIdFiletr,
            this.DeliveryTypeIdFiletr,
            this.StockOrderIdFiletr,
            this.organizationUnit,
            this.dateFilterType,
            this.StartDate,
            this.EndDate,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.summary=result.items;
            
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            
            
        });
    }
    
   
    reloadPage(flafValue: boolean): void {
        this.ExpandedView = true;
        this.flag = flafValue;
        this.paginator.changePage(this.paginator.getPage());
        if (!flafValue) {
            this.navigateToPurchaseOrderDetail(this.SelectedPurtchaseOrderId);
        }
    }
    navigateToPurchaseOrderDetail(purchaseOrderId): void {
        debugger;
        this.ExpandedView = !this.ExpandedView;
        this.ExpandedView = false;
        this.SelectedPurtchaseOrderId = purchaseOrderId;
        this.PurchaseOrderName = "PurchaseOrderName";
        this.ViewPurchaseOrderDetail.showDetail(purchaseOrderId, this.PurchaseOrderName, 1);
    } 
    deleteStockOrder(GetAllStockOrderOutputDto: GetAllStockOrderOutputDto): void {
        
        this.message.confirm(
            this.l('AreYouSureWanttoDelete'),
            this.l('Delete'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._stockOrderServiceProxy.delete(GetAllStockOrderOutputDto.id)
                        .subscribe(() => {
                            this.getStockOrder();
                            this.notify.success(this.l('SuccessfullyDeleted'));

                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete Stock Order: ' + GetAllStockOrderOutputDto.orderNo;
                            log.section = 'Stock Order';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });
                        });
                }
            }
        );
    }
    exportToExcel(excelorcsv): void {
        
        this._stockOrderServiceProxy.getStockOrderToExcel(
            this.filterName,
            this.filterText,
            this.GstTypeIdFilter,
            this.TransportCompanyIdFiletr,
            this.StockOrderStatusIds,
            this.WarehouseLocationIdFiletr,
            this.PaymentStatusIdFiletr,
            this.PaymentTypeIdFiletr,
            this.VendorIdFiletr,
            this.DeliveryTypeIdFiletr,
            this.StockOrderIdFiletr,
            this.organizationUnit,
            this.dateFilterType,
            this.StartDate,
            this.EndDate,
               
            )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
             });
    }
  
    clear() {
        this.filterText = '';
        let date = new Date();
        this.GstTypeIdFilter,
        this.TransportCompanyIdFiletr=undefined,
        this.StockOrderStatusIds =[],
        this.WarehouseLocationIdFiletr=[],
        this.PaymentStatusIdFiletr=undefined,
        this.dateFilterType='',
        this.VendorIdFiletr=undefined,
        this.DeliveryTypeIdFiletr=undefined,
        this.StockOrderIdFiletr=undefined,
        this.dateFilterType  = undefined;
        this.StartDate = moment().add(0, 'days').endOf('day');
        this.EndDate = moment().add(0, 'days').endOf('day');
        
        this.getStockOrder();
    }

    createPurchaseOrder(lId : number): void {
        this.CreateOrEditPurchaseOrderModal.show(lId,this.organizationUnit);        
    }
   
    expandGrid() {
        this.ExpandedView = true;
    }
    
    //  viewAllUploads(): void {
    //        this.ViewAllUploadsModal.show();
    //     }

}
