import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLookupDto,CommonLookupServiceProxy, StockOrderServiceProxy,GetAllPurchaseDocumentEditOutput } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Component, ElementRef, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConsts } from '@shared/AppConsts';
import { FileUpload } from 'primeng';
import { AppSessionService } from '@shared/common/session/app-session.service';
@Component({
    selector: 'ViewAllUploadsModal',
    templateUrl: './view-uploads-modal.component.html',
})
export class ViewAllUploadsModal extends AppComponentBase {
    @ViewChild('ViewAllUploadsModal', { static: true }) modal: ModalDirective;
    @ViewChild("myFile") myFileRef: ElementRef;
  uploadUrl: string;
    ExpandedViewApp: boolean = true;
    active = false;    
    allPurchaseDocument: CommonLookupDto[];
    maxFileSize: number;
    allowedFormats: string[] = [];
    allowedFormatss: any[] = [];
    STR: string;
    appSession: AppSessionService;
    saveDoc: boolean = false;
    tenantId: number;
    CreateId: number;
    purchaseOrderId: number;
    orgId: number;
    selectedDocumentId: number;
    images = [];
    fileList: File[] = [];
    documents: GetAllPurchaseDocumentEditOutput[] = [];
    fileName: any;
    allowedFileTypes: string = '';
    messagee: string = '';
    DocName = '';
    ManualOrder = '';
    ContainerNo = '';
    constructor(injector: Injector,
         private _commonLookupService: CommonLookupServiceProxy,
         private _stockOrderService: StockOrderServiceProxy,
         private _httpClient: HttpClient,
        ) {
        super(injector);
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/api/Document/UploadPurchaseOrderDocuments';
    }
    ngOnInit(): void {
        this._commonLookupService.getPurchaseDoucmentDropdown().subscribe(result => {
            this.allPurchaseDocument = result;
        });
    }
    show( purchaseOrderId: number, orgId: number,manualOrder = '', containerNo = '') {
        this.tenantId =this.appSession.tenantId;
       this.CreateId=this.appSession.userId;
        this.purchaseOrderId = purchaseOrderId;
        this.orgId = orgId;
        this.getDocumentList();
        this.Reset();
        this.modal.show();
    }
    getDocumentList() {
        this._stockOrderService.getAllPurchaseDocument(this.purchaseOrderId).subscribe(result => {
            this.documents = result.items;
        });
    }
    Reset(): void {
        this.selectedDocumentId = undefined; 
        this.ResetFileInput();
        this.messagee = '';  
    }
    ResetFileInput(){
        this.fileList = []; 
        const fileInput = document.querySelector('.fileuploadboxbtn') as HTMLInputElement;
        if (fileInput) {
            fileInput.value = ''; 
        } 
    }
    onDocumentTypeChange(event: any) {
        debugger
        this.selectedDocumentId = event.target.value;
        if (this.selectedDocumentId) {
            this._stockOrderService.getPurchaseDocumentData(this.selectedDocumentId).subscribe(result => {
                this.maxFileSize = result.size;
                this.allowedFormats =  result.formats;
                this.allowedFormatss = result.formats.map(format => '.' + format.toLowerCase());
                this.allowedFileTypes = this.allowedFormatss.join(',');
                this.messagee = `Note: you can upload only ${this.allowedFormatss.join(', ')} files 
                less than ${this.maxFileSize} kb`;
                this.myFileRef.nativeElement.accept = this.allowedFormatss;
                this.ResetFileInput();
            });
        } else {
            this.allowedFileTypes = '';
            this.messagee = '';  
        }
    }
    onFileChange(event) {
        debugger
       
        if (event.target.files && event.target.files[0]) {
            const file = event.target.files[0];
            const fileSizeInKB = file.size / 1024;
            const fileExtension = file.name.split('.').pop().toLowerCase();
            if (fileSizeInKB > this.maxFileSize) {
                this.notify.warn(`File size exceeds the allowed limit of ${this.maxFileSize} KB.`);
                this.ResetFileInput();
                return;
            }
            const allowedFormatsLowerCase = this.allowedFormats.map(format => format.toLowerCase());
           
            if (!allowedFormatsLowerCase.includes(fileExtension)) {
                this.notify.warn(`Invalid file format. Only ${this.allowedFormats.join(', ')} files are allowed.`);
                this.ResetFileInput();
                return;
            }
            var filesAmount = event.target.files.length;
            for (let i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = (event: any) => {
                    this.images.push(event.target.result);
                }

                reader.readAsDataURL(event.target.files[i]);

                const file = event.target.files[i];
                this.fileList.push(file)
            }
        }
    }
    save() {
        debugger;
        if (!this.selectedDocumentId) {
            this.notify.warn('Please select a document type before Uploading the file.');
            return;
        }
        this.STR = `${this.tenantId},${this.purchaseOrderId},${this.selectedDocumentId},${this.orgId},${this.CreateId},${this.DocName}`;
        this.showMainSpinner();
        const formData = new FormData();
        if (this.fileList.length > 0) {
            for (let i = 0; i < this.fileList.length; i++) {
                const file = this.fileList[i];
                let name = this.fileName + "_" + i;
                formData.append(name, file, file.name);
            }
            this._httpClient
                .post<any>(this.uploadUrl + "?str=" + encodeURIComponent(this.STR), formData)
                .subscribe(response => {
                    if (response.success) {
                        this.saveDoc = true;
                        this.getDocumentList();
                        this.Reset();
                    } else if (response.error != null) {
                        this.notify.error(this.l('Upload failed.'));
                    }
                    this.hideMainSpinner();
                }, e => {
                    this.hideMainSpinner();
                });
        } else {
            this.notify.info("Please select an image to upload.", "Empty");
        }
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }
    deletePurchaseDocument(id: number): void {
        this.message.confirm(
            this.l('AreYouSureWanttoDelete'),
            this.l('Delete'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._stockOrderService.deletePurchaseDoucment(id)
                        .subscribe(() => { 
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            this.getDocumentList();
                        });
                }
            }
        );
    }
    downloadfile(file,filename): void {
        debugger;
        let FileName = AppConsts.docUrl + file+ "/" +filename;
        window.open(FileName, "_blank");
    }
    
    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;                
    }
    expandApp() {
        this.ExpandedViewApp = false;
    }
}
