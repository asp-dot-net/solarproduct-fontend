import { Component, ViewChild, Injector, Output, EventEmitter, OnInit, ElementRef, Directive, Optional } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {
    LeadsServiceProxy, PurchaseOrderHistoryDto, StockOrderServiceProxy, WholeSaleLeadServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import PlaceResult = google.maps.places.PlaceResult;


@Component({
    selector: 'purchaseOrderactivityloghistory',
    templateUrl: './activity-log-history.component.html',
})
export class PurchaseOrderActivityLogHistoryComponent extends AppComponentBase {
    @ViewChild("myNameElem") myNameElem: ElementRef;
    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @ViewChild('SampleDatePicker', { static: true }) sampleDatePicker: ElementRef;


    PurchaseOrderHistory: PurchaseOrderHistoryDto[];
    constructor(
        injector: Injector,
        private _purchaseOrderServiceProxy: StockOrderServiceProxy,
        private _el: ElementRef,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,

    ) {
        super(injector);
        this._el.nativeElement.setAttribute('autocomplete', 'off');
        this._el.nativeElement.setAttribute('autocorrect', 'off');
        this._el.nativeElement.setAttribute('autocapitalize', 'none');
        this._el.nativeElement.setAttribute('spellcheck', 'false');
    }

    show(leadId?: number): void {
        debugger
        this._purchaseOrderServiceProxy.getPurchaseOrderActivityLogHistory(leadId, 0, 0, false, false).subscribe(result => {
                    this.PurchaseOrderHistory = result;
                    this.modal.show();
                    let log = new UserActivityLogDto();
                    log.actionId = 79;
                    log.actionNote ='Open Activity Log History';
                    log.section = 'Stock Order';
                     this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
                });
    }
    close(): void {

        this.modal.hide();
    }
}
