﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit, ElementRef, Directive, Optional } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import {GetStockOrderForViewDto,
    StockOrderServiceProxy,CommonLookupDto,
    CommonLookupServiceProxy, CreateOrEditStockOrderDto,
    StockOrderDto
} from '@shared/service-proxies/service-proxies';
import { AppConsts } from '@shared/AppConsts';
import {  TokenService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute, Router } from '@angular/router';
import PlaceResult = google.maps.places.PlaceResult;
import { ViewMyLeadComponent } from '@app/main/myleads/myleads/view-mylead.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { finalize } from 'rxjs/operators';
import { FileUpload } from 'primeng/fileupload';

@Component({
    selector: 'StockOrderDetailModal',
    templateUrl: './stock-order-detail-modal.component.html',    
})
export class StockOrderDetailModal extends AppComponentBase implements OnInit {
    @ViewChild("myNameElem") myNameElem: ElementRef;
    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @ViewChild('SampleDatePicker', { static: true }) sampleDatePicker: ElementRef;
    @ViewChild('StockOrderDetailModal', { static: true }) StockOrderDetailModal: StockOrderDetailModal;
    @ViewChild('ExcelFileUpload', { static: false }) excelFileUpload: FileUpload;

    active = false;
    saving = false;
    allPurchaseCompany: CommonLookupDto[];
    allStockOrderStatus: CommonLookupDto[];
    allPaymentType: CommonLookupDto[];
    allPaymentMethod: CommonLookupDto[];
    allDeliveryType: CommonLookupDto[];
    allLocation: CommonLookupDto[];
    allStockOrderfor: CommonLookupDto[];
    allCurrency: CommonLookupDto[];
    allStockFrom :CommonLookupDto[];
    allVendors :CommonLookupDto[];
    allPaymentStatus:CommonLookupDto[];
    purchaseOrder: GetStockOrderForViewDto = new GetStockOrderForViewDto();
    organizationUnit = 0;
    stockorderId = 0;
    PurchaseOrderItemList: any[];

    path = AppConsts.docUrl;
    uploadUrlstc: string;
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _stockOrderServiceProxy: StockOrderServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _tokenService: TokenService,
        private _httpClient: HttpClient,
        private _router: Router,
        private _el: ElementRef,

    
        @Optional() private viewLeadDetail?: ViewMyLeadComponent,
        
    ) {
        super(injector);
        this._el.nativeElement.setAttribute('autocomplete', 'off');
        this._el.nativeElement.setAttribute('autocorrect', 'off');
        this._el.nativeElement.setAttribute('autocapitalize', 'none');
        this._el.nativeElement.setAttribute('spellcheck', 'false');
        this.purchaseOrder= new GetStockOrderForViewDto();
        this.purchaseOrder.purchaseOrder = new StockOrderDto();
        this.uploadUrlstc = AppConsts.remoteServiceBaseUrl + '/Users/ImportPurchaseOrderItemFromExcel';
    }
 
    ngOnInit(): void {
        this.stockorderId = this._activatedRoute.snapshot.queryParams['OId'];
        debugger;
        if (this.stockorderId) {
            this._stockOrderServiceProxy.getStockOrderById(this.stockorderId).subscribe(result => {
              this.purchaseOrder=result;
            //   this.purchaseOrder.purchaseOrder.containerNo
              this.PurchaseOrderItemList = [];
                this.purchaseOrder.purchaseOrderItemList.map((item) => {
                    let PurchaseOrderItemCreateOrEdit = { id: 0, productTypeId: 0,productType:" ", productItemId: 0, productItemName: "", quantity:0, pricePerWatt: 0, unitRate: 0,estRate:0 ,amount: 0,productModel: "",
                    expiryDate: null,}
                    PurchaseOrderItemCreateOrEdit.id = item.id;
                    PurchaseOrderItemCreateOrEdit.productTypeId = item.productTypeId;
                    PurchaseOrderItemCreateOrEdit.productItemId = item.productItemId;
                    PurchaseOrderItemCreateOrEdit.productItemName = item.productItem;
                    PurchaseOrderItemCreateOrEdit.productType = item.productType;
                    PurchaseOrderItemCreateOrEdit.quantity = item.quantity;
                    PurchaseOrderItemCreateOrEdit.pricePerWatt = item.pricePerWatt;
                    PurchaseOrderItemCreateOrEdit.unitRate = item.unitRate;
                    PurchaseOrderItemCreateOrEdit.amount = item.amount;
                    PurchaseOrderItemCreateOrEdit.estRate=item.estRate;
                    PurchaseOrderItemCreateOrEdit.productModel= item.modelNo,
                    PurchaseOrderItemCreateOrEdit.expiryDate=item.expiryDate,
                    this.PurchaseOrderItemList.push(PurchaseOrderItemCreateOrEdit);
                });
            });
        }


        this._commonLookupService.getPurchaseCompanyDropdown().subscribe(result => {
            this.allPurchaseCompany = result;
        })
        this._commonLookupService.getStockOrderStatusDropdown().subscribe(result => {
            this.allStockOrderStatus = result;
        })
        this._commonLookupService.getpaymentTypeDropdown().subscribe(result => {
            this.allPaymentType = result;
        })
        this._commonLookupService.getStockOrderPaymentStatusDropdown().subscribe(result => {
            this.allPaymentStatus = result;
        })
        this._commonLookupService.getpaymentMethodDropdown().subscribe(result => {
            this.allPaymentMethod = result;
        })
        this._commonLookupService.getDeliveryTypeDropdown().subscribe(result => {
            this.allDeliveryType = result;
        })
        this._commonLookupService.getcurrencyDropdown().subscribe(result => {
            this.allCurrency = result;
        })
        this._commonLookupService.getLocationDropdown().subscribe(result => {
            this.allLocation = result;
        })
        this._commonLookupService.getStockorderfoDropdown().subscribe(result => {
            this.allStockOrderfor = result;
        })
        this._commonLookupService.getStockOrderFromOutsideDropdown().subscribe(result => {
            this.allStockFrom = result;
        })
        this._commonLookupService.getAllVendorDropdown().subscribe(result => {
            this.allVendors = result;
       
        });
    }
    onShown(){}

    close(): void {
       
        this.modal.hide();
        this.saving = false;
    }

    cancel(): void {
        
        this._router.navigate(['/app/main/inventory/purchase-order-new/']);     
        
    }
    uploadExcelstc(data: { files: File }): void {
        debugger;
        var headers_object = new HttpHeaders().set("Authorization", "Bearer " + this._tokenService.getToken());

        const httpOptions = {
            headers: headers_object
        };
        const formData: FormData = new FormData();
        const file = data.files[0];
        formData.append('file' + ',' + this.organizationUnit, file, file.name);
        this._httpClient
            .post<any>(this.uploadUrlstc, formData, httpOptions)
            .pipe(finalize(() => this.excelFileUpload.clear()))
            .subscribe(response => {
                if (response.success) {
                    this.notify.success(this.l('ImportInstallerInvoicePaymentProcessStart'));
                } else if (response.error != null) {
                    this.notify.error(this.l('ImportInstallerInvoicePaymentUploadFailed'));
                }
            });
    }

    onUploadExcelErrorstc(): void {
        this.notify.error(this.l('ImportInstallerInvoicePaymentUploadFailed'));
    }
}
