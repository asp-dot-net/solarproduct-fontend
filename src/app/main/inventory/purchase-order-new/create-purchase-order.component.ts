﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit, ElementRef, Directive, Optional } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {
    LeadsServiceProxy
    , LeadStateLookupTableDto
    , LeadSourceLookupTableDto
    , LeadStatusLookupTableDto,
    CheckExistLeadDto,
    GetDuplicateLeadPopupDto,
    CommonLookupServiceProxy,
    PostCodeRangeServiceProxy,
    JobInstallerInvoicesServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { FileUpload } from 'primeng/fileupload';
import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AgmCoreModule } from '@agm/core';
import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';
import PlaceResult = google.maps.places.PlaceResult;
import { Appearance, GermanAddress, Location } from '@angular-material-extensions/google-maps-autocomplete';
//import { ViewDuplicatePopUpModalComponent } from './duplicate-lead-popup.component';

import { NgxSpinnerService } from 'ngx-spinner';

import { AddStockItemModal } from './add-stock-item-modal.component';

@Component({
    selector: 'createEditPurchaseOrder',
    templateUrl: './create-purchase-order.component.html',
    //styleUrls: ['./create-purchase-order.component.less']
})
export class createEditPurchaseOrder extends AppComponentBase implements OnInit {
    @ViewChild("myNameElem") myNameElem: ElementRef;
    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @ViewChild('SampleDatePicker', { static: true }) sampleDatePicker: ElementRef;
    //@ViewChild('duplicatepopupModal', { static: true }) duplicatepopupModal: ViewDuplicatePopUpModalComponent;
    @ViewChild('createEditPurchaseOrder', { static: true }) createEditPurchaseOrder: createEditPurchaseOrder;
    @ViewChild('AddStockItemModal', { static: true }) AddStockItemModal: AddStockItemModal;

    active = false;
    saving = false;
    
    postCodeSuburb = '';
    stateName = '';
    leadSourceName = '';
    LeadStatusName = '';
    latitude = '';
    longitude = '';
    leadStatus: any;
    allStates: LeadStateLookupTableDto[];
    allLeadSources: LeadSourceLookupTableDto[];
    allLeadStatus: LeadStatusLookupTableDto[];
    ShowAddress: boolean = true;
    unitType: any;
    streetType: any;
    streetName: any;
    suburb: any;
    postalUnitType: any;
    postalStreetType: any;
    postalStreetName: any;
    postalSuburb: any;
    filteredunitTypes: LeadStatusLookupTableDto[];
    filteredstreettypes: LeadStatusLookupTableDto[];
    filteredstreetnames: LeadStatusLookupTableDto[];
    filteredsuburbs: LeadStatusLookupTableDto[];
    output: LeadStatusLookupTableDto[] = new Array<LeadStatusLookupTableDto>();
    unitTypesSuggestions: string[];
    streetTypesSuggestions: string[];
    streetNamesSuggestions: string[];
    suburbSuggestions: string[];
    postalunitTypesSuggestions: string[];
    postalstreetTypesSuggestions: string[];
    postalstreetNamesSuggestions: string[];
    postalsuburbSuggestions: string[];
    selectAddress: boolean = true;
    selectAddress2: boolean = true;
    from: string;
    item: GetDuplicateLeadPopupDto[];
    role: string = '';
    showFootNote: boolean = false;
    DelAddress = false;

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _router: Router,
        private _el: ElementRef,
        private spinner: NgxSpinnerService,
        private _postCodeRangeServiceProxy: PostCodeRangeServiceProxy,
        private _jobInstallerInvoiceServiceProxy: JobInstallerInvoicesServiceProxy,
        // @Optional() private viewLeadDetail?: ViewMyLeadComponent,
        
    ) {
        super(injector);
        this._el.nativeElement.setAttribute('autocomplete', 'off');
        this._el.nativeElement.setAttribute('autocorrect', 'off');
        this._el.nativeElement.setAttribute('autocapitalize', 'none');
        this._el.nativeElement.setAttribute('spellcheck', 'false');
    }
 
    ngOnInit(): void {
        this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
            this.role = result;
        });
    }
    onShown(){}

    // get UnitType
    filterunitTypes(event): void {
        this._leadsServiceProxy.getAllUnitType(event.query).subscribe(output => {
            this.unitTypesSuggestions = output;
        });
    }

    // get StreetType
    filterstreettypes(event): void {
        this._leadsServiceProxy.getAllStreetType(event.query).subscribe(output => {
            this.streetTypesSuggestions = output;
        });
    }

    // get StreetName
    filterstreetnames(event): void {
        this._leadsServiceProxy.getAllStreetName(event.query).subscribe(output => {
            this.streetNamesSuggestions = output;
        });
    }

    // get Suburb
    filtersuburbs(event): void {
        this._leadsServiceProxy.getAllSuburb(event.query).subscribe(output => {
            this.suburbSuggestions = output;
        });
    }
    //get state & Postcode
    fillstatepostcode(): void {
        var splitted = this.suburb.split("||");
        // this.lead.suburb = splitted[0];
        // this.lead.state = splitted[1].trim();
        // this.lead.postCode = splitted[2].trim();
        // this.lead.country = "AUSTRALIA";

        this.bindArea();
    }

    close(): void {
        this.Reset();
        this.modal.hide();
        this.saving = false;
    }

    cancel(): void {
        this.Reset();
        this._router.navigate(['/app/main/inventory/purchase-order-new/']);     
        
    }

    bindArea(): void {
        
        // if(this.lead.postCode != null && this.lead.postCode != "" && this.lead.postCode != undefined)
        // {
        //     this._postCodeRangeServiceProxy.getAreaByPostCodeRange(this.lead.postCode).subscribe((result) => 
        //     {
        //         this.lead.area = result;
        //     });
        // }
    }

     OnSelectDetails(event): void {
         // alert(event.leadId);
        //  this.lead.referralLeadId = event.leadId;
        //  this.lead.referralName = event.customerName;
     }
     // End Referral Details Code
 
     referal: boolean = false;
     LeadSourceOnChange() {
        //  if(this.lead.leadSource == 'Referral'){
 
        //      this.referal = true;
        //  }
        //  else{
        //      this.referal = false;
        //  }
     }

     Reset(): void {
        this.selectAddress = true;
        // this.lead.companyName = undefined;
        // this.lead.leadSource = "";
        // this.lead.area = "";
        // this.lead.type = "";
        // this.lead.email = undefined;
        // this.lead.phone = undefined;
        // this.lead.mobile = undefined;
        // this.lead.altPhone = undefined;
        // this.lead.requirements = undefined;
        // this.lead.solarType = undefined;
        // this.lead.address = undefined;
        // this.lead.address = null;
        // this.lead.unitNo = undefined;
        // this.unitType = undefined;
        // this.lead.streetNo = undefined;
        // this.streetName = undefined;
        // this.streetType = undefined;
        // this.suburb = undefined;
        // this.lead.state = "";
        // this.lead.postCode = undefined;
        // this.lead.country = undefined;
        // this.lead.abn = undefined;
        // this.lead.fax = undefined;
        // this.lead.isGoogle = "Google";
    }

    addStockItem(): void {
        this.AddStockItemModal.show();
      }  

}
