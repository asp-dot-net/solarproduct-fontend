import { Component, EventEmitter, Injector, Input, OnInit, Output, ViewChild } from '@angular/core';
import {
    StockOrderServiceProxy, GetPurchaseOrderActivityLogViewDto, StockOrderDto, GetStockOrderForViewDto, LeadDto, CommonLookupDto, LeadsServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import PlaceResult = google.maps.places.PlaceResult;
import { NgxSpinnerService } from 'ngx-spinner';
import { PurchaseOrderActivityLogHistoryComponent } from './activity-log-history.component';

@Component({
    selector: 'purchaseOrderactivityLog',
    templateUrl: './purchase-order-activity-log.component.html',
    animations: [appModuleAnimation()]
})
export class PurchaseOrderActivityLogComponent extends AppComponentBase implements OnInit {

    @ViewChild('purchaseOrderactivityloghistory', { static: true }) purchaseOrderactivityloghistory: PurchaseOrderActivityLogHistoryComponent;
    @Input() SelectedPurtchaseOrderId: number = 0;
    @Output() reloadLead = new EventEmitter();
    active = false;
    saving = false;
    purchaseOrder: GetStockOrderForViewDto = new GetStockOrderForViewDto();
    activeTabIndex: number = 0;
    activityDatabyDate: any[] = [];
    actionId: number = 0;
    activity: boolean = false;
    PurchaseOrderActivityList: GetPurchaseOrderActivityLogViewDto[];
    leadActionList: CommonLookupDto[];
    PurchaseOrderId: number = 0;
    id: number = 0;
    role: string = '';
    sectionId: number = 0;
    showsectionwise = false;
    currentactivity: boolean = false;
    allActivity: boolean = false;
    serviceId : number =0 ;
    PurchaseId = 0;

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _stockOrderServiceProxy: StockOrderServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _router: Router,
        private spinner: NgxSpinnerService
    ) {
        super(injector);
        this.purchaseOrder= new GetStockOrderForViewDto();
         this.purchaseOrder.purchaseOrder = new StockOrderDto();
    }


    ngOnInit(): void {
    
        // this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
        //     this.role = result;
        //     if (this.role == 'Sales Rep') {
        //         this.activity = true;
        //     }
        // });
    }

    show(purchaseOrderId?: number, param?: number, sectionId?: number, service? : number): void {
        debugger;
        this.sectionId = sectionId;
        this.activity = false;
        this.allActivity = false;
        this.serviceId = service;
        this.PurchaseId = purchaseOrderId;
        if (this.sectionId == 0 || this.sectionId > 12) {
            this.showsectionwise = true;
        } 
        if (param != null) {
            this.id = param;
        }
        this.PurchaseOrderId = purchaseOrderId;
        this.spinner.show();
            //this.actionId = 1;
            this._stockOrderServiceProxy.getPurchaseOrderActivityLog(purchaseOrderId, this.actionId, this.sectionId, this.currentactivity, this.allActivity).subscribe(result => {
                debugger;
                  this.PurchaseOrderActivityList = result;
                  this.spinner.hide();   
              });
        
    }

    showDetail(leadId: number): void {
        let that = this;
        debugger

        this._stockOrderServiceProxy.getPurchaseOrderActivityLog(leadId, this.actionId, this.sectionId, this.currentactivity, this.allActivity).subscribe(result => {
           
              this.PurchaseOrderActivityList = result;
          });
    }

    registerToEvents() {
        abp.event.on('app.onCancelModalSaved', () => {
            this.showDetail(this.PurchaseOrderId);
        });
    }

    addActivitySuccess() {
        this.showDetail(this.PurchaseOrderId);
    }

    changeActivity(leadId: number) {
        let that = this;
        this.spinner.show();
        debugger
        this._stockOrderServiceProxy.getPurchaseOrderActivityLog(leadId, this.actionId, this.sectionId, this.currentactivity, this.allActivity).subscribe(result => {
            if (result.length > 0) {
                this.notify.success(this.l('ActivityFiltered'));
            }
            else {
                this.notify.success(this.l('NoActivityFound'));
            }
            this.PurchaseOrderActivityList = result;
            this.spinner.hide();
        });
    }
    navigateToLeadHistory(leadid): void {

        // this.wholesaleactivityloghistory.show(leadid);
    }

    myActivity(PurchaseId: number) {
        let that = this;
        this.spinner.show();
        this._stockOrderServiceProxy.getPurchaseOrderActivityLog(PurchaseId, this.actionId, this.sectionId, this.currentactivity, this.allActivity).subscribe(result => {
            if (result.length > 0) {
                this.notify.success(this.l('ActivityFiltered'));
            }
            else {
                this.notify.success(this.l('NoActivityFound'));
            }
            this.PurchaseOrderActivityList = result;
            this.spinner.hide();
        });
    }
}
