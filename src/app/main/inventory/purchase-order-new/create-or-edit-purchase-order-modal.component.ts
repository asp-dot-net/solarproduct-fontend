import { ModalDirective } from 'ngx-bootstrap/modal';
import { AppComponentBase } from '@shared/common/app-component-base';
import { Component, ViewChild, Injector, Output, EventEmitter, OnInit, ElementRef, Directive, Optional } from '@angular/core';
import { finalize } from 'rxjs/operators';
import {PurchaseOrderItemDto,
    StockOrderServiceProxy
    ,CreateOrEditStockOrderDto,
    CommonLookupServiceProxy,
    CommonLookupDto,JobsServiceProxy,
    CreateOrEditStockOrderVariation,
    GetActualCostItemListInput
} from '@shared/service-proxies/service-proxies';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import PlaceResult = google.maps.places.PlaceResult;
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
    selector: 'CreateOrEditPurchaseOrderModal',
    templateUrl: './create-or-edit-purchase-order-modal.component.html',
    styleUrls: ['./Create-or-edit-purchase-order-Modal.Component.less']
})
export class CreateOrEditPurchaseOrderModalComponent extends AppComponentBase {

    @ViewChild('createoreditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
   
    active = false;
    saving = false;
    PurchaseOrderItemList: any[];
    productTypes: CommonLookupDto[];
    productItemSuggestions: any[];
    allPurchaseCompany: CommonLookupDto[];
    allStockOrderStatus: CommonLookupDto[];
    allPaymentType: CommonLookupDto[];
    allPaymentMethod: CommonLookupDto[];
    allDeliveryType: CommonLookupDto[];
    allLocation: CommonLookupDto[];
    allStockOrderfor: CommonLookupDto[];
    allCurrency: CommonLookupDto[];
    allStockFrom :CommonLookupDto[];
    allVendors :CommonLookupDto[];
    allPaymentStatus:CommonLookupDto[];
    allIncoTerm:CommonLookupDto[];
    stockorder: CreateOrEditStockOrderDto = new CreateOrEditStockOrderDto();
    date = new Date();
    targetArrivalDate = null;
    eta =  null;
    etd =  null;
    paymentDueDate =  null;
    physicalDeliveryDate =  null;
    PurchaseOrderId = 0;
    orderNumber: number;
    totalAmountWithoutGST: number = null;
    gstPercentage: number = 10; 
    gstAmount: number = null;
    totalAmountWithGST: number = null;
    vendor ='';
    variations = [];
    BasicTotal = 0;

    constructor(
        injector: Injector,
        private _stockOrderServiceProxy: StockOrderServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _jobServiceProxy: JobsServiceProxy,
        private _router: Router,
        private _el: ElementRef,
        private _activatedRoute: ActivatedRoute,
       
    ) {
        super(injector);
      
    }
    show(stockorderId?: number, oid?: number): void {
        debugger;
        this._commonLookupService.getAllStockVariation().subscribe(result => {
            this.variations = result;
        });
        if (stockorderId && stockorderId > 0) {
            this._stockOrderServiceProxy.getStockOrderForEdit(stockorderId).subscribe((result) => {
                this.stockorder = result.stockOrder;
                this.eta = moment(result.stockOrder.eta);
                this.etd = moment(result.stockOrder.etd);
                this.paymentDueDate = moment(result.stockOrder.paymentDueDate);
                this.physicalDeliveryDate = moment(result.stockOrder.physicalDeliveryDate);
                this.targetArrivalDate = moment(result.stockOrder.targetArrivalDate);
                this.vendor = result.vendor;
                
                this.PurchaseOrderItemList = result.stockOrder.purchaseOrderInfo.map((item) => ({
                    id: item.id,
                    productTypeId: item.productTypeId,
                    productItemId: item.productItemId,
                    productItemName: item.productItem,
                    quantity: item.quantity,
                    pricePerWatt: item.pricePerWatt,
                    unitRate: item.unitRate,
                    amount: item.amount,
                    estRate: item.estRate,
                    productModel: item.modelNo,
                    expiryDate: item.expiryDate,
                }));
                this.onItemAmountChange();
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Stock Order : ' + this.stockorder.orderNo;
                log.section = 'Stock Order';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });

               // this.stockorder.stockOrderVariationDto = [];
                if(!this.stockorder.stockOrderVariationDto || result.stockOrder.stockOrderVariationDto.length == 0){
                    var variation = new CreateOrEditStockOrderVariation();
                    this.stockorder.stockOrderVariationDto.push(variation);
                }
            });
        } else {
            
            this.stockorder = new CreateOrEditStockOrderDto();
            this.stockorder.organizationId = oid;
            this.stockorder.stockOrderVariationDto = [];

            var variation = new CreateOrEditStockOrderVariation();
            this.stockorder.stockOrderVariationDto.push(variation);
            
            if (oid) {
                this._stockOrderServiceProxy.getLastOrderNumber(oid).subscribe((lastOrderNumber) => {
                    this.stockorder.orderNo = lastOrderNumber; 
                    this.targetArrivalDate = null;
                    this.eta =  null;
                    this.etd =  null;
                    this.paymentDueDate =  null;
                    this.physicalDeliveryDate =  null;
                    this.orderNumber=null;
                    this.totalAmountWithGST=null;
                    this.totalAmountWithoutGST=null;
                    this.PurchaseOrderItemList = [{
                        id: 0,
                        productTypeId: 0,
                        productItemId: 0,
                        productItemName: "",
                        quantity: 0,
                        pricePerWatt: 0,
                        unitRate: 0,
                        estRate: 0,
                        amount: 0,
                        productModel: "",
                        expiryDate: null,
                    }];
    
                    this.active = true;
                    this.modal.show();
                    let log = new UserActivityLogDto();
                    log.actionId = 79;
                    log.actionNote ='Open For Create New Stock Order';
                    log.section = 'Stock Order';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    });
                
                });
            }
        }
    }
    getData(): void {
        
        if (this.orderNumber) {
          this._stockOrderServiceProxy.getStockOrderByOrderNo(this.orderNumber).subscribe(
            (result) => {
              result.stockOrder.id=null;
              result.stockOrder.orderNo = this.stockorder.orderNo;
              result.stockOrder.organizationId = this.stockorder.organizationId;
              this.stockorder = result.stockOrder;
              this.eta = moment(result.stockOrder.eta);
              this.etd = moment(result.stockOrder.etd);
              this.paymentDueDate = moment(result.stockOrder.paymentDueDate);
              this.physicalDeliveryDate = moment(result.stockOrder.physicalDeliveryDate);
              this.targetArrivalDate = moment(result.stockOrder.targetArrivalDate);
              
              this.PurchaseOrderItemList = result.stockOrder.purchaseOrderInfo.map((item) => ({
                
                productTypeId: item.productTypeId,
                productItemId: item.productItemId,
                productItemName: item.productItem,
                quantity: item.quantity,
                pricePerWatt: item.pricePerWatt,
                unitRate: item.unitRate,
                amount: item.amount,
                estRate: item.estRate,
                productModel: item.modelNo,
                expiryDate: item.expiryDate,
              }));
            }
            
          );
        } else {
          this.notify.warn('Please enter a valid order number.'); 
        }
      }
      
    ngOnInit(): void {
        
        this._commonLookupService.getAllProductTypeForTableDropdown().subscribe(result => {
            this.productTypes = result;
        });

        this._commonLookupService.getPurchaseCompanyDropdown().subscribe(result => {
            this.allPurchaseCompany = result;
        })
        this._commonLookupService.getStockOrderStatusDropdown().subscribe(result => {
            this.allStockOrderStatus = result;
        })
        this._commonLookupService.getpaymentTypeDropdown().subscribe(result => {
            this.allPaymentType = result;
        })
        this._commonLookupService.getStockOrderPaymentStatusDropdown().subscribe(result => {
            this.allPaymentStatus = result;
        })
        this._commonLookupService.getpaymentMethodDropdown().subscribe(result => {
            this.allPaymentMethod = result;
        })
        this._commonLookupService.getDeliveryTypeDropdown().subscribe(result => {
            this.allDeliveryType = result;
        })
        this._commonLookupService.getcurrencyDropdown().subscribe(result => {
            this.allCurrency = result;
        })
        this._commonLookupService.getLocationDropdown().subscribe(result => {
            this.allLocation = result;
        })
        this._commonLookupService.getStockorderfoDropdown().subscribe(result => {
            this.allStockOrderfor = result;
        })
        this._commonLookupService.getStockOrderFromOutsideDropdown().subscribe(result => {
            this.allStockFrom = result;
        })
        this._commonLookupService.getAllVendorDropdown().subscribe(result => {
            this.allVendors = result;
        });
        this._commonLookupService.getIncoTermDropdown().subscribe(result => {
            this.allIncoTerm = result;
        });
    }
    filterVendor = [];
    filterVendors(event): void {
        if(!event.query){
            this.stockorder.vendorId = 0;
            this.vendor = "";
            // this.getInvoiceIssued();
        }
        else{
            this.filterVendor = Object.assign([], this.allVendors).filter(
                item => item.displayName.toLowerCase().indexOf(event.query.toLowerCase()) > -1
              )
        }
       
       
    }
    onVendorChange(event: any): void {
        debugger;
        this.stockorder.vendorId = event.id;
        this.vendor = event.displayName;
        const selectedVendorId = event.id;
        if (selectedVendorId && selectedVendorId !== 'undefined') {
            this.fetchCreditDays(selectedVendorId);
        }
    }
    fetchCreditDays(vendorId?: number): void {
        this._stockOrderServiceProxy.getCreditDays(vendorId).subscribe(
            (creditDays) => {
                const days: number = creditDays;
                this.calculatePaymentDueDate(days);
            } 
        );
    }
    calculatePaymentDueDate(creditDays: number): void {
        this.paymentDueDate = moment().add(creditDays, 'days');
    }
    onShown(): void {
        
        // document.getElementById('stockorder_stockorder').focus();
    }
    
    cancel(): void {
        this.Reset();
        this.modal.hide();    
        
    }
    close(): void {
        this.Reset();
        this.modal.hide();
        this.saving = false;
    }
    Reset(): void {
    }
    
    changeProductType(i) : void {
        this.PurchaseOrderItemList[i].productItemId = "";
        this.PurchaseOrderItemList[i].productItemName = "";
        this.PurchaseOrderItemList[i].quantity = 0;
        this.PurchaseOrderItemList[i].pricePerWatt = 0;
        this.PurchaseOrderItemList[i].unitPrice = 0;
        this.PurchaseOrderItemList[i].estRate = 0;
        this.PurchaseOrderItemList[i].amount = 0;
        this.PurchaseOrderItemList[i].productModel ="";
        this.PurchaseOrderItemList[i].expiryDate= null;
    }
    calculateTotalAmountWithoutGST(): void {
        this.totalAmountWithoutGST = 0;
        this.PurchaseOrderItemList.forEach((item) => {
          const itemAmount = parseFloat(item.amount);
          if (!isNaN(itemAmount)) {
            this.totalAmountWithoutGST += itemAmount;
          }
        });
        this.totalAmountWithoutGST = parseFloat(this.totalAmountWithoutGST.toFixed(2));
      }
      
      calculateGSTAndNetPrice(): void {
        this.gstAmount = (this.totalAmountWithoutGST * this.gstPercentage) / 100;
        this.totalAmountWithGST = parseFloat((this.totalAmountWithoutGST + this.gstAmount).toFixed(2));
      }
      
      onItemAmountChange(): void {
        this.calculateTotalAmountWithoutGST();
        this.calculateGSTAndNetPrice();
      }
      
      updateAmount(i): void {
        const item = this.PurchaseOrderItemList[i];
        item.amount = (this.BasicTotal + (item.quantity * item.unitRate)).toFixed(2);
        this.onItemAmountChange();
      }
      
      
    filterProductIteams(event, i): void {
        let Id = this.PurchaseOrderItemList[i].productTypeId;

        this._jobServiceProxy.getPicklistProductItemList(Id, event.query).subscribe(output => {
            
            debugger;
            var items =this.PurchaseOrderItemList.map(i=>i.productItemName+'');
            this.productItemSuggestions = output.filter(x=> !items.includes(x.productItem))
        });
    }
     selectProductItem(event, i) {

        this.PurchaseOrderItemList[i].productItemId = event.id;
        this.PurchaseOrderItemList[i].productItemName = event.productItem;
        this.PurchaseOrderItemList[i].productModel = event.productModel;
        this.PurchaseOrderItemList[i].expiryDate = moment(event.expiryDate);
        // this.calculateUnitPrice(i);
    }
    removeInstallationItem(installationItem): void {
        if (this.PurchaseOrderItemList.length == 1)
            return;
       
        if (this.PurchaseOrderItemList.indexOf(installationItem) === -1) {
            
        } else {
            this.PurchaseOrderItemList.splice(this.PurchaseOrderItemList.indexOf(installationItem), 1);
        }
        this.calculateTotalAmountWithoutGST();
        this.calculateGSTAndNetPrice();
    }
    addInstallationItem(): void {
        let PurchaseOrderItemCreateOrEdit = { id: 0, productTypeId: 0, productItemId: 0, productItemName: "", quantity:0, pricePerWatt: 0, unitRate: 0,estRate:0,amount:0,productModel: "",
        expiryDate: null }
        this.PurchaseOrderItemList.push(PurchaseOrderItemCreateOrEdit);
        
    }
    save(): void {
        debugger;
            this.saving = true;
            debugger;
            this.stockorder.targetArrivalDate = this.targetArrivalDate; 
             this.stockorder.eta = this.eta;
             this.stockorder.etd = this.etd;
             this.stockorder.physicalDeliveryDate = this.physicalDeliveryDate;
             this.stockorder.paymentDueDate = this.paymentDueDate;

             this.stockorder.purchaseOrderInfo = [];
             this.PurchaseOrderItemList.map((item) => {
                 let PurchaseOrderItemListDto = new PurchaseOrderItemDto();
                 PurchaseOrderItemListDto.id = item.id;
                 PurchaseOrderItemListDto.productItemId = item.productItemId;
                 PurchaseOrderItemListDto.productItem = item.productItemName;
                 PurchaseOrderItemListDto.pricePerWatt = item.pricePerWatt;
                 PurchaseOrderItemListDto.quantity = item.quantity;
                 PurchaseOrderItemListDto.unitRate= item.unitRate;
                 PurchaseOrderItemListDto.estRate= item.estRate;
                 PurchaseOrderItemListDto.amount= item.amount;
                 PurchaseOrderItemListDto.modelNo= item.productModel;
                 PurchaseOrderItemListDto.expiryDate = item.expiryDate;
                 this.stockorder.purchaseOrderInfo.push(PurchaseOrderItemListDto);
             });
            
            this._stockOrderServiceProxy.createOrEdit(this.stockorder)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.modal.hide();
                this.modalSave.emit(null);
                if(this.stockorder.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Stock Order Updated : '+ this.stockorder.orderNo;
                    log.section = 'Stock Order';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Stock Order Created : '+ this.stockorder.orderNo;
                    log.section = 'Stock Order';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }

             });
             this.calculateTotalAmountWithoutGST();
    }

    calculateRates(): void {
        debugger;
        let that = this;
        this.BasicTotal = 0;
        let price = 0

        if (this.stockorder.stockOrderVariationDto) {
            this.stockorder.stockOrderVariationDto.forEach(function (item) {
                let varobj = that.variations.filter(x => x.id == item.stockVariationId);
                if (item.cost > 0) {
                    if (varobj[0].actionName == "Minus") {
                        price = price - parseFloat(item.cost.toString());
                    }
                    else {
                        price = price + parseFloat(item.cost.toString());
                    }
                } else {
                    price = price;
                }
            })

            // if (this.job.solarVICRebate != null && this.job.solarVICLoanDiscont != null) {
            //     this.job.totalCost = parseFloat(BasicTotal.toString());
            //     this.job.solarVICRebate = parseFloat(SolarVLCRebate.toString());
            //     this.job.solarVICLoanDiscont = parseFloat(SolarVICLoanDiscont.toString());
            //     let totalCostafterdic = (this.job.totalCost) - (this.job.solarVICRebate + this.job.solarVICLoanDiscont)
            //     if (this.job.depositRequired < 0) {
            //         this.job.depositRequired = Math.round(totalCostafterdic * 10 / 100);
            //     }
            //     this.billToPay = (totalCostafterdic) - (this.job.depositRequired);
            // } else {
            //     this.job.totalCost = parseFloat(BasicTotal.toString());
            //     if (this.job.depositRequired < 0) {
            //         this.job.depositRequired = Math.round(this.job.totalCost * 10 / 100);
            //     }
            //     this.billToPay = (this.job.totalCost) - (this.job.depositRequired);
            // }
        }

        this.BasicTotal = price;
        
        this.PurchaseOrderItemList.forEach(item =>
            item.amount = ((item.quantity * item.unitRate) +  this.BasicTotal).toFixed(2)
        );

        this.onItemAmountChange();

    }
    productItem = [];

    getActualCost() : void {

        // if(!this.checkActualCostFeatures())
        // {
        //     return;
        // }

        this.productItem = [];

        this.PurchaseOrderItemList.map((item) => {
            let jobProductItem = new GetActualCostItemListInput();
            jobProductItem.productTypeId = item.productTypeId;
            jobProductItem.productItemId = item.productItemId;
            jobProductItem.size = item.size;
            jobProductItem.qty = item.quantity;
            if (jobProductItem.productItemId != 0 && jobProductItem.productItemId != undefined && jobProductItem.qty != 0 && jobProductItem.qty != undefined) {
                this.productItem.push(jobProductItem);
            }
        });

        let jobVariationIds = [];
        this.stockorder.stockOrderVariationDto.map((item) => {
            if(item.stockVariationId != undefined && item.stockVariationId != null && item.stockVariationId != 0)
            {
                jobVariationIds.push(item.stockVariationId);
            }
        });

        // let systemCapacity = this.job.systemCapacity == null ? undefined : this.job.systemCapacity;
        // let houseTypeId = this.job.houseTypeId == null ? undefined : this.job.houseTypeId;
        // let roofTypeId = this.job.roofTypeId == null ? undefined : this.job.roofTypeId;
        // let roofAngleId = this.job.roofAngleId == null ? undefined : this.job.roofAngleId;
        // let stc = this.job.stc == null ? undefined : this.job.stc;
        // let warehouseDistance = this.warehouseDistance == null ? undefined : this.warehouseDistance;
        // let financeOptionId = this.job.financeOptionId == null ? undefined : this.job.financeOptionId;
        // let totalCost = this.job.totalCost == null ? undefined : this.job.totalCost;

        // let categoryDDiscountAmt = 0;
        // categoryDDiscountAmt = this.job.categoryDDiscountAmt == null ? undefined : this.job.categoryDDiscountAmt;
        // if(this.permission.isGranted("Pages.LeadDetails.CategoryD"))
        // {
        //     categoryDDiscountAmt = this.job.categoryDDiscountAmt == null ? undefined : this.job.categoryDDiscountAmt;
        // }

        // this._jobServiceProxy.getActualCost(this.lead.id, this.job.id, this.lead.area, this.job.state, systemCapacity, this.productItem, houseTypeId, roofTypeId, roofAngleId, jobVariationIds
        //     ,  stc, warehouseDistance, financeOptionId, totalCost, categoryDDiscountAmt).subscribe(result => {
        //     this.actualCost = result.actualCost;
        //     this.category = result.category;
        //     this.panelCost = result.panelCost;
        //     this.pricePerWatt = result.pricePerWatt;
        //     this.inverterCost = result.inverterCost;
        //     this.batteryCost = result.batteryCost;
        //     this.installationCost = result.installationCost;
        //     this.batteryInstallationCost = result.betteryInstallationCost;

        //     this.checkCategory();
        // });

        // this._jobServiceProxy.getActualCostList(this.lead.id, this.job.id, this.lead.area, this.job.state, systemCapacity, this.productItem, houseTypeId, roofTypeId, roofAngleId, jobVariationIds
        //     ,  stc, warehouseDistance, financeOptionId, totalCost, categoryDDiscountAmt).subscribe(result => {
        //     this.ActualCostList = result;
        // });
    }

    removeJobVariation(rowitem): void {
        debugger;
        // if (this.JobVariations.length == 1)
        //     return;
        // this.JobVariations = this.JobVariations.filter(item => item.variationId != JobVariation.variationId);

        if (this.stockorder.stockOrderVariationDto.indexOf(rowitem) === -1) {
            // this.JobVariations.push(rowitem);
        } else {
            this.stockorder.stockOrderVariationDto.splice(this.stockorder.stockOrderVariationDto.indexOf(rowitem), 1);
        }

        if(this.stockorder.stockOrderVariationDto.length == 0){
            this.stockorder.stockOrderVariationDto.push(new CreateOrEditStockOrderVariation());
        }

        this.calculateRates();
        this.getActualCost();
    }

    addJobVariation(item): void {
        debugger;
        if(!item.stockVariationId){
            this.notify.error("Please Select variationId");
            return;
        }
        if(!item.cost){
            this.notify.error("Please Enter Cost");
            return;
        }
        var displayName = this.variations.find(e => e.id == item.stockVariationId).displayName;
        
        if(displayName.includes("Discount") && !item.notes){
            this.notify.error("Please Enter Notes");
            return;
        }
        
        this.stockorder.stockOrderVariationDto.push(new CreateOrEditStockOrderVariation());
    }
}