import { ModalDirective } from 'ngx-bootstrap/modal';
import { AppComponentBase } from '@shared/common/app-component-base';
import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { finalize } from 'rxjs/operators';
import {StockPaymentOrderListDto,CreateOrEditStockPaymentDto, CommonLookupServiceProxy,CommonLookupDto,StockPaymentServiceProxy
} from '@shared/service-proxies/service-proxies';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import * as moment from 'moment';
import { HttpClient } from '@angular/common/http';
import { AppConsts } from '@shared/AppConsts';
import { FileUpload } from 'primeng';
import PlaceResult = google.maps.places.PlaceResult;

@Component({
    selector: 'CreateOrEditStockPaymentModal',
    templateUrl: './create-or-edit-stock-payment-modal.component.html',
    styleUrls: ['./create-or-edit-stock-payment-modal.component.less']
})
export class CreateOrEditStockPaymentModalComponent extends AppComponentBase {

    @ViewChild('createoreditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    saving = false;
    StockPaymentOrderItemList: any[];
    productTypes: CommonLookupDto[];
    ordernoItemSuggestions: any[]=[];
    allPaymentMethod: CommonLookupDto[]; 
    allCurrency: CommonLookupDto[];
    stockpayment: CreateOrEditStockPaymentDto = new CreateOrEditStockPaymentDto();
    date = new Date();
    paymentDate = null;
    StockPaymentId = 0;
    stockNo: number;
    selectedCurrencyDisplayName: string;
    isRateDisabled: boolean = false;
    uploadUrl: string;
    images = [];
    fileList: File[] = [];
    fileName: any;
    STR: string;
    saveDoc: boolean = false;
    tenantId: number;
    purchaseOrderId: number;
    orgId: number;

    constructor(
        injector: Injector,
        private _stockPaymentServiceProxy: StockPaymentServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _httpClient: HttpClient,
    ) {
        super(injector);
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/api/Document/UploadPaymentRecipt';
    }
    show(stockpaymentId?: number, oid?: number): void {
        debugger;
        this.tenantId =this.appSession.tenantId;
       
        this.StockPaymentId = stockpaymentId;
        this.orgId = oid;
        if (stockpaymentId && stockpaymentId > 0) {
            this._stockPaymentServiceProxy.getStockPaymentForEdit(stockpaymentId).subscribe((result) => {
                this.stockpayment = result.stockPayment;  
                this.orgId=result.stockPayment.organizationId;
                this.paymentDate = moment(result.stockPayment.paymentDate);
                this.StockPaymentOrderItemList = result.stockPayment.stockPaymentOrderlist.map((item) => ({
                    id: item.id,
                    purchaseOrderId: item.purchaseOrderId,
                    stockPaymentId: item.stockPaymentId,
                    orderNo: item.orderNo,
                    amount: item.amount, 
                }));
    
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Stock Payment : ' + this.stockpayment.stockNo;
                log.section = 'Stock Payment';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        } 
        else {
            
            this.stockpayment = new CreateOrEditStockPaymentDto();
            this.stockpayment.organizationId = oid;

            if (oid) {
                this._stockPaymentServiceProxy.getLastStockNo(oid).subscribe((lastStockNo) => {
                    this.stockpayment.stockNo = lastStockNo; 
                });
                this.ResetFileInput();
                    this.paymentDate =  null;
                    this.stockNo=null;
                    this.StockPaymentOrderItemList = [{
                        id: 0,
                        purchaseOrderId: 0,
                        stockPaymentId: 0,
                        amount: 0,
                        
                    }];
    
                    this.active = true;
                    this.modal.show();
                    let log = new UserActivityLogDto();
                    log.actionId = 79;
                    log.actionNote ='Open For Create New Stock Payment';
                    log.section = 'Stock Payment';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    });
            }
        }
    }
    onShown(): void {
        
        document.getElementById('stockorder_stockorder').focus();
    } 
    cancel(): void {
        this.modal.hide();    
        
    }
    close(): void {
        this.modal.hide();
        this.saving = false;
    }
    ResetFileInput(){
        this.fileList = []; 
        const fileInput = document.querySelector('.fileuploadboxbtn') as HTMLInputElement;
        if (fileInput) {
            fileInput.value = ''; 
        } 
    }
    // filterOrderNos(event: any, i: number): void {
    //     const query = event.query;
    //     const currencyId = this.stockpayment.currencyId;
    //     const gstType = this.stockpayment.gstType;
    //     const orgId = this.stockpayment.organizationId; 
    

    //     this._stockPaymentServiceProxy.getOrderNoSuggestions(query, orgId, currencyId, gstType).subscribe(result => {
    //         debugger;
    //         var items =this.StockPaymentOrderItemList.map(i=>i.orderNo);
    //         this.ordernoItemSuggestions = result.filter(x=> !items.includes(x.orderNo))
    //     });
    // }
    filterOrderNos(event: any, i: number): void {
        const query = event.query;
        const currencyId = this.stockpayment.currencyId;
        const gstType = this.stockpayment.gstType;
        const orgId = this.stockpayment.organizationId;
    
        this._stockPaymentServiceProxy.getOrderNoSuggestions(query, orgId, currencyId, gstType).subscribe(result => {
          
            const selectedOrderNos = this.StockPaymentOrderItemList
                .filter((item, index) => item.orderNo && index !== i) 
                .map(item => item.orderNo.toString().trim()); 
           
            this.ordernoItemSuggestions = result.filter(order => 
                !selectedOrderNos.includes(order.orderNo.toString().trim())
            );
    
            
        });
    }
    
    
    isCurrencyAndGSTSelected(): boolean {
        return this.stockpayment.currencyId !== undefined && this.stockpayment.gstType !== undefined;
    }
    ngOnInit(): void {
        this._commonLookupService.getpaymentMethodDropdown().subscribe(result => {
            this.allPaymentMethod = result;
        })
        this._commonLookupService.getcurrencyDropdown().subscribe(result => {
            this.allCurrency = result;
        })
    }
    selectOrderNo(event, i): void {
        debugger
        let selectedOrderNo = event.orderNo;
        let selectedOrder = this.ordernoItemSuggestions.find(order => order.orderNo === selectedOrderNo);
        this.StockPaymentOrderItemList[i].orderNo = event.orderNo;
        
        if (selectedOrder) {
            this.StockPaymentOrderItemList[i].purchaseOrderId = selectedOrder.purchaseOrderId;
        }
    }
    
    AUD_TO_USD = 0.67;
    USD_TO_AUD = 1.50;
    calculateTotalPaymentAmount(): void {
        this.stockpayment.paymentAmount = 0;  
        this.StockPaymentOrderItemList.forEach((item) => {
            const itemAmount = parseFloat(item.amount); 
            if (!isNaN(itemAmount)) {  
                this.stockpayment.paymentAmount += itemAmount;  
            }
        });
        this.convertCurrency();
    }
    convertCurrency(): void {
        debugger
        if (this.selectedCurrencyDisplayName === 'AUD') {
            this.stockpayment.rate = 1 ;
            this.isRateDisabled = true;
            this.stockpayment.aud = this.stockpayment.paymentAmount;
            this.stockpayment.usd = this.stockpayment.paymentAmount *this.AUD_TO_USD;
        } else if (this.selectedCurrencyDisplayName === 'USD') {
            this.isRateDisabled = false;
            this.stockpayment.rate = 0 ;
            this.stockpayment.usd = this.stockpayment.paymentAmount;
            this.stockpayment.aud = this.stockpayment.paymentAmount * this.USD_TO_AUD;
        }
        else if(this.selectedCurrencyDisplayName === 'CNY'){
            this.isRateDisabled = false;
            this.stockpayment.rate = 0 ;
        }
    }
    onCurrencyChange(): void {
        debugger;
        const selectedCurrency = this.allCurrency.find(item => item.id === Number(this.stockpayment.currencyId));

        if (selectedCurrency) {
            this.selectedCurrencyDisplayName = selectedCurrency.displayName;
        } else {
            this.selectedCurrencyDisplayName = null; 
        }
    this.calculateTotalPaymentAmount();
    }
 
    removeStockPaymentOrderItem(OrderItem): void {
        debugger
        if (this.StockPaymentOrderItemList.length == 1)
            return;
       
        if (this.StockPaymentOrderItemList.indexOf(OrderItem) === -1) {
            
        } else {
            this.StockPaymentOrderItemList.splice(this.StockPaymentOrderItemList.indexOf(OrderItem), 1);
        }
        this.calculateTotalPaymentAmount();
    }
    addStockPaymentOrderItem(): void {
        debugger    
        let StockPaymentOrderItemCreateOrEdit = { id: 0,orderNo:'', amount: 0 }
        this.StockPaymentOrderItemList.push(StockPaymentOrderItemCreateOrEdit);
        
    }
    onFileChange(event) {
        debugger
       
        if (event.target.files && event.target.files[0]) {
            const file = event.target.files[0];
         
            
            var filesAmount = event.target.files.length;
            for (let i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = (event: any) => {
                    this.images.push(event.target.result);
                }

                reader.readAsDataURL(event.target.files[i]);

                const file = event.target.files[i];
                this.fileList.push(file)
            }
        }
    }
    save(): void {
        debugger;
        this.STR = `${this.tenantId},${this.StockPaymentId},${this.orgId},${this.stockpayment.stockNo}`;
        this.showMainSpinner();
        this.saving = true;
        this.stockpayment.paymentDate = this.paymentDate;
        
        // Map the order list items
        this.stockpayment.stockPaymentOrderlist = [];
        this.StockPaymentOrderItemList.map((item) => {
            let PurchaseOrderItemList = new StockPaymentOrderListDto();
            PurchaseOrderItemList.id = item.id;
            PurchaseOrderItemList.purchaseOrderId = item.purchaseOrderId;
            PurchaseOrderItemList.stockPaymentId = item.stockPaymentId;
            PurchaseOrderItemList.amount = item.amount;
            this.stockpayment.stockPaymentOrderlist.push(PurchaseOrderItemList);
        });
    
        if (this.fileList.length > 0) {
            // Files are selected, proceed with the upload
            const formData = new FormData();
            for (let i = 0; i < this.fileList.length; i++) {
                const file = this.fileList[i];
                let name = this.fileName + "_" + i;
                formData.append(name, file, file.name);
            }
    
            
            this._httpClient.post<any>(this.uploadUrl + "?str=" + encodeURIComponent(this.STR), formData)
           
                .subscribe(response => {
                    if (response.success) {
                        debugger;
                        
                        this.stockpayment.filePath = response.result.filePath;
                        this.stockpayment.fileName = response.result.fileName;

                        this.saveStockPayment();
                    }
                     else 
                    {
                        this.notify.error(this.l('Upload failed.'));
                        this.hideMainSpinner();
                    }
                }, );
        }
         else {
            
            this.saveStockPayment();
        }
    }
    downloadfile(file,fileName): void {
        debugger;
        let FileName = AppConsts.docUrl + file + "/" + fileName;

        window.open(FileName, "_blank");

    }
    
    saveStockPayment(): void {
        this._stockPaymentServiceProxy.createOrEdit(this.stockpayment)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.modal.hide();
                this.modalSave.emit(null);
                this.hideMainSpinner();
            }, 
            e => {
                this.notify.error(this.l('SaveFailed'));
            });
    }
   
    // save(): void {
    //     debugger;
    //     this.STR = `${this.tenantId},${this.StockPaymentId},${this.orgId},${this.stockpayment.stockNo}`;
    //     this.showMainSpinner();
    
    //     this.saving = true;
    //     this.stockpayment.paymentDate = this.paymentDate;
    
    //     this.stockpayment.stockPaymentOrderlist = [];
    //     this.StockPaymentOrderItemList.map((item) => {
    //         let PurchaseOrderItemList = new StockPaymentOrderListDto();
    //         PurchaseOrderItemList.id = item.id;
    //         PurchaseOrderItemList.purchaseOrderId = item.purchaseOrderId;
    //         PurchaseOrderItemList.stockPaymentId = item.stockPaymentId;
    //         PurchaseOrderItemList.amount = item.amount;
    //         this.stockpayment.stockPaymentOrderlist.push(PurchaseOrderItemList);
    //     });

    //     this._stockPaymentServiceProxy.createOrEdit(this.stockpayment)
    //         .pipe(finalize(() => { this.saving = false; }))
    //         .subscribe(() => {
    //             this.notify.info(this.l('SavedSuccessfully'));
    
                
    //             if (this.fileList.length > 0) {
    //                 const formData = new FormData();
    //                 for (let i = 0; i < this.fileList.length; i++) {
    //                     const file = this.fileList[i];
    //                     let name = this.fileName + "_" + i;
    //                     formData.append(name, file, file.name);
    //                 }

    //                 this._httpClient.post<any>(this.uploadUrl + "?str=" + encodeURIComponent(this.STR), formData)
    //                     .subscribe(response => {
    //                         if (response.success) {
    //                             this.saveDoc = true;
    //                             this.modal.hide();
    //                             this.modalSave.emit(null);
    //                         } else if (response.error != null) {
    //                             this.notify.error(this.l('Upload failed.'));
    //                         }
    //                         this.hideMainSpinner();
    //                     }, e => {
    //                         this.hideMainSpinner();
    //                     });
    //             } else {
    //                 this.notify.info("Please select an image to upload.", "Empty");
    //                 this.modal.hide();
    //                 this.modalSave.emit(null);
    //             }
    
                
    //             if (this.stockpayment.id) {
    //                 let log = new UserActivityLogDto();
    //                 log.actionId = 82;
    //                 log.actionNote = 'Stock Payment Updated: ' + this.stockpayment.stockNo;
    //                 log.section = 'Stock Payment';
    //                 this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
    //                     .subscribe(() => {});
    //             } else {
    //                 let log = new UserActivityLogDto();
    //                 log.actionId = 81;
    //                 log.actionNote = 'Stock Payment Created: ' + this.stockpayment.stockNo;
    //                 log.section = 'Stock Payment';
    //                 this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
    //                     .subscribe(() => {});
    //             }
    //         });
    // }
    
    // save(): void {
    //     debugger;
        
    //     this.STR = `${this.tenantId},${this.StockPaymentId},${this.orgId}`;
    //     this.showMainSpinner();
    //     const formData = new FormData();
    //     if (this.fileList.length > 0) {
    //         for (let i = 0; i < this.fileList.length; i++) {
    //             const file = this.fileList[i];
    //             let name = this.fileName + "_" + i;
    //             formData.append(name, file, file.name);
    //         }
    //         this._httpClient
    //             .post<any>(this.uploadUrl + "?str=" + encodeURIComponent(this.STR), formData)
    //             .subscribe(response => {
    //                 if (response.success) {
    //                     this.saveDoc = true;
    //                 } else if (response.error != null) {
    //                     this.notify.error(this.l('Upload failed.'));
    //                 }
    //                 this.hideMainSpinner();
    //             }, e => {
    //                 this.hideMainSpinner();
    //             });
    //     } else {
    //         this.notify.info("Please select an image to upload.", "Empty");
    //     }
    //         this.saving = true;
            
    //         this.stockpayment.paymentDate = this.paymentDate; 
           

    //          this.stockpayment.stockPaymentOrderlist = [];
    //          this.StockPaymentOrderItemList.map((item) => {
    //             let PurchaseOrderItemList= new StockPaymentOrderListDto();
    //             PurchaseOrderItemList.id = item.id;
    //             PurchaseOrderItemList.purchaseOrderId = item.purchaseOrderId;
    //             PurchaseOrderItemList.stockPaymentId = item.stockPaymentId;
    //             PurchaseOrderItemList.amount = item.amount;
                
    //              this.stockpayment.stockPaymentOrderlist.push(PurchaseOrderItemList);
    //          });
            
    //         this._stockPaymentServiceProxy.createOrEdit(this.stockpayment)
    //          .pipe(finalize(() => { this.saving = false;}))
    //          .subscribe(() => {
    //             this.notify.info(this.l('SavedSuccessfully'));
    //             this.modal.hide();
    //             this.modalSave.emit(null);
    //             if(this.stockpayment.id){
    //                 let log = new UserActivityLogDto();
    //                 log.actionId = 82;
    //                 log.actionNote ='Stock Payment Updated : '+ this.stockpayment.stockNo;
    //                 log.section = 'Stock Payment';
    //                 this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
    //                     .subscribe(() => {
    //                 }); 
    //             }else{
    //                 let log = new UserActivityLogDto();
    //                 log.actionId = 81;
    //                 log.actionNote ='Stock Payment Created : '+ this.stockpayment.stockNo;
    //                 log.section = 'Stock Payment';
    //                 this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
    //                     .subscribe(() => {
    //                 }); 
    //             }

    //          });
             
    // }
}