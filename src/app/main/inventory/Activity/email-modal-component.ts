import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { LeadsServiceProxy, ActivityLogServiceProxy, GetVendorForSMSEmailDto,SMSEmailDto,GetLeadForSMSEmailTemplateDto, JobsServiceProxy,StockOrderActivityServiceProxy, CommonLookupDto, CreateOrEditWholeSaleLeadDto } from '@shared/service-proxies/service-proxies';
import { EmailEditorComponent } from 'angular-email-editor';
import * as _ from 'lodash';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from "rxjs/operators";
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
@Component({
    selector: 'EmailModal',
    templateUrl: './email-modal-component.html',
})
export class EmailModalComponent extends AppComponentBase {
    @ViewChild('LeadSMSModal', { static: true }) modal: ModalDirective;
    
    @ViewChild(EmailEditorComponent)
    private emailEditor: EmailEditorComponent;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    installerinvoiceId =0;
    ExpandedViewApp: boolean = true;
    active = false;    
    activityLog: SMSEmailDto = new SMSEmailDto();
    saving = false;
    role: string = '';
    total = 0;
    credit = 0;
    allSMSTemplates: CommonLookupDto[];
    leadCompanyName: any;
    activityName = '';
    fromemails: CommonLookupDto[];
    emailFrom : any;
    allEmailTemplates: CommonLookupDto[];
    wholeSalelead : CreateOrEditWholeSaleLeadDto;
    ccbox = false;
    bccbox = false;
    emailData = '';
    smsData = '';
    templateBody : any;
    vendor : GetVendorForSMSEmailDto = new GetVendorForSMSEmailDto();
    templateData : GetLeadForSMSEmailTemplateDto = new GetLeadForSMSEmailTemplateDto();
    constructor(injector: Injector
      , private _activityLogServiceProxy : ActivityLogServiceProxy 
     
         , private _jobsServiceProxy : JobsServiceProxy
         , private _StockOrderActivityServiceProxy : StockOrderActivityServiceProxy
         , private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
          private _leadsServiceProxy : LeadsServiceProxy
        ,
        private sanitizer: DomSanitizer
        ) {
        super(injector);
        this.wholeSalelead = new CreateOrEditWholeSaleLeadDto();
        this.activityLog = new  SMSEmailDto();
        this.activityLog.emailTemplateId = 0;
    }
    sectionName = '';
    show(PurchaseOrderID : number, sectionId : number,section : string) {
      this.showMainSpinner();
      // this.installerinvoiceId = InvoiceId;
      this.sectionName=section;
      let log = new UserActivityLogDto();
      log.actionId = 79;
      log.actionNote ='Open Email';
      
      log.section = this.sectionName;
      this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
          .subscribe(() => {
      });
      this.activityLog = new SMSEmailDto();
      this.vendor = new GetVendorForSMSEmailDto();
      this.templateData = new GetLeadForSMSEmailTemplateDto();
      this.activityLog.purchaseOrderId = PurchaseOrderID;
        this.activityLog.trackerId = sectionId;
        this.activityLog.body = "";
        // this.activityLog.id = Id;
        this.activityLog.emailTemplateId = 0;
        this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
            this.role = result;
          },err => {
            this.hideMainSpinner(); 
        });

        this._StockOrderActivityServiceProxy.getVendorForSMSEmailActivity(PurchaseOrderID).subscribe(result => {
          this.vendor = result;
         
           this.templateData = new GetLeadForSMSEmailTemplateDto();
      })

      //   this._leadsServiceProxy.getallEmailTemplates(PurchaseOrderID).subscribe(result => {
      //       this.allEmailTemplates = result;
      //   },err => {
      //     this.hideMainSpinner(); 
      // });

        this._leadsServiceProxy.getOrgWiseDefultandownemailadd(PurchaseOrderID).subscribe(result => {
            debugger;
            this.fromemails = result;
            this.activityLog.emailFrom = this.fromemails[0].displayName;
        },err => {
          this.hideMainSpinner(); 
      });
      this.hideMainSpinner(); 
      this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;                
    }
    expandApp() {
        this.ExpandedViewApp = false;
    }

    save(){
      this.saving = true;
      this._StockOrderActivityServiceProxy.sendEmail(this.activityLog)
      .pipe(finalize(() => { this.saving = false; }))
      .subscribe(() => {
        this.notify.info(this.l('EmailSendSuccessfully'));
        this.modal.hide();
        this.modalSave.emit(null);
            let log = new UserActivityLogDto();
            log.actionId = 7;
            log.actionNote ='Email Sent';
            log.section = this.sectionName;
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        this.activityLog.body = "";
        this.activityLog.emailTemplateId = 0;
        this.activityLog.smsTemplateId = 0;
        this.activityLog.customeTagsId = 0;
        this.activityLog.subject = '';
      });
  }
    
    countCharcters(): void {

        if (this.role != 'Admin') {
          this.total = this.activityLog.body.length;
          this.credit = Math.ceil(this.total / 160);
          if (this.total > 320) {
            this.notify.warn(this.l('You Can Not Add more than 320 characters'));
          }
        }
        else {
          this.total = this.activityLog.body.length;
          this.credit = Math.ceil(this.total / 160);
        }
    }

    opencc(): void {
      this.ccbox = !this.ccbox;
    }
    openbcc(): void {
      this.bccbox = !this.bccbox;
    }
    getEmailTemplate(emailHTML,templateData : GetLeadForSMSEmailTemplateDto) {
      //let myTemplateStr = "Hello {{user.name}}, you are {{user.age.years}} years old";
      let myTemplateStr = emailHTML;
      //let addressValue = this.lead.address + " " + this.lead.suburb + ", " + this.lead.state + "-" + this.item.lead.postCode;
      let myVariables = {
        Customer: {
          Name: templateData.companyName,
          Address: templateData.address,
          Mobile: templateData.mobile,
          Email: templateData.email,
          Phone: templateData.phone,
          SalesRep: templateData.currentAssignUserName
        },
        Sales: {
          Name: templateData.currentAssignUserName,
          Mobile: templateData.currentAssignUserMobile,
          Email: templateData.currentAssignUserEmail
        },
        Quote: {
          ProjectNo: templateData.jobNumber,
          SystemCapacity: templateData.systemCapacity ,
          AllproductItem: templateData.qunityAndModelList,
          TotalQuoteprice: templateData.totalQuotaion,
          InstallationDate: templateData.installationDate,
          InstallerName: templateData.installerName,
          InvoiceAmount : templateData.invoiceAmount,
          ApproedAmount : templateData.approedAmount
        },
        Freebies: {
          DispatchedDate: templateData.dispatchedDate,
          TrackingNo: templateData.trackingNo,
          TransportCompany: templateData.transportCompanyName,
          TransportLink: templateData.transportLink,
          PromoType: templateData.promoType,
        },
        Invoice: {
          UserName: templateData.userName,
          UserMobile: templateData.userMobile,
          UserEmail: templateData.userEmail,
          Owning: templateData.owing,
        },
        Organization: {
          orgName: templateData.orgName,
          orgEmail: templateData.orgEmail,
          orgMobile: templateData.orgMobile,
          orglogo: AppConsts.docUrl + "/" + templateData.orglogo,
        },
        Review: {
         link: templateData.link,
        },
        Finance : {
          PaymentOption : templateData.paymentOption,
          FinPaymentType : templateData.finPaymentType,
          DepositOption : templateData.depositOption,
          FinancePurchaseNo : templateData.financePurchaseNo,
          FinanceAmount : templateData.financeAmount,
          FinanceDepositeAmount : templateData.financeDepositeAmount,
          FinanceNetAmount : templateData.financeNetAmount,
          PaymentTerm : templateData.paymentTerm,
          RepaymentAmount : templateData.repaymentAmount,

        }
      }
  
      // use custom delimiter {{ }}
      _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;
  
      // interpolate
      let compiled = _.template(myTemplateStr);
      let myTemplateCompiled = compiled(myVariables);
      return myTemplateCompiled;
    }

    saveDesign() {
      if (this.activityLog.emailTemplateId == 0) {
        this.saving = true;
        const emailHTML = this.activityLog.body;
        this.setHTML(emailHTML)
      }
      else {
        this.saving = true;
        this.emailEditor.editor.exportHtml((data) =>
          this.setHTML(data.html)
        );
      }
    }
    onTagChange(event): void {

      const id = event.target.value;
      if (id == 1) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Customer.Name}}";
        } else {
          this.activityLog.body = "{{Customer.Name}}";
        }
  
      } else if (id == 2) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Customer.Mobile}}";
        } else {
          this.activityLog.body = "{{Customer.Mobile}}";
        }
      } else if (id == 3) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Customer.Phone}}";
        } else {
          this.activityLog.body = "{{Customer.Phone}}";
        }
      } else if (id == 4) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Customer.Email}}";
        } else {
          this.activityLog.body = "{{Customer.Email}}";
        }
      } else if (id == 5) {
  
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Customer.Address}}";
        } else {
          this.activityLog.body = "{{Customer.Address}}";
        }
      } else if (id == 6) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Sales.Name}}";
        } else {
          this.activityLog.body = "{{Sales.Name}}";
        }
      } else if (id == 7) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Sales.Mobile}}";
        } else {
          this.activityLog.body = "{{Sales.Mobile}}";
        }
      } else if (id == 8) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Sales.Email}}";
        } else {
          this.activityLog.body = "{{Sales.Email}}";
        }
      }
      else if (id == 9) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Quote.ProjectNo}}";
        } else {
          this.activityLog.body = "{{Quote.ProjectNo}}";
        }
      }
      else if (id == 10) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Quote.SystemCapacity}}";
        } else {
          this.activityLog.body = "{{Quote.SystemCapacity}}";
        }
      }
      else if (id == 11) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Quote.AllproductItem}}";
        } else {
          this.activityLog.body = "{{Quote.AllproductItem}}";
        }
      }
      else if (id == 12) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Quote.TotalQuoteprice}}";
        } else {
          this.activityLog.body = "{{Quote.TotalQuoteprice}}";
        }
      }
      else if (id == 13) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Quote.InstallationDate}}";
        } else {
          this.activityLog.body = "{{Quote.InstallationDate}}";
        }
      }
      else if (id == 14) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Quote.InstallerName}}";
        } else {
          this.activityLog.body = "{{Quote.InstallerName}}";
        }
      }
      else if (id == 15) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Freebies.TransportCompany}}";
        } else {
          this.activityLog.body = "{{Freebies.TransportCompany}}";
        }
      }
      else if (id == 16) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Freebies.TransportLink}}";
        } else {
          this.activityLog.body = "{{Freebies.TransportLink}}";
        }
      }
      else if (id == 17) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Freebies.DispatchedDate}}";
        } else {
          this.activityLog.body = "{{Freebies.DispatchedDate}}";
        }
      }
      else if (id == 18) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Freebies.TrackingNo}}";
        } else {
          this.activityLog.body = "{{Freebies.TrackingNo}}";
        }
      }
      else if (id == 19) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Freebies.PromoType}}";
        } else {
          this.activityLog.body = "{{Freebies.PromoType}}";
        }
      }
      else if (id == 20) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Invoice.UserName}}";
        } else {
          this.activityLog.body = "{{Invoice.UserName}}";
        }
      }
      else if (id == 21) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Invoice.UserMobile}}";
        } else {
          this.activityLog.body = "{{Invoice.UserMobile}}";
        }
      }
      else if (id == 22) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Invoice.UserEmail}}";
        } else {
          this.activityLog.body = "{{Invoice.UserEmail}}";
        }
      }
      else if (id == 23) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Invoice.Owning}}";
        } else {
          this.activityLog.body = "{{Invoice.Owning}}";
        }
      }
      else if (id == 24) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Organization.orgName}}";
        } else {
          this.activityLog.body = "{{Organization.orgName}}";
        }
      }
      else if (id == 25) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Organization.orgEmail}}";
        } else {
          this.activityLog.body = "{{Organization.orgEmail}}";
        }
      }
      else if (id == 26) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Organization.orgMobile}}";
        } else {
          this.activityLog.body = "{{Organization.orgMobile}}";
        }
      }
      else if (id == 27) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Organization.orglogo}}";
        } else {
          this.activityLog.body = "{{Organization.orglogo}}";
        }
      }
      else if (id == 28) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Review.link}}";
        } else {
          this.activityLog.body = "{{Review.link}}";
        }
      }        
      else if (id == 29) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Finance.PaymentOption}}";
        } else {
          this.activityLog.body = "{{Finance.PaymentOption}}";
        }
      }      
      else if (id == 30) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Finance.FinPaymentType}}";
        } else {
          this.activityLog.body = "{{Finance.FinPaymentType}}";
        }
      }      
      else if (id == 31) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Finance.DepositOption}}";
        } else {
          this.activityLog.body = "{{Finance.DepositOption}}";
        }
      }      
      else if (id == 32) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Finance.FinancePurchaseNo}}";
        } else {
          this.activityLog.body = "{{Finance.FinancePurchaseNo}}";
        }
      }      
      else if (id == 33) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Finance.FinanceAmount}}";
        } else {
          this.activityLog.body = "{{Finance.FinanceAmount}}";
        }
      }      
      else if (id == 34) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Finance.FinanceDepositeAmount}}";
        } else {
          this.activityLog.body = "{{Finance.FinanceDepositeAmount}}";
        }
      }      
      else if (id == 35) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Finance.FinanceNetAmount}}";
        } else {
          this.activityLog.body = "{{Finance.FinanceNetAmount}}";
        }
      }      
      else if (id == 36) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Finance.PaymentTerm}}";
        } else {
          this.activityLog.body = "{{Finance.PaymentTerm}}";
        }
      }      
      else if (id == 37) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Finance.RepaymentAmount}}";
        } else {
          this.activityLog.body = "{{Finance.RepaymentAmount}}";
        }
      }
      else if (id == 38) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Quote.InvoiceAmount}}";
        } else {
          this.activityLog.body = "{{Quote.InvoiceAmount}}";
        }
      }
      else if (id == 39) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Quote.ApproedAmount}}";
        } else {
          this.activityLog.body = "{{Quote.ApproedAmount}}";
        }
      }

    }
  
    setHTML(emailHTML) {
      debugger;
      var ed = '';
      ed = emailHTML;
      if(!ed.includes('{{')){
        this.activityLog.body = emailHTML;
        this.save();
        return;
      }

      this.showMainSpinner();
            if(!this.templateData.id || this.templateData.id == 0){
              this._activityLogServiceProxy.getLeadForSMSEmailTemplate(this.activityLog.purchaseOrderId,0,this.installerinvoiceId).subscribe(result => {
                this.templateData = result;
                let htmlTemplate = this.getEmailTemplate(emailHTML,result);
                this.activityLog.body = htmlTemplate;
                this.save();
                this.hideMainSpinner(); 
              }, err => {
                  this.hideMainSpinner(); 
              });
            }
            else{
              let htmlTemplate = this.getEmailTemplate(emailHTML,this.templateData);
              this.activityLog.body = htmlTemplate;
              this.save();
              this.hideMainSpinner(); 
            }
      
      
    }
  
    
  
    editorLoaded() {

      this.activityLog.body = "";
      if ((this.activityLog.emailTemplateId != 0 && this.activityLog.emailTemplateId !== null && this.activityLog.emailTemplateId !== undefined)) {
        this._jobsServiceProxy.getEmailTemplateForEditForEmail(this.activityLog.emailTemplateId).subscribe(result => {
          this.activityLog.subject = result.emailTemplate.subject;
          this.emailData = result.emailTemplate.body;
          if (this.emailData != "") {
            this.showMainSpinner();
            if(!this.templateData.id || this.templateData.id == 0){
              this._activityLogServiceProxy.getLeadForSMSEmailTemplate(this.activityLog.purchaseOrderId,0,this.installerinvoiceId).subscribe(result => {
                this.templateData = result;
                this.emailData = this.getEmailTemplate(this.emailData,result);
                this.emailEditor.editor.loadDesign(JSON.parse(this.emailData));
                this.hideMainSpinner(); 
              }, err => {
                  this.hideMainSpinner(); 
              });
            }
            else{
              this.emailData = this.getEmailTemplate(this.emailData, this.templateData);
              this.emailEditor.editor.loadDesign(JSON.parse(this.emailData));
              this.hideMainSpinner();
            }
            
            
          }
        });
      }
  
    }
}
