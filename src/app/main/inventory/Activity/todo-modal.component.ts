import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import {  UserActivityLogDto, UserActivityLogServiceProxy, GetVendorForSMSEmailDto, CommonLookupDto, StockOrderActivityLogInput, GetLeadForActivityOutput, GetLeadForSMSEmailTemplateDto, JobsServiceProxy, WholeSaleActivityServiceProxy, WholeSaleActivitylogInput, WholeSaleLeadServiceProxy, StockOrderActivityServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'todosModal',
    templateUrl: './todo-modal.component.html',
})
export class StockOrderToDoModalComponent extends AppComponentBase {
    @ViewChild('todoModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    ExpandedViewApp: boolean = true;
    active = false;    
    vendor : GetVendorForSMSEmailDto = new GetVendorForSMSEmailDto();
    activityLog : StockOrderActivityLogInput = new StockOrderActivityLogInput();
    activitytype : number;
    
    saving = false;   
    userName = '';
    allUsers: CommonLookupDto[];
    header = "";
    constructor(injector: Injector
        , private _jobsServiceProxy :JobsServiceProxy
        , private _activityLogServiceProxy : StockOrderActivityServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy) {
        super(injector);
        this.vendor = new GetVendorForSMSEmailDto();
        this.activityLog = new StockOrderActivityLogInput();
    }
    sectionName=""
    show(PurchaseId: number, sectionId:number,section = '') {

        this.showMainSpinner();

        this.activityLog = new StockOrderActivityLogInput();
        this.vendor = new GetVendorForSMSEmailDto();
        this._activityLogServiceProxy.getVendorForSMSEmailActivity(PurchaseId).subscribe(result => {
            this.vendor = result;
           this.activityLog.purchaseOrderId=PurchaseId;
           this.activityLog.sectionId=sectionId;
           
            this._jobsServiceProxy.getUsersListForToDo().subscribe(result => {
                this.allUsers = result;
            }, err => {
                this.hideMainSpinner(); 
            });
            this.modal.show();  
            this.sectionName =section;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Open ToDo';
        log.section = section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });

        this.hideMainSpinner(); 
        }, err => {
            this.hideMainSpinner(); 
        });
               
    }
    
    save(){
        this.saving = true;
        this._activityLogServiceProxy.addToDoActivityLog(this.activityLog)
        .pipe(finalize(() => { this.saving = false; }))
        .subscribe(() => {
          this.notify.info(this.l('SavedSuccessfully'));
          this.modal.hide();
          this.modalSave.emit(null);
          let log = new UserActivityLogDto();
          log.actionId = 25;
          log.actionNote ='ToDo';
          log.section = this.sectionName;
          this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
              .subscribe(() => {
          });
        });
    }
    close(): void {
        this.saving = false;
        this.modal.hide();
    }

    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;                
    }
    expandApp() {
        this.ExpandedViewApp = false;
    }
}
