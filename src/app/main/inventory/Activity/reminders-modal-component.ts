import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import {  UserActivityLogDto,UserActivityLogServiceProxy, GetVendorForSMSEmailDto, LeadsServiceProxy, StockOrderActivityLogInput, StockOrderActivityServiceProxy, WholeSaleActivitylogInput, WholeSaleLeadServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import * as moment from 'moment';
@Component({
    selector: 'RemindersModal',
    templateUrl: './reminders-modal-component.html',
})
export class RemindersModalComponent extends AppComponentBase {
    @ViewChild('RemindersModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    
    // CurrentDate = new Date();

    ExpandedViewApp: boolean = true;
    active = false;    
    vendor : GetVendorForSMSEmailDto = new GetVendorForSMSEmailDto();
    activityLog : StockOrderActivityLogInput = new StockOrderActivityLogInput();
    saving = false;   
    userName = '';
    header ='';
    constructor(injector: Injector
        , private _activityLogServiceProxy : StockOrderActivityServiceProxy   
        , private _leadsServiceProxy : LeadsServiceProxy   
        ,private _userActivityLogServiceProxy : UserActivityLogServiceProxy  
        )
         {
            super(injector);
            this.vendor = new GetVendorForSMSEmailDto();
            this.activityLog = new StockOrderActivityLogInput();
    }
    sectionName = '';
    show(PurchaseOrderID: number, sectionId:number,section = '') {
      
        this.showMainSpinner();

        this._activityLogServiceProxy.getVendorForSMSEmailActivity(PurchaseOrderID).subscribe(result => {
            this.vendor = result;
            this.activityLog = new StockOrderActivityLogInput();
            this.activityLog.purchaseOrderId = PurchaseOrderID;
            this.activityLog.sectionId = sectionId;
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Reminder';
            log.section = section;
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            
        })
        this._leadsServiceProxy.getCurrentUserIdName().subscribe(result => {
            this.userName = result;

        } )
            this.modal.show(); 

            this.hideMainSpinner(); 
        }
                
    

    close(): void {
        this.saving = false;
        this.modal.hide();
    }

    save(){
        debugger;
        this.saving = true;
        
        let date = moment(new Date(), "DD/MM/YYYY");

        let ActivityDate = moment(this.activityLog.activityDate, "DD/MM/YYYY");
        
        if (ActivityDate < date) {
            this.notify.warn(this.l('Please Select Date Greater Date than Current Date'));
            return;
        }

        this._activityLogServiceProxy.addReminderActivityLog(this.activityLog)
        .pipe(finalize(() => { this.saving = false; }))
        .subscribe(() => {
          this.notify.info(this.l('SavedSuccessfully'));
          this.modal.hide();
          this.modalSave.emit(null);
          let log = new UserActivityLogDto();
            log.actionId = 8;
            log.actionNote ='Reminder Set';
            log.section = this.sectionName;
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        });
    }
    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;                
    }
    expandApp() {
        this.ExpandedViewApp = false;
    }
}
