import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { StockOrderActivityServiceProxy, StockOrderActivityLogInput, UserActivityLogDto, UserActivityLogServiceProxy,  GetVendorForSMSEmailDto, LeadsServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'CommentsModal',
    templateUrl: './comments-modal.component.html',
})
export class CommentsModalComponent extends AppComponentBase {
    @ViewChild('CommentModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    

    ExpandedViewApp: boolean = true;
    active = false;    
    vendor : GetVendorForSMSEmailDto = new GetVendorForSMSEmailDto();
    activityLog : StockOrderActivityLogInput = new StockOrderActivityLogInput();
    saving = false;   
    userName = '';
    header ='';
    constructor(injector: Injector
        , private _activityLogServiceProxy : StockOrderActivityServiceProxy   
        , private _leadsServiceProxy : LeadsServiceProxy  ,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy   
        )
         {
            super(injector);
            this.vendor = new GetVendorForSMSEmailDto();
            this.activityLog = new StockOrderActivityLogInput();
    }
    sectionName = '';
    show(PurchaseOrderID: number, sectionId:number,section = '') {
      
        this.showMainSpinner();

        this._activityLogServiceProxy.getVendorForSMSEmailActivity(PurchaseOrderID).subscribe(result => {
            this.vendor = result;
            this.activityLog = new StockOrderActivityLogInput();
            this.activityLog.purchaseOrderId = PurchaseOrderID;
            this.activityLog.sectionId = sectionId;
            
        })
        this._leadsServiceProxy.getCurrentUserIdName().subscribe(result => {
            this.userName = result;

        } )
            this.modal.show(); 
            this.sectionName =section;
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Comment';
            log.section = section;
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.hideMainSpinner(); 
        }
          
        save(){
            debugger;
            this.saving = true;
            this._activityLogServiceProxy.addCommentActivityLog(this.activityLog)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.modal.hide();
                this.modalSave.emit(null);
                let log = new UserActivityLogDto();
                log.actionId = 24;
                log.actionNote ='Comment';
                log.section = this.sectionName;
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
            
        }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;                
    }
    expandApp() {
        this.ExpandedViewApp = false;
    }
}
