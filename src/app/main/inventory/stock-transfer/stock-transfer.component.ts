import { Component, Injector, ViewEncapsulation, ViewChild, OnInit, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLookupDto, CommonLookupServiceProxy, GetJobForViewDto, GetQuotationForViewDto, InvoicePaymentInvoicePaymentMethodLookupTableDto, InvoicePaymentsServiceProxy, JobFinanceOptionLookupTableDto, JobPaymentOptionLookupTableDto, JobsServiceProxy, JobStatusTableDto, LeadsServiceProxy, LeadStateLookupTableDto, OrganizationUnitDto, QuotationsServiceProxy, ReportServiceProxy, TokenAuthServiceProxy, UserServiceProxy, TexInvoiceDto, PaymentReceiptDto } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { AppConsts } from '@shared/AppConsts';
import { HttpClient } from '@angular/common/http';
import { finalize } from 'rxjs/operators';
import { ViewApplicationModelComponent } from '@app/main/jobs/jobs/view-application-model/view-application-model.component';
import { AddActivityModalComponent } from '@app/main/myleads/myleads/add-activity-model.component';
import { ViewMyLeadComponent } from '@app/main/myleads/myleads/view-mylead.component';
import { CreateOrEditJobInvoiceModalComponent } from '@app/main/jobs/jobs/edit-jobinvoice-model.component';
import { FileUpload } from 'primeng/fileupload';
import { DomSanitizer, SafeHtml, Title } from '@angular/platform-browser';
import { StockTransferDetailModal } from './stock-transfer-view-modal.component';
import { CreateStockTransferModal } from './create-stock-transfer-modal.component';

@Component({
    templateUrl: './stock-transfer.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class StockTransferComponent extends AppComponentBase implements OnInit {
    @ViewChild('StockTransferDetailModal', { static: true }) StockTransferDetailModal: StockTransferDetailModal;      
    @ViewChild('CreateStockTransferModal', { static: true }) CreateStockTransferModal: CreateStockTransferModal;      
    
    FiltersData = false;
    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    organizationUnitlength: number = 0;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    RefundDateRange: moment.Moment[];
    ActualPayDateRange: moment.Moment[];
    reportPath: string;
    maxRefundDateFilter: moment.Moment;
    minRefundDateFilter: moment.Moment;
    maxPaymentNumberFilter: number;
    maxPaymentNumberFilterEmpty: number;
    minPaymentNumberFilter: number;
    minPaymentNumberFilterEmpty: number;
    isVerifiedFilter = -1;
    receiptNumberFilter = '';
    maxActualPayDateFilter: moment.Moment;
    minActualPayDateFilter: moment.Moment;
    paidCommentFilter = '';
    jobNoteFilter = '';
    userNameFilter = '';
    userName2Filter = '';
    userName3Filter = '';
    jobPaymentOption = 0;
    organizationUnit = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    totalpaymentData = 0;
    totalpaymentVerifyData = 0;
    totalpaymentnotVerifyData = 0;
    totalcount: GetJobForViewDto;
    alljobstatus: JobStatusTableDto[];
    allStates: LeadStateLookupTableDto[];
    suburbSuggestions: string[];
    allInvoicePaymentMethods: InvoicePaymentInvoicePaymentMethodLookupTableDto[];
    allInvoicePaymentStatus: CommonLookupDto[];
    jobFinOptions: JobFinanceOptionLookupTableDto[];
    postCodeSuburbFilter = '';
    stateNameFilter = '';
    jobStatusID: [];
    paymentMethodId = 0;
    invoicePaymentStatusId = 0;
    financeOptionId = 0;
    totalAmountOfInvoice = "0";
    totalAmountReceived = 0;
    balanceOwning = 0;
    invoiceDateNameFilter = 'InstallDate';
    
    date = new Date();
    firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    startDate = moment(this.firstDay);
    endDate = moment().endOf('day');

    postalcodefrom = '';
    postalcodeTo = '';
    areaNameFilter = '';
    installerid = 0;
    ExpandedView: boolean = true;
    SelectedLeadId: number = 0;
    JobQuotations: GetQuotationForViewDto[];
    jobPaymentOptions: JobPaymentOptionLookupTableDto[];
    totalCancelAmount = 0;
    totalCustAmountOfInvoice = 0;
    totalCustBAlOwning = 0;
    totalCustAmountReceived = 0;
    totalVICAmountOfInvoice = 0;
    totalVICAmountReceived = 0;
    totalVICBAlOwning = 0;
    totalRefundamount = 0;
    newtotalAmountReceived = "0";
    newbalanceOwning = "0";
    uploadUrl: string;
    invoicenamefilter= 'All';
    excelorcsvfile = 0;
    firstrowcount = 0;
    last = 0;
    vicrebate = "All";
    solarRebateStatus = 0;

    taxInvoiceData: TexInvoiceDto;
    paymentReceiptData: PaymentReceiptDto;

    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 330;

    toggle: boolean = true;

    change() {
        this.toggle = !this.toggle;
      }


    constructor(
        injector: Injector,
        private _invoicePaymentsServiceProxy: InvoicePaymentsServiceProxy,
        private _notifyService: NotifyService,
        private _commonLookupService: CommonLookupServiceProxy,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService,
        private _userServiceProxy: UserServiceProxy,
        private _router: Router,
        private _jobsServiceProxy: JobsServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _httpClient: HttpClient,
        private _reportServiceProxy: ReportServiceProxy,
        private _quotationsServiceProxy: QuotationsServiceProxy,
        private titleService: Title,
        private sanitizer: DomSanitizer
    ) {
        super(injector);
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/Users/ImportInvoicesFromExcel';
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        this.titleService.setTitle(this.appSession.tenancyName + " |  Wholesale Invoice");
       
        this._commonLookupService.getAllJobStatusTableDropdown().subscribe(result => {
            this.alljobstatus = result;
        });
        this._commonLookupService.getAllPaymentOptionForTableDropdown().subscribe(result => {
            this.jobPaymentOptions = result;
        });
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });

        this._invoicePaymentsServiceProxy.getAllInvoicePaymentMethodForTableDropdown().subscribe(result => {
            this.allInvoicePaymentMethods = result;
        });
        this._invoicePaymentsServiceProxy.getAllInvoiceStatusForTableDropdown().subscribe(result => {
            this.allInvoicePaymentStatus = result;
        });

        this._commonLookupService.getAllFinanceOptionForTableDropdown().subscribe(result => {
            this.jobFinOptions = result;
        });
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            const newLocal = this;
            newLocal.getInvoiceIssued();
            // this.getCount(this.organizationUnit);
        });
    }

    createLead(): void {
        this._router.navigate(['/app/main/wholesale/invoice/createEditWholesaleInvoice'], { queryParams: { OId: null } });
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }
        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }

    filtersuburbs(event): void {
        this._commonLookupService.getOnlySuburb(event.query).subscribe(output => {
            this.suburbSuggestions = output;
        });
    }

    getInvoiceIssued(event?: LazyLoadEvent) {
           debugger;
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        
        this.primengTableHelper.showLoadingIndicator();
        this._invoicePaymentsServiceProxy.getAllInvoiceIssuedData(
            '',
            this.filterText,
            this.jobNoteFilter,
            this.jobPaymentOption,
            this.organizationUnit,
            undefined,
            undefined,
            this.postCodeSuburbFilter,
            this.stateNameFilter,
            this.jobStatusID,
            this.paymentMethodId,
            this.invoicePaymentStatusId,
            this.financeOptionId,
            this.invoiceDateNameFilter,
            this.startDate,
            this.endDate,
            this.postalcodefrom,
            this.postalcodeTo,
            this.areaNameFilter,
            this.installerid,
            this.invoicenamefilter,
            undefined,
            this.vicrebate,
            this.solarRebateStatus,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {

            //console.log(result.items);

            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            this.shouldShow = false;
            if (result.totalCount > 0) {

                //this.totalpaymentData = result.items[0].totalpaymentData;
                // this.totalpaymentVerifyData = result.items[0].totalpaymentVerifyData;
                // this.totalpaymentnotVerifyData = result.items[0].totalpaymentnotVerifyData;
                this.totalAmountOfInvoice = (result.items[0].totalAmountOfInvoice.toFixed(2));
                // this.totalRefundamount = result.items[0].totalRefundamount;
                // this.totalAmountReceived = (result.items[0].totalAmountReceived - result.items[0].totalRefundamount);
                this.newtotalAmountReceived = (result.items[0].newTotalAmountReceived).toFixed(2);
                // this.balanceOwning = (result.items[0].totalAmountOfInvoice - result.items[0].totalAmountReceived);
                this.newbalanceOwning = (result.items[0].totalAmountOfInvoice - result.items[0].newTotalAmountReceived).toFixed(2);
                //this.totalCancelAmount = (result.items[0].totalCancelAmount);
                this.totalVICAmountOfInvoice = (result.items[0].totalVICAmountOfInvoice) + (result.items[0].totalVICLoanOfInvoice);
                this.totalCustAmountOfInvoice = (result.items[0].totalAmountOfInvoice) - (this.totalVICAmountOfInvoice);
                this.totalCustAmountReceived = (result.items[0].totalCustAmountReceived);
                this.totalCustBAlOwning = (this.totalCustAmountOfInvoice - result.items[0].totalCustAmountReceived);
                this.totalVICAmountReceived = result.items[0].totalVICAmmointReceived;
                this.totalVICBAlOwning = (this.totalVICAmountOfInvoice - this.totalVICAmountReceived);
            } else {
                this.totalpaymentData = 0;
                this.totalpaymentVerifyData = 0;
                this.totalpaymentnotVerifyData = 0;
                this.totalAmountOfInvoice = "0";
                this.totalRefundamount = 0;
                this.totalAmountReceived = 0;
                this.balanceOwning = 0;
                this.totalCancelAmount = 0;
                this.totalCustAmountOfInvoice = 0;
                this.totalCustAmountReceived = 0;
                this.totalCustBAlOwning = 0;
                this.totalVICAmountOfInvoice = 0;
                this.totalVICAmountReceived = 0;
                this.totalVICBAlOwning = 0;
                this.newtotalAmountReceived = "0";
                this.newbalanceOwning = "0";
            }
        },
        err => {
            this.primengTableHelper.hideLoadingIndicator();
        });
        // this.getCount(this.organizationUnit);
    }
    getJobQuotations(jobid): void {
        this._quotationsServiceProxy.getAll(jobid, '', 0, 9999).subscribe(result => {

            this.JobQuotations = result.items;
            if (result.items.length > 0) {
                this.reportPath = this.JobQuotations[0].quotation.quoteFilePath;
            } else {
            }
        })
    }

    htmlStr: any;
    taxInvoice(jobid): void {
        this.spinnerService.show();
        this._reportServiceProxy.taxInvoiceByJobId(jobid).subscribe(result => {
            this.taxInvoiceData = result;

            //console.log(this.taxInvoiceData);

            if(this.taxInvoiceData.viewHtml != '')
            {
                this.htmlStr = this.GetTaxInvoice(this.taxInvoiceData.viewHtml);
                this.htmlStr = this.sanitizer.bypassSecurityTrustHtml(this.htmlStr);

                let html = this.htmlStr.changingThisBreaksApplicationSecurity;
                this._commonLookupService.downloadPdf(this.taxInvoiceData.jobNumber, "TaxInvoice", html).subscribe(result => {
                    let FileName = result;
                    this.spinnerService.hide();
                    window.open(result, "_blank");
                },
                err => {
                    this.spinnerService.hide();
                });
            }
        },
        err => {
            this.spinnerService.hide();
        });
        
    }

    paymentReceipt(jobid): void {
        
        this.spinnerService.show();
        this._reportServiceProxy.paymentReceiptByJobId(jobid).subscribe(result => {
            this.paymentReceiptData = result;

            console.log(this.paymentReceiptData);

            if(this.paymentReceiptData.viewHtml != '')
            {
               
                let html = this.GetPaymentReceipt(this.paymentReceiptData.viewHtml);

                this._commonLookupService.downloadPdf(this.paymentReceiptData.jobNumber, "PaymetReceipt", html).subscribe(result => {
                    let FileName = result;
                    this.spinnerService.hide();
                    window.open(result, "_blank");
                },
                err => {
                    this.spinnerService.hide();
                });
            }
        },
        err => {
            this.spinnerService.hide();
        });

    }

    reloadPage($event): void {
        this.ExpandedView = true;
        this.paginator.changePage(this.paginator.getPage());
    }

    exportToExcel(excelorcsv): void {
        
        this.excelorcsvfile = excelorcsv
        this._invoicePaymentsServiceProxy.getAllInvoiceIssuedDataExcel(
            '',
            this.filterText,
            this.jobNoteFilter,
            this.jobPaymentOption,
            this.organizationUnit,
            undefined,
            undefined,
            this.postCodeSuburbFilter,
            this.stateNameFilter,
            this.jobStatusID,
            this.paymentMethodId,
            this.invoicePaymentStatusId,
            this.financeOptionId,
            this.invoiceDateNameFilter,
            this.startDate,
            this.endDate,
            this.postalcodefrom,
            this.postalcodeTo,
            this.areaNameFilter,
            this.installerid,
            this.invoicenamefilter,
            this.excelorcsvfile ,
            this.vicrebate,
            this.solarRebateStatus,
            undefined,
            undefined,
            undefined,
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    depositeexportToExcel(): void {
        
        this._invoicePaymentsServiceProxy.getAllInvoiceIssueddepositeDataExcel(
            '',
            undefined,
            undefined,
            undefined,
            this.organizationUnit,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    owningexportToExcel(): void {
       
        this._invoicePaymentsServiceProxy.getAllInvoiceIssuedOwningDataExcel(
            '',
            undefined,
            undefined,
            undefined,
            this.organizationUnit,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined
        )
        .subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
        });
    }

    clear() {
        this.filterText = '';
        this.jobPaymentOption = 0;
        this.ActualPayDateRange = null;
        this.postCodeSuburbFilter = '';
        this.stateNameFilter = '';
        this.jobStatusID = [];
        this.paymentMethodId = 0;
        this.invoicePaymentStatusId = 0;
        this.financeOptionId = 0;
        this.postalcodefrom = '';
        this.postalcodeTo = '';
        this.areaNameFilter = '';
        this.installerid = 0;
        this.invoiceDateNameFilter = 'InstallDate';
        let date = new Date();
        let firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
        this.startDate = moment(this.firstDay);
        this.endDate = moment().endOf('day');
        
        this.getInvoiceIssued();
    }

    quickview(jobid): void {
        //this.viewApplicationModal.show(jobid);
    }

    navigateToLeadDetail(leadid): void {
        this.ExpandedView = !this.ExpandedView;
        this.ExpandedView = false;
        this.SelectedLeadId = leadid;
        //this.viewLeadDetail.showDetail(leadid,null,9);
    }

    
    expandGrid() {
        this.ExpandedView = true;
    }
    
    GetTaxInvoice(html) {
        let htmlstring = html;

        let myTemplateStr = htmlstring;
        let TableList = [];

        if (this.taxInvoiceData.qunityAndModelLists != null) {
            this.taxInvoiceData.qunityAndModelLists.forEach(obj => {
                TableList.push("<div class='mb5'>" + obj.name + "</div>");
            })
        }

        var netCost = this.taxInvoiceData.netCost != "" ? parseFloat(this.taxInvoiceData.netCost) : 0;
        var gst = (netCost * 10) / 100;

        let myVariables = {
            TI: {
                JobNumber: this.taxInvoiceData.jobNumber,
                Date: this.taxInvoiceData.date,
                BalanceDue: this.taxInvoiceData.balanceDue,

                Cust: {
                    Name: this.taxInvoiceData.name,
                    Mobile: this.taxInvoiceData.mobile,
                    Email: this.taxInvoiceData.email,
                    AddressLine1: this.taxInvoiceData.addressLine1,
                    AddressLine2: this.taxInvoiceData.addressLine2,
                    MeterPhase: this.taxInvoiceData.meterPhase,
                    MeterUpgrad: this.taxInvoiceData.meterUpgrad,
                    RoofType: this.taxInvoiceData.roofType,
                    PropertyType: this.taxInvoiceData.propertyType,
                    RoofPitch: this.taxInvoiceData.roofPitch,
                    ElecDist: this.taxInvoiceData.elecDist,
                    ElecRetailer: this.taxInvoiceData.elecRetailer,
                    NIMINumber: this.taxInvoiceData.nmiNumber,
                    MeterNo: this.taxInvoiceData.meterNo
                },

                PD: {

                    SystemDetails: TableList.toString().replace(/,/g, ''),
                    //SystemDetails: '',
                    TCost: this.taxInvoiceData.totalCost != "" && this.taxInvoiceData.totalCost != "0" ? this.taxInvoiceData.totalCost : "0.00",
                    TotalCost: this.taxInvoiceData.tCost != "" && this.taxInvoiceData.tCost != "0" ? this.taxInvoiceData.tCost : "0.00",
                    Stc: this.taxInvoiceData.stc != "" && this.taxInvoiceData.stc != "0" ? this.taxInvoiceData.stc : "0.00",
                    SolarRebate: this.taxInvoiceData.solarRebate != "" && this.taxInvoiceData.solarRebate != "0" ? this.taxInvoiceData.solarRebate : "0.00",
                    SolarLoan: this.taxInvoiceData.solarLoan != "" && this.taxInvoiceData.solarLoan != "0" ? this.taxInvoiceData.solarLoan : "0.00",
                    NetCost: this.taxInvoiceData.netCost != "" && this.taxInvoiceData.netCost != "0" ? this.taxInvoiceData.netCost : "0.00",
                    Deposit: this.taxInvoiceData.deposit != "" && this.taxInvoiceData.deposit != "0" ? this.taxInvoiceData.deposit : "0.00",
                    ACharge: this.taxInvoiceData.aCharge != "" && this.taxInvoiceData.aCharge != "0" ? this.taxInvoiceData.aCharge : "0.00",
                    Discount: this.taxInvoiceData.discount != "" && this.taxInvoiceData.discount != "0" ? this.taxInvoiceData.discount : "0.00",
                    Gst: gst
                }
            }
        }

        // use custom delimiter {{ }}
        _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;
    
        // interpolate
        let compiled = _.template(myTemplateStr);
        let myTemplateCompiled = compiled(myVariables);
        return myTemplateCompiled;
    }

    GetPaymentReceipt(html) {
        let htmlstring = html;

        let myTemplateStr = htmlstring;
        let TableList = [];

        if (this.paymentReceiptData.paymentDetails != null) {
            this.paymentReceiptData.paymentDetails.forEach(obj => {
                // TableList.push("<div class='mb5'>" + obj.name + "</div>");
                TableList.push("<tr><td>" + obj.date + "</td><td>$" + obj.amount + "</td><td>$" + obj.ssCharge + "</td><td>" + obj.method + "</td></tr>");
            })
        }

        let myVariables = {
            PR: {
                JobNumber: this.paymentReceiptData.jobNumber,
                Date: this.paymentReceiptData.date,
                
                Cust: {
                    Name: this.paymentReceiptData.name,
                    Mobile: this.paymentReceiptData.mobile,
                    Email: this.paymentReceiptData.email,
                    AddressLine1: this.paymentReceiptData.addressLine1,
                    AddressLine2: this.paymentReceiptData.addressLine2
                },

                ID: {
                    TCost: this.paymentReceiptData.totalCost != "" && this.paymentReceiptData.totalCost != "0" ? this.paymentReceiptData.totalCost : "0.00",
                    LessStcRebate: this.paymentReceiptData.lessStcRebate != "" && this.paymentReceiptData.lessStcRebate != "0" ? this.paymentReceiptData.lessStcRebate : "0.00",
                    SolarVICRebate: this.paymentReceiptData.solarVICRebate != "" && this.paymentReceiptData.solarVICRebate != "0" ? this.paymentReceiptData.solarVICRebate : "0.00",
                    SolarVICLoan: this.paymentReceiptData.solarVICLoan != "" && this.paymentReceiptData.solarVICLoan != "0" ? this.paymentReceiptData.solarVICLoan : "0.00",
                    ACharge: this.paymentReceiptData.aCharge != "" && this.paymentReceiptData.aCharge != "0" ? this.paymentReceiptData.aCharge : "0.00",
                    Discount: this.paymentReceiptData.discount != "" && this.paymentReceiptData.discount != "0" ? this.paymentReceiptData.discount : "0.00",
                    NetCost: this.paymentReceiptData.netCost != "" && this.paymentReceiptData.netCost != "0" ? this.paymentReceiptData.netCost : "0.00",
                    BalanceDue: this.paymentReceiptData.balanceDue != "" && this.paymentReceiptData.balanceDue != "0" ? this.paymentReceiptData.balanceDue : "0.00"
                },

                PD: {
                    PaymentDetails: TableList.toString().replace(/,/g, '')
                    //PaymentDetails: TableList.toString()
                },

                SA: {
                    AddressLine1: this.paymentReceiptData.siteAddressLine1,
                    AddressLine2: this.paymentReceiptData.siteAddressLine2
                }
            },
        }

        // use custom delimiter {{ }}
        _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;
    
        // interpolate
        let compiled = _.template(myTemplateStr);
        let myTemplateCompiled = compiled(myVariables);
        return myTemplateCompiled;
    }

    createStockTransfer(): void {
        this.CreateStockTransferModal.show();
     }  
    
     viewStockTransfer(): void {
        this.StockTransferDetailModal.show();
     }  

}
