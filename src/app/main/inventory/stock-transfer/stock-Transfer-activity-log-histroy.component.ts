import { Component, ViewChild, Injector, Output, EventEmitter, OnInit, ElementRef, Directive, Optional } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {
    LeadsServiceProxy, PurchaseOrderHistoryDto, StockOrderServiceProxy, WholeSaleLeadServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AgmCoreModule } from '@agm/core';
import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';
import PlaceResult = google.maps.places.PlaceResult;
import { Appearance, GermanAddress, Location } from '@angular-material-extensions/google-maps-autocomplete';

@Component({
    selector: 'purchaseOrderactivityloghistory',
    templateUrl: './activity-log-history.component.html',
})
export class PurchaseOrderActivityLogHistoryComponent extends AppComponentBase {
    @ViewChild("myNameElem") myNameElem: ElementRef;
    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @ViewChild('SampleDatePicker', { static: true }) sampleDatePicker: ElementRef;


    PurchaseOrderHistory: PurchaseOrderHistoryDto[];
    constructor(
        injector: Injector,
        private _purchaseOrderServiceProxy: StockOrderServiceProxy,
        private _el: ElementRef

    ) {
        super(injector);
        this._el.nativeElement.setAttribute('autocomplete', 'off');
        this._el.nativeElement.setAttribute('autocorrect', 'off');
        this._el.nativeElement.setAttribute('autocapitalize', 'none');
        this._el.nativeElement.setAttribute('spellcheck', 'false');
    }

    show(leadId?: number): void {
        debugger
        this._purchaseOrderServiceProxy.getPurchaseOrderActivityLogHistory(leadId, 0, 0, false, false).subscribe(result => {
                    this.PurchaseOrderHistory = result;
                    this.modal.show();
                });
    }
    close(): void {

        this.modal.hide();
    }
}
