import { Component, Injector, ViewEncapsulation } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DashboardCustomizationConst } from '@app/shared/common/customizable-dashboard/DashboardCustomizationConsts';
import { Title } from '@angular/platform-browser';
import { DatePipe } from '@angular/common';

@Component({
    templateUrl: './dashboard-inventory.component.html',
    styleUrls: ['./dashboard-inventory.component.less'],
    encapsulation: ViewEncapsulation.None ,
    // styles: ['.kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper { padding-top:75px !important; }']
})

export class DashboardInventoryComponent extends AppComponentBase {
    dashboardName = DashboardCustomizationConst.dashboardNames.defaultTenantDashboard;
    pipe = new DatePipe('en-US');
    today = this.pipe.transform(new Date(), 'MMM dd');

    constructor(
        injector: Injector,
        private titleService: Title) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Inventory Dashboard");
    }
    showchild: boolean = true;
    shouldShow: boolean = false;
    addTaskShow: boolean = false;
}
