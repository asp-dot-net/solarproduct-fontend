import { Component, Injector, ViewEncapsulation, ViewChild, OnInit, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { StockOrderServiceProxy, CommonLookupDto, CommonLookupServiceProxy, OrganizationUnitDto, GetAllStockPaymentTrackerWithSummary, StockPaymentServiceProxy, StockPaymentTrackerServiceProxy } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { DomSanitizer, SafeHtml, Title } from '@angular/platform-browser';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto } from '@shared/service-proxies/service-proxies';

@Component({
    templateUrl: './stock-payment-tracker.component.html',

    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class StockPaymentTrackerComponent extends AppComponentBase implements OnInit {
    //  @ViewChild('ViewStockPaymentModal', { static: true }) ViewStockPaymentModal: ViewStockPaymentModalComponent;      


    FiltersData = false;
    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    organizationUnitlength: number = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnit = 0;
    changeOrganization = 0;
    flag = true;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    allLocation: CommonLookupDto[];

    PaymentMethodFilter: number = undefined;
    GstTypeIdFilter: number = undefined;
    advancedFiltersAreShown = false;
    allPaymentMethod: CommonLookupDto[];

    date = new Date();
    firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    StartDate = moment().startOf('month').add(1, 'days');
    EndDate = moment().add(0, 'days').endOf('day');
    dateFilterType = undefined;
    filterText = '';
    areaNameFilter = '';
    filterName = "OrderNo";
    ExpandedView: boolean = true;
    excelorcsvfile = 0;
    firstrowcount = 0;
    last = 0;
    vicrebate = "All";
    solarRebateStatus = 0;
    showCancel: boolean = false;
    public screenWidth: any;
    public screenHeight: any;
    testHeight = 260;
    allBDMs: any[];
    allSalesRap: any[];
    toggle: boolean = true;
    SelectedPurtchaseOrderId: number = 0;
    PurchaseOrderName: string;
    WarehouseLocationIdFiletr = [];
    allStockOrderStatus: CommonLookupDto[];
    StockOrderStatusIds = [];
    VendorIdFiletr  :number = undefined;
    allVendors: CommonLookupDto[];
    allDeliveryType: CommonLookupDto[];
    StockOrderIdFiletr :number = undefined;
    allStockOrderfor:CommonLookupDto[];
    allTransportCompany:CommonLookupDto[];
    TransportCompanyIdFiletr :number = undefined;
    PaymentStatusIdFiletr  :number = undefined;
    allPaymentStatus:CommonLookupDto[];
    PaymentTypeIdFiletr = [];
    DeliveryTypeIdFiletr :number = undefined;
    summary: GetAllStockPaymentTrackerWithSummary[];

    change() {
        this.toggle = !this.toggle;
    }

    constructor(
        injector: Injector,
        private _stockPaymentTrackerServiceProxy: StockPaymentTrackerServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _commonLookupService: CommonLookupServiceProxy,
        private _userActivityLogServiceProxy: UserActivityLogServiceProxy,
        private _router: Router,
        private titleService: Title,

    ) {
        super(injector);

    }
    searchLog(): void {

        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote = 'Searched by Filters';
        log.section = 'Stock Payment Tracker';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
            });
    }


    ngOnInit(): void {
        this.StartDate = moment().startOf('month').add(1, 'days');
        this.EndDate = moment().add(0, 'days').endOf('day');
        this.screenHeight = window.innerHeight;
        this.titleService.setTitle(this.appSession.tenancyName + " |  Stock Payment Tracker");

        this._commonLookupService.getStockOrderPaymentStatusDropdown().subscribe(result => {
            this.allPaymentStatus = result;
        })
        this._commonLookupService.getpaymentMethodDropdown().subscribe(result => {
            this.allPaymentMethod = result;
        })
        this._commonLookupService.getLocationDropdown().subscribe(result => {
            this.allLocation = result;
        })
        this._commonLookupService.getStockOrderStatusDropdown().subscribe(result => {
            this.allStockOrderStatus = result;
        })
        this._commonLookupService.getAllVendorDropdown().subscribe(result => {
            this.allVendors = result;
        });
        this._commonLookupService.getDeliveryTypeDropdown().subscribe(result => {
            this.allDeliveryType = result;
        })
        this._commonLookupService.getStockorderfoDropdown().subscribe(result => {
            this.allStockOrderfor = result;
        })
        this._commonLookupService.getTransportCompanyDropdown().subscribe(result => {
            this.allTransportCompany = result;
        })
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;

            this.getStockPayment();
        });
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote = 'Open Stock Payment Tracker';
        log.section = 'Stock Payment Tracker';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
            });
    }


    // stockOrderDetail(orderId?: number): void {
    //     this._router.navigate(['/app/main/inventory/purchase-order-new/StockOrderDetailModal'], { queryParams: { OId: orderId } });
    // }

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        this.screenHeight = window.innerHeight;
    }

    closeModal() {
        this.shouldShow = false; // Hides the modal
    }
    getStockPayment(event?: LazyLoadEvent) {
        debugger;
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._stockPaymentTrackerServiceProxy.getAll(
            this.filterName,
            this.filterText,
            this.GstTypeIdFilter,
            this.TransportCompanyIdFiletr,
            this.StockOrderStatusIds,
            this.WarehouseLocationIdFiletr,
            this.PaymentStatusIdFiletr,
            this.PaymentTypeIdFiletr,
            this.VendorIdFiletr,
            this.DeliveryTypeIdFiletr,
            this.StockOrderIdFiletr,
            this.organizationUnit,
            this.dateFilterType,
            this.StartDate,
            this.EndDate,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.summary=result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();


        });
    }


    // reloadPage(flafValue: boolean): void {
    //     this.ExpandedView = true;
    //     this.flag = flafValue;
    //     this.paginator.changePage(this.paginator.getPage());
    //     if (!flafValue) {
    //         this.navigateToPurchaseOrderDetail(this.SelectedPurtchaseOrderId);
    //     }
    // }
    // navigateToPurchaseOrderDetail(purchaseOrderId): void {
    //     debugger;
    //     this.ExpandedView = !this.ExpandedView;
    //     this.ExpandedView = false;
    //     this.SelectedPurtchaseOrderId = purchaseOrderId;
    //     this.PurchaseOrderName = "PurchaseOrderName";
    //     this.ViewPurchaseOrderDetail.showDetail(purchaseOrderId, this.PurchaseOrderName, 1);
    // } 
    // deleteStockPayment(GetAllStockPaymentOutputDto: GetAllStockPaymentOutputDto): void {

    //     this.message.confirm(
    //         this.l('AreYouSureWanttoDelete'),
    //         this.l('Delete'),
    //         (isConfirmed) => {
    //             if (isConfirmed) {
    //                 this._stockPaymentServiceProxy.delete(GetAllStockPaymentOutputDto.id)
    //                     .subscribe(() => {
    //                         this.getStockPayment();
    //                         this.notify.success(this.l('SuccessfullyDeleted'));

    //                         let log = new UserActivityLogDto();
    //                         log.actionId = 83;
    //                         log.actionNote ='Delete Stock Payment Tracker: ' + GetAllStockPaymentOutputDto.stockNo;
    //                         log.section = 'Stock Payment Tracker';
    //                         this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
    //                             .subscribe(() => {
    //                         });
    //                     });
    //             }
    //         }
    //     );
    // }
    exportToExcel(): void {

        this._stockPaymentTrackerServiceProxy.getStockOrderToExcel(
            this.filterName,
            this.filterText,
            this.GstTypeIdFilter,
            this.TransportCompanyIdFiletr,
            this.StockOrderStatusIds,
            this.WarehouseLocationIdFiletr,
            this.PaymentStatusIdFiletr,
            this.PaymentTypeIdFiletr,
            this.VendorIdFiletr,
            this.DeliveryTypeIdFiletr,
            this.StockOrderIdFiletr,
            this.organizationUnit,
            this.dateFilterType,
            this.StartDate,
            this.EndDate)
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
             });
    }

    clear() {
        this.filterText = '';
        let date = new Date();
        this.GstTypeIdFilter;
        this.PaymentMethodFilter = undefined;
        this.dateFilterType = '';
        this.dateFilterType = undefined;
        this.StartDate = moment().add(0, 'days').endOf('day');
        this.EndDate = moment().add(0, 'days').endOf('day');
        this.WarehouseLocationIdFiletr = [];
        this.StockOrderStatusIds = [];
        this.VendorIdFiletr=undefined;
        this.StockOrderIdFiletr = undefined;
        this.TransportCompanyIdFiletr = undefined;
        this.PaymentStatusIdFiletr = undefined;
        this.PaymentTypeIdFiletr = [];
        this.DeliveryTypeIdFiletr = undefined;
        this.getStockPayment();
    }

    createStockPayment(lId: number): void {
    }

    expandGrid() {
        this.ExpandedView = true;
    }

    //  viewAllUploads(): void {
    //        this.ViewAllUploadsModal.show();
    //     }

}
