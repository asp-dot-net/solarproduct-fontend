﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { RefundReasonsServiceProxy, CreateOrEditRefundReasonDto, JobsServiceProxy, CreateOrEditStcStatusDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'createOrEditStcStatusModal',
    templateUrl: './create-or-edit-StcStatus-modal.component.html'
})
export class CreateOrEditStcStatusModalComponent extends AppComponentBase {
   
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    refundReason: CreateOrEditRefundReasonDto = new CreateOrEditRefundReasonDto();
stcpvd:CreateOrEditStcStatusDto = new CreateOrEditStcStatusDto();


    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _jobServiceProxy: JobsServiceProxy
    ) {
        super(injector);
    }
    
    show(statusId?: number): void {
    

        if (!statusId) {
            this.stcpvd = new CreateOrEditRefundReasonDto();
            this.stcpvd.id = statusId;

            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Stc Status';
            log.section = 'Stc Status';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._jobServiceProxy.getstatusForEdit(statusId).subscribe(result => {
                this.stcpvd = result;


                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Stc Status : ' + this.refundReason.name;
                log.section = 'Stc Status';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        
    }

    onShown(): void {
        
        document.getElementById('Name').focus();
    }
    
    save(): void {
            this.saving = true;

			debugger;
			
            this._jobServiceProxy.createOrEditStcStatus(this.stcpvd)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.refundReason.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Stc Status Updated : '+ this.refundReason.name;
                    log.section = 'Stc Status';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Stc Status Created : '+ this.refundReason.name;
                    log.section = 'Stc Status';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }







    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
