﻿import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute , Router} from '@angular/router';
import { RefundReasonsServiceProxy, RefundReasonDto, JobsServiceProxy, CreateOrEditJobRefundDto  } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { ViewRefundReasonModalComponent } from '@app/main/jobs/refundReasons/view-refundReason-modal.component';
import { CreateOrEditStcStatusModalComponent } from './create-or-edit-StcStatus-modal.component';
import { Title } from '@angular/platform-browser';

 

@Component({
    templateUrl: './stcpvdstatus.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class Stcpvdstatuscomponent   extends AppComponentBase {
    
    @ViewChild('createOrEditStcStatusModal', { static: true }) createOrEditStcStatusModal: CreateOrEditStcStatusModalComponent;
    @ViewChild('viewRefundReasonModalComponent', { static: true }) viewRefundReasonModal: ViewRefundReasonModalComponent;   
    
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    nameFilter = '';
    firstrowcount = 0;
    last = 0;
    public screenHeight: any;  
    testHeight = 250;
    constructor(
        injector: Injector,
        private _refundReasonsServiceProxy: RefundReasonsServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _jobServiceProxy: JobsServiceProxy,
        private titleService: Title  
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Stc Status");
    }
    
searchLog() : void {
        
    let log = new UserActivityLogDto();
        log.actionId =80;
        log.actionNote ='Searched by Filters';
        log.section = 'Stc Status';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
}
ngOnInit(): void {
    this.screenHeight = window.innerHeight; 
   
    let log = new UserActivityLogDto();
    log.actionId =79;
    log.actionNote ='Open Stc Status';
    log.section = 'Stc Status';
    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {
    });
}
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }
    onShown(): void {
        document.getElementById('filterText').focus();
    }
    getRefundReasons(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();
        this._jobServiceProxy.getAllPvdStatusForGrid(
            this.filterText,
            this.nameFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }
    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }
    createRefundReason(): void {
        debugger
        this.createOrEditStcStatusModal.show();        
    }
    deleteStcStatus(refundReason: CreateOrEditJobRefundDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    debugger;
                    this._jobServiceProxy.deleteStcPvdStatus(refundReason.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));

                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete Stc Status: ' + refundReason.amount;
                            log.section = 'Stc Status';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });
                        });
                }
            }
        );
    }
    exportToExcel(filename): void {
        this._jobServiceProxy.getAllPvdStatusForGridExcel(
        this.filterText,
            this.nameFilter,
            filename
        )
        .subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
         });
    }  
}
