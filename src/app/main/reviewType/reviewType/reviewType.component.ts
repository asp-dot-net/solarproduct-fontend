﻿import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute , Router} from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import * as _ from 'lodash';
import * as moment from 'moment';
import { Title } from '@angular/platform-browser';
import { ViewReviewTypeModalComponent } from './view-reviewType-modal.component';
import { CreateOrEditReviewTypeModalComponent } from './create-or-edit-reviewType-modal.component';
import { ReviewTypeDto, ReviewTypesServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';

@Component({
    templateUrl: './reviewType.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ReviewTypeComponent extends AppComponentBase {
    
    @ViewChild('createOrEditreviewTypeModal', { static: true }) createOrEditreviewTypeModal: CreateOrEditReviewTypeModalComponent;
    @ViewChild('viewreviewTypeModal', { static: true }) viewreviewTypeModal: ViewReviewTypeModalComponent;   
    
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    holdReasonFilter = '';
    firstrowcount = 0;
    last = 0;
    reviewTypeLinkFilter = '';
    public screenHeight: any;  
    testHeight = 250;
    constructor(
        injector: Injector,
        private _reviewTypesReviewProxy: ReviewTypesServiceProxy,
        private titleReview: Title,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
        this.titleReview.setTitle("SolarProduct | Review Type");
    }
    searchLog() : void {
        
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Review Type';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
       
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Review Type';
        log.section = 'Review Type';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
    }
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }
    getreviewType(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._reviewTypesReviewProxy.getAll(
            this.filterText,
            undefined,
            this.reviewTypeLinkFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createReviewType(): void {
        this.createOrEditreviewTypeModal.show();        
    }


    deletereviewType(reviewtypes: ReviewTypeDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._reviewTypesReviewProxy.delete(reviewtypes.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete Review Type: ' + reviewtypes.name;
                            log.section = 'Review Type';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });
                        });
                }
            }
        );
    }

    exportToExcel(): void {
        // this._reviewTypesReviewProxy.getHoldReasonsToExcel(
        // this.filterText,
        //     this.holdReasonFilter,
        // )
        // .subscribe(result => {
        //     this._fileDownloadReview.downloadTempFile(result);
        //  });
    }
    
    
    
    
}
