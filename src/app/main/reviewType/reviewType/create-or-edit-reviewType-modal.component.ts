﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { HoldReasonsServiceProxy, CreateOrEditHoldReasonDto, CreateOrEditServiceCategoryDto, ServiceCategorysServiceProxy, CreateOrEditServiceTypeDto, ServiceTypesServiceProxy, ReviewTypesServiceProxy, CreateOrEditReviewTypeDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'createOrEditreviewTypeModal',
    templateUrl: './create-or-edit-reviewType-modal.component.html'
})
export class CreateOrEditReviewTypeModalComponent extends AppComponentBase {
   
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    reviewTypes: CreateOrEditReviewTypeDto = new CreateOrEditReviewTypeDto();

    constructor(
        injector: Injector,
        private _reviewTypesServiceProxy: ReviewTypesServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }
    
    show(reviewTypesId?: number): void {
    debugger;
        if (!reviewTypesId) {
            this.reviewTypes = new CreateOrEditReviewTypeDto();
            this.reviewTypes.id = reviewTypesId;

            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Review Type';
            log.section = 'Review Type';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._reviewTypesServiceProxy.getReviewTypeForEdit(reviewTypesId).subscribe(result => {
                this.reviewTypes = result.reviewTypes;

                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Review Type : ' + this.reviewTypes.name;
                log.section = 'Review Type';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });

            });
        }
        
    }
    onShown(): void {
        
        document.getElementById('ReviewType_ReviewType').focus();
    }
    save(): void {
            this.saving = true;
            this._reviewTypesServiceProxy.createOrEdit(this.reviewTypes)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.reviewTypes.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Review Type Updated : '+ this.reviewTypes.name;
                    log.section = 'Review Type';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Review Type Created : '+ this.reviewTypes.name;
                    log.section = 'Review Type';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }

             });
    }


    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
