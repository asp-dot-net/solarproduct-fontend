﻿
import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppCommonModule } from '@app/shared/common/app-common.module';
import { SmsTemplatesComponent } from './smsTemplates/smsTemplates/smsTemplates.component';
import { ViewSmsTemplateModalComponent } from './smsTemplates/smsTemplates/view-smsTemplate-modal.component';
import { CreateOrEditSmsTemplateModalComponent } from './smsTemplates/smsTemplates/create-or-edit-smsTemplate-modal.component';

import { FreebieTransportsComponent } from './jobs/freebieTransports/freebieTransports.component';
import { ViewFreebieTransportModalComponent } from './jobs/freebieTransports/view-freebieTransport-modal.component';
import { CreateOrEditFreebieTransportModalComponent } from './jobs/freebieTransports/create-or-edit-freebieTransport-modal.component';

import { InvoiceStatusesComponent } from './invoices/invoiceStatuses/invoiceStatuses.component';
import { ViewInvoiceStatusModalComponent } from './invoices/invoiceStatuses/view-invoiceStatus-modal.component';
import { CreateOrEditInvoiceStatusModalComponent } from './invoices/invoiceStatuses/create-or-edit-invoiceStatus-modal.component';

import { HoldReasonsComponent } from './holdReasons/holdReasons/holdReasons.component';
import { ViewHoldReasonModalComponent } from './holdReasons/holdReasons/view-holdReason-modal.component';
import { CreateOrEditHoldReasonModalComponent } from './holdReasons/holdReasons/create-or-edit-holdReason-modal.component';

import { CalendersComponent } from './calenders/calenders/calenders.component';

import { VariationsComponent } from './jobs/variations/variations.component';
import { ViewVariationModalComponent } from './jobs/variations/view-variation-modal.component';
import { CreateOrEditVariationModalComponent } from './jobs/variations/create-or-edit-variation-modal.component';


import { InvoicePaymentMethodsComponent } from './invoices/invoicePaymentMethods/invoicePaymentMethods.component';
import { ViewInvoicePaymentMethodModalComponent } from './invoices/invoicePaymentMethods/view-invoicePaymentMethod-modal.component';
import { CreateOrEditInvoicePaymentMethodModalComponent } from './invoices/invoicePaymentMethods/create-or-edit-invoicePaymentMethod-modal.component';

import { InvoicePaymentsComponent } from './invoices/invoicePayments/invoicePayments.component';
import { CreateOrEditInvoicePaymentComponent } from './invoices/invoicePayments/create-or-edit-invoicePayment.component';

import { PromotionMastersComponent } from './jobs/promotionMasters/promotionMasters.component';
import { ViewPromotionMasterModalComponent } from './jobs/promotionMasters/view-promotionMaster-modal.component';
import { CreateOrEditPromotionMasterModalComponent } from './jobs/promotionMasters/create-or-edit-promotionMaster-modal.component';
import { JobsListComponent } from './jobs/jobs/jobslist.component';

import { FreebiesComponent } from './jobs/jobPromotions/freebies.component'
import { FreebiesTrackingModalComponent } from './jobs/jobPromotions/freebies-tracking-model.component'

import { FinanceOptionsComponent } from './jobs/financeOptions/financeOptions.component';
import { ViewFinanceOptionModalComponent } from './jobs/financeOptions/view-financeOption-modal.component';
import { CreateOrEditFinanceOptionModalComponent } from './jobs/financeOptions/create-or-edit-financeOption-modal.component';

import { HouseTypesComponent } from './jobs/houseTypes/houseTypes.component';
import { ViewHouseTypeModalComponent } from './jobs/houseTypes/view-houseType-modal.component';
import { CreateOrEditHouseTypeModalComponent } from './jobs/houseTypes/create-or-edit-houseType-modal.component';

import { CancelReasonsComponent } from './cancelReasons/cancelReasons/cancelReasons.component';
import { ViewCancelReasonModalComponent } from './cancelReasons/cancelReasons/view-cancelReason-modal.component';
import { CreateOrEditCancelReasonModalComponent } from './cancelReasons/cancelReasons/create-or-edit-cancelReason-modal.component';

import { RefundReasonsComponent } from './jobs/refundReasons/refundReasons.component';
import { ViewRefundReasonModalComponent } from './jobs/refundReasons/view-refundReason-modal.component';
import { CreateOrEditRefundReasonModalComponent } from './jobs/refundReasons/create-or-edit-refundReason-modal.component';

import { RejectReasonsComponent } from './rejectReasons/rejectReasons/rejectReasons.component';
import { ViewRejectReasonModalComponent } from './rejectReasons/rejectReasons/view-rejectReason-modal.component';
import { CreateOrEditRejectReasonModalComponent } from './rejectReasons/rejectReasons/create-or-edit-rejectReason-modal.component';

import { MeterPhasesComponent } from './jobs/meterPhases/meterPhases.component';
import { ViewMeterPhaseModalComponent } from './jobs/meterPhases/view-meterPhase-modal.component';
import { CreateOrEditMeterPhaseModalComponent } from './jobs/meterPhases/create-or-edit-meterPhase-modal.component';

import { MeterUpgradesComponent } from './jobs/meterUpgrades/meterUpgrades.component';
import { ViewMeterUpgradeModalComponent } from './jobs/meterUpgrades/view-meterUpgrade-modal.component';
import { CreateOrEditMeterUpgradeModalComponent } from './jobs/meterUpgrades/create-or-edit-meterUpgrade-modal.component';

import { ElecRetailersComponent } from './jobs/elecRetailers/elecRetailers.component';
import { ViewElecRetailerModalComponent } from './jobs/elecRetailers/view-elecRetailer-modal.component';
import { CreateOrEditElecRetailerModalComponent } from './jobs/elecRetailers/create-or-edit-elecRetailer-modal.component';

import { DepositOptionsComponent } from './jobs/depositOptions/depositOptions.component';
import { ViewDepositOptionModalComponent } from './jobs/depositOptions/view-depositOption-modal.component';
import { CreateOrEditDepositOptionModalComponent } from './jobs/depositOptions/create-or-edit-depositOption-modal.component';

import { PaymentOptionsComponent } from './jobs/paymentOptions/paymentOptions.component';
import { ViewPaymentOptionModalComponent } from './jobs/paymentOptions/view-paymentOption-modal.component';
import { CreateOrEditPaymentOptionModalComponent } from './jobs/paymentOptions/create-or-edit-paymentOption-modal.component';

import { JobStatusesComponent } from './jobs/jobStatuses/jobStatuses.component';
import { ViewJobStatusModalComponent } from './jobs/jobStatuses/view-jobStatus-modal.component';
import { CreateOrEditJobStatusModalComponent } from './jobs/jobStatuses/create-or-edit-jobStatus-modal.component';

import { ProductItemsComponent } from './jobs/productItems/productItems.component';
import { ViewProductItemComponent } from './jobs/productItems/view-productItem.component';
import { CreateOrEditProductItemComponent } from './jobs/productItems/create-or-edit-productItem.component';

import { ElecDistributorsComponent } from './jobs/elecDistributors/elecDistributors.component';
import { ViewElecDistributorModalComponent } from './jobs/elecDistributors/view-elecDistributor-modal.component';
import { CreateOrEditElecDistributorModalComponent } from './jobs/elecDistributors/create-or-edit-elecDistributor-modal.component';

import { RoofAnglesComponent } from './jobs/roofAngles/roofAngles.component';
import { ViewRoofAngleModalComponent } from './jobs/roofAngles/view-roofAngle-modal.component';
import { CreateOrEditRoofAngleModalComponent } from './jobs/roofAngles/create-or-edit-roofAngle-modal.component';

import { RoofTypesComponent } from './jobs/roofTypes/roofTypes.component';
import { ViewRoofTypeModalComponent } from './jobs/roofTypes/view-roofType-modal.component';
import { CreateOrEditRoofTypeModalComponent } from './jobs/roofTypes/create-or-edit-roofType-modal.component';

import { ProductTypesComponent } from './jobs/productTypes/productTypes.component';
import { ViewProductTypeModalComponent } from './jobs/productTypes/view-productType-modal.component';
import { CreateOrEditProductTypeModalComponent } from './jobs/productTypes/create-or-edit-productType-modal.component';

import { JobTypesComponent } from './jobs/jobTypes/jobTypes.component';
import { ViewJobTypeComponent } from './jobs/jobTypes/view-jobType.component';
import { CreateOrEditJobTypeComponent } from './jobs/jobTypes/create-or-edit-jobType.component';
import { ViewJobTypeModalComponent } from './jobs/jobTypes/view-jobType-modal.component';
import { CreateOrEditJobTypeModalComponent } from './jobs/jobTypes/create-or-edit-jobType-modal.component';


import { PromotionUsersComponent } from './promotions/promotionUsers/promotionUsers.component';
import { ViewPromotionUserModalComponent } from './promotions/promotionUsers/view-promotionUser-modal.component';
import { CreateOrEditPromotionUserModalComponent } from './promotions/promotionUsers/create-or-edit-promotionUser-modal.component';

import { PromotionsComponent } from './promotions/promotions/promotions.component';
import { ViewPromotionModalComponent } from './promotions/promotions/view-promotion-modal.component';
import { CreateOrEditPromotionModalComponent } from './promotions/promotions/create-or-edit-promotion-modal.component';


import { PromotionResponseStatusesComponent } from './promotions/promotionResponseStatuses/promotionResponseStatuses.component';
import { ViewPromotionResponseStatusModalComponent } from './promotions/promotionResponseStatuses/view-promotionResponseStatus-modal.component';
import { CreateOrEditPromotionResponseStatusModalComponent } from './promotions/promotionResponseStatuses/create-or-edit-promotionResponseStatus-modal.component';

import { DepartmentsComponent } from './departments/departments/departments.component';
import { ViewDepartmentModalComponent } from './departments/departments/view-department-modal.component';
import { CreateOrEditDepartmentModalComponent } from './departments/departments/create-or-edit-department-modal.component';

import { LeadExpensesComponent } from './expense/leadExpenses/leadExpenses.component';
import { ViewLeadExpenseModalComponent } from './expense/leadExpenses/view-leadExpense-modal.component';

import { UserTeamsComponent } from './theSolarDemo/userTeams/userTeams.component';
import { ViewUserTeamComponent } from './theSolarDemo/userTeams/view-userTeam.component';
import { CreateOrEditUserTeamComponent } from './theSolarDemo/userTeams/create-or-edit-userTeam.component';


import { CategoriesComponent } from './theSolarDemo/categories/categories.component';
import { ViewCategoryModalComponent } from './theSolarDemo/categories/view-category-modal.component';
import { CreateOrEditCategoryModalComponent } from './theSolarDemo/categories/create-or-edit-category-modal.component';

import { TeamsComponent } from './theSolarDemo/teams/teams.component';
import { ViewTeamComponent } from './theSolarDemo/teams/view-team.component';
import { CreateOrEditTeamComponent } from './theSolarDemo/teams/create-or-edit-team.component';

import { ViewTeamModalComponent } from './theSolarDemo/teams/view-team-modal.component';
import { CreateOrEditTeamModalComponent } from './theSolarDemo/teams/create-or-edit-team-modal.component';

import { LeadsComponent } from './leads/leads/leads.component';
import { ViewLeadComponent } from './leads/leads/view-lead.component';
import { CreateOrEditLeadComponent } from './leads/leads/create-or-edit-lead.component';

import { LeadSourcesComponent } from './leadSources/leadSources/leadSources.component';
import { ViewLeadSourceModalComponent } from './leadSources/leadSources/view-leadSource-modal.component';
import { CreateOrEditLeadSourceModalComponent } from './leadSources/leadSources/create-or-edit-leadSource-modal.component';

import { PostalTypesComponent } from './postalTypes/postalTypes/postalTypes.component';
import { ViewPostalTypeModalComponent } from './postalTypes/postalTypes/view-postalType-modal.component';
import { CreateOrEditPostalTypeModalComponent } from './postalTypes/postalTypes/create-or-edit-postalType-modal.component';

import { PostCodesComponent } from './postCodes/postCodes/postCodes.component';
import { ViewPostCodeModalComponent } from './postCodes/postCodes/view-postCode-modal.component';
import { CreateOrEditPostCodeModalComponent } from './postCodes/postCodes/create-or-edit-postCode-modal.component';

import { StreetNamesComponent } from './streetNames/streetNames/streetNames.component';
import { ViewStreetNameModalComponent } from './streetNames/streetNames/view-streetName-modal.component';
import { CreateOrEditStreetNameModalComponent } from './streetNames/streetNames/create-or-edit-streetName-modal.component';

import { StreetTypesComponent } from './streetTypes/streetTypes/streetTypes.component';
import { ViewStreetTypeModalComponent } from './streetTypes/streetTypes/view-streetType-modal.component';
import { CreateOrEditStreetTypeModalComponent } from './streetTypes/streetTypes/create-or-edit-streetType-modal.component';

// import { ViewStreetTypeComponent } from './streetTypes/streetTypes/view-streetType.component';
// import { CreateOrEditStreetTypeComponent } from './streetTypes/streetTypes/create-or-edit-streetType.component';

import { UnitTypesComponent } from './unitTypes/unitTypes/unitTypes.component';
import { ViewUnitTypeModalComponent } from './unitTypes/unitTypes/view-unitType-modal.component';
import { CreateOrEditUnitTypeModalComponent } from './unitTypes/unitTypes/create-or-edit-unitType-modal.component';

import { QuotationTemplateComponent } from './quotationtemplate/quotationtemplate/quotationtemplate.component';
import { CreateOrEditQuotationTemplateModalComponent } from "./quotationtemplate/quotationtemplate/create-or-edit-quotationtemplate-modal.component"

import { AutoCompleteModule } from 'primeng/autocomplete';
import { PaginatorModule } from 'primeng/paginator';
import { EditorModule } from 'primeng/editor';
import { InputMaskModule } from 'primeng/inputmask'; import { FileUploadModule } from 'primeng/fileupload';
import { TableModule } from 'primeng/table';

import { UtilsModule } from '@shared/utils/utils.module';
import { CountoModule } from 'angular2-counto';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DashboardInvoiceComponent } from './dashboard-invoice/dashboard-invoice.component';
import { MainRoutingModule } from './main-routing.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { BsDatepickerConfig, BsDaterangepickerConfig, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgxBootstrapDatePickerConfigService } from 'assets/ngx-bootstrap/ngx-bootstrap-datepicker-config.service';

import { AgmCoreModule } from '@agm/core';
import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';
import { AssignOrTransferLeadComponent } from './leads/leads/assign-or-transfer-lead.component';
import { ActivityLogLeadComponent } from './leads/leads/activity-log-lead.component';
import { ChangeStatusLeadComponent } from './leads/leads/change-status-lead.component';
import { MyLeadsComponent } from './myleads/myleads/myleads.component';
import { ActivityLogMyLeadComponent } from './myleads/myleads/activity-log-mylead.component';
import { ViewMyLeadComponent } from './myleads/myleads/view-mylead.component';
import { UserDashboardComponent } from './userdashboard/user-dashboard.component';
import { DuplicateleadsComponent } from './duplicatelead/duplicatelead/duplicateleads.component';
import { ClosedleadsComponent } from './closedlead/closedlead/closedleads.component';
import { ViewClosedLeadComponent } from './closedlead/closedlead/view-closed-lead.component';
// import { AddActivityModalComponent } from './myleads/myleads/add-activity-model.component';
import { AddActivityModalComponent } from './myleads/myleads/add-activity-model.component';
import { VerifyInvoiceModalComponent } from './invoices/invoicePayments/verify-invoice-model.component';
import { JobNotesModalComponent } from './jobs/jobs/job-notes-model.component';
import { AppBsModalModule } from '@shared/common/appBsModal/app-bs-modal.module';
import { ViewDuplicateModalComponent } from './leads/leads/view-duplicate-lead.component';
import { CreateOrEditLeadSourceComponent } from './leadSources/leadSources/create-or-edit-leadSource.component';
import { ViewLeadSourceComponent } from './leadSources/leadSources/view-leadSource.component';
import { LeadTrackerComponent } from './leads/leads/lead-tracker.component';
import { ResultsComponent } from './result/result/results.component';
import { ViewResultLeadComponent } from './result/result/view-result-lead.component';
import { JobsComponent } from './jobs/jobs/jobs.component';
import { FullCalendarModule } from '@fullcalendar/angular'; // the main connector. must go first
import dayGridPlugin from '@fullcalendar/daygrid'; // a plugin
import timeGridPlugin from '@fullcalendar/timeGrid'; // a plugin
import interactionPlugin from '@fullcalendar/interaction'; // a plugin
import { InstallationComponent } from './installation/installation/installation.component';
import { CancelleadsComponent } from './cancelleads/cancelleads/cancelleads.component';
import { jobGridComponent } from './jobs/jobGrid/jobGrid.component';
import { RefundJobModalComponent } from './jobs/jobRefunds/job-refund-model.component';
import { JobRefundsComponent } from './jobs/jobRefunds/jobRefunds.component';
import { ViewJobRefundModalComponent } from './jobs/jobRefunds/view-jobRefund-modal.component';
import { CreateOrEditJobRefundModalComponent } from './jobs/jobRefunds/create-or-edit-jobRefund-modal.component';
import { EmailEditorModule } from 'angular-email-editor';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { EmailTemplateComponent } from './emailtemplates/emailtemplates.component';
import { DocumentTypesComponent } from './documentTypes/documenttypes.component';
import { CreateOrEditDocumentTypeModalComponent } from './documentTypes/create-or-edit-documenttype-modal.component';
import { CreateOrEditJobCancellationReasonModalComponent } from './jobs/jobCancellationReason/create-or-edit-jobcancellationreason-modal.component';
import { JobCancellationReasonComponent } from './jobs/jobCancellationReason/jobcancellationreason.component';
import { JobActiveModalComponent } from './jobs/jobs/check-job-active.component';
import { CreateOrEditJobInvoiceModalComponent } from './jobs/jobs/edit-jobinvoice-model.component';
import { JobStatusModalComponent } from './jobs/jobs/job-status.component';
import { JobDetailModalComponent } from './jobs/jobs/job-detail-model.component';
import { PendingInstallationComponent } from './installation/installation/pendinginstallation.component';
import { PendingInstallationDetailComponent } from './installation/installation/pendinginstallationdetail.component';
import { StcTrackerComponent } from './jobs/stctracker/stctracker.component';
import { CreateOrEditJobStcModalComponent } from './jobs/stctracker/add-stc.modal.component';
import { GridConnectionTrackerComponent } from './gridconnectiontracker/gridconnectiontracker.component';
import { GridConnectionDetailComponent } from './gridconnectiontracker/gridconnectiondetail.component';
import { AddMeterDetailModalComponent } from './gridconnectiontracker/add-meterdetail-modal.component';
import { FinanceTrackerComponent } from './jobs/financeTracker/financeTracker.component';
import { JobFinanceDetailModalComponent } from './jobs/financeTracker/financeDetail.component';
import { RejectleadsComponent } from './rejectleads/rejectleads/rejectleads.component';
import { ActiveJobModalComponent } from './jobs/jobs/job-active.component';

import { NewinstallerComponent } from './installer/newinstaller/newinstaller.component';
import { EditupdateNewinvoiceComponent } from './installer/newinstaller/editupdate-newinvoice/editupdate-newinvoice.component';
import { JobActiveRequestComponent } from './jobs/jobs/job-active-request.component';
import { ViewDuplicatePopUpModalComponent } from './leads/leads/duplicate-lead-popup.component'; import { ViewApplicationModelComponent } from './jobs/jobs/view-application-model/view-application-model.component';;
;
import { JobSmsEmailModelComponent } from './jobs/jobs/job-sms-email-model/job-sms-email-model.component'
	;
import { FinanceSmsEmailModelComponent } from './jobs/financeTracker/finance-sms-email-model/finance-sms-email-model.component'
	;
import { JobActiveSmsEmailModelComponent } from './jobs/jobs/job-active-sms-email-model/job-active-sms-email-model.component'
	;
import { JobgridSmsEmailModelComponent } from './jobs/jobGrid/jobgrid-sms-email-model/jobgrid-sms-email-model.component'
	;
import { GridconnectionSmsemailModelComponent } from './gridconnectiontracker/gridconnection-smsemail-model/gridconnection-smsemail-model.component'
	;
import { StcSmsEmailModelComponent } from './jobs/stctracker/stc-sms-email-model/stc-sms-email-model.component';;
import { FreebiesSmsEmailModelComponent } from './jobs/jobPromotions/freebies-sms-email-model/freebies-sms-email-model.component'
	;
import { JobRefundSmsemailModelComponent } from './jobs/jobRefunds/job-refund-smsemail-model/job-refund-smsemail-model.component'
	;
import { SmsEmailModelComponent } from './invoices/invoicePayments/sms-email-model/sms-email-model.component'
import { DocumentRequestLinkModalComponent } from './jobs/jobs/document-request-link.component';;
import { ViewJobcancellationreasonModelComponent } from './jobs/jobCancellationReason/view-jobcancellationreason-model/view-jobcancellationreason-model.component'
import { ViewDocumentTypeModalComponent } from './documentTypes/view-documenttype-model.component';
import { CreateEditLeadComponent } from './leads/leads/create-edit-lead.component';
import { EmailReplyComponent } from './reply/emailReply/email-reply.component';
import { SmsReplyComponent } from './reply/smsreply/sms-reply.component';
import { AddAttachmentProductItemComponent } from './jobs/productItems/add-attachment.component';
import { ViewEmailBodyComponent } from './reply/emailReply/view-emailbody.component';
import { DocumentLibrarysComponent } from './documentLibrary/documentlibrary.component';
import { CreateOrEditDocumentLibraryModalComponent } from './documentLibrary/create-or-edit-documentlibrary-modal.component';
import { ViewDocumentLibraryModalComponent } from './documentLibrary/view-documentlibrary-model.component';
import { ViewSMSBodyComponent } from './reply/smsreply/view-smsbody.component';
import { SendQuoteModalComponent } from './jobs/jobs/send-quote-email.component';
import { CreateOrEditPickListModalComponent } from './jobs/jobs/edit-picklist-model.component';
import { InvoiceIssuedComponent } from './invoices/invoiceIssued/invoiceIssued.component';
import { InvoiceIssuedSMSEmailModelComponent } from './invoices/invoiceIssued/invoice-issued-smsemail-model/invoice-issued-smsemail-model.component';
import { ApproveRejectCancelRequestComponent } from './jobs/jobs/approve-reject-jobcancelreq.component';
import { SelectDropDownModule } from 'ngx-select-dropdown';
import { NgSelectModule } from '@ng-select/ng-select';
import { InvoiceinstallerComponent } from './installer/invoiceinstaller/invoiceinstaller.component';
import { CreateNewinstallerinvoiceComponent } from './installer/newinstaller/create-newinstallerinvoice/create-newinstallerinvoice.component'
import { HttpClientModule } from '@angular/common/http';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { ActivityLogHistoryComponent } from './myleads/myleads/activity-log-history.component';;
import { InvoiceinstallerModalComponent } from './installer/invoiceinstaller/invoiceinstaller-modal.component'

import { IntallerMapComponent } from './intaller-map/intaller-map.component';
import { ReadytoPayinstallerComponent } from './installer/radytoPayInstaller/readytoPayinstaller.component';
import { ReadyTopayComponent } from './installer/paidinstaller/readyTopay.component';;
import { CreateOrEditPromotionComponent } from './promotions/promotions/create-or-edit-promotion.component';
import { InstallerInvoiceFileListComponent } from './installer/installerInvoiceFileList/installerInvoiceFileList.component';;
import { NewInvoiceIssuedComponent } from './invoices/new-invoice-issued/new-invoice-issued.component'
import { JobBookingComponent } from './installation/jobBooking.component';
import { EditinstallerinvoiceComponent } from './installer/invoiceinstaller/edit-newinstallerinvoice/editinstallerinvoice.component';
import { PendingInstallationSmsEmailComponent } from './installation/installation/pendinginstallation-sms-email-model/pendinginstallatioin-smsemail.component'; import { Stcpvdstatuscomponent } from './stcPvdStatus/stcpvdstatus/stcpvdstatus.component';
import { CreateOrEditStcStatusModalComponent } from './stcPvdStatus/stcpvdstatus/create-or-edit-StcStatus-modal.component';
import { ActivityreportComponent } from './report/activityreport/activityreport.component';
import { TodoActivityListComponent } from './report/todo-activity-list/todo-activity-list.component';
import { ReferraltrackerComponent } from './referral/referraltracker.component';
import { ReferralModalComponent } from './referral/referral-modal.component';
import { EditupdateReferralComponent } from './referral/editupdate-referral.component';
import { WarrantytrackerComponent } from './warranty/warrantytracker.component';
import { AddWarrantyattachmentComponent } from './warranty/add-warrantyattachment.component';
import { WarrantySmsEmailModelComponent } from './warranty/warranty-sms-email-model.component';
import { CreateOrEditLeadExpenseModalComponent } from './expense/leadExpenses/create-or-edit-leadExpense-modal.component';
import { EditLeadExpenseModalComponent } from './expense/leadExpenses/edit-leadExpense-modal.component';;
import { LeadexpenseReportComponent } from './report/leadexpensereport/leadexpense-report/leadexpense-report.component'
	;
import { ViewPaymentComponent } from './invoices/view-payment.component';
import { EditInvoiceComponent } from './invoices/new-invoice-issued/edit-new-invoice.component';
import { InvoicePaidSMSEmailModelComponent } from './invoices/new-invoice-issued/new-invoice-paid-smsemail-model/new-invoice-paid-smsemail-model.component';
import { ViewLalLongMissingJobComponent } from './intaller-map/view-latlongmissingdata.component';
import { EditAddressComponent } from './intaller-map/edit-address.component';
import { PromotionStopResponceComponent } from './promotions/promotionUsers/stop-responce.component';
import { InvoiceInstallerSmsemailModelComponent } from './installer/invoiceinstaller/invoiceinstaller-smsemail-model/invoiceinstaller-smsemail-model.component';
import { JobBookingSmsEmailComponent } from './installation/jobBooking-sms-email-model/jobBooking-sms-email.component';
import { InvoiceSalesReportToexcelComponent } from './report/invoice-SalesReport-To-excel.component';
import { OutStandingReportComponent } from './report/outstandingreport/outstandingreport.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { CreateOrEditServiceCategoryModalComponent } from './serviceCategory/serviceCategory/create-or-edit-serviceCategory-modal.component';
import { ViewServiceCategoryModalComponent } from './serviceCategory/serviceCategory/view-serviceCategory-modal.component';
import { ServiceCategoryComponent } from './serviceCategory/serviceCategory/serviceCategory.component';
import { ServiceSourcesComponent } from './serviceSources/serviceSources/serviceSources.component';
import { CreateOrEditServiceSourcesModalComponent } from './serviceSources/serviceSources/create-or-edit-serviceSources-modal.component';
import { ViewServiceSourcesModalComponent } from './serviceSources/serviceSources/view-serviceSources-modal.component';
import { CreateOrEditServiceStatusModalComponent } from './serviceStatus/serviceStatus/create-or-edit-serviceStatus-modal.component';
import { ViewServiceStatusModalComponent } from './serviceStatus/serviceStatus/view-serviceStatus-modal.component';
import { ServiceStatusComponent } from './serviceStatus/serviceStatus/serviceStatus.component';
import { ViewServiceTypeModalComponent } from './serviceType/serviceType/view-serviceType-modal.component';
import { CreateOrEditServiceTypeModalComponent } from './serviceType/serviceType/create-or-edit-serviceType-modal.component';
import { ServiceTypeComponent } from './serviceType/serviceType/serviceType.component';
import { ServiceSubCategoryComponent } from './serviceSubCategory/serviceSubCategory/serviceSubCategory.component';
import { CreateOrEditServiceSubCategoryModalComponent } from './serviceSubCategory/serviceSubCategory/create-or-edit-serviceSubCategory-modal.component';
import { ViewServiceSubCategoryModalComponent } from './serviceSubCategory/serviceSubCategory/view-serviceSubCategory-modal.component';
import { ManageServicesComponent } from './services/manageservices/manageservice.component';
import { MyServicesComponent } from './services/myservices/myservice.component';
import { CreateOrEditMyServiceOptionModalComponent } from './services/myservices/create-or-edit-myservice-modal.component';
import { MyServiceSmsemailModelComponent } from './services/myservices/myservice-smsemail-model/myservice-smsemail-model.component';
import { ReviewComponent } from './review/review.component';
import { ReviewSmsemailModelComponent } from './review/review/review-smsemail-model.component';
import { ReviewNotesModalComponent } from './review/review-notes-model.component';
import { ViewServiceDuplicateModalComponent } from './services/manageservices/view-duplicate-service.component';
import { ViewDuplicateServicePopUpModalComponent } from './services/myservices/duplicate-service-popup.component';
import { CreateOrEditReviewTypeModalComponent } from './reviewType/reviewType/create-or-edit-reviewType-modal.component';
import { ReviewTypeComponent } from './reviewType/reviewType/reviewType.component';
import { ViewReviewTypeModalComponent } from './reviewType/reviewType/view-reviewType-modal.component';
import { ServiceMapComponent } from './services/service-map/service-map.component'; import { PaywayGetcardDetailModelComponent } from './jobs/jobs/payway/payway-getcard-detail-model.component';

import { ServiceInstallerModalComponent } from './services/myservices/service-installer-modal.component';
import { AddServiceAttachmentComponent } from './services/myservices/add-serviceattachment.component';
import { SubCategoryAttachmentComponent } from './serviceSubCategory/serviceSubCategory/add-subcategoryattachment.component';
import { InvoicePayWaySMSEmailModelComponent } from './invoices/invoice-payway/new-invoice-payway-smsemail-model/new-invoice-payway-smsemail-model.component';
import { InvoicePayWayComponent } from './invoices/invoice-payway/invoice-payway.component';
import { MyinstallerComponent } from './installation/myinstaller/myinstaller.component';
import { CreateOrEditMyInstallerModalComponent } from './installation/myinstaller/create-or-edit-myinstaller-modal.component';
import { InstallerUploadDocComponent } from './installation/myinstaller/myintaller-uploadDoc';
import { AddMyInstallerActivityModalComponent } from './installation/myinstaller/add-myinstaller-activity-model.component';
import { MyInstallerSmsEmailModelComponent } from './installation/myinstaller/myintaller-sms-email-model.component';
import { MyinstallerDetailComponent } from './installation/myinstaller/myinstaller-detail-modal.component';
import { ServiceInstallationComponent } from './services/serviceInstallation/serviceInstallation.component';
import { ViewPriceItemListsModalComponent } from './priceItemLists/priceItemLists/view-PriceItemLists-modal.component';
import { PriceItemListsComponent } from './priceItemLists/priceItemLists/priceItemLists.component';
import { CreateOrEditPriceItemListsModalComponent } from './priceItemLists/priceItemLists/create-or-edit-PriceItemLists-modal.component';
import { CreateOrEditPriceListModalComponent } from './installation/myinstaller/create-or-edit-PriceList-modal.component';
import { HoldJobsComponent } from './jobs/holdJobs/holdJobs.component';
import { HoldjobsSmsEmailModelComponent } from './jobs/holdJobs/holdjobs-sms-email-model/holdjobs-sms-email-model.component';
import { JobHoldReasonModalComponent } from './jobs/holdJobs/add-jobholdreason-model.component';
import { ViewMyinstallerComponent } from './installation/myinstaller/view-myintaller.component';
import { ActivityLogMyinstallerComponent } from './installation/myinstaller/activity-log-myinstaller.component';
import { MyinstallerActivityLogHistoryComponent } from './installation/myinstaller/myinstaller-activity-log-history.component';
import { ViewQuoteModalComponent } from './jobs/jobs/viewquotation.component';
import { PostCodeRangeComponent } from './postcoderange/postcoderange/postcoderange.component';
import { CreateOrEditPostCodeRangeModalComponent } from './postcoderange/postcoderange/create-or-edit-postcoderange-modal.component';
import { WarehouseLocationComponent } from './warehouselocation/warehouselocation.component';
import { CreateOrEditLocationModalComponent } from './warehouselocation/create-or-edit-warehouselocation-modal.component';
import { ProductPackageComponent } from './productpackage/productpackage.component';
import { CreateOrEditProductPackageModalComponent } from './productpackage/create-or-edit-productpackage-modal.component';
import { ViewPackageModalComponent } from './jobs/jobs/viewpackage.component';
import { ViewCalculatorModalComponent } from './jobs/jobs/cal.component';

import { MyLeadGenerationComponent } from './my-lead-generation/myLeadGeneration.component';
import { LeadGenMapComponent } from './lead-generation-map/lead-gen-map.component';
import { LeadGenInstallationComponent } from './lead-generation-installation/lead-generation-installation.component';
import { DashboardWholesaleComponent } from './dashboard-wholesale/dashboard-wholesale.component';
import { WholesaleLeadsComponent } from './wholesale/leads/wholesaleleads.component';
import { CreateEditWholesaleLeadComponent } from './wholesale/leads/create-edit-wholesalelead.component';
import { WholesalePromotionsComponent } from './wholesale/promotions/promotions/promotions.component';
import { WholesaleCreateOrEditPromotionModalComponent } from './wholesale/promotions/promotions/create-or-edit-promotion-modal.component';

import { LeadDetailModal } from './wholesale/leads/lead-detail-modal.component';

import { WholesaleInvoiceComponent } from './wholesale/invoice/invoice.component';
import { createEditWholesaleInvoiceComponent } from './wholesale/invoice/create-edit-invoice.component';
import { InvoiceDetailModal } from './wholesale/invoice/invoice-detail-modal.component';
import { DashboardSalesComponent } from './dashboard-sales/dashboard-sales.component';
import { CreateOrEditWhlPromotionComponent } from './wholesale/promotions/promotions/create-or-edit-promotion.component';
import { PromotionUsersWholesaleComponent } from './wholesale/promotions/promotionUsers/promotionUsers.component';
import { WholesaleStatusTypesComponent } from './wholesale/data-vaults/wholesale-type/WholesaleStatusTypes.component';
import { WholesaleTypeModal } from './wholesale/data-vaults/wholesale-type/wholesale-type-modal.component';
import { CreateEditWholesaleStatusTypeModal } from './wholesale/data-vaults/wholesale-type/create-edit-wholesale-status-type-modal.component';
import { InvoiceTypesComponent } from './wholesale/data-vaults/invoice-type/InvoiceTypes.component';
import { InvoiceTypeModal } from './wholesale/data-vaults/invoice-type/invoice-type-modal.component';
import { CreateEditInvoiceTypeModal } from './wholesale/data-vaults/invoice-type/create-edit-invoice-type-modal.component';
import { WholesaleBDMComponent } from './wholesale/data-vaults/wholesale-bdm/wholesale-bdm.component';
import { WholesaleBDMModal } from './wholesale/data-vaults/wholesale-bdm/wholesale-bdm-modal.component';
import { CreateEditWholesaleBDMModal } from './wholesale/data-vaults/wholesale-bdm/create-edit-wholesale-bdm-modal.component';
import { DeliveryOptionsComponent } from './wholesale/data-vaults/delivery-options/delivery-options.component';
import { DeliveryOptionModal } from './wholesale/data-vaults/delivery-options/delivery-options-modal.component';
import { CreateEditDeliveryOptionModal } from './wholesale/data-vaults/delivery-options/create-edit-delivery-option-modal.component';
import { WholesaleJobTypesComponent } from './wholesale/data-vaults/job-type/JobTypes.component';
import { WholesaleJobTypeModal } from './wholesale/data-vaults/job-type/job-type-modal.component';
import { CreateEditWholesaleJobTypeModal } from './wholesale/data-vaults/job-type/create-edit-job-type-modal.component';
import { TransportTypesComponent } from './wholesale/data-vaults/transport-type/TransportTypes.component';
import { TransportTypeModal } from './wholesale/data-vaults/transport-type/transport-type-modal.component';
import { CreateEditMyLeadGenModal } from './my-lead-generation/create-edit-myLeadGeneration-modal.component';
import { DashboardServiceComponent } from './dashboard-service/dashboard-service.component';
import { Dashboard2Component } from './dashboard2/dashboard-kanban.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { LeadNewContactModal } from './wholesale/leads/add-contact-modal.component';
import { EditInvoiceModal } from './wholesale/invoice/edit-invoice-modal.component';
import { InvoiceDocumentUploadModal } from './wholesale/invoice/invoice-doc-upload-modal.component';
import { DashboardInventoryComponent } from './inventory/dashboard/dashboard-inventory.component';
// import { VendorsComponent } from './inventory/data-vaults/vendors/vendors.component';
// import { CreateVendoreModal } from './inventory/data-vaults/vendors/create-vendor-modal.component';
// import { VendoreDetailModal } from './inventory/data-vaults/vendors/vendor-view-modal.component';
// import { TransportCompanyComponent } from './inventory/data-vaults/transport-company/transport-company.component';
// import { TransportCompanyModal } from './inventory/data-vaults/transport-company/transport-company-modal.component';
// import { CreateEditCompanyModal } from './inventory/data-vaults/transport-company/create-edit-company-modal.component';
// import { StockOrderStatusComponent } from './inventory/data-vaults/stock-order-status/stock-order-status.component';
// import { StockOrderModal } from './inventory/data-vaults/stock-order-status/stock-order-modal.component';
// import { CreateEditStockOrderModal } from './inventory/data-vaults/stock-order-status/create-edit-stock-order-modal.component';
// import { PaymentMethodComponent } from './inventory/data-vaults/payment-method/payment-method.component';
// import { PaymentMethodModal } from './inventory/data-vaults/payment-method/payment-method-modal.component';
// import { CreateEditPaymentMethodModal } from './inventory/data-vaults/payment-method/create-edit-payment-method-modal.component';
// import { DeliveryTypeComponent } from './inventory/data-vaults/delivery-type/delivery-type.component';
// import { DeliveryTypeModal } from './inventory/data-vaults/delivery-type/delivery-type-modal.component';
// import { CreateDeliveryTypeModal } from './inventory/data-vaults/delivery-type/create-delivery-type-modal.component';
// import { StockOrderComponent } from './inventory/data-vaults/stock-order/stock-order.component';
// import { StockOrderForModal } from './inventory/data-vaults/stock-order/stock-order-modal.component';
// import { CreateStockOrderForModal } from './inventory/data-vaults/stock-order/create-stock-order-modal.component';
// import { StockCurrencyComponent } from './inventory/data-vaults/stock-currency/stock-currency.component';
// import { StockCurrencyForModal } from './inventory/data-vaults/stock-currency/stock-currency-modal.component';
// import { CreateStockCurrencyModal } from './inventory/data-vaults/stock-currency/create-stock-currency-modal.component';
// import { StockFromComponent } from './inventory/data-vaults/stock-from/stock-from.component';
// import { StockFromModal } from './inventory/data-vaults/stock-from/stock-from-modal.component';
// import { CreateStockFromModal } from './inventory/data-vaults/stock-from/create-stock-from-modal.component';
// import { PurchaseCompanyComponent } from './inventory/data-vaults/purchase-company/purchase-company.component';
// import { PurchaseCompanyModal } from './inventory/data-vaults/purchase-company/purchase-company-modal.component';
// import { CreateEditPurchaseCompanyModal } from './inventory/data-vaults/purchase-company/create-edit-company-modal.component';
import { StockTransferComponent } from './inventory/stock-transfer/stock-transfer.component';
import { StockTransferDetailModal } from './inventory/stock-transfer/stock-transfer-view-modal.component';
import { CreateStockTransferModal } from './inventory/stock-transfer/create-stock-transfer-modal.component';
import { PurchaseOrderComponent } from './inventory/purchase-order-new/purchase-order-new.component';
import { createEditPurchaseOrder } from './inventory/purchase-order-new/create-purchase-order.component';
import { AddStockItemModal } from './inventory/purchase-order-new/add-stock-item-modal.component';
import { StockOrderDetailModal } from './inventory/purchase-order-new/stock-order-detail-modal.component';
import { ViewAllUploadsModal } from './inventory/purchase-order-new/view-uploads-modal.component';
import { OtherChargesComponent } from './installation-cost/other-charges/other-charges.component';
import { CreateOrEditChargeModalComponent } from './installation-cost/other-charges/create-or-edit-charges-modal.component';
import { StateWiseCostComponent } from './installation-cost/statewiseCost/state-wise-cost.component';
import { createOrEditStateWiseCostModal } from './installation-cost/statewiseCost/createEdit-state-wise-cost-modal.component';
import { InstallationItemComponent } from './installation-cost/installationItemList/installation-item-list.component';
import { CreateOrEditInstallationModalComponent } from './installation-cost/installationItemList/createEdit-installation-item-modal.component';
import { FixCostPriceComponent } from './installation-cost/fixCostPrice/fix-cost-price.component';
import { createOrEditFixCostModal } from './installation-cost/fixCostPrice/createEdit-fix-cost-price-modal.component';
import { ViewInstallationModalComponent } from './installation-cost/installationItemList/view-installation-item-modal.component';
import { ExtraInstallatIonChargeComponent } from './installation-cost/extraInstallationCharge/extra-installation-cost.component';
import { createOrEditExtraInstallationModal } from './installation-cost/extraInstallationCharge/createEdit-extra-installation-cost-modal.component';
import { JobCostComponent } from './report/jobcost/jobcost.component';

import { ViewDetailsModelComponent } from './result/result/view-detail-model/view-model.component';

import { STCCostComponent } from './installation-cost/stcCost/stcCost.componenet';
import { CreateOrEditSTCCosteModalComponent } from './installation-cost/stcCost/create-or-edit-stcCost-model.componenet';
import { BatteryInstallationCostComponent } from './installation-cost/batteryInstallationCost/batteryInstalltionCost.coponent';
import { CreateOrEditBatteryInstallationCostDto, CreateOrEditStockOrderVariation } from '@shared/service-proxies/service-proxies';
import { CreateOrEditBatteryInstallationCosteModalComponent } from './installation-cost/batteryInstallationCost/create-or-edit-batteryCost-model.componenet';
import { UserCallHistoryComponent } from './report/callhistory/usercallhistory/usercallhistory.component';
import { StateWiseCallHistoryComponent } from './report/callhistory/statewisecallhistory/statewisecallhistory.component';
import { callDetailModalComponent } from './report/callhistory/usercallhistory/calldetailuserwisemodal.component';
import { statecallDetailModalComponent } from './report/callhistory/statewisecallhistory/calldetailstatewisemodal.component';
import { LeadAssignReportComponent } from './report/leadassign/leadassign.component';
import { CallFlowQueueComponent } from './callflowqueue/call-flow-queue.component';
import { createOrEditCallFlowQueueModalComponent } from './callflowqueue/create-or-edit-callFlowQueue-modal.component';
import { viewcallFlowQueueModalComponent } from './callflowqueue/view-callFlowQueue-modal-modal.component';
import { CxCallQueueComponent } from './report/callQueue/csx-call-queue.component';
import { CsxCallQueueReportModalComponent } from './report/callQueue/csx-call-queue-modal.component';
import { UserCsxCallQueueReportModalComponent } from './report/callQueue/user-csx-call-queue-modal.component';
import { WarrentyClaimComponent } from './services/warrentyClaim/warrentyClaim.component';
import { CreateOrEditServiceDocumentTypeModalComponent } from './serviceDocumentTypes/create-or-edit-serviceDocumentType-modal.component';
import { ServiceDocumentTypesComponent } from './serviceDocumentTypes/serviceDocumenttypes.component';
import { ServiceDocumentRequestLinkModalComponent } from './services/warrentyClaim/service-document-request-link.component';
import { EmployeeJobCostComponent } from './report/employeeJobCost/employeeJobcost.component';
import { ServiceCaseNotesModalComponent } from './services/warrentyClaim/create-or-edit-servicecasenotes-model.component';
import { ItemPickupModalComponent } from './services/warrentyClaim/pickup-Items-model.componenet';
import { ServiceInvoiceModelComponent } from './services/warrentyClaim/serviceInvoice.component';
import { CreateOrEditServiceInvoiceModalComponent } from './services/warrentyClaim/create-or-edit-serviceInvoice-model.component';
import { ViewWholeSalePromotionModalComponent } from './wholesale/promotions/promotions/view-promotion-modal.component';
import { WholeSaleSMSEmailModalComponent } from './wholesale/activity/sms-email-modal.component';
import { WholeSaleNotifyCommentModalComponent } from './wholesale/activity/notify-comment-modal.component';
import { WholeSaleReminderModalComponent } from './wholesale/activity/reminder-modal.component';
import { WholeSaletodoModalComponent } from './wholesale/activity/todo-modal.component';
import { WholeSaleDocumentUploadModalComponent } from './wholesale/activity/doc-model.component';
import { WholeSaleLeadDocumentTypesComponent } from './wholesale/data-vaults/wholesalelead-documenttype/wholeSaleLeadDocumenttypes.component';
import { CreateOrEditWholeSaleLeadDocumentTypeModalComponent } from './wholesale/data-vaults/wholesalelead-documenttype/create-or-edit-wholeSaleLeadDocumentType-modal.component';
import { WholeSaleSmsTemplatesComponent } from './wholesale/data-vaults/smsTemplates/smsTemplates.component';
import { ViewWholeSaleSmsTemplateModalComponent } from './wholesale/data-vaults/smsTemplates/view-smsTemplate-modal.component';
import { CreateOrEditWholeSaleSmsTemplateModalComponent } from './wholesale/data-vaults/smsTemplates/create-or-edit-smsTemplate-modal.component';
import { CreateInvoicePaidComponent } from './invoices/new-invoice-issued/createNewInvoicePaid.component';
import { ViewWholesaleLeadComponent } from './wholesale/leads/view-wholesalelead.component';
import { WholeSaleActivityLogComponent } from './wholesale/leads/wholesale-activity-log.component';
import { WholeSaleActivityLogHistoryComponent } from './wholesale/leads/activity-log-history.component';
import { WholeSaleEmailTemplatesComponent } from './wholesale/data-vaults/emailTemplates/emailTemplates.component';
import { CreateOrEditWholeSaleEmailTemplateModalComponent } from './wholesale/data-vaults/emailTemplates/create-or-edit-emailTemplates-modal.component';
import { LeadSoldReportComponent } from './report/LeadSold/leadsold.component';
import { WholesaleDashboardComponent } from './wholesale/dashboard-kanban/dashboard-kanban.component';
import { WholeSaleActivityLogDetailComponent } from './wholesale/leads/wholesale-activity-log-detail.component';
import { ActivityLogPromotionDetailComponent } from './myleads/myleads/activity-log-promotion-detail.component';
import { CommissionComponent } from './commission/commission.component';
import { CreateEditcommissionModel } from './commission/create-commission.component';
import { SMSModelComponent } from './activitylog/sms-modal.component';
import { EmailModelComponent } from './activitylog/email-modal.component';
import { NotifyModelComponent } from './activitylog/notify-modal.component';
import { CommentModelComponent } from './activitylog/comment-modal.component';
import { ToDoModalComponent} from './activitylog/todo-modal.component';
import { ReminderModalComponent } from './activitylog/reminder-modal.component';
import { IssuedActivityLogModelComponent } from './invoices/invoiceIssued/Invoiceactivitylog-model.component';
import { JobCommissionComponent } from './report/jobcommission/jobcommission.component';
import { CreateEditJobCommissionModel } from './report/jobcommission/create-jobcommission.component';
import { CommissionRangeComponent } from './commission-range/commission-range.component';
import { createOrEditCommissionRangeModalComponent } from './commission-range/commission-range-modal.component';
import { viewcommissionRangeModalComponent } from './commission-range/view-commission-range-modal.component';
import { LeadDetailsComponent } from './leaddetails/leaddetails.component';
import { DetailModalComponent } from './report/jobcommission/detailuserwisemodal.component';
import { ViewSalesModalComponent } from './dashboard-sales/view-sales.component';
import { DashboardMessageComponent } from './dashboard-message/dashboard-messages.component';
import { createOrEditDashboardMessageModalComponent } from './dashboard-message/create-edit-dashboard-messages.component';
import { viewdashboardMessageModalComponent } from './dashboard-message/view-dashboard-messages-modal.component';
import { CategoryInstallationItemComponent } from './installation-cost/categoryInstallationItem/category-installation-item.component';
import { CreateOrEditCategoryInstallationModalComponent } from './installation-cost/categoryInstallationItem/createEdit-category-installation-item-modal.component';
import { ViewCategoryInstallationModalComponent } from './installation-cost/categoryInstallationItem/view-category-installation-item-modal.component';
import { ProductSoldReportComponent } from './report/productSold/productsold.component';
import { JobCommissionPaidComponent } from './report/jobcommissionpaid/jobcommissionpaid.component';
import { SmsCountReportComponent } from './report/smscountreport/smscountreport.component';
import { ECommerceSliderComponent } from './wholesale/ecommerce/ecommerceslider/ecommerceslider.component';
import { CreateOrEditECommerceSliderModalComponent } from './wholesale/ecommerce/ecommerceslider/create-or-edit-ecommerceslider.component';
import { WholesaleProductComponent } from './wholesale/ecommerce/add-product/add-product.component';
import { CreateOrEditProductModalComponent } from './wholesale/ecommerce/add-product/create-or-edit-product-modal.component';
import { ViewProductModalComponent } from './wholesale/ecommerce/add-product/view-product-modal.component';
import { SpecialOffersComponent } from './wholesale/ecommerce/special-offers/special-offers.component';
import { CreateOrEditSpecialOfferModalComponent } from './wholesale/ecommerce/special-offers/create-or-edit-offer-modal.component';
import { ViewSpecialOfferModalComponent } from './wholesale/ecommerce/special-offers/view-offer-modal.component';
import { CreditApplicationsComponent } from './wholesale/credit/credit-applications/credit-applications.component';
import { ViewCreditApplicationModalComponent } from './wholesale/credit/credit-applications/view-credit-application-modal.component';
import { ProductDocumentModalComponent } from './wholesale/ecommerce/add-product/document-product-modal.component';
import { CreditApplicationNotesModalComponent } from './wholesale/credit/credit-applications/credit-applications-notes.component';
import { postCodeWiseCostComponent } from './installation-cost/postcodecost/postcode-wise-cost.component';
import { CreateOrEditPostCodeWiseCostModal } from './installation-cost/postcodecost/createedit-postcode-wise-cost.component';
import { SectionWiseSmsCountModelComponent } from './report/smscountreport/sectionwisesmscountreport.component';
import { ProductSoldPriceReportModelComponent } from './report/productSold/productsoldprice.component';
import { BrandingPartnerComponent } from './wholesale/ecommerce/brandingpartner/brandingpartner.component';
import { CreateOrEditBrandingPartnerModalComponent } from './wholesale/ecommerce/brandingpartner/create-or-edit-brandingpartner.component';
import { AddProductImageComponent } from './jobs/productItems/add-productimage.component';
import { UserRequestComponent } from './wholesale/ecommerce/userrequest/userrequest.component';
import { EcommerceSolarPackagePackageComponent } from './wholesale/ecommerce/solarpackage/solarpackage.component';
import { CreateOrEditEcommerceSolarPackageModalComponent } from './wholesale/ecommerce/solarpackage/create-or-edit-solarpackage-modal.component';
import { CxCallQueueWeeklyComponent } from './report/callQueueweekly/csx-call-queue-weekly.component';
import { CxCallQueueUserWiseComponent } from './report/callQueueUserwise/csx-call-queue-userwise.component';
import { CsxCallQueueUserWiseReportModalComponent } from './report/callQueueUserwise/csx-call-queue-userwise-callqueue.component';
import { ApproveUserReuestModalComponent } from './wholesale/ecommerce/userrequest/approveuserrequest-modal.component';
import { ContactUsComponent } from './wholesale/ecommerce/contactus/contactus.component';
import { UserDetailReportComponent } from './report/userdetail/userdetail.component';
import { ComparisonReportComponent } from './report/comparison/comparison.component';
import { ThirdrdPartyLeadTrackerComponent } from './leads/leads/3rd-party-lead-tracker.component';
import { CreateOrEdit3rdPartyLeadsModelComponent } from './leads/leads/createoredit-3rd-party-leads.component';
import { DataVaultActivityLogComponent } from './datavault-activitylog/datavaultactivitylog.component';
import { EditProductItemModalComponent } from './jobs/productItems/edit-productItem.component';
import { StateComponent } from './state/state.component';
import { InvoiceDetailModalComponent } from './installer/radytoPayInstaller/invoicedetail.component';
import { InstallerInvoicePaymentReportComponent } from './report/installerinvoicepayment/installerinvoicepayment.component';
import { CheckDepositReceivedComponent } from './checkDepositReceived/checkDepositReceived.component';
import { CreateOrEditCheckReceivedModalComponent } from './checkDepositReceived/create-or-edit-checkDepositeReceived-modal.component';
import { EditWholesaleLeadModalComponent } from './wholesale/leads/edit-wholesalelead.component';
import { InstallerPaymentInstallerWiseReportComponent } from './report/installerpaymentinstallerwise/installerpaymentinstallerwise.component';
import { TransportCostComponent } from './transportcost/transportcost.component';
import { CreateOrEditTransportCostModalComponent } from './transportcost/create-or-edit-transportcost-modal.component';
import { CreateOrEditCheckActiveModalComponent } from './checkActives/chcekActives/create-or-edit-checkactive-modal.component';
import { CheckActiveComponent } from './checkActives/chcekActives/checkActive.component';
import { CheckApplicationComponent } from './chcekApplications/checkApplications/check-application.component';
import { CreateOrEditCheckApplicationModalComponent } from './chcekApplications/checkApplications/create-or-edit-checkapplication-modal.component';
import { StockOrderStatusComponent } from './quickstock/stockorderstatus/stockorderstatus.component';
import { CreateOrEditStockOrderStatusModalComponent } from './quickstock/stockorderstatus/createoredit-stockorderstatus-model.component';
import { DeliveryTypeComponent } from './quickstock/deliverytype/deliverytype.component';
import { CreateOrEditDeliveryTypeModalComponent } from './quickstock/deliverytype/createoredit-deliverytype-modal.component';
import{ PurchaseCompanyComponent } from './quickstock/PurchaseCompany/purchase-company.component';
import{ CreateOrEditPurchaseCompanyModalComponent } from  './quickstock/PurchaseCompany/create-or-edit-purchase-company-modal.component';
import { TransportcompanyComponent } from './transportcompany/transportcompany.component';
import { CreateOrEditTransportCompanyModalComponent } from './transportcompany/create-or-edit-transportcompany-modal.component';
import { CreateOrEditPaymentMethodModalComponent } from './quickstock/paymentmethod/createoredit-paymentmethod-modal.component';
import { PaymentMethodComponent } from './quickstock/paymentmethod/paymentmethod.component';
import { CreateOrEditPaymentStatusModalComponent } from './quickstock/paymentstatus/creteoredit-paymentstatus-modal.component';
import { PaymentStatusComponent } from './quickstock/paymentstatus/paymentstatus.component';
import { PaymentTypeComponent } from './quickstock/paymenttype/paymenttype.component';
import { CreateOrEditPaymentTypeModalComponent } from './quickstock/paymenttype/createoredit-paymenttype-modal.component';
import { CurrencyComponent } from './quickstock/currency/currency.component';
import { CreateOrEditCurrencyModalComponent } from './quickstock/currency/createoredit-currency-Modal.component';
import { FreightCompanyComponent } from './quickstock/freightcompany/freightcompany.component';
import { CreateOrEditFreightCompanyModalComponent } from './quickstock/freightcompany/createoredit-freightcompany-modal.component';
import { StockFromComponent } from './quickstock/stockfrom/stockfrom.component';
import { CreateOrEditStockFromModalComponent } from './quickstock/stockfrom/createoredit-stockfrom-modal.component';
import { CreateOrEditStockOrderForModalComponent } from './quickstock/stockorderfor/createoredit-stockorderfor-modal.component';
import { StockOrderForComponent } from './quickstock/stockorderfor/stockorderfor.component';
import { PurchaseDocumentListComponent } from './quickstock/purchasedocumentlist/purchasedocumentlist.component';
import { CreateOrEditPurchaseDocumentListModalComponent } from './quickstock/purchasedocumentlist/creteoredit-purchasedocumentlist-modal.component';
import { JobCostMonthWiseComponent } from './report/jobcostmonthwise/jobcostmonthwise.component';
import { ViewDuplicateWholesaleLeadPopUpModalComponent } from './wholesale/leads/duplicate-wholesalelead.component';
import{CreateOrEditVendorModalComponent} from './quickstock/vendors/createoredit-vendor-modal.component';
import {vendorComponent } from './quickstock/vendors/vendor.component';
import { ViewVendorModalComponent } from './quickstock/vendors/viewvendor.modal.component';
import {ViewPurchaseCompanyModalComponent} from './quickstock/PurchaseCompany/view-purchase-company-modal.component'
import { SubscriberEmailModalComponent } from './wholesale/ecommerce/subcribers/sendemail-subscriber-modal.component';
import { EcommerceSubscriberComponent } from './wholesale/ecommerce/subcribers/subscriber.component';
import { ProductImageModalComponent } from './wholesale/ecommerce/add-product/image-product-modal.component';
import { AddtoLaedContactUsModalComponent } from './wholesale/ecommerce/contactus/addtolead-modal.component';
import { ContactUSSMSEmailModalComponent } from './wholesale/ecommerce/contactus/contactus-sms-email-modal.component';
import { CreateOrEditECommerceContactUsModalComponent } from './wholesale/ecommerce/contactus/create-or-edit-contactus.component';
import{CreateOrEditStockOrderModalComponent} from './quickstock/stockorder/create-or-edit-stock-order-modal.component';
import {StockOrderComponent } from './quickstock/stockorder/stock-order.component';
import {StockTransfersComponent} from './quickstock/stockTransfers/stock-transfers.component';
import { CreateOrEditStockTransferModalComponent} from './quickstock/stockTransfers/create-or-edit-stock-transfer-modal.componenet';
import {SMSModalComponent} from './inventory/Activity/sms-modal-component';
import{ViewStockOrderModalComponent} from './quickstock/stockorder/view-stock-order-modal.component';
import {EmailModalComponent} from './inventory/Activity/email-modal-component';
import{RemindersModalComponent} from './inventory/Activity/reminders-modal-component';
import{CommentsModalComponent} from './inventory/Activity/comments-modal.component';
import{IncoTermComponent} from './quickstock/incoTerm/incoterm.component';
import{CreateOrEditIncoTermModalComponent} from './quickstock/incoTerm/create-or-edit-incoterm-modal.component';
import{CreateOrEditPurchaseOrderModalComponent} from './inventory/purchase-order-new/create-or-edit-purchase-order-modal.component';
import { WholeSaleLeadHeaderResultsComponent } from './wholesaleheaderresult/wholesaleheaderresult.component';
import { ViewResultWholeSaleLeadComponent } from './wholesaleheaderresult/view-wholesaleheaderresult.component';
import { ViewDetailsWholeSaleModelComponent } from './wholesaleheaderresult/view-detail-model/view-wholesale-model.component';
import { JobVariationReportComponent } from './report/jobvariation/jobvariation.component';
import { ViewEcommerceSliderModalComponent } from './wholesale/ecommerce/ecommerceslider/view-ecommerceslider.component';
import { ViewBrandingPartnerModalComponent } from './wholesale/ecommerce/brandingpartner/view-brandingpartner.component';
import { ViewEcommerceSolarPackageModalComponent } from './wholesale/ecommerce/solarpackage/view-solarpackage.component';
import{ViewPurchaseOrderDetailComponent} from './inventory/purchase-order-new/purchase-order-details.component';
import {PurchaseOrderActivityLogComponent} from './inventory/purchase-order-new/purchase-order-activity-log.component';
import {PurchaseOrderActivityLogHistoryComponent} from './inventory/purchase-order-new/activity-log-history.component';
import { ViewPostCodeWiseCostModalComponent } from './installation-cost/postcodecost/view-postcode-wise-cost.component';
import { MonthlyComparisonReportComponent } from './report/monthlycomparison/monthlycomparison.component';
import { CreateOrEditSeriesModalComponent } from './wholesale/data-vaults/series/create-or-edit-series-Modal.component';
import { SeriesComponent } from './wholesale/data-vaults/series/series.component';
import { RequestToWholeSaleLeadTransferComponent } from './wholesale/requesttotransferlead/requesttotransferlead.component';
import {WholeSaleDataVaultActivityLogComponent} from './wholesale/data-vaults/activity/wholesale-datavault-activitylog.component';
import {EcommerceDataVaultActivityLogComponent} from './wholesale/ecommerce/activity/ecommerce-datavault-activitylog.component';
import { CreateEditTransportTypeModal } from './wholesale/data-vaults/transport-type/create-edit-transport-type-modal.component';
import { PropertyTypeComponent } from './wholesale/data-vaults/propery-type/propertyTypes.component';
import { CreateOrEditPropertyModalComponent } from './wholesale/data-vaults/propery-type/create-or-edit-property-type-modal.component';
import { PVDStatusComponent } from './wholesale/data-vaults/pvd-status/pvdStatus.component';
import { CreateOrEditPVDStatusModalComponent } from './wholesale/data-vaults/pvd-status/create-or-edit-pvd-status-modal.component';
import {EssentialTrackerComponent} from './jobs/essentialTracker/eseentialTracker.component';
import {AddEssentialNotesModalComponent} from './jobs/essentialTracker/add-notes-model.component';
import { WholesaleSmsReplyComponent } from './wholesale/reply/wholesale-sms-reply.component';
import {EssentialTrackerActiveModalComponent} from './jobs/essentialTracker/essential-Tracker-active.component';
import { EcommerceProductTypeComponent } from './wholesale/ecommerce/producttype/ecommerceproducttype.component';
import { CreateOrEditEcommerceProductTypeModalComponent } from './wholesale/ecommerce/producttype/createoredit-ecommerceproducttype.component';
import{LeadSourceComparisonReportComponent} from './report/leadSourceComparison/leadSourceComparison-report.component';	
import { RescheduleInstallationReportComponent } from './report/rescheduleinstallation/rescheduleinstallation.component';
import { RescheduleInstallationHistoryModalComponent } from './report/rescheduleinstallation/rescheduleinstallationhistory-modal.component';
import { JobActualCostModalComponent } from './jobs/jobs/job-actualcost-model.component';
import { WholesaleJobStatusComponent } from './wholesale/data-vaults/jobStatus/jobStatus.component';
import { CreateOrEditWholesaleJobStatusModalComponent } from './wholesale/data-vaults/jobStatus/create-or-edit-job-status-modal.component';
import {InstallerDeatilsComponent} from './report/installerpaymentinstallerwise/installerdetails.component';
import { EcommerceSpecificationComponent } from './wholesale/ecommerce/specification/specification.component';
import { CreateOrEditSpecificationModalComponent } from './wholesale/ecommerce/specification/create-or-edit-specification-modal.component';
import { ProductSpecificationModalComponent } from './wholesale/ecommerce/add-product/specification-product-modal.component';
import {StockTranferDetailComponent} from './quickstock/stockTransfers/stock-Transfer-details.component';
import {StockTranferActivityLogComponent} from './quickstock/stockTransfers/stock-transfer-activity-log.component';
import { StockTransferActivityLogHistoryComponent} from './quickstock/stockTransfers/stock-Transfer-activity-log-histroy.component';
import{StockOrderToDoModalComponent} from './inventory/Activity/todo-modal.component';
import{ViewPoModalComponent} from './inventory/purchase-order-new/view-po.component';
import { VariationTypePriceReportModelComponent } from './report/jobvariation/jobvariationTypeAmountState.component';
import { JobCostFixExpenseComponent } from './installation-cost/jobcostfixexpense/jobcostfixexpense.component';
import { ViewJobCostFixExpenseModalComponent } from './installation-cost/jobcostfixexpense/view-jobcostfixexpense-modal.component';
import { CreateOrEditJobCostFixExpenseModalComponent } from './installation-cost/jobcostfixexpense/create-or-edit-jobcostfixexpense-modal.component';
import{CreateOrEditStockPaymentModalComponent} from './inventory/stock-payment/create-or-edit-stock-payment-modal.component';
import{StockPaymentComponent} from './inventory/stock-payment/stock-payment.component';
import { CIMETLeadTrackerComponent } from './leads/leads/cimet-tracker.component';
import {SuggestedProductModalComponent} from './jobs/jobs/suggested-product-modal.component';
import { StockPaymentTrackerComponent } from './inventory/stock-payment-tracker/stock-payment-tracker.component';
import { StockVariationsComponent } from './quickstock/stock-variation/stock-variation.component';
import { CreateOrEditStockVariationModalComponent } from './quickstock/stock-variation/crete-or-edit-stock-variation.component';
import { ViewStockVariationModalComponent } from './quickstock/stock-variation/view-stock-variation.component';
import{InstallerinvoiceJobwiseComponent} from './report/installerinvoicepayment/InstallerinvoiceJobwise-modal.component';
import {EcommerceStcRegisterComponent} from './wholesale/ecommerce/stcregister/stcregister.component';
import{EcommerceStcDealPriceComponent} from './wholesale/ecommerce/stcDealPrice/stcdealprice.component';
import{CreateOrEditStcDealPriceModalComponent} from './wholesale/ecommerce/stcDealPrice/create-edit-stcDealPrice-modal.component';
import { InventoryDataVaultActivityLogComponent } from './inventory/data-vaults/activitylog/inventorydatavaultactivitylog.Component';
import{StockOrderReceivedComponent} from './inventory/reports/stockOrderReceivedReport.component';
import{ReviewReportComponent} from './report/reviewReport/reviewreport.component';
import{leadMonthlyComparison} from './report/leadmonthlycomparison/lead-monthly-comparison.component';
import{EmployeeProfitReportComponent} from './report/employeeProfitReport/employeeProfitReport.component';
import { CreateOrEditStateModalComponent } from './state/create-or-edit-state-modal.component';
import {InstallerInvoiceDetailModalComponent} from './report/installerpaymentinstallerwise/installerInvoicedetail-modal.component';
import {CreateOrEditSerialNoModalComponent} from './inventory/data-vaults/serialnostatuses/create-serialnostatus-modal.component';
import {SerialNoStatusComponent} from './inventory/data-vaults/serialnostatuses/serialnoStatus.component';
import {SerialNoHistoryReportComponent} from './inventory/reports/serialNoHistoryReport.component';
import {ApplicationFeeTrackerComponent} from './jobs/applicationfeeTracker/applicationFeeTracker.component';
import{ProgressReportComponent}from './report/progessReport/progressReport.component';
import{VoucherComponent}from './vouchers/voucher.component';
import{CreateOrEditVoucherModalComponent}from './vouchers/create-or-edit-voucher-modal.component' ;
NgxBootstrapDatePickerConfigService.registerNgxBootstrapDatePickerLocales();

FullCalendarModule.registerPlugins([
	dayGridPlugin,
	timeGridPlugin,
	interactionPlugin
]);

@NgModule({
	imports: [		
    	DragDropModule,
		HttpClientModule,
		AngularEditorModule,
		NgSelectModule,
		SelectDropDownModule,
		FileUploadModule,
		AutoCompleteModule,
		PaginatorModule,
		EditorModule,
		InputMaskModule,
		TableModule,
		CommonModule,
		FormsModule,
		ModalModule,
		TabsModule,
		TooltipModule,
		AppCommonModule,
		UtilsModule,
		MainRoutingModule,
		CountoModule,
		NgxChartsModule,

		BsDatepickerModule.forRoot(),
		BsDropdownModule.forRoot(),
		PopoverModule.forRoot(),
		AgmCoreModule.forRoot({
			apiKey: 'AIzaSyALoDvKqgmTfpfPOEjqS2fI1kcabuxImMA',
			libraries: ['places']
		}),
		MatGoogleMapsAutocompleteModule,
		EmailEditorModule,
		AppBsModalModule,
		FullCalendarModule,
		OwlDateTimeModule,
		OwlNativeDateTimeModule,
		NgxSpinnerModule,
		
	],

	declarations: [
		LeadGenMapComponent,
		InstallerInvoiceFileListComponent,
		ReadyTopayComponent,
		IntallerMapComponent,
		SendQuoteModalComponent,
		AddAttachmentProductItemComponent,
		DocumentRequestLinkModalComponent,
		SmsTemplatesComponent,

		ViewSmsTemplateModalComponent,
		CreateOrEditSmsTemplateModalComponent,
		FreebieTransportsComponent,

		ViewFreebieTransportModalComponent,
		CreateOrEditFreebieTransportModalComponent,
		InvoiceStatusesComponent,

		ViewInvoiceStatusModalComponent,
		CreateOrEditInvoiceStatusModalComponent,
		ViewDuplicatePopUpModalComponent,
		ActiveJobModalComponent,
		JobActiveRequestComponent,
		JobFinanceDetailModalComponent,
		FinanceTrackerComponent,
		StcTrackerComponent,
		CreateOrEditJobStcModalComponent,
		HoldReasonsComponent,
		PendingInstallationComponent,
		GridConnectionTrackerComponent,
		GridConnectionDetailComponent,
		AddMeterDetailModalComponent,
		PendingInstallationDetailComponent,
		ViewHoldReasonModalComponent,
		CreateOrEditHoldReasonModalComponent,
		JobDetailModalComponent,
		JobCancellationReasonComponent,
		CreateOrEditJobCancellationReasonModalComponent,
		JobActiveModalComponent,
		JobStatusModalComponent,
		CreateOrEditJobInvoiceModalComponent,
		DocumentTypesComponent,
		ViewDepartmentModalComponent,
		CreateOrEditDocumentTypeModalComponent,
		CreateOrEditJobRefundModalComponent,
		ViewJobRefundModalComponent,
		JobRefundsComponent,
		JobsListComponent,
		RefundJobModalComponent,
		RefundReasonsComponent,
		ViewRefundReasonModalComponent,
		CreateOrEditRefundReasonModalComponent,
		InstallationComponent,
		CalendersComponent,
		VariationsComponent,
		ViewVariationModalComponent,
		CreateOrEditVariationModalComponent,
		InvoicePaymentMethodsComponent,
		ViewInvoicePaymentMethodModalComponent,
		CreateOrEditInvoicePaymentMethodModalComponent,
		InvoicePaymentsComponent,
		CreateOrEditInvoicePaymentComponent,
		jobGridComponent,
		JobsListComponent,
		PromotionMastersComponent,
		FreebiesComponent,
		FreebiesTrackingModalComponent,
		ViewPromotionMasterModalComponent,
		CreateOrEditPromotionMasterModalComponent,
		FinanceOptionsComponent,
		ViewFinanceOptionModalComponent,
		CreateOrEditFinanceOptionModalComponent,
		HouseTypesComponent,
		ViewHouseTypeModalComponent,
		CreateOrEditHouseTypeModalComponent,
		CancelReasonsComponent,
		ViewCancelReasonModalComponent,
		CreateOrEditCancelReasonModalComponent,
		RejectReasonsComponent,
		ViewRejectReasonModalComponent,
		CreateOrEditRejectReasonModalComponent,
		ViewResultLeadComponent,
		ResultsComponent,
		MeterPhasesComponent,
		ViewMeterPhaseModalComponent,
		CreateOrEditMeterPhaseModalComponent,
		MeterUpgradesComponent,
		ViewMeterUpgradeModalComponent,
		CreateOrEditMeterUpgradeModalComponent,
		ElecRetailersComponent,
		ViewElecRetailerModalComponent,
		CreateOrEditElecRetailerModalComponent,
		DepositOptionsComponent,
		ViewDepositOptionModalComponent,
		CreateOrEditDepositOptionModalComponent,
		PaymentOptionsComponent,
		ViewPaymentOptionModalComponent,
		CreateOrEditPaymentOptionModalComponent,
		JobStatusesComponent,
		ViewJobStatusModalComponent,
		CreateOrEditJobStatusModalComponent,
		ProductItemsComponent,
		ViewProductItemComponent,
		CreateOrEditProductItemComponent,
		ElecDistributorsComponent,
		ViewElecDistributorModalComponent,
		CreateOrEditElecDistributorModalComponent,
		RoofAnglesComponent,
		ViewRoofAngleModalComponent,
		CreateOrEditRoofAngleModalComponent,
		RoofTypesComponent,
		ViewRoofTypeModalComponent,
		CreateOrEditRoofTypeModalComponent,
		ProductTypesComponent,
		ViewProductTypeModalComponent,
		CreateOrEditProductTypeModalComponent,
		JobTypesComponent,
		ViewJobTypeModalComponent,
		ViewJobTypeComponent,
		CreateOrEditJobTypeModalComponent,
		CreateOrEditJobTypeComponent,
		CreateOrEditTeamModalComponent,
		CreateOrEditTeamComponent,
		TeamsComponent,
		ViewTeamModalComponent,
		ViewTeamComponent,
		ViewLeadSourceComponent,
		ViewLeadSourceModalComponent,
		LeadSourcesComponent,
		CreateOrEditLeadSourceComponent,
		CreateOrEditLeadSourceModalComponent,
		PromotionUsersComponent,
		ViewPromotionUserModalComponent,
		CreateOrEditPromotionUserModalComponent,
		PromotionsComponent,
		ViewPromotionModalComponent,
		CreateOrEditPromotionModalComponent,
		PromotionResponseStatusesComponent,
		ViewPromotionResponseStatusModalComponent,
		CreateOrEditPromotionResponseStatusModalComponent,
		DepartmentsComponent,
		ViewDepartmentModalComponent,
		CreateOrEditDepartmentModalComponent,
		UserDashboardComponent,//TransferLeadModalComponent,
		LeadExpensesComponent,

		ViewLeadExpenseModalComponent,
		UserTeamsComponent,
		CreateOrEditUserTeamComponent,
		ViewUserTeamComponent,
		CategoriesComponent,
		CreateOrEditCategoryModalComponent,
		ViewCategoryModalComponent,
		TeamsComponent,
		CreateOrEditTeamModalComponent,
		ViewTeamModalComponent,
		CancelleadsComponent,
		DuplicateleadsComponent,
		ViewDuplicateModalComponent,
		ClosedleadsComponent,
		ViewClosedLeadComponent,
		LeadsComponent,
		ViewLeadComponent,
		CreateOrEditLeadComponent,
		AssignOrTransferLeadComponent,
		ActivityLogLeadComponent,
		ChangeStatusLeadComponent,
		MyLeadsComponent, AddActivityModalComponent,
		VerifyInvoiceModalComponent,
		JobNotesModalComponent,
		ViewMyLeadComponent,
		ActivityLogMyLeadComponent,
		LeadSourcesComponent,
		ViewLeadSourceModalComponent,
		CreateOrEditLeadSourceModalComponent,
		PostalTypesComponent,
		ViewPostalTypeModalComponent,
		CreateOrEditPostalTypeModalComponent,
		PostCodesComponent,
		ViewPostCodeModalComponent,
		CreateOrEditPostCodeModalComponent,
		StreetNamesComponent,
		ViewStreetNameModalComponent,
		CreateOrEditStreetNameModalComponent,
		StreetTypesComponent,
		// ViewStreetTypeComponent,		CreateOrEditStreetTypeComponent,
		ViewStreetTypeModalComponent,
		CreateOrEditStreetTypeModalComponent,
		UnitTypesComponent,
		// ViewUnitTypeComponent,		CreateOrEditUnitTypeComponent,
		ViewUnitTypeModalComponent,
		CreateOrEditUnitTypeModalComponent,
		DashboardComponent,
		DashboardInvoiceComponent,
		LeadTrackerComponent,
		JobsComponent,
		EmailTemplateComponent,
		RejectleadsComponent,
		NewinstallerComponent,
		EditupdateNewinvoiceComponent,
		ViewApplicationModelComponent,
		JobSmsEmailModelComponent,
		FinanceSmsEmailModelComponent,
		JobActiveSmsEmailModelComponent,
		JobgridSmsEmailModelComponent,
		StcSmsEmailModelComponent,
		GridconnectionSmsemailModelComponent,
		FreebiesSmsEmailModelComponent,
		SmsEmailModelComponent,
		JobRefundSmsemailModelComponent,
		ViewJobcancellationreasonModelComponent,
		ViewDocumentTypeModalComponent,
		CreateEditLeadComponent,
		SmsReplyComponent,
		EmailReplyComponent,
		ViewEmailBodyComponent,
		ViewDocumentLibraryModalComponent,
		CreateOrEditDocumentLibraryModalComponent,
		DocumentLibrarysComponent,
		ViewSMSBodyComponent,
		CreateOrEditPickListModalComponent,
		InvoiceIssuedComponent,
		InvoiceIssuedSMSEmailModelComponent,
		ApproveRejectCancelRequestComponent,
		InvoiceinstallerComponent,
		CreateNewinstallerinvoiceComponent,
		InvoiceinstallerModalComponent,
		ReadytoPayinstallerComponent,
		ActivityLogHistoryComponent,
		CreateOrEditPromotionComponent,
		NewInvoiceIssuedComponent,
		JobBookingComponent,
		MyLeadGenerationComponent,
		EditinstallerinvoiceComponent,
		PendingInstallationSmsEmailComponent,
		Stcpvdstatuscomponent,
		CreateOrEditStcStatusModalComponent,
		ActivityreportComponent,
		CreateOrEditLeadExpenseModalComponent,
		TodoActivityListComponent,
		 ReferraltrackerComponent, ReferralModalComponent, EditupdateReferralComponent, WarrantytrackerComponent, AddWarrantyattachmentComponent, WarrantySmsEmailModelComponent, EditLeadExpenseModalComponent
		, LeadexpenseReportComponent,
		ViewPaymentComponent,
		EditInvoiceComponent,
		CreateInvoicePaidComponent,
		InvoicePaidSMSEmailModelComponent,
		ViewLalLongMissingJobComponent,
		EditAddressComponent,
		PromotionStopResponceComponent,
		InvoiceInstallerSmsemailModelComponent,
		JobBookingSmsEmailComponent,
		InvoiceSalesReportToexcelComponent,
		OutStandingReportComponent,
		CreateOrEditServiceCategoryModalComponent,
		ViewServiceCategoryModalComponent,
		ServiceCategoryComponent,
		ServiceSourcesComponent,
		CreateOrEditServiceSourcesModalComponent,
		ViewServiceSourcesModalComponent,
		CreateOrEditServiceStatusModalComponent,
		ViewServiceStatusModalComponent,
		ServiceStatusComponent,
		ManageServicesComponent,
		ViewServiceTypeModalComponent,
		CreateOrEditServiceTypeModalComponent,
		ServiceTypeComponent,
		ViewServiceSubCategoryModalComponent,
		CreateOrEditServiceSubCategoryModalComponent,
		ServiceSubCategoryComponent,
		MyServicesComponent,
		CreateOrEditMyServiceOptionModalComponent,
		MyServiceSmsemailModelComponent,
		ReviewComponent,
		ReviewSmsemailModelComponent,
		ReviewNotesModalComponent,
		ViewServiceDuplicateModalComponent,
		ViewDuplicateServicePopUpModalComponent,
		ViewReviewTypeModalComponent,
		ReviewTypeComponent,
		CreateOrEditReviewTypeModalComponent,
		ServiceMapComponent,
		PaywayGetcardDetailModelComponent,
		ServiceInstallerModalComponent,
		AddServiceAttachmentComponent,
		SubCategoryAttachmentComponent,
		InvoicePayWaySMSEmailModelComponent,
		InvoicePayWayComponent,
		MyinstallerComponent,
		CreateOrEditMyInstallerModalComponent,
        InstallerUploadDocComponent,
        AddMyInstallerActivityModalComponent,
        MyInstallerSmsEmailModelComponent,
        MyinstallerDetailComponent,
		ServiceInstallationComponent,
		CreateOrEditPriceItemListsModalComponent,
		PriceItemListsComponent,
		ViewPriceItemListsModalComponent,
		CreateOrEditPriceListModalComponent,
		HoldJobsComponent,
		HoldjobsSmsEmailModelComponent,
		JobHoldReasonModalComponent,
		ViewMyinstallerComponent,
		ActivityLogMyinstallerComponent,
		MyinstallerActivityLogHistoryComponent,
		ViewQuoteModalComponent,
		QuotationTemplateComponent,
		CreateOrEditQuotationTemplateModalComponent,
		PostCodeRangeComponent,
		CreateOrEditPostCodeRangeModalComponent,
		WarehouseLocationComponent,
		CreateOrEditLocationModalComponent,
		ProductPackageComponent,
		CreateOrEditProductPackageModalComponent,
		ViewPackageModalComponent,
		ViewCalculatorModalComponent,
		LeadGenInstallationComponent,
		DashboardWholesaleComponent,
		WholesaleLeadsComponent,	
		CreateEditWholesaleLeadComponent,
		WholesalePromotionsComponent,
		WholesaleCreateOrEditPromotionModalComponent,
		CreateOrEditWhlPromotionComponent,			
		LeadDetailModal,
		WholeSaleSMSEmailModalComponent,
		WholeSaletodoModalComponent,
		WholeSaleDocumentUploadModalComponent,
		WholesaleInvoiceComponent,
		createEditWholesaleInvoiceComponent,	
		InvoiceDetailModal,
		WholeSaleReminderModalComponent,
		WholeSaleNotifyCommentModalComponent,
		DashboardSalesComponent,
		CreateOrEditWhlPromotionComponent,
		PromotionUsersWholesaleComponent,
		WholesaleStatusTypesComponent,
		ViewJobTypeModalComponent,
		WholesaleTypeModal,
		CreateEditWholesaleStatusTypeModal,
		InvoiceTypesComponent,
		InvoiceTypeModal,
		CreateEditInvoiceTypeModal,
		WholesaleBDMComponent,
		WholesaleBDMModal,
		CreateEditWholesaleBDMModal,
		DeliveryOptionsComponent,
		DeliveryOptionModal,
		CreateEditDeliveryOptionModal,
		WholesaleJobTypesComponent,		
		WholesaleJobTypeModal,
		CreateEditWholesaleJobTypeModal,	
		TransportTypesComponent,
		TransportTypeModal,
		CreateEditMyLeadGenModal,
		DashboardServiceComponent,
		Dashboard2Component,
		LeadNewContactModal,		
		EditInvoiceModal,
		InvoiceDocumentUploadModal,
		DashboardInventoryComponent,
		// VendorsComponent,
		// CreateVendoreModal,
		// VendoreDetailModal,
		// TransportCompanyComponent,
		// TransportCompanyModal,
		// CreateEditCompanyModal,		
		// StockOrderStatusComponent,
		// StockOrderModal,
		// CreateEditStockOrderModal,
		// PaymentMethodComponent,
		// PaymentMethodModal,
		// CreateEditPaymentMethodModal,
		// DeliveryTypeComponent,
		// DeliveryTypeModal,
		// CreateDeliveryTypeModal,
		// StockOrderComponent,
		// StockOrderForModal,			
		// CreateStockOrderForModal,
		// StockCurrencyComponent,
		// StockCurrencyForModal,
		// CreateStockCurrencyModal,
		// StockFromComponent,
		// StockFromModal,
		// CreateStockFromModal,		
		// PurchaseCompanyComponent,
		// PurchaseCompanyModal,
		// CreateEditPurchaseCompanyModal,
		StockTransferComponent,
		StockTransferDetailModal,
		CreateStockTransferModal,
		PurchaseOrderComponent,		
		createEditPurchaseOrder,
		AddStockItemModal,		
		StockOrderDetailModal,
		ViewAllUploadsModal,
		OtherChargesComponent,
		CreateOrEditChargeModalComponent,
		StateWiseCostComponent,
		createOrEditStateWiseCostModal,
		InstallationItemComponent,
		CreateOrEditInstallationModalComponent,
		FixCostPriceComponent,
		createOrEditFixCostModal,
		ViewInstallationModalComponent,
		ExtraInstallatIonChargeComponent,
		createOrEditExtraInstallationModal,
		JobCostComponent,
		ViewDetailsModelComponent,
		STCCostComponent,
		CreateOrEditSTCCosteModalComponent,
		BatteryInstallationCostComponent,
		CreateOrEditBatteryInstallationCosteModalComponent,
		UserCallHistoryComponent,
		StateWiseCallHistoryComponent,
		callDetailModalComponent,
		statecallDetailModalComponent,
		LeadAssignReportComponent,
		CallFlowQueueComponent,
		createOrEditCallFlowQueueModalComponent,
		viewcallFlowQueueModalComponent,
		CxCallQueueComponent,
		CsxCallQueueReportModalComponent,
		UserCsxCallQueueReportModalComponent,
		WarrentyClaimComponent,
		ServiceDocumentTypesComponent,
		CreateOrEditServiceDocumentTypeModalComponent,
		ServiceDocumentRequestLinkModalComponent,
		EmployeeJobCostComponent,
		ServiceCaseNotesModalComponent,
		ItemPickupModalComponent,
		ServiceInvoiceModelComponent,
		CreateOrEditServiceInvoiceModalComponent,
		ViewWholeSalePromotionModalComponent,
		WholeSaleLeadDocumentTypesComponent,
		CreateOrEditWholeSaleLeadDocumentTypeModalComponent,
		WholeSaleSmsTemplatesComponent,
		ViewWholeSaleSmsTemplateModalComponent,
		CreateOrEditWholeSaleSmsTemplateModalComponent,
		ViewWholesaleLeadComponent,
		WholeSaleActivityLogComponent,
		WholeSaleActivityLogHistoryComponent,
		WholeSaleActivityLogDetailComponent,
		WholeSaleEmailTemplatesComponent,
		CreateOrEditWholeSaleEmailTemplateModalComponent,
		LeadSoldReportComponent,
		WholesaleDashboardComponent,
		ActivityLogPromotionDetailComponent,
		CommissionComponent,
		CreateEditcommissionModel,
		SMSModelComponent,
		EmailModelComponent,
		NotifyModelComponent,
		CommentModelComponent,
		ToDoModalComponent,
		ReminderModalComponent,
		IssuedActivityLogModelComponent,
		JobCommissionComponent,
		CreateEditJobCommissionModel,
		CommissionRangeComponent,
		createOrEditCommissionRangeModalComponent,
		viewcommissionRangeModalComponent,
		LeadDetailsComponent,
		DetailModalComponent,
		ViewSalesModalComponent,
		DashboardMessageComponent,
		createOrEditDashboardMessageModalComponent,
		viewdashboardMessageModalComponent,
		CategoryInstallationItemComponent,
		CreateOrEditCategoryInstallationModalComponent,
		ViewCategoryInstallationModalComponent,
		ProductSoldReportComponent,
		JobCommissionPaidComponent,
		SmsCountReportComponent,
		ECommerceSliderComponent,
		CreateOrEditECommerceSliderModalComponent,
		WholesaleProductComponent,
		CreateOrEditProductModalComponent,
		ViewProductModalComponent,
		SpecialOffersComponent,
		CreateOrEditSpecialOfferModalComponent,
		ViewSpecialOfferModalComponent,
		CreditApplicationsComponent,
		ViewCreditApplicationModalComponent,
		ProductDocumentModalComponent,
		CreditApplicationNotesModalComponent,
		postCodeWiseCostComponent,
		CreateOrEditPostCodeWiseCostModal,
		SectionWiseSmsCountModelComponent,
		ProductSoldPriceReportModelComponent,
		BrandingPartnerComponent,
		CreateOrEditBrandingPartnerModalComponent,
		// ViewServiceDocumentTypeModalComponent
		AddProductImageComponent,
		UserRequestComponent,
		EcommerceSolarPackagePackageComponent,
		CreateOrEditEcommerceSolarPackageModalComponent,
		CxCallQueueWeeklyComponent,
		CxCallQueueUserWiseComponent,
		CsxCallQueueUserWiseReportModalComponent,
		ApproveUserReuestModalComponent,
		ContactUsComponent,
		UserDetailReportComponent,
		ComparisonReportComponent,
		ThirdrdPartyLeadTrackerComponent,
		CreateOrEdit3rdPartyLeadsModelComponent,
		DataVaultActivityLogComponent,
		EditProductItemModalComponent,
		StateComponent,
		InvoiceDetailModalComponent,
		InstallerInvoicePaymentReportComponent,
		CheckDepositReceivedComponent,
        CreateOrEditCheckReceivedModalComponent,
		EditWholesaleLeadModalComponent,
		InstallerPaymentInstallerWiseReportComponent,
		TransportCostComponent,
		CreateOrEditTransportCostModalComponent,
        CreateOrEditCheckActiveModalComponent,
        CheckActiveComponent,
        CheckApplicationComponent,
        CreateOrEditCheckApplicationModalComponent,
		StockOrderStatusComponent,
		CreateOrEditStockOrderStatusModalComponent,
		DeliveryTypeComponent,
		CreateOrEditDeliveryTypeModalComponent,
		PurchaseCompanyComponent,
		CreateOrEditPurchaseCompanyModalComponent,
		TransportcompanyComponent,
		CreateOrEditTransportCompanyModalComponent,
		PaymentStatusComponent,
        CreateOrEditPaymentStatusModalComponent,
        PaymentMethodComponent,
        CreateOrEditPaymentMethodModalComponent,
		PaymentTypeComponent,
		CreateOrEditPaymentTypeModalComponent,
		CurrencyComponent,
		CreateOrEditCurrencyModalComponent,
		FreightCompanyComponent,
		CreateOrEditFreightCompanyModalComponent,
		StockFromComponent,
		CreateOrEditStockFromModalComponent,
		StockOrderForComponent,
		CreateOrEditStockOrderForModalComponent,
		PurchaseDocumentListComponent,
		CreateOrEditPurchaseDocumentListModalComponent,
		JobCostMonthWiseComponent,
		ViewDuplicateWholesaleLeadPopUpModalComponent,
		vendorComponent,
		CreateOrEditVendorModalComponent,
		ViewVendorModalComponent,
		ViewPurchaseCompanyModalComponent,
		EcommerceSubscriberComponent,
        SubscriberEmailModalComponent,
		ProductImageModalComponent,
		CreateOrEditECommerceContactUsModalComponent,
        ContactUSSMSEmailModalComponent,
        AddtoLaedContactUsModalComponent,
		CreateOrEditStockOrderModalComponent,
		StockOrderComponent,
		StockTransfersComponent,
		CreateOrEditStockTransferModalComponent,
		ViewStockOrderModalComponent,
		SMSModalComponent,
		EmailModalComponent,
		RemindersModalComponent,
		CommentsModalComponent,
		IncoTermComponent,
		CreateOrEditIncoTermModalComponent,
		CreateOrEditPurchaseOrderModalComponent,
		WholeSaleLeadHeaderResultsComponent,
		ViewResultWholeSaleLeadComponent,
		ViewDetailsWholeSaleModelComponent,
		JobVariationReportComponent,
		ViewEcommerceSliderModalComponent,
		ViewBrandingPartnerModalComponent,
		ViewEcommerceSolarPackageModalComponent,
		PurchaseOrderActivityLogComponent,
		ViewPurchaseOrderDetailComponent,
		PurchaseOrderActivityLogHistoryComponent,
		ViewPostCodeWiseCostModalComponent,
		MonthlyComparisonReportComponent,
		SeriesComponent,
		CreateOrEditSeriesModalComponent,
		RequestToWholeSaleLeadTransferComponent,
		WholeSaleDataVaultActivityLogComponent,
		EcommerceDataVaultActivityLogComponent,
		CreateEditTransportTypeModal,
		PropertyTypeComponent,
		CreateOrEditPropertyModalComponent,
		PVDStatusComponent,
		CreateOrEditPVDStatusModalComponent,
		WholesaleJobStatusComponent,
		CreateOrEditWholesaleJobStatusModalComponent,
		EssentialTrackerComponent,
		AddEssentialNotesModalComponent,
		WholesaleSmsReplyComponent,
		EssentialTrackerActiveModalComponent,
		EcommerceProductTypeComponent,
		CreateOrEditEcommerceProductTypeModalComponent,
		LeadSourceComparisonReportComponent,
		RescheduleInstallationReportComponent,
		RescheduleInstallationHistoryModalComponent,
		JobActualCostModalComponent,
		InstallerDeatilsComponent,
		EcommerceSpecificationComponent,
	    CreateOrEditSpecificationModalComponent,
		ProductSpecificationModalComponent,
		StockTranferDetailComponent,
		StockTranferActivityLogComponent,
		StockTransferActivityLogHistoryComponent,
		StockOrderToDoModalComponent,
		ViewPoModalComponent,
		VariationTypePriceReportModelComponent,
		CreateOrEditJobCostFixExpenseModalComponent,
		JobCostFixExpenseComponent,
		ViewJobCostFixExpenseModalComponent,
		CreateOrEditStockPaymentModalComponent,
		StockPaymentComponent,
		CIMETLeadTrackerComponent,
		SuggestedProductModalComponent,
		StockPaymentTrackerComponent,
		StockVariationsComponent,
		CreateOrEditStockVariationModalComponent,
		ViewStockVariationModalComponent,
		InstallerinvoiceJobwiseComponent,
		EcommerceStcRegisterComponent,
		EcommerceStcDealPriceComponent,
		CreateOrEditStcDealPriceModalComponent,
		InventoryDataVaultActivityLogComponent,
		StockOrderReceivedComponent,
		ReviewReportComponent,
		leadMonthlyComparison,
		EmployeeProfitReportComponent,
		CreateOrEditStateModalComponent,
		InstallerInvoiceDetailModalComponent,
		SerialNoStatusComponent,
		CreateOrEditSerialNoModalComponent,
		SerialNoHistoryReportComponent,
		ApplicationFeeTrackerComponent,
		ProgressReportComponent,
		VoucherComponent,
		CreateOrEditVoucherModalComponent
	],
	
	providers: [
		{ provide: BsDatepickerConfig, useFactory: NgxBootstrapDatePickerConfigService.getDatepickerConfig },
		{ provide: BsDaterangepickerConfig, useFactory: NgxBootstrapDatePickerConfigService.getDaterangepickerConfig },
		{ provide: BsLocaleService, useFactory: NgxBootstrapDatePickerConfigService.getDatepickerLocale },
		{ provide: DatePipe }
	]
})
export class MainModule { }
