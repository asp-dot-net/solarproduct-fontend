﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetServiceSourcesForViewDto, ServiceSourcesDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
@Component({
    selector: 'viewServiceSourcesModal',
    templateUrl: './view-serviceSources-modal.component.html'
})
export class ViewServiceSourcesModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetServiceSourcesForViewDto;


    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
        this.item = new GetServiceSourcesForViewDto();
        this.item.jobServiceSources = new ServiceSourcesDto();
    }

    show(item: GetServiceSourcesForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Opened Service Sources View : ' + this.item.jobServiceSources.jobServiceSources;
        log.section = 'Service Sources';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {            
         });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
