﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { CreateOrEditServiceSourcesDto, ServiceSourcesServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';


@Component({
    selector: 'createOrEditServiceSourcesModal',
    templateUrl: './create-or-edit-serviceSources-modal.component.html'
})
export class CreateOrEditServiceSourcesModalComponent extends AppComponentBase {
   
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    serviceSources: CreateOrEditServiceSourcesDto = new CreateOrEditServiceSourcesDto();



    constructor(
        injector: Injector,
        private _serviceSourcesServiceProxy: ServiceSourcesServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }
    
    show(serviceSourcesId?: number): void {
    

        if (!serviceSourcesId) {
            this.serviceSources = new CreateOrEditServiceSourcesDto();
            this.serviceSources.id = serviceSourcesId;

            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Service Sources';
            log.section = 'Service Sources';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._serviceSourcesServiceProxy.getServiceSourcesForEdit(serviceSourcesId).subscribe(result => {
                this.serviceSources = result.jobServiceSources;


                this.active = true;
                this.modal.show();
                
let log = new UserActivityLogDto();
log.actionId = 79;
log.actionNote ='Open For Edit Service Sources : ' + this.serviceSources.name;
log.section = 'Service Sources';
this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
    .subscribe(() => {
});
            });
        }
        
    }
    onShown(): void {
        
        document.getElementById('ServiceSources_ServiceSources').focus();
    }
    save(): void {
            this.saving = true;

			
			
            this._serviceSourcesServiceProxy.createOrEdit(this.serviceSources)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.serviceSources.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Service Sources Updated : '+ this.serviceSources.name;
                    log.section = 'Service Sources';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Service Sources Created : '+ this.serviceSources.name;
                    log.section = 'Service Sources';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
