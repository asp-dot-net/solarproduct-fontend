import { Component, ViewChild, Injector, Output, EventEmitter, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { ProductPackageServiceProxy,UserActivityLogServiceProxy, UserActivityLogDto, CreateOrEditProductPackageDto, CreateOrEditProductPackageItemDto, JobProductItemProductItemLookupTableDto, CommonLookupServiceProxy, JobsServiceProxy, CommonLookupDto, LeadStateLookupTableDto, OrganizationUnitDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';

@Component({
    selector: 'createOrEditProductPackageModal',
    templateUrl: './create-or-edit-productpackage-modal.component.html'
})
export class CreateOrEditProductPackageModalComponent extends AppComponentBase implements OnInit {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    productPackage: CreateOrEditProductPackageDto = new CreateOrEditProductPackageDto();
    allOrganizationUnits: OrganizationUnitDto[];
    allStates: LeadStateLookupTableDto[];

    productList: any[];

    JobProducts: any[];
    productItems: JobProductItemProductItemLookupTableDto[];
    productTypes: CommonLookupDto[];
    productItemSuggestions: any[];

    constructor(
        injector: Injector,
        private _productPackageServiceProxy: ProductPackageServiceProxy,
        private _commonLookupService : CommonLookupServiceProxy,
        private _jobServiceProxy: JobsServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void { 
        this._commonLookupService.getAllProductTypeForTableDropdown().subscribe(result => {
            this.productTypes = result;
        });
        // this._commonLookupService.getAllProductItemForTableDropdownForEdit().subscribe(result => {
        //     this.productItems = result;
        // });
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;           
        });

    }

    show(productPackageId?: number): void {
        if (!productPackageId) {
            this.productPackage = new CreateOrEditProductPackageDto();
            this.productPackage.id = productPackageId;

            this.JobProducts = [];
            var jobProductCreateOrEdit = { id: 0, productItemId: 0, productItemName: "", quantity: 0, model: "", productItems: [], productTypeId: 0 }
            jobProductCreateOrEdit.productItems = this.productItems;
            this.JobProducts.push(jobProductCreateOrEdit);

            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Product Packages';
            log.section = 'Product Packages';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
           
        } else {
            this._productPackageServiceProxy.getProductPackageForEdit(productPackageId).subscribe(result => {
                this.productPackage = result.productPackage;
                this.JobProducts = [];
                this.productPackage.productPackageItem.map((item) => {
                    var productPackageCreateOrEdit = { id: 0, productItemId: 0, productItemName: "", quantity: 0, model: "", productItems: [], productTypeId: 0 }
                    productPackageCreateOrEdit.productItemId = item.productItemId;
                    productPackageCreateOrEdit.productTypeId = item.productTypeId;
                    productPackageCreateOrEdit.productItemName = item.productItem;

                    productPackageCreateOrEdit.quantity = item.quantity;
                    productPackageCreateOrEdit.id = item.id;
                    productPackageCreateOrEdit.model = item.model;
                    this.JobProducts.push(productPackageCreateOrEdit);
                    
                    // let pitemname = this.productItems.find(x => x.id == item.productItemId);
                    // productPackageCreateOrEdit.productItemName = pitemname.displayName;
                    // this.JobProducts.push(productPackageCreateOrEdit);
                })

                if (this.productPackage.productPackageItem.length == 0) {
                    var productPackageCreateOrEdit = { id: 0, productItemId: 0, productItemName: "", quantity: 0, model: "", productItems: [], productTypeId: 0 }
                    productPackageCreateOrEdit.productItems = this.productItems;
                    this.JobProducts.push(productPackageCreateOrEdit);
                }

                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Product Packages : ' + this.productPackage.name;
                log.section = 'Product Packages';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
    }

    onShown(): void {
       document.getElementById('Name').focus();
    }
   
    save(): void {
        this.saving = true;

        this.productPackage.productPackageItem = [];
        this.JobProducts.map((item) => {
            let createOrEditProductPackageItemDto = new CreateOrEditProductPackageItemDto();
            createOrEditProductPackageItemDto.productItemId = item.productItemId;
            createOrEditProductPackageItemDto.quantity = item.quantity;
            createOrEditProductPackageItemDto.id = item.id;
            if (item.productTypeId != undefined && item.productItemId != undefined && item.quantity != undefined && item.model != undefined) {
                this.productPackage.productPackageItem.push(createOrEditProductPackageItemDto);
            }
        });

        this._productPackageServiceProxy.createOrEdit(this.productPackage).pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.productPackage.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Product Packages Updated : '+ this.productPackage.name;
                    log.section = 'Product Packages';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Product Packages Created : '+ this.productPackage.name;
                    log.section = 'Product Packages';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
            },
            error => {
                this.saving = false;
            }
        );
        
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
    // changeProductType(i) : void {
    //     this.installationItemList[i].productItemId = "";
    //     this.installationItemList[i].productItemName = "";
    //     this.installationItemList[i].size = 0;
    //     this.installationItemList[i].pricePerWatt = 0;
    //     this.installationItemList[i].unitPrice = 0;
    // }
    filterProductItem(item, i): void {
        //this.JobProducts[i].productItems = this.productItems.filter(x => x.productTypeId == item.productTypeId);
        this.JobProducts[i].productItemId = "";
        this.JobProducts[i].productItemName = "";
        this.JobProducts[i].modal = "";
        this.JobProducts[i].size = 0;
        this.JobProducts[i].pricePerWatt = 0;
        this.JobProducts[i].unitPrice = 0;
    }

    filterProductIteams(event, i): void {
        // let Id = 0;
        // for (let j = 0; j < this.JobProducts.length; j++) {
        //     if (j == i) {
        //         Id = this.JobProducts[i].productTypeId
        //         this.JobProducts[i].model = '';
        //     }
        // }

        // this._jobServiceProxy.getProductItemList(Id, event.query, 'QLD', true).subscribe(output => {
        //     this.productItemSuggestions = output;
        // });
        let Id = this.JobProducts[i].productTypeId;

        this._jobServiceProxy.getPicklistProductItemList(Id, event.query).subscribe(output => {
            //this.productItemSuggestions = output;
            debugger;
            var items =this.JobProducts.map(i=>i.productItemName+'');
            this.productItemSuggestions = output.filter(x=> !items.includes(x.productItem))
        });
    }

    getProductTypeId(i) {
        let pItem = this.productItems.find(x => x.displayName == this.JobProducts[i].productItemName)
        this.JobProducts[i].productItemId = pItem.id;
        this.JobProducts[i].model = pItem.model;
    }

    check(event) {
        if (event.target.value < 1) {
            event.target.value = '';
        }
    }

    removeJobProduct(JobProduct): void {
        if (this.JobProducts.length == 1)
            return;
        
        if (this.JobProducts.indexOf(JobProduct) === -1) {
            
        } else {
            this.JobProducts.splice(this.JobProducts.indexOf(JobProduct), 1);
        }
    }

    addJobProduct(): void {
        var jobProductCreateOrEdit = { id: 0, productItemId: 0, productItemName: "", quantity: 0, model: "", productItems: [], productTypeId: 0 }
        this.JobProducts.push(jobProductCreateOrEdit);
    }

    selectProductItem(event, i) {
        debugger;
        this.JobProducts[i].productItemId = event.id;
        this.JobProducts[i].productItemName = event.productItem;
        this.JobProducts[i].size = event.size;
        this.JobProducts[i].model = event.productModel;
        //this.calculateUnitPrice(i);
    }
}
