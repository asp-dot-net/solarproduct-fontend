﻿import { AppConsts } from "@shared/AppConsts";
import { Component, ViewChild, Injector, Output, EventEmitter, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { CommonLookupServiceProxy, CreateOrEditJobDto, CreateOrEditLeadDto, GetSmsTemplateForViewDto, JobsServiceProxy, LeadsServiceProxy, SmsTemplateDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import PlaceResult = google.maps.places.PlaceResult;
import { Location } from '@angular-material-extensions/google-maps-autocomplete';
import { finalize } from "rxjs/operators";

@Component({
    selector: 'editaddressModal',
    templateUrl: './edit-address.component.html'
})
export class EditAddressComponent extends AppComponentBase implements OnInit {

    @ViewChild('editaddressModal', {static: true}) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    item : any;
    isGoogle = "Google";

    Address : string = '';

    selectAddress: boolean = true;
    lead: CreateOrEditLeadDto;
    job: CreateOrEditJobDto = new CreateOrEditJobDto();
    allStates: any [];
    unitTypesSuggestions: string[];
    streetNamesSuggestions: string[];
    streetTypesSuggestions: string[];
    suburbSuggestions: string[];

    constructor(
        injector: Injector,
        private _jobServiceProxy : JobsServiceProxy,
        private _commonLookupService : CommonLookupServiceProxy,
        private _leadsServiceProxy : LeadsServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
    }

    show(record: any): void {
        this.showMainSpinner();
        this.active = true;
        this.item = record;
        this.isGoogle = "Google";
        this._jobServiceProxy.getJobForEdit(record.id).subscribe(result => {
            this.job = result.job;
            this.Address = result.job.address + ', ' + result.job.suburb + ', ' + result.job.state + '-' + result.job.postalCode;
            // this.job.address = '';
            this.hideMainSpinner();
        }, e => {
            this.hideMainSpinner();
        })
        
        this.modal.show();
    }

    onShown(): void {
        // document.getElementById('Lead_Address').focus();
    }

    save(): void {
        this.saving = true;
        this._jobServiceProxy.updateAddress(this.job)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            }, e => {
            this.saving = false;
        });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    onAutocompleteSelected(result: PlaceResult) {
        if (result.address_components.length == 7) {
            this.job.unitNo = "";
            this.job.unitType = "";
            this.job.streetNo = "";
            this.job.streetName = "";
            this.job.streetType = "";
            this.job.suburb = "";
            this.job.state = "";
            this.job.suburbId = 0;
            this.job.stateId = 0;
            this.job.postalCode = "";
            this.job.address = "";
            this.job.country = "";

            this.job.streetNo = result.address_components[0].long_name.toUpperCase();
            var str = result.address_components[1].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.job.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.job.streetType = splitted[3].toUpperCase().trim();
            }
            else if (splitted.length == 3) {
                this.job.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.job.streetType = splitted[2].toUpperCase().trim();
            }
            else {
                this.job.streetName = splitted[0].toUpperCase().trim();
                this.job.streetType = splitted[1].toUpperCase().trim();
            }
            this.job.suburb = result.address_components[2].long_name.toUpperCase().trim();
            this.job.state = result.address_components[4].short_name;
            this._leadsServiceProxy.getSuburbId(result.address_components[6].long_name).subscribe(result => {
                this.job.suburbId = result;
            });
            this._leadsServiceProxy.stateId(result.address_components[4].short_name).subscribe(result => {
                this.job.stateId = result;
            });
            this.job.postalCode = result.address_components[6].long_name.toUpperCase();
            this.job.address = this.job.unitNo + " " + this.job.unitType + " " + this.job.streetNo + " " + this.job.streetName + " " + this.job.streetType;  //result.formatted_address;
            this.job.country = result.address_components[5].long_name.toUpperCase();
        }
        else if (result.address_components.length == 6) {
            this.job.unitNo = "";
            this.job.unitType = "";
            this.job.streetNo = "";
            this.job.streetName = "";
            this.job.streetType = "";
            this.job.suburb = "";
            this.job.state = "";
            this.job.suburbId = 0;
            this.job.stateId = 0;
            this.job.postalCode = "";
            this.job.address = "";
            this.job.country = "";

            this.job.streetNo = result.address_components[0].long_name.toUpperCase();
            var str = result.address_components[1].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.job.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.job.streetType = splitted[3].toUpperCase().trim();
            }
            else if (splitted.length == 3) {
                this.job.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.job.streetType = splitted[2].toUpperCase().trim();
            }
            else {
                this.job.streetName = splitted[0].toUpperCase().trim();
                this.job.streetType = splitted[1].toUpperCase().trim();
            }
            this.job.suburb = result.address_components[2].long_name.toUpperCase().trim();
            this.job.state = result.address_components[3].short_name;
            this._leadsServiceProxy.getSuburbId(result.address_components[5].long_name).subscribe(result => {
                this.job.suburbId = result;
            });
            this._leadsServiceProxy.stateId(result.address_components[3].short_name).subscribe(result => {
                this.job.stateId = result;
            });
            this.job.postalCode = result.address_components[5].long_name.toUpperCase();
            this.job.address = this.job.unitNo + " " + this.job.unitType + " " + this.job.streetNo + " " + this.job.streetName + " " + this.job.streetType;  //result.formatted_address;
            this.job.country = result.address_components[4].long_name.toUpperCase();
        }
        else {
            this.job.unitNo = "";
            this.job.unitType = "";
            this.job.streetNo = "";
            this.job.streetName = "";
            this.job.streetType = "";
            this.job.suburb = "";
            this.job.state = "";
            this.job.suburbId = 0;
            this.job.stateId = 0;
            this.job.postalCode = "";
            this.job.address = "";
            this.job.country = "";

            var str1 = result.address_components[0].long_name.toUpperCase();
            var splitted1 = str1.split(" ");
            if (splitted1.length == 1) {
                this.job.unitNo = splitted1[0].toUpperCase().trim();
            }
            else {
                this.job.unitNo = splitted1[1].toUpperCase().trim();
                this.job.unitType = splitted1[0].toUpperCase().trim();
                this.job.unitType = splitted1[0].toUpperCase().trim();
            }
            this.job.streetNo = result.address_components[1].long_name.toUpperCase();
            var str = result.address_components[2].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.job.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.job.streetType = splitted[3].toUpperCase().trim();
            }
            else if (splitted.length == 3) {
                this.job.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.job.streetType = splitted[2].toUpperCase().trim();
            }
            else {
                this.job.streetName = splitted[0].toUpperCase().trim();
                this.job.streetType = splitted[1].toUpperCase().trim();
            }
            this.job.suburb = result.address_components[3].long_name.toUpperCase().trim();
            this.job.state = result.address_components[5].short_name;
            this._leadsServiceProxy.getSuburbId(result.address_components[7].long_name).subscribe(result => {
                this.job.suburbId = result;
            });
            this._leadsServiceProxy.stateId(result.address_components[5].short_name).subscribe(result => {
                this.job.stateId = result;
            });
            this.job.postalCode = result.address_components[7].long_name.toUpperCase();
            this.job.address = this.job.unitNo + " " + this.job.unitType + " " + this.job.streetNo + " " + this.job.streetName + " " + this.job.streetType;
            this.job.country = result.address_components[6].long_name.toUpperCase();
        }
    }

    onLocationSelected(location: Location) {
        this.job.latitude = location.latitude.toString();
        this.job.longitude = location.longitude.toString();
    }

    filterunitTypes(event): void {
        this._leadsServiceProxy.getAllUnitType(event.query).subscribe(output => {
            this.unitTypesSuggestions = output;
        });
    }

    filterstreetnames(event): void {
        this._leadsServiceProxy.getAllStreetName(event.query).subscribe(output => {
            this.streetNamesSuggestions = output;
        });
    }

    filterstreettypes(event): void {
        this._leadsServiceProxy.getAllStreetType(event.query).subscribe(output => {
            this.streetTypesSuggestions = output;
        });
    }

    // get Suburb
    filtersuburbs(event): void {
        this._leadsServiceProxy.getAllSuburb(event.query).subscribe(output => {
            this.suburbSuggestions = output;
        });
    }

    fillstatepostcode(): void {
        var splitted = this.job.suburb.split("||");
        this.job.suburb = splitted[0];
        this.job.state = splitted[1].trim();
        this.job.postalCode = splitted[2].trim();
        this.job.country = "AUSTRALIA";
    }
}
