import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntallerMapComponent } from './intaller-map.component';

describe('IntallerMapComponent', () => {
  let component: IntallerMapComponent;
  let fixture: ComponentFixture<IntallerMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntallerMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntallerMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
