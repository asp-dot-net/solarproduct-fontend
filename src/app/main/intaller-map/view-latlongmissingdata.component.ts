﻿import { AppConsts } from "@shared/AppConsts";
import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetSmsTemplateForViewDto, InstallationServiceProxy, SmsTemplateDto, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { EditAddressComponent } from "./edit-address.component";
import { LinkAccountModalComponent } from "@app/shared/layout/link-account-modal.component";
import { LazyLoadEvent, Paginator } from "primeng";
import { Table } from 'primeng/table';
import { finalize } from "rxjs/operators";

@Component({
    selector: 'viewLatLongMissingModal',
    templateUrl: './view-latlongmissingdata.component.html'
})
export class ViewLalLongMissingJobComponent extends AppComponentBase {

    @ViewChild('viewLatLongMissingModal', {static: true}) modal: ModalDirective;
    @ViewChild('editaddressModal', {static: true}) editaddressModal: EditAddressComponent;
    @ViewChild('dataTable', {static: true}) dataTable: Table;
    @ViewChild('paginator', {static: true}) paginator: Paginator;

    @Output() modalClose: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item = [];

    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _insallationServiceProxy: InstallationServiceProxy,
    ) {
        super(injector);
    }

    showDetail(item, section =''): void {
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Open Job Detail';
        log.section = section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
        this.item = item;
        // console.log(item);
        this.getMissingList();
        this.modal.show();
    }

    getMissingList(event?: LazyLoadEvent) {
        this.primengTableHelper.showLoadingIndicator();
        
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this._insallationServiceProxy.getMissingJobs(
            this.item,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            // this.firstrowcount =  totalrows + 1;
            // this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
        }, e => {
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    close(): void {
        this.modal.hide();
        this.modalClose.emit(null);
    }

    editAddress (record: any): void {
        this.editaddressModal.show(record);
    }
}
