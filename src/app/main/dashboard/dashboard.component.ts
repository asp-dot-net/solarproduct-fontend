import { Component, Injector, ViewEncapsulation } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DashboardCustomizationConst } from '@app/shared/common/customizable-dashboard/DashboardCustomizationConsts';
import { Title } from '@angular/platform-browser';
import { DatePipe } from '@angular/common';
import { UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.less'],
    encapsulation: ViewEncapsulation.None 
})

export class DashboardComponent extends AppComponentBase {
    dashboardName = DashboardCustomizationConst.dashboardNames.defaultTenantDashboard;

    pipe = new DatePipe('en-US');
    today = this.pipe.transform(new Date(), 'MMM dd');

    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private titleService: Title) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Dashboard");
        
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open';
        log.section = 'Dashboard';
        _userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {
        });
        
    }
    showchild: boolean = true;
    shouldShow: boolean = false;
    addTaskShow: boolean = false;
}
