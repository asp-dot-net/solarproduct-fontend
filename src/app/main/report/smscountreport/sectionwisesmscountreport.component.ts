import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { UserCallHistoryServiceProxy, GetAllLocalityCountOfState, SmsCountReportServiceProxy, GetAllSmsCountSectionWiseDto, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { debug } from 'console';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { result } from 'lodash';

@Component({
    selector: 'SectionWiseSmsCountModel',
    templateUrl: './sectionwisesmscountreport.component.html'
})
export class SectionWiseSmsCountModelComponent extends AppComponentBase {

    @ViewChild('SectionWiseSmsCountModel', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
   ///  @ViewChild('TeamName') searchElement;
  //  @ViewChild('TeamName', { read: ElementRef }) searchElement:ElementRef;
    active = false;
    saving = false;
    SectionWiseData : GetAllSmsCountSectionWiseDto[] =[];
    user : string;
    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _smsCountReportServiceProxy : SmsCountReportServiceProxy,
        // private _fileDownloadService: FileDownloadService,

    ) {
        super(injector);
    }

    show(oid : number, userId : number, userName : string, datetype : string, startDate : moment.Moment, endDate : moment.Moment, section =''): void {
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='View Section Wise SMS Count';
        log.section = section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
        this.user = userName;
        // this._userCallHistoryServiceProxy.getAllLocalityCountOfStateHistory(oId,0,'',state,mobile,startDate,endDate,'',0,10).subscribe(result => {
        //     this.localityData = result;
        //     this.modal.show();
        // });
        this._smsCountReportServiceProxy.getAllSectionWise(oid,userId,datetype,startDate,endDate,'').subscribe(result => {
            this.SectionWiseData = result;
            this.modal.show();

        })
    }

    // exportToExcel(): void {
        
    //     this._userCallHistoryServiceProxy.getAllLocalityCountOfStateHistoryExport(
    //         this.oId,0,'',this.state,this.mobile,this.startDate,this.endDate,'',0,10
    //     )
    //     .subscribe(result => {
    //         this._fileDownloadService.downloadTempFile(result);
    //     });
    // }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
