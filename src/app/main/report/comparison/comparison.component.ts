import { Component, Injector, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLookupServiceProxy, ComparisonServiceProxy, GetAllComparisonDto, LeadSourceLookupTableDto, LeadStateLookupTableDto, OrganizationUnitDto, UserActivityLogDto, UserActivityLogServiceProxy} from '@shared/service-proxies/service-proxies';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { result } from 'lodash';
import * as moment from 'moment';
import { LazyLoadEvent, Paginator, Table } from 'primeng';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './comparison.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ComparisonReportComponent extends AppComponentBase {

  show: boolean = true;
  showchild: boolean = true;
  toggleBlock() {
    this.show = !this.show;
  };
  toggleBlockChild() {
    this.showchild = !this.showchild;
  };


  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;

  filterText = '';
  date = new Date();
  maxEnddate = new Date();
  allOrganizationUnits: OrganizationUnitDto[];
  organizationid = 0;
  startDate: moment.Moment = moment(this.date);
  endDate: moment.Moment = moment(this.date);
  allLeadSources: LeadSourceLookupTableDto[];
  allStates: LeadStateLookupTableDto[];
  leadSourceIdFilter = [];
  stateFilter = [];
  areaFilter = [];
  shouldShow: boolean = false;
  data : GetAllComparisonDto = new GetAllComparisonDto();
  allAreas = [];
  allhederFeild = [];
  allheder = [];
  lead = 0;
  sold = 0
  installed = 0;
  expence = 0;
  colspan = 4;

  constructor(
    injector: Injector,
    private _comparisonServiceProxy : ComparisonServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
    private titleService: Title
  ) {
    super(injector);
    this.titleService.setTitle(this.appSession.tenancyName + " |  Comparison Report");
    // this._commonLookupService.getTeamForFilter().subscribe(teams => {
    //   this.filteredTeams = teams;
    // // });

    // this._leadsServiceProxy.getUserByTeamId(this.teamId).subscribe(userList => {
    //   this.userlist = userList;
    // });


  }
  ngOnInit(): void {
    var CreateOrEditHeader1 = new LeadStateLookupTableDto;
    CreateOrEditHeader1.id = 1;
    CreateOrEditHeader1.displayName = "Lead";
    this.allhederFeild.push(CreateOrEditHeader1);
    var CreateOrEditHeader2 = new LeadStateLookupTableDto;
    CreateOrEditHeader2.id = 2;
    CreateOrEditHeader2.displayName = "KW Sold / Battery KW ";
    this.allhederFeild.push(CreateOrEditHeader2);
    var CreateOrEditHeader3 = new LeadStateLookupTableDto;
    CreateOrEditHeader3.id = 3;
    CreateOrEditHeader3.displayName = "KW Installed";
    this.allhederFeild.push(CreateOrEditHeader3);
    var CreateOrEditHeader4 = new LeadStateLookupTableDto;
    CreateOrEditHeader4.id = 4;
    CreateOrEditHeader4.displayName = "Expense";
    this.allhederFeild.push(CreateOrEditHeader4);
    
    this._commonLookupService.getOrganizationUnit().subscribe(output => {
      let log = new UserActivityLogDto();
      log.actionId = 79;
      log.actionNote ='Open Comparison Report';
      log.section = 'Comparison Report';
      this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
          .subscribe(() => {
      });
      this.allOrganizationUnits = output;
      this.organizationid = this.allOrganizationUnits[0].id;

      this._commonLookupService.getAllLeadSourceDropdown().subscribe(result => {
        this.allLeadSources = result;
      });

      this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
        this.allStates = result;
      });

        this.allAreas = [];
        this.allAreas.push("Metro");
        this.allAreas.push("Regional");
        this.allAreas.push("NoArea");
      
      this.getAcivityList();
    });
  }
  getAcivityList(event?: LazyLoadEvent) {
   
    this.showMainSpinner();
    if (this.allheder.length > 0) {
      this.colspan = this.allheder.length;
      this.lead = 1;
      this.sold = 1;
      this.installed = 1;
      this.expence = 1;
    }
    else {
      this.colspan = 4;
      this.lead = 0;
      this.sold = 0;
      this.installed = 0;
      this.expence = 0;
    }

    if(this.startDate && this.endDate){
           this._comparisonServiceProxy.getAll(this.startDate,this.endDate,this.organizationid,this.leadSourceIdFilter,this.stateFilter,this.areaFilter
          
        ).subscribe(result => {
            this.data = result;
            this.hideMainSpinner();
    
        })
    }
    else{
        this.hideMainSpinner();

    }
    this.allheder.forEach(item => {
      if (item == 1) {
        this.lead = 0;

      }
      if (item == 2) {

        this.sold = 0;

      } if (item == 3) {

        this.installed = 0;

      } if (item == 4) {

        this.expence = 0;

      } 
      // this.primengTableHelper.hideLoadingIndicator();
    })
    
   

  }

  reloadPage(): void {
    this.paginator.changePage(this.paginator.getPage());
  }

  exportToExcel(excelorcsv): void {
    // let id = document.getElementById("tblrpt").innerHTML;
    // this.excelorcsvfile = excelorcsv
    //  this._leadExpensesServiceProxy.getAllexpenseTableForReportExport(this.startDate, this.endDate, this.states, this.leadSources, this.organizationid
    // )
    //   .subscribe(result => {
    //     this._fileDownloadService.downloadTempFile(result);
    //   });
  }

  addSearchLog(filter): void {
    let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Searched by '+ filter ;
        log.section = 'Comparison Report';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
}

RereshLog():void{
    let log = new UserActivityLogDto();
    log.actionId = 80;
    log.actionNote ='Refresh the data' ;
    log.section = 'Comparison Report';
    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {
    }); 
}

}