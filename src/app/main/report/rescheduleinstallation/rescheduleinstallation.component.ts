import { Component, Injector, ViewEncapsulation, ViewChild, ElementRef, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

import { CommonLookupServiceProxy, LeadStateLookupTableDto, OrganizationUnitDto, RescheduleInstallationsServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { RescheduleInstallationHistoryModalComponent } from './rescheduleinstallationhistory-modal.component';
import { finalize } from 'rxjs/operators';


@Component({
    templateUrl: './rescheduleinstallation.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class RescheduleInstallationReportComponent extends AppComponentBase implements OnInit {

    FiltersData = false;
    public screenWidth: any;  
    public screenHeight: any;  
    // testHeight = 330;
    testHeight = 330;
    firstrowcount = 0;
    last = 0;
    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    filterText = '';
    stateNameFilter = '';
    userFilter = 0;
    date = new Date();
    // firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    // startDate = moment().startOf('day');
    // endDate = moment().endOf('day');
    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);
    // allStates: LeadStateLookupTableDto[];
    // userList : CommonLookupDto[];
    organizationUnit = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnitlength: number = 0;
    totalOutbound = 0;
    totalInbound = 0;
    totalNotAnswered = 0;
    totalMissed = 0;
    uploadUrlstc: string;
    totalCall = 0;
    variationIds = [];
    AllVariation = [];
    Filter = 'JobNumber';
    dateFilter = 'CreationDate';
    allStates: LeadStateLookupTableDto[];
    AreaName: string;
    StateFilter : string;
    @ViewChild('RescheduleInstallationHistoryModal', { static: true }) RescheduleInstallationHistoryModal: RescheduleInstallationHistoryModalComponent;

    toggleBlock(): void {
        this.show = !this.show;
    };
      
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };
      toggle: boolean = true;
    
    change() {
        this.toggle = !this.toggle;
    }
    orgCode = "";
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
        
    summaryList =[];

    constructor(
        injector: Injector,
        private _RescheduleInstallationsServiceProxy: RescheduleInstallationsServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private titleService: Title,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _commonLookupService : CommonLookupServiceProxy,
    
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Reschedule Installation Report");
    }

    getAllJobVariation(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();
        var fText ="";
        if(this.filterText != undefined && this.filterText != null && this.filterText.trim() != ''){
            fText = this.orgCode + this.filterText.trim();
        }
        this._RescheduleInstallationsServiceProxy.getAll(   
            this.Filter,
            fText,
            this.dateFilter,
            this.startDate,
            this.endDate,
            this.organizationUnit,
            this.AreaName,
            this.StateFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
            ).subscribe(result => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
                this.firstrowcount =  totalrows + 1;
                this.last = totalrows + result.items.length;
                this.primengTableHelper.hideLoadingIndicator();
                this.shouldShow = false;
                if (result.totalCount > 0) {
                    this.summaryList = result.items[0].summary;
                }
                else{
                    
                    this.summaryList = [];
                }
            })
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
        this._commonLookupService.getAllVariationForDropdown().subscribe(output => {
            this.AllVariation = output;
        });
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Reschedule Installation Report';
            log.section = 'Reschedule Installation Report';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.orgCode = this.allOrganizationUnits[0].code;

            
            this.getAllJobVariation();

        });

    }
    
   
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  

    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }
        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }

    exportToExcel(): void {
        var fText ="";
        if(this.filterText != undefined && this.filterText != null && this.filterText.trim() != ''){
            fText = this.orgCode + this.filterText.trim();
        }
        // this._jobVariationReportServiceProxy.getAllForExport(            
        //     fText,
        //     this.startDate,
        //     this.endDate,
        //     this.variationIds,
        //     this.organizationUnit,
        // )
        // .subscribe(result => {
        //     this._fileDownloadService.downloadTempFile(result);
        // });
    }

    onClear() : void{
        this.startDate = moment(this.date);
        this.endDate = moment(this.date);
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Reschedule Installation Report';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Reschedule Installation Report';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }
}