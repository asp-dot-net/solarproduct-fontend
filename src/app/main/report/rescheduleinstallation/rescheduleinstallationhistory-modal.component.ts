import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetLeadSourceForViewDto, LeadSourceDto, UserCallHistoryServiceProxy, GetAllCallFlowQueueCallHistoryDetailsDto, RescheduleInstallationsServiceProxy, GetRescheduleInstallationHistoryDto, UserActivityLogServiceProxy, UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { finalize } from 'rxjs/operators';


@Component({
    selector: 'RescheduleInstallationHistoryModal',
    templateUrl: './rescheduleinstallationhistory-modal.component.html'
})
export class RescheduleInstallationHistoryModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    queueExtentionNumber = '';
    extentionNumber = '';
    item: GetRescheduleInstallationHistoryDto[];
   jobnumber = ''

    constructor(
        injector: Injector,
        private _RescheduleInstallationsServiceProxy: RescheduleInstallationsServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
        ,private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    show(jobId :number, jobnumber : string,section =''): void {
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='View Reschedule Detail';
        log.section = section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
        this.jobnumber = jobnumber;
        this._RescheduleInstallationsServiceProxy.getRescheduleInstallationHistory(jobId).subscribe(result => {
            this.item = result;         
            this.modal.show();
        });
        this.active = true;
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    

}
