import { Component, Injector, OnInit, ViewChild, HostListener } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AddActivityModalComponent } from '@app/main/myleads/myleads/add-activity-model.component';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLookupDto, CommonLookupServiceProxy, GetActivityForViewDto, GetActivitytaskcountDto, JobInstallerInvoicesServiceProxy, JobsServiceProxy, LeadActivityLogDto, LeadsServiceProxy, LeadUsersLookupTableDto, OrganizationUnitDto, SystemReportsServiceProxy, UserServiceProxy } from '@shared/service-proxies/service-proxies';
import * as moment from 'moment';
import { LazyLoadEvent, Paginator } from 'primeng';
import { Table } from 'primeng/table';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-todo-activity-list',
  templateUrl: './todo-activity-list.component.html',
  styleUrls: ['./todo-activity-list.component.css']
})
export class TodoActivityListComponent extends AppComponentBase {

  show: boolean = true;
  showchild: boolean = true;
  shouldShow: boolean = false;
  addTaskShow: boolean = false;
  saving = false;
  toggleBlock() {
    this.show = !this.show;
  };
  toggleBlockChild() {
    this.showchild = !this.showchild;
  };

  toggle: boolean = true;

  change() {
      this.toggle = !this.toggle;
  }

  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;
  @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
  filterText = '';
  date = new Date();
  sampleDateRange: moment.Moment[] = [moment().add(0, 'days').endOf('day'), moment().add(0, 'days').endOf('day')];
  CopanyNameFilter = '';
  teamId = 0;
  userId = 0;
  dateNameFilter = '';
  startDate: moment.Moment;
  endDate: moment.Moment;
  filteredLeadSource: { jobNumber: string; fullDetails: string }[] = [];
  Deatils: string; 
  selectedAll: any;
  tableRecords: any;
  filteredTeams: LeadUsersLookupTableDto[];
  userlist: LeadUsersLookupTableDto[];
  allOrganizationUnits: OrganizationUnitDto[];
  activitylist:GetActivityForViewDto[];
  taskcount:GetActivitytaskcountDto = new GetActivitytaskcountDto();
  allUsers: CommonLookupDto[];
  activityeditlog: LeadActivityLogDto = new LeadActivityLogDto();
  createactivitylog: LeadActivityLogDto = new LeadActivityLogDto();
  organizationid = 0;
  jobIds: string;
  taskid:number;
  firstrowcount = 0;
  last = 0;
  public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 245;

  constructor(
    injector: Injector,
    private _systemReportsServiceProxy: SystemReportsServiceProxy,
    private _jobInstallerInvoiceServiceProxy: JobInstallerInvoicesServiceProxy,
    private _commonLookupService: CommonLookupServiceProxy,
    private _leadsServiceProxy: LeadsServiceProxy,
    private _jobsServiceProxy: JobsServiceProxy,
    private _userServiceProxy: UserServiceProxy,
    private titleService: Title
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  To do Activity Detail");
    // this._userServiceProxy.getOrganizationUnit().subscribe(output => {
    //   this.allOrganizationUnits = output;
    //   this.organizationid = this.allOrganizationUnits[0].id;
    // });
}
ngOnInit(): void {
  debugger;
  this.screenHeight = window.innerHeight;
  this._commonLookupService.getOrganizationUnit().subscribe(output => {
    this.allOrganizationUnits = output;
    this.organizationid = this.allOrganizationUnits[0].id;
    this.getTodoActivityList();
  });
  this._jobsServiceProxy.getUsers().subscribe(result => {
    this.allUsers = result;
});
}
@HostListener('window:resize', ['$event'])  
onResize(event) { 
    this.screenHeight = window.innerHeight;        
}  

getTodoActivityList(event?: LazyLoadEvent) {
  this.taskid=0;
    this.startDate = this.sampleDateRange[0];
    this.endDate = this.sampleDateRange[1];
    // if (this.primengTableHelper.shouldResetPaging(event)) {
    //   this.paginator.changePage(0);
    //   return;
    // }
   
   // this.primengTableHelper.showLoadingIndicator();

    this._systemReportsServiceProxy.getAllTodoActivityList(
      this.filterText,
      this.organizationid,
      undefined,
      undefined,
      undefined,
      this.dateNameFilter,
      this.startDate,
      this.endDate,this.taskid
      ,
      this.primengTableHelper.getSorting(this.dataTable),
      this.primengTableHelper.getSkipCount(this.paginator, event),
      this.primengTableHelper.getMaxResultCount(this.paginator, event)
    ).subscribe(result => {

      this.activitylist=result.items;
     // this. getTodoActivityListCount() ;
     this.primengTableHelper.totalRecordsCount = result.totalCount;
      this.primengTableHelper.records = result.items;
      this.tableRecords = result.items;
      const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
      this.firstrowcount = totalrows + 1;
      this.last = totalrows + result.items.length;
      this.primengTableHelper.hideLoadingIndicator(); 
    });
  }
 filtertodoActivity(id:number): void {
    
    this._systemReportsServiceProxy.getactivitydetailfromId(id).subscribe(result => {
      this.activityeditlog = result;
    });
  }
  reloadPage(): void {
    this.paginator.changePage(this.paginator.getPage());
  }
  closeDiv(): void {
    this.shouldShow = false;
    this.jobIds="";
  }
  closeTaskDiv(): void {
    this.addTaskShow = false;
    this.jobIds="";
  }
  Updatediv():void {
    this.saving = true;
    this._systemReportsServiceProxy.updateActivityLog(this.activityeditlog)
    .pipe(finalize(() => { this.saving = false; }))
    .subscribe(() => {
        
        this.notify.info(this.l('DataUpdatedSuccessfully'));
        this.getTodoActivityList();
    });
  }
  filterjobnumber(event): void {
    this._jobInstallerInvoiceServiceProxy.getAlljobNumberforAddToDo(event.query).subscribe(result => {
     
      this.filteredLeadSource = result.map(item => {
        const parts = item.split('|'); // Split the string by '|'
        return {
          jobNumber: parts[0]?.trim() || '', // Extract the first part (job number)
          fullDetails: item.trim(), // Keep the full details for display
        };
      });
    });
  }
  
checkAll(ev) {
  debugger;
  this.tableRecords.forEach(x => x.isSelected = ev.target.checked);
}

isAllChecked() {
  if (this.tableRecords)
      return this.tableRecords.every(_ => _.isSelected)
    ;
}
 
  savetask():void
  {debugger
    this.saving = true;  
    // this.createactivitylog.jobdetails = this.jobIds.fullDetails;
    const selectedJob = this.filteredLeadSource.find(
      item => item.jobNumber === item.jobNumber
    );
  
    
    this.createactivitylog.jobdetails = selectedJob ? selectedJob.fullDetails : '';
    this._systemReportsServiceProxy.saveActivityLogTask(this.createactivitylog)
    .pipe(finalize(() => { this.saving = false; }))
    .subscribe(() => {
        
        this.notify.info(this.l('SavedSuccessfully'));
        this.getTodoActivityList();
        this.addTaskShow = false;
        this.saving = false;
    });
  }
  markascomplete(id:number):void {
    this.saving = true;
    this._systemReportsServiceProxy.markascompleteActivityLog(id )
    .pipe(finalize(() => { this.saving = false; }))
    .subscribe(() => {
        
        this.notify.info(this.l('DataUpdatedSuccessfully'));
        this.getTodoActivityList();
        this.shouldShow = false;
    });
  }
  // exportToExcel(): void {

  // }

  getTodoActivityListfilter(event?: LazyLoadEvent) : void {
  
    this._systemReportsServiceProxy.getAllTodoActivityListFilterwise(
      this.filterText,
      this.organizationid,
      undefined,
      undefined,
      undefined,
      this.dateNameFilter,
      this.startDate,
      this.endDate,
      this.taskid
      ,
      this.primengTableHelper.getSorting(this.dataTable),
      this.primengTableHelper.getSkipCount(this.paginator, event),
      this.primengTableHelper.getMaxResultCount(this.paginator, event)
    ).subscribe(result => {

      this.primengTableHelper.totalRecordsCount = result.totalCount;
      this.primengTableHelper.records = result.items;
      this.tableRecords = result.items;
      this.primengTableHelper.hideLoadingIndicator(); 
      //this. getTodoActivityListCount() ;
    });
  }



  deleteTask(id:number):void {
    this.saving = true;
    this._systemReportsServiceProxy.deleteActivityLog(id )
    .pipe(finalize(() => { this.saving = false; }))
    .subscribe(() => {
        
        this.notify.info(this.l('DeletedSuccessfully'));
        this.getTodoActivityList();
    });
  }
  deleteAllTask(): void {
    debugger;
    let selectedids = [];
    this.saving = true;
    this.primengTableHelper.records.forEach(function (record) {
        if (record.isSelected) {
            selectedids.push(record.id);
        }
    });
    if (selectedids.length == 0) {
        this.notify.warn(this.l('NoDataSelected'));
        this.saving = false;
        return;
    }
    
    this._systemReportsServiceProxy.deleteAllActivityLog(selectedids)
        .pipe(finalize(() => { this.saving = false; }))
        .subscribe(() => {
            
           // this.reloadPage(true);
            this.saving = false;
            this.notify.info(this.l('DeletedSuccessfully'));
            this.getTodoActivityListfilter();
           
        });

}
}
