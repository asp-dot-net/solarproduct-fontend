import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoActivityListComponent } from './todo-activity-list.component';

describe('TodoActivityListComponent', () => {
  let component: TodoActivityListComponent;
  let fixture: ComponentFixture<TodoActivityListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodoActivityListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoActivityListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
