import { Component, Injector, ViewEncapsulation, ViewChild, ElementRef, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
//import { callDetailModalComponent } from './call-detail-modal.component';
import {  CommonLookupServiceProxy, LeadStateLookupTableDto, OrganizationUnitDto, CommonLookupDto, ProductSoldReportServiceProxy, JobStatusTableDto, ProductItemProductTypeLookupTableDto, LeadUsersLookupTableDto, SmsCountReportServiceProxy, UserDetailReportServiceProxy, UserActivityLogServiceProxy, UserActivityLogDto, JobCostServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';


@Component({
    templateUrl: './progressReport.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ProgressReportComponent extends AppComponentBase implements OnInit {
 show: boolean = true;
    FiltersData = false;
    isShowDivIf = true;  
    showchild: boolean = true;
    shouldShow: boolean = false;
    ExpandedView: boolean = true;
    SelectedLeadId: number = 0;
    salesRapeList : any[];
    toggle: boolean = true;

    change() {
        this.toggle = !this.toggle;
    }

    toggleDisplayDivIf() {  
        this.isShowDivIf = !this.isShowDivIf;  
      }      
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 330;
    firstrowcount = 0;
    last = 0;
    DateTypeFilter = 'FirstDeposite';
    date = new Date();
    stateFilter: any = "";
    filterText = '';

    // firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    
    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);
    totalProfit : number = 0;
    totalLoss : number = 0;
    allStates: LeadStateLookupTableDto[];
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnitlength: number = 0;
    organizationUnit = 0;
    
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    constructor(
        injector: Injector,
        private _jobCostServiceProxy: JobCostServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private titleService: Title
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Progress Report");
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 

        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Open Progress Report';
        log.section = 'Progress Report';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            
            this.getJobs();
        });
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  

    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }
        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }

    clear() {
        this.DateTypeFilter = '';
        this.startDate = moment(this.date);
        this.endDate = moment(this.date);
        this.getJobs();
    }

    getJobs(event?: LazyLoadEvent) {
        debugger;
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();
        this._jobCostServiceProxy.getAllProgressReport(
            this.organizationUnit,
            this.filterText,
            this.stateFilter,
            this.DateTypeFilter,
            this.startDate,
            this.endDate,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
           
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
           
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
         
            this.shouldShow = false;
        },
        err => {
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    

    exportToExcel(): void {
        
        this._jobCostServiceProxy.getAllProgressReportToExcel(
            this.organizationUnit,
            this.filterText,
            this.stateFilter,
            this.DateTypeFilter,
            this.startDate,
            this.endDate,
            )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
             });
    }
    
    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Progress Report';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Progress Report';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }

}