import { Component, Injector, ViewEncapsulation, ViewChild, ElementRef, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
//import { callDetailModalComponent } from './call-detail-modal.component';

import { TeamsServiceProxy, TeamDto, CommonLookupServiceProxy, LeadAssignReportServiceProxy, OrganizationUnitDto, CommonLookupDto, ProductSoldReportServiceProxy, JobStatusTableDto, ProductItemProductTypeLookupTableDto, LeadUsersLookupTableDto, SmsCountReportServiceProxy, InstallerInvoicePaymentDetailServiceProxy, CommonLookupInstaller, InstallerServiceProxy, UserActivityLogServiceProxy, UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { serialize } from 'v8';
import { finalize } from 'rxjs/operators';
import {InstallerinvoiceJobwiseComponent} from './InstallerinvoiceJobwise-modal.component'

@Component({
    templateUrl: './installerinvoicepayment.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class InstallerInvoicePaymentReportComponent extends AppComponentBase implements OnInit {

    FiltersData = false;
    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 330;
    firstrowcount = 0;
    last = 0;
  
    filterText = '';
    nameFilter = '';
    organizationUnit = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnitlength: number = 0;
    date = new Date();
    // firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    areafilter = "";
    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);
    userList : LeadUsersLookupTableDto[];
  state="";
    filteredTeams: LeadUsersLookupTableDto[];
    invoiceTypeFilter = "";
    installerInvoiceType : any[];
    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    toggleBlock() {
        this.show = !this.show;
      };
      toggleBlockChild() {
        this.showchild = !this.showchild;
      };
      toggle: boolean = true;
    
      change() {
          this.toggle = !this.toggle;
        }
        flag = true;
        showCancel: boolean = false;
        ExpandedView: boolean = true;
       
    datetype = 'FirstDeposite';
    jobStatus = 0;
    productItemId = 0;
    productTypeid = 0;
    userFilter = 0;
    alljobstatus: JobStatusTableDto[];
    allProductType : ProductItemProductTypeLookupTableDto[]; 
    jobStatusIDFilter = [];
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    
    filterName = 'JobNumber';
    orgCode = '';
    installerId: number;
    installername = "";
    total = 0;
    totalnsw = 0;
    totalqld = 0;
    totalsa = 0;
    totalvic = 0;
    //@ViewChild('callDetailModal', { static: true }) callDetailModal: callDetailModalComponent;
    @ViewChild('installerinvoiceJobwise', { static: true }) installerinvoiceJobwise: InstallerinvoiceJobwiseComponent;

    
    constructor(
        injector: Injector,
        private _teamsServiceProxy: TeamsServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _commonLookupService: CommonLookupServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _installerInvoicePaymentReportComponent : InstallerInvoicePaymentDetailServiceProxy,
        private titleService: Title,
        private _installerServiceProxy: InstallerServiceProxy
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Installer Invoice Payment Report");
        this.userFilter = 0;

    }

    
    getInstallerInvoicePayment(event?: LazyLoadEvent){
        this.primengTableHelper.showLoadingIndicator();
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this._installerInvoicePaymentReportComponent.getAll(
            filterText_,
            this.organizationUnit,
            this.installerId,
            this.startDate,
            this.endDate,
            this.areafilter,
            this.invoiceTypeFilter,
           
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
            ).subscribe(result => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
                this.firstrowcount =  totalrows + 1;
                this.last = totalrows + result.items.length;
                debugger;
                if (result.totalCount > 0) {
                    this.total = result.items[0].detailSummary.total;
                    this.totalnsw = result.items[0].detailSummary.nsw;
                    this.totalqld = result.items[0].detailSummary.qld;
                    this.totalsa = result.items[0].detailSummary.sa;
                    this.totalvic = result.items[0].detailSummary.vic;
                }
                else{
                    this.total = this.totalnsw = this.totalqld = this.totalsa = this.totalvic = 0;
                }
                this.shouldShow = false;
                this.primengTableHelper.hideLoadingIndicator();
            })

    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
        this._commonLookupService.getTeamForFilter().subscribe(teams => {
            this.filteredTeams = teams;
        });
        
        this._commonLookupService.getAllInstallerInvoiceType().subscribe(result => {
            this.installerInvoiceType = result
          })
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Installer Payment Report';
            log.section = 'Installer Payment Report';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.orgCode = this.allOrganizationUnits[0].code;
            
            this._commonLookupService.getAllJobStatusTableDropdown().subscribe(result => {
                this.alljobstatus = result;

                this._commonLookupService.getAllProductTypeForTableDropdown().subscribe(result => {
                    this.allProductType = result;
                });
                
            });
            this.bindInstaller();
            this.getInstallerInvoicePayment();
            //this.onOrganizationUnitChange();
        });
        
        
    }
    selectInstaller(event): void {
        this.installerId = event.id;
        this.installername = event.installerName;
        this.getInstallerInvoicePayment();
    }
    // onOrganizationUnitChange(event?: LazyLoadEvent) : void{
    //     this._commonLookupService.getAllUsersTableDropdownForUserCallHistory(this.organizationUnit).subscribe(rep => {
    //         this.userList = rep;
    //     });
    //     this.getInstallerInvoicePayment();
    // }
    installerSearchResult: any [];
    filterInstaller(event): void {
        debugger;
        if(!event.query){
            this.installerId = 0;
            this.installername = "";
            this.getInstallerInvoicePayment();
        }
        else{

            this.installerSearchResult = Object.assign([], this.InstallerList).filter(
                item => item.installerName.toLowerCase().indexOf(event.query.toLowerCase()) > -1  || item.companyName?.toLowerCase().indexOf(event.query.toLowerCase())>-1
            )
        }
        
    }
    installerinvoice(state? : any){
        debugger
    this.installerinvoiceJobwise.show(state,this.organizationUnit,this.areafilter,this.invoiceTypeFilter,this.startDate,this.endDate)
    }
    InstallerList: CommonLookupInstaller[];
    bindInstaller(){
        this.InstallerList = [];
        this._installerServiceProxy.getAllInstallersWithCompnayName(this.organizationUnit).subscribe(result => {
            this.InstallerList = result;
        });
    }
    
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  

    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }
        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }

    exportToExcel(): void {
        var filterText_ = this.filterText;
        
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }

        this._installerInvoicePaymentReportComponent.getInstallerInvoicePaymentExport(
            filterText_,
            this.organizationUnit,
            this.installerId,
            this.startDate,
            this.endDate,
            this.areafilter,
            this.invoiceTypeFilter,
            "",
            0,
            25
        )
        .subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
        });
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Installer Payment Report';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    
    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Installer Payment Report';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }

}