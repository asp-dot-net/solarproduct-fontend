import { Component, ViewChild, Injector, Output, EventEmitter, OnInit, ElementRef, Directive, Optional } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {InstallerInvoicePaymentDetailServiceProxy, JobNumberAmountDto, PurchaseOrderHistoryDto, StockOrderServiceProxy, WholeSaleLeadServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import PlaceResult = google.maps.places.PlaceResult;
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
    selector: 'installerinvoiceJobwise',
    templateUrl: './InstallerinvoiceJobwise-modal.component.html',
})
export class InstallerinvoiceJobwiseComponent extends AppComponentBase {
    @ViewChild("myNameElem") myNameElem: ElementRef;
    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @ViewChild('SampleDatePicker', { static: true }) sampleDatePicker: ElementRef;
    date = new Date();
    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);
    state:"";
    JobNumberAmountDto: JobNumberAmountDto[];
    constructor(
        injector: Injector,
        private _installerInvoicePaymentReportComponent: InstallerInvoicePaymentDetailServiceProxy,
        private _el: ElementRef,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,

    ) {
        super(injector);
        this._el.nativeElement.setAttribute('autocomplete', 'off');
        this._el.nativeElement.setAttribute('autocorrect', 'off');
        this._el.nativeElement.setAttribute('autocapitalize', 'none');
        this._el.nativeElement.setAttribute('spellcheck', 'false');
    }
    onShown(): void {
        
        // document.getElementById('stockorder_stockorder').focus();
    }
    show( State? : any, orgid?: number,areafilter?:any,invoiceTypeFilter?:any, startDate?: any,endDate?: any): void {
        debugger
        this._installerInvoicePaymentReportComponent.getAllInstallerPaymentJobNumberWise("", orgid,startDate,endDate,areafilter,invoiceTypeFilter,State).subscribe(result => {
                    this.JobNumberAmountDto = result;
                    this.state=State;
                    this.modal.show();
                    let log = new UserActivityLogDto();
                    log.actionId = 79;
                    log.actionNote ='Open Activity Log History';
                    log.section = 'Stock Order';
                     this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
        });
      
    }
    close(): void {

        this.modal.hide();
    }
}
