import { Component, Injector, OnInit, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLookupServiceProxy, JobPaymentOptionLookupTableDto, JobsServiceProxy, LeadsServiceProxy, LeadUsersLookupTableDto, OrganizationUnitDto, OutstandingReportDto, SystemReportsServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy, UserServiceProxy } from '@shared/service-proxies/service-proxies';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as moment from 'moment';
import { LazyLoadEvent, Paginator, Table } from 'primeng';
import { InvoiceSalesReportToexcelComponent } from '../invoice-SalesReport-To-excel.component';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-outstandingreport',
  templateUrl: './outstandingreport.component.html',
  styleUrls: ['./outstandingreport.component.css'],
  encapsulation: ViewEncapsulation.None,
  animations: [appModuleAnimation()]
})
export class OutStandingReportComponent extends AppComponentBase {

  show: boolean = true;
  showchild: boolean = true;
  toggleBlock() {
    this.show = !this.show;
  };
  toggleBlockChild() {
    this.showchild = !this.showchild;
  };

  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;
  @ViewChild('salesreporttoexcel', { static: true }) salesreporttoexcel: InvoiceSalesReportToexcelComponent;
  active = false;
  count = 0;
  OrgId = 0;
  date = '01/01/2021'
  public sampleDateRange: moment.Moment[] = [moment(this.date), moment().endOf('day')];
  StartDate: moment.Moment;
  EndDate: moment.Moment;
  jobPaymentOptions: JobPaymentOptionLookupTableDto[];
  paymenttypeid = [];
  OrgIds = [];
  result : OutstandingReportDto[];
  organizationid=0;
  allOrganizationUnits: OrganizationUnitDto[];
  headerss=[];
  constructor(
    injector: Injector,
    private _systemReportsServiceProxy: SystemReportsServiceProxy,
    private _fileDownloadService: FileDownloadService,
    private _jobsServiceProxy: JobsServiceProxy,
    private _userServiceProxy: UserServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
    private titleService: Title
    ) {
    super(injector);
    this.titleService.setTitle(this.appSession.tenancyName + " |  Out Standing Report");
  }

  ngOnInit() {
    this._commonLookupService.getAllPaymentOptionForTableDropdown().subscribe(result => {
      this.jobPaymentOptions = result;
    });
    this._userServiceProxy.getOrganizationUnit().subscribe(output => {
      let log = new UserActivityLogDto();
      log.actionId = 79;
      log.actionNote ='Open OutStanding Report';
      log.section = 'OutStanding Report';
      this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
          .subscribe(() => {
      });
      this.allOrganizationUnits = output;
      this.organizationid = this.allOrganizationUnits[0].id;
      this.getOutStanding();
    });
   
  }

  getOutStanding(event?: LazyLoadEvent) {
    debugger;
    
    if (this.sampleDateRange != null) {
      this.StartDate = this.sampleDateRange[0];
      this.EndDate = this.sampleDateRange[1];
    } else {
      this.StartDate = null;
      this.EndDate = null;
    }
    this._systemReportsServiceProxy.getoutstandingsForReport(
      this.organizationid,
      this.StartDate,
      this.EndDate,
      this.paymenttypeid

    ).subscribe(Output => {
      this.headerss = [];
      this.result = Output;
      Output[0].months.forEach(iteam => {
        this.headerss.push(iteam.headerName)
      });
    });
  }

  reloadPage(): void {
    this.paginator.changePage(this.paginator.getPage());
  }

  // exportToExcel(): void {

  // }
  // public exportExcel(): void {
  //   this.excelService.exportAsExcelFile(this.reportValueData, 'PunchFromReader' , this.myDate);
  // }

  // public exportAsExcelFile(json: any[], excelFileName: string, date: any): void {
  //   const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
  //   console.log('worksheet', worksheet);
  //   const workbook: XLSX.WorkBook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
  //   const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
  //   // const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
  //   this.saveAsExcelFile(excelBuffer, excelFileName, date);
  // }

  // private saveAsExcelFile(buffer: any, fileName: string, date: any): void {

  //   const data: Blob = new Blob([buffer], {
  //     type: EXCEL_TYPE
  //   });
  //   // FileSaver.saveAs(data, `${fileName}_export_${Date}${EXCEL_EXTENSION}`);
  //   // FileSaver.saveAs(data, fileName + '_' + date + EXCEL_EXTENSION);
  //   const content = `${fileName}_${date}${EXCEL_EXTENSION}`;
  //   FileSaver.saveAs(data, content);
  // }

//   import { Injectable, ElementRef } from '@angular/core';
// import * as FileSaver from 'file-saver';
// import * as moment from 'moment';
// import { DateTimeAdapter } from 'ng-pick-datetime';
// import * as XLSX from 'xlsx';

// const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
// const EXCEL_EXTENSION = '.xlsx';

// public exportPdf(): void {

//   const doc = new jsPDF('p', 'mm', 'a1');
//   doc.fromHTML(this.pdfDataElements.nativeElement, 70);
//  // doc.autoPrint();
//   doc.save('PunchFromReader.pdf');
// }

// public exportExcel(): void {
//   // let element = document.getElementById('excel-table');
//   const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.tablePrint.nativeElement);

//   /* generate workbook and add the worksheet */
//   const wb: XLSX.WorkBook = XLSX.utils.book_new();
//   XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

//   /* save to file */
//   XLSX.writeFile(wb, `working-weekly-off-report-${Date.now()}.xlsx`);
// }

addSearchLog(filter): void {
  let log = new UserActivityLogDto();
      log.actionId = 80;
      log.actionNote ='Searched by '+ filter ;
      log.section = 'OutStanding Report';
      this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
          .subscribe(() => {
      }); 
}



}
