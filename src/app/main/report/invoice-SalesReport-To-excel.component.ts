﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit, ElementRef, Directive, Optional } from '@angular/core';
import {
    CommonLookupServiceProxy,
    ImportDataViewDto,
    InstallerInvoiceImportDataViewDto,
    InvoicePaymentsServiceProxy,
    JobPaymentOptionLookupTableDto,
    JobsServiceProxy,
    LeadsServiceProxy,
    SystemReportsServiceProxy,
    UserActivityLogDto,
    UserActivityLogServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute, Router } from '@angular/router';
import { ViewMyLeadComponent } from '@app/main/myleads/myleads/view-mylead.component';
import { ModalDirective } from 'ngx-bootstrap/modal';
import * as moment from 'moment';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { finalize } from 'rxjs/operators';


@Component({
    selector: 'salesreporttoexcel',
    templateUrl: './invoice-SalesReport-To-excel.component.html',
})
export class InvoiceSalesReportToexcelComponent extends AppComponentBase implements OnInit {
    applicationview: ImportDataViewDto[];
    @ViewChild('viewModel', { static: true }) modal: ModalDirective;
    active = false;
    count = 0;
    OrgId = 0;
    date = '01/01/2021'
    public sampleDateRange: moment.Moment[] = [moment(this.date), moment().endOf('day')];
    StartDate: moment.Moment;
    EndDate: moment.Moment;
    jobPaymentOptions: JobPaymentOptionLookupTableDto[];
    paymenttypeid=[];
    OrgIds=[];
    constructor(
        injector: Injector,
        private _systemReportsServiceProxy: SystemReportsServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _jobsServiceProxy: JobsServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
    ) {
        super(injector);
    }
    ngOnInit() {

    }
  SectionName = '';
  show(OrgId: any, section =''): void {
        this.SectionName = section;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote = "Open Export Invoice Sales Report";
        log.section = section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
        this.OrgIds.push(OrgId);
        this._commonLookupService.getAllPaymentOptionForTableDropdown().subscribe(result => {
            this.jobPaymentOptions = result;
        });
        this.active = true;
        this.modal.show();
    }

    Submit(): void {
        debugger;
        if (this.sampleDateRange != null) {
            this.StartDate = this.sampleDateRange[0];
            this.EndDate = this.sampleDateRange[1];
        } else {
            this.StartDate = null;
            this.EndDate = null;
        }
        this._systemReportsServiceProxy.getSalesReportToExcel(
            this.OrgIds[0],
            this.StartDate,
            this.EndDate,
            this.paymenttypeid
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    close(): void {
        this.OrgIds = [];
        this.active = false;
        this.modal.hide();
    }

}
