﻿import {AppConsts} from '@shared/AppConsts';
import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute , Router} from '@angular/router';
import { UserCallHistoryServiceProxy, CommonLookupServiceProxy, OrganizationUnitDto, UserActivityLogDto, UserActivityLogServiceProxy  } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';


import { appModuleAnimation } from '@shared/animations/routerTransition';
// import { Table } from 'primeng/table';
// import { Paginator } from 'primeng/paginator';
// import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
// import { createOrEditCallFlowQueueModalComponent } from './create-or-edit-callFlowQueue-modal.component';
import { CsxCallQueueReportModalComponent } from './csx-call-queue-modal.component';
import { UserCsxCallQueueReportModalComponent } from './user-csx-call-queue-modal.component';

import { Title } from '@angular/platform-browser';

import { LazyLoadEvent, Paginator, Table } from 'primeng';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './csx-call-queue.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class CxCallQueueComponent extends AppComponentBase {
    
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;    
    @ViewChild('CsxCallQueueReportModal', { static: true }) CsxCallQueueReportModal: CsxCallQueueReportModalComponent;
    @ViewChild('UserCsxCallQueueReportModal', { static: true }) UserCsxCallQueueReportModal: UserCsxCallQueueReportModalComponent;
    
    advancedFiltersAreShown = false;
    filterText = '';
    nameFilter = '';
    firstrowcount = 0;
    last = 0;
    shouldShow: boolean = false;
    date = new Date();
    sampleDateRange: moment.Moment[] = [moment().add(0, 'days').endOf('day'), moment().add(0, 'days').endOf('day')];
    firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    startDate = moment().startOf('day');
    endDate = moment().endOf('day');
    organizationUnit = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnitlength: number = 0;
    public screenHeight: any;  
    testHeight = 250;
    FiltersData = false
    totalCalls: number = 0;
    showchild: boolean = true;
    
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };
      toggle: boolean = true;
    
    change() {
        this.toggle = !this.toggle;
    }

    constructor(
        injector: Injector,
        private _userCallHistoryServiceProxy: UserCallHistoryServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _fileDownloadService: FileDownloadService,
			private _router: Router,
            private titleService: Title
            ) {
                super(injector);
                this.titleService.setTitle(this.appSession.tenancyName + " |  3CX Call Queue");
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open 3CX Call Queue';
            log.section = '3CX Call Queue';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.getCallFlowQueue();
        });
    }
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  

    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }
        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }

    getCallFlowQueue(event?: LazyLoadEvent) {
        this.startDate = this.sampleDateRange[0];
        this.endDate = this.sampleDateRange[1];
        
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._userCallHistoryServiceProxy.getAllCallQueueCallHistory(
            this.organizationUnit,
            this.startDate,
            this.endDate,
            this.filterText,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
            ).subscribe(result => {
                // Filter records where count > 0
                const filteredItems = result.items.filter(item => item.count > 0);
        
                this.primengTableHelper.totalRecordsCount = filteredItems.length;
                this.primengTableHelper.records = filteredItems;
        
                const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
                this.firstrowcount = totalrows + 1;
                this.last = totalrows + filteredItems.length;
        
                if (filteredItems.length > 0) {
                    this.totalCalls = filteredItems[0].totalCalls;
                } else {
                    this.totalCalls = 0;
                }
        
                this.primengTableHelper.hideLoadingIndicator();
            });
        }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    // createLeadSource(): void {
    //     this._router.navigate(['/app/main/leadSources/leadSources/createOrEdit']);        
    // }

    exportToExcel(): void {
        this._userCallHistoryServiceProxy.getAllCallQueueCallHistoryExcel(
            this.organizationUnit,
            this.startDate,
            this.endDate,
            this.filterText,
            '',
            0,
            25
        )
        .subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
         });
    }
    
    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = '3CX Call Queue';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = '3CX Call Queue';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }   
    
}
