﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetAllCallFlowQueueCountDetailsDto, LeadSourceDto, UserActivityLogDto, UserActivityLogServiceProxy, UserCallHistoryServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

import { appModuleAnimation } from '@shared/animations/routerTransition';
import * as _ from 'lodash';
import * as moment from 'moment';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { UserCsxCallQueueReportModalComponent } from './user-csx-call-queue-modal.component';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'CsxCallQueueReportModal',
    templateUrl: './csx-call-queue-modal.component.html'
})
export class CsxCallQueueReportModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('UserCsxCallQueueReportModal', { static: true }) UserCsxCallQueueReportModal: UserCsxCallQueueReportModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    active = false;
    saving = false;
    organizationUnit = 0;

    item: GetAllCallFlowQueueCountDetailsDto[];
  
    queueExtentionNumber : string;
    startDate? : moment.Moment;
    endDate? : moment.Moment
    mobile ='';

    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
        ,private _userCallHistoryServiceProxy:UserCallHistoryServiceProxy
        ,private _fileDownloadService: FileDownloadService

    ) {
        super(injector);
    }
    sectionName ='';
    show(extentionNumber :number ,orgId : number,startDate? : moment.Moment, endDate? : moment.Moment, mobile? : string,section = ''): void {
        this.sectionName = section;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='View Detail User Name Wise Call History';
        log.section = section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
        this.queueExtentionNumber = extentionNumber.toString();
        this.startDate = startDate;
        this.endDate = endDate;
        this.organizationUnit = orgId;
        this.mobile = mobile;
        this._userCallHistoryServiceProxy.getCallQueueDetailsCount(orgId, startDate, endDate, '',extentionNumber.toString(),mobile).subscribe(result => {
            this.item = result;
            this.modal.show();
        });
        this.active = true;
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    exportToExcel(): void {
        this._userCallHistoryServiceProxy.getCallQueueDetailsCountExcel(
            this.organizationUnit, this.startDate, this.endDate, '', this.queueExtentionNumber, this.mobile
        )
        .subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
         });
    }
}
