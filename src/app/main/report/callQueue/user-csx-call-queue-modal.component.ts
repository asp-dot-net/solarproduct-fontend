﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetLeadSourceForViewDto, LeadSourceDto, UserCallHistoryServiceProxy, GetAllCallFlowQueueCallHistoryDetailsDto, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { finalize } from 'rxjs/operators';


@Component({
    selector: 'UserCsxCallQueueReportModal',
    templateUrl: './user-csx-call-queue-modal.component.html'
})
export class UserCsxCallQueueReportModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    queueExtentionNumber = '';
    extentionNumber = '';
    item: GetAllCallFlowQueueCallHistoryDetailsDto[];
    totalDuration = '';
    AVGDuration = '';
    startDate? : moment.Moment;
    endDate? : moment.Moment;
    mobile ='';

    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _userCallHistoryServiceProxy: UserCallHistoryServiceProxy
        ,private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    show(extentionNumber :number, startDate? : moment.Moment, endDate? : moment.Moment, queue? :string, mobile? :string,section = ''): void {
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='View Duration Detail';
        log.section = section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
        this.startDate = startDate;
        this.endDate = endDate;
        this.queueExtentionNumber = queue;
        this.extentionNumber = extentionNumber.toString();
        this.mobile = mobile;
        this._userCallHistoryServiceProxy.getCallQueueCallHistoryDetails(0, startDate, endDate, extentionNumber.toString() , queue,mobile).subscribe(result => {
            this.item = result;
            let dateObj = new Date(result[0].summary.avgDuration * 1000);
            let hours = dateObj.getUTCHours();
            let minutes = dateObj.getUTCMinutes();
            let seconds = dateObj.getSeconds();
    
            this.AVGDuration = hours.toString().padStart(2, '0') + ':' + 
        minutes.toString().padStart(2, '0') + ':' + 
        seconds.toString().padStart(2, '0');
        
         dateObj = new Date(result[0].summary.totalDuration * 1000);
         hours = dateObj.getUTCHours();
         minutes = dateObj.getUTCMinutes();
         seconds = dateObj.getSeconds();

        
            this.totalDuration = hours.toString().padStart(2, '0') + ':' + 
            minutes.toString().padStart(2, '0') + ':' + 
            seconds.toString().padStart(2, '0');

            this.modal.show();
        });
        this.active = true;
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    exportToExcel(): void {
        this._userCallHistoryServiceProxy.getCallQueueCallHistoryDetailsExcel(
            0, this.startDate, this.endDate, this.extentionNumber , this.queueExtentionNumber,this.mobile
        )
        .subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
         });
    }

}
