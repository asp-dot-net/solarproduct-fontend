import { Component, Injector, ViewEncapsulation, ViewChild, ElementRef, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
//import { callDetailModalComponent } from './call-detail-modal.component';

import { TeamsServiceProxy, TeamDto, CommonLookupServiceProxy, LeadAssignReportServiceProxy, OrganizationUnitDto, CommonLookupDto, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';


@Component({
    templateUrl: './leadassign.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class LeadAssignReportComponent extends AppComponentBase implements OnInit {

    FiltersData = false;
    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 330;
    firstrowcount = 0;
    last = 0;
    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    filterText = '';
    nameFilter = '';
    organizationUnit = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnitlength: number = 0;
    userFilter = 0;
    date = new Date();
    // firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    
    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);
    userList : CommonLookupDto[];
    totalAssign = 0;
    totalTV = 0 ;
    totalGoogle = 0;
    totalFaceBook = 0;
    totalRefferal = 0;
    totalOther = 0;
    dateFilterType = "CreationDate";
    toggleBlock() {
        this.show = !this.show;
      };
      toggleBlockChild() {
        this.showchild = !this.showchild;
      };
      toggle: boolean = true;
    
      change() {
          this.toggle = !this.toggle;
        }

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    //@ViewChild('callDetailModal', { static: true }) callDetailModal: callDetailModalComponent;

    viewModel(): void {
        //this.callDetailModal.show();
    }

    constructor(
        injector: Injector,
        private _teamsServiceProxy: TeamsServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _leadAssignReportServiceProxy: LeadAssignReportServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private titleService: Title
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Lead Assign Report");
    }

    
    getLeadAssign(event?: LazyLoadEvent){
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._leadAssignReportServiceProxy.getAll(
            this.organizationUnit,
            this.userFilter,
            this.startDate,
            this.endDate,
            this.dateFilterType,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
            ).subscribe(result => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
                this.firstrowcount =  totalrows + 1;
                this.last = totalrows + result.items.length;
                if (result.totalCount > 0) {
                    this.totalFaceBook = result.items[0].summary.totalFacebook;
                    this.totalGoogle = result.items[0].summary.totalGoogle;
                    this.totalOther = result.items[0].summary.totalOthers;
                    this.totalRefferal = result.items[0].summary.totalRefferal;
                    this.totalTV = result.items[0].summary.totalTV;
                    this.totalAssign = this.totalTV + this.totalRefferal + this.totalOther + this.totalGoogle + this.totalFaceBook ;
                }
                else{
                    this.totalAssign = this.totalTV = this.totalRefferal =  this.totalOther = this.totalGoogle = this.totalFaceBook = 0;
                }
                this.shouldShow = false;
                this.primengTableHelper.hideLoadingIndicator();
            })

    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            debugger;
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Lead Assign Report';
            log.section = 'Lead Assign Report';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            
            this.onOrganizationUnitChange();
        });

        
    }

    onOrganizationUnitChange(event?: LazyLoadEvent) : void{
        this._commonLookupService.getAllUsersByRoleNameTableDropdown('Sales Rep',this.organizationUnit).subscribe(result => {
            this.userList = result;
        });
        this.getLeadAssign(event);
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  

    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }
        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }

    exportToExcel(excelorcsv): void {
        
        this._leadAssignReportServiceProxy.getLeadAssignToExcel(
            this.organizationUnit,
            this.userFilter,
            this.startDate,
            this.endDate,
            excelorcsv
        )
        .subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
        });
    }
    
    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Lead Assign Report';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Lead Assign Report';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }

}