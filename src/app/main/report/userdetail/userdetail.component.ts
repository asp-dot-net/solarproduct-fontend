import { Component, Injector, ViewEncapsulation, ViewChild, ElementRef, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
//import { callDetailModalComponent } from './call-detail-modal.component';

import { TeamsServiceProxy, TeamDto, CommonLookupServiceProxy, LeadAssignReportServiceProxy, OrganizationUnitDto, CommonLookupDto, ProductSoldReportServiceProxy, JobStatusTableDto, ProductItemProductTypeLookupTableDto, LeadUsersLookupTableDto, SmsCountReportServiceProxy, UserDetailReportServiceProxy, UserActivityLogServiceProxy, UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';


@Component({
    templateUrl: './userdetail.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class UserDetailReportComponent extends AppComponentBase implements OnInit {

    FiltersData = false;
    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 250;
    firstrowcount = 0;
    last = 0;
    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    filterText = '';
    nameFilter = '';
    organizationUnit = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnitlength: number = 0;

    
  
    toggleBlock() {
        this.show = !this.show;
      };
      toggleBlockChild() {
        this.showchild = !this.showchild;
      };
      toggle: boolean = true;
    
      change() {
          this.toggle = !this.toggle;
        }

    filterName = 'Name';
    jobStatus = 0;
    productItemId = 0;
    productTypeid = 0;
    userFilter = 0;
    alljobstatus: JobStatusTableDto[];
    allProductType : ProductItemProductTypeLookupTableDto[]; 
    jobStatusIDFilter = [];
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
 
    //@ViewChild('callDetailModal', { static: true }) callDetailModal: callDetailModalComponent;

    
    constructor(
        injector: Injector,        
        private _fileDownloadService: FileDownloadService,
        private _commonLookupService: CommonLookupServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _userDetailReportServiceProxy : UserDetailReportServiceProxy,
        private titleService: Title
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  User Detail Report");
        this.userFilter = 0;

    }

    
    getUserDetail(event?: LazyLoadEvent){
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._userDetailReportServiceProxy.getAll(
            this.organizationUnit,
            this.filterName,
            this.filterText,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
            ).subscribe(result => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
                this.firstrowcount =  totalrows + 1;
                this.last = totalrows + result.items.length;
                
                
                this.shouldShow = false;
                this.primengTableHelper.hideLoadingIndicator();
            })

    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
        
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open User Detail Report';
            log.section = 'User Detail Report';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            

            this.onOrganizationUnitChange();
        });
        
        
    }

    onOrganizationUnitChange(event?: LazyLoadEvent) : void{
        
        this.getUserDetail();
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  

    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }
        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }

    exportToExcel(excelorcsv): void {
        this.primengTableHelper.showLoadingIndicator();

        this._userDetailReportServiceProxy.getUserDetailToExcel(
            this.organizationUnit,
            this.filterName,
            this.filterText,
            excelorcsv
        )
        .subscribe(result => {
            this.primengTableHelper.hideLoadingIndicator();
            this._fileDownloadService.downloadTempFile(result);
        });
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'User Detail Report';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'User Detail Report';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }

}