﻿import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { TeamsServiceProxy, CreateOrEditTeamDto, GetCallHistoryDetailDto, UserCallHistoryServiceProxy, CommissionReportServiceProxy, GetAllCommissionReportDetailsViewDto, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { FileDownloadService } from '@shared/utils/file-download.service';

@Component({
    selector: 'DetailModal',
    templateUrl: './detailuserwisemodal.component.html'
})
export class DetailModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    callDetailInput : GetAllCommissionReportDetailsViewDto[] ;
    
    startDate : moment.Moment;
    endDate : moment.Moment;
    userId : number;
    dateType : string;

    team: CreateOrEditTeamDto = new CreateOrEditTeamDto();
    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _userCallHistoryServiceProxy : UserCallHistoryServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _commissionReportServiceProxy: CommissionReportServiceProxy,

    ) {
        super(injector);
    }

  SectionName = '';
  show(userId : number, orgId : number, dateType? : string,startDate? : moment.Moment, endDate? : moment.Moment, section = '',commissiondate =''): void {
    debugger;
        this.SectionName = section;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote = "view Commission Job Wise";
        log.section = section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
        this.startDate = startDate;
        this.endDate = endDate;
        this.userId = userId;
        this.dateType = dateType;

        this._commissionReportServiceProxy.getAllCommissionReportDetail(
            '',orgId,userId,dateType,startDate,endDate,commissiondate).subscribe(result => {
            this.callDetailInput = result;
            this.modal.show();

            });

        // this._userCallHistoryServiceProxy.getUSerCallHistoryDetails(stateNameFilter, filterText, startDate, endDate, agent, callType).subscribe(result => {
        //     this.callDetailInput = result;
        //     this.modal.show();
        // });
    }

    exportToExcel(): void {
        
        // this._userCallHistoryServiceProxy.getUserCallHistoryDetailsToExcel(
        //     this.stateNameFilter, this.filterText, this.startDate, this.endDate, this.agent, this.callType
        // )
        // .subscribe(result => {
        //     this._fileDownloadService.downloadTempFile(result);
        // });
    }

    close(): void {
        this.modal.hide();
    }
}
