import { Component, Injector, ViewEncapsulation, ViewChild, HostListener, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { JobsServiceProxy, LeadsServiceProxy, UserCallHistoryServiceProxy,   CommonLookupDto,  OrganizationUnitDto, CommonLookupServiceProxy, LeadGenerationServiceProxy, CommissionReportServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { EntityTypeHistoryModalComponent } from '@app/shared/common/entityHistory/entity-type-history-modal.component';
import * as _ from 'lodash';
import * as moment from 'moment';
import { finalize } from 'rxjs/operators';
import { Title } from '@angular/platform-browser';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { CreateEditJobCommissionModel } from './create-jobcommission.component';
import { callDetailModalComponent } from '../callhistory/usercallhistory/calldetailuserwisemodal.component';

import { DetailModalComponent } from './detailuserwisemodal.component';

@Component({
    templateUrl: './jobcommission.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class JobCommissionComponent extends AppComponentBase {



    sectionId: number = 32;
    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    assignLead = 0;
    ExpandedView: boolean = true;
    SelectedLeadId: number = 0;
    filter= "";

    toggle: boolean = true;
    @Output() reloadmyLeadGeneration = new EventEmitter<boolean>();
    change() {
        this.toggle = !this.toggle;
      }

    
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    orgCode = '';

    //@ViewChild('entityTypeHistoryModal', { static: true }) entityTypeHistoryModal: EntityTypeHistoryModalComponent;
    //@ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    // @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
    //@ViewChild('jobBookingsmsemail', { static: true }) jobBookingsmsemail: JobBookingSmsEmailComponent;
    //@ViewChild('viewApplicationModal', { static: true }) viewApplicationModal: ViewApplicationModelComponent;
    // @ViewChild('CreateEditMyLeadGenModal', { static: true }) CreateEditMyLeadGenModal: CreateEditMyLeadGenModal;
     @ViewChild('createEditJobCommissionModel', { static: true }) createEditJobCommissionModel: CreateEditJobCommissionModel;

    @ViewChild('DetailModal', { static: true }) DetailModal: DetailModalComponent;

    viewModel(userID: number,commissionDate = ''): void {
        debugger;
        this.DetailModal.show(userID, this.organizationUnit, this.dateType, this.startDate, this.endDate,'Job Commission Report',commissionDate);
    }

    advancedFiltersAreShown = false;
    
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnit = 0;
    organizationUnitlength: number = 0;
    userId : number;
    users: CommonLookupDto[];

    dateType = 'InstallDate'
    // StartDate = moment().add(0, 'days').endOf('day');
    // EndDate = moment().add(0, 'days').endOf('day');
    currRoleName = '';
    date = new Date();
    StartDate: moment.Moment = moment(this.date);
    EndDate: moment.Moment = moment(this.date);
    firstrowcount = 0;
    last = 0;

    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 330;
    FiltersData = false;

    selectedRecords = 0;
    assignUserId = 0;
    assignUser: CommonLookupDto[];
    saving = false;
    filterName = 'JobNumber';
    uploadUrlstc: string;
    userFilter = 0;
    stateNameFilter = '';
    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);
    filterText = '';
    totalCount = 0;
    totalCommission = 0;
    totalNoOfPanel = 0;
    totalSystemCapacity = 0;
    totalBatteryKW = 0;
    constructor(
        injector: Injector,
        private _commonLookupService: CommonLookupServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy,
        private titleService: Title,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _commissionReportServiceProxy: CommissionReportServiceProxy,
        private _fileDownloadService: FileDownloadService,
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Commission");
    }

    roleName = ['Leadgen Manager', 'Leadgen SalesRep'];
    

    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
       
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Job Commission Report';
            log.section = 'Job Commission Report';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.orgCode = this.allOrganizationUnits[0].code;
            // this._commonLookupService.getAllUsersByRoleNameContainsTableDropdown(this.roleName, this.organizationUnit).subscribe(result => {
            //     this.users = result;
            // });
            let assignUserRole = ['Sales Rep'];

            this._commonLookupService.getAllUsersByRoleNameContainsTableDropdown(assignUserRole,this.allOrganizationUnits[0].id).subscribe(result => {
                this.users = result;
            });
            this.bindUsers(this.allOrganizationUnits[0].id);

            this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
                this.currRoleName = result;
                if(result == "Admin" || result == "Sales Manager")
                {
                    this.userId = 0;
                }
                else
                {
                    this.userId = this.appSession.user.id;
                }
            });

           // this.getLeads();
            this.getUserCallHistory();
        });
    }

    bindUsers(orgId) : void {
        let assignUserRole = ['Sales Rep'];
        this._commonLookupService.getAllUsersTableDropdownForUserCallHistory(orgId).subscribe(result => {
            this.users = result;
        });
        this._commonLookupService.getAllUsersByRoleNameContainsTableDropdown(assignUserRole, orgId).subscribe(result => {
            this.assignUser = result;
        });
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }

    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + 82 ;
        }
        else {
            this.testHeight = this.testHeight - 82 ;
        }
    }

    // private setIsEntityHistoryEnabled(): boolean {
    //     let customSettings = (abp as any).custom;
    //     return this.isGrantedAny('Pages.Administration.AuditLogs') && customSettings.EntityHistory && customSettings.EntityHistory.isEnabled && _.filter(customSettings.EntityHistory.enabledEntities, entityType => entityType === this._entityTypeFullName).length === 1;
    // }

    changeOrgCode() {
        this.orgCode = this.allOrganizationUnits.find(x => x.id == this.organizationUnit).code;
    }

    // getLeads(event?: LazyLoadEvent) {
    //     if (this.primengTableHelper.shouldResetPaging(event)) {
    //         this.paginator.changePage(0);
    //         return;
    //     }

    //     this.primengTableHelper.showLoadingIndicator();
    //     var text = this.filter != '' && this.filter !=null && this.filter!=undefined ? this.orgCode+this.filter  : ''  ;
        


    //     this._commissionReportServiceProxy.getAll(
    //         text,
    //         this.organizationUnit,
    //         this.userId,
    //         this.dateType,
    //         this.StartDate,
    //         this.EndDate,
    //         this.primengTableHelper.getSorting(this.dataTable),
    //         this.primengTableHelper.getSkipCount(this.paginator, event),
    //         this.primengTableHelper.getMaxResultCount(this.paginator, event)
    //     ).subscribe(result => {
    //         this.primengTableHelper.totalRecordsCount = result.totalCount;
    //         this.primengTableHelper.records = result.items;
    //         // this.tableRecords = result.items;
    //         const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
    //         this.firstrowcount =  totalrows + 1;
    //         this.last = totalrows + result.items.length;
    //         this.primengTableHelper.hideLoadingIndicator();
    //         this.shouldShow = false;
    //     });
    // }

    getUserCallHistory(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._commissionReportServiceProxy.getAll(
            this.filterText,
            this.organizationUnit,
            this.userId,
            this.dateType,
            this.startDate,
            this.endDate,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
            ).subscribe(result => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
                this.firstrowcount =  totalrows + 1;
                this.last = totalrows + result.items.length;
                this.primengTableHelper.hideLoadingIndicator();
                this.shouldShow = false;
                if (result.totalCount > 0) {
                    this.totalCount = result.items[0].summary.totalJobCount;
                    this.totalCommission = result.items[0].summary.totalCommission;
                    this.totalNoOfPanel = result.items[0].summary.totalNoOfPanel;
                    this.totalSystemCapacity = result.items[0].summary.totalSystemCapacity;
                    this.totalBatteryKW = result.items[0].summary.totalBatteryKw;
                }
                else{
                    
                    this.totalCount = this.totalCommission = this.totalNoOfPanel =  this.totalSystemCapacity = this.totalBatteryKW = 0;
                }
            })
    }

    reloadPage(event): void {
        this.ExpandedView = true;
        this.paginator.changePage(this.paginator.getPage());
    }



    
   
    expandGrid() {
        this.ExpandedView = true;
    }
    
    navigateToLeadDetail(leadid): void {
        // debugger;
        // this.ExpandedView = !this.ExpandedView;
        // this.ExpandedView = false;
        // this.SelectedLeadId = leadid;
        // this.viewLeadDetail.showDetail(leadid,null,12);
    }
    
    CreateAppontmentModal(leadId): void {
        // this.CreateEditMyLeadGenModal.show(leadId,this.organizationUnit,32);
     }

    clear() : void {
        this.dateType = 'Assign'
        this.StartDate = moment(this.date);
        this.EndDate = moment(this.date);
        this.getUserCallHistory();
        // this.getLeads();
    }

    count = 0;
    oncheckboxCheck() {
        this.count = 0;
        this.primengTableHelper.records.forEach(item => {

            if (item.isSelected == true) {
                this.count = this.count + 1;
            }
        })
    }

    checkAll(ev) {
        this.primengTableHelper.records.forEach(x =>{
            if(x.commissionDate == ''){
                x.isSelected = ev.target.checked
            }
        });
        this.oncheckboxCheck();
    }

    isAllChecked() {
        if (this.primengTableHelper.records)
            return this.primengTableHelper.records.every(_ => _.isSelected);
    }

    submit(jobs = [],action = '',issalesrap = true,user = 0,commitionDate = null): void {
        debugger;
        //let selectedids: number[] = [];
        
        // if (this.userId == 0) {
        //     this.notify.warn(this.l('PleaseSelectUser'));
        //     return;
        // }
        // this.primengTableHelper.records.forEach(function (lead) {
        //     if (lead.isSelected) {
        //         lead.jobs.forEach(function(id){
        //             selectedids.push(id);

        //         })
        //     }
        // });
        // if (selectedids.length == 0) {
        //     this.notify.warn('Please Select Data');
        //     return;
        // }
        // else if(selectedids.length > 1){
        //     this.notify.warn('Please Select only one');
        //     return;
        // }

        this._commissionReportServiceProxy.getCommissionAmountByUserId(jobs,this.organizationUnit).subscribe(result => {
            if(this.dateType = "NoDate"){
                this.createEditJobCommissionModel.show(result,jobs,37,'Job Commission Report',issalesrap,user,commitionDate)

            }else{
                this.createEditJobCommissionModel.show(result,jobs,37,'Job Commission Report',issalesrap)

            }

        })
        // if (selectedids.length == 0) {
        //     this.notify.warn(this.l('NoLeadsSelected'));
        //     return;
        // }

        // this.saving = true;
        // this._leadGenerationServiceProxy.transferToSalesRep(this.assignUserId, this.sectionId, selectedids).pipe(finalize(() => { this.saving = false; })).subscribe(() => {
        //     this.notify.success(this.l('AssignedSuccessfully'));
        //     this.getLeads();
        // }, error => {
        //     this.saving = false;
        // });
    }

    exportToExcel(): void {
        this._commissionReportServiceProxy.getCommissionToExcel(
            this.filterText,
            this.organizationUnit,
            this.userId,
            this.dateType,
            this.startDate,
            this.endDate,
            // this.primengTableHelper.getSorting(this.dataTable),
            // this.primengTableHelper.getSkipCount(this.paginator,0),
            // this.primengTableHelper.getMaxResultCount(this.paginator, event)
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
             });  
       
    }
    userSearchResult: any [];

    filterUser(event): void {
        this.userSearchResult = Object.assign([], this.users).filter(
          item => item.displayName.toLowerCase().indexOf(event.query.toLowerCase()) > -1
        )
      }

      selectUser(event): void {
        this.userId = event.id;
        this.getUserCallHistory();
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Job Commission Report';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Job Commission Report';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }

}
