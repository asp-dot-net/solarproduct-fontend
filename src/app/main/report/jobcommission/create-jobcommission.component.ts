import { Component, ElementRef, Injector, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { LeadGenerationServiceProxy, BonusCommissionItemDto, CommonLookupServiceProxy, CommonLookupDto, CreateCommissionDto, CommissionReportServiceProxy, GetCommissionAmountList, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import * as moment from 'moment';

@Component({
    selector: 'createEditJobCommissionModel',
    templateUrl: './create-jobcommission.component.html',
})
export class CreateEditJobCommissionModel extends AppComponentBase {
    @ViewChild('createEditJobCommissionModel', { static: true }) modal: ModalDirective;
    @ViewChild("myNameElem") myNameElem: ElementRef;

    ExpandedViewApp: boolean = true;
    active = false;
    saving = true;
    leadId = 0;
    allBonusList: CommonLookupDto[];
    leadGenAppointment: CreateCommissionDto = new CreateCommissionDto();
    userList: CommonLookupDto[];
    sectionId: number;
    CommissionDate = moment().add(0, 'days').endOf('day');
    commissionAmountList: GetCommissionAmountList[];
    PurchaseOrderItemList: any[];
    issalesrap = true;
    isVerified=false;
    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy: UserActivityLogServiceProxy,
        private _commissionReportServiceProxy: CommissionReportServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this._commonLookupService.getBonusesDropdown().subscribe(result => {
            this.allBonusList = result;
        });
    }

    toggleBonusCommission(event: any): void {
        this.leadGenAppointment.isBonusCommission = event.target.checked;
    }

    SectionName = '';
    Action = 'Create';
    userid =0 ;
    // show(commissionList: GetCommissionAmountList[], appointmentIds: number[], sectionId?: number, section = '', role = true,user = 0,commitionDate = null) {
    //     debugger;
    //     this.userid = user;
    //     this.issalesrap = role;
    //     // this.Action = action;
    //     this.SectionName = section;
    //     this.commissionAmountList = commissionList;

    //     if (this.Action == 'Create') {
    //         let log = new UserActivityLogDto();
    //         log.actionId = 79;
    //         log.actionNote = "Open For Create Commission";
    //         log.section = section;
    //         this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
    //             .subscribe(() => {
    //             });
    //         this.leadGenAppointment = new CreateCommissionDto();
    //         this.leadGenAppointment.note = "";
    //         this.leadGenAppointment.commitionDate = moment().add(0, 'days').endOf('day');
    //         this.leadGenAppointment.amount = commissionList.reduce((prev, next) => prev + next.value, 0);
    //         this.leadGenAppointment.appointmentIds = appointmentIds;
    //         this.leadGenAppointment.sectionId = sectionId;
    //         this.leadGenAppointment.bonusCommissionItemDto = [];
    //         this.leadGenAppointment.bonusCommissionItemDto.push(new BonusCommissionItemDto);
    //         this.calculateRate();
    //     }
    //     else {
    //         let log = new UserActivityLogDto();
    //         log.actionId = 79;
    //         log.actionNote = "Open For Update Commission";
    //         log.section = section;
    //         this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
    //             .subscribe(() => {
    //             });

    //         if(user > 0 ){
    //             this._commissionReportServiceProxy.getBonusCommissionForDto(commitionDate,user).subscribe(result => {
    //                 this.leadGenAppointment = result;
    //                 if (result.bonusCommissionItemDto) {
    //                     if (result.bonusCommissionItemDto.length == 0) {
    //                         this.leadGenAppointment.bonusCommissionItemDto = [];
    //                         this.leadGenAppointment.bonusCommissionItemDto.push(new BonusCommissionItemDto);
    //                     }
    //                 } else {
    //                     this.leadGenAppointment.bonusCommissionItemDto = [];
    //                     this.leadGenAppointment.bonusCommissionItemDto.push(new BonusCommissionItemDto);
    //                 }
    //             })
    //         }
    //         else{
    //                 this._commissionReportServiceProxy.getCommissionForDto(appointmentIds[0]).subscribe(result => {
    //                     this.leadGenAppointment = result;
    //                 if (result.bonusCommissionItemDto) {
    //                     if (result.bonusCommissionItemDto.length == 0) {
    //                         this.leadGenAppointment.bonusCommissionItemDto = [];
    //                         this.leadGenAppointment.bonusCommissionItemDto.push(new BonusCommissionItemDto);
    //                     }
    //                 } else {
    //                     this.leadGenAppointment.bonusCommissionItemDto = [];
    //                     this.leadGenAppointment.bonusCommissionItemDto.push(new BonusCommissionItemDto);
    //                 }
    //             })
    //         }
            

    //     }

    //     this.modal.show();
    // }
    show(commissionList: GetCommissionAmountList[], appointmentIds: number[], sectionId?: number, section = '', role = true, user = 0, commitionDate = null) {
        debugger;
        this.userid = user;
        this.issalesrap = role;
        this.SectionName = section;
        this.commissionAmountList = commissionList;
    
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote = user > 0 ? "Open For Update Commission" : "Open For Create Commission";
        log.section = section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { })).subscribe(() => { });
    
        if (user > 0) {
            this._commissionReportServiceProxy.getCommissionForDto(appointmentIds[0]).subscribe(result => {
                this.leadGenAppointment = result;
                this.leadGenAppointment.appointmentIds=appointmentIds;
                this.isVerified = result.commitionIsVerified;
                this.leadGenAppointment.bonusCommissionItemDto = result.bonusCommissionItemDto?.length ? result.bonusCommissionItemDto : [new BonusCommissionItemDto()];
            }); 
        } else {
            this.leadGenAppointment = new CreateCommissionDto();
            this.leadGenAppointment.note = "";
            this.leadGenAppointment.commitionDate = moment().add(0, 'days').endOf('day');
            this.leadGenAppointment.amount = commissionList.reduce((prev, next) => prev + next.value, 0);
            this.leadGenAppointment.appointmentIds = appointmentIds;
            this.leadGenAppointment.sectionId = sectionId;
            this.leadGenAppointment.bonusCommissionItemDto = [new BonusCommissionItemDto()];
            this.calculateRate();
            this._commissionReportServiceProxy.getBonusCommissionForDto(commitionDate, user).subscribe(result => {
                this.leadGenAppointment = result;
                this.leadGenAppointment.bonusCommissionItemDto = result.bonusCommissionItemDto?.length ? result.bonusCommissionItemDto : [new BonusCommissionItemDto()];
            });
            
        }
    
        this.modal.show();
    }
    
    close(): void {
        this.active = false;
        this.modal.hide();
    }

    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;
    }

    expandApp() {
        this.ExpandedViewApp = false;
    }

    addPriceList(): void {
        this.leadGenAppointment.bonusCommissionItemDto.push(new BonusCommissionItemDto);
        this.calculateRate();
    }

    removePriceList(rowitem): void {
        if (this.leadGenAppointment.bonusCommissionItemDto.length == 1) return;
        this.leadGenAppointment.bonusCommissionItemDto.splice(this.leadGenAppointment.bonusCommissionItemDto.indexOf(rowitem), 1);
        this.calculateRate();
    }
    calculateRate(): void {
        let totalamount = 0;
        this.leadGenAppointment.bonusCommissionItemDto.forEach(function (item) {
            if (item.amount > 0) {
                totalamount = parseFloat(totalamount.toString()) + parseFloat(item.amount.toString());
            }
        });

        // Adding the leadGenAppointment.amount to the total amount
        totalamount += parseFloat(this.leadGenAppointment.amount.toString());

        this.leadGenAppointment.commitionActualAmount = totalamount;
    }

    save(): void {
        debugger
        this.saving = true;
        this.leadGenAppointment.userId = this.userid;
        this.leadGenAppointment.bonusCommissionItemDto.forEach(item => {
            item.jobId = this.leadGenAppointment.appointmentIds[0] ?? 0; 
            item.userId=this.leadGenAppointment.userId;
        });
        this._commissionReportServiceProxy.createCommission(this.leadGenAppointment)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                let log = new UserActivityLogDto();
                log.actionId = 65;
                log.actionNote = this.Action == 'Create' ? "Commission Created" : "Commission Updated";
                log.section = this.SectionName;
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                    });
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
            });
    }
}
