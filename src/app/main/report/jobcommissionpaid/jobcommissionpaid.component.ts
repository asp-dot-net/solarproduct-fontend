import { Component, Injector, ViewEncapsulation, ViewChild, HostListener, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { JobsServiceProxy, LeadsServiceProxy, UserCallHistoryServiceProxy,   CommonLookupDto,  OrganizationUnitDto, CommonLookupServiceProxy, LeadGenerationServiceProxy, CommissionReportServiceProxy, LeadUsersLookupTableDto, UserActivityLogServiceProxy, UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import * as _ from 'lodash';
import * as moment from 'moment';
import { Title } from '@angular/platform-browser';
import { finalize } from 'rxjs/operators';


@Component({
    templateUrl: './jobcommissionpaid.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class JobCommissionPaidComponent extends AppComponentBase {

    sectionId: number = 32;
    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    assignLead = 0;
    ExpandedView: boolean = true;
    SelectedLeadId: number = 0;
    filter= "";

    toggle: boolean = true;
    @Output() reloadmyLeadGeneration = new EventEmitter<boolean>();
    change() {
        this.toggle = !this.toggle;
      }

    
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    orgCode = '';
;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
   

    // viewModel(userID: number): void {
    //     this.DetailModal.show(userID, this.organizationUnit, this.dateType, this.startDate, this.endDate);
    // }

    advancedFiltersAreShown = false;
    
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnit = 0;
    organizationUnitlength: number = 0;
    userId : number;
    users: CommonLookupDto[];

    dateType = 'InstallDate'
    // StartDate = moment().add(0, 'days').endOf('day');
    // EndDate = moment().add(0, 'days').endOf('day');
    currRoleName = '';
    date = new Date();
    StartDate: moment.Moment = moment(this.date);
    EndDate: moment.Moment = moment(this.date);
    firstrowcount = 0;
    last = 0;

    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 250;
    FiltersData = false;

    selectedRecords = 0;
    assignUserId = 0;
    assignUser: CommonLookupDto[];
    saving = false;
    filterName = 'JobNumber';

    totalInbound = 0;
    totalNotAnswered = 0;
    totalMissed = 0;
    uploadUrlstc: string;
    totalCall = 0;
    userFilter = 0;
    stateNameFilter = '';
    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);
    filterText = '';
    totalOutbound = 0;
    filteredTeams: LeadUsersLookupTableDto[];
    teamId = 0;
 
    constructor(
        injector: Injector,
        private _commonLookupService: CommonLookupServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy,
        private titleService: Title,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _commissionReportServiceProxy: CommissionReportServiceProxy,

    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Commission Paid");
        this._commonLookupService.getTeamForFilter().subscribe(teams => {
            this.filteredTeams = teams;
          });
    }

    roleName = ['Leadgen Manager', 'Leadgen SalesRep'];
    

    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
       
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Job Commission Paid Report';
            log.section = 'Job Commission Paid Report';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.orgCode = this.allOrganizationUnits[0].code;
            // this._commonLookupService.getAllUsersByRoleNameContainsTableDropdown(this.roleName, this.organizationUnit).subscribe(result => {
            //     this.users = result;
            // });
            let assignUserRole = ['Sales Rep'];

            this._commonLookupService.getAllUsersByRoleNameContainsTableDropdown(assignUserRole,this.allOrganizationUnits[0].id).subscribe(result => {
                this.users = result;
            });
            this.bindUsers(this.allOrganizationUnits[0].id);

            this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
                this.currRoleName = result;
                if(result == "Admin" || result == "Sales Manager")
                {
                    this.userId = 0;
                }
                else
                {
                    this.userId = this.appSession.user.id;
                }
            });

           // this.getLeads();
            this.getUserCallHistory();
        });
    }

    bindUsers(orgId) : void {
        let assignUserRole = ['Sales Rep'];
        this._commonLookupService.getAllUsersByRoleNameContainsTableDropdown(assignUserRole,orgId).subscribe(result => {
            this.users = result;
        });
        this._commonLookupService.getAllUsersByRoleNameContainsTableDropdown(assignUserRole, orgId).subscribe(result => {
            this.assignUser = result;
        });
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }

    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + 82 ;
        }
        else {
            this.testHeight = this.testHeight - 82 ;
        }
    }

    // private setIsEntityHistoryEnabled(): boolean {
    //     let customSettings = (abp as any).custom;
    //     return this.isGrantedAny('Pages.Administration.AuditLogs') && customSettings.EntityHistory && customSettings.EntityHistory.isEnabled && _.filter(customSettings.EntityHistory.enabledEntities, entityType => entityType === this._entityTypeFullName).length === 1;
    // }

    changeOrgCode() {
        this.orgCode = this.allOrganizationUnits.find(x => x.id == this.organizationUnit).code;
    }

    // getLeads(event?: LazyLoadEvent) {
    //     if (this.primengTableHelper.shouldResetPaging(event)) {
    //         this.paginator.changePage(0);
    //         return;
    //     }

    //     this.primengTableHelper.showLoadingIndicator();
    //     var text = this.filter != '' && this.filter !=null && this.filter!=undefined ? this.orgCode+this.filter  : ''  ;
        


    //     this._commissionReportServiceProxy.getAll(
    //         text,
    //         this.organizationUnit,
    //         this.userId,
    //         this.dateType,
    //         this.StartDate,
    //         this.EndDate,
    //         this.primengTableHelper.getSorting(this.dataTable),
    //         this.primengTableHelper.getSkipCount(this.paginator, event),
    //         this.primengTableHelper.getMaxResultCount(this.paginator, event)
    //     ).subscribe(result => {
    //         this.primengTableHelper.totalRecordsCount = result.totalCount;
    //         this.primengTableHelper.records = result.items;
    //         // this.tableRecords = result.items;
    //         const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
    //         this.firstrowcount =  totalrows + 1;
    //         this.last = totalrows + result.items.length;
    //         this.primengTableHelper.hideLoadingIndicator();
    //         this.shouldShow = false;
    //     });
    // }

    getUserCallHistory(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();
        var filterText_ = this.filterText;
        if(this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._commissionReportServiceProxy.getAllCommissionPaidReport(
            filterText_,
            this.organizationUnit,
            this.teamId,
            this.userId,
            this.startDate,
            this.endDate,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
            ).subscribe(result => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
                this.firstrowcount =  totalrows + 1;
                this.last = totalrows + result.items.length;
                this.primengTableHelper.hideLoadingIndicator();
                this.shouldShow = false;
                // if (result.totalCount > 0) {
                //     this.totalOutbound = result.items[0].summary.totalOutbound;
                //     this.totalInbound = result.items[0].summary.totalInbound;
                //     this.totalNotAnswered = result.items[0].summary.totalNotanswered;
                //     this.totalMissed = result.items[0].summary.totalMissed;
                //     this.totalCall = this.totalOutbound + this.totalInbound + this.totalNotAnswered + this.totalMissed ;
                // }
                // else{
                    
                //     this.totalOutbound = this.totalInbound = this.totalNotAnswered =  this.totalMissed = this.totalCall = 0;
                // }
            })
    }

    reloadPage(event): void {
        this.ExpandedView = true;
        this.paginator.changePage(this.paginator.getPage());
    }



    
   
    expandGrid() {
        this.ExpandedView = true;
    }
    
    navigateToLeadDetail(leadid): void {
        // debugger;
        // this.ExpandedView = !this.ExpandedView;
        // this.ExpandedView = false;
        // this.SelectedLeadId = leadid;
        // this.viewLeadDetail.showDetail(leadid,null,12);
    }
    
    CreateAppontmentModal(leadId): void {
        // this.CreateEditMyLeadGenModal.show(leadId,this.organizationUnit,32);
     }

    clear() : void {
        this.dateType = 'Assign'
        this.StartDate = moment(this.date);
        this.EndDate = moment(this.date);
        this.getUserCallHistory();
        // this.getLeads();
    }

    count = 0;
    oncheckboxCheck() {
        this.count = 0;
        this.primengTableHelper.records.forEach(item => {

            if (item.isSelected == true) {
                this.count = this.count + 1;
            }
        })
    }

    checkAll(ev) {
        this.primengTableHelper.records.forEach(x => x.isSelected = ev.target.checked);
        this.oncheckboxCheck();
    }

    isAllChecked() {
        if (this.primengTableHelper.records)
            return this.primengTableHelper.records.every(_ => _.isSelected);
    }

   
    userSearchResult: any [];

    filterUser(event): void {
        this.userSearchResult = Object.assign([], this.users).filter(
          item => item.displayName.toLowerCase().indexOf(event.query.toLowerCase()) > -1
        )
      }

      selectUser(event): void {
        this.userId = event.id;
        this.getUserCallHistory();
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Job Commission Paid Report';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Job Commission Paid Report';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }

}
