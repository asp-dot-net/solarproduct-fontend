﻿import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { TeamsServiceProxy, CreateOrEditTeamDto, GetCallHistoryDetailDto, UserCallHistoryServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { FileDownloadService } from '@shared/utils/file-download.service';

@Component({
    selector: 'callDetailModal',
    templateUrl: './calldetailuserwisemodal.component.html'
})
export class callDetailModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    // callDetailInput : GetCallHistoryDetailDto[] ;
    callDetailInput: GetCallHistoryDetailDto[] = [];
originalCallDetailInput: GetCallHistoryDetailDto[] = []; 
    callType = '';
    agentName = '';
    durationFilter=';'
    stateNameFilter = '';
    filterText = '';
    startDate : moment.Moment;
    endDate : moment.Moment;
    agent : string;
    sortField: string | null = null; 
    sortOrder: string | null = null; 
    team: CreateOrEditTeamDto = new CreateOrEditTeamDto();
    constructor(
        injector: Injector,
        private _userCallHistoryServiceProxy : UserCallHistoryServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _fileDownloadService: FileDownloadService,

    ) {
        super(injector);
    }

    show(stateNameFilter : string , filterText : string, startDate? : moment.Moment, endDate? : moment.Moment, agent? : string, callType? : string, agentName? : string,section = ''): void {
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='View ' + agentName + ' - ' + callType + ' Call History';
        log.section = section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
        this.stateNameFilter = stateNameFilter;
        this.filterText = filterText;
        this.startDate = startDate;
        this.endDate = endDate;
        this.agent = agent;
        this.callType = callType;
        this.agentName = agentName;
       this.GetCallHistroeyDetail()
            this.modal.show();
        
    }
    DurationApply(): void {
        if (this.durationFilter) {
            this.callDetailInput = this.originalCallDetailInput.filter(call => {
                const callDurationInMinutes = this.convertDurationToMinutes(call.duration);
                switch (this.durationFilter) {
                    case 'LessThan1':
                        return callDurationInMinutes < 1;
                    case 'GreaterThan5':
                        return callDurationInMinutes > 5;
                    case 'GreaterThan10':
                        return callDurationInMinutes > 10;
                    default:
                        return true;
                }
            });
        } else {
           
            this.callDetailInput = [...this.originalCallDetailInput];
        }
    }
    
    
    
    GetCallHistroeyDetail() {
        this._userCallHistoryServiceProxy.getUSerCallHistoryDetails(
            this.stateNameFilter, 
            this.filterText, 
            this.startDate, 
            this.endDate, 
            this.agent, 
            this.callType,
            ""

        ).subscribe(result => {
            this.originalCallDetailInput = result;
            this.callDetailInput = result;
        });
    }
    convertDurationToMinutes(duration: string): number {
        debugger
        const [hours, minutes] = duration.split(':').map(num => parseInt(num, 10));
        return hours;
    }
    sortData(field: string): void {
        
        if (this.sortField === field) {
            this.sortOrder = this.sortOrder === 'asc' ? 'desc' : 'asc';
        } else {
           
            this.sortField = field;
            this.sortOrder = 'asc';
        }

        
        if (this.sortField && this.sortOrder) {
            this.callDetailInput.sort((a, b) => {
                const aValue = a[field];
                const bValue = b[field];

                if (aValue < bValue) return this.sortOrder === 'asc' ? -1 : 1;
                if (aValue > bValue) return this.sortOrder === 'asc' ? 1 : -1;
                return 0; 
            });
        }
    }
    exportToExcel(): void {
        
        this._userCallHistoryServiceProxy.getUserCallHistoryDetailsToExcel(
            this.stateNameFilter, this.filterText, this.startDate, this.endDate, this.agent, this.callType,""
        )
        .subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
        });
    }

    close(): void {
        this.modal.hide();
        this.durationFilter=''
    }
}
