import { Component, Injector, ViewEncapsulation, ViewChild, ElementRef, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { callDetailModalComponent } from './calldetailuserwisemodal.component';

import { TeamsServiceProxy, TeamDto , UserCallHistoryServiceProxy , GetAllUserCallHistoryDto, CommonLookupServiceProxy, LeadStateLookupTableDto, UserServiceProxy, CommonLookupDto, OrganizationUnitDto, GetCallHistoryDetailDto, UserActivityLogServiceProxy, UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';


@Component({
    templateUrl: './usercallhistory.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class UserCallHistoryComponent extends AppComponentBase implements OnInit {

    FiltersData = false;
    public screenWidth: any;  
    public screenHeight: any;  
    // testHeight = 330;
    testHeight = 330;
    firstrowcount = 0;
    last = 0;
    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    filterText = '';
    stateNameFilter = '';
    userFilter = 0;
    date = new Date();
    // firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    // startDate = moment().startOf('day');
    // endDate = moment().endOf('day');
    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);
    allStates: LeadStateLookupTableDto[];
    userList : CommonLookupDto[];
    organizationUnit = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnitlength: number = 0;
    totalOutbound = 0;
    totalInbound = 0;
    totalNotAnswered = 0;
    totalMissed = 0;
    uploadUrlstc: string;
    totalCall = 0;

    toggleBlock(): void {
        this.show = !this.show;
    };
      
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };
      toggle: boolean = true;
    
    change() {
        this.toggle = !this.toggle;
    }

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('callDetailModal', { static: true }) callDetailModal: callDetailModalComponent;

    viewModel(agent: string, callType : string, agentName :string): void {
        this.callDetailModal.show(this.stateNameFilter, this.filterText, this.startDate, this.endDate, agent, callType, agentName,'User Call History');
    }

    constructor(
        injector: Injector,
        private _teamsServiceProxy: TeamsServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private titleService: Title,
        private _userCallHistoryServiceProxy : UserCallHistoryServiceProxy,
        private _commonLookupService : CommonLookupServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _userServiceProxy : UserServiceProxy,
    
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  User Call History");
    }

    getUserCallHistory(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._userCallHistoryServiceProxy.getAll(
            this.organizationUnit,
            this.userFilter,
            this.stateNameFilter,
            this.filterText,
            this.startDate,
            this.endDate,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
            ).subscribe(result => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
                this.firstrowcount =  totalrows + 1;
                this.last = totalrows + result.items.length;
                this.primengTableHelper.hideLoadingIndicator();
                if (result.totalCount > 0) {
                    this.totalOutbound = result.items[0].summary.totalOutbound;
                    this.totalInbound = result.items[0].summary.totalInbound;
                    this.totalNotAnswered = result.items[0].summary.totalNotanswered;
                    this.totalMissed = result.items[0].summary.totalMissed;
                    this.totalCall = this.totalOutbound + this.totalInbound + this.totalNotAnswered + this.totalMissed ;
                }
                else{
                    
                    this.totalOutbound = this.totalInbound = this.totalNotAnswered =  this.totalMissed = this.totalCall = 0;
                }
            })
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 

        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open User Call History';
            log.section = 'User Call History';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.onOrganizationUnitChange();
        });

        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
    }
    
   
    onOrganizationUnitChange(event?: LazyLoadEvent) : void{
        this._commonLookupService.getAllUsersTableDropdownForUserCallHistory(this.organizationUnit).subscribe(result => {
            this.userList = result;
        });
        this.getUserCallHistory();
    }

    userSearchResult: any [];
    filterUser(event): void {
      this.userSearchResult = Object.assign([], this.userList).filter(
        item => item.displayName.toLowerCase().indexOf(event.query.toLowerCase()) > -1
      )
    }

    selectUser(event): void {
        this.userFilter = event.id;
        this.getUserCallHistory();
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  

    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }
        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }

    exportToExcel(excelorcsv): void {
        
        this._userCallHistoryServiceProxy.getUserCallHistoryToExcel(
            this.organizationUnit,
            this.userFilter,
            this.stateNameFilter,
            '',
            this.filterText,
            this.startDate,
            this.endDate,
            excelorcsv
        )
        .subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
        });
    }

    onClear() : void{
        this.startDate = moment(this.date);
        this.endDate = moment(this.date);
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'User Call History';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'User Call History';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }

}