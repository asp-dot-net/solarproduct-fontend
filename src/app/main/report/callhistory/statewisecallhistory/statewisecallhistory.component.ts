import { Component, Injector, ViewEncapsulation, ViewChild, ElementRef, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { statecallDetailModalComponent } from './calldetailstatewisemodal.component';

import { TeamsServiceProxy, TeamDto, CommonLookupServiceProxy, LeadStateLookupTableDto, OrganizationUnitDto, UserCallHistoryServiceProxy, LocalityLookupDto, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FileUpload } from 'primeng/fileupload';
import { finalize } from 'rxjs/operators';
import { NotifyService, TokenService } from 'abp-ng2-module';
import { AppConsts } from '@shared/AppConsts';

@Component({
    templateUrl: './statewisecallhistory.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class StateWiseCallHistoryComponent extends AppComponentBase implements OnInit {

    FiltersData = false;
    public screenWidth: any;
    public screenHeight: any;
    // testHeight = 330;
    testHeight = 330;
    firstrowcount = 0;
    last = 0;
    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    filterText = '';
    nameFilter = '';
    uploadUrlstc: string;
    allStates: LeadStateLookupTableDto[];
    organizationUnit = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnitlength: number = 0;
    stateNameFilter = '';
    date = new Date();
    // firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    // startDate = moment().startOf('day');
    // endDate = moment().endOf('day');
    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);
    allLocality: LocalityLookupDto[];
    locality = '';
    totalQLD = 0;
    totalSA = 0;
    totalNSW = 0;
    totalVIC = 0;
    totalOthers = 0;
    totalCall = 0;


    toggleBlock() {
        this.show = !this.show;
    };

    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    toggle: boolean = true;

    change() {
        this.toggle = !this.toggle;
    }

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('statecallDetailModal', { static: true }) statecallDetailModal: statecallDetailModalComponent;
    @ViewChild('ExcelFileUpload', { static: false }) excelFileUpload: FileUpload;

    viewModel(state: string): void {
        this.statecallDetailModal.show(this.filterText, this.startDate, this.endDate, state, this.organizationUnit,'State Wise Call History');
    }

    constructor(
        injector: Injector,
        private _teamsServiceProxy: TeamsServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private titleService: Title,
        private _commonLookupService: CommonLookupServiceProxy,
        private _tokenService: TokenService,
        private _httpClient: HttpClient,
        private _userActivityLogServiceProxy: UserActivityLogServiceProxy,
        private _userCallHistoryServiceProxy: UserCallHistoryServiceProxy,

    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  State Call History");
        this.uploadUrlstc = AppConsts.remoteServiceBaseUrl + '/Users/ImportEnfornicaCallDetailsFromExcel';

    }

    getStateWiseCallHistory(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._userCallHistoryServiceProxy.getAllStateWiseCallHistory(
            this.organizationUnit,
            0,
            this.locality,
            this.stateNameFilter,
            this.filterText,
            this.startDate,
            this.endDate,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            if (result.totalCount > 0) {
                this.totalQLD = result.items[0].summary.totalQLD;
                this.totalSA = result.items[0].summary.totalSA;
                this.totalNSW = result.items[0].summary.totalNSW;
                this.totalVIC = result.items[0].summary.totalVIC;
                this.totalOthers = result.items[0].summary.totalOther;
                this.totalCall = this.totalQLD + this.totalSA + this.totalNSW + this.totalVIC + this.totalOthers;
            }
            else {

                this.totalQLD = this.totalSA = this.totalNSW = this.totalVIC = this.totalOthers = this.totalCall = 0;
            }
        })
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote = 'Open State Wise Call History';
            log.section = 'State Wise Call History';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
                });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;

            this.getStateWiseCallHistory();
        });

        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });

        this._commonLookupService.getAlLocalityForTableDropdown().subscribe(result => {
            this.allLocality = result;
        });

    }

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        this.screenHeight = window.innerHeight;
    }

    testHeightSize() {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82;
        }
        else {
            this.testHeight = this.testHeight - -82;
        }
    }

    LocalitySearchResult: any[];
    filterLocality(event): void {
        this.LocalitySearchResult = Object.assign([], this.allLocality).filter(
            item => item.displayName.toLowerCase().indexOf(event.query.toLowerCase()) > -1
        )
    }

    selectLocality(event): void {
        this.locality = event.displayName;
        this.getStateWiseCallHistory();
    }

    uploadExcelEnfornicaCallDetails(data: { files: File }): void {

        var headers_object = new HttpHeaders().set("Authorization", "Bearer " + this._tokenService.getToken());

        const httpOptions = {
            headers: headers_object
        };
        const formData: FormData = new FormData();
        const file = data.files[0];
        formData.append('file' + ',' + this.organizationUnit, file, file.name);
        this._httpClient
            .post<any>(this.uploadUrlstc, formData, httpOptions)
            .pipe(finalize(() => this.excelFileUpload.clear()))
            .subscribe(response => {
                if (response.success) {
                    this.notify.success(this.l('ImportEnfornicaCallDetailsProcessStart'));
                } else if (response.error != null) {
                    this.notify.error(this.l('ImportEnfornicaCallDetailsUploadFailed'));
                }
            });
    }

    onUploadExcelErrorEnfornicaCallDetails(): void {
        this.notify.error(this.l('ImportSTCUploadFailed'));
    }

    exportToExcel(excelorcsv): void {

        this._userCallHistoryServiceProxy.getStateWiseCallHistoryToExcel(
            this.organizationUnit,
            0,
            this.locality,
            this.stateNameFilter,
            this.filterText,
            this.startDate,
            this.endDate,
            excelorcsv
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    onClear(): void {
        this.startDate = moment(this.date);
        this.endDate = moment(this.date);
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote = 'Searched by ' + filter;
        log.section = 'State Wise Call History';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
            });
    }

    RereshLog(): void {
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote = 'Refresh the data';
        log.section = 'State Wise Call History';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
            });
    }
}