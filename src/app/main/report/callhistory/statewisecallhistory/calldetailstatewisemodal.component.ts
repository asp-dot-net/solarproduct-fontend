﻿import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { UserCallHistoryServiceProxy, GetAllLocalityCountOfState, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { debug } from 'console';
import { FileDownloadService } from '@shared/utils/file-download.service';

@Component({
    selector: 'statecallDetailModal',
    templateUrl: './calldetailstatewisemodal.component.html'
})
export class statecallDetailModalComponent extends AppComponentBase {

    @ViewChild('statecallDetailModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
   ///  @ViewChild('TeamName') searchElement;
  //  @ViewChild('TeamName', { read: ElementRef }) searchElement:ElementRef;
    active = false;
    saving = false;
    localityData: GetAllLocalityCountOfState[];
    mobile = '';
    startDate : moment.Moment;
    endDate : moment.Moment;
    state = '';
    oId = 0;

    constructor(
        injector: Injector,
        private _userCallHistoryServiceProxy : UserCallHistoryServiceProxy,
        private _userActivityLogServiceProxy: UserActivityLogServiceProxy,
        private _fileDownloadService: FileDownloadService,

    ) {
        super(injector);
    }

    show(mobile : string, startDate : moment.Moment, endDate : moment.Moment, state : string, oId : number, section = '' ): void {
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='View '+ state +' - Locality Call History';
        log.section = section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
        this.mobile = mobile;
        this.startDate = startDate;
        this.endDate = endDate;
        this.oId = oId;
        this.state = state;
        this._userCallHistoryServiceProxy.getAllLocalityCountOfStateHistory(oId,0,'',state,mobile,startDate,endDate,'',0,10).subscribe(result => {
            this.localityData = result;
            this.modal.show();
        });
    }

    exportToExcel(): void {
        
        this._userCallHistoryServiceProxy.getAllLocalityCountOfStateHistoryExport(
            this.oId,0,'',this.state,this.mobile,this.startDate,this.endDate,'',0,10
        )
        .subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
        });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
