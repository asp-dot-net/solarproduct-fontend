import { Component, Injector, ViewEncapsulation, ViewChild, ElementRef, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { JobCostServiceProxy, OrganizationUnitDto, CommonLookupServiceProxy, JobStatusTableDto, GetAllJobCostForExcelInput, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
// import { EntityTypeHistoryModalComponent } from '@app/shared/common/entityHistory/entity-type-history-modal.component';
import * as _ from 'lodash';
import * as moment from 'moment';
import { OnInit } from '@angular/core';
// import { ViewApplicationModelComponent } from '../jobs/view-application-model/view-application-model.component';
// import { AddActivityModalComponent } from '@app/main/myleads/myleads/add-activity-model.component';
// import { JobgridSmsEmailModelComponent } from './jobgrid-sms-email-model/jobgrid-sms-email-model.component';
// import { ViewMyLeadComponent } from '@app/main/myleads/myleads/view-mylead.component';
import { Title } from '@angular/platform-browser';
import { ViewApplicationModelComponent } from '@app/main/jobs/jobs/view-application-model/view-application-model.component';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './employeeJobcost.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class EmployeeJobCostComponent extends AppComponentBase implements OnInit {

    show: boolean = true;
    FiltersData = false;
    isShowDivIf = true;  
    showchild: boolean = true;
    shouldShow: boolean = false;
    organizationUnitlength: number = 0;
    organizationUnit = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    ExpandedView: boolean = true;
    SelectedLeadId: number = 0;
    salesRapeList : any[];
    toggle: boolean = true;
    salesRap = '';
    
    @ViewChild('viewApplicationModal', { static: true }) viewApplicationModal: ViewApplicationModelComponent;

    change() {
        this.toggle = !this.toggle;
    }

    toggleDisplayDivIf() {  
        this.isShowDivIf = !this.isShowDivIf;  
      }      
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    public screenWidth: any;  
    public screenHeight: any;  
    // testHeight = 330;
    testHeight = 330;

    firstrowcount = 0;
    last = 0;

    jobDateFilter = 'InstallBook';
    date = new Date();

    alljobstatus: JobStatusTableDto[];
    jobStatus: [];
    jobCostInput = new GetAllJobCostForExcelInput();
    // filterText = 'AS821831';
    // firstDay = new Date(this.date.getFullYear() - 5, this.date.getMonth(), 1);

    filterText = '';
    firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    // startDate = moment(this.firstDay);
    // endDate = moment().endOf('day');
    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);
    totalProfit : number = 0;
    totalLoss : number = 0;

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    constructor(
        injector: Injector,
        private _jobCostServiceProxy: JobCostServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private titleService: Title
    ) {
        super(injector);
        // this.titleService.setTitle(this.appSession.tenancyName + " |  Job Cost");
        this.titleService.setTitle(this.appSession.tenancyName + " |  Employee Job Cost");
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 

        this._commonLookupService.getAllJobStatusTableDropdown().subscribe(result => {
            this.alljobstatus = result;
        });
        
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Employee Job Cost Report';
            log.section = 'Employee Job Cost Report';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.getJobs();
            this.getSalesRape();
        });
    }
 
    getSalesRape() : void{
        this._commonLookupService.getSalesRepForFilter(this.organizationUnit,undefined).subscribe(result => {
            this.salesRapeList = result;
        })
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  

    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }
        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }

    clear() {
        this.jobDateFilter = 'CreationDate';
        this.startDate = moment(this.date);
        this.endDate = moment(this.date);
        this.getJobs();
    }

    getJobs(event?: LazyLoadEvent) {
        debugger;
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();
        this._jobCostServiceProxy.getAll(
            this.organizationUnit,
            this.filterText,
            this.jobStatus,
            this.jobDateFilter,
            this.startDate,
            this.endDate,
            true,
            this.salesRap,
            0, // Cost Filter
            '', //Area Name Filter
            0, // System With Filter
            '',
            '',
            "",
            undefined,
            undefined,
            undefined,
            undefined,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
           
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            this.totalCount(event);
            this.shouldShow = false;
        },
        err => {
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    selectOrganizationUnit() : void{
        this.getSalesRape();
        this.getJobs();
    }

    totalCount(event): void{
        this.jobCostInput.organizationUnit = this.organizationUnit;
        this.jobCostInput.filter = this.filterText;
        this.jobCostInput.jobStatus = this.jobStatus;
        this.jobCostInput.dateType = this.jobDateFilter;
        this.jobCostInput.startDate = this.startDate;
        this.jobCostInput.endDate = this.endDate;
        this.jobCostInput.isEmpJobCost = true;
        this.jobCostInput.salesRap = this.salesRap;

        this._jobCostServiceProxy.totalCount(this.jobCostInput)
        .subscribe(result => {
            this.totalProfit = result.totalProfit;
            this.totalLoss = -(result.totalLoss)
        });
    }

    exportToExcel(excelorcsv): void {

        //let record = this.primengTableHelper.records;

        this._jobCostServiceProxy.getJobCostToExcel(
            this.organizationUnit,
            this.filterText,
            this.jobStatus,
            this.jobDateFilter,
            this.startDate,
            this.endDate,
            true,
            this.salesRap,
            0, // Cost Filter
            '', //Area Name Filter
            0, // System With Filter
            '',
            '',
            "",
            null,
            null,
            null,
            null
        )
        .subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
        });
    }

    salesRapSearchResult: any [];
    filterSalesRap(event): void {
        debugger;
        this.salesRapSearchResult = Object.assign([], this.salesRapeList).filter(
        item => item.displayName.toLowerCase().indexOf(event.query.toLowerCase()) > -1
        )
    }

    selectSalesRap(event): void {
        debugger;
        this.salesRap = event.displayName;
        this.getJobs();
    }

    
    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Employee Job Cost Report';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Employee Job Cost Report';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }

}