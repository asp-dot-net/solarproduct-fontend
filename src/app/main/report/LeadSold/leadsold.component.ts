import { Component, Injector, ViewEncapsulation, ViewChild, ElementRef, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
//import { callDetailModalComponent } from './call-detail-modal.component';

import { TeamsServiceProxy, TeamDto, CommonLookupServiceProxy, LeadSoldReportServiceProxy, OrganizationUnitDto, CommonLookupDto, LeadSourceLookupTableDto, LeadStateLookupTableDto, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';


@Component({
    templateUrl: './leadsold.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class LeadSoldReportComponent extends AppComponentBase implements OnInit {

    FiltersData = false;
    public screenWidth: any;  
    public screenHeight: any;  
    // testHeight = 330;
    testHeight = 330;
    firstrowcount = 0;
    last = 0;
    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    filterText = '';
    nameFilter = '';
    organizationUnit = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnitlength: number = 0;
    userFilter = 0;
    date = new Date();
    // firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    
    // startDate = moment(this.firstDay);
    // endDate = moment().endOf('day');
    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);
    userList : CommonLookupDto[];
    totalSold = 0;
    totalTV = 0 ;
    soldLeadCount = 0;
    leadAssignCount = 0;
    missingCount = 0;
    ratioCount = 0;
    ActualsoldLeadCount = 0;
    ActualleadAssignCount = 0;
    ActualmissingCount = 0;
    ActualratioCount = 0;
    leadsource: number[];
    allLeadSources: LeadSourceLookupTableDto[];
    leadSourceIdFilter = [];
    reportType = 1;
    teamId = 0;
    filteredTeams : any[];
    stateFilter = [];
    allStates: LeadStateLookupTableDto[];
    
    
    toggleBlock() {
        this.show = !this.show;
      };
      toggleBlockChild() {
        this.showchild = !this.showchild;
      };
      toggle: boolean = true;
    
      change() {
          this.toggle = !this.toggle;
        }

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    //@ViewChild('callDetailModal', { static: true }) callDetailModal: callDetailModalComponent;

    viewModel(): void {
        //this.callDetailModal.show();
    }

    constructor(
        injector: Injector,
        private _teamsServiceProxy: TeamsServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _leadSoldReportServiceProxy: LeadSoldReportServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private titleService: Title
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Lead Sold");
    }

    
    getLeadSold(event?: LazyLoadEvent){
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._leadSoldReportServiceProxy.getAll(
            this.organizationUnit,
            this.userFilter,
            this.startDate,
            this.endDate,
            this.leadSourceIdFilter,
            this.reportType,
            this.teamId,
            this.stateFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
            ).subscribe(result => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
                this.firstrowcount =  totalrows + 1;
                this.last = totalrows + result.items.length;
                if (result.totalCount > 0) {
                    this.leadAssignCount = result.items[0].summaryCount.leadAssignCount;
                    this.soldLeadCount = result.items[0].summaryCount.soldLeadCount;
                    this.ratioCount = result.items[0].summaryCount.ratioCount;
                    this.missingCount = result.items[0].summaryCount.missingCount;
                    this.ActualleadAssignCount = result.items[0].summaryCount.actualLeadAssignCount;
                    this.ActualsoldLeadCount = result.items[0].summaryCount.actualSoldLeadCount;
                    this.ActualratioCount = result.items[0].summaryCount.actualRatioCount;
                    this.ActualmissingCount = result.items[0].summaryCount.actualMissingCount;
                    //this.totalSold = this.totalTV + this.missingCount + this.ratioCount + this.soldLeadCount + this.leadAssignCount ;
                }
                else{
                    this.totalSold = this.totalTV = this.missingCount =  this.ratioCount = this.soldLeadCount = this.leadAssignCount = 0;
                }
                this.shouldShow = false;
                this.primengTableHelper.hideLoadingIndicator();
            })

    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
        this._commonLookupService.getAllLeadSourceDropdown().subscribe(result => {
            this.allLeadSources = result;
        });
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Lead Sold Report';
            log.section = 'Lead Sold Report';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            debugger;
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            
            this.onOrganizationUnitChange();
        });
        this._commonLookupService.getTeamForFilter().subscribe(teams => {
            this.filteredTeams = teams;
        });
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
          });
        
    }

    onOrganizationUnitChange(event?: LazyLoadEvent) : void{
        this._commonLookupService.getAllUsersByRoleNameTableDropdown('Sales Rep',this.organizationUnit).subscribe(result => {
            this.userList = result;
        });
        this.getLeadSold(event);
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  

    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }
        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }

    exportToExcel(excelorcsv): void {
        
        this._leadSoldReportServiceProxy.getLeadSoldToExcel(
            this.organizationUnit,
            this.userFilter,
            this.startDate,
            this.endDate,
            this.leadSourceIdFilter,
            this.reportType,
            excelorcsv,
            this.stateFilter
        )
        .subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
        });
    }

    clear() {
        this.filterText = '';
        this.leadSourceIdFilter = [];
        this.userFilter = 0;
        this.startDate = moment(this.date);
        this.endDate = moment(this.date);
        this.getLeadSold();
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Lead Sold Report';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Lead Sold Report';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }
    
}