import { Component, HostListener, Injector, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLookupServiceProxy, ComparisonServiceProxy, GetAllComparisonDto, LeadSourceLookupTableDto, LeadStateLookupTableDto, OrganizationUnitDto, UserActivityLogDto, UserActivityLogServiceProxy} from '@shared/service-proxies/service-proxies';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { result } from 'lodash';
import * as moment from 'moment';
import { LazyLoadEvent, Paginator, Table } from 'primeng';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './monthlycomparison.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class MonthlyComparisonReportComponent extends AppComponentBase {

  show: boolean = true;
  showchild: boolean = true;


  toggleBlock() {
    this.show = !this.show;
  };
  toggleBlockChild() {
    this.showchild = !this.showchild;
  };
  toggle: boolean = true;

  change() {
      this.toggle = !this.toggle;
    }
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;
  
  FiltersData = false;
  public screenWidth: any;  
  public screenHeight: any;  
  testHeight = 330;
  firstrowcount = 0;
  last = 0;
  filterText = '';
  date = new Date();
  maxEnddate = new Date();
  allOrganizationUnits: OrganizationUnitDto[];
  organizationid = 0;
  startDate: moment.Moment = moment(this.date);
  endDate: moment.Moment = moment(this.date);
  allLeadSources: LeadSourceLookupTableDto[];
  allStates: LeadStateLookupTableDto[];
  leadSourceIdFilter = [];
  stateFilter = [];
  areaFilter = [];
  shouldShow: boolean = false;
  data : GetAllComparisonDto = new GetAllComparisonDto();
  allAreas = [];
  allhederFeild = [];
  allheder = [];
  lead = 0;
  sold = 0
  installed = 0;
  expence = 0;
  colspan = 4;
  width= 550;
  logoWidth ='600px'
  constructor(
    injector: Injector,
    private _comparisonServiceProxy : ComparisonServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
    private titleService: Title
  ) {
    super(injector);
    this.titleService.setTitle(this.appSession.tenancyName + " |  Monthly Comparison Report");
    // this._commonLookupService.getTeamForFilter().subscribe(teams => {
    //   this.filteredTeams = teams;
    // // });

    // this._leadsServiceProxy.getUserByTeamId(this.teamId).subscribe(userList => {
    //   this.userlist = userList;
    // });


  }

  testHeightSize () {
    if (this.FiltersData == true) {
        this.testHeight = this.testHeight + -82 ;
    }
    else {
        this.testHeight = this.testHeight - -82 ;
    }
 }

  @HostListener('window:resize', ['$event'])  
  onResize(event) { 
      this.screenHeight = window.innerHeight;        
  }  

  ngOnInit(): void {
    debugger;
    this.screenHeight = window.innerHeight; 
    this.logoWidth= '600px';

    var CreateOrEditHeader1 = new LeadStateLookupTableDto;
    CreateOrEditHeader1.id = 1;
    CreateOrEditHeader1.displayName = "Lead";
    this.allhederFeild.push(CreateOrEditHeader1);
    var CreateOrEditHeader2 = new LeadStateLookupTableDto;
    CreateOrEditHeader2.id = 2;
    CreateOrEditHeader2.displayName = "KW Sold / Battery KW ";
    this.allhederFeild.push(CreateOrEditHeader2);
    var CreateOrEditHeader3 = new LeadStateLookupTableDto;
    CreateOrEditHeader3.id = 3;
    CreateOrEditHeader3.displayName = "KW Installed";
    this.allhederFeild.push(CreateOrEditHeader3);
    var CreateOrEditHeader4 = new LeadStateLookupTableDto;
    CreateOrEditHeader4.id = 4;
    CreateOrEditHeader4.displayName = "Expense";
    this.allhederFeild.push(CreateOrEditHeader4); 

    this._commonLookupService.getOrganizationUnit().subscribe(output => {
      let log = new UserActivityLogDto();
      log.actionId = 79;
      log.actionNote ='Open Monthly Comparison Report';
      log.section = 'Monthly Comparison Report';
      this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
          .subscribe(() => {
      });
      this.allOrganizationUnits = output;
      this.organizationid = this.allOrganizationUnits[0].id;

      this._commonLookupService.getAllLeadSourceDropdown().subscribe(result => {
        this.allLeadSources = result;
      });

      this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
        this.allStates = result;
      });

        this.allAreas = [];
        this.allAreas.push("Metro");
        this.allAreas.push("Regional");
        this.allAreas.push("NoArea");
      
      this.getAcivityList();
    });
  }
  getAcivityList(event?: LazyLoadEvent) {
   
    this.primengTableHelper.showLoadingIndicator();

     if (this.allheder.length > 0) {
      this.colspan = this.allheder.length;
      this.lead = 1;
      this.sold = 1;
      this.installed = 1;
      this.expence = 1;
    }
    else {
    this.logoWidth =  '650px';
    this.colspan = 4;

      this.lead = 0;
      this.sold = 0;
      this.installed = 0;
      this.expence = 0;
    }

    if (this.primengTableHelper.shouldResetPaging(event)) {
        this.paginator.changePage(0);
        return;
    }

    if(this.startDate && this.endDate){
        this._comparisonServiceProxy.getAllMonthly(this.organizationid,
            this.leadSourceIdFilter,
            this.stateFilter,
            this.areaFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
    
        })
    }
    else{
        this.hideMainSpinner();

    }
    this.width= 0;
    this.allheder.forEach(item => {
      if (item == 1) {
        this.lead = 0;
        this.width = this.width +100;
      }
      if (item == 2) {
        this.sold = 0;
        this.width = this.width +200;
      } 
      if (item == 3) {
        this.installed = 0;
        this.width = this.width +100;
      } 
      if (item == 4) {
        this.expence = 0;
        this.width = this.width +150;
      } 
  // this.primengTableHelper.hideLoadingIndicator();
    }) 
    debugger;
    if (this.allheder.length > 0) {
      this.logoWidth =  this.width.toString() + 'px';

    }
   

  }

  reloadPage(): void {
    this.paginator.changePage(this.paginator.getPage());
  }

  exportToExcel(excelorcsv): void {
    // let id = document.getElementById("tblrpt").innerHTML;
    // this.excelorcsvfile = excelorcsv
    //  this._leadExpensesServiceProxy.getAllexpenseTableForReportExport(this.startDate, this.endDate, this.states, this.leadSources, this.organizationid
    // )
    //   .subscribe(result => {
    //     this._fileDownloadService.downloadTempFile(result);
    //   });
  }

  addSearchLog(filter): void {
    let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Searched by '+ filter ;
        log.section = 'Monthly Comparison Report';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
}

RereshLog():void{
    let log = new UserActivityLogDto();
    log.actionId = 80;
    log.actionNote ='Refresh the data' ;
    log.section = 'Monthly Comparison Report';
    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {
    }); 

}

}