import { Component, Injector, OnInit, ViewChild, HostListener } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLookupServiceProxy, LeadsServiceProxy, LeadUsersLookupTableDto, OrganizationUnitDto, SystemReportsServiceProxy, UserActivityLogServiceProxy, UserServiceProxy } from '@shared/service-proxies/service-proxies';
import * as moment from 'moment';
import { LazyLoadEvent, Paginator, Table } from 'primeng';

@Component({
  selector: 'app-activityreport',
  templateUrl: './activityreport.component.html',
  styleUrls: ['./activityreport.component.css']
})
export class ActivityreportComponent extends AppComponentBase {

  show: boolean = true;
  showchild: boolean = true;
  public screenWidth: any;  
  public screenHeight: any;  
  testHeight = 330;
  FiltersData = false;  

  firstrowcount = 0;
  last = 0

  toggleBlock() {
    this.show = !this.show;
  };
  toggleBlockChild() {
    this.showchild = !this.showchild;
  };
  toggle: boolean = true;

  change() {
      this.toggle = !this.toggle;
    }

  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;

  filterText = '';
  date = new Date();
  // sampleDateRange: moment.Moment[] = [moment().add(0, 'days').endOf('day'), moment().add(0, 'days').endOf('day')];
  startDate: moment.Moment = moment(this.date);
  endDate: moment.Moment = moment(this.date);
  CopanyNameFilter = '';
  teamId = 0;
  userId = 0;
  dateNameFilter = '';
  // startDate: moment.Moment;
  // endDate: moment.Moment;
  filteredTeams: LeadUsersLookupTableDto[];
  userlist: LeadUsersLookupTableDto[];
  allOrganizationUnits: OrganizationUnitDto[];
  organizationid = 0;
  shouldShow: boolean = false;
  
  constructor(
    injector: Injector,
    private _systemReportsServiceProxy: SystemReportsServiceProxy,
    private _userActivityLogServiceProxy: UserActivityLogServiceProxy,
    private _commonLookupService: CommonLookupServiceProxy,
    private _leadsServiceProxy: LeadsServiceProxy,
    private _userServiceProxy: UserServiceProxy,
    private titleService: Title
  ) {
    super(injector);
    this.titleService.setTitle(this.appSession.tenancyName + " |  Lead Activity Detail");
    this._commonLookupService.getTeamForFilter().subscribe(teams => {
      this.filteredTeams = teams;
    });

    this._leadsServiceProxy.getUserByTeamId(this.teamId).subscribe(userList => {
      this.userlist = userList;
    });
    this._commonLookupService.getOrganizationUnit().subscribe(output => {
      this.allOrganizationUnits = output;
      this.organizationid = this.allOrganizationUnits[0].id;
      this.getAcivityList();
    });
  }
  getAcivityList(event?: LazyLoadEvent) {
    // this.startDate = this.sampleDateRange[0];
    // this.endDate = this.sampleDateRange[1];
    if (this.primengTableHelper.shouldResetPaging(event)) {
      this.paginator.changePage(0);
      return;
    }

    if (this.teamId != 0) {
      // this._leadsServiceProxy.getUserByTeamId(this.teamId).subscribe(userList => {
      //   this.userlist = userList;
      // });
    }
    this.primengTableHelper.showLoadingIndicator();

    // this._systemReportsServiceProxy.getAll(
    //   this.filterText,
    //   this.organizationid,
    //   undefined,
    //   this.teamId,
    //   this.userId,
    //   this.dateNameFilter,
    //   this.startDate,
    //   this.endDate,
    //   this.primengTableHelper.getSorting(this.dataTable),
    //   this.primengTableHelper.getSkipCount(this.paginator, event),
    //   this.primengTableHelper.getMaxResultCount(this.paginator, event)
    // ).subscribe(result => {
    //   this.primengTableHelper.totalRecordsCount = result.totalCount;
    //   this.primengTableHelper.records = result.items;
    //   const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
    //   this.firstrowcount = totalrows + 1;
    //   this.last = totalrows + result.items.length;
    //   this.primengTableHelper.hideLoadingIndicator();
    //   this.shouldShow = false;

    // });

    
    this._userActivityLogServiceProxy.getAll(
      this.startDate,
      this.endDate,
      this.userId,
      this.primengTableHelper.getSorting(this.dataTable),
      this.primengTableHelper.getSkipCount(this.paginator, event),
      this.primengTableHelper.getMaxResultCount(this.paginator, event)
    ).subscribe(result => {
      this.primengTableHelper.totalRecordsCount = result.totalCount;
      this.primengTableHelper.records = result.items;
      const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
      this.firstrowcount = totalrows + 1;
      this.last = totalrows + result.items.length;
      this.primengTableHelper.hideLoadingIndicator();
      this.shouldShow = false;

    });
  }

  ngOnInit(): void {
    this.screenHeight = window.innerHeight; 
  }
  @HostListener('window:resize', ['$event']) 
  reloadPage(): void {
    this.paginator.changePage(this.paginator.getPage());
  }

  @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  

    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }
        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }
    clear() {
      this.startDate =  moment(this.date);
      this.endDate =  moment(this.date);
      this.getAcivityList();
    }
  // exportToExcel(): void {

  // }

}
