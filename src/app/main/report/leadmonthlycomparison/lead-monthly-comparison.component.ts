import { Component, HostListener,Injector, ViewChild, ViewEncapsulation } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLookupServiceProxy, ComparisonServiceProxy, GetAllComparisonDto, LeadSourceLookupTableDto, LeadStateLookupTableDto, OrganizationUnitDto, UserActivityLogDto, UserActivityLogServiceProxy} from '@shared/service-proxies/service-proxies';
import { LazyLoadEvent, Paginator, Table } from 'primeng';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './lead-monthly-comparison.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class leadMonthlyComparison extends AppComponentBase {

  show: boolean = true;
  showchild: boolean = true;
  toggleBlock() {
    this.show = !this.show;
  };
  toggleBlockChild() {
    this.showchild = !this.showchild;
  };

  toggle: boolean = true;

  change() {
      this.toggle = !this.toggle;
    }
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;
  firstrowcount = 0;
  public screenHeight: any; 
  testHeight = 315;
  last = 0;
  filterText = '';
  allOrganizationUnits: OrganizationUnitDto[];
  organizationid = 0;

  allLeadSources: LeadSourceLookupTableDto[];
  allStates: LeadStateLookupTableDto[];
  leadSourceIdFilter = [];
  stateFilter = [];
  areaFilter = [];
  shouldShow: boolean = false;
  allAreas = [];
  allhederFeild = [];
  allHeaders = [];
  colspan = 4;
  allheder = [];
  lead = 0;
  sold = 0
  installed = 0;
  expence = 0;
  selectedMonths: number[] = [1,2,3];
  selectedYear: number = 2024;
  logoWidth ='600px'
  years: number[] = [];
  monthsList = [
    { id: 1, name: 'January' },
    { id: 2, name: 'February' },
    { id: 3, name: 'March' },
    { id: 4, name: 'April' },
    { id: 5, name: 'May' },
    { id: 6, name: 'June' },
    { id: 7, name: 'July' },
    { id: 8, name: 'August' },
    { id: 9, name: 'September' },
    { id: 10, name: 'October' },
    { id: 11, name: 'November' },
    { id: 12, name: 'December' }
];
monthlyTotals: { month: string, totalLead: number, panelSold_KW: number, batterySold_KW: number, installed_KW: number, spend: number }[] = [];
 monthNames = ["January", "February", "March", "April", "May", "June", 
  "July", "August", "September", "October", "November", "December"];
  constructor(
    injector: Injector,
    private _comparisonServiceProxy : ComparisonServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
    private titleService: Title
  ) {
    super(injector);
    this.titleService.setTitle(this.appSession.tenancyName + " |  Lead Monthly Comparison Report");
  }
  ngOnInit(): void {
    for (let year = 1999; year <= 3000; year++) {
      this.years.push(year);
  }

    var CreateOrEditHeader1 = new LeadStateLookupTableDto;
    CreateOrEditHeader1.id = 1;
    CreateOrEditHeader1.displayName = "Lead";
    this.allhederFeild.push(CreateOrEditHeader1);
    var CreateOrEditHeader2 = new LeadStateLookupTableDto;
    CreateOrEditHeader2.id = 2;
    CreateOrEditHeader2.displayName = "KW Sold / Battery KW ";
    this.allhederFeild.push(CreateOrEditHeader2);
    var CreateOrEditHeader3 = new LeadStateLookupTableDto;
    CreateOrEditHeader3.id = 3;
    CreateOrEditHeader3.displayName = "KW Installed";
    this.allhederFeild.push(CreateOrEditHeader3);
    var CreateOrEditHeader4 = new LeadStateLookupTableDto;
    CreateOrEditHeader4.id = 4;
    CreateOrEditHeader4.displayName = "Expense";
    this.allhederFeild.push(CreateOrEditHeader4);

    this.screenHeight = window.innerHeight; 
    this._commonLookupService.getOrganizationUnit().subscribe(output => {
      let log = new UserActivityLogDto();
      log.actionId = 79;
      log.actionNote ='Open Lead Monthly Comparison';
      log.section = 'Lead Monthly Comparison';
      this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
          .subscribe(() => {
      });
      this.allOrganizationUnits = output;
      this.organizationid = this.allOrganizationUnits[0].id;

      this._commonLookupService.getAllLeadSourceDropdown().subscribe(result => {
        this.allLeadSources = result;
      });

      this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
        this.allStates = result;
      });

        this.allAreas = [];
        this.allAreas.push("Metro");
        this.allAreas.push("Regional");
        this.allAreas.push("NoArea");
      
      this.getAcivityList();
    });

  }
 
@HostListener('window:resize', ['$event'])  
onResize(event) { 
    this.screenHeight = window.innerHeight;        
}
  getAcivityList(event?: LazyLoadEvent) {
    this.showMainSpinner();
    this.primengTableHelper.showLoadingIndicator();
     if (this.allheder.length > 0) {
      this.colspan = this.allheder.length;
      this.lead = 1;
      this.sold = 1;
      this.installed = 1;
      this.expence = 1;
    }
    else {
      this.colspan = 4;
      this.lead = 0;
      this.sold = 0;
      this.installed = 0;
      this.expence = 0;
    }
    if (this.primengTableHelper.shouldResetPaging(event)) {
        this.paginator.changePage(0);
        return;
    }
    
      this._comparisonServiceProxy.getAllLeadSourceMonthly(
        this.selectedYear,
        this.selectedMonths,
        this.organizationid,
        this.leadSourceIdFilter,
        this.stateFilter,
        this.areaFilter, 
        this.primengTableHelper.getSorting(this.dataTable),
        this.primengTableHelper.getSkipCount(this.paginator, event),
        this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
          debugger;
          const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
          // Extract unique months from compareValues
        const uniqueMonths = new Set<number>();
        result.items.forEach(item => {
            item.compareValues.forEach(value => {
                uniqueMonths.add(value.month); // Collect unique month numbers
            });
        });

        
        this.allHeaders = Array.from(uniqueMonths).sort((a, b) => a - b).map(month => monthNames[month - 1]);
      
        this.monthlyTotals = this.allHeaders.map(month => ({
          month: month,
          totalLead: 0,
          panelSold_KW: 0,
          batterySold_KW: 0,
          installed_KW: 0,
          spend: 0
      }));

      // Aggregate totals by month
      result.items.forEach(record => {
          record.compareValues.forEach(value => {
              const monthName = monthNames[value.month - 1];
              const monthTotal = this.monthlyTotals.find(m => m.month === monthName);

              if (monthTotal) {
                  monthTotal.totalLead += value.totalLead || 0;
                  monthTotal.panelSold_KW += value.panelSold_KW || 0;
                  monthTotal.batterySold_KW += value.batterySold_KW || 0;
                  monthTotal.installed_KW += value.installed_KW || 0;
                  monthTotal.spend += value.spend || 0;
              }
          });
      });


        this.primengTableHelper.totalRecordsCount = result.totalCount;
        
        this.primengTableHelper.records = result.items
        
        const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
        this.firstrowcount =  totalrows + 1;
        this.last = totalrows + result.items.length;
        this.primengTableHelper.hideLoadingIndicator();
        const firstRecord = result.items[0];
      
        this.hideMainSpinner();

         
       
    })
    


 
  
   

  }

  reloadPage(): void {
    this.paginator.changePage(this.paginator.getPage());
  }
  addSearchLog(filter): void {
    let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Searched by '+ filter ;
        log.section = 'Lead Monthly Comparison';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
}
RereshLog():void{
    let log = new UserActivityLogDto();
    log.actionId = 80;
    log.actionNote ='Refresh the data' ;
    log.section = 'Lead Monthly Comparison';
    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {
    }); 
}

}