import { Component, Injector, ViewEncapsulation, ViewChild, ElementRef, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { JobCostServiceProxy, OrganizationUnitDto, CommonLookupServiceProxy, JobStatusTableDto, JobCostSummary, GetAllJobCostViewDto, UserActivityLogServiceProxy, UserActivityLogDto, LeadStateLookupTableDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
// import { EntityTypeHistoryModalComponent } from '@app/shared/common/entityHistory/entity-type-history-modal.component';
import * as _ from 'lodash';
import * as moment from 'moment';
import { OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ViewApplicationModelComponent } from '@app/main/jobs/jobs/view-application-model/view-application-model.component';
import { CreateOrEditPostCodeWiseCostModal } from '@app/main/installation-cost/postcodecost/createedit-postcode-wise-cost.component';
import { CreateOrEditInstallationModalComponent } from '@app/main/installation-cost/installationItemList/createEdit-installation-item-modal.component';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './jobcost.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class JobCostComponent extends AppComponentBase implements OnInit {

    show: boolean = true;
    FiltersData = false;
    isShowDivIf = true;  
    showchild: boolean = true;
    shouldShow: boolean = false;
    organizationUnitlength: number = 0;
    organizationUnit = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    ExpandedView: boolean = true;
    SelectedLeadId: number = 0;

    toggle: boolean = true;

    change() {
        this.toggle = !this.toggle;
    }

    toggleDisplayDivIf() {  
        this.isShowDivIf = !this.isShowDivIf;  
      }      
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    public screenWidth: any;  
    public screenHeight: any;  
    // testHeight = 330;
    testHeight = 465;

    firstrowcount = 0;
    last = 0;

    jobDateFilter = 'InstallBook';
    curruntDateFilter = '';
    date = new Date();

    alljobstatus: JobStatusTableDto[];
    jobStatus: [];

    // filterText = 'AS821831';
    // firstDay = new Date(this.date.getFullYear() - 5, this.date.getMonth(), 1);

    filterText = '';
    // firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    // startDate = moment(this.firstDay);
    // endDate = moment().endOf('day');
    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);

    cost: any = '';
    areaNameFilter: string = "";
    systemWithFilter: any = 0;
    
    allStates: LeadStateLookupTableDto[];
    stateFilter: any = "";
    installationFilter: any = "";
    startSysCapRangeFilter: any;
    endSysCapRangeFilter: any;

    productItemName: any;
    productItemSuggestions: any;
    productItemId: any;
    // totalNoOfPanles: number = 0;
    // pricePerKw: any = 0.0;
    // np: any = 0;
    // gp: any = 0;
    // megaWatt: any = 0;
    summary: JobCostSummary = new JobCostSummary();
    category: string = "";

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('viewApplicationModal', { static: true }) viewApplicationModal: ViewApplicationModelComponent;
    @ViewChild('createOrEditPostCodeWiseCostModal', { static: true }) createOrEditCostModal: CreateOrEditPostCodeWiseCostModal;
    @ViewChild('createOrEditInstallationModal', { static: true }) createOrEditInstallationModal: CreateOrEditInstallationModalComponent;
    
    constructor(
        injector: Injector,
        private _jobCostServiceProxy: JobCostServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private titleService: Title
    ) {
        super(injector);
        // this.titleService.setTitle(this.appSession.tenancyName + " |  Job Cost");
        this.titleService.setTitle(this.appSession.tenancyName + " |  Job Cost");
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 

        this._commonLookupService.getAllJobStatusTableDropdown().subscribe(result => {
            this.alljobstatus = result;
        });
        
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Job Cost Report';
            log.section = 'Job Cost Report';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.getJobs();
        });

        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  

    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -225 ;
        }
        else {
            this.testHeight = this.testHeight - -225 ;
        }
    }

    clear() {
        this.jobDateFilter = 'CreationDate';

        this.areaNameFilter = "";
        this.systemWithFilter = 0;
        
        this.stateFilter = "",
        this.installationFilter = "",
        this.startSysCapRangeFilter = "",
        this.endSysCapRangeFilter = "",

        this.productItemId = undefined;
        this.productItemName = "";
        
        this.startDate =  moment(this.date);
        this.endDate =  moment(this.date);
        this.getJobs();
    }

    getJobs(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();
        this._jobCostServiceProxy.getAll(
            this.organizationUnit,
            this.filterText,
            this.jobStatus,
            this.jobDateFilter,
            this.startDate,
            this.endDate,
            false,
            '',
            this.cost,
            this.areaNameFilter,
            this.systemWithFilter,
            this.category,
            this.curruntDateFilter,
            this.stateFilter,
            this.installationFilter,
            this.startSysCapRangeFilter,
            this.endSysCapRangeFilter,
            this.productItemId,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;

            if(result.totalCount > 0)
            {
                this.summary = result.items[0].jobCostSummary;
            }

            this.primengTableHelper.hideLoadingIndicator();
            
            this.shouldShow = false;
            
        },
        err => {
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    exportToExcel(excelorcsv): void {

        //let record = this.primengTableHelper.records;

        this._jobCostServiceProxy.getJobCostToExcel(
            this.organizationUnit,
            this.filterText,
            this.jobStatus,
            this.jobDateFilter,
            this.startDate,
            this.endDate,
            false,
            '',
            this.cost,
            this.areaNameFilter,
            this.systemWithFilter,
            this.category,
            this.curruntDateFilter,
            this.stateFilter,
            this.installationFilter,
            this.startSysCapRangeFilter,
            this.endSysCapRangeFilter,
            this.productItemId
        )
        .subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
        });
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Job Cost Report';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Job Cost Report';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }

    // Called When Product Item Entered.
    filterProductIteams(event): void {
        this._commonLookupService.getAllProductItemForSearchable(event.query).subscribe(output => {
            this.productItemSuggestions = output;
        });
    }

    // Called When Product Item Selected.
    selectProductItem(event) {
        this.productItemName = event.displayName;
        this.productItemId = event.id;
    }

    onClearProductItem() {
        this.productItemId = undefined;
    }
}