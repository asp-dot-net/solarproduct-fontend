import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { UserCallHistoryServiceProxy, GetAllLocalityCountOfState, SmsCountReportServiceProxy, GetAllSmsCountSectionWiseDto, ProductSoldReportServiceProxy, UserActivityLogServiceProxy, UserActivityLogDto, JobVariationReportServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { debug } from 'console';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { result } from 'lodash';
import { Moment } from 'moment/moment';

@Component({
    selector: 'VariationTypePriceReportModel',
    templateUrl: './jobvariationTypeAmountState.component.html'
})
export class VariationTypePriceReportModelComponent extends AppComponentBase {

    @ViewChild('VariationTypePriceReportModel', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    ///  @ViewChild('TeamName') searchElement;
    //  @ViewChild('TeamName', { read: ElementRef }) searchElement:ElementRef;
    active = false;
    saving = false;
    //SectionWiseData : GetAllSmsCountSectionWiseDto[] =[];
    user: string;
    constructor(
        injector: Injector,
        private _jobVariationReportServiceProxy: JobVariationReportServiceProxy,
        private _userActivityLogServiceProxy: UserActivityLogServiceProxy,


    ) {
        super(injector);
    }
    result = [];
stateName = ''
    SectionName = '';
    show(JobNumber = '', sdate: moment.Moment, endDate: moment.Moment, state = [], variationIds = [], Oid = 0, section = ''): void {
        this.SectionName = section;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote = "Open For View Variation Type Wise Amount";
        log.section = section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
            });
        this.stateName = state[0]; 
        this._jobVariationReportServiceProxy.getVariationTypeAmountByState(
            JobNumber,
            sdate,
            endDate,
            variationIds,
            Oid,
            state,
            '',
            0,
            25)
            .subscribe(result => {
                this.result = result;
                this.modal.show();
            })


    }

    // exportToExcel(): void {

    //     this._userCallHistoryServiceProxy.getAllLocalityCountOfStateHistoryExport(
    //         this.oId,0,'',this.state,this.mobile,this.startDate,this.endDate,'',0,10
    //     )
    //     .subscribe(result => {
    //         this._fileDownloadService.downloadTempFile(result);
    //     });
    // }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
