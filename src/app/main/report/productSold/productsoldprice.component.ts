import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { UserCallHistoryServiceProxy, GetAllLocalityCountOfState, SmsCountReportServiceProxy, GetAllSmsCountSectionWiseDto, ProductSoldReportServiceProxy, UserActivityLogServiceProxy, UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { debug } from 'console';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { result } from 'lodash';
import { Moment } from 'moment/moment';

@Component({
    selector: 'productSoldPriceReportModel',
    templateUrl: './productsoldprice.component.html'
})
export class ProductSoldPriceReportModelComponent extends AppComponentBase {

    @ViewChild('productSoldPriceReportModel', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
   ///  @ViewChild('TeamName') searchElement;
  //  @ViewChild('TeamName', { read: ElementRef }) searchElement:ElementRef;
    active = false;
    saving = false;
    //SectionWiseData : GetAllSmsCountSectionWiseDto[] =[];
    user : string;
    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _productSoldReportServiceProxy : ProductSoldReportServiceProxy,
        // private _smsCountReportServiceProxy : SmsCountReportServiceProxy,
        // private _fileDownloadService: FileDownloadService,

    ) {
        super(injector);
    }

    productItem ='';
    nswMetro = 0;
    nswRegional = 0;
    nswTotal = 0;
    saMetro = 0;
    saRegional = 0;
    saTotal = 0;
    qldMetro = 0;
    qldRegional = 0;
    qldTotal = 0;
    vicMetro = 0;
    vicRegional = 0;
    vicTotal = 0;
    metroTotal = 0;
    regionalTotal = 0;
    total = 0;

  SectionName = '';
  show(id : number, productItem : string ,oid :number, userid : number, datetype : string, sdate : moment.Moment, endDate : moment.Moment,jobStatusIDFilter = [], producttype :number, filter : string, TeamId : number, systemWithFilter: any, areaNameFilter: string,section =''): void {
    this.SectionName = section;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote = "Open For View State Wise Product Sold";
        log.section = section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
    this._productSoldReportServiceProxy.getProductSoldPriceDto(
        oid,
        userid,
        datetype,
        sdate,
        endDate,
        jobStatusIDFilter,
        producttype,
        id,
        filter,
        TeamId,'0',
        systemWithFilter,
        areaNameFilter)
        .subscribe(result => {
            this.productItem = productItem;
            this.nswMetro = result.nswMetroPrice;
            this.nswRegional = result.nswRegionalPrice;
            this.nswTotal = (result.nswMetroPrice + result.nswRegionalPrice);
            this.saMetro = result.saMetroPrice;
            this.saRegional = result.saRegionalPrice;
            this.saTotal = (result.saMetroPrice + result.saRegionalPrice);
            this.qldMetro = result.qldMetroPrice;
            this.qldRegional = result.qldRegionalPrice;
            this.qldTotal = (result.qldMetroPrice + result.qldRegionalPrice);
            this.vicMetro = result.vicMetroPrice;
            this.vicRegional = result.vicRegionalPrice;
            this.vicTotal = (result.vicMetroPrice + result.vicRegionalPrice);

            this.metroTotal = (result.nswMetroPrice + result.saMetroPrice + result.qldMetroPrice + result.vicMetroPrice);
            this.regionalTotal = (result.nswRegionalPrice + result.saRegionalPrice + result.qldRegionalPrice + result.vicRegionalPrice);
            this.total = (this.nswTotal + this.saMetro + this.qldRegional + this.vicTotal);
           
            this.modal.show();
        })
        
        
    }

    // exportToExcel(): void {
        
    //     this._userCallHistoryServiceProxy.getAllLocalityCountOfStateHistoryExport(
    //         this.oId,0,'',this.state,this.mobile,this.startDate,this.endDate,'',0,10
    //     )
    //     .subscribe(result => {
    //         this._fileDownloadService.downloadTempFile(result);
    //     });
    // }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
