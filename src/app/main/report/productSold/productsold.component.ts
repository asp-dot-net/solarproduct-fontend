import { Component, Injector, ViewEncapsulation, ViewChild, ElementRef, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
//import { callDetailModalComponent } from './call-detail-modal.component';

import { TeamsServiceProxy, TeamDto, CommonLookupServiceProxy, LeadAssignReportServiceProxy, OrganizationUnitDto, CommonLookupDto, ProductSoldReportServiceProxy, JobStatusTableDto, ProductItemProductTypeLookupTableDto, LeadUsersLookupTableDto, UserActivityLogServiceProxy, UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { ProductSoldPriceReportModelComponent } from './productsoldprice.component';
import { finalize } from 'rxjs/operators';


@Component({
    templateUrl: './productsold.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ProductSoldReportComponent extends AppComponentBase implements OnInit {

    FiltersData = false;
    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 330;
    firstrowcount = 0;
    last = 0;
    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    filterText = '';
    nameFilter = '';
    organizationUnit = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnitlength: number = 0;
    date = new Date();
    // firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    
    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);
    userList : LeadUsersLookupTableDto[];
    totalNSW = 0;
    totalSA = 0;
    totalVIC = 0;
    totalQLD = 0;
    total = 0;
    teamId = 0;
    filterName = 'itemSearch';
    orgCode = "";
    filteredTeams: LeadUsersLookupTableDto[];

    areaNameFilter: string = "";
    systemWithFilter: any = 0;

    toggleBlock() {
        this.show = !this.show;
      };
      toggleBlockChild() {
        this.showchild = !this.showchild;
      };
      toggle: boolean = true;
    
      change() {
          this.toggle = !this.toggle;
        }

    datetype = 'FirstDeposite';
    jobStatus = 0;
    productItemId = 0;
    productTypeid = 0;
    userFilter = 0;
    alljobstatus: JobStatusTableDto[];
    allProductType : ProductItemProductTypeLookupTableDto[]; 
    jobStatusIDFilter = [];
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    //@ViewChild('callDetailModal', { static: true }) callDetailModal: callDetailModalComponent;
    @ViewChild('productSoldPriceReportModel', { static: true }) productSoldPriceReportModel: ProductSoldPriceReportModelComponent;

    viewModel(): void {
        //this.callDetailModal.show();
    }

    constructor(
        injector: Injector,
        private _teamsServiceProxy: TeamsServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _commonLookupService: CommonLookupServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _productSoldReportServiceProxy : ProductSoldReportServiceProxy,
        private titleService: Title
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Product Sold");
        this.userFilter = 0;

    }

    
    getLeadAssign(event?: LazyLoadEvent){
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._productSoldReportServiceProxy.getAll(
            this.organizationUnit,
            this.userFilter,
            this.datetype,
            this.startDate,
            this.endDate,
            this.jobStatusIDFilter,
            this.productTypeid,
            this.productItemId,
            this.filterText,
            this.teamId,
            this.systemWithFilter,
            this.areaNameFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
            ).subscribe(result => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
                this.firstrowcount =  totalrows + 1;
                this.last = totalrows + result.items.length;
                
                if (result.totalCount > 0) {
                    this.totalNSW = result.items[0].summary.sumNSW;
                    this.totalQLD = result.items[0].summary.sumQLD;
                    this.totalSA = result.items[0].summary.sumSA;
                    this.totalVIC = result.items[0].summary.sumVIC;
                    this.total = result.items[0].summary.totalSum;
                }
                else{
                    this.totalNSW = this.totalQLD = this.totalSA =  this.totalVIC = this.total = 0;
                }
                this.shouldShow = false;
                this.primengTableHelper.hideLoadingIndicator();
            })

    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
        this._commonLookupService.getTeamForFilter().subscribe(teams => {
            this.filteredTeams = teams;
        });
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Product Sold Report';
            log.section = 'Product Sold Report';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            
            this._commonLookupService.getAllJobStatusTableDropdown().subscribe(result => {
                this.alljobstatus = result;

                this._commonLookupService.getAllProductTypeForTableDropdown().subscribe(result => {
                    this.allProductType = result;
                });
                
            });

            this.onOrganizationUnitChange();
        });
        
        
    }

    onOrganizationUnitChange(event?: LazyLoadEvent) : void{
        this._commonLookupService.getSalesRepForFilter(this.organizationUnit, undefined).subscribe(rep => {
            this.userList = rep;
        });
        this.getLeadAssign();
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  

    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }
        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }

    exportToExcel(excelorcsv): void {
        
        this._productSoldReportServiceProxy.getProductSoldToExcel(
            this.organizationUnit,
            this.userFilter,
            this.datetype,
            this.startDate,
            this.endDate,
            this.jobStatusIDFilter,
            this.productTypeid,
            this.productItemId,
            this.filterText,
            this.teamId,
            excelorcsv,
            this.systemWithFilter,
            this.areaNameFilter
        )
        .subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
        });
    }

    viewmodel(id : number, name : string) :void{
        this.productSoldPriceReportModel.show(
            id,name,this.organizationUnit,this.userFilter,this.datetype,
            this.startDate,this.endDate,this.jobStatusIDFilter,
            this.productTypeid,this.filterText,this.teamId, this.systemWithFilter,this.areaNameFilter, 'Product Sold Report');
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Product Sold Report';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Product Sold Report';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }
    
}