
import { Component, Injector, ViewEncapsulation, ViewChild, OnInit, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ReviewReportDto,CommonLookupServiceProxy ,CommonLookupDto,OrganizationUnitDto,GetAllStockOrderOutputDto, StockOrderReceivedReportServiceProxy, ReportServiceProxy} from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { DomSanitizer, SafeHtml, Title } from '@angular/platform-browser';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';

@Component({
    templateUrl: './reviewreport.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ReviewReportComponent extends AppComponentBase implements OnInit {
   
    
    FiltersData = false;
    testHeight = 330;
    shouldShow: boolean = false;
    organizationUnitlength: number = 0;
    flag = true;
    show: boolean = true;
    showchild: boolean = true;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    toggle: boolean = true;
    showCancel: boolean = false;
    ExpandedView: boolean = true;
    change() {
        this.toggle = !this.toggle;
      }
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
   
   
    advancedFiltersAreShown = false;
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnit = 0;
    changeOrganization = 0;
    date = new Date();
    firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    StartDate = moment().startOf('month').add(1, 'days');
    EndDate = moment().add(0, 'days').endOf('day');
    dateFilterType  = undefined;
    filterText = '';
    
    filterName ="UserName";
    firstrowcount = 0;
    last = 0;
    summary: ReviewReportDto[];
    reviewType  :number = undefined;
    public screenWidth: any;  
    public screenHeight: any;  
    allReviewType: CommonLookupDto[];

    showACTCountColumn = true;
showNSWCountColumn = true;
showNTCountColumn = true;
showQLDCountColumn = true;
showSACountColumn = true;
showTASCountColumn = true;
showVICCountColumn = true;
showWACountColumn = true;
    constructor(
        injector: Injector,
        private _ReportServiceProxyRepository: ReportServiceProxy,
        
        private _commonLookupService: CommonLookupServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private titleService: Title,
       
    ) {
        super(injector);
        
    }
    searchLog() : void {
        
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Review Report';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
  
   
    ngOnInit(): void {
     
        this.screenHeight = window.innerHeight;
        this.titleService.setTitle(this.appSession.tenancyName + " | Review Report ");
        this._commonLookupService.getReviewTypeDropdown().subscribe(result => {
            this.allReviewType = result;
            this.getReviewReport();
        });
        
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Open Review Report';
        log.section = 'Review Report';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
         
    }

    expandGrid() {
        this.ExpandedView = true;
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    
    getReviewReport(event?: LazyLoadEvent) {
        debugger;
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
      
        this.primengTableHelper.showLoadingIndicator();

        this._ReportServiceProxyRepository.getAllReviewList(
            
            this.filterName,
            this.filterText,
            this.reviewType,
            this.organizationUnit,
            this.dateFilterType,
            this.StartDate,
            this.EndDate,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.summary=result.items;
            this.updateColumnVisibility(result.items);
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            
            
        });
    }
    updateColumnVisibility(records: any[]) {
        debugger
        this.showACTCountColumn = !records.every(record => record.acTcount == 0);
        this.showNSWCountColumn = !records.every(record => record.nsWcount == 0);
        this.showNTCountColumn = !records.every(record => record.nTcount == 0);
        this.showQLDCountColumn = !records.every(record => record.qlDcount == 0);
        this.showSACountColumn = !records.every(record => record.sAcount == 0);
        this.showTASCountColumn = !records.every(record => record.taScount == 0);
        this.showVICCountColumn = !records.every(record => record.viCcount == 0);
        this.showWACountColumn = !records.every(record => record.wAcount == 0);
    }
  
    

}
