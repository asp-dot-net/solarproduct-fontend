import { Component, ViewChild, Injector, Output, EventEmitter, OnInit, ElementRef, Directive, Optional } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { Paginator } from 'primeng/paginator';
import {
    InstallerInvoicePaymentDetailServiceProxy, LeadtrackerHistoryDto,
    InstallerinvoiceInstallerWiseSummary,
    UserActivityLogServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
    selector: 'InstallerInvoiceDetailmodal',
    templateUrl: './installerInvoicedetail-modal.component.html',
})
export class InstallerInvoiceDetailModalComponent extends AppComponentBase {
    @ViewChild("myNameElem") myNameElem: ElementRef;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @ViewChild('SampleDatePicker', { static: true }) sampleDatePicker: ElementRef;
    installer: String;

    InstallerinvoiceInstallerWiseSummary: InstallerinvoiceInstallerWiseSummary[];
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _installerInvoicePaymentReportComponent : InstallerInvoicePaymentDetailServiceProxy,
        private _router: Router,
        private _el: ElementRef

    ) {
        super(injector);
        this._el.nativeElement.setAttribute('autocomplete', 'off');
        this._el.nativeElement.setAttribute('autocorrect', 'off');
        this._el.nativeElement.setAttribute('autocapitalize', 'none');
        this._el.nativeElement.setAttribute('spellcheck', 'false');
    }

    show(installerId?: number,startDate? : any ,endDate?: any, AreaType? : string): void {

                // let log = new UserActivityLogDto();
                // log.actionId = 79;
                // log.actionNote ='Open Activity Log History';
                // log.section = section;
                // this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                //     .subscribe(() => {
                // }); 
       
            this._installerInvoicePaymentReportComponent.getAllInstallerInvoiceInstallerWise(installerId,startDate,endDate,AreaType
           ).subscribe(result => {
                this.InstallerinvoiceInstallerWiseSummary = result;
                this.installer=result[0].installer
                ;
               
            });
            this.modal.show();
        
    }
    close(): void {

        this.modal.hide();
    }
}
