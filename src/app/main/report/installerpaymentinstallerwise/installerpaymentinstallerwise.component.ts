import { Component, Injector, ViewEncapsulation, ViewChild, ElementRef, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { InstallerDeatilsComponent } from './installerdetails.component';
import{ InstallerInvoiceDetailModalComponent} from './installerInvoicedetail-modal.component';
import { TeamsServiceProxy, TeamDto, CommonLookupServiceProxy, LeadStateLookupTableDto, OrganizationUnitDto, CommonLookupDto, ProductSoldReportServiceProxy, JobStatusTableDto, ProductItemProductTypeLookupTableDto, LeadUsersLookupTableDto, SmsCountReportServiceProxy, InstallerInvoicePaymentDetailServiceProxy, CommonLookupInstaller, InstallerServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';


@Component({
    templateUrl: './installerpaymentinstallerwise.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class InstallerPaymentInstallerWiseReportComponent extends AppComponentBase implements OnInit {

    FiltersData = false;
    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 395;
    firstrowcount = 0;
    last = 0;
    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    filterText = '';
    nameFilter = '';
    organizationUnit = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnitlength: number = 0;
    date = new Date();
    // firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    areafilter = "";
    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);
    userList : LeadUsersLookupTableDto[];
  allStates: LeadStateLookupTableDto[];
    filteredTeams: LeadUsersLookupTableDto[];
    toggleBlock() {
        this.show = !this.show;
      };
      toggleBlockChild() {
        this.showchild = !this.showchild;
      };
      toggle: boolean = true;
    
      change() {
          this.toggle = !this.toggle;
        }
    state = '';
    datetype = 'FirstDeposite';
    jobStatus = 0;
    productItemId = 0;
    productTypeid = 0;
    userFilter = 0;
    alljobstatus: JobStatusTableDto[];
    allProductType : ProductItemProductTypeLookupTableDto[]; 
    jobStatusIDFilter = [];
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('InstallerInvoiceDetailmodal', { static: true }) InstallerInvoiceDetailmodal: InstallerInvoiceDetailModalComponent;
    filterName = 'JobNumber';
    orgCode = '';
    installerId: number;
    installername = "";
    installerInvoiceType : any[];
    invoiceTypeFilter = "";
    totalMetroKW = 0;
    totalMetroPrice = 0;
    totalMetroPricePerKW = 0;
    totalRegionalKW = 0;
    totalRegionalPrice = 0;
    totalRegionalPricePerKW = 0;
    @ViewChild('InstallerDetailmodal', { static: true }) InstallerDetailmodal: InstallerDeatilsComponent;
    // @ViewChild('SectionWiseSmsCountModel', { static: true }) SectionWiseSmsCountModel: SectionWiseSmsCountModelComponent;

    
    constructor(
        injector: Injector,
        private _teamsServiceProxy: TeamsServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _commonLookupService: CommonLookupServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _installerInvoicePaymentReportComponent : InstallerInvoicePaymentDetailServiceProxy,
        private titleService: Title,
        private _installerServiceProxy: InstallerServiceProxy
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Installer Wise Invoice Payment Report");
        this.userFilter = 0;

    }

    
    getInstallerInvoicePayment(event?: LazyLoadEvent){
        this.primengTableHelper.showLoadingIndicator();
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        

        this._installerInvoicePaymentReportComponent.getAllInstallerPaymentInstallerWise(
            this.startDate,
            this.endDate,
            this.invoiceTypeFilter,
            0,
            this.state,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
            ).subscribe(result => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
                this.firstrowcount =  totalrows + 1;
                this.last = totalrows + result.items.length;
                debugger;
                if (result.totalCount > 0) {
                    this.totalMetroKW = result.items[0].summary.metroKW;
                    this.totalMetroPrice = result.items[0].summary.metroPrice;
                    this.totalMetroPricePerKW = result.items[0].summary.metroPricePerKW;
                    this.totalRegionalKW = result.items[0].summary.regionalKW;
                    this.totalRegionalPrice = result.items[0].summary.regionalPrice;
                    this.totalRegionalPricePerKW = result.items[0].summary.regionalPricePerKW;
                }
                else{
                    this.totalMetroKW = this.totalMetroPrice = this.totalMetroPricePerKW = this.totalRegionalKW = this.totalRegionalPrice = this.totalRegionalPricePerKW = 0;
                }
                this.shouldShow = false;
                this.primengTableHelper.hideLoadingIndicator();
            })

    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
        this._commonLookupService.getAllInstallerInvoiceType().subscribe(result => {
            this.installerInvoiceType = result
          })
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Invoice Payment Report';
            log.section = 'Invoice Payment Report';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.orgCode = this.allOrganizationUnits[0].code;
            
            this._commonLookupService.getAllJobStatusTableDropdown().subscribe(result => {
                this.alljobstatus = result;

                this._commonLookupService.getAllProductTypeForTableDropdown().subscribe(result => {
                    this.allProductType = result;
                });
                
            });
            this.bindInstaller();
            this.getInstallerInvoicePayment();
            //this.onOrganizationUnitChange();
        });
        
        
    }
    selectInstaller(event): void {
        this.installerId = event.id;
        this.installername = event.installerName;
        this.getInstallerInvoicePayment();
    }
    // onOrganizationUnitChange(event?: LazyLoadEvent) : void{
    //     this._commonLookupService.getAllUsersTableDropdownForUserCallHistory(this.organizationUnit).subscribe(rep => {
    //         this.userList = rep;
    //     });
    //     this.getInstallerInvoicePayment();
    // }
    installerSearchResult: any [];
    filterInstaller(event): void {
        debugger;
        if(!event.query){
            this.installerId = 0;
            this.installername = "";
            this.getInstallerInvoicePayment();
        }
        else{

            this.installerSearchResult = Object.assign([], this.InstallerList).filter(
                item => item.installerName.toLowerCase().indexOf(event.query.toLowerCase()) > -1  || item.companyName?.toLowerCase().indexOf(event.query.toLowerCase())>-1
            )
        }
        
    }

    InstallerList: CommonLookupInstaller[];
    bindInstaller(){
        this.InstallerList = [];
        this._installerServiceProxy.getAllInstallersWithCompnayName(this.organizationUnit).subscribe(result => {
            this.InstallerList = result;
        });
    }
    InstallerDetailmodalmethod(installerId? : number){
      
   
        this.InstallerDetailmodal.show(installerId,this.startDate,this.endDate,'Invoice Payment Report');
    }
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  

    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }
        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }

    exportToExcel(): void {

        this._installerInvoicePaymentReportComponent.getInstallerPaymentInstallerWiseExport(
            this.startDate,
            this.endDate,
            this.invoiceTypeFilter,
            0,
            this.state,
            "",
            0,
            25
        )
        .subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
        });
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Invoice Payment Report';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    
    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Invoice Payment Report';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }

}