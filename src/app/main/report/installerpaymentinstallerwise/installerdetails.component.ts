import { Component, ViewChild, Injector, Output, EventEmitter, OnInit, ElementRef, Directive, Optional } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {
    InstallerInvoicePaymentDetailServiceProxy, GetAllInstallerPaymentInstallerJobWiseDto,
    UserActivityLogDto,
    UserActivityLogServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AgmCoreModule } from '@agm/core';
import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';
import PlaceResult = google.maps.places.PlaceResult;
import { Appearance, GermanAddress, Location } from '@angular-material-extensions/google-maps-autocomplete';

@Component({
    selector: 'InstallerDetailmodal',
    templateUrl: './installerdetails.component.html',
})
export class InstallerDeatilsComponent extends AppComponentBase {
    @ViewChild("myNameElem") myNameElem: ElementRef;
    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @ViewChild('SampleDatePicker', { static: true }) sampleDatePicker: ElementRef;
    // installerpaymentjobwise: GetAllInstallerPaymentInstallerJobWiseDto[];
    installerName: string = '';
    installerpaymentjobwise: any[] = []; 
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _installerInvoicePaymentReportComponent : InstallerInvoicePaymentDetailServiceProxy,
        private _router: Router,
        private _el: ElementRef

    ) {
        super(injector);
        this._el.nativeElement.setAttribute('autocomplete', 'off');
        this._el.nativeElement.setAttribute('autocorrect', 'off');
        this._el.nativeElement.setAttribute('autocapitalize', 'none');
        this._el.nativeElement.setAttribute('spellcheck', 'false');
    }

    show(Id?: number, startDate?: any, endDate?: any,section =''): void {
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='View Job Wise Detail Installer Invoice Payment';
        log.section = section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
      
        this._installerInvoicePaymentReportComponent.getAllInstallerPaymentInstallerJobWise(startDate, endDate, '', Id, '','', 0, 25)
         .subscribe(result => {
        this.installerpaymentjobwise = result.items; 
        if (this.installerpaymentjobwise.length > 0) {
            this.installerName = this.installerpaymentjobwise[0].installer;
        }
        this.modal.show();
      });
 
        
      }
    close(): void {

        this.modal.hide();
    }
}
