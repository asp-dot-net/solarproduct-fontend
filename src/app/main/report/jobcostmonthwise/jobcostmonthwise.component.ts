import { Component, Injector, ViewEncapsulation, ViewChild, ElementRef, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { JobCostServiceProxy, OrganizationUnitDto, CommonLookupServiceProxy, JobStatusTableDto, JobCostSummary, GetAllJobCostViewDto, LeadsServiceProxy, UserActivityLogServiceProxy, UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
// import { EntityTypeHistoryModalComponent } from '@app/shared/common/entityHistory/entity-type-history-modal.component';
import * as _ from 'lodash';
import * as moment from 'moment';
import { OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './jobcostmonthwise.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class JobCostMonthWiseComponent extends AppComponentBase implements OnInit {

    show: boolean = true;
    FiltersData = false;
    shouldShow: boolean = false;
    organizationUnitlength: number = 0;
    organizationUnit = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    ExpandedView: boolean = true;
    SelectedLeadId: number = 0;

    toggle: boolean = true;

    change() {
        this.toggle = !this.toggle;
    }

    toggleBlock() {
        this.show = !this.show;
    };

    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 248;

    firstrowcount = 0;
    last = 0;

    dateType = 'InstallBook';
    
    yearList = this.generateYearList();
    year = new Date().getFullYear();

    allStates = [];
    statefilter = [];

    allAreas = [];
    areaFilter = "";

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    
    constructor(
        injector: Injector,
        private _jobCostServiceProxy: JobCostServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy,
        private titleService: Title
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Job Cost Month Wise");
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  

    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }
        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
        
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });

        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Job Cost Month Wise Report';
            log.section = 'Job Cost Month Wise Report';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;

            this.getJobCost();
        });
    }

    clear() {
        this.statefilter = [];
        this.areaFilter = "";

        this.getJobCost();
    }

    getJobCost(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();
        this._jobCostServiceProxy.getAllJobCostMonthWise(
            this.organizationUnit,
            this.dateType,
            this.year,
            this.statefilter,
            this.areaFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            this.shouldShow = false;
            this.primengTableHelper.hideLoadingIndicator();
        }, 
        err => {
                this.primengTableHelper.hideLoadingIndicator();
            }
        );
    }

    exportToExcel(): void {

    // this._jobCostServiceProxy.getAllJobCostMonthWiseExcel(
    //     this.organizationUnit,
    //     this.jobDateFilter,
    //     // this.yearfilter,

    //     undefined,
    //     this.statefilter,
    //     this.areaNameFilter,
    //     this.leadsourcefilter,
    //     this.teamid,
    //     this.salesRepId
    //     )
    //         .subscribe(result => {
    //             this._fileDownloadService.downloadTempFile(result);
    //         });
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote = 'Searched by ' + filter;
        log.section = 'Job Cost Month Wise Report';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { })).subscribe(() => {}); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Job Cost Month Wise Report';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { })).subscribe(() => {}); 
    }
    
    generateYearList(): any {
        let list = [];
        let curruntYear = new Date().getFullYear();
        for (let index = 2018; index <= curruntYear; index++) {
            list.push(index);
        }

        return list;
    }
}