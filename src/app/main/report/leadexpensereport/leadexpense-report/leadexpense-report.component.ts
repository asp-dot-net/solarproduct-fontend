import { Component, Injector, OnInit, ViewChild ,Output, EventEmitter} from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLookupServiceProxy, LeadExpenseAddDto, LeadExpenseLeadSourceLookupTableDto, LeadExpenseReportDto, LeadExpensesServiceProxy, LeadSourceLookupTableDto, LeadsServiceProxy, LeadStateLookupTableDto, OrganizationUnitDto, SystemReportsServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy, UserServiceProxy } from '@shared/service-proxies/service-proxies';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as moment from 'moment';
import { LazyLoadEvent, Paginator, Table } from 'primeng';
import { finalize } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgxSpinnerTextService } from '@app/shared/ngx-spinner-text.service';
import { appModuleAnimation } from '@shared/animations/routerTransition';
@Component({
  selector: 'app-leadexpense-report',
  animations: [appModuleAnimation()],
  templateUrl: './leadexpense-report.component.html',
  styleUrls: ['./leadexpense-report.component.css']
})
export class LeadexpenseReportComponent extends AppComponentBase implements OnInit {
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @Output() reloadLead = new EventEmitter<boolean>();
  show: boolean = true;
  showchild: boolean = true;
  toggleBlock() {
    this.show = !this.show;
  };
  toggleBlockChild() {
    this.showchild = !this.showchild;
  };


  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;

  filterText = '';
  BatteryFilter = '';
  date = new Date();
  maxEnddate = new Date();
  firstDay = moment(new Date(this.date.getFullYear(), this.date.getMonth(), 1));
  lastday = moment().endOf('month');
  // sampleDateRange: moment.Moment[] = [this.firstDay, this.lastday];
  CopanyNameFilter = '';
  teamId = 0;
  excelorcsvfile = 0;
  userId = 0;
  dateNameFilter = '';
  // startDate = moment(new Date(this.date.getFullYear(), this.date.getMonth(), 1));
  // endDate = moment().endOf('month');
  allStates: LeadStateLookupTableDto[];
  allStatesheder: LeadStateLookupTableDto[];
  allhederFeild = [];
  leadstatedata: LeadExpenseReportDto[];
  states: number[];
  leadSources: [];
  allheder = [];
  dt = [];
  Spend = 0;
  Lead = 0;
  CostLead = 0;
  KwSold = 0;
  CostKW = 0;
  InstallKwSold = 0;
  CostInstallKW = 0;
  ProjOpen = 0;
  ProjOpenper = 0;
  DepRec = 0;
  DepRecper = 0;
  PanelSold = 0;
  CostPanel = 0;
  // filteredTeams: LeadUsersLookupTableDto[];
  // userlist: LeadUsersLookupTableDto[];
  allOrganizationUnits: OrganizationUnitDto[];
  allLeadSources: LeadSourceLookupTableDto[];
  allheader: LeadExpenseLeadSourceLookupTableDto[];
  organizationid = 0;
  currentyear = this.date.getFullYear();
  currentmonthyear: string;
  dateTypeFilter = 'DepositeReceived';
  startDate: moment.Moment = moment(this.date);
  endDate: moment.Moment = moment(this.date);
  loading: boolean = false;
  shouldShow: boolean = false;
  area = 'All'
  withBattery = false ;

  constructor(
    injector: Injector,
    private _systemReportsServiceProxy: SystemReportsServiceProxy,
    private _leadsServiceProxy: LeadsServiceProxy,
    private spinner: NgxSpinnerService,
    private _commonLookupService: CommonLookupServiceProxy,
    private _userServiceProxy: UserServiceProxy,
    private _fileDownloadService: FileDownloadService,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _leadExpensesServiceProxy: LeadExpensesServiceProxy,
    private titleService: Title
  ) {
    super(injector);
    this.titleService.setTitle(this.appSession.tenancyName + " |  Lead Expense Report");
    // this._commonLookupService.getTeamForFilter().subscribe(teams => {
    //   this.filteredTeams = teams;
    // // });

    // this._leadsServiceProxy.getUserByTeamId(this.teamId).subscribe(userList => {
    //   this.userlist = userList;
    // });


  }
  ngOnInit(): void {
    this._leadExpensesServiceProxy.getAllStateForTableDropdownForReport(this.dateTypeFilter,this.startDate, this.endDate, this.states, this.leadSources, this.organizationid, this.area,this.withBattery).subscribe(result => {
      this.allStates = result;
      var T = new LeadStateLookupTableDto();
      T.id = 0;
      T.displayName = "Total"
      this.allStates.push(T);
      this.states = [];
      this.states.push(0);

      this._leadExpensesServiceProxy.getAllStateForTableDropdownReportHeader(this.dateTypeFilter,this.startDate, this.endDate, this.states, this.leadSources, this.organizationid, this.area,this.withBattery).subscribe(result => {
        this.allStatesheder = []; 
        debugger;
        if(this.states){
  
          if(this.states.length > 0){
            this.allStatesheder = result;
    
          }
        }
        if(this.states.includes(0)){
          var CreateOrEditLead = new LeadStateLookupTableDto;
          CreateOrEditLead.id = 0;
          CreateOrEditLead.displayName = "Total";
    
          this.allStatesheder.push(CreateOrEditLead);
          console.log(this.allStatesheder);
        }
        
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
          let log = new UserActivityLogDto();
          log.actionId = 79;
          log.actionNote ='Open Lead Expense Report';
          log.section = 'Lead Expense Report';
          this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
              .subscribe(() => {
          });
          this.allOrganizationUnits = output;
          this.organizationid = this.allOrganizationUnits[0].id;
          this.getAcivityList();
        });

      });

    });
    
    
    this._commonLookupService.getAllLeadSourceForTableDropdown().subscribe(result => {
      this.allLeadSources = result;

    });


    var CreateOrEditHeader = new LeadStateLookupTableDto;
    CreateOrEditHeader.id = 1;
    CreateOrEditHeader.displayName = "Spend";
    this.allhederFeild.push(CreateOrEditHeader);
    var CreateOrEditHeader1 = new LeadStateLookupTableDto;
    CreateOrEditHeader1.id = 2;
    CreateOrEditHeader1.displayName = "Lead";
    this.allhederFeild.push(CreateOrEditHeader1);
    var CreateOrEditHeader2 = new LeadStateLookupTableDto;
    CreateOrEditHeader2.id = 3;
    CreateOrEditHeader2.displayName = "Cost/Lead";
    this.allhederFeild.push(CreateOrEditHeader2);
    var CreateOrEditHeader3 = new LeadStateLookupTableDto;
    CreateOrEditHeader3.id = 4;
    CreateOrEditHeader3.displayName = "Kw Sold";
    this.allhederFeild.push(CreateOrEditHeader3);
    var CreateOrEditHeader4 = new LeadStateLookupTableDto;
    CreateOrEditHeader4.id = 5;
    CreateOrEditHeader4.displayName = "Cost/KW";
    this.allhederFeild.push(CreateOrEditHeader4);
    var CreateOrEditHeader12 = new LeadStateLookupTableDto;
    CreateOrEditHeader12.id = 12;
    CreateOrEditHeader12.displayName = "Install Kw Sold";
    this.allhederFeild.push(CreateOrEditHeader12);
    var CreateOrEditHeader13 = new LeadStateLookupTableDto;
    CreateOrEditHeader13.id = 13;
    CreateOrEditHeader13.displayName = "Install Cost/KW";
    this.allhederFeild.push(CreateOrEditHeader13);
    var CreateOrEditHeader5 = new LeadStateLookupTableDto;
    CreateOrEditHeader5.id = 6;
    CreateOrEditHeader5.displayName = "Proj Open";
    this.allhederFeild.push(CreateOrEditHeader5);
    var CreateOrEditHeader6 = new LeadStateLookupTableDto;
    CreateOrEditHeader6.id = 7;
    CreateOrEditHeader6.displayName = "Proj Open per";
    this.allhederFeild.push(CreateOrEditHeader6);
    var CreateOrEditHeader7 = new LeadStateLookupTableDto;
    CreateOrEditHeader7.id = 8;
    CreateOrEditHeader7.displayName = "Cx Sold";
    this.allhederFeild.push(CreateOrEditHeader7);
    var CreateOrEditHeader8 = new LeadStateLookupTableDto;
    CreateOrEditHeader8.id = 9;
    CreateOrEditHeader8.displayName = "Cx Sold %";
    this.allhederFeild.push(CreateOrEditHeader8);
    var CreateOrEditHeader9 = new LeadStateLookupTableDto;
    CreateOrEditHeader9.id = 10;
    CreateOrEditHeader9.displayName = "Panel Sold";
    this.allhederFeild.push(CreateOrEditHeader9);
    var CreateOrEditHeader10 = new LeadStateLookupTableDto;
    CreateOrEditHeader10.id = 11;
    CreateOrEditHeader10.displayName = "Cost/Panel";
    this.allhederFeild.push(CreateOrEditHeader10);
    // this._leadExpensesServiceProxy.getAllStateForTableDropdownReportHeader().subscribe(result => {
    //   this.allheader = result;

    // });
    
  }
  getAcivityList(event?: LazyLoadEvent) {
    this.primengTableHelper.showLoadingIndicator();
    const monthNames = ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    ];
    this.showMainSpinner();
    const d = new Date();
    this.currentmonthyear = monthNames[d.getMonth()] + '-' + this.currentyear;
    this.shouldShow = false;
    // this._leadExpensesServiceProxy.leadExpenseExsistenceById(this.currentmonthyear).subscribe(result => {
    //   if (result.isexsistdata) {
    //     // this.startDate = result.fromDate;
    //     // this.endDate = result.toDate;
    //     this.shouldShow = false;
    //     // this.sampleDateRange = [this.startDate, this.endDate];
    //   } else {
    //     // this.startDate = this.sampleDateRange[0];
    //     // this.endDate = this.sampleDateRange[1];
    //   }
    //   this.maxEnddate = this.endDate.toDate();
    // });

    // if (this.primengTableHelper.shouldResetPaging(event)) {
    //   this.paginator.changePage(0);
    //   return;
    // }Spend = 0;
    if (this.allheder.length > 0) {
      this.Spend = 1;
      this.Lead = 1;
      this.CostLead = 1;
      this.KwSold = 1;
      this.CostKW = 1;
      this.ProjOpen = 1;
      this.ProjOpenper = 1;
      this.DepRec = 1;
      this.DepRecper = 1;
      this.PanelSold = 1;
      this.CostPanel = 1;
      this.InstallKwSold = 1;
      this.CostInstallKW = 1;
    }
    else {
      this.Spend = 0;
      this.Lead = 0;
      this.CostLead = 0;
      this.KwSold = 0;
      this.CostKW = 0;
      this.ProjOpen = 0;
      this.ProjOpenper = 0;
      this.DepRec = 0;
      this.DepRecper = 0;
      this.PanelSold = 0;
      this.CostPanel = 0;
      this.InstallKwSold = 0;
      this.CostInstallKW = 0;
    }
    if (this.teamId != 0) {
      // this._leadsServiceProxy.getUserByTeamId(this.teamId).subscribe(userList => {
      //   this.userlist = userList;
      // });
    }
    // this.spinner.show();

   // this.primengTableHelper.showLoadingIndicator();
    this.dt = [];
    // this.spinner.show();
    this._leadExpensesServiceProxy
      .getAllexpenseTableForReport(this.dateTypeFilter, this.startDate, this.endDate, this.states, this.leadSources, this.organizationid, this.area,this.withBattery)
      .subscribe(result => {
        
        this.leadstatedata = result;
        // let a =this.leadstatedata.length;
        // let ds= this.leadstatedata.splice(a-1,1).pop(); 
        //  console.log(this.leadstatedata);
        //   this.dt.push(ds);
        // this.leadSourceSourceName = result.leadSourceSourceName;
        // this.sampleDateRange.push(this.leadExpense.fromDate);
        // this.sampleDateRange.push(this.leadExpense.toDate); 

      });
    this._leadExpensesServiceProxy.getAllStateForTableDropdownReportHeader(this.dateTypeFilter,this.startDate, this.endDate, this.states, this.leadSources, this.organizationid, this.area,this.withBattery).subscribe(result => {
      this.allStatesheder = []; 
      this.hideMainSpinner();

      if(this.states){

        if(this.states.length > 0){
          this.allStatesheder = result;
          this.spinner.hide();
        }
      }
      if(this.states.includes(0)){
        var CreateOrEditLead = new LeadStateLookupTableDto;
        CreateOrEditLead.id = 0;
        CreateOrEditLead.displayName = "Total";
        this.allStatesheder.unshift(CreateOrEditLead);
        console.log(this.allStatesheder);
      }
      
    });
    this.allheder.forEach(item => {
      if (item == 1) {
        this.Spend = 0;

      }
      if (item == 2) {

        this.Lead = 0;

      } if (item == 3) {

        this.CostLead = 0;

      } if (item == 4) {

        this.KwSold = 0;

      } if (item == 5) {

        this.CostKW = 0;

      } if (item == 6) {
        this.ProjOpen = 0;


      }
      if (item == 7) {
        this.ProjOpenper = 0;
      }
      if (item == 8) {
        this.DepRec = 0;

      }
      if (item == 9) {
        this.DepRecper = 0;

      }
      if (item == 10) {
        this.PanelSold = 0;
      }
      if (item == 11) { this.CostPanel = 0; }
      if (item == 12) { this.InstallKwSold = 0; }
      if (item == 13) { this.CostInstallKW = 0; }
      // this.primengTableHelper.hideLoadingIndicator();
    })
    // this.spinner.hide();

  }

  reloadPage(): void {
    this.paginator.changePage(this.paginator.getPage());
  }

  exportToExcel(excelorcsv): void {
    let id = document.getElementById("tblrpt").innerHTML;
    this.excelorcsvfile = excelorcsv
     this._leadExpensesServiceProxy.getAllexpenseTableForReportExport(this.dateTypeFilter,this.startDate, this.endDate, this.states, this.leadSources, this.organizationid, this.area,this.withBattery
    )
      .subscribe(result => {
        this._fileDownloadService.downloadTempFile(result);
      });
  }

  addSearchLog(filter): void {
    let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Searched by '+ filter ;
        log.section = 'Lead Expense Report';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
}

RereshLog():void{
    let log = new UserActivityLogDto();
    log.actionId = 80;
    log.actionNote ='Refresh the data' ;
    log.section = 'Lead Expense Report';
    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {
    }); 
}

}