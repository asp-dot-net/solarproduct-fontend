import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeadexpenseReportComponent } from './leadexpense-report.component';

describe('LeadexpenseReportComponent', () => {
  let component: LeadexpenseReportComponent;
  let fixture: ComponentFixture<LeadexpenseReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeadexpenseReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeadexpenseReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
