import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetAllCallFlowQueueCountDetailsDto, GetAllCallQueueHistoryDto, LeadSourceDto, UserActivityLogDto, UserActivityLogServiceProxy, UserCallHistoryServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

import { appModuleAnimation } from '@shared/animations/routerTransition';
import * as _ from 'lodash';
import * as moment from 'moment';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { UserCsxCallQueueReportModalComponent } from '../callQueue/user-csx-call-queue-modal.component';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'CsxCallQueueUserWiseReportModal',
    templateUrl: './csx-call-queue-userwise-callqueue.component.html'
})
export class CsxCallQueueUserWiseReportModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('UserCsxCallQueueReportModal', { static: true }) UserCsxCallQueueReportModal: UserCsxCallQueueReportModalComponent;

    active = false;
    saving = false;
    organizationUnit = 0;

    item: GetAllCallQueueHistoryDto[];
  
    ExtentionNumber : number;
    startDate? : moment.Moment;
    endDate? : moment.Moment
    mobile ='';

    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
        ,private _userCallHistoryServiceProxy:UserCallHistoryServiceProxy
        ,private _fileDownloadService: FileDownloadService

    ) {
        super(injector);
    }

    show(extentionNumber :number ,orgId : number,startDate? : moment.Moment, endDate? : moment.Moment, mobile? : string,section = ''): void {
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='View Detail 3cx Call Queue History';
        log.section = section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
        this.ExtentionNumber = extentionNumber;
        this.startDate = startDate;
        this.endDate = endDate;
        this.organizationUnit = orgId;
        this.mobile = mobile;
        this._userCallHistoryServiceProxy.getCallQueueUserWiseDetailsCount(orgId, startDate, endDate, mobile,extentionNumber.toString()).subscribe(result => {
            this.item = result;
            this.modal.show();
        });
        this.active = true;
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    exportToExcel(): void {
        // this._userCallHistoryServiceProxy.getCallQueueDetailsCountExcel(
        //     this.organizationUnit, this.startDate, this.endDate, '', this.ExtentionNumber, this.mobile
        // )
        // .subscribe(result => {
        //     this._fileDownloadService.downloadTempFile(result);
        //  });
    }

   
}
