﻿import { Component, Injector, ViewEncapsulation, ViewChild, EventEmitter, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AddInstallerAvailabilityDto, CommonLookupDto, CommonLookupServiceProxy, GetInstallerAvailabilityDto, InstallerServiceProxy, OrganizationUnitDto, UserActivityLogDto, UserActivityLogServiceProxy, UserServiceProxy } from '@shared/service-proxies/service-proxies';
import { AbpSessionService, NotifyService, PermissionCheckerService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import * as _ from 'lodash';
import * as moment from 'moment';
import { LazyLoadEvent } from 'primeng/api';
import { CalendarOptions } from '@fullcalendar/angular';
import { CreateOrEditCancelReasonModalComponent } from '@app/main/cancelReasons/cancelReasons/create-or-edit-cancelReason-modal.component';
import { finalize } from 'rxjs/operators';
import { forEach } from 'lodash';
import { Title } from '@angular/platform-browser';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  templateUrl: './calenders.component.html',
  styleUrls: ['./calenders.component.less'],
  encapsulation: ViewEncapsulation.None,
  animations: [appModuleAnimation()]
})
export class CalendersComponent extends AppComponentBase {
  @ViewChild('createOrEditCancelReasonModal', { static: true }) createOrEditCancelReasonModal: CreateOrEditCancelReasonModalComponent;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  active = false;
  saving = false;
  installerId = 0;
  calendaruserid = 0;
  availablefalse = true;
  availabletrue = false;
  isChange = false;
  availableinstallerId = 0;
  documentStatus = 1;
  InstallerList: CommonLookupDto[];
  eventsData: GetInstallerAvailabilityDto[];
  installerAvailability: AddInstallerAvailabilityDto = new AddInstallerAvailabilityDto();
  calendarOptions: CalendarOptions = {
    headerToolbar: {
      left: 'prev,next title today',
      right: 'dayGridMonth,timeGridWeek,timeGridDay'// 'dayGridMonth,timeGridWeek,timeGridDay'
    },

    initialView: 'dayGridMonth',
    weekends: true,
    editable: true,
    selectable: true,
    selectMirror: true,
    dayMaxEvents: true,
    dateClick: this.handleDateClick.bind(this), // bind is important!
    select: this.handleDateSelect.bind(this),
    eventClick: this.handleEventClick.bind(this),
    eventsSet: this.handleEvents.bind(this),
    events: [],
  };

  calendarOptionsforcount: CalendarOptions = {
    headerToolbar: {
      left: 'prev,next title today',
      right: 'dayGridMonth,timeGridWeek,timeGridDay'// 'dayGridMonth,timeGridWeek,timeGridDay'
    },
    initialView: 'dayGridMonth',
    events: [],
    eventDisplay: 'list-item'

  };
  organizationUnit = 0;
  advancedFiltersAreShown = false;
  filterText = '';
  cancelReasonNameFilter = '';
  allOrganizationUnits: OrganizationUnitDto[];
  organizationUnitlength: number = 0;
  show = 0;

  toggle: boolean = true;

    change() {
        this.toggle = !this.toggle;
      }

 
  constructor(
    injector: Injector,
    private _sessionService: AbpSessionService,
    private _installerServiceProxy: InstallerServiceProxy,
    private _commonLookupService: CommonLookupServiceProxy,
    private _permissionChecker: PermissionCheckerService,
    private _notifyService: NotifyService,
    private _tokenAuth: TokenAuthServiceProxy,
    private _activatedRoute: ActivatedRoute,
    private titleService: Title,
    private _userServiceProxy: UserServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private spinner: NgxSpinnerService
  ) {
    super(injector);
    this.titleService.setTitle(this.appSession.tenancyName + " |  Calendar");
  }

  handleDateClick(arg) {
    this.saving = true;
    this.installerAvailability.availabilityDate = arg.date;
    var userid = this._sessionService.userId;
    if (this.installerId > 0) {
      userid = this.installerId;
      this.calendaruserid = this.installerId;
    }
    this.installerAvailability.userId = userid;
    let curDate = new Date();
    let myDate = new Date(curDate.getFullYear(), curDate.getMonth(), curDate.getDate());
    
    this._installerServiceProxy.addInstallerAvailability(this.installerAvailability)
      .pipe(finalize(() => { this.saving = false; }))
      .subscribe(result => {
        if (result == 1) {
          this.notify.info(this.l('SavedSuccessfully'));
          this.close();
          this.modalSave.emit(null);
          this.getEvents();
        } else {
          this.notify.warn("You Can Not Booked more than 2 jobs");
        }

      });
  }

  handleDateSelect(arg) {
    // alert('date click! ' + arg.dateStr)        
  }

  handleEventClick(arg) {
    //alert('date click! ' + arg.event.id);    
    this._installerServiceProxy.removeInstallerAvailability(arg.event.id)
      .subscribe(() => {
        this.notify.success(this.l('SuccessfullyDeleted'));
        this.getEvents();
      });
  }

  handleEvents(arg) {
    // alert('date click! ' + arg.dateStr)        
  }
  close(): void {
    this.active = false;
  }
  ngOnInit(): void {
    if (this._permissionChecker.isGranted('Pages.Tenant.InstallerManager.InstallerCalendar')) {
      this.availablefalse = true;
      this.org();
      this.getInstaller();
      
    }
    else {
      this.calendaruserid = this._sessionService.userId;
      this.availablefalse = true;
      this.getEvents();
    }

  }
  org() {
    this._commonLookupService.getOrganizationUnit().subscribe(output => {
      let log = new UserActivityLogDto();
      log.actionId = 79;
      log.actionNote ='Open Installation Calender';
      log.section = 'Installation Calender';
      this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
          .subscribe(() => {
      });
      this.allOrganizationUnits = output;
      this.organizationUnit = this.allOrganizationUnits[0].id;
      this.organizationUnitlength = this.allOrganizationUnits.length;
      this.getJobbookcount();
    });
  }

  getInstaller(): void {
    this._installerServiceProxy.getAllInstallers(this.organizationUnit).subscribe(result => {
      this.InstallerList = result;
    });
  };

  getEvents(): void {
    debugger;
    var userid = this._sessionService.userId;
    if (this.installerId > 0) {
      userid = this.installerId;
      this.calendaruserid = this.installerId;
    }
    this.showMainSpinner();
    this._installerServiceProxy.getAll(userid).subscribe(result => {
      this.eventsData = result;
      var eventlog = [];
      for (let item of result) {
        var edate = item.availabilityDate.format('YYYY-MM-DD');
        eventlog.push({ id: item.id, title: "", date: edate });
      }
      this.calendarOptions.events = eventlog;
      this.hideMainSpinner();
    }, e => {
      this.hideMainSpinner();
    });
  }

  getJobbookcount(): void {

    if (this.documentStatus == 1) {
      debugger;
      this.show = 1;

      var userid = this._sessionService.userId;
      if (this.installerId > 0) {
        userid = this.installerId;
        this.calendaruserid = this.installerId;
      } else {
        this.calendaruserid = 0;
      }
      this.showMainSpinner();
      this._installerServiceProxy.getAllJobBokingCount(0, this.calendaruserid, this.organizationUnit).subscribe(result => {
        this.eventsData = result;
        var eventlog = [];
        for (let item of result) {
          var edate = item.availabilityDate.format('YYYY-MM-DD');
          eventlog.push({ id: item.id, title: "Total Count :" + item.installationCount, date: edate });
        }
        this.calendarOptionsforcount.events = eventlog;
        this.hideMainSpinner();
      }, e => { this.hideMainSpinner(); });
    }
    else if (this.documentStatus == 2) {
      this.show = 2;
      this.getEvents()
    }

  }

  installerSearchResult: any [];
  filterInstaller(event): void {
    this.installerSearchResult = Object.assign([], this.InstallerList).filter(
      item => item.displayName.toLowerCase().indexOf(event.query.toLowerCase()) > -1
    )
  }

  selectInstaller(event): void {
    this.installerId = event.id;
    this.getJobbookcount();
  }

  addSearchLog(filter): void {
    let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Searched by '+ filter ;
        log.section = 'Installation Calender';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
}

}
