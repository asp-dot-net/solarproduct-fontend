import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { DepartmentsServiceProxy, CreateOrEditDepartmentDto, CreateOrEditServiceDocumentTypeDto, ServiceDocumentTypeServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'CreateOrEditServiceDocumentTypeModal',
    templateUrl: './create-or-edit-serviceDocumentType-modal.component.html'
})
export class CreateOrEditServiceDocumentTypeModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    ServiceDocumenttype: CreateOrEditServiceDocumentTypeDto = new CreateOrEditServiceDocumentTypeDto();



    constructor(
        injector: Injector,
        private _ServiceDocumenttypeServiceProxy: ServiceDocumentTypeServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }

    show(ServiceDocumenttypeId?: number): void {

        if (!ServiceDocumenttypeId) {
            this.ServiceDocumenttype = new CreateOrEditServiceDocumentTypeDto();
            this.ServiceDocumenttype.id = ServiceDocumenttypeId;

            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Service Document Type';
            log.section = 'Service Document Type';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._ServiceDocumenttypeServiceProxy.getDepartmentForEdit(ServiceDocumenttypeId).subscribe(result => {
                this.ServiceDocumenttype = result.createorEditServiceDocumentTypeDto;
                this.active = true;
                this.modal.show();
                
let log = new UserActivityLogDto();
log.actionId = 79;
log.actionNote ='Open For Edit Service Document Type : ' + this.ServiceDocumenttype.title;
log.section = 'Service Document Type';
this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
    .subscribe(() => {
});
            });
        }
    }

    onShown(): void {
        
        document.getElementById('Department_Name').focus();
    }

    save(): void {
        this.saving = true;


        this._ServiceDocumenttypeServiceProxy.createOrEdit(this.ServiceDocumenttype)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.ServiceDocumenttype.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Service Document Type Updated : '+ this.ServiceDocumenttype.title;
                    log.section = 'Service Document Type';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Service Document Type Created : '+ this.ServiceDocumenttype.title;
                    log.section = 'Service Document Type';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
            });
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
