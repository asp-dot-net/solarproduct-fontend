﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { UserTeamsServiceProxy, GetUserTeamForViewDto, UserTeamDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
    selector: 'viewUserTeam',
    templateUrl: './view-userTeam.component.html',
    animations: [appModuleAnimation()]
})
export class ViewUserTeamComponent extends AppComponentBase implements OnInit {

    active = false;
    saving = false;

    item: GetUserTeamForViewDto;


    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
         private _userTeamsServiceProxy: UserTeamsServiceProxy
    ) {
        super(injector);
        this.item = new GetUserTeamForViewDto();
        this.item.userTeam = new UserTeamDto();        
    }

    ngOnInit(): void {
        this.show(this._activatedRoute.snapshot.queryParams['id']);
    }

    show(userTeamId: number): void {
      this._userTeamsServiceProxy.getUserTeamForView(userTeamId).subscribe(result => {      
                 this.item = result;
                this.active = true;
            });       
    }
}
