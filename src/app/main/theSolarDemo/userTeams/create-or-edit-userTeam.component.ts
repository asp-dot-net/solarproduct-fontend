﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { UserTeamsServiceProxy, CreateOrEditUserTeamDto ,UserTeamUserLookupTableDto
					,UserTeamTeamLookupTableDto
					} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { ActivatedRoute } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
    selector: 'createOrEditUserTeam',
    templateUrl: './create-or-edit-userTeam.component.html',
    animations: [appModuleAnimation()]
})
export class CreateOrEditUserTeamComponent extends AppComponentBase implements OnInit {
    active = false;
    saving = false;
    
    userTeam: CreateOrEditUserTeamDto = new CreateOrEditUserTeamDto();

    userName = '';
    teamName = '';

	allUsers: UserTeamUserLookupTableDto[];
						allTeams: UserTeamTeamLookupTableDto[];
					
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,        
        private _userTeamsServiceProxy: UserTeamsServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.show(this._activatedRoute.snapshot.queryParams['id']);
    }

    show(userTeamId?: number): void {

        if (!userTeamId) {
            this.userTeam = new CreateOrEditUserTeamDto();
            this.userTeam.id = userTeamId;
            this.userName = '';
            this.teamName = '';

            this.active = true;
        } else {
            this._userTeamsServiceProxy.getUserTeamForEdit(userTeamId).subscribe(result => {
                this.userTeam = result.userTeam;

                this.userName = result.userName;
                this.teamName = result.teamName;

                this.active = true;
            });
        }
        this._userTeamsServiceProxy.getAllUserForTableDropdown().subscribe(result => {						
						this.allUsers = result;
					});
					this._userTeamsServiceProxy.getAllTeamForTableDropdown().subscribe(result => {						
						this.allTeams = result;
					});
					
    }

    save(): void {
            this.saving = true;

			
            this._userTeamsServiceProxy.createOrEdit(this.userTeam)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
             });
    }







}
