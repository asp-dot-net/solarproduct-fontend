﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { TeamsServiceProxy, CreateOrEditTeamDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { ActivatedRoute } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
    selector: 'createOrEditTeam',
    templateUrl: './create-or-edit-team.component.html',
    animations: [appModuleAnimation()]
})
export class CreateOrEditTeamComponent extends AppComponentBase implements OnInit {
    active = false;
    saving = false;

    team: CreateOrEditTeamDto = new CreateOrEditTeamDto();



    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _teamsServiceProxy: TeamsServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.show(this._activatedRoute.snapshot.queryParams['id']);
    }

    show(teamId?: number): void {

        if (!teamId) {
            this.team = new CreateOrEditTeamDto();
            this.team.id = teamId;

            this.active = true;
        } else {
            this._teamsServiceProxy.getTeamForEdit(teamId).subscribe(result => {
                this.team = result.team;


                this.active = true;
            });
        }

    }

    save(): void {
        if (this.team.name != null) {
            this.saving = true;
            this._teamsServiceProxy.createOrEdit(this.team)
                .pipe(finalize(() => { this.saving = false; }))
                .subscribe(() => {
                    this.notify.info(this.l('SavedSuccessfully'));
                });
        } else {
            this.notify.info(this.l('Please Enter Name'));
        }

    }







}
