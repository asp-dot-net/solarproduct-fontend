﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { TeamsServiceProxy, GetTeamForViewDto, TeamDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
    selector: 'viewTeam',
    templateUrl: './view-team.component.html',
    animations: [appModuleAnimation()]
})
export class ViewTeamComponent extends AppComponentBase implements OnInit {

    active = false;
    saving = false;

    item: GetTeamForViewDto;


    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
         private _teamsServiceProxy: TeamsServiceProxy
    ) {
        super(injector);
        this.item = new GetTeamForViewDto();
        this.item.team = new TeamDto();        
    }

    ngOnInit(): void {
        this.show(this._activatedRoute.snapshot.queryParams['id']);
    }

    show(teamId: number): void {
      this._teamsServiceProxy.getTeamForView(teamId).subscribe(result => {      
                 this.item = result;
                this.active = true;
            });       
    }
}
