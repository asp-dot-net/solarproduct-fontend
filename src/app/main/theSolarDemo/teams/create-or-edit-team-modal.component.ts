﻿import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { TeamsServiceProxy, CreateOrEditTeamDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
@Component({
    selector: 'createOrEditTeamModal',
    templateUrl: './create-or-edit-team-modal.component.html'
})
export class CreateOrEditTeamModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
   ///  @ViewChild('TeamName') searchElement;
  //  @ViewChild('TeamName', { read: ElementRef }) searchElement:ElementRef;
    active = false;
    saving = false;

    team: CreateOrEditTeamDto = new CreateOrEditTeamDto();



    constructor(
        injector: Injector,
        private _teamsServiceProxy: TeamsServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }

    show(teamId?: number): void {
        
        if (!teamId) {
            this.team = new CreateOrEditTeamDto();
            this.team.id = teamId;

            this.active = true;
           /// $('#name').focus();
         
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Sales Teams';
            log.section = 'Sales Teams';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            
            this._teamsServiceProxy.getTeamForEdit(teamId).subscribe(result => {
                this.team = result.team;
                this.active = true;
              ///  this.searchElement.nativeElement.focus();
               // alert("focus");
                this.modal.show();
                
let log = new UserActivityLogDto();
log.actionId = 79;
log.actionNote ='Open For Edit Sales Teams : ' + this.team.name;
log.section = 'Sales Teams';
this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
    .subscribe(() => {
});
            });
        }
        
    }

    save(): void {
            this.saving = true;

			
            this._teamsServiceProxy.createOrEdit(this.team)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.team.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Sales Teams Updated : '+ this.team.name;
                    log.section = 'Sales Teams';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Sales Teams Created : '+ this.team.name;
                    log.section = 'Sales Teams';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }

             });
    }

    onShown(): void {
        
        document.getElementById('Team_Name').focus();
    }






    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
