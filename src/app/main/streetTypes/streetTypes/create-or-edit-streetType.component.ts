﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { StreetTypesServiceProxy, CreateOrEditStreetTypeDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { ActivatedRoute } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
    selector: 'createOrEditStreetType',
    templateUrl: './create-or-edit-streetType.component.html',
    animations: [appModuleAnimation()]
})
export class CreateOrEditStreetTypeComponent extends AppComponentBase implements OnInit {
    active = false;
    saving = false;
    
    streetType: CreateOrEditStreetTypeDto = new CreateOrEditStreetTypeDto();



    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,        
        private _streetTypesServiceProxy: StreetTypesServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.show(this._activatedRoute.snapshot.queryParams['id']);
    }

    show(streetTypeId?: number): void {

        if (!streetTypeId) {
            this.streetType = new CreateOrEditStreetTypeDto();
            this.streetType.id = streetTypeId;

            this.active = true;
        } else {
            this._streetTypesServiceProxy.getStreetTypeForEdit(streetTypeId).subscribe(result => {
                this.streetType = result.streetType;


                this.active = true;
            });
        }
        
    }

    save(): void {
            this.saving = true;

			
            this._streetTypesServiceProxy.createOrEdit(this.streetType)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
             });
    }







}
