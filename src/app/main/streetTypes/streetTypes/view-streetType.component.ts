﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { StreetTypesServiceProxy, GetStreetTypeForViewDto, StreetTypeDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
    selector: 'viewStreetType',
    templateUrl: './view-streetType.component.html',
    animations: [appModuleAnimation()]
})
export class ViewStreetTypeComponent extends AppComponentBase implements OnInit {

    active = false;
    saving = false;

    item: GetStreetTypeForViewDto;


    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
         private _streetTypesServiceProxy: StreetTypesServiceProxy
    ) {
        super(injector);
        this.item = new GetStreetTypeForViewDto();
        this.item.streetType = new StreetTypeDto();        
    }

    ngOnInit(): void {
        this.show(this._activatedRoute.snapshot.queryParams['id']);
    }

    show(streetTypeId: number): void {
      this._streetTypesServiceProxy.getStreetTypeForView(streetTypeId).subscribe(result => {      
                 this.item = result;
                this.active = true;
            });       
    }
}
