﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { StreetTypesServiceProxy, CreateOrEditStreetTypeDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'createOrEditStreetTypeModal',
    templateUrl: './create-or-edit-streetType-modal.component.html'
})
export class CreateOrEditStreetTypeModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    streetType: CreateOrEditStreetTypeDto = new CreateOrEditStreetTypeDto();



    constructor(
        injector: Injector,
        private _streetTypeServiceProxy: StreetTypesServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
    }

    onShown(): void {
        document.getElementById('StreetType_Name').focus();
    } 

    show(streetTypeId?: number): void {

        if (!streetTypeId) {
            this.streetType = new CreateOrEditStreetTypeDto();
            this.streetType.id = streetTypeId;

            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Street Types';
            log.section = 'Street Types';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._streetTypeServiceProxy.getStreetTypeForEdit(streetTypeId).subscribe(result => {
                this.streetType = result.streetType;


                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Street Types : ' + this.streetType.name;
                log.section = 'Street Types';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        
    }

    save(): void {
            this.saving = true;

            this._streetTypeServiceProxy.createOrEdit(this.streetType)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.streetType.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Street Types Updated : '+ this.streetType.name;
                    log.section = 'Street Types';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Street Types Created : '+ this.streetType.name;
                    log.section = 'Street Types';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }







    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
