﻿import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute , Router} from '@angular/router';
import { StreetTypesServiceProxy, StreetTypeDto  } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { CreateOrEditStreetTypeModalComponent } from './create-or-edit-streetType-modal.component';
import { ViewStreetTypeModalComponent } from './view-streetType-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { Title } from '@angular/platform-browser';

@Component({
    templateUrl: './streetTypes.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class StreetTypesComponent extends AppComponentBase {
    
    show: boolean = true;
    showchild: boolean = true;
    toggleBlock(){
        this.show = !this.show;
    };
    toggleBlockChild(){
        this.showchild = !this.showchild;
    };
       
    @ViewChild('createOrEditStreetTypeModal', { static: true }) createOrEditStreetTypeModal: CreateOrEditStreetTypeModalComponent;
    @ViewChild('viewStreetTypeModalComponent', { static: true }) viewStreetTypeModal: ViewStreetTypeModalComponent;   
	
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    nameFilter = '';
    codeFilter = '';
    public screenHeight: any;  
    testHeight = 270;

    constructor(
        injector: Injector,
        private _streetTypesServiceProxy: StreetTypesServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _fileDownloadService: FileDownloadService,
			private _router: Router,
            private titleService: Title
            ) {
                super(injector);
                this.titleService.setTitle(this.appSession.tenancyName + " |  Street Types");
    }
    searchLog() : void {
        
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Street Types';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
       
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Street Types';
        log.section = 'Street Types';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
    }
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }
    getStreetTypes(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._streetTypesServiceProxy.getAll(
            this.filterText,
            this.nameFilter,
            this.codeFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createStreetType(): void {
		this.createOrEditStreetTypeModal.show();        
        // this._router.navigate(['/app/main/streetTypes/streetTypes/createOrEdit']);        
    }


    deleteStreetType(streetType: StreetTypeDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._streetTypesServiceProxy.delete(streetType.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            
let log = new UserActivityLogDto();
log.actionId = 83;
log.actionNote ='Delete Street Types: ' + streetType.name;
log.section = 'Street Types';
this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
    .subscribe(() => {
});
                        });
                }
            }
        );
    }

    exportToExcel(): void {
        this._streetTypesServiceProxy.getStreetTypesToExcel(
        this.filterText,
            this.nameFilter,
            this.codeFilter,
        )
        .subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
         });
    }
}
