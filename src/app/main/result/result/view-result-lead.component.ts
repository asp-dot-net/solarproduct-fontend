import { Component, ViewChild, Injector, Input, Output, EventEmitter, OnInit, Optional } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { LeadsServiceProxy, GetLeadForViewDto, LeadDto, GetActivityLogViewDto, GetLeadForChangeStatusOutput } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import * as moment from 'moment';
import { ActivityLogMyLeadComponent } from '@app/main/myleads/myleads/activity-log-mylead.component';
import { PromotionStopResponceComponent } from '@app/main/promotions/promotionUsers/stop-responce.component';
@Component({
    selector: 'viewResultLead',
    templateUrl: './view-result-lead.component.html',
    animations: [appModuleAnimation()]
})
export class ViewResultLeadComponent extends AppComponentBase implements OnInit {

    @Input() SelectedLeadId: number = 0;
    @Output() reloadLead = new EventEmitter();
    active = false;
    saving = false;
    @ViewChild('promoStopRespmodel', { static: true }) promot: PromotionStopResponceComponent;
    @ViewChild('activityLog', { static: true }) activityLogLead: ActivityLogMyLeadComponent;
    item: GetLeadForViewDto;
    activityLog: GetActivityLogViewDto[];
    showforresult = false;
    activeTabIndex: number = 0;
    activityDatabyDate: any[] = [];
    sectionId = 0;
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _router: Router,
    ) {
        super(injector);
        this.item = new GetLeadForViewDto();
        this.item.lead = new LeadDto();
    }

    ngOnInit(): void {
        //this.showDetail(this._activatedRoute.snapshot.queryParams['LeadId']);
    }

    goBack() {
        window.history.back();
    }

    showDetail(leadId: number, sectionId? : number): void {
       this.showMainSpinner();
        this.sectionId = sectionId;
        if (leadId != null) {
            let that = this;
            this._leadsServiceProxy.getLeadForView(leadId,0).subscribe(result => {
                this.item = result;
                this.hideMainSpinner();
            });
            this.activityLogLead.show(leadId, 9,0);
        }
    }
}
