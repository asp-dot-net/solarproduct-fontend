import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewApplicationModelComponent } from './view-application-model.component';

describe('ViewApplicationModelComponent', () => {
  let component: ViewApplicationModelComponent;
  let fixture: ComponentFixture<ViewApplicationModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewApplicationModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewApplicationModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
