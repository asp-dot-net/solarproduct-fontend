import { Component, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CreateOrEditJobDto, JobApplicationViewDto, JobsServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'viewDetailsModal',
  templateUrl: './view-model.component.html',
  styleUrls: ['./view-model.component.css']
})
export class ViewDetailsModelComponent extends AppComponentBase {
  @ViewChild('viewModel', { static: true }) modal: ModalDirective;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  active = false;
  jobid: number;
  finance = false;
  invoiceinstaller=0;
  applicationview: JobApplicationViewDto = new JobApplicationViewDto();
  job: CreateOrEditJobDto = new CreateOrEditJobDto();

  mapSrc: any = '';

  billToPay: number = 0;

  constructor(
    injector: Injector,
    private _jobsServiceProxy: JobsServiceProxy,
    private spinner: NgxSpinnerService
  ) {
    super(injector);
  }

  show(jobid: number): void {
    debugger;
    this.spinner.show();
    this._jobsServiceProxy.getDataForQuickViewByJobID(jobid,0).subscribe(result => {
      this.applicationview = result;
      this.invoiceinstaller=0;

      this.job = result.job;

      if (this.job.solarVICRebate != null && this.job.solarVICLoanDiscont != null) {
        let totalCostafterdic = (this.job.totalCost) - (this.job.solarVICRebate + this.job.solarVICLoanDiscont)
        if (this.job.depositRequired < 0) {
            this.job.depositRequired = Math.round(totalCostafterdic * 10 / 100);
        }
        this.billToPay = (totalCostafterdic) - (this.job.depositRequired);
      } else {
          if (this.job.depositRequired < 0) {
              this.job.depositRequired = Math.round(this.job.totalCost * 10 / 100);
          }
          this.billToPay = (this.job.totalCost) - (this.job.depositRequired);
      }

      if(this.applicationview.job.latitude != null && this.applicationview.job.longitude != null)
      {
        this.mapSrc = "https://maps.google.com/?q=" + this.applicationview.job.latitude + "," + this.applicationview.job.longitude
      }
      this.finance = true;

      
       this.modal.show();
       this.spinner.hide();
     });
  }
  downloadfile(file): void {
     debugger
    let FileName = AppConsts.docUrl + "/" + file.documentPath + file.fileName;
    window.open(FileName, "_blank");

  }
  downloadfile1(file): void {
    debugger
      let FileName = AppConsts.docUrl + "/" + file.filePathinv + file.filenameinv;
    window.open(FileName, "_blank");

  }
  downloadaprovefile(file): void {
    debugger
      let FileName = AppConsts.docUrl + "/" + file.gridFilePath + file.gridFilename;
    window.open(FileName, "_blank");

    // var date = moment(file.creationTime).toDate()
    //     var MigrationDate = new Date('2022/09/17')

    //     if(this.lead.organizationId == 8 || this.lead.organizationId == 7)
    //     {
    //         if(date < MigrationDate)
    //         {
    //             let FileName = AppConsts.oldDocUrl + "/" + file.documentPath + file.fileName;
    //             window.open(FileName, "_blank");
    //         }
    //     }
    //     else{
    //         let FileName = AppConsts.docUrl + "/" + file.documentPath + file.fileName;
    //         window.open(FileName, "_blank");
    //     }
  }

  close(): void {
    this.active = false;
    this.modal.hide();
  }
}
