import { Component, Injector, ViewEncapsulation } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DashboardCustomizationConst } from '@app/shared/common/customizable-dashboard/DashboardCustomizationConsts';
import { Title } from '@angular/platform-browser';
import { DatePipe } from '@angular/common';

import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';

@Component({
    templateUrl: './dashboard-kanban.component.html',
    styleUrls: ['./dashboard-kanban.component.less'],
    encapsulation: ViewEncapsulation.None 
})

export class Dashboard2Component extends AppComponentBase {
    constructor(
        injector: Injector,
        private titleService: Title) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Kanban Dashboard");
    }

    fleet = [
        {
          class:'blue',  
          item:'To Do',
          children:[
            { item: { itemTitle: "Job Booking", itemDesc: "Get the installation confirmation from installer.", itemUser: "RD", itemDate: "17 Jan" } },            
            { item: { itemTitle: "Installation Completed", itemDesc: "Installation will complete soon.", itemUser: "ND", itemDate: "16 Jan" } },            
            { item: { itemTitle: "New Promotions", itemDesc: "Special discount offer will come soon", itemUser: "ND", itemDate: "16 Jan" } },            
          ]
        },
        {
          class:'yellow',    
          item:'In Progress',
          children: [
            { item: { itemTitle: "Document Signed", itemDesc: "Waiting for customer signed in all related documents.", itemUser: "ND", itemDate: "16 Jan" } },            
            { item: { itemTitle: "Lead Generated", itemDesc: "New lead generation", itemUser: "RD", itemDate: "15 Jan" } },            
            { item: { itemTitle: "STC", itemDesc: "Waiting for STC process", itemUser: "RD", itemDate: "15 Jan" } },            
          ]
        },
        {
          class:'red',
          item: 'Review',
          children: [
            { item: { itemTitle: "Installer Updates", itemDesc: "Installer wants to visit the installation site first.", itemUser: "SP", itemDate: "17 Jan" } },            
            { item: { itemTitle: "Document Signed", itemDesc: "Need review for documents signed from customer", itemUser: "ND", itemDate: "17 Jan" } },            
          ]
        },
        {
            class:'green',
            item: 'Done',
            children: [
                { item: { itemTitle: "Job Booking", itemDesc: "Job booked succesfully.", itemUser: "RD", itemDate: "17 Jan" } },                            
            ]
          }
      ]

    todo = [
        'Job Booking',
        'Installation Completed',
        'Installer Update',
        'Site Visit by Installer',
        'New Promotions',
        
      ];

      progress = [
        'Site Visit by Installer',
        'Lead Generated',
        'New Promotions',        
        'STC',
      ];

      review = [
        'Installation Completed',
        'Installer Update',
        'Document Signed'
      ];
    
      done = [
        'Lead Generated',
        'Job Booking',
        'Document Signed'
      ];   

      drop1(event: CdkDragDrop<{}[]>){
        if(event.previousContainer == event.container){
          moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
        } else {
          transferArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
        }
        
        console.log(this.fleet);
      }
      name = 'Angular';
            
    
      drop(event: CdkDragDrop<string[]>) {
        if (event.previousContainer === event.container) {
          moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
        } else {
          transferArrayItem(event.previousContainer.data,
                            event.container.data,
                            event.previousIndex,
                            event.currentIndex);
        }
      }    


}
