import { Component, EventEmitter, Injector, OnInit, Output, ViewChild } from "@angular/core";
import { AppConsts } from "@shared/AppConsts";
import { AppComponentBase } from "@shared/common/app-component-base";
import { ActivityLogServiceProxy, CommonLookupDto, GetLeadForSMSEmailDto, GetLeadForSMSEmailTemplateDto, JobsServiceProxy, LeadsServiceProxy, SmsEmailDto, UserActivityLogDto, UserActivityLogServiceProxy } from "@shared/service-proxies/service-proxies";
import { ModalDirective } from "ngx-bootstrap/modal";
import { finalize } from "rxjs/operators";
import * as _ from 'lodash';

@Component({
    selector: 'smsModel',
    templateUrl: './sms-modal.component.html',
  })
  export class SMSModelComponent extends AppComponentBase implements OnInit {
    @ViewChild('LeadSMSModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    ExpandedViewApp: boolean = true;
    lead : GetLeadForSMSEmailDto = new GetLeadForSMSEmailDto();
    activityLog: SmsEmailDto = new SmsEmailDto();
    allSMSTemplates: CommonLookupDto[];
    active = false;    
    saving = false;
    role: string = '';
    total = 0;
    credit = 0;
    smsData = '';
    templateData : GetLeadForSMSEmailTemplateDto = new GetLeadForSMSEmailTemplateDto();
    header= "";
    ngOnInit(): void {
    }

    constructor(injector: Injector
        , private _activityLogServiceProxy : ActivityLogServiceProxy 
        , private _leadsServiceProxy : LeadsServiceProxy
        , private _userActivityLogServiceProxy : UserActivityLogServiceProxy
        , private _jobsServiceProxy : JobsServiceProxy
        ) {
        super(injector);
        this.lead = new GetLeadForSMSEmailDto();
        this.templateData = new GetLeadForSMSEmailTemplateDto();
    }

    sectionName = '';
    show(leadId : number, sectionId : number, Id = 0,section = '') {
      this.activityLog = new SmsEmailDto();
      this.lead = new GetLeadForSMSEmailDto();
      this.sectionName =section;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Open SMS';
        log.section = section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
      this.templateData = new GetLeadForSMSEmailTemplateDto();
        this.activityLog.leadId = leadId;
        this.activityLog.trackerId = sectionId;
        this.activityLog.body = "";
        this.activityLog.id = Id;
        this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
            this.role = result;
          });

        this._activityLogServiceProxy.getLeadForSMSEmailActivity(leadId).subscribe(result => {
            this.lead = result;
            this.header = result.companyName + (result.jobnumber != "" ? " - " + result.jobnumber : "")
            this.templateData = new GetLeadForSMSEmailTemplateDto();
        })

        this._leadsServiceProxy.getallSMSTemplates(leadId).subscribe(result => {
            this.allSMSTemplates = result;
          });
        
        this.modal.show();        

    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;                
    }
    expandApp() {
        this.ExpandedViewApp = false;
    }
    save(){
        this.saving = true;
      if (this.role != 'Admin') {
        if (this.total > 320) {
          this.notify.warn(this.l('You Can Not Add more than 320 characters'));
          this.saving = false;
        } else {
          this._activityLogServiceProxy.sendSms(this.activityLog)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
              let log = new UserActivityLogDto();
            log.actionId = 6;
            log.actionNote ='Sms Sent';
            log.section = this.sectionName;
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
              this.modal.hide();
              this.notify.info(this.l('SmsSendSuccessfully'));
              this.modalSave.emit(null);
              this.activityLog.body = "";
              this.activityLog.emailTemplateId = 0;
              this.activityLog.smsTemplateId = 0;
              this.activityLog.customeTagsId = 0;
              this.activityLog.subject = '';
            });
        }
      }
      else {
        this._activityLogServiceProxy.sendSms(this.activityLog)
          .pipe(finalize(() => { this.saving = false; }))
          .subscribe(() => {
            let log = new UserActivityLogDto();
            log.actionId = 6;
            log.actionNote ='Sms Sent';
            log.section = this.sectionName;
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.modal.hide();
            this.notify.info(this.l('SmsSendSuccessfully'));
            this.modalSave.emit(null);
            this.activityLog.body = "";
            this.activityLog.emailTemplateId = 0;
            this.activityLog.smsTemplateId = 0;
            this.activityLog.customeTagsId = 0;
            this.activityLog.subject = '';
          });
      }
      
    }

    countCharcters(): void {
        debugger;
        if (this.role != 'Admin') {
          this.total = this.activityLog.body.length;
          this.credit = Math.ceil(this.total / 160);
          if (this.total > 320) {
            this.notify.warn(this.l('You Can Not Add more than 320 characters'));
          }
        }
        else {
          this.total = this.activityLog.body.length;
          this.credit = Math.ceil(this.total / 160);
        }
    }

    smseditorLoaded() {

        this.activityLog.body = "";
        if ((this.activityLog.smsTemplateId != 0 && this.activityLog.smsTemplateId !== null && this.activityLog.smsTemplateId !== undefined)) {
          this._jobsServiceProxy.getSmsTemplateForEditForSms(this.activityLog.smsTemplateId).subscribe(result => {
            this.smsData = result.smsTemplate.text;
            if (this.smsData != "") {
              this.setsmsHTML(this.smsData)
            }
          });
        }
    
      }
    
    
      setsmsHTML(smsHTML) {
        debugger;
        this.showMainSpinner();
        if(!this.templateData.id || this.templateData.id == 0){
          this._activityLogServiceProxy.getLeadForSMSEmailTemplate(this.activityLog.leadId,0,0).subscribe(result => {
            this.templateData = result;
            let htmlTemplate = this.getsmsTemplate(smsHTML,result);
            this.activityLog.body = htmlTemplate;
            this.countCharcters();
            this.hideMainSpinner(); 
          }, err => {
              this.hideMainSpinner(); 
          });
        }
        else{
          let htmlTemplate = this.getsmsTemplate(smsHTML,this.templateData);
          this.activityLog.body = htmlTemplate;
          this.countCharcters();
          this.hideMainSpinner();
        }

       
      }
    
      getsmsTemplate(smsHTML,templateData : GetLeadForSMSEmailTemplateDto) {
        debugger;
        

        //let myTemplateStr = "Hello {{user.name}}, you are {{user.age.years}} years old";
        let myTemplateStr = smsHTML;
        //let addressValue = this.templateData.address + " " + this.item.templateData.suburb + ", " + this.item.templateData.state + "-" + this.item.templateData.postCode;
        let myVariables = {
          Customer: {
            Id : templateData.id,
            Name: templateData.companyName,
            Address: templateData.address,
            Mobile: templateData.mobile,
            Email: templateData.email,
            Phone: templateData.phone,
            SalesRep: templateData.currentAssignUserName
          },
          Sales: {
            Name: templateData.currentAssignUserName,
            Mobile: templateData.currentAssignUserMobile,
            Email: templateData.currentAssignUserEmail,
            CimetLink: templateData.currentAssignUserCimetLink
          },
          Quote: {
            ProjectNo: templateData.jobNumber,
            SystemCapacity: templateData.systemCapacity ,
            AllproductItem: templateData.qunityAndModelList,
            TotalQuoteprice: templateData.totalQuotaion,
            InstallationDate: templateData.installationDate,
            InstallerName: templateData.installerName,
          },
          Freebies: {
            DispatchedDate: templateData.dispatchedDate,
            TrackingNo: templateData.trackingNo,
            TransportCompany: templateData.transportCompanyName,
            TransportLink: templateData.transportLink,
            PromoType: templateData.promoType,
          },
          Invoice: {
            UserName: templateData.userName,
            UserMobile: templateData.userMobile,
            UserEmail: templateData.userEmail,
            Owning: templateData.owing,
          },
          Organization: {
            orgName: templateData.orgName,
            orgEmail: templateData.orgEmail,
            orgMobile: templateData.orgMobile,
            orglogo: AppConsts.docUrl + "/" + templateData.orglogo,
          },
          Review: {
            link: templateData.link,
           },
        }
    
        // use custom delimiter {{ }}
        _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;
    
        // interpolate
        let compiled = _.template(myTemplateStr);
        let myTemplateCompiled = compiled(myVariables);
        return myTemplateCompiled;
      }
  }