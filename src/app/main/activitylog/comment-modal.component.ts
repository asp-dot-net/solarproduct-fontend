import { Component, EventEmitter, Injector, OnInit, Output, ViewChild } from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import { ActivityLogServiceProxy, ActivitylogInput, GetLeadForActivityOutput, GetLeadForSMSEmailTemplateDto, UserActivityLogDto, UserActivityLogServiceProxy } from "@shared/service-proxies/service-proxies";
import { ModalDirective } from "ngx-bootstrap/modal";
import { finalize } from "rxjs/operators";

@Component({
    selector: 'commentModel',
    templateUrl: './comment-modal.component.html',
  })
  
export class CommentModelComponent extends AppComponentBase implements OnInit {
    @ViewChild('CommentModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    
    ngOnInit(): void {

    }

    ExpandedViewApp: boolean = true;

    lead : GetLeadForActivityOutput = new GetLeadForActivityOutput();
    activityLog : ActivitylogInput = new ActivitylogInput();
    saving = false;
    active = false;
    header = "";
    constructor(injector: Injector
        , private _activityLogServiceProxy : ActivityLogServiceProxy 
        , private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
        this.lead = new GetLeadForActivityOutput();
    }
    
    sectionName = '';
    show(leadId: number, sectionId:number, serviceId =0,section = '') {
        debugger
        this.showMainSpinner();
        this.activityLog = new ActivitylogInput();
        this.lead = new GetLeadForActivityOutput();
        this.sectionName =section;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Open Comment';
        log.section = section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
        this._activityLogServiceProxy.getLeadForActivity(leadId).subscribe(result => {
            this.lead = result;
            this.header = result.companyName + (result.jobNumber != "" ? " - " + result.jobNumber : "")
            this.activityLog = new ActivitylogInput();
            this.activityLog.leadId = leadId;
            this.activityLog.sectionId = sectionId;
            this.activityLog.activityNote = "";
            this.activityLog.serviceId = serviceId;
            this.modal.show(); 
             
            this.hideMainSpinner(); 
        }, err => {
            this.hideMainSpinner(); 
        });
         
    }

    save(){
        debugger;
        this.saving = true;
        this._activityLogServiceProxy.addCommentActivityLog(this.activityLog)
        .pipe(finalize(() => { this.saving = false; }))
        .subscribe(() => {
            let log = new UserActivityLogDto();
            log.actionId = 24;
            log.actionNote ='Comment';
            log.section = this.sectionName;
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.notify.info(this.l('SavedSuccessfully'));
            this.modal.hide();
            this.modalSave.emit(null);
        });
        
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }

    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;                
    }
    expandApp() {
        this.ExpandedViewApp = false;
    }
}