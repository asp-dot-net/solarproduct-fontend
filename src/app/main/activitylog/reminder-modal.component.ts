import { Component, ElementRef, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivityLogServiceProxy, ActivitylogInput,  GetLeadForActivityOutput,  GetLeadForSMSEmailTemplateDto, LeadsServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy, WholeSaleActivityServiceProxy, WholeSaleActivitylogInput, WholeSaleLeadServiceProxy } from '@shared/service-proxies/service-proxies';
import * as moment from 'moment';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'ReminderModal',
    templateUrl: './reminder-modal.component.html',
})
export class ReminderModalComponent extends AppComponentBase {
    @ViewChild('ReminderModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    
    // CurrentDate = new Date();

    ExpandedViewApp: boolean = true;
    active = false;    
    lead : GetLeadForActivityOutput = new GetLeadForActivityOutput();
    activityLog : ActivitylogInput = new ActivitylogInput();
    saving = false;   
    userName = '';
    header ='';
    constructor(injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
        , private _activityLogServiceProxy : ActivityLogServiceProxy   
        , private _leadsServiceProxy : LeadsServiceProxy     
        )
         {
            super(injector);
            this.lead = new GetLeadForActivityOutput();
            this.activityLog = new ActivitylogInput();
    }

    sectionName = '';
    show(leadId: number, sectionId:number,serviceId =0,section = '') {
      
        this.showMainSpinner();
        this.sectionName =section;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Open Reminder';
        log.section = section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
        this._activityLogServiceProxy.getLeadForActivity(leadId).subscribe(result => {
            this.lead = result;
            this.header = result.companyName + (result.jobNumber != "" ? " - " + result.jobNumber : "")

            this.activityLog = new ActivitylogInput();
            this.activityLog.leadId = leadId;
            this.activityLog.sectionId = sectionId;
            this.activityLog.serviceId = serviceId;
            this._leadsServiceProxy.getCurrentUserIdName().subscribe(result => {
                this.userName = result;
    
            } , err => {
                    this.hideMainSpinner(); 
            });
            this.modal.show(); 

            this.hideMainSpinner(); 
        }, err => {
            this.hideMainSpinner(); 
        });
                
    }

    close(): void {
        this.saving = false;
        this.modal.hide();
    }

    save(){
        debugger;
        this.saving = true;
        
        let date = moment(new Date(), "DD/MM/YYYY");

        let ActivityDate = moment(this.activityLog.activityDate, "DD/MM/YYYY");
        
        if (ActivityDate < date) {
            this.notify.warn(this.l('Please Select Date Greater Date than Current Date'));
            return;
        }

        this._activityLogServiceProxy.addReminderActivityLog(this.activityLog)
        .pipe(finalize(() => { this.saving = false; }))
        .subscribe(() => {
            let log = new UserActivityLogDto();
            log.actionId = 8;
            log.actionNote ='Reminder Set';
            log.section = this.sectionName;
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
          this.notify.info(this.l('SavedSuccessfully'));
          this.modal.hide();
          this.modalSave.emit(null);
        });
    }

    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;                
    }
    expandApp() {
        this.ExpandedViewApp = false;
    }
}
