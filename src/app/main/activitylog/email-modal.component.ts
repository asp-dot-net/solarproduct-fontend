import { Component, EventEmitter, Injector, OnInit, Output, ViewChild } from "@angular/core";
import { AppConsts } from "@shared/AppConsts";
import { AppComponentBase } from "@shared/common/app-component-base";
import { ActivityLogServiceProxy, CommonLookupDto, GetLeadForSMSEmailDto, GetLeadForSMSEmailTemplateDto, JobsServiceProxy, LeadsServiceProxy, SmsEmailDto, UserActivityLogDto, UserActivityLogServiceProxy } from "@shared/service-proxies/service-proxies";
import { ModalDirective } from "ngx-bootstrap/modal";
import { finalize } from "rxjs/operators";
import * as _ from 'lodash';
import { EmailEditorComponent } from "angular-email-editor";

@Component({
    selector: 'emailModel',
    templateUrl: './email-modal.component.html',
  })
  export class EmailModelComponent extends AppComponentBase implements OnInit {
    @ViewChild('LeadEmailModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    @ViewChild(EmailEditorComponent)
    private emailEditor: EmailEditorComponent;
    
    ExpandedViewApp: boolean = true;
    lead : GetLeadForSMSEmailDto = new GetLeadForSMSEmailDto();
    activityLog: SmsEmailDto = new SmsEmailDto();
    active = false;    
    saving = false;
    role: string = '';
    ccbox = false;
    bccbox = false;
    allEmailTemplates: CommonLookupDto[];
    fromemails: CommonLookupDto[];
    emailData = '';
    templateData : GetLeadForSMSEmailTemplateDto = new GetLeadForSMSEmailTemplateDto();
    header = "";
    installerinvoiceId =0;
    ngOnInit(): void {
    }

    constructor(injector: Injector
        , private _activityLogServiceProxy : ActivityLogServiceProxy 
        , private _leadsServiceProxy : LeadsServiceProxy
        , private _jobsServiceProxy : JobsServiceProxy
        , private _userActivityLogServiceProxy : UserActivityLogServiceProxy
        ,
        ) {
        super(injector);
        this.lead = new GetLeadForSMSEmailDto();
        this.templateData = new GetLeadForSMSEmailTemplateDto();
    }

    sectionName = '';
    show(leadId : number, sectionId : number, Id = 0,InvoiceId? :number,section = '') {
      this.showMainSpinner();
      this.sectionName =section;
      let log = new UserActivityLogDto();
      log.actionId = 79;
      log.actionNote ='Open Email';
      log.section = section;
      this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
          .subscribe(() => {
      });
      this.installerinvoiceId = InvoiceId;
      this.activityLog = new SmsEmailDto();
      this.lead = new GetLeadForSMSEmailDto();
      this.templateData = new GetLeadForSMSEmailTemplateDto();
      this.activityLog.leadId = leadId;
        this.activityLog.trackerId = sectionId;
        this.activityLog.body = "";
        this.activityLog.id = Id;
        this.activityLog.emailTemplateId = 0;
        this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
            this.role = result;
          },err => {
            this.hideMainSpinner(); 
        });

        this._activityLogServiceProxy.getLeadForSMSEmailActivity(leadId).subscribe(result => {
            this.lead = result;
            this.header = result.companyName + (result.jobnumber != "" ? " - " + result.jobnumber : "")
            this.templateData = new GetLeadForSMSEmailTemplateDto();
        },err => {
          this.hideMainSpinner(); 
      })

        this._leadsServiceProxy.getallEmailTemplates(leadId).subscribe(result => {
            this.allEmailTemplates = result;
        },err => {
          this.hideMainSpinner(); 
      });

        this._leadsServiceProxy.getOrgWiseDefultandownemailadd(leadId).subscribe(result => {
            debugger;
            this.fromemails = result;
            this.activityLog.emailFrom = this.fromemails[0].displayName;
        },err => {
          this.hideMainSpinner(); 
      });
      this.hideMainSpinner(); 
      this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;                
    }
    expandApp() {
        this.ExpandedViewApp = false;
    }
    save(){
        this.saving = true;
        this._activityLogServiceProxy.sendEmail(this.activityLog)
        .pipe(finalize(() => { this.saving = false; }))
        .subscribe(() => {
          let log = new UserActivityLogDto();
            log.actionId = 7;
            log.actionNote ='Email Sent';
            log.section = this.sectionName;
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
          this.notify.info(this.l('EmailSendSuccessfully'));
          this.modal.hide();
          this.modalSave.emit(null);
          this.activityLog.body = "";
          this.activityLog.emailTemplateId = 0;
          this.activityLog.smsTemplateId = 0;
          this.activityLog.customeTagsId = 0;
          this.activityLog.subject = '';
        });
    }

    onTagChange(event): void {

        const id = event.target.value;
        if (id == 1) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Customer.Name}}";
          } else {
            this.activityLog.body = "{{Customer.Name}}";
          }
    
        } else if (id == 2) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Customer.Mobile}}";
          } else {
            this.activityLog.body = "{{Customer.Mobile}}";
          }
        } else if (id == 3) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Customer.Phone}}";
          } else {
            this.activityLog.body = "{{Customer.Phone}}";
          }
        } else if (id == 4) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Customer.Email}}";
          } else {
            this.activityLog.body = "{{Customer.Email}}";
          }
        } else if (id == 5) {
    
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Customer.Address}}";
          } else {
            this.activityLog.body = "{{Customer.Address}}";
          }
        } else if (id == 6) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Sales.Name}}";
          } else {
            this.activityLog.body = "{{Sales.Name}}";
          }
        } else if (id == 7) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Sales.Mobile}}";
          } else {
            this.activityLog.body = "{{Sales.Mobile}}";
          }
        } else if (id == 8) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Sales.Email}}";
          } else {
            this.activityLog.body = "{{Sales.Email}}";
          }
        }
        else if (id == 9) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Quote.ProjectNo}}";
          } else {
            this.activityLog.body = "{{Quote.ProjectNo}}";
          }
        }
        else if (id == 10) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Quote.SystemCapacity}}";
          } else {
            this.activityLog.body = "{{Quote.SystemCapacity}}";
          }
        }
        else if (id == 11) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Quote.AllproductItem}}";
          } else {
            this.activityLog.body = "{{Quote.AllproductItem}}";
          }
        }
        else if (id == 12) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Quote.TotalQuoteprice}}";
          } else {
            this.activityLog.body = "{{Quote.TotalQuoteprice}}";
          }
        }
        else if (id == 13) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Quote.InstallationDate}}";
          } else {
            this.activityLog.body = "{{Quote.InstallationDate}}";
          }
        }
        else if (id == 14) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Quote.InstallerName}}";
          } else {
            this.activityLog.body = "{{Quote.InstallerName}}";
          }
        }
        else if (id == 15) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Freebies.TransportCompany}}";
          } else {
            this.activityLog.body = "{{Freebies.TransportCompany}}";
          }
        }
        else if (id == 16) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Freebies.TransportLink}}";
          } else {
            this.activityLog.body = "{{Freebies.TransportLink}}";
          }
        }
        else if (id == 17) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Freebies.DispatchedDate}}";
          } else {
            this.activityLog.body = "{{Freebies.DispatchedDate}}";
          }
        }
        else if (id == 18) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Freebies.TrackingNo}}";
          } else {
            this.activityLog.body = "{{Freebies.TrackingNo}}";
          }
        }
        else if (id == 19) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Freebies.PromoType}}";
          } else {
            this.activityLog.body = "{{Freebies.PromoType}}";
          }
        }
        else if (id == 20) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Invoice.UserName}}";
          } else {
            this.activityLog.body = "{{Invoice.UserName}}";
          }
        }
        else if (id == 21) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Invoice.UserMobile}}";
          } else {
            this.activityLog.body = "{{Invoice.UserMobile}}";
          }
        }
        else if (id == 22) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Invoice.UserEmail}}";
          } else {
            this.activityLog.body = "{{Invoice.UserEmail}}";
          }
        }
        else if (id == 23) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Invoice.Owning}}";
          } else {
            this.activityLog.body = "{{Invoice.Owning}}";
          }
        }
        else if (id == 24) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Organization.orgName}}";
          } else {
            this.activityLog.body = "{{Organization.orgName}}";
          }
        }
        else if (id == 25) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Organization.orgEmail}}";
          } else {
            this.activityLog.body = "{{Organization.orgEmail}}";
          }
        }
        else if (id == 26) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Organization.orgMobile}}";
          } else {
            this.activityLog.body = "{{Organization.orgMobile}}";
          }
        }
        else if (id == 27) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Organization.orglogo}}";
          } else {
            this.activityLog.body = "{{Organization.orglogo}}";
          }
        }
        else if (id == 28) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Review.link}}";
          } else {
            this.activityLog.body = "{{Review.link}}";
          }
        }        
        else if (id == 29) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Finance.PaymentOption}}";
          } else {
            this.activityLog.body = "{{Finance.PaymentOption}}";
          }
        }      
        else if (id == 30) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Finance.FinPaymentType}}";
          } else {
            this.activityLog.body = "{{Finance.FinPaymentType}}";
          }
        }      
        else if (id == 31) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Finance.DepositOption}}";
          } else {
            this.activityLog.body = "{{Finance.DepositOption}}";
          }
        }      
        else if (id == 32) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Finance.FinancePurchaseNo}}";
          } else {
            this.activityLog.body = "{{Finance.FinancePurchaseNo}}";
          }
        }      
        else if (id == 33) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Finance.FinanceAmount}}";
          } else {
            this.activityLog.body = "{{Finance.FinanceAmount}}";
          }
        }      
        else if (id == 34) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Finance.FinanceDepositeAmount}}";
          } else {
            this.activityLog.body = "{{Finance.FinanceDepositeAmount}}";
          }
        }      
        else if (id == 35) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Finance.FinanceNetAmount}}";
          } else {
            this.activityLog.body = "{{Finance.FinanceNetAmount}}";
          }
        }      
        else if (id == 36) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Finance.PaymentTerm}}";
          } else {
            this.activityLog.body = "{{Finance.PaymentTerm}}";
          }
        }      
        else if (id == 37) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Finance.RepaymentAmount}}";
          } else {
            this.activityLog.body = "{{Finance.RepaymentAmount}}";
          }
        }
        else if (id == 38) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Quote.InvoiceAmount}}";
          } else {
            this.activityLog.body = "{{Quote.InvoiceAmount}}";
          }
        }
        else if (id == 39) {
          if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Quote.ApproedAmount}}";
          } else {
            this.activityLog.body = "{{Quote.ApproedAmount}}";
          }
        }

      }
    
      saveDesign() {
        if (this.activityLog.emailTemplateId == 0) {
          this.saving = true;
          const emailHTML = this.activityLog.body;
          this.setHTML(emailHTML)
        }
        else {
          this.saving = true;
          this.emailEditor.editor.exportHtml((data) =>
            this.setHTML(data.html)
          );
        }
      }

      setHTML(emailHTML) {
        debugger;
        var ed = '';
        ed = emailHTML;
        if(!ed.includes('{{')){
          this.activityLog.body = emailHTML;
          this.save();
          return;
        }

        this.showMainSpinner();
              if(!this.templateData.id || this.templateData.id == 0){
                this._activityLogServiceProxy.getLeadForSMSEmailTemplate(this.activityLog.leadId,0,this.installerinvoiceId).subscribe(result => {
                  this.templateData = result;
                  let htmlTemplate = this.getEmailTemplate(emailHTML,result);
                  this.activityLog.body = htmlTemplate;
                  this.save();
                  this.hideMainSpinner(); 
                }, err => {
                    this.hideMainSpinner(); 
                });
              }
              else{
                let htmlTemplate = this.getEmailTemplate(emailHTML,this.templateData);
                this.activityLog.body = htmlTemplate;
                this.save();
                this.hideMainSpinner(); 
              }
        
        
      }
    
      getEmailTemplate(emailHTML,templateData : GetLeadForSMSEmailTemplateDto) {
        //let myTemplateStr = "Hello {{user.name}}, you are {{user.age.years}} years old";
        let myTemplateStr = emailHTML;
        //let addressValue = this.lead.address + " " + this.lead.suburb + ", " + this.lead.state + "-" + this.item.lead.postCode;
        let myVariables = {
          Customer: {
            Name: templateData.companyName,
            Address: templateData.address,
            Mobile: templateData.mobile,
            Email: templateData.email,
            Phone: templateData.phone,
            SalesRep: templateData.currentAssignUserName
          },
          Sales: {
            Name: templateData.currentAssignUserName,
            Mobile: templateData.currentAssignUserMobile,
            Email: templateData.currentAssignUserEmail
          },
          Quote: {
            ProjectNo: templateData.jobNumber,
            SystemCapacity: templateData.systemCapacity ,
            AllproductItem: templateData.qunityAndModelList,
            TotalQuoteprice: templateData.totalQuotaion,
            InstallationDate: templateData.installationDate,
            InstallerName: templateData.installerName,
            InvoiceAmount : templateData.invoiceAmount,
            ApproedAmount : templateData.approedAmount
          },
          Freebies: {
            DispatchedDate: templateData.dispatchedDate,
            TrackingNo: templateData.trackingNo,
            TransportCompany: templateData.transportCompanyName,
            TransportLink: templateData.transportLink,
            PromoType: templateData.promoType,
          },
          Invoice: {
            UserName: templateData.userName,
            UserMobile: templateData.userMobile,
            UserEmail: templateData.userEmail,
            Owning: templateData.owing,
          },
          Organization: {
            orgName: templateData.orgName,
            orgEmail: templateData.orgEmail,
            orgMobile: templateData.orgMobile,
            orglogo: AppConsts.docUrl + "/" + templateData.orglogo,
          },
          Review: {
           link: templateData.link,
          },
          Finance : {
            PaymentOption : templateData.paymentOption,
            FinPaymentType : templateData.finPaymentType,
            DepositOption : templateData.depositOption,
            FinancePurchaseNo : templateData.financePurchaseNo,
            FinanceAmount : templateData.financeAmount,
            FinanceDepositeAmount : templateData.financeDepositeAmount,
            FinanceNetAmount : templateData.financeNetAmount,
            PaymentTerm : templateData.paymentTerm,
            RepaymentAmount : templateData.repaymentAmount,

          }
        }
    
        // use custom delimiter {{ }}
        _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;
    
        // interpolate
        let compiled = _.template(myTemplateStr);
        let myTemplateCompiled = compiled(myVariables);
        return myTemplateCompiled;
      }
    
      editorLoaded() {

        this.activityLog.body = "";
        if ((this.activityLog.emailTemplateId != 0 && this.activityLog.emailTemplateId !== null && this.activityLog.emailTemplateId !== undefined)) {
          this._jobsServiceProxy.getEmailTemplateForEditForEmail(this.activityLog.emailTemplateId).subscribe(result => {
            this.activityLog.subject = result.emailTemplate.subject;
            this.emailData = result.emailTemplate.body;
            if (this.emailData != "") {
              this.showMainSpinner();
              if(!this.templateData.id || this.templateData.id == 0){
                this._activityLogServiceProxy.getLeadForSMSEmailTemplate(this.activityLog.leadId,0,this.installerinvoiceId).subscribe(result => {
                  this.templateData = result;
                  this.emailData = this.getEmailTemplate(this.emailData,result);
                  this.emailEditor.editor.loadDesign(JSON.parse(this.emailData));
                  this.hideMainSpinner(); 
                }, err => {
                    this.hideMainSpinner(); 
                });
              }
              else{
                this.emailData = this.getEmailTemplate(this.emailData, this.templateData);
                this.emailEditor.editor.loadDesign(JSON.parse(this.emailData));
                this.hideMainSpinner();
              }
              
              
            }
          });
        }
    
      }
      
      opencc(): void {
        this.ccbox = !this.ccbox;
      }
      openbcc(): void {
        this.bccbox = !this.bccbox;
      }
  }