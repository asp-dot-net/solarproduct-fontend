import { Component, Injector, ViewEncapsulation, EventEmitter, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { CalculateJobProductItemDeatilDto, CallHistoryServiceProxy, CommonLookupDto, CommonLookupServiceProxy, CreateJobDto, CreateOrEditAddressDto, EditJobAddressDto, EditSalesTabDto, ExistJobSiteAddDto, GetActivityLogViewDto, GetCallHistoryViewDto, GetLeadDetailsForOutput, GetLeadForChangeStatusOutput, JobDetails, JobJobTypeLookupTableDto, JobProductItemDto, JobsServiceProxy, LeadDetails, LeadDetailsServiceProxy, LeadStateLookupTableDto, LeadsServiceProxy, OrganizationUnitMapDto } from '@shared/service-proxies/service-proxies';
import { PromotionStopResponceComponent } from '../promotions/promotionUsers/stop-responce.component';
import { CreateEditLeadComponent } from '../leads/leads/create-edit-lead.component';
import { DatePipe } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';
import { SMSModelComponent } from '../activitylog/sms-modal.component';
import { EmailModelComponent } from '../activitylog/email-modal.component';
import { NotifyModelComponent } from '../activitylog/notify-modal.component';
import { CommentModelComponent } from '../activitylog/comment-modal.component';
import { ToDoModalComponent } from '../activitylog/todo-modal.component';
import { ReminderModalComponent } from '../activitylog/reminder-modal.component';
import PlaceResult = google.maps.places.PlaceResult;
import { Location } from '@angular-material-extensions/google-maps-autocomplete';
import { JobStatusModalComponent } from '../jobs/jobs/job-status.component';
import { JobActiveModalComponent } from '../jobs/jobs/check-job-active.component';
import { ApproveRejectCancelRequestComponent } from '../jobs/jobs/approve-reject-jobcancelreq.component';
import { ViewPackageModalComponent } from '../jobs/jobs/viewpackage.component';
import { ViewCalculatorModalComponent } from '../jobs/jobs/cal.component';


@Component({
    selector: 'leaddetails',
    templateUrl: './leaddetails.component.html',
    styleUrls: ['./leaddetails.component.css'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class LeadDetailsComponent extends AppComponentBase { 
    
    @ViewChild('promoStopRespmodel', { static: true }) promot: PromotionStopResponceComponent;
    @ViewChild('createEditLead', { static: true }) createEditLead: CreateEditLeadComponent;
    @Output() reloadLead = new EventEmitter<boolean>();

    @ViewChild('smsModel', { static: true }) smsModel: SMSModelComponent;
    @ViewChild('emailModel', { static: true }) emailModel: EmailModelComponent;
    @ViewChild('notifyModel', { static: true }) notifyModel: NotifyModelComponent;
    @ViewChild('commentModel', { static: true }) commentModel: CommentModelComponent;
    @ViewChild('todoModal', { static: true }) todoModal: ToDoModalComponent;
    @ViewChild('ReminderModal', { static: true }) ReminderModal: ReminderModalComponent;
    @ViewChild('jobStatusModal', { static: true }) jobStatusModal: JobStatusModalComponent;
    @ViewChild('jobActiveModal', { static: true }) jobActiveModal: JobActiveModalComponent;
    @ViewChild('cancelrequestapprovedreject', { static: true }) cancelrequestapprovedreject: ApproveRejectCancelRequestComponent;
    @ViewChild('viewPackegeModel', { static: true }) viewPackegeModel: ViewPackageModalComponent;
    @ViewChild('viewCalculatorModel', { static: true}) viewCalculatorModel: ViewCalculatorModalComponent;

    lead: GetLeadDetailsForOutput;

    currentUserRole: string;

    activeTabIndex: number = 0;
    sectionId: number;
    selectedLeadId: number;
    leadName: string
    serviceId : number = 0;

    OrganizationMapDto: OrganizationUnitMapDto[];
    mapSrc: string = "";
    selectedMap: string;
    lat: number;
    lng: number;

    activityType = 6;
    showsectionwise = false;
    actionId: number = 0;
    currentactivity: boolean = false;
    activity: boolean = false;
    allActivity: boolean = false;
    leadActivityList: GetActivityLogViewDto[];

    jobCancelReq: boolean = false;

    productTypes: CommonLookupDto[];

    constructor(
        injector: Injector,
        private readonly _leadDetailsServiceProxy : LeadDetailsServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy,
        private readonly _commonLookupService: CommonLookupServiceProxy,
        private datepipe: DatePipe,
        private readonly _callHistoryService: CallHistoryServiceProxy,
        private spinner: NgxSpinnerService,
        private _jobServiceProxy: JobsServiceProxy
    ) {
        super(injector);
        this.lead = new GetLeadDetailsForOutput();
        this.lead.leadDetails = new LeadDetails();
        this.lead.jobDetails = new JobDetails();
    }

    show(leadId: number, sectionId: number, leadName?: string, service : number = 0)
    {
        this.sectionId = sectionId;
        this.selectedLeadId = leadId;
        this.leadName = leadName;
        this.serviceId = service;
        this.changeAddress = false;
        
        this.changeJobAddress = false;
        
        this.leadActivityList = [];
        if (sectionId == 15) {
            this.activeTabIndex = 1;
        }
        else if (sectionId == 28 || sectionId == 22) {
            this.activeTabIndex = 1;
        } else {
            this.activeTabIndex = 0;
        }

        if (this.sectionId == 0 || this.sectionId > 12) {
            this.showsectionwise = true;
        } 
        if (this.sectionId == 30) {
            this.showsectionwise = false;
        } 

        if(this.sectionId == 23 || this.sectionId == 24 || this.sectionId == 25)
        {
            this.currentactivity =  true;
        }
        if (this.sectionId == 15) {
            this.actionId = 11;
        }
        else if (this.sectionId == 28) {
            this.actionId = 6;
        }

        this.showMainSpinner();

        this.lead = new GetLeadDetailsForOutput();
        this.lead.leadDetails = new LeadDetails();
        this.lead.jobDetails = new JobDetails();
        
        this._leadDetailsServiceProxy.getLeadDetailsByLeadId(leadId).subscribe(result => {

            this.lead = result;
            this.lng = parseFloat(result.leadDetails.longitude);
            this.lat = parseFloat(result.leadDetails.latitude);
            this.currentUserRole = result.currentUserRole;

            //Create Job Address
            this.suburb = this.lead.leadDetails.suburb;
            this.unitNo = this.lead.leadDetails.unitNo;
            this.unitType = this.lead.leadDetails.unitType;
            this.streetNo = this.lead.leadDetails.streetNo;
            this.streetName = this.lead.leadDetails.streetName;
            this.streetType = this.lead.leadDetails.streetType;
            this.suburb = this.lead.leadDetails.suburb;
            this.state = this.lead.leadDetails.state;
            this.postCode = this.lead.leadDetails.postCode;
            this.country = "AUSTRALIA";
            this.jobLatitude = this.lead.leadDetails.latitude;
            this.jobLongitude = this.lead.leadDetails.longitude;

            if(this.OrganizationMapDto == null)
            {
                this._commonLookupService.getOrganizationMapById(result.leadDetails.organizationId).subscribe((result) => {
                    this.OrganizationMapDto = result;
                });
            }
            this.selectedMap = 'GoogleMap';
            if(this.selectedMap != 'GoogleMap')
            {
                this.bindMap();
            }
            
            if (this.lead.currentUserRole == 'Sales Rep') {
                this.activity = true;
            }

            //Job Details
            if(this.lead.jobDetails)
            {
                this.isJob = false;
            }
            else
            {
                this.isJob = true;
            }

            if(this.lead.leadDetails.streetNo != '' && this.lead.leadDetails.streetName != '' && this.lead.leadDetails.streetType != '' && this.lead.leadDetails.suburb != '' && this.lead.leadDetails.state != '' && this.lead.leadDetails.postCode != '') {
                this.isVarify = true;
            }
            else {
                this.isVarify = false;
            }

            if(this.jobTypes == null)
            {
                this._commonLookupService.getAllJobTypeForTableDropdown().subscribe(result => {
                    this.jobTypes = result;
                });
            }

            if(this.allStates == null)
            {
                this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
                    this.allStates = result;
                });
            }

            if(this.lead.jobDetails)
            {
                if(this.productTypes == null)
                {
                    this._commonLookupService.getAllProductTypeForTableDropdown().subscribe(result => {
                        this.productTypes = result;
                    });
                }

                if (this.lead.jobDetails.isJobCancelRequest == true && this.lead.currentUserRole != 'Sales Rep') {
                    this.jobCancelReq = true;
                } else {
                    this.jobCancelReq = false;
                }

                if(this.lead.currentUserRole == 'Sales Rep')
                {
                    if(this.lead.jobDetails.jobStatusId > 4)
                    {
                        this.SalesRepManagerAdmin = true;
                        this.disbaleIfSaleRep = true;
                    }
                }
                else if(this.lead.currentUserRole == 'Sales Manager')
                {
                    if(this.lead.jobDetails.jobStatusId > 5)
                    {
                        this.SalesRepManagerAdmin = true;
                        this.disbaleIfSaleRep = true;
                    }
                }

                this.countCharcters();

                this.jobUnitNo = this.lead.jobDetails.unitNo;
                this.jobUnitType = this.lead.jobDetails.unitType;
                this.jobStreetNo = this.lead.jobDetails.streetNo;
                this.jobStreetName = this.lead.jobDetails.streetName;
                this.jobStreetType = this.lead.jobDetails.streetType;
                this.jobSuburb = this.lead.jobDetails.suburb;
                this.jobState = this.lead.jobDetails.state;
                this.jobPostCode = this.lead.jobDetails.postCode;
                this.job_Latitude = this.lead.jobDetails.latitude;
                this.job_Longitude = this.lead.jobDetails.longitude;
                this.jobCountry = this.lead.jobDetails.country;

                if(this.lead.jobDetails.jobProducts != null)
                {
                    this.JobProducts = [];
                    this.JobProducts = this.lead.jobDetails.jobProducts;
                }
                else{
                    this.JobProducts = [];
                    this.JobProducts.push(new JobProductItemDto());
                }
                
                // if (this.lead.currentUserRole == 'Sales Rep' && this.lead.jobDetails.jobStatusId == 4) {
                //     //this.disbaleifsalerep = false;
                //     this.SalesRepManagerAdmin = true;
                // }
                // else if ((this.lead.currentUserRole == 'Sales Manager' || this.lead.currentUserRole == 'Sales Rep') && this.job.jobStatusId >= 5) {
                   
                //     this.SalesRepManageradmin = false;

                //     if(this.lead.currentUserRole == 'Sales Manager' && this.job.jobStatusId <= 5)
                //     {
                //         this.disbaleifsalerep = true;
                //         this.SalesRepManageradmin = true;
                //     }
                // } else if (this.lead.currentUserRole == 'Admin') {
                //     this.SalesRepManageradmin = true;
                //     this.disbaleifsalerep = true;
                // }
                // else {
                //     this.SalesRepManageradmin = true;
                //     this.disbaleifsalerep = true;
                // }
            }

            this.hideMainSpinner();
        },
        ere => { this.hideMainSpinner() });
    }

    deleteLead(leadId): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._leadsServiceProxy.deleteLeads(leadId)
                        .subscribe(() => {
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            this.reloadLead.emit(true);
                        });
                }
            }
        );
    }

    warm(leadId): void {
        let status: GetLeadForChangeStatusOutput = new GetLeadForChangeStatusOutput();
        status.id = leadId;
        status.leadStatusID = 4;
        this.message.confirm('',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._leadsServiceProxy.changeStatus(status)
                        .subscribe(() => {
                            this.notify.success(this.l('LeadStatusChangedToWarm'));
                            //this._router.navigate(['/app/main/myleads/myleads']);
                            //this.modalSave.emit(null);
                            //this.showDetail(lead.id);
                            this.reloadLead.emit(false);
                        });
                }
            }
        );
    }

    cold(leadId): void {
        let status: GetLeadForChangeStatusOutput = new GetLeadForChangeStatusOutput();
        status.id = leadId;
        status.leadStatusID = 3;
        this.message.confirm('',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._leadsServiceProxy.changeStatus(status)
                        .subscribe(() => {
                            this.notify.success(this.l('LeadStatusChangedToCold'));
                            //this._router.navigate(['/app/main/myleads/myleads']);
                            //this.modalSave.emit(null);
                            //this.showDetail(lead.id);
                            this.reloadLead.emit(false);
                        });
                }
            }
        );
    }

    hot(leadId): void {
        let status: GetLeadForChangeStatusOutput = new GetLeadForChangeStatusOutput();
        status.id = leadId;
        status.leadStatusID = 5;
        this.message.confirm('',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._leadsServiceProxy.changeStatus(status)
                        .subscribe(() => {
                            this.notify.success(this.l('LeadStatusChangedToHot'));
                            //this._router.navigate(['/app/main/myleads/myleads']);
                            //this.modalSave.emit(null);
                            //this.showDetail(lead.id);
                            this.reloadLead.emit(false);
                        });
                }
            }
        );
    }

    cancel(leadId): void {
        abp.event.trigger('app.show.cancelLeadModal', leadId);
    }

    reject(leadId): void {
        abp.event.trigger('app.show.rejectLeadModal', leadId);
    }

    //-------------------------------------Start Map Details--------------------------------------------------
    bindMap() {
        if(this.selectedMap != "GoogleMap")
        {
            let orgMap = this.OrganizationMapDto.find(x => x.mapProvider == this.selectedMap);

            let Src = "";
            if(orgMap != null && orgMap != undefined)
            {
                if(orgMap.mapProvider == "GoogleMap") {
                    // Src = "https://maps.googleapis.com/maps/api/staticmap?center="+ this.lat + "," + this.lng +"&zoom=19&scale=2&size=800x300&maptype=satellite&key="+ orgMap.mapApiKey +"&markers=size:tiny|color:red|label:|" + this.lat + "," + this.lng;
                }
                else if (orgMap.mapProvider == "NearMap") {
                    let date = new Date();
                    let latest_date =this.datepipe.transform(date, 'yyyyMMdd');
    
                    Src = "https://au0.nearmap.com/staticmap?center="+ this.lat + "," + this.lng +"&zoom=20&size=600x1270&maptype=satellite&httpauth=false&date="+ latest_date +"&apikey="+ orgMap.mapApiKey;
                
                    //https://au0.nearmap.com/staticmap?center=-27.238610,153.039820&size=800x800&zoom=20&date=20200205&httpauth=false&apikey=MzA4YzMwYjItZTg3Yy00Yzg3LTg3ZWItYWRkZTA4NDI4ZDky
                }
                else if (orgMap.mapProvider == "MetroMap") {
                    Src = "api.metromap.com.au/metromapkey/getlayers?lng="+ this.lng  + "&lat=" + this.lat +"&type=point&zoom=19&scale=2&size=800x300&maptype=satellite&key="+ orgMap.mapApiKey +"&markers=size:tiny|color:red|label:|" + this.lat + "," + this.lng;
                
                    //https://api.metromap.com.au/metromapkey/getlayers?key={api-key}&lng={lng}&lat={lat}&type=point
                }
                else if (orgMap.mapProvider == "Pylon") {
                    Src = this.lead.jobDetails.pylonUrl;
                }
                else if (orgMap.mapProvider == "BingMap") {
                    Src = "https://dev.virtualearth.net/REST/v1/Imagery/Map/AerialWithLabels/"+ this.lat + "," + this.lng +"/20?mapSize=1270,600&pp="+ this.lat + "," + this.lng +";66&key="+ orgMap.mapApiKey;
                }
            }
    
            this.mapSrc = Src;
        }
    }
    //--------------------------------------End Map Details---------------------------------------------------

    //-------------------------------------Start Call History-------------------------------------------------
    myHistory: boolean = false;
    callType: any = '';
    callHistories: GetCallHistoryViewDto[];

    getCallHistory()
    {
        if(this.permission.isGranted("Pages.LeadDetails.CallHistory"))
        {
            this.showMainSpinner();
            this._callHistoryService.getCallHistory(this.lead.leadDetails.leadId, this.callType, this.myHistory).subscribe((result) => {
                this.callHistories = result;
                this.hideMainSpinner();
            }, err => {
                this.hideMainSpinner();
            });
        }
    }
    //--------------------------------------End Call History--------------------------------------------------

    //-------------------------------------Start Activity Log-------------------------------------------------
    changeActivity(leadId: number) {
        let that = this;
        this.spinner.show();
        debugger
        this._leadsServiceProxy.getLeadActivityLog(leadId, this.actionId, this.sectionId,this.currentactivity, false, 0, this.allActivity, this.serviceId).subscribe(result => {
            if (result.length > 0) {
                this.notify.success(this.l('ActivityFiltered'));
            }
            else {
                this.notify.success(this.l('NoActivityFound'));
            }
            this.leadActivityList = result;
            this.spinner.hide();
        });
    }

    myActivity(leadId: number) {
        let that = this;
        this.spinner.show();
        this._leadsServiceProxy.getLeadActivityLog(leadId, this.actionId, this.sectionId,this.currentactivity, this.activity, 0, this.allActivity, this.serviceId).subscribe(result => {
            if (result.length > 0) {
                this.notify.success(this.l('ActivityFiltered'));
            }
            else {
                this.notify.success(this.l('NoActivityFound'));
            }
            this.leadActivityList = result;
            this.spinner.hide();
        });
    }

    showActivity(){
        if(this.activityType == 6){
            this.smsModel.show(this.selectedLeadId, this.sectionId,this.serviceId)
        }
        else if(this.activityType == 7){
            this.emailModel.show(this.selectedLeadId, this.sectionId,this.serviceId)
        }
        else if(this.activityType == 8){
            this.ReminderModal.show(this.selectedLeadId, this.sectionId,this.serviceId)
        }
        else if(this.activityType == 9){
            this.notifyModel.show(this.selectedLeadId, this.sectionId,this.serviceId)
        }
        else if(this.activityType == 24){
            this.commentModel.show(this.selectedLeadId, this.sectionId,this.serviceId)
        }
        else if(this.activityType == 25){
            this.todoModal.show(this.selectedLeadId, this.sectionId, this.serviceId)
        }
    }
    //--------------------------------------End Activity Log--------------------------------------------------
    isJob: boolean = true;
    isVarify: boolean = false;
    savingNewJob: boolean = false;
    
    jobTypes: JobJobTypeLookupTableDto[];
    changeAddress: boolean = false;
    selectAddress: boolean = true;
    isGoogle = "Google";
    searchAddress: string;
    unitTypesSuggestions: string[];
    streetNamesSuggestions: string[];
    streetTypesSuggestions: string[];
    suburbSuggestions: string[];
    allStates: LeadStateLookupTableDto[];
    
    jobTypeId: number;
    
    unitNo: string;
    unitType: string;
    streetNo: string;
    streetName: string;
    streetType: string;
    suburb: any;
    state: string;
    postCode: string;
    country: string;
    jobLatitude: string;
    jobLongitude: string;

    actualCost: number = 0;

    savingSalesTab: boolean = false;
    SalesRepManagerAdmin: boolean = false;
    disbaleIfSaleRep: boolean = false;

    total: number = 0;

    changeJobAddress: boolean = false;
    jobIsGoogle = "Google";
    selectJobAddress: boolean = true;
    jobUnitTypesSuggestions: string[];
    jobStreetNamesSuggestions: string[];
    jobStreetTypesSuggestions: string[];
    jobSuburbSuggestions: string[];

    jobUnitNo: string;
    jobUnitType: string;
    jobStreetNo: string;
    jobStreetName: string;
    jobStreetType: string;
    jobSuburb: any;
    jobState: string;
    jobPostCode: string;
    jobCountry: string;
    job_Latitude: string;
    job_Longitude: string;
    savingJobAddress: boolean = false;

    JobProducts: any[];

    productItemSuggestions: any[];

    saveJob(): void {
        this.savingNewJob = true;

        let createJobDto = new CreateJobDto;
        createJobDto.leadId = this.selectedLeadId;
        createJobDto.jobTypeId = this.jobTypeId;
        createJobDto.sectionId = this.sectionId;

        createJobDto.address = new CreateOrEditAddressDto;
        createJobDto.address.unitNo = this.unitNo;
        createJobDto.address.unitType = this.unitType;
        createJobDto.address.streetNo = this.streetNo;
        createJobDto.address.streetName = this.streetName;
        createJobDto.address.streetType = this.streetType;
        createJobDto.address.suburb = this.suburb;
        createJobDto.address.state = this.state;
        createJobDto.address.postCode = this.postCode;
        createJobDto.address.state = this.state;
        createJobDto.address.country = this.country;
        createJobDto.address.longitude = this.jobLongitude;
        createJobDto.address.latitude = this.jobLatitude;

        this._leadDetailsServiceProxy.createNewJob(createJobDto).subscribe(result => {
            
            if(result == "OnlyAssignUserCanCreateJob") 
            {
                this.savingNewJob = false;
                this.notify.info(this.l('OnlyAssignUserCanCreateJob'));
            }
            else if(result == "AddreessAlredyExist") 
            {
                this.savingNewJob = false;
                this.notify.info(this.l('AddreessAlredyExist'));
            }
            else 
            {
                this.savingNewJob = false;
                this.notify.info(this.l('SavedSuccessfully'));
                this.show(this.selectedLeadId, this.sectionId, this.leadName, this.serviceId);
            }
        });
    }
    //--------------------------------------Start Job Tab------------------------------------------------------

    onAutocompleteSelected(result: PlaceResult) {

        this.unitNo = "";
        this.unitType = "";
        this.streetNo = "";
        this.streetName = "";
        this.streetType = "";
        this.suburb = "";
        this.state = "";
        this.postCode = "";
        this.country = "";

        if (result.address_components.length == 7) {

            this.streetNo = result.address_components[0].long_name.toUpperCase();
            
            var str = result.address_components[1].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.streetType = splitted[3].toUpperCase().trim();
            }
            else if (splitted.length == 3) {
                this.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.streetType = splitted[2].toUpperCase().trim();
            }
            else {
                this.streetName = splitted[0].toUpperCase().trim();
                this.streetType = splitted[1].toUpperCase().trim();
            }
            
            this.suburb = result.address_components[2].long_name.toUpperCase().trim();
            
            this.state = result.address_components[4].short_name;
            
            // this._leadsServiceProxy.getSuburbId(result.address_components[6].long_name).subscribe(result => {
            //     this.job.suburbId = result;
            // });
            // this._leadsServiceProxy.stateId(result.address_components[4].short_name).subscribe(result => {
            //     this.job.stateId = result;
            // });

            this.postCode = result.address_components[6].long_name.toUpperCase();
            //this.job.address = this.job.unitNo + " " + this.job.unitType + " " + this.job.streetNo + " " + this.streetName + " " + this.streetType;  //result.formatted_address;
            this.country = result.address_components[5].long_name.toUpperCase();
        }
        else if (result.address_components.length == 6) {

            this.streetNo = result.address_components[0].long_name.toUpperCase();
            
            var str = result.address_components[1].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.streetType = splitted[3].toUpperCase().trim();
            }
            else if (splitted.length == 3) {
                this.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.streetType = splitted[2].toUpperCase().trim();
            }
            else {
                this.streetName = splitted[0].toUpperCase().trim();
                this.streetType = splitted[1].toUpperCase().trim();
            }
            
            this.suburb = result.address_components[2].long_name.toUpperCase().trim();
            
            this.state = result.address_components[3].short_name;
            
            // this._leadsServiceProxy.getSuburbId(result.address_components[5].long_name).subscribe(result => {
            //     this.job.suburbId = result;
            // });
            // this._leadsServiceProxy.stateId(result.address_components[3].short_name).subscribe(result => {
            //     this.job.stateId = result;
            // });

            this.postCode = result.address_components[5].long_name.toUpperCase();
            //this.job.address = this.job.unitNo + " " + this.job.unitType + " " + this.job.streetNo + " " + this.streetName + " " + this.streetType;  //result.formatted_address;
            this.country = result.address_components[4].long_name.toUpperCase();
        }
        else {
            var str1 = result.address_components[0].long_name.toUpperCase();
            var splitted1 = str1.split(" ");
            if (splitted1.length == 1) {
                this.unitNo = splitted1[0].toUpperCase().trim();
            }
            else {
                this.unitNo = splitted1[1].toUpperCase().trim();
                this.unitType = splitted1[0].toUpperCase().trim();
                this.unitType = splitted1[0].toUpperCase().trim();
            }
            
            this.streetNo = result.address_components[1].long_name.toUpperCase();
            
            var str = result.address_components[2].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.streetType = splitted[3].toUpperCase().trim();
            }
            else if (splitted.length == 3) {
                this.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.streetType = splitted[2].toUpperCase().trim();
            }
            else {
                this.streetName = splitted[0].toUpperCase().trim();
                this.streetType = splitted[1].toUpperCase().trim();
            }
            
            this.suburb = result.address_components[3].long_name.toUpperCase().trim();
            
            this.state = result.address_components[5].short_name;
            
            // this._leadsServiceProxy.getSuburbId(result.address_components[7].long_name).subscribe(result => {
            //     this.job.suburbId = result;
            // });
            // this._leadsServiceProxy.stateId(result.address_components[5].short_name).subscribe(result => {
            //     this.job.stateId = result;
            // });
            
            this.postCode = result.address_components[7].long_name.toUpperCase();
            //this.job.address = this.job.unitNo + " " + this.job.unitType + " " + this.job.streetNo + " " + this.streetName + " " + this.streetType;  //result.formatted_address;
            this.country = result.address_components[6].long_name.toUpperCase();
        }
    }

    onLocationSelected(location: Location) {
        this.jobLatitude = location.latitude.toString();
        this.jobLongitude = location.longitude.toString();
    }

    google(): void {
        this.selectAddress = false;
    }

    database(): void {
        this.selectAddress = true;
    }

    addressChange() : void {
        if(this.isGoogle == 'Google')
        {
            this.selectAddress = false;
        }
        else
        {
            this.selectAddress = true;
        }
    }

    addressChangeClose() : void {
        if(this.isGoogle == 'Google')
        {
            this.selectAddress = false;
        }
        else
        {
            this.selectAddress = true;
        }

        this.unitNo = this.lead.leadDetails.unitNo;
        this.unitType = this.lead.leadDetails.unitType;
        this.streetNo = this.lead.leadDetails.streetNo;
        this.streetName = this.lead.leadDetails.streetName;
        this.streetType = this.lead.leadDetails.streetType;
        this.suburb = this.lead.leadDetails.suburb;
        this.state = this.lead.leadDetails.state;
        this.postCode = this.lead.leadDetails.postCode;
        this.country = "AUSTRALIA";
    }

    filterUnitTypes(event): void {
        this._commonLookupService.getAllUnitType(event.query).subscribe(output => {
            this.unitTypesSuggestions = output;
        });
    }

    filterStreetNames(event): void {
        this._commonLookupService.getAllStreetName(event.query).subscribe(output => {
            this.streetNamesSuggestions = output;
        });
    }

    filterStreetTypes(event): void {
        this._commonLookupService.getAllStreetType(event.query).subscribe(output => {
            this.streetTypesSuggestions = output;
        });
    }

    filtersuburbs(event): void {
        this._commonLookupService.getAllSuburb(event.query).subscribe(output => {
            this.suburbSuggestions = output;
        });
    }

    fillstatepostcode(): void {
        var splitted = this.suburb.split("||");
        this.suburb = splitted[0].trim();
        this.lead.leadDetails.suburb = splitted[0].trim();
        this.lead.leadDetails.state = splitted[1].trim();
        this.lead.leadDetails.postCode = splitted[2].trim();
        this.country = "AUSTRALIA";
    }

    checkActualCostFeaturesAndPermission() {
        return this.feature.isEnabled("App.LeadDetails.Job.ActualCost") && this.permission.isGranted("Pages.LeadDetails.CheckActualCost");
    }

    getJobValidationForStatus(role: string, jobStatus: any): boolean {
        if(role == "Admin")
        {
            return true;
        }
        else{
            if(jobStatus < 6)
            {
                return true;
            }
            else {
                return false;
            }
        }
    }

    getJobStatus(): void {
        this._leadDetailsServiceProxy.getJobStatus(this.lead.jobDetails.jobId).subscribe(result => {
            this.lead.jobDetails.jobStatusId = result.jobStatusId;
            this.lead.jobDetails.jobStatus = result.jobStatus;
            this.lead.jobDetails.jobStatusColorClass = result.jobStatusColorClass;
        });
    }

    countCharcters(): void {

        if(this.lead.jobDetails.note != null && this.lead.jobDetails.note != undefined)
        {
            this.total = this.lead.jobDetails.note.length;
        }
    }

    jobGoogle(): void {
        this.selectJobAddress = false;
    }

    jobDatabase(): void {
        this.selectJobAddress = true;
    }

    jobAddressChange() : void {
        if(this.jobIsGoogle == 'Google')
        {
            this.selectJobAddress = false;
        }
        else
        {
            this.selectJobAddress = true;
        }
    }

    jobAddressChangeClose() : void {
        if(this.jobIsGoogle == 'Google')
        {
            this.selectJobAddress = false;
        }
        else
        {
            this.selectJobAddress = true;
        }

        this.jobUnitNo = this.lead.jobDetails.unitNo;
        this.jobUnitType = this.lead.jobDetails.unitType;
        this.jobStreetNo = this.lead.jobDetails.streetNo;
        this.jobStreetName = this.lead.jobDetails.streetName;
        this.jobStreetType = this.lead.jobDetails.streetType;
        this.jobSuburb = this.lead.jobDetails.suburb;
        this.jobState = this.lead.jobDetails.state;
        this.jobPostCode = this.lead.jobDetails.postCode;
        this.job_Latitude = this.lead.jobDetails.latitude;
        this.job_Longitude = this.lead.jobDetails.longitude;
        this.jobCountry = this.lead.jobDetails.country;
    }

    jobOnAutocompleteSelected(result: PlaceResult) {

        this.jobUnitNo = "";
        this.jobUnitType = "";
        this.jobStreetNo = "";
        this.jobStreetName = "";
        this.jobStreetType = "";
        this.jobSuburb = "";
        this.jobState = "";
        this.jobPostCode = "";
        this.jobCountry = "";

        if (result.address_components.length == 7) {

            this.jobStreetNo = result.address_components[0].long_name.toUpperCase();
            
            var str = result.address_components[1].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.jobStreetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.jobStreetType = splitted[3].toUpperCase().trim();
            }
            else if (splitted.length == 3) {
                this.jobStreetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.jobStreetType = splitted[2].toUpperCase().trim();
            }
            else {
                this.jobStreetName = splitted[0].toUpperCase().trim();
                this.jobStreetType = splitted[1].toUpperCase().trim();
            }
            
            this.jobSuburb = result.address_components[2].long_name.toUpperCase().trim();
            
            this.jobState = result.address_components[4].short_name;
            
            // this._leadsServiceProxy.getSuburbId(result.address_components[6].long_name).subscribe(result => {
            //     this.job.suburbId = result;
            // });
            // this._leadsServiceProxy.stateId(result.address_components[4].short_name).subscribe(result => {
            //     this.job.stateId = result;
            // });

            this.jobPostCode = result.address_components[6].long_name.toUpperCase();
            //this.job.address = this.job.unitNo + " " + this.job.unitType + " " + this.job.streetNo + " " + this.streetName + " " + this.streetType;  //result.formatted_address;
            this.jobCountry = result.address_components[5].long_name.toUpperCase();
        }
        else if (result.address_components.length == 6) {

            this.jobStreetNo = result.address_components[0].long_name.toUpperCase();
            
            var str = result.address_components[1].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.jobStreetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.jobStreetType = splitted[3].toUpperCase().trim();
            }
            else if (splitted.length == 3) {
                this.jobStreetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.jobStreetType = splitted[2].toUpperCase().trim();
            }
            else {
                this.jobStreetName = splitted[0].toUpperCase().trim();
                this.jobStreetType = splitted[1].toUpperCase().trim();
            }
            
            this.jobSuburb = result.address_components[2].long_name.toUpperCase().trim();
            
            this.jobState = result.address_components[3].short_name;
            
            // this._leadsServiceProxy.getSuburbId(result.address_components[5].long_name).subscribe(result => {
            //     this.job.suburbId = result;
            // });
            // this._leadsServiceProxy.stateId(result.address_components[3].short_name).subscribe(result => {
            //     this.job.stateId = result;
            // });

            this.jobPostCode = result.address_components[5].long_name.toUpperCase();
            //this.job.address = this.job.unitNo + " " + this.job.unitType + " " + this.job.streetNo + " " + this.streetName + " " + this.streetType;  //result.formatted_address;
            this.jobCountry = result.address_components[4].long_name.toUpperCase();
        }
        else {
            var str1 = result.address_components[0].long_name.toUpperCase();
            var splitted1 = str1.split(" ");
            if (splitted1.length == 1) {
                this.jobUnitNo = splitted1[0].toUpperCase().trim();
            }
            else {
                this.jobUnitNo = splitted1[1].toUpperCase().trim();
                this.jobUnitType = splitted1[0].toUpperCase().trim();
            }
            
            this.jobStreetNo = result.address_components[1].long_name.toUpperCase();
            
            var str = result.address_components[2].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.jobStreetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.jobStreetType = splitted[3].toUpperCase().trim();
            }
            else if (splitted.length == 3) {
                this.jobStreetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.jobStreetType = splitted[2].toUpperCase().trim();
            }
            else {
                this.jobStreetName = splitted[0].toUpperCase().trim();
                this.jobStreetType = splitted[1].toUpperCase().trim();
            }
            
            this.jobSuburb = result.address_components[3].long_name.toUpperCase().trim();
            
            this.jobState = result.address_components[5].short_name;
            
            // this._leadsServiceProxy.getSuburbId(result.address_components[7].long_name).subscribe(result => {
            //     this.job.suburbId = result;
            // });
            // this._leadsServiceProxy.stateId(result.address_components[5].short_name).subscribe(result => {
            //     this.job.stateId = result;
            // });
            
            this.jobPostCode = result.address_components[7].long_name.toUpperCase();
            //this.job.address = this.job.unitNo + " " + this.job.unitType + " " + this.job.streetNo + " " + this.streetName + " " + this.streetType;  //result.formatted_address;
            this.jobCountry = result.address_components[6].long_name.toUpperCase();
        }
    }

    jobOnLocationSelected(location: Location) {
        this.job_Latitude = location.latitude.toString();
        this.job_Longitude = location.longitude.toString();
    }

    filterJobUnitTypes(event): void {
        this._commonLookupService.getAllUnitType(event.query).subscribe(output => {
            this.jobUnitTypesSuggestions = output;
        });
    }

    filterJobStreetNames(event): void {
        this._commonLookupService.getAllStreetName(event.query).subscribe(output => {
            this.jobStreetNamesSuggestions = output;
        });
    }

    filterJobStreetTypes(event): void {
        this._commonLookupService.getAllStreetType(event.query).subscribe(output => {
            this.jobStreetTypesSuggestions = output;
        });
    }

    filterJobSuburbs(event): void {
        this._commonLookupService.getAllSuburb(event.query).subscribe(output => {
            this.jobSuburbSuggestions = output;
        });
    }

    fillJobStatepostcode(): void {
        var splitted = this.jobSuburb.split("||");
        this.jobSuburb = splitted[0].trim();
        this.jobState = splitted[1].trim();
        this.jobPostCode = splitted[2].trim();
        this.jobCountry = "AUSTRALIA";
    }

    saveJobAddress(): void {
        
        this.savingJobAddress = true;

        let editJobAddressDto = new EditJobAddressDto();
        editJobAddressDto.jobId = this.lead.jobDetails.jobId;
        editJobAddressDto.sectionId = this.sectionId;

        editJobAddressDto.address = new CreateOrEditAddressDto;
        editJobAddressDto.address.unitNo = this.jobUnitNo;
        editJobAddressDto.address.unitType = this.jobUnitType;
        editJobAddressDto.address.streetNo = this.jobStreetNo;
        editJobAddressDto.address.streetName = this.jobStreetName;
        editJobAddressDto.address.streetType = this.jobStreetType;
        editJobAddressDto.address.suburb = this.jobSuburb;
        editJobAddressDto.address.state = this.jobState;
        editJobAddressDto.address.postCode = this.jobPostCode;
        editJobAddressDto.address.country = this.jobCountry;
        editJobAddressDto.address.longitude = this.job_Longitude;
        editJobAddressDto.address.latitude = this.job_Latitude;

        this._leadDetailsServiceProxy.updateJobAddress(editJobAddressDto).subscribe(result => {
            
            if(result == "AddreessAlredyExist") 
            {
                this.savingJobAddress = false;
                this.notify.info(this.l('AddreessAlredyExist'));
            }
            else if(result == "Success")
            {
                this.changeJobAddress = false;
                this.selectJobAddress = false;
                this.savingJobAddress = false;
                this.notify.info(this.l('SavedSuccessfully'));
                //this.show(this.selectedLeadId, this.sectionId, this.leadName, this.serviceId);
            }
        }, e => { this.savingJobAddress = false; this.notify.error(this.l('Error')); });
    }

    changePackage(event) {
       
        var packageId = event;

        this.spinnerService.show();
        if(packageId == 0)
        {
            // this._jobProductItemsServiceProxy.getJobProductItemByJobId(this.job.id).subscribe(result => {
            //     this.JobProducts = [];
                
            //     result.map((item) => {
            //         var jobproductcreateOrEdit = { id: 0, productItemId: 0, productItemName: "", quantity: 0, jobId: 0, size: 0, model: "", productItems: [], productTypeId: 0, stock: "" }
            //         jobproductcreateOrEdit.productItemId = item.jobProductItem.productItemId;
            //         jobproductcreateOrEdit.productTypeId = item.jobProductItem.productTypeId;
            //         jobproductcreateOrEdit.quantity = item.jobProductItem.quantity;
            //         jobproductcreateOrEdit.id = item.jobProductItem.id;
            //         jobproductcreateOrEdit.jobId = this.jobid;
            //         jobproductcreateOrEdit.size = item.size;
            //         jobproductcreateOrEdit.model = item.model;
                    
            //         this.JobProducts.push(jobproductcreateOrEdit);
            //     });

            //     if (result.length == 0) {
            //         var jobproductcreateOrEdit = { id: 0, productItemId: 0, productItemName: "", quantity: 0, jobId: 0, size: 0, model: "", productItems: [], productTypeId: 0 , stock: ""}
            //         this.JobProducts.push(jobproductcreateOrEdit);
            //     }

            //     this.job.basicCost = this.basicCost;

            //     let that = this;
            //     setTimeout(function () {
            //         that.calculateRates();
            //     }, 5000);

            //     this.getStockOnHandForAllView();

            //     this.saving = false;
            //     this.spinnerService.hide();
            // });
        }
        else {
            // this._productPackageServiceProxy.getProductPackageItemByPackageId(packageId).subscribe(result => {
            //     this.JobProducts = [];

            //     result.map((item) => {
            //         var jobproductcreateOrEdit = { id: 0, productItemId: 0, productItemName: "", quantity: 0, jobId: 0, size: 0, model: "", productItems: [], productTypeId: 0 }

            //         jobproductcreateOrEdit.productItemId = item.productItemId;
            //         jobproductcreateOrEdit.productTypeId = item.productTypeId;
            //         jobproductcreateOrEdit.quantity = item.quantity;
            //         jobproductcreateOrEdit.jobId = this.jobid;
            //         jobproductcreateOrEdit.size = item.size;
            //         jobproductcreateOrEdit.model = item.model;

            //         this.JobProducts.push(jobproductcreateOrEdit);
            //     });

            //     this._productPackageServiceProxy.getProductPackageForEdit(packageId).subscribe(result => {
            //         this.job.basicCost = result.productPackage.price;
            //     });

            //     this.getStockOnHandForAllView();

            //     let that = this;
            //     setTimeout(function () {
            //         that.calculateRates();
            //     }, 5000);
            //     this.saving = false;
            //     this.spinnerService.hide();
            // });
        }
    }

    calItems: CalculateJobProductItemDeatilDto[];
    viewCal() : void {
        this.calItems = [];

        this.JobProducts.map((item) => {
            var items = new CalculateJobProductItemDeatilDto();
            items.productItemId = item.productItemId;
            items.model = item.model;
            items.quantity = item.quantity == null || item.quantity == undefined ? 0 : item.quantity;
            this.calItems.push(items);
        });
    }

    checkLiveStockFeaturesAndPermission() {
        return this.feature.isEnabled("App.StockManagement.ProductItems.NetSituation") && this.permission.isGranted("Pages.LeadDetails.CheckLiveStock");
    }

    changeProductType(i) : void {
        this.JobProducts[i].productItemId = null;
        this.JobProducts[i].name = '';
        this.JobProducts[i].model = '';
        this.JobProducts[i].liveStock = 0;
    }

    filterProductIteams(event, i): void {
        let Id = this.JobProducts[i].productTypeId;

        this._jobServiceProxy.getProductItemList(Id, event.query, this.lead.jobDetails.state, true).subscribe(output => {
            this.productItemSuggestions = output;
        });
    }

    // Called When Product Item Selected.
    selectProductItem(event, i) {

        this.JobProducts[i].productItemId = event.id;
        this.JobProducts[i].name = event.productItem;
        this.JobProducts[i].model = event.productModel;
        this.JobProducts[i].size = event.size;

        // this.calculateSTCRebate(i);

        // if(this.checkNetSituationFeatures())
        // {
        //     this._quickstockServiceProxy.getStockDetailByState(this.JobProducts[i].productItemId, this.job.state).subscribe(result => {
        //         this.JobProducts[i].stock = result.stockOnHand;

        //         if(result.stockOnHand < 0)
        //         {
        //             this.JobProducts[i].productItemId = 0;
        //             this.JobProducts[i].productItemName = "";
        //             this.JobProducts[i].model = "";
        //             this.JobProducts[i].size = 0;

        //             this.notify.warn("Product Out Of Stock.");
        //         }

        //         this.invoiceBtnCheck();
        //     });
        // }
    }

    addJobProduct(): void {
        this.JobProducts.push(new JobProductItemDto());
    }

    removeJobProduct(JobProduct): void {
        if (this.JobProducts.length == 1)
            return;
       
        if (this.JobProducts.indexOf(JobProduct) === -1) {
            
        } else {
            this.JobProducts.splice(this.JobProducts.indexOf(JobProduct), 1);
        }
        // this.calculateSTCRebate(0);
        // this.invoiceBtnCheck();

        // if(JobProduct.productTypeId == 1)
        // {
        //     this.job.systemCapacity = 0;
        //     this.job.stc = 0;
        //     this.job.stcPrice = 0;
        //     this.job.rebate = 0;
        // }
    }

    check(event) {
        if (event.target.value < 1) {
            event.target.value = '';
        }
    }

    calculateSTCRebate(i): void {

        // let id = this.productTypes.find(x => x.id == 1);
        // if (this.JobProducts[i].productTypeId == id.id) {
        //     this.job.systemCapacity = 0;
        //     this.job.stc = 0;
        //     this.job.stcPrice = 0;
        //     this.job.rebate = 0;
        //     this.job.stcPrice = 36;

        //     debugger

        //     if (this.STCPostalCode.length > 0) {
        //         let stcpostalcode = this.STCPostalCode.find(x => parseInt(x.postCodeFrom) <= parseInt(this.job.postalCode) && parseInt(x.postCodeTo) >= parseInt(this.job.postalCode));
        //         if (this.STCYearWiseRate.length > 0) {
        //             let stcRate = this.STCYearWiseRate.find(x => x.year == (new Date()).getFullYear());
        //             if (stcpostalcode) {
        //                 if (this.STCZoneRating.length > 0) {
        //                     let zoneRating = this.STCZoneRating.find(x => x.id === stcpostalcode.zoneId);
                            
        //                     let size = this.JobProducts[i].size;
        //                     this.job.systemCapacity = parseFloat((this.JobProducts[i].quantity * size / 1000).toFixed(2));
        //                     this.job.stc = Math.floor((this.job.systemCapacity * zoneRating.rating * stcRate.rate));
        //                     this.job.rebate = this.job.stc * 36;
                           
        //                 }
        //             }
        //         }
        //     }
        // }
        // else {
        //     if (this.STCPostalCode.length > 0) {
        //         let stcpostalcode = this.STCPostalCode.find(x => x.postCodeFrom <= this.job.postalCode && x.postCodeTo >= this.job.postalCode);
        //         if (this.STCYearWiseRate.length > 0) {
        //             let stcRate = this.STCYearWiseRate.find(x => x.year == (new Date()).getFullYear());
        //             if (stcpostalcode) {
        //                 if (this.STCZoneRating.length > 0) {
        //                     let zoneRating = this.STCZoneRating.find(x => x.id === stcpostalcode.zoneId);
        //                 }
        //             }
        //         }
        //     }
        // }

        // this.getActualCost();
    }

    saveSales(): void {
        this.savingSalesTab = true;

        let editSalesDto = new EditSalesTabDto();
        editSalesDto.jobId = this.lead.jobDetails.jobId;
        editSalesDto.sectionId = this.sectionId;

        //Job Details
        editSalesDto.jobTypeId = this.lead.jobDetails.jobTypeId;
        editSalesDto.manualQuote = this.lead.jobDetails.manualQuote;
        editSalesDto.priceType = this.lead.jobDetails.priceType;
        editSalesDto.note = this.lead.jobDetails.note;
        editSalesDto.oldSystemDetails = this.lead.jobDetails.oldSystemDetails;
        editSalesDto.installerNotes = this.lead.jobDetails.installerNotes;

        //Rebate
        editSalesDto.systemCapacity = this.lead.jobDetails.systemCapacity;
        editSalesDto.stc = this.lead.jobDetails.stc;
        editSalesDto.rebate = this.lead.jobDetails.rebate;

        this._leadDetailsServiceProxy.updateSalesTab(editSalesDto).subscribe(result => {
            
            if(result == "Success") 
            {
                this.savingSalesTab = false;
                this.notify.info(this.l('SavedSuccessfully'));
            }
            else 
            {
                this.savingSalesTab = false; 
                this.notify.error(this.l('Error'));
            }
        }, e => { this.savingSalesTab = false; this.notify.error(this.l('Error')); });
    }
}