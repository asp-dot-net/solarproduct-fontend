import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TransportationCostServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy} from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';


import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { Title } from '@angular/platform-browser';
import { CreateOrEditTransportCostModalComponent } from './create-or-edit-transportcost-modal.component';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './transportCost.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class TransportCostComponent extends AppComponentBase {

    show: boolean = true;
    showchild: boolean = true;
    toggleBlock() {
        this.show = !this.show;
      };
      toggleBlockChild() {
        this.showchild = !this.showchild;
      };
      toggle: boolean = true;
    
      change() {
          this.toggle = !this.toggle;
        }
        shouldShow: boolean = false;

    @ViewChild('createOrEditTransportCostModal', { static: true }) createOrEditTransportCostModal: CreateOrEditTransportCostModalComponent;
    // @ViewChild('viewTeamModalComponent', { static: true }) viewTeamModal: ViewTeamModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    filterName = 'Jobnumber';
    firstrowcount = 0;
    last = 0;

    public screenHeight: any;  
    testHeight = 250;
    date = new Date();
    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);
    FiltersData = false;

    constructor(
        injector: Injector,
        private _transportationCostServiceProxy: TransportationCostServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _router: Router,
        private titleService: Title
        ) {
            super(injector);
            this.titleService.setTitle(this.appSession.tenancyName + " |  Transport Cost");
    }
    ngOnInit(): void {
        let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Transport Cost Tracker';
            log.section = 'Transport Cost Tracker';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        this.screenHeight = window.innerHeight; 
        this.getTransportCost();
    }
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  

    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }
        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }

    getTransportCost(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._transportationCostServiceProxy.getAll(
            this.filterName,
            this.filterText,
            this.startDate,
            this.endDate,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            25
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            this.shouldShow = false;
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    



    deleteTeam(id : number): void {
        this.message.confirm(
            this.l('AreYouSureWanttoDelete'),
            this.l('Delete'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._transportationCostServiceProxy.delete(id)
                        .subscribe(() => {
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Transport Cost Deleted';
                            log.section = 'Transport Cost Tracker';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Transport Cost Tracker';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Transport Cost Tracker';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }
}
