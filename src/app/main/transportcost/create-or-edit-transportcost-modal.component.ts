import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { CommonLookupServiceProxy, CreateOrEditTransportCost, TransportationCostServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { id } from '@swimlane/ngx-charts';
import { result } from 'lodash';

@Component({
    selector: 'createOrEditTransportCostModal',
    templateUrl: './create-or-edit-transportcost-modal.component.html'
})
export class CreateOrEditTransportCostModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    
    isActive = false;
    active = false;
    saving = false;

    TransportCost: CreateOrEditTransportCost = new CreateOrEditTransportCost();
    filteredJobs = []; 
    Companies = [];

    constructor(
        injector: Injector,
        private _transportationCostServiceProxy: TransportationCostServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy
    ) {
        super(injector);
    }

    sectionName = '';
    show(Id?: number,section = ''): void { 
        this._commonLookupService.getTransportCompanyDropdown().subscribe(result => {
            this.Companies = result;
        });
        this.sectionName =section;
        let log = new UserActivityLogDto();
             log.actionId = 79;
             log.actionNote = 'Open For ' + (!Id ? this.l('CreateTransportCost') : this.l('EditTransportCost')) ;
             log.section = section;
             this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                 .subscribe(() => {
             });

        if (!Id) {
            this.TransportCost = new CreateOrEditTransportCost();
            this.TransportCost.id = Id;
            this.active = true;
            this.isActive = true;
            setTimeout  (() => {
                this.modal.show();
            },2000);
        } else {
            this._transportationCostServiceProxy.getTransportCostForEdit(Id).subscribe(result => {
                this.TransportCost = result;
                this.active = true;
                this.isActive = true;
                this.modal.show();
            });
        }    
        
        console.log(this.TransportCost);
    }

    filterJobs(event): void {
            this._commonLookupService.getJobsDropdown(event.query).subscribe(teams => {
                this.filteredJobs = teams;
            });
    }

    save(): void {
        this.saving = true;

        this._transportationCostServiceProxy.createOrEdit(this.TransportCost)
            .pipe(finalize(() => { this.saving = false;}))
            .subscribe(() => {
                let log = new UserActivityLogDto();
                log.actionId = !this.TransportCost.id ? 81 : 82;
                log.actionNote = !this.TransportCost.id ? 'New Transport Cost Created' : 'Transport Cost Updated' ;
                log.section = this.sectionName;
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            this.notify.info(this.l('SavedSuccessfully'));
            this.close();
            this.modalSave.emit(null);
        });
    }

    onShown(): void {
        
        document.getElementById('TransportCost_Name').focus();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
