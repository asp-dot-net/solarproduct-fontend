﻿import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { FixedCostPriceServiceProxy, FixedCostPriceDto,UserActivityLogServiceProxy, UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import * as _ from 'lodash';
import { Title } from '@angular/platform-browser';
import { finalize } from 'rxjs/operators';
import { createOrEditFixCostModal } from './createEdit-fix-cost-price-modal.component';

@Component({
    templateUrl: './fix-cost-price.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class FixCostPriceComponent extends AppComponentBase {

    show: boolean = true;
    showchild: boolean = true;

    toggleBlock() {
        this.show = !this.show;
    };
    
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    @ViewChild('createOrEditFixCostModal', { static: true }) createOrEditFixCostModal: createOrEditFixCostModal;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    advancedFiltersAreShown = false;
    filterText = '';
    firstrowcount = 0;
    last = 0;
    public screenHeight: any;  
    testHeight = 250;
    constructor(
        injector: Injector,
        private _fixedCostPriceServiceProxy: FixedCostPriceServiceProxy,
        private titleService: Title,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    )   {
            super(injector);
            this.titleService.setTitle(this.appSession.tenancyName + " |  Fixed Cost Price");
    }

    searchLog() : void {
        debugger;
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Fixed Cost Price';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Fixed Cost Price';
        log.section = 'Fixed Cost Price';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
    }
    @HostListener('window:resize', ['$event'])  
    
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    
    getFixedCostPrice(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._fixedCostPriceServiceProxy.getAll(
            this.filterText,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    delete(fixedCostPrice: FixedCostPriceDto): void {
        this.message.confirm(
            this.l('AreYouSureWanttoDelete'),
            this.l('Delete'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._fixedCostPriceServiceProxy.delete(fixedCostPrice.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete Fixed Cost Price: ' + fixedCostPrice.name;
                            log.section = 'Fixed Cost Price';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });
                        });
                }
            }
        );
    }

   
}
