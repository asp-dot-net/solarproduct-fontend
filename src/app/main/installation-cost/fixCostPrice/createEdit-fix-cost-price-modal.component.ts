﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { FixedCostPriceServiceProxy, CreateOrEditFixedCostPriceDto, CommonLookupServiceProxy,UserActivityLogServiceProxy, UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'createOrEditFixCostModal',
    templateUrl: './createEdit-fix-cost-price-modal.component.html'
})
export class createOrEditFixCostModal extends AppComponentBase implements OnInit {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
   
    active = false;
    saving = false;
    fixedCostPrice: CreateOrEditFixedCostPriceDto = new CreateOrEditFixedCostPriceDto();
    constructor(
        injector: Injector,
        private _fixedCostPriceServiceProxy: FixedCostPriceServiceProxy,
        private _commonLookupServiceProxy: CommonLookupServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
    }

    show(id?: number): void {
        
        if (!id) {
            this.fixedCostPrice = new CreateOrEditFixedCostPriceDto();
            this.fixedCostPrice.id = id;
            this.fixedCostPrice.type = "";
            this.active = true;
            this.modal.show();

            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Fixed Cost Price';
            log.section = 'Fixed Cost Price';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            
            this._fixedCostPriceServiceProxy.getFixedCostPriceForEdit(id).subscribe(result => {
                this.fixedCostPrice = result.fixedCostPrice;
                this.active = true;
                this.modal.show();

                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Fixed Cost Price : ' + this.fixedCostPrice.name;
                log.section = 'Fixed Cost Price';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        
    }

    save(): void {
        this.saving = true;	
        this._fixedCostPriceServiceProxy.createOrEdit(this.fixedCostPrice)
            .pipe(finalize(() => { this.saving = false;}))
            .subscribe(() => {
               this.notify.info(this.l('SavedSuccessfully'));
               this.close();
               this.modalSave.emit(null);
               debugger
               if(this.fixedCostPrice.id){
                let log = new UserActivityLogDto();
                log.actionId = 82;
                log.actionNote ='Fixed Cost Price Updated : '+ this.fixedCostPrice.name;
                log.section = 'Fixed Cost Price';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
               }
               else{
                let log = new UserActivityLogDto();
                log.actionId = 81;
                log.actionNote ='Fixed Cost Price Created : '+ this.fixedCostPrice.name;
                log.section = 'Fixed Cost Price';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }
            });
    }

    onShown(): void {        
        document.getElementById('name').focus();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
