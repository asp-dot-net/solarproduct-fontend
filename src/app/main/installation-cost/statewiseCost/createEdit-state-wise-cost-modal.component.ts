﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { StateWiseInstallationCostServiceProxy, CreateOrEditStateWiseInstallationCostDto, CommonLookupServiceProxy ,UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'createOrEditStateWiseCostModal',
    templateUrl: './createEdit-state-wise-cost-modal.component.html'
})
export class createOrEditStateWiseCostModal extends AppComponentBase implements OnInit {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    saving = false;
    allState: any;
    stateWiseInstallationCost: CreateOrEditStateWiseInstallationCostDto = new CreateOrEditStateWiseInstallationCostDto();

    constructor(
        injector: Injector,
        private _stateWiseInstallationCostServiceProxy: StateWiseInstallationCostServiceProxy,
        private _commonLookupServiceProxy: CommonLookupServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this._commonLookupServiceProxy.getAllStateForTableDropdown().subscribe(result => {
            this.allState = result;
        });
    }

    show(id?: number): void {
        
        if (!id) {
            this.stateWiseInstallationCost = new CreateOrEditStateWiseInstallationCostDto();
            this.stateWiseInstallationCost.id = id;
            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New State Wise Cost';
            log.section = 'State Wise Cost';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            
            this._stateWiseInstallationCostServiceProxy.getStateWiseInstallationCostForEdit(id).subscribe(result => {
                this.stateWiseInstallationCost = result.stateWiseInstallationCost;
                this.active = true;
            
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit State Wise Cost : ' + this.stateWiseInstallationCost.stateId;
                log.section = 'State Wise Cost';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        
    }

    save(): void {
            this.saving = true;
			
            this._stateWiseInstallationCostServiceProxy.createOrEdit(this.stateWiseInstallationCost)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.stateWiseInstallationCost.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='State Wise Cost Updated : '+ this.stateWiseInstallationCost.stateId;
                    log.section = 'State Wise Cost';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='State Wise Cost Created : '+ this.stateWiseInstallationCost.stateId;
                    log.section = 'State Wise Cost';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }

    onShown(): void {        
        //document.getElementById('Team_Name').focus();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
