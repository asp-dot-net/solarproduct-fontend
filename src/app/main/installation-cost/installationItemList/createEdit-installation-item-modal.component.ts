﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { InstallationItemListServiceProxy, CreateOrEditInstallationItemPeriodDto, CreateOrEditInstallationItemListDto, OrganizationUnitDto, CommonLookupServiceProxy, CommonLookupDto 
, JobsServiceProxy, UserActivityLogServiceProxy, UserActivityLogDto ,
JobCostServiceProxy} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { result } from 'lodash';
import * as _ from 'lodash';
// import { debug } from 'console';

@Component({
    selector: 'createOrEditInstallationModal',
    templateUrl: './createEdit-installation-item-modal.component.html'
})
export class CreateOrEditInstallationModalComponent extends AppComponentBase implements OnInit {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
   
    active = false;
    saving = false;
    
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnitlength: number=0;
    date = new Date();
    installationItemList: any[];
    installationItemPeriod: CreateOrEditInstallationItemPeriodDto = new CreateOrEditInstallationItemPeriodDto();

    productTypes: CommonLookupDto[];
    productItemSuggestions: any[];

    month: number;
    monthList = [
        { Value: 1, Text: 'Jan' },
        { Value: 2, Text: 'Feb' },
        { Value: 3, Text: 'Mar' },
        { Value: 4, Text: 'Apr' },
        { Value: 5, Text: 'May' },
        { Value: 6, Text: 'June' },
        { Value: 7, Text: 'July' },
        { Value: 8, Text: 'Aug' },
        { Value: 9, Text: 'Sep' },
        { Value: 10, Text: 'Oct' },
        { Value: 11, Text: 'Nov' },
        { Value: 12, Text: 'Dec' }
    ];

    constructor(
        injector: Injector,
        private _installationItemListServiceProxy: InstallationItemListServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _jobServiceProxy: JobsServiceProxy,
        private _jobCostServiceProxy: JobCostServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
            // this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
        });

        this._commonLookupService.getAllProductTypeForTableDropdown().subscribe(result => {
            this.productTypes = result;
        });
    }

    show(Id?: number): void {
        debugger;
        if (!Id) {
            this.installationItemPeriod = new CreateOrEditInstallationItemPeriodDto();
            this.installationItemPeriod.id = Id;
            this.installationItemPeriod.organizationUnit = this.allOrganizationUnits[0].id;
            this.installationItemPeriod.month = this.date.getMonth() + 1;
            // this.installationItemPeriod.startDate = this.startDate;
            // this.installationItemPeriod.endDate = this.endDate;   
            this.installationItemList = [];
            let installationItemCreateOrEdit = { id: "", productTypeId: "", productItemId: "", productItemName: "", size: 0, pricePerWatt: 0, unitPrice: 0 }
            this.installationItemList.push(installationItemCreateOrEdit);
            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Installation Product Item List';
            log.section = 'Installation Product Item List';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });


        } else {
            this._installationItemListServiceProxy.getInstallationItemPeriodForEdit(Id).subscribe(result => {
                this.installationItemPeriod = result.installationItemPeriod;
                this.installationItemList = [];
                this.installationItemPeriod.installationItemList.map((item) => {
                    let installationItemCreateOrEdit = { id: 0, productTypeId: 0, productItemId: 0, productItemName: "", size: 0, pricePerWatt: 0, unitPrice: 0 }
                    installationItemCreateOrEdit.id = item.id;
                    installationItemCreateOrEdit.productTypeId = item.productTypeId;
                    installationItemCreateOrEdit.productItemId = item.productItemId;
                    installationItemCreateOrEdit.productItemName = item.productItem;
                    installationItemCreateOrEdit.pricePerWatt = item.pricePerWatt;
                    installationItemCreateOrEdit.unitPrice = item.unitPrice;
                    installationItemCreateOrEdit.size = item.size;
                    this.installationItemList.push(installationItemCreateOrEdit);
                });

                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Installation Product Item List : ' + this.installationItemPeriod.name;
                log.section = 'Installation Product Item List';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
    }

    save(): void {
        this.saving = true;
        this.installationItemPeriod.installationItemList = [];
        this.installationItemList.map((item) => {
            let installationItemListDto = new CreateOrEditInstallationItemListDto();
            installationItemListDto.id = item.id;
            installationItemListDto.productItemId = item.productItemId;
            installationItemListDto.productItem = item.productItemName;
            installationItemListDto.pricePerWatt = item.pricePerWatt;
            installationItemListDto.unitPrice = item.unitPrice;

            this.installationItemPeriod.installationItemList.push(installationItemListDto);
        });
        this._installationItemListServiceProxy.checkExistInstallationItemList(this.installationItemPeriod)
        .subscribe(result => {
            if(result.length > 0){
                this.saving = false;
                this.notify.warn('This dates are already exist');
            }
            else{
                this._installationItemListServiceProxy.createOrEdit(this.installationItemPeriod)
                .pipe(finalize(() => { this.saving = false;}))
                .subscribe(() => {
                    this.notify.info(this.l('SavedSuccessfully'));
                    this.close();
                    this.modalSave.emit(null);
                    if(this.installationItemPeriod.id){
                        let log = new UserActivityLogDto();
                        log.actionId = 82;
                        log.actionNote ='Installation Product Item List Updated : '+ this.installationItemPeriod.name;
                        log.section = 'Installation Product Item List';
                        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                            .subscribe(() => {
                        }); 
                    }else{
                        let log = new UserActivityLogDto();
                        log.actionId = 81;
                        log.actionNote ='Installation Product Item List Created : '+ this.installationItemPeriod.name;
                        log.section = 'Installation Product Item List';
                        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                            .subscribe(() => {
                        }); 
                    }
                });
                
            }
        });
        
    }

    onShown(): void {        
        document.getElementById('Name').focus();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    removeInstallationItem(installationItem): void {
        if (this.installationItemList.length == 1)
            return;
       
        if (this.installationItemList.indexOf(installationItem) === -1) {
            
        } else {
            this.installationItemList.splice(this.installationItemList.indexOf(installationItem), 1);
        }
    }

    addInstallationItem(): void {
        let installationItemCreateOrEdit = { id: "", productTypeId: "", productItemId: "", productItemName: "", size: 0, pricePerWatt: 0, unitPrice: 0 }
        this.installationItemList.push(installationItemCreateOrEdit);
    }

    // Called When Product Type Change
    changeProductType(i) : void {
        this.installationItemList[i].productItemId = "";
        this.installationItemList[i].productItemName = "";
        this.installationItemList[i].size = 0;
        this.installationItemList[i].pricePerWatt = 0;
        this.installationItemList[i].unitPrice = 0;
    }

    // Called When Product Item Entered.
    filterProductIteams(event, i): void {
        let Id = this.installationItemList[i].productTypeId;

        this._jobServiceProxy.getPicklistProductItemList(Id, event.query).subscribe(output => {
            //this.productItemSuggestions = output;
            debugger;
            var items =this.installationItemList.map(i=>i.productItemName+'');
            this.productItemSuggestions = output.filter(x=> !items.includes(x.productItem))
        });
    }

    // Called When Product Item Selected.
    selectProductItem(event, i) {

        this.installationItemList[i].productItemId = event.id;
        this.installationItemList[i].productItemName = event.productItem;
        this.installationItemList[i].size = event.size;

        this.calculateUnitPrice(i);
    }

    calculateUnitPrice(i: any) : void {
        let typeId = this.installationItemList[i].productTypeId;
        let pricePerWatt = this.installationItemList[i].pricePerWatt;
        let size = this.installationItemList[i].size;

        if(typeId == 1)
        {
            let unitPrice = pricePerWatt * size;
            this.installationItemList[i].unitPrice = unitPrice;
        }
    }

    getPrevProductItemsList(): void{
        this._installationItemListServiceProxy.getInstallationItemListForCreateView(this.installationItemPeriod.month, this.installationItemPeriod.organizationUnit)
            .subscribe(result => {
                    // if(!this.installationItemList[this.installationItemList.length-1].productTypeId){
                    //     this.installationItemList.splice(this.installationItemList.indexOf(this.installationItemList[this.installationItemList.length-1]), 1);
                    // }
                    this.installationItemList = [];
                    result.map((item) => {
                        let installationItemCreateOrEdit = { id: 0, productTypeId: 0, productItemId: 0, productItemName: "", size: 0, pricePerWatt: 0, unitPrice: 0 }
                        installationItemCreateOrEdit.id = item.id;
                        installationItemCreateOrEdit.productTypeId = item.productTypeId;
                        installationItemCreateOrEdit.productItemId = item.productItemId;
                        installationItemCreateOrEdit.productItemName = item.productItem;
                        installationItemCreateOrEdit.pricePerWatt = item.pricePerWatt;
                        installationItemCreateOrEdit.unitPrice = item.unitPrice;
                        installationItemCreateOrEdit.size = item.size;
                        this.installationItemList.push(installationItemCreateOrEdit);
                    });

                    if(result.length<=0){
                        this.notify.info("There is no item to fatch");
                        let installationItemCreateOrEdit = { id: 0, productTypeId: "", productItemId: 0, productItemName: "", size: 0, pricePerWatt: 0, unitPrice: 0 }
                        this.installationItemList.push(installationItemCreateOrEdit);
                    }
                
            //this.installationItemList = result;
            
        }); 
    }

    showJobCost(typeId: number, cost: number, jobId: number, dateType: string) : void {
        if(cost == 0)
        {
            this.showMainSpinner();
            this._jobCostServiceProxy.getCheckSystemCost(typeId, jobId, dateType).subscribe(result => {
                if(result.isExists) {
                    this.hideMainSpinner();
                    if (result.installationItemPeriodId == 0) {
                        this.installationItemPeriod = new CreateOrEditInstallationItemPeriodDto();

                        this.installationItemPeriod.organizationUnit = result.organizationUnit;

                        // this.installationItemPeriod.month = this.date.getMonth();
                        // this.installationItemPeriod.startDate = this.startDate;
                        // this.installationItemPeriod.endDate = this.endDate;
                        
                        this.installationItemList = [];
                        let installationItemCreateOrEdit = { id: "", productTypeId: "", productItemId: "", productItemName: "", size: 0, pricePerWatt: 0, unitPrice: 0 }
                        this.installationItemList.push(installationItemCreateOrEdit);

                        this.active = true;
                        this.modal.show();
                        this.hideMainSpinner();
                    } else {
                        this._installationItemListServiceProxy.getInstallationItemPeriodForEdit(result.installationItemPeriodId).subscribe(res => {
                            this.installationItemPeriod = res.installationItemPeriod;

                            this.installationItemList = [];
                            this.installationItemPeriod.installationItemList.map((item) => {
                                let installationItemCreateOrEdit = { id: 0, productTypeId: 0, productItemId: 0, productItemName: "", size: 0, pricePerWatt: 0, unitPrice: 0 }
                                installationItemCreateOrEdit.id = item.id;
                                installationItemCreateOrEdit.productTypeId = item.productTypeId;
                                installationItemCreateOrEdit.productItemId = item.productItemId;
                                installationItemCreateOrEdit.productItemName = item.productItem;
                                installationItemCreateOrEdit.pricePerWatt = item.pricePerWatt;
                                installationItemCreateOrEdit.unitPrice = item.unitPrice;
                                installationItemCreateOrEdit.size = item.size;
                                this.installationItemList.push(installationItemCreateOrEdit);
                            });

                            // let index = this.installationItemList.findIndex(x => x.productItemName === result.productItemName);
                            // if(index > 0) {
                            //     document.getElementById('jp' + index).focus();
                            // }

                            this.active = true;
                            this.modal.show();
                            this.hideMainSpinner();
                        }, error => { this.hideMainSpinner(); });
                    }
                }
                else {
                    this.hideMainSpinner();
                    this.notify.warn(result.productType + " does not exist.", "Stock Warning")
                }
                
            }, err => { this.hideMainSpinner(); });
        }
        else {
            //this.hideMainSpinner();
        }
        // this.showMainSpinner();
        
    }
}
