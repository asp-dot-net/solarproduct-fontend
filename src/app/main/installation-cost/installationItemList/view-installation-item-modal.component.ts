﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { InstallationItemListServiceProxy, GetInstallationPeriodForViewDto, InstallationItemPeriodDto ,UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
@Component({
    selector: 'viewInstallationModal',
    templateUrl: './view-installation-item-modal.component.html'
})
export class ViewInstallationModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
   
    insPeriod : GetInstallationPeriodForViewDto;

    // installationItemPeriod: InstallationItemPeriodDto;

    constructor(
        injector: Injector,
        private _installationItemListServiceProxy: InstallationItemListServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
        this.insPeriod = new GetInstallationPeriodForViewDto();
        this.insPeriod.installationItemPeriod = new InstallationItemPeriodDto();
        this.insPeriod.installationItemList = [];
    }

    
    installationItemList : any[];
    show(Id: number): void {
        
        this._installationItemListServiceProxy.getInstallationItemPeriodForView(Id).subscribe(result => {
            this.insPeriod = result;
            this.modal.show();
           
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Installation Product Item List  View : ' +this.insPeriod.installationItemPeriod.name ;
            log.section = 'Installation Product Item List';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {            
         });
        });
    }

    close(): void {
        this.modal.hide();
    }
}
