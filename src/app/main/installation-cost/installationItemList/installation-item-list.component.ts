﻿import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { InstallationItemListServiceProxy, InstallationItemPeriodDto,CommonLookupServiceProxy,UserActivityLogServiceProxy,OrganizationUnitDto, UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import * as _ from 'lodash';
import { Title } from '@angular/platform-browser';
import { CreateOrEditInstallationModalComponent } from './createEdit-installation-item-modal.component';
import { ViewInstallationModalComponent } from './view-installation-item-modal.component';
import * as moment from 'moment';
import { finalize } from 'rxjs/operators';
@Component({
    templateUrl: './installation-item-list.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class InstallationItemComponent extends AppComponentBase {

    show: boolean = true;
    showchild: boolean = true;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    @ViewChild('createOrEditInstallationModal', { static: true }) createOrEditInstallationModal: CreateOrEditInstallationModalComponent;
    @ViewChild('viewInstallationModal', { static: true }) viewInstallationModal: ViewInstallationModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    organizationUnit = 0;
    organizationUnitlength: number = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    advancedFiltersAreShown = false;
    filterText = '';
    firstrowcount = 0;
    last = 0;
    date = new Date();
    today: moment.Moment = moment(this.date);
    public screenHeight: any;  
    testHeight = 250;

    constructor(
        injector: Injector,
        private _installationItemListServiceProxy: InstallationItemListServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
         private _commonLookupService: CommonLookupServiceProxy,
        private titleService: Title
    )   {
            super(injector);
            this.titleService.setTitle(this.appSession.tenancyName + " |  Installation Product Item List");
    }

    searchLog() : void {
        debugger;
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Installation Product Item List';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Extra Installation Charges';
        log.section = 'Installation Product Item List';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
           
            
           
            this.getInstallationItemList();
        });
    }
    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Installation Product Item List';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    
    @HostListener('window:resize', ['$event'])  
    
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    
    getInstallationItemList(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._installationItemListServiceProxy.getAll(
            this.filterText,
            this.organizationUnit,
            null,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    // createOtherCharge(): void {
    //     this.createOrEditInstallationModal.show();
    // }

    // viewIntItem(): void {
    //     this.viewInstallationModal.show();
    // }

    delete(installationItemPeriod: InstallationItemPeriodDto): void {
        this.message.confirm(
            this.l('AreYouSureWanttoDelete'),
            this.l('Delete'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._installationItemListServiceProxy.delete(installationItemPeriod.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete Extra Installation Charges: ' + installationItemPeriod.name;
                            log.section = 'Installation Product Item List';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });
                        });
                }
            }
        );
    }

   
}
