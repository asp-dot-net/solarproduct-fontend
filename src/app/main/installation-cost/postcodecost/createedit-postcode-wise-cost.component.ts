import { Component, ViewChild, Injector, Output, EventEmitter, OnInit} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { PostCodeCostServiceProxy, CreateOrEditPostCodePricePeriodDto, CreateOrEditPostCodePriceListDto, JobCostServiceProxy,UserActivityLogServiceProxy,UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { forEach } from 'lodash';

@Component({
    selector: 'createOrEditPostCodeWiseCostModal',
    templateUrl: './createedit-postcode-wise-cost.component.html'
})
export class CreateOrEditPostCodeWiseCostModal extends AppComponentBase implements OnInit {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
   
    active = false;
    saving = false;
    fetch = false;

    allState: any;

    postCodeList: any[];
    postCodePricePeriod: CreateOrEditPostCodePricePeriodDto = new CreateOrEditPostCodePricePeriodDto();

    constructor(
        injector: Injector,
        private _postCodeWiseInstallationCostServiceProxy: PostCodeCostServiceProxy,
        private _jobCostServiceProxy: JobCostServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        
    }

    show(id?: number): void {
        
        if (!id) {
            this.active = true;
            this.postCodePricePeriod = new CreateOrEditPostCodePricePeriodDto();
            this.postCodePricePeriod.id = id;

            this.postCodePricePeriod.date = moment(new Date());

            this.postCodeList = [];
            let postCodeCreateOrEdit = { id: undefined, postCode: undefined, price: undefined }
            this.postCodeList.push(postCodeCreateOrEdit);
         
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Post Code Price';
            log.section = 'Post Code Price';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this.showMainSpinner();
            this._postCodeWiseInstallationCostServiceProxy.getPostCodeCostForEdit(id).subscribe(result => {
                this.postCodePricePeriod = result;

                this.postCodeList = [];
                this.postCodePricePeriod.postCodePriceList.map((item) => {
                    let postCodeCreateOrEdit = { id: undefined, postCode: undefined, price: undefined }
                    postCodeCreateOrEdit.id = item.id;
                    postCodeCreateOrEdit.postCode = item.postCode;
                    postCodeCreateOrEdit.price = item.price;
                    this.postCodeList.push(postCodeCreateOrEdit);

                });

                this.active = true;
            
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Post Code Price : ' + this.postCodePricePeriod.name;
                log.section = 'Post Code Price';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
                this.hideMainSpinner();
            });
        }
        
    }

    save(): void {
        this.saving = true;

        debugger;
       
       
        
          this.postCodeList.forEach(item => { 
            if(this.postCodeList.filter(e => e.postCode == item.postCode).length > 1){
                this.saving = false;
                this.notify.warn( item.postCode + " is duplicate");
                return;
            }
              
        });

        if(this.saving == true){
            this.postCodePricePeriod.postCodePriceList = [];
            this.postCodeList.map((item) => {
                let installationItemListDto = new CreateOrEditPostCodePriceListDto();
                installationItemListDto.id = item.id;
                installationItemListDto.postCode = item.postCode;
                installationItemListDto.price = item.price;
    
                if (installationItemListDto.postCode != undefined && installationItemListDto.postCode != null) {
                    this.postCodePricePeriod.postCodePriceList.push(installationItemListDto);
                }
            });
            this._postCodeWiseInstallationCostServiceProxy.checkExistingPostCode(this.postCodePricePeriod).subscribe(r => {
                if (!r) {
                    this._postCodeWiseInstallationCostServiceProxy.createOrEdit(this.postCodePricePeriod)
                        .pipe(finalize(() => { this.saving = false; }))
                        .subscribe(() => {
                            this.notify.info(this.l('SavedSuccessfully'));
                            this.close();
                            this.modalSave.emit(null);
                            if(this.postCodePricePeriod.id){
                                let log = new UserActivityLogDto();
                                log.actionId = 82;
                                log.actionNote ='Post Code Price Updated : '+ this.postCodePricePeriod.name;
                                log.section = 'Post Code Price';
                                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                    .subscribe(() => {
                                }); 
                            }else{
                                let log = new UserActivityLogDto();
                                log.actionId = 81;
                                log.actionNote ='Post Code Price Created : '+ this.postCodePricePeriod.name;
                                log.section = 'Post Code Price';
                                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                    .subscribe(() => {
                                }); 
                            }
                        });
                }
                else {
                    this.notify.warn('Already Exist.');
                    this.saving = false;
                }
            })
        }
        
    }

    onShown(): void {        
        document.getElementById('Name').focus();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    showJobCost(installationCost: number, jobId: number, postCode: string, dateType: string): void {
        this.showMainSpinner();
        if(installationCost == 0)
        {
            this._jobCostServiceProxy.getCheckInstallationCost(1, jobId, postCode, dateType).subscribe(result => {
                if(result.isExists) {
                    if (!result.postCodeId) {
                        this.active = true;
                        this.postCodePricePeriod = new CreateOrEditPostCodePricePeriodDto();

                        this.postCodePricePeriod.date = result.date;

                        this.postCodeList = [];
                        let postCodeCreateOrEdit = { id: undefined, postCode: undefined, price: undefined }
                        this.postCodeList.push(postCodeCreateOrEdit);
                    
                        this.modal.show();
                        this.hideMainSpinner();
                    } else {
                        
                        this._postCodeWiseInstallationCostServiceProxy.getPostCodeCostForEdit(result.postCodeId).subscribe(result => {
                            this.postCodePricePeriod = result;

                            this.postCodeList = [];
                            this.postCodePricePeriod.postCodePriceList.map((item) => {
                                let postCodeCreateOrEdit = { id: undefined, postCode: undefined, price: undefined }
                                postCodeCreateOrEdit.id = item.id;
                                postCodeCreateOrEdit.postCode = item.postCode;
                                postCodeCreateOrEdit.price = item.price;
                                this.postCodeList.push(postCodeCreateOrEdit);
                            });

                            this.active = true;
                            this.modal.show();
                            this.hideMainSpinner();
                        });
                    }
                }
                else {
                    this.hideMainSpinner();
                    this.notify.warn("Panel does not exist.", "Stock Warning")
                }
                
            }, err => { this.hideMainSpinner(); });
        }
        else {
            this.hideMainSpinner();
        }
    }

    removePostCode(postCode): void {
        if (this.postCodeList.length == 1)
            return;
       
        if (this.postCodeList.indexOf(postCode) === -1) {
            
        } else {
            this.postCodeList.splice(this.postCodeList.indexOf(postCode), 1);
        }
    }

    addPostCode(): void {
        let postCodeCreateOrEdit = { id: undefined, postCode: undefined, price: undefined }
        this.postCodeList.push(postCodeCreateOrEdit);
    }

    getPrevPostCodePriceList(): void{
        this.fetch = true;
        if (!this.postCodePricePeriod.date) {
            this.notify.warn("Select Month");
            this.fetch = false;
            return;
        }

        this._postCodeWiseInstallationCostServiceProxy.getPrevPostCodePriceList(this.postCodePricePeriod.date)
            .pipe(finalize(() => { this.fetch = false; }))
            .subscribe(result => {

                this.postCodeList = [];
                result.map((item) => {
                    let postCodeCreateOrEdit = { id: undefined, postCode: undefined, price: undefined }
                    postCodeCreateOrEdit.id = item.id;
                    postCodeCreateOrEdit.postCode = item.postCode;
                    postCodeCreateOrEdit.price = item.price;
                    this.postCodeList.push(postCodeCreateOrEdit);
                });

                if (result.length <= 0) {
                    this.notify.info("There is no item to fatch");
                    let postCodeCreateOrEdit = { id: undefined, postCode: undefined, price: undefined }
                    this.postCodeList.push(postCodeCreateOrEdit);
                }

                this.fetch = false;
            }, err => { this.fetch = false; }
        ); 
    }
}
