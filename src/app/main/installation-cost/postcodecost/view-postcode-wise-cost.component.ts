import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import {  PostCodeCostServiceProxy, CreateOrEditPostCodePricePeriodDto, UserActivityLogServiceProxy, UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
@Component({
    selector: 'viewPostCodeWiseCostModal',
    templateUrl: './view-postcode-wise-cost.component.html'
})
export class ViewPostCodeWiseCostModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
   
    insPeriod : CreateOrEditPostCodePricePeriodDto;

    // installationItemPeriod: InstallationItemPeriodDto;

    constructor(
        injector: Injector,
        private _postCodeWiseInstallationCostServiceProxy: PostCodeCostServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
        this.insPeriod = new CreateOrEditPostCodePricePeriodDto();
        this.insPeriod.postCodePriceList = [];
    }

    
    installationItemList : any[];
    show(Id: number): void {
        
        this._postCodeWiseInstallationCostServiceProxy.getPostCodeCostForEdit(Id).subscribe(result => {

            this.insPeriod = result;

            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Post Code Price  View : ' +this.insPeriod.name ;
            log.section = 'Post Code Price';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {            
         });
        });
    }

    close(): void {
        this.modal.hide();
    }
}
