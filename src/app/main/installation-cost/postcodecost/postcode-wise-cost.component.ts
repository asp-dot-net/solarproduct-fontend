import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import * as _ from 'lodash';
import { Title } from '@angular/platform-browser';
import { CreateOrEditPostCodeWiseCostModal } from './createedit-postcode-wise-cost.component';
import { PostCodeCostServiceProxy ,UserActivityLogServiceProxy, UserActivityLogDto,PostCodePricePeriodDto } from '@shared/service-proxies/service-proxies';
import { ViewPostCodeWiseCostModalComponent } from './view-postcode-wise-cost.component';
import { finalize } from 'rxjs/operators';
@Component({
    templateUrl: './postcode-wise-cost.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class postCodeWiseCostComponent extends AppComponentBase {

    show: boolean = true;
    showchild: boolean = true;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    @ViewChild('createOrEditPostCodeWiseCostModal', { static: true }) createOrEditCostModal: CreateOrEditPostCodeWiseCostModal;
    @ViewChild('viewPostCodeWiseCostModal', { static: true }) viewPostCodeWiseCostModal: ViewPostCodeWiseCostModalComponent;

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    firstrowcount = 0;
    last = 0;
    filterText = '';

    public screenHeight: any;  
    testHeight = 250;

    constructor(
        injector: Injector,
        private _postcodeWiseInstallationCostServiceProxy: PostCodeCostServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        // private _tokenAuth: TokenAuthServiceProxy,
        // private _activatedRoute: ActivatedRoute,
        // private _fileDownloadService: FileDownloadService,
        // private _router: Router,
        private titleService: Title
        ) {
            super(injector);
            this.titleService.setTitle(this.appSession.tenancyName + " |  postCode Wise Installation Cost");
    }
    
    searchLog() : void {
        debugger;
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Post Code Price';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Post Code Price';
        log.section = 'Post Code Price';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
    }
    
    @HostListener('window:resize', ['$event'])  
    
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    
    getpostCodeWiseInstallationCost(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._postcodeWiseInstallationCostServiceProxy.getAll(
            this.filterText,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
        }, err => {
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createOtherCharge(): void {
        this.createOrEditCostModal.show();
    }

    delete( postCodeDto : PostCodePricePeriodDto): void {
        this.message.confirm(
            this.l('AreYouSureWanttoDelete'),
            this.l('Delete'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._postcodeWiseInstallationCostServiceProxy.delete(postCodeDto.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete Post Code Price: ' + postCodeDto.name;
                            log.section = 'Post Code Price';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });
                        });
                }
            }
        );
    }

   
}
