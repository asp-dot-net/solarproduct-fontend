﻿import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { CategoryInstallationItemListServiceProxy, InstallationItemPeriodDto,UserActivityLogServiceProxy, CommonLookupServiceProxy,UserActivityLogDto,OrganizationUnitDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import * as _ from 'lodash';
import { Title } from '@angular/platform-browser';
import { finalize } from 'rxjs/operators';
 import { CreateOrEditCategoryInstallationModalComponent } from './createEdit-category-installation-item-modal.component';
 import { ViewCategoryInstallationModalComponent } from './view-category-installation-item-modal.component';
import * as moment from 'moment';

@Component({
    templateUrl: './category-installation-item.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class CategoryInstallationItemComponent extends AppComponentBase {

    show: boolean = true;
    showchild: boolean = true;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    @ViewChild('createOrEditCategoryInstallationModal', { static: true }) createOrEditCategoryInstallationModal: CreateOrEditCategoryInstallationModalComponent;
    @ViewChild('viewCategoryInstallationModal', { static: true }) viewCategoryInstallationModal: ViewCategoryInstallationModalComponent;  
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    advancedFiltersAreShown = false;
    organizationUnit = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnitlength: number = 0;
    orgCode = "";
    filterText = '';
    firstrowcount = 0;
    last = 0;
    date = new Date();
    today: moment.Moment = moment(this.date);
    public screenHeight: any;  
    testHeight = 250;
    constructor(
        injector: Injector,
        private _categoryInstallationItemListServiceProxy: CategoryInstallationItemListServiceProxy,
        private titleService: Title,
        private _commonLookupService: CommonLookupServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    )   {
            super(injector);
            this.titleService.setTitle(this.appSession.tenancyName + " |  Category Installation Item");
    }

    searchLog() : void {
        debugger;
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Category Installation Item';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Category Installation Item';
        log.section = 'Category Installation Item';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
           
            
           
            this.getInstallationItemList();
        });
    }
    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Category Installation Item';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    @HostListener('window:resize', ['$event'])  
    
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    changeOrgCode() {
        this.orgCode = this.allOrganizationUnits.find(x => x.id == this.organizationUnit).code;
        this.getInstallationItemList();

    }
    getInstallationItemList(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._categoryInstallationItemListServiceProxy.getAll(
            this.filterText,
            this.organizationUnit,
            null,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    delete(installationItemPeriod: InstallationItemPeriodDto): void {
        this.message.confirm(
            this.l('AreYouSureWanttoDelete'),
            this.l('Delete'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._categoryInstallationItemListServiceProxy.delete(installationItemPeriod.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete Category Installation Item: ' + installationItemPeriod.name;
                            log.section = 'Category Installation Item';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });
                        });
                }
            }
        );
    }

   
}
