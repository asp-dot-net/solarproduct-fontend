﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { OrganizationUnitDto, CommonLookupServiceProxy, CommonLookupDto ,
CategoryInstallationItemListServiceProxy,
CreateOrEditCategoryInstallationItemPeriodDto,
CreateOrEditCategoryInstallationItemListDto,UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';

@Component({
    selector: 'createOrEditCategoryInstallationModal',
    templateUrl: './createEdit-category-installation-item-modal.component.html'
})
export class CreateOrEditCategoryInstallationModalComponent extends AppComponentBase implements OnInit {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    saving = false;   
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnitlength: number=0;
    currentDate = moment();
    firstDay = this.currentDate.clone().startOf('isoWeek');
    lastday = this.currentDate.clone().endOf('isoWeek').add(-1, 'days');
    startDate = moment(this.firstDay);
    endDate = moment(this.lastday);  
    installationItemList: any[];
    installationItemPeriod: CreateOrEditCategoryInstallationItemPeriodDto = new CreateOrEditCategoryInstallationItemPeriodDto();
    productTypes: CommonLookupDto[];
    productItemSuggestions: any[];

    constructor(
        injector: Injector,
        private _categoryInstallationItemListServiceProxy: CategoryInstallationItemListServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
            this.organizationUnitlength = this.allOrganizationUnits.length;
        });
        this._commonLookupService.getAllProductTypeForTableDropdown().subscribe(result => {
            this.productTypes = result;
        });
    }
    show(Id?: number): void {
        debugger;
        if (!Id) {
            this.installationItemPeriod = new CreateOrEditCategoryInstallationItemPeriodDto();
            this.installationItemPeriod.id = Id;
            this.installationItemPeriod.organizationUnit = this.allOrganizationUnits[0].id;
            this.installationItemPeriod.startDate = this.startDate;
            this.installationItemPeriod.endDate = this.endDate;
            this.installationItemList = [];
            let installationItemCreateOrEdit = { id: "", productTypeId: "", productItemId: "", productItemName: "", size: 0, pricePerWatt: 0, unitPrice: 0 }
            this.installationItemList.push(installationItemCreateOrEdit);
            this.active = true;
            this.modal.show();

            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Category Installation Item';
            log.section = 'Category Installation Item';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._categoryInstallationItemListServiceProxy.getInstallationItemPeriodForEdit(Id).subscribe(result => {
                this.installationItemPeriod = result.categoryInstallationItemPeriod;

                this.installationItemList = [];
                this.installationItemPeriod.categoryInstallationItemList.map((item) => {
                    let installationItemCreateOrEdit = { id: 0, categoryName : '', startValue : 0, endValue : 0 }
                    installationItemCreateOrEdit.id = item.id;
                    installationItemCreateOrEdit.categoryName = item.categoryName;
                    installationItemCreateOrEdit.startValue = item.startValue;
                    installationItemCreateOrEdit.endValue = item.endValue;
                    this.installationItemList.push(installationItemCreateOrEdit);
                });
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Category Installation Item : ' + this.installationItemPeriod.name;
                log.section = 'Category Installation Item';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            });
        }
    }

    save(): void {
        this.saving = true;

        this.installationItemPeriod.categoryInstallationItemList = [];
        this.installationItemList.map((item) => {
            let installationItemListDto = new CreateOrEditCategoryInstallationItemListDto();
            installationItemListDto.id = item.id;
            installationItemListDto.categoryName= item.categoryName;
            installationItemListDto.startValue = item.startValue;
            installationItemListDto.endValue = item.endValue;

            this.installationItemPeriod.categoryInstallationItemList.push(installationItemListDto);
        });
        this._categoryInstallationItemListServiceProxy.checkExistInstallationItemList(this.installationItemPeriod)
        .subscribe(result => {
            if(result.length > 0){
                this.saving = false;
                this.notify.info(' This dates are already exist');
            }
            else{
                this._categoryInstallationItemListServiceProxy.createOrEdit(this.installationItemPeriod)
                .pipe(finalize(() => { this.saving = false;}))
                .subscribe(() => {
                    this.notify.info(this.l('SavedSuccessfully'));
                    this.close();
                    this.modalSave.emit(null);
                    if(this.installationItemPeriod.id){
                        let log = new UserActivityLogDto();
                        log.actionId = 82;
                        log.section = 'Category Installation Item';
                        log.actionNote ='Category Installation Item Updated : '+ this.installationItemPeriod.name;
                        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                            .subscribe(() => {
                        }); 
                    }else{
                        let log = new UserActivityLogDto();
                        log.actionId = 81;
                        log.actionNote ='Category Installation Item Created : '+ this.installationItemPeriod.name;
                        log.section = 'Category Installation Item';
                        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                            .subscribe(() => {
                        }); 
                    }
                });
            }
     });    
    }
    onShown(): void {        
        document.getElementById('Name').focus();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    removeInstallationItem(installationItem): void {
        if (this.installationItemList.length == 1)
            return;
       
        if (this.installationItemList.indexOf(installationItem) === -1) {
            
        } else {
            this.installationItemList.splice(this.installationItemList.indexOf(installationItem), 1);
        }
    }

    addInstallationItem(): void {
        let installationItemCreateOrEdit = { id: "", productTypeId: "", productItemId: "", productItemName: "", size: 0, pricePerWatt: 0, unitPrice: 0 }
        this.installationItemList.push(installationItemCreateOrEdit);
    }

    // Called When Product Type Change
    // changeProductType(i) : void {
    //     this.installationItemList[i].productItemId = "";
    //     this.installationItemList[i].productItemName = "";
    //     this.installationItemList[i].size = 0;
    //     this.installationItemList[i].pricePerWatt = 0;
    //     this.installationItemList[i].unitPrice = 0;
    // }

    // // Called When Product Item Entered.
    // filterProductIteams(event, i): void {
    //     let Id = this.installationItemList[i].productTypeId;

    //     this._jobServiceProxy.getPicklistProductItemList(Id, event.query).subscribe(output => {
    //         this.productItemSuggestions = output;
    //     });
    // }

    // Called When Product Item Selected.
    // selectProductItem(event, i) {

    //     this.installationItemList[i].productItemId = event.id;
    //     this.installationItemList[i].productItemName = event.productItem;
    //     this.installationItemList[i].size = event.size;

    //     this.calculateUnitPrice(i);
    // }

    // calculateUnitPrice(i: any) : void {
    //     let typeId = this.installationItemList[i].productTypeId;
    //     let pricePerWatt = this.installationItemList[i].pricePerWatt;
    //     let size = this.installationItemList[i].size;

    //     if(typeId == 1)
    //     {
    //         let unitPrice = pricePerWatt * size;
    //         this.installationItemList[i].unitPrice = unitPrice;
    //     }
    //     // else {
    //     //     this.installationItemList[i].unitPrice = 0;
    //     // }
    // }

    // getProductItems(): void{
    //     this._installationItemListServiceProxy.getInstallationItemListForCreateView(this.installationItemPeriod.startDate,this.installationItemPeriod.endDate, this.installationItemPeriod.organizationUnit)
    //         .subscribe(result => {
    //                 if(!this.installationItemList[this.installationItemList.length-1].productTypeId){
    //                     this.installationItemList.splice(this.installationItemList.indexOf(this.installationItemList[this.installationItemList.length-1]), 1);
    //                 }
    //                     result.map((item) => {
    //                         let installationItemCreateOrEdit = { id: 0, productTypeId: 0, productItemId: 0, productItemName: "", size: 0, pricePerWatt: 0, unitPrice: 0 }
    //                         installationItemCreateOrEdit.id = item.id;
    //                         installationItemCreateOrEdit.productTypeId = item.productTypeId;
    //                         installationItemCreateOrEdit.productItemId = item.productItemId;
    //                         installationItemCreateOrEdit.productItemName = item.productItem;
    //                         installationItemCreateOrEdit.pricePerWatt = item.pricePerWatt;
    //                         installationItemCreateOrEdit.unitPrice = item.unitPrice;
    //                         installationItemCreateOrEdit.size = item.size;
    //                         this.installationItemList.push(installationItemCreateOrEdit);
    //                     });

    //                 if(result.length<=0){
    //                     this.notify.info("There is no item to fatch");
    //                     let installationItemCreateOrEdit = { id: 0, productTypeId: "", productItemId: 0, productItemName: "", size: 0, pricePerWatt: 0, unitPrice: 0 }
    //                     this.installationItemList.push(installationItemCreateOrEdit);
    //                 }
                
    //         //this.installationItemList = result;
            
    //     }); 
    // }
}
