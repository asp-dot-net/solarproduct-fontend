﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import {  CategoryInstallationItemListServiceProxy, InstallationItemPeriodDto,GetCategoryInstallationItemPeriodForEditOutput, CreateOrEditCategoryInstallationItemPeriodDto,UserActivityLogServiceProxy,UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
@Component({
    selector: 'viewCategoryInstallationModal',
    templateUrl: './view-category-installation-item-modal.component.html'
})
export class ViewCategoryInstallationModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
   
    insPeriod : GetCategoryInstallationItemPeriodForEditOutput;
  
    constructor(
        injector: Injector,
        private _categoryInstallationItemListServiceProxy: CategoryInstallationItemListServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
        this.insPeriod = new GetCategoryInstallationItemPeriodForEditOutput();
        this.insPeriod.categoryInstallationItemPeriod = new CreateOrEditCategoryInstallationItemPeriodDto();
        this.insPeriod.categoryInstallationItemPeriod.categoryInstallationItemList = [];
    }

    
    installationItemList : any[];
    show(InsPeriod: InstallationItemPeriodDto): void {
        this._categoryInstallationItemListServiceProxy.getInstallationItemPeriodForView(InsPeriod.id).subscribe(result => {
            this.insPeriod = result;
            debugger;
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Opened Category Installation Item View : ' + InsPeriod.name;
            log.section = 'Category Installation Item';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {            
         });
         this.modal.show();
        });
    }

    close(): void {
        this.modal.hide();
    }
}
