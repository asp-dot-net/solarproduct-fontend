﻿import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { OtherChargesServiceProxy, CreateOrEditOtherChargesDto,UserActivityLogServiceProxy, UserActivityLogDto  } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
// import * as moment from 'moment';
// import { debug } from 'console';

@Component({
    selector: 'createOrEditChargeModal',
    templateUrl: './create-or-edit-charges-modal.component.html'
})
export class CreateOrEditChargeModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
   
    active = false;
    saving = false;

    otherCharges: CreateOrEditOtherChargesDto = new CreateOrEditOtherChargesDto();

    constructor(
        injector: Injector,
        private _otherChargesServiceProxy: OtherChargesServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
    }

    show(Id?: number): void {
        
        if (!Id) {
            this.otherCharges = new CreateOrEditOtherChargesDto();
            this.otherCharges.id = Id;

            this.active = true;
         
            this.modal.show();
            
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Other Charges';
            log.section = 'Other Charges';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._otherChargesServiceProxy.getOtherChargesForEdit(Id).subscribe(result => {
                this.otherCharges = result.otherCharges;
                this.active = true;
                this.modal.show();
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Other Charges : ' + this.otherCharges.name;
                log.section = 'Other Charges';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
    }

    save(): void {
            this.saving = true;

            this._otherChargesServiceProxy.createOrEdit(this.otherCharges)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.otherCharges.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Other Charges Updated : '+ this.otherCharges.name;
                    log.section = 'Other Charges';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Other Charges Created : '+ this.otherCharges.name;
                    log.section = 'Other Charges';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
            });
    }

    onShown(): void {        
        document.getElementById('Name').focus();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
