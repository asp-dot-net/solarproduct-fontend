import { Component, ViewChild, Injector, Output, EventEmitter, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {
    InstallationItemListServiceProxy, CreateOrEditInstallationItemPeriodDto, CreateOrEditInstallationItemListDto, OrganizationUnitDto, CommonLookupServiceProxy, CommonLookupDto
    , JobsServiceProxy, UserActivityLogServiceProxy, UserActivityLogDto,
    JobCostServiceProxy,
    JobCostFixExpenseListServiceProxy,
    CreateOrEditJobCostFixExpensePeriodDto,
    CreateOrEditJobCostFixExpenseListDto
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { result } from 'lodash';
import * as _ from 'lodash';
import { createEditPurchaseOrder } from '@app/main/inventory/purchase-order-new/create-purchase-order.component';
// import { debug } from 'console';

@Component({
    selector: 'CreateOrEditJobCostFixExpenseModal',
    templateUrl: './create-or-edit-jobcostfixexpense-modal.component.html'
})
export class CreateOrEditJobCostFixExpenseModalComponent extends AppComponentBase implements OnInit {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnitlength: number = 0;
    date = new Date();
    installationItemPeriod: CreateOrEditJobCostFixExpensePeriodDto = new CreateOrEditJobCostFixExpensePeriodDto();

    productTypes: CommonLookupDto[];
    productItemSuggestions: any[];

    month: number;
    monthList = [
        { Value: 1, Text: 'Jan' },
        { Value: 2, Text: 'Feb' },
        { Value: 3, Text: 'Mar' },
        { Value: 4, Text: 'Apr' },
        { Value: 5, Text: 'May' },
        { Value: 6, Text: 'June' },
        { Value: 7, Text: 'July' },
        { Value: 8, Text: 'Aug' },
        { Value: 9, Text: 'Sep' },
        { Value: 10, Text: 'Oct' },
        { Value: 11, Text: 'Nov' },
        { Value: 12, Text: 'Dec' }
    ];

    constructor(
        injector: Injector,
        private _jobCostFixExpenseListServiceProxy: JobCostFixExpenseListServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _userActivityLogServiceProxy: UserActivityLogServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
       

        
    }

    show(Id?: number,oid?: number): void {
        debugger;
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
            // this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;

            this._commonLookupService.getJobCostFixExpenseTypeDropdown().subscribe(result => {
                this.productTypes = result;

                if (!Id) {
                    this.installationItemPeriod = new CreateOrEditJobCostFixExpensePeriodDto();
                    this.installationItemPeriod.id = Id;
                    this.installationItemPeriod.organizationUnit = this.allOrganizationUnits[0].id;
                    // this.installationItemPeriod.date = this.date.getMonth() + 1;
                    // this.installationItemPeriod.startDate = this.startDate;
                    // this.installationItemPeriod.endDate = this.endDate;   
                    this.installationItemPeriod.jobCostFixExpenseList = [];
                    //let installationItemCreateOrEdit = { id: "", productTypeId: "", productItemId: "", productItemName: "", size: 0, pricePerWatt: 0, unitPrice: 0 }
                    this.installationItemPeriod.jobCostFixExpenseList.push(new CreateOrEditJobCostFixExpenseListDto);
                    this.active = true;
                    this.modal.show();
                    let log = new UserActivityLogDto();
                    log.actionId = 79;
                    log.actionNote = 'Open For Create New Installation Product Item List';
                    log.section = 'Job Cost Fix Expence';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                        });
        
        
                } else {
                    this._jobCostFixExpenseListServiceProxy.getJobCostFixExpensePeriodForEdit(Id).subscribe(result => {
                        this.installationItemPeriod = result;
        
                        if (result.jobCostFixExpenseList) {
                            if (result.jobCostFixExpenseList.length == 0) {
                                this.installationItemPeriod.jobCostFixExpenseList = [];
                                this.installationItemPeriod.jobCostFixExpenseList.push(new CreateOrEditJobCostFixExpenseListDto);
                            }
                        }
                        else {
                            this.installationItemPeriod.jobCostFixExpenseList = [];
                            this.installationItemPeriod.jobCostFixExpenseList.push(new CreateOrEditJobCostFixExpenseListDto);
        
                        }
        
                        this.active = true;
                        this.modal.show();
                        let log = new UserActivityLogDto();
                        log.actionId = 79;
                        log.actionNote = 'Open For Edit Job Cost Fix Expence : ' + this.installationItemPeriod.name;
                        log.section = 'Job Cost Fix Expence';
                        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                            .subscribe(() => {
                            });
                    });
                }
            });

        }); 
    }
    save(): void {
        this.saving = true;

            this._jobCostFixExpenseListServiceProxy.checkExistJobCostFixExpenseList(this.installationItemPeriod).subscribe(r => {
                if (!r)
                {
                    this._jobCostFixExpenseListServiceProxy.createOrEdit(this.installationItemPeriod)
                    .pipe(finalize(() => { this.saving = false; }))
                    .subscribe(() => {
                        this.notify.info(this.l('SavedSuccessfully'));
                        this.close();
                        this.modalSave.emit(null);
                        if (this.installationItemPeriod.id) {
                            let log = new UserActivityLogDto();
                            log.actionId = 82;
                            log.actionNote = 'Job Cost Fix Expence Updated : ' + this.installationItemPeriod.name;
                            log.section = 'Job Cost Fix Expence';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                                });
                        } else {
                            let log = new UserActivityLogDto();
                            log.actionId = 81;
                            log.actionNote = 'Job Cost Fix Expence Created : ' + this.installationItemPeriod.name;
                            log.section = 'Job Cost Fix Expence';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                                });
                        }
                    });
                }
                else {
                    this.notify.warn('Already Exist.');
                    this.saving = false;
                }
            })
    }
    onShown(): void {
        document.getElementById('Name').focus();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
    removeInstallationItem(installationItem): void {
        if (this.installationItemPeriod.jobCostFixExpenseList.length == 1)
            return;

        if (this.installationItemPeriod.jobCostFixExpenseList.indexOf(installationItem) === -1) {

        } else {
            this.installationItemPeriod.jobCostFixExpenseList.splice(this.installationItemPeriod.jobCostFixExpenseList.indexOf(installationItem), 1);
        }   
    }
    addInstallationItem(): void {

        this.installationItemPeriod.jobCostFixExpenseList.push(new CreateOrEditJobCostFixExpenseListDto);
    }

    getPrevProductItemsList(): void {
      
        this._jobCostFixExpenseListServiceProxy.getJobCostFixExpenseForCreateView(this.installationItemPeriod.date, this.installationItemPeriod.organizationUnit)
            .subscribe(result => {
                // If the last item in the list has no JobCostFixExpenseTypeId, remove it
                const lastItemIndex = this.installationItemPeriod.jobCostFixExpenseList.length - 1;
                if (lastItemIndex >= 0 && !this.installationItemPeriod.jobCostFixExpenseList[lastItemIndex].jobCostFixExpenseTypeId) {
                    this.installationItemPeriod.jobCostFixExpenseList.splice(lastItemIndex, 1);
                }
    
                // Initialize the list if the result is empty
                if (result.length <= 0) {
                    this.notify.info("There is no item to fetch.");
                    this.installationItemPeriod.jobCostFixExpenseList = [];
                    const installationItemCreateOrEdit = new CreateOrEditJobCostFixExpenseListDto();
                    this.installationItemPeriod.jobCostFixExpenseList.push(installationItemCreateOrEdit);
                } else {
                    // Map the API result to jobCostFixExpenseList
                    this.installationItemPeriod.jobCostFixExpenseList = result.map(item => {
                        const installationItemCreateOrEdit = new CreateOrEditJobCostFixExpenseListDto();
                        installationItemCreateOrEdit.jobCostFixExpenseTypeId = item.jobCostFixExpenseTypeId;
                        installationItemCreateOrEdit.jobCostFixExpensePeriodId = item.jobCostFixExpensePeriodId;
                        installationItemCreateOrEdit.amount = item.amount;
                        return installationItemCreateOrEdit;
                    });
                }
            });
    }
    


}
