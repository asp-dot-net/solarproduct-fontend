import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { InstallationItemListServiceProxy, GetInstallationPeriodForViewDto, InstallationItemPeriodDto ,UserActivityLogServiceProxy, UserActivityLogDto, JobCostFixExpenseListServiceProxy, CreateOrEditJobCostFixExpensePeriodDto, CreateOrEditJobCostFixExpenseListDto} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
@Component({
    selector: 'ViewJobCostFixExpenseModal',
    templateUrl: './view-jobcostfixexpense-modal.component.html'
})
export class ViewJobCostFixExpenseModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
   
    insPeriod : CreateOrEditJobCostFixExpensePeriodDto;

    // installationItemPeriod: InstallationItemPeriodDto;

    constructor(
        injector: Injector,
        private _jobCostFixExpenseListServiceProxy: JobCostFixExpenseListServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
        this.insPeriod = new CreateOrEditJobCostFixExpensePeriodDto();
        this.insPeriod.jobCostFixExpenseList = [];
    }

    
    installationItemList : any[];
    show(Id: number): void {
        
        this._jobCostFixExpenseListServiceProxy.getJobCostFixExpensePeriodForEdit(Id).subscribe(result => {
            this.insPeriod = result;
            this.modal.show();
           
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Job Cost Fix Expence View : ' +this.insPeriod.name ;
            log.section = 'Job Cost Fix Expence';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {            
         });
        });
    }

    close(): void {
        this.modal.hide();
    }
}
