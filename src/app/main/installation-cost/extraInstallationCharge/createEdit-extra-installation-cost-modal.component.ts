﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { ExtraInstallationChargesServiceProxy, CreateOrEditExtraInstallationChargeDto, CommonLookupServiceProxy, NameValueOfString,UserActivityLogServiceProxy, UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'createOrEditExtraInstallationModal',
    templateUrl: './createEdit-extra-installation-cost-modal.component.html'
})
export class createOrEditExtraInstallationModal extends AppComponentBase implements OnInit {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    saving = false;
    allState: any;
    extraInstallationCharge: CreateOrEditExtraInstallationChargeDto = new CreateOrEditExtraInstallationChargeDto();
    stateIds: any = [];
    filteredStates: NameValueOfString[];
    constructor(
        injector: Injector,
        private _extraInstallationChargesServiceProxy: ExtraInstallationChargesServiceProxy,
        private _commonLookupServiceProxy: CommonLookupServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this._commonLookupServiceProxy.getAllStateForTableDropdown().subscribe(result => {
            this.allState = result;
        });
    }

    cat = "";
    show(id?: number): void {
        this.stateIds = [];
        if (!id) {
            this.extraInstallationCharge = new CreateOrEditExtraInstallationChargeDto();
            this.extraInstallationCharge.id = id;
            this.allName = [];
            this.extraInstallationCharge.category = "";
            this.extraInstallationCharge.nameId = 0;
            this.extraInstallationCharge.stateId = 0;
            this.extraInstallationCharge.type = "";
            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Extra Installation Charges';
            log.section = 'Extra Installation Charges';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });

        } else {
            this._extraInstallationChargesServiceProxy.getExtraInstallationChargesForEdit(id).subscribe(result => {
                this.extraInstallationCharge = result.extraInstallationCharge;
                this.cat = this.extraInstallationCharge.category
                this.bindName();
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Extra Installation Charges : ' + this.extraInstallationCharge.category;
                log.section = 'Extra Installation Charges';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        
    }

    save(): void {
        debugger;
       
        this.extraInstallationCharge.stateIds = [];
        this.stateIds.map((item) => {
            this.extraInstallationCharge.stateIds.push(item.id);
        });

        this.saving = true;
        this._extraInstallationChargesServiceProxy.createOrEdit(this.extraInstallationCharge)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.extraInstallationCharge.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Extra Installation Charges Updated : '+ this.extraInstallationCharge.category;
                    log.section = 'Extra Installation Charges';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Extra Installation Charges Created : '+ this.extraInstallationCharge.category;
                    log.section = 'Extra Installation Charges';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
            });
        
    }

    onShown(): void {        
        //document.getElementById('Team_Name').focus();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    allName: any[];
    bindName() : void {
        this.allName = [];
        if(this.extraInstallationCharge.category == "House Type")
        {
            this._commonLookupServiceProxy.getAllHouseTypeForDropdown().subscribe(result => {
                this.allName = result;
            });
        }
        else if(this.extraInstallationCharge.category == "Roof Type")
        {
            this._commonLookupServiceProxy.getAllRoofTypeForDropdown().subscribe(result => {
                this.allName = result;
            });
        }
        else if(this.extraInstallationCharge.category == "Roof Angle")
        {
            this._commonLookupServiceProxy.getAllRoofAngleForDropdown().subscribe(result => {
                this.allName = result;
            });
        }
        else if(this.extraInstallationCharge.category == "Price Variation")
        {
            this._commonLookupServiceProxy.getAllVariationForDropdown().subscribe(result => {
                this.allName = result;
            });
        }
        else if(this.extraInstallationCharge.category == "Other Charges")
        {
            this._commonLookupServiceProxy.getAllOtherChargeForDropdown().subscribe(result => {
                this.allName = result;
            });
        }

        if(this.cat != this.extraInstallationCharge.category){
            this.extraInstallationCharge.nameId = 0
        }
    }

    filterState(): void {
        this.filteredStates = [];
        this.filteredStates = this.allState;
    }

    filterStates(event): void {
        this.filteredStates = this.allState.filter(x => x.displayName.toLowerCase().includes(event.query.toLowerCase()));
    }
}
