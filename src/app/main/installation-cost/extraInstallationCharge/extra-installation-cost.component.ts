﻿import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ExtraInstallationChargesServiceProxy, ExtraInstallationChargesDto, CommonLookupServiceProxy ,UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import * as _ from 'lodash';
import { Title } from '@angular/platform-browser';
import { createOrEditExtraInstallationModal } from './createEdit-extra-installation-cost-modal.component';
import { finalize } from 'rxjs/operators';
@Component({
    templateUrl: './extra-installation-cost.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ExtraInstallatIonChargeComponent extends AppComponentBase {

    show: boolean = true;
    showchild: boolean = true;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    @ViewChild('createOrEditExtraInstallationModal', { static: true }) createOrEditExtraInstallationModal: createOrEditExtraInstallationModal; 
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    advancedFiltersAreShown = false;
    firstrowcount = 0;
    last = 0;
    stateId = 0;
    public screenHeight: any;  
    testHeight = 250;
    category: string = "";
    nameId: number = 0;
    constructor(
        injector: Injector,
        private _extraInstallationChargesServiceProxy: ExtraInstallationChargesServiceProxy,
        private _commonLookupServiceProxy: CommonLookupServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private titleService: Title
        ) {
            super(injector);
            this.titleService.setTitle(this.appSession.tenancyName + " |  Extra Installation Charges");
    }
    
    searchLog() : void {
        debugger;
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Extra Installation Charges';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Extra Installation Charges';
        log.section = 'Extra Installation Charges';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
    }
    
    @HostListener('window:resize', ['$event'])  
    
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    
    getExtraInstallationCharges(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._extraInstallationChargesServiceProxy.getAll(
            '',
            this.category,
            this.nameId,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
        }, err => {
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createOtherCharge(): void {
        this.createOrEditExtraInstallationModal.show();
    }

    delete(extraInstallationChargesDto: ExtraInstallationChargesDto): void {
        this.message.confirm(
            this.l('AreYouSureWanttoDelete'),
            this.l('Delete'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._extraInstallationChargesServiceProxy.delete(extraInstallationChargesDto.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete Extra Installation Charges: ' + extraInstallationChargesDto.name;
                            log.section = 'Extra Installation Charges';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });
                        });
                }
            }
        );
    }

    allName: any[];
    bindName() : void {
        this.allName = [];
        this.nameId = 0;
        if(this.category == "House Type")
        {
            this._commonLookupServiceProxy.getAllHouseTypeForDropdown().subscribe(result => {
                this.allName = result;
            });
        }
        else if(this.category == "Roof Type")
        {
            this._commonLookupServiceProxy.getAllRoofTypeForDropdown().subscribe(result => {
                this.allName = result;
            });
        }
        else if(this.category == "Roof Angle")
        {
            this._commonLookupServiceProxy.getAllRoofAngleForDropdown().subscribe(result => {
                this.allName = result;
            });
        }
        else if(this.category == "Price Variation")
        {
            this._commonLookupServiceProxy.getAllVariationForDropdown().subscribe(result => {
                this.allName = result;
            });
        }
        else if(this.category == "Other Charges")
        {
            this._commonLookupServiceProxy.getAllOtherChargeForDropdown().subscribe(result => {
                this.allName = result;
            });
        }

        if(this.category == "")
        {
            this.getExtraInstallationCharges();
        }
        // if(this.cat != this.extraInstallationCharge.category){
        //     this.extraInstallationCharge.nameId = 0
        // }
    }
}
