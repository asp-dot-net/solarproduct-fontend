import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { BatteryInstallationCostServiceProxy, UserActivityLogDto, BatteryInstallationCostDto,UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import * as _ from 'lodash';
import { Title } from '@angular/platform-browser';
import { CreateOrEditBatteryInstallationCosteModalComponent } from './create-or-edit-batteryCost-model.componenet';
import { finalize } from 'rxjs/operators';
@Component({
    templateUrl: './batteryInstallation.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class BatteryInstallationCostComponent extends AppComponentBase {

    show: boolean = true;
    showchild: boolean = true;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    @ViewChild('CreateOrEditBatteryInstallationCosteModalComponent', { static: true }) CreateOrEditBatteryInstallationCosteModal: CreateOrEditBatteryInstallationCosteModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    advancedFiltersAreShown = false;
    filterText = '';
    firstrowcount = 0;
    last = 0;
    public screenHeight: any;  
    testHeight = 250;
    constructor(
        injector: Injector,
        private _batteryInstallationCostServiceProxy: BatteryInstallationCostServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private titleService: Title
    )   {
            super(injector);
            this.titleService.setTitle(this.appSession.tenancyName + " |  Battery Installation Cost");
    }
    searchLog() : void {
        debugger;
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Battery Installation Cost';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Battery Installation Cost';
        log.section = 'Battery Installation Cost';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });

    }
    
    @HostListener('window:resize', ['$event'])  
    
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    
    getBatteryInstallationCost(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._batteryInstallationCostServiceProxy.getAll(
            this.filterText,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createBatteryInstallationCost(): void {
        this.CreateOrEditBatteryInstallationCosteModal.show();
    }

    delete(batteryInstallation:BatteryInstallationCostDto): void {
        this.message.confirm(
            this.l('AreYouSureWanttoDelete'),
            this.l('Delete'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._batteryInstallationCostServiceProxy.delete(batteryInstallation.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete Battery Installation Cost: ' + batteryInstallation.fixCost;
                            log.section = 'Battery Installation Cost';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            }); 
                        });
                }
            }
        );
    }

   
}
