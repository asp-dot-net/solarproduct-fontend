import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { BatteryInstallationCostServiceProxy, CreateOrEditBatteryInstallationCostDto,UserActivityLogServiceProxy,UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
@Component({
    selector: 'CreateOrEditBatteryInstallationCosteModal',
    templateUrl: './create-or-edit-batteryCost-model.componenet.html'
})
export class CreateOrEditBatteryInstallationCosteModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    saving = false;

    batteryInstallationCost: CreateOrEditBatteryInstallationCostDto = new CreateOrEditBatteryInstallationCostDto();
    constructor(
        injector: Injector,
        private _batteryInstallationCostServiceProxy: BatteryInstallationCostServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);  
    }
    show(Id?: number): void {    
        if (!Id) {
            this.batteryInstallationCost = new CreateOrEditBatteryInstallationCostDto();
            this.batteryInstallationCost.id = Id;
            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Battery Installation Cost';
            log.section = 'Battery Installation Cost';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._batteryInstallationCostServiceProxy.getBatteryInstallationCostForEdit(Id).subscribe(result => {
                this.batteryInstallationCost = result.batteryInstallationCosts;
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Battery Installation Cost : ' + this.batteryInstallationCost.fixCost;
                log.section = 'Battery Installation Cost';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            });
        }
    }
    save(): void {
            this.saving = true;

            this._batteryInstallationCostServiceProxy.createOrEdit(this.batteryInstallationCost)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.batteryInstallationCost.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.section = 'Battery Installation Cost';
                    log.actionNote ='Battery Installation Cost Updated : '+ this.batteryInstallationCost.fixCost;
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Battery Installation Cost Created : '+ this.batteryInstallationCost.fixCost;
                    log.section = 'Battery Installation Cost';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
            });
    }

    onShown(): void {        
        document.getElementById('FixCost').focus();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
