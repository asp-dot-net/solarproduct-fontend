import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { STCCostServiceProxy, CreateOrEditSTCCostDto,UserActivityLogServiceProxy, UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
// import * as moment from 'moment';
// import { debug } from 'console';

@Component({
    selector: 'CreateOrEditSTCCosteModal',
    templateUrl: './create-or-edit-stcCost-model.componenet.html'
})
export class CreateOrEditSTCCosteModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
   
    active = false;
    saving = false;

    stccost: CreateOrEditSTCCostDto = new CreateOrEditSTCCostDto();

    constructor(
        injector: Injector,
        private _stcCostServiceProxy: STCCostServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
    }

    show(Id?: number): void {
        
        if (!Id) {
            this.stccost = new CreateOrEditSTCCostDto();
            this.stccost.id = Id;
            this.active = true;
            this.modal.show();

            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New STC Cost';
            log.section = 'STC Cost';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._stcCostServiceProxy.getSTCCostForEdit(Id).subscribe(result => {
                this.stccost = result.stcCosts;
                this.active = true;
                this.modal.show();
                
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit STC Cost : ' + this.stccost.cost;
                log.section = 'STC Cost';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
    }

    save(): void {
            this.saving = true;

            this._stcCostServiceProxy.createOrEdit(this.stccost)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.stccost.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='STC Cost Updated : '+ this.stccost.cost;
                    log.section = 'STC Cost';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='STC Cost Created : '+ this.stccost.cost;
                    log.section = 'STC Cost';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
            });
    }

    onShown(): void {        
        document.getElementById('cost').focus();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
