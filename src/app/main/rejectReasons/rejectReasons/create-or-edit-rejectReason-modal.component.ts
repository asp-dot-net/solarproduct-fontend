﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { RejectReasonsServiceProxy, CreateOrEditRejectReasonDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'createOrEditRejectReasonModal',
    templateUrl: './create-or-edit-rejectReason-modal.component.html'
})
export class CreateOrEditRejectReasonModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    rejectReason: CreateOrEditRejectReasonDto = new CreateOrEditRejectReasonDto();



    constructor(
        injector: Injector,
        private _rejectReasonsServiceProxy: RejectReasonsServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }

    show(rejectReasonId?: number): void {

        if (!rejectReasonId) {
            this.rejectReason = new CreateOrEditRejectReasonDto();
            this.rejectReason.id = rejectReasonId;

            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Reject Reason';
            log.section = 'Reject Reason';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._rejectReasonsServiceProxy.getRejectReasonForEdit(rejectReasonId).subscribe(result => {
                this.rejectReason = result.rejectReason;

                this.active = true;
                this.modal.show();
                
let log = new UserActivityLogDto();
log.actionId = 79;
log.actionNote ='Open For Edit Reject Reason : ' + this.rejectReason.rejectReasonName;
log.section = 'Reject Reason';
this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
    .subscribe(() => {
});
            });
        }
        
    }
    onShown(): void {
        
        document.getElementById('RejectReason_RejectReasonName').focus();
    }
    save(): void {
            this.saving = true;

			
            this._rejectReasonsServiceProxy.createOrEdit(this.rejectReason)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.rejectReason.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Reject Reason Updated : '+ this.rejectReason.rejectReasonName;
                    log.section = 'Reject Reason';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Reject Reason Created : '+ this.rejectReason.rejectReasonName;
                    log.section = 'Reject Reason';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
