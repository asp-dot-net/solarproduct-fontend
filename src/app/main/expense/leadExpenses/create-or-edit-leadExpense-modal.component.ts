﻿

import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {
    LeadExpensesServiceProxy, CreateOrEditLeadExpenseDto,UserActivityLogServiceProxy, UserActivityLogDto, LeadStateLookupTableDto, LeadsServiceProxy, LeadExpenseAddDto, OrganizationUnitDto, UserServiceProxy, StateForLeadExpenseDto, CommonLookupServiceProxy, LeadSourceLookupTableDto
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';

@Component({
    selector: 'createOrEditLeadExpenseModal',
    templateUrl: './create-or-edit-leadExpense-modal.component.html'
})
export class CreateOrEditLeadExpenseModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    leadExpense: CreateOrEditLeadExpenseDto = new CreateOrEditLeadExpenseDto();
    date = new Date();
    //    sampleDateRange: moment.Moment[];
    firstDay = moment(new Date(this.date.getFullYear(), this.date.getMonth(), 1));
     lastday   = moment().endOf('month');
    //sampleDateRange: moment.Moment[] = [this.firstDay, this.lastday];
    leadSourceSourceName = '';
    totalamount = 0.00;
    allLeadSources: LeadSourceLookupTableDto[];
    allStates: LeadStateLookupTableDto[];
    leadstatedata: LeadExpenseAddDto[];
    startDate: moment.Moment;
    endDate: moment.Moment;
    minDate= moment(new Date(this.date.getFullYear(), this.date.getMonth(), 1));
    states:[];
    leadSources:[];
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnit = 0;
    organizationUnitlength: number=0;
    expensesforlead: any[];
    investstatedt:StateForLeadExpenseDto[];    
    nswTotalAmount =0 ;
    qldTotalAmount =0 ;
    saTotalAmount =0 ;
    waTotalAmount =0 ;
    vicTotalAmount =0 ;
    dateTypeFilter = 'DepositeReceived';
    BatteryFilter = '';
    withBattery = false ;
    constructor(
        injector: Injector,
        private _leadExpensesServiceProxy: LeadExpensesServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }

    show(leadExpenseId?: number): void {
        this.totalamount=0;
        this.waTotalAmount = 0;
        this.saTotalAmount = 0;
        this.qldTotalAmount = 0;
        this.vicTotalAmount = 0;
        this.nswTotalAmount = 0;
        this._leadExpensesServiceProxy.getAllStateForTableDropdownForReport(this.dateTypeFilter,this.startDate, this.endDate,this.states,this.leadSources,this.organizationUnit,'All',this.withBattery).subscribe(result => {
            this.allStates = result;
        });
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            
        });
     
        this._commonLookupService.getAllLeadSourceForTableDropdown().subscribe(result => {
            this.allLeadSources = result;
        
        });

        var CreateOrEditLead = new CreateOrEditLeadExpenseDto;
        CreateOrEditLead.leadSourceId=0;
        CreateOrEditLead.nswAmount=0;
        CreateOrEditLead.qldAmount=0;
        CreateOrEditLead.saAmount=0;
        CreateOrEditLead.waAmount=0;
        CreateOrEditLead.vicAmount=0;

        //{ leadSourceId: 0, nswAmount: 0, qldAmount: 0,  saAmount: 0, waAmount:0 }
       
    
        this.expensesforlead = [];
         
        this.expensesforlead.push(CreateOrEditLead);
        this.active = true;
        this.modal.show();
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Open For Create New Lead Expenses';
        log.section = 'Lead Expenses';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });

    }
    removeJobProduct(JobProduct): void {
        // if (this.expensesforlead.length == 1)
        //     return;
        // this.expensesforlead = this.expensesforlead.filter(item => item.leadSourceId != JobProduct.leadSourceId);
        if (this.expensesforlead.length == 1)
            return;
       
        if (this.expensesforlead.indexOf(JobProduct) === -1) {
            
        } else {
            this.expensesforlead.splice(this.expensesforlead.indexOf(JobProduct), 1);
        }
        this.caculateTotalamount(0);
    }


    onShown(): void {

       // document.getElementById('leadSourceId').focus();
    }
    caculateTotalamount(ev) {
        debugger;
        this.totalamount=0;
        this.waTotalAmount = 0;
        this.saTotalAmount = 0;
        this.qldTotalAmount = 0;
        this.vicTotalAmount = 0;
        this.nswTotalAmount = 0;
        this.expensesforlead.forEach(item => {
             
                this.totalamount = this.totalamount  + parseFloat(item.nswAmount)+parseFloat(item.qldAmount)+parseFloat(item.saAmount)+parseFloat(item.waAmount) + parseFloat(item.vicAmount);
                this.nswTotalAmount = this.nswTotalAmount + parseFloat( item.nswAmount);
                this.waTotalAmount = this.waTotalAmount + parseFloat(  item.waAmount);
                this.saTotalAmount = this.saTotalAmount + parseFloat( item.saAmount);
                this.qldTotalAmount = this.qldTotalAmount + parseFloat( item.qldAmount);
                this.vicTotalAmount = this.vicTotalAmount + parseFloat(  item.vicAmount) ;
        })
    }

    checkAll(ev) {
        debugger;
        this.totalamount=0;
        // this.waTotalAmount = 0;
        // this.saTotalAmount = 0;
        // this.qldTotalAmount = 0;
        // this.vicTotalAmount = 0;
        // this.nswTotalAmount = 0;
        this.leadstatedata.forEach(iteam => {
            iteam.leadstates.forEach(item => {
                this.totalamount = this.totalamount + item.amount;
                // this.nswTotalAmount = this.nswTotalAmount + parseFloat( item.nswAmount);
                // this.waTotalAmount = this.waTotalAmount + parseFloat(  item.waAmount);
                // this.saTotalAmount = this.saTotalAmount + parseFloat( item.saAmount);
                // this.qldTotalAmount = this.qldTotalAmount + parseFloat( item.qldAmount);
                // this.vicTotalAmount = this.vicTotalAmount + parseFloat(  item.vicAmount) ;
            })
        })
        
    }
    //this.oncheckboxCheck();
    /// this.totalcount = this.tableRecords.forEach(x => x.lead.isSelected = ev.target.checked);

    save(): void {
        debugger;
        
            if (this.totalamount > 0) {
                

                this.expensesforlead.forEach(iteam => {
                     iteam.fromDate=this.firstDay;
                     iteam.toDate = this.lastday;
                     iteam.organizationId=this.organizationUnit;
                     iteam.totalAmount=this.totalamount;
                })
                this.saving = true;
                this._leadExpensesServiceProxy.createExpense(this.expensesforlead)
                    .pipe(finalize(() => { this.saving = false; }))
                    .subscribe(() => {
                        this.notify.info(this.l('SavedSuccessfully'));
                        this.close();
                        this.modalSave.emit(null);

                        
                    });
            } else {
                this.notify.info(this.l('PleaseEnterAmount'));
                this.saving = false;
            }
        
    }

    addJobProduct():void
    {
        //
        // var CreateOrEditLead = { leadSourceId: 0, nswAmount: 0, qldAmount: 0,  saAmount: 0, waAmount:0 }
        
        // this.expensesforlead.push(CreateOrEditLead);

        var CreateOrEditLead = new CreateOrEditLeadExpenseDto;
        CreateOrEditLead.leadSourceId=0;
        CreateOrEditLead.nswAmount=0;
        CreateOrEditLead.qldAmount=0;
        CreateOrEditLead.saAmount=0;
        CreateOrEditLead.waAmount=0;
        CreateOrEditLead.vicAmount=0;
        this.expensesforlead.push(CreateOrEditLead);

    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
