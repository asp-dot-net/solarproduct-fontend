﻿import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import {
    LeadExpensesServiceProxy, LeadExpenseDto,
    CreateOrEditLeadExpenseDto,
    CommonLookupServiceProxy,
    LeadSourceLookupTableDto,
    OrganizationUnitDto,UserActivityLogServiceProxy, UserActivityLogDto
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { EditLeadExpenseModalComponent } from './edit-leadExpense-modal.component';
import { Title } from '@angular/platform-browser';

@Component({
    templateUrl: './leadExpenses.component.html',
    styleUrls: ['./leadExpenses.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class LeadExpensesComponent extends AppComponentBase {

    show: boolean = true;
    showchild: boolean = true;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };
    disableAddBtn = false;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('editLeadExpenseModal', { static: true }) editLeadExpenseModal: EditLeadExpenseModalComponent;
    advancedFiltersAreShown = false;
    filterText = '';
    FromDateFilter: moment.Moment;
    ToDateFilter: moment.Moment;
    maxAmountFilter: number;
    maxAmountFilterEmpty: number;
    minAmountFilter: number;
    minAmountFilterEmpty: number;
    leadSourceIdFilter: number = 0;
    leadSourceSourceNameFilter = '';
    currentmonthyear: string;
    allLeadSources: LeadSourceLookupTableDto[];

    leadExpense: CreateOrEditLeadExpenseDto = new CreateOrEditLeadExpenseDto();
    date = new Date();
    firstDay = moment(new Date(this.date.getFullYear(), this.date.getMonth(), 1));
    lastday = moment().endOf('month');
    currentyear = this.date.getFullYear();
    firstrowcount = 0;
    last = 0;
    organizationUnit = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    public screenHeight: any;  
    testHeight = 250;
    totalSummary = 0;
    totalNSWSummary = 0;
    totalQLDSummary = 0;
    totalSASummary = 0;
    totalWASummary = 0;
    totalVICSummary = 0;

    constructor(
        injector: Injector,
        private _leadExpensesServiceProxy: LeadExpensesServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _commonLookupService: CommonLookupServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private titleService: Title
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Lead Expenses");
        this._commonLookupService.getAllLeadSourceForTableDropdown().subscribe(result => {
            this.allLeadSources = result;
            this.leadSourceIdFilter = 0;
        });
    }
    
    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
        });
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Lead Expenses';
        log.section = 'Lead Expenses';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
    }
    searchLog() : void {
        debugger;
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Lead Expenses';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
  
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  

    getLeadExpenses(event?: LazyLoadEvent) {
        const monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ];
        const d = new Date();
        this.currentmonthyear = monthNames[d.getMonth()] + '-' + this.currentyear;
        
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._leadExpensesServiceProxy.getAll(
            this.filterText,
            this.maxAmountFilter == null ? this.maxAmountFilterEmpty : this.maxAmountFilter,
            this.minAmountFilter == null ? this.minAmountFilterEmpty : this.minAmountFilter,
            this.firstDay,
            this.lastday,
            this.leadSourceIdFilter,
            this.organizationUnit,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();

            if (result.totalCount > 0) {
                this.totalSummary = result.items[0].totalSummaryAmount;
                this.totalNSWSummary = result.items[0].totalNSWAmount;
                this.totalQLDSummary = result.items[0].totalQLDAmount;
                this.totalSASummary = result.items[0].totalSAAmount;
                this.totalWASummary = result.items[0].totalWAAmount;
                this.totalVICSummary = result.items[0].totalVICAmount;

            } else {
                 this.totalSummary = this.totalNSWSummary = this.totalQLDSummary = this.totalSASummary = this.totalWASummary = this.totalVICSummary = 0;
               
            }

            this.primengTableHelper.records.forEach(item => {
                if (item.forPeriod == this.currentmonthyear) {
                    this.disableAddBtn = true;
                }
            });
        });

    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    // createLeadExpense(): void {
    //     this.createOrEditLeadExpenseModal.show();        
    // }


    deleteLeadExpense(leadExpense: LeadExpenseDto): void {
        this.message.confirm(
            this.l('AreYouSureWanttoDelete'),
            this.l('Delete'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._leadExpensesServiceProxy.delete(leadExpense.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete Lead Expenses: ' + leadExpense.amount;
                            log.section = 'Lead Expenses';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });
                        });
                }
            }
        );
    }

    exportToExcel(filename): void {

        this._leadExpensesServiceProxy.getLeadExpensesToExcel(
            this.filterText,
            this.maxAmountFilter == null ? this.maxAmountFilterEmpty : this.maxAmountFilter,
            this.minAmountFilter == null ? this.minAmountFilterEmpty : this.minAmountFilter,
            this.firstDay,
            this.lastday,
            this.leadSourceIdFilter,
            this.organizationUnit,
            filename
        )
        .subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
         });
    }

}
