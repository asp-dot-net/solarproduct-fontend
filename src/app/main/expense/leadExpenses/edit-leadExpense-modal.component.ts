﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {
    LeadExpensesServiceProxy, CreateOrEditLeadExpenseDto,UserActivityLogServiceProxy, UserActivityLogDto, LeadStateLookupTableDto, LeadsServiceProxy, LeadExpenseAddDto, OrganizationUnitDto, UserServiceProxy, CommonLookupServiceProxy, LeadSourceLookupTableDto
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';

@Component({
    selector: 'editLeadExpenseModal',
    templateUrl: './edit-leadExpense-modal.component.html'
})
export class  EditLeadExpenseModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    leadExpense: CreateOrEditLeadExpenseDto = new CreateOrEditLeadExpenseDto();
    date = new Date();
    //    sampleDateRange: moment.Moment[];
    firstDay = moment(new Date(this.date.getFullYear(), this.date.getMonth(), 1));
     lastday   = moment().endOf('month');
    //sampleDateRange: moment.Moment[] = [this.firstDay, this.lastday];
    leadSourceSourceName = '';
    totalamount = 0;
    expensesforlead:any[];
    allLeadSources: LeadSourceLookupTableDto[];
    allStates: LeadStateLookupTableDto[];
    leadstatedata: LeadExpenseAddDto[];
    editleadstatedata:CreateOrEditLeadExpenseDto[];
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnit = 0;
    organizationUnitlength: number=0;
    states:[];
    leadSources:[];
    startDate: moment.Moment;
    endDate: moment.Moment;
    investmentid=0;
    nswTotalAmount =0 ;
    qldTotalAmount =0 ;
    saTotalAmount =0 ;
    waTotalAmount =0 ;
    vicTotalAmount =0 ;
    dateTypeFilter = 'DepositeReceived';
    withBattery = false ;
    constructor(
        injector: Injector,
        private _commonLookupService: CommonLookupServiceProxy,
        private _leadExpensesServiceProxy: LeadExpensesServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _userServiceProxy: UserServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
        
    }

    show(frdate?: any,leadExpenseId?: number): void {
        debugger;
        this._commonLookupService.getAllLeadSourceForTableDropdown().subscribe(result => {
            this.allLeadSources = result;
            this._commonLookupService.getOrganizationUnit().subscribe(output => {
                this.allOrganizationUnits = output;
                this.organizationUnit = this.allOrganizationUnits[0].id;
                this.organizationUnitlength = this.allOrganizationUnits.length;
                this._leadExpensesServiceProxy.getAllStateForTableDropdownForReport(this.dateTypeFilter,this.startDate, this.endDate,this.states,this.leadSources,output[0].id,'All',this.withBattery).subscribe(result => {
                    this.allStates = result;
    
                });
                if (!leadExpenseId) {
                    this.leadExpense = new CreateOrEditLeadExpenseDto();
                    this.leadExpense.id = leadExpenseId;
                    this.leadExpense.fromDate = moment().startOf('day');
                    this.leadExpense.toDate = moment().startOf('day');
                    this.leadExpense.totalAmount = 0;
                    this.leadSourceSourceName = '';
                    this.leadExpense.leadSourceId = 0;
                    this.active = true;
                    this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Lead Expenses';
            log.section = 'Lead Expenses';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
                } else {
                    debugger;
                    this._leadExpensesServiceProxy
                   
                        .getAllexpenseTableFor_Edit(frdate,leadExpenseId)
                        .subscribe(result => {
                            debugger;
                            this.expensesforlead = result;
        
                            // this.leadSourceSourceName = result.leadSourceSourceName;
                            // this.sampleDateRange.push(this.leadExpense.fromDate);
                            // this.sampleDateRange.push(this.leadExpense.toDate);
                            this.active = true;
                            this.modal.show();
                            let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Lead Expenses : ' + this.leadExpense.totalAmount;
                log.section = 'Lead Expenses';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
                        });
                }
            });
           
        });
       
        
       

         
        // this._leadsServiceProxy.getAllexpenseTable().pipe(finalize(() => { }))
        //     .subscribe(result => {
        //         this.leadstatedata = result;
        //         //console.log(this.leadstatedata);
        //     });
        
        
        this._leadExpensesServiceProxy.getTotalCount(leadExpenseId).subscribe(result => {
                this.totalamount = result.totalAmount;
                this.firstDay = moment(result.fromDate);
                this.lastday = moment(result.toDate);
                this.nswTotalAmount = result.totalNswAmount;
                this.waTotalAmount = result.totalWAAmount;
                this.saTotalAmount = result.totalSAAmount;
                this.qldTotalAmount = result.totalQLDAmount;
                this.vicTotalAmount = result.totalVICAmount;
                //this.sampleDateRange = [moment(result.fromDate), moment(result.toDate)];
            });
    }

    onShown(): void {

       // document.getElementById('leadSourceId').focus();
    }

    removeJobProduct(JobProduct): void {
        // debugger;
        // if (this.expensesforlead.length == 1)
        //     return;
           
         
        //     this._leadExpensesServiceProxy.deleteExpense(JobProduct.leadSourceId,id).subscribe(() => {
                
                 
        //     });
        // this.expensesforlead = this.expensesforlead.filter(item => item.leadSourceId != JobProduct.leadSourceId);
        if (this.expensesforlead.length == 1)
            return;
       
        if (this.expensesforlead.indexOf(JobProduct) === -1) {
            
        } else {
            this.expensesforlead.splice(this.expensesforlead.indexOf(JobProduct), 1);
        }
        this.caculateTotalamount(0);
    }
    caculateTotalamount(ev) {
        debugger;
        this.totalamount=0;
        this.waTotalAmount = 0;
        this.saTotalAmount = 0;
        this.qldTotalAmount = 0;
        this.vicTotalAmount = 0;
        this.nswTotalAmount = 0;
        this.expensesforlead.forEach(item => {
             
                this.totalamount = this.totalamount  + parseFloat( item.nswAmount) + parseFloat( item.qldAmount)  + parseFloat( item.saAmount) + parseFloat(  item.waAmount) + parseFloat(item.vicAmount);
                this.nswTotalAmount = this.nswTotalAmount + parseFloat( item.nswAmount);
                this.waTotalAmount = this.waTotalAmount + parseFloat(  item.waAmount);
                this.saTotalAmount = this.saTotalAmount + parseFloat( item.saAmount);
                this.qldTotalAmount = this.qldTotalAmount + parseFloat( item.qldAmount);
                this.vicTotalAmount = this.vicTotalAmount + parseFloat(  item.vicAmount) ;
            
        })
    }

     
    //this.oncheckboxCheck();
    /// this.totalcount = this.tableRecords.forEach(x => x.lead.isSelected = ev.target.checked);

    save(): void {
        debugger;
        
            if (this.totalamount > 0) {
                

                this.expensesforlead.forEach(iteam => {
                    iteam.fromDate=this.firstDay;
                    iteam.toDate = this.lastday;
                    iteam.organizationId=this.organizationUnit;
                    iteam.totalAmount=this.totalamount;
                })
                this.saving = true;
                this._leadExpensesServiceProxy.editExpenselist(this.expensesforlead)
                    .pipe(finalize(() => { this.saving = false; }))
                    .subscribe(() => {
                        this.notify.info(this.l('SavedSuccessfully'));
                        this.close();
                        this.modalSave.emit(null);
                        if(this.leadExpense.id){
                            let log = new UserActivityLogDto();
                            log.actionId = 82;
                            log.actionNote ='Lead Expenses Updated : '+ this.leadExpense.totalAmount;
                            log.section = 'Lead Expenses';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            }); 
                        }else{
                            let log = new UserActivityLogDto();
                            log.actionId = 81;
                            log.actionNote ='Lead Expenses Created : '+ this.leadExpense.totalAmount;
                            log.section = 'Lead Expenses';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            }); 
                        }
                    });
            } else {
                this.notify.info(this.l('PleaseEnterAmount'));
                this.saving = false;
            }
        
    }

    addJobProduct():void
    {
        //
        // var CreateOrEditLead = { leadSourceId: 0, nswAmount: 0, qldAmount: 0,  saAmount: 0, waAmount:0 }
        
        // this.expensesforlead.push(CreateOrEditLead);

        var CreateOrEditLead = new CreateOrEditLeadExpenseDto;
        CreateOrEditLead.leadSourceId=0;
        CreateOrEditLead.nswAmount=0;
        CreateOrEditLead.qldAmount=0;
        CreateOrEditLead.saAmount=0;
        CreateOrEditLead.waAmount=0;
        CreateOrEditLead.vicAmount =0;
        this.expensesforlead.push(CreateOrEditLead);

    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
