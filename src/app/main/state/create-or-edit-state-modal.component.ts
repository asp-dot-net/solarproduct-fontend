import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {StateServiceProxy, UserActivityLogServiceProxy,UserActivityLogDto, StatesDto} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
@Component({
    selector: 'createOrEditStateModal',
    templateUrl: './create-or-edit-state-modal.component.html'
})
export class CreateOrEditStateModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    saving = false;
    state: StatesDto = new StatesDto();
    constructor(
        injector: Injector,
        private _statesServiceProxy: StateServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
    }
    show(stateId?: number): void {
        if (!stateId) {
            this.state = new StatesDto();
            this.state.id = stateId;
            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New House Types';
            log.section = 'House Types';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 

        } else {
            this._statesServiceProxy.getStateForEdit(stateId).subscribe(result => {
                this.state = result;
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit House Types : ' + this.state.name;
                log.section = 'House Types';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            });
        }
    }
    onShown(): void {
        document.getElementById('State_Name').focus();
    }
    save(): void {
            this.saving = true;
            this._statesServiceProxy.updateRebate(this.state)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.state.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='House Types Updated : '+ this.state.name;
                    log.section = 'House Types';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='House Types Created : '+ this.state.name;
                    log.section = 'House Types';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
