﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { UnitTypesServiceProxy, CreateOrEditUnitTypeDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { ActivatedRoute } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
    selector: 'createOrEditUnitType',
    templateUrl: './create-or-edit-unitType.component.html',
    animations: [appModuleAnimation()]
})
export class CreateOrEditUnitTypeComponent extends AppComponentBase implements OnInit {
    active = false;
    saving = false;
    
    unitType: CreateOrEditUnitTypeDto = new CreateOrEditUnitTypeDto();



    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,        
        private _unitTypesServiceProxy: UnitTypesServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.show(this._activatedRoute.snapshot.queryParams['id']);
    }

    show(unitTypeId?: number): void {

        if (!unitTypeId) {
            this.unitType = new CreateOrEditUnitTypeDto();
            this.unitType.id = unitTypeId;

            this.active = true;
        } else {
            this._unitTypesServiceProxy.getUnitTypeForEdit(unitTypeId).subscribe(result => {
                this.unitType = result.unitType;


                this.active = true;
            });
        }
        
    }

    save(): void {
            this.saving = true;

			
            this._unitTypesServiceProxy.createOrEdit(this.unitType)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
             });
    }







}
