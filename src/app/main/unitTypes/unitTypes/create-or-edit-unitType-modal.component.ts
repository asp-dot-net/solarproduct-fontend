﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { UnitTypesServiceProxy, CreateOrEditUnitTypeDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';


@Component({
    selector: 'createOrEditUnitTypeModal',
    templateUrl: './create-or-edit-unitType-modal.component.html'
})
export class CreateOrEditUnitTypeModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    unitType: CreateOrEditUnitTypeDto = new CreateOrEditUnitTypeDto();



    constructor(
        injector: Injector,
        private _unitTypeServiceProxy: UnitTypesServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,

    ) {
        super(injector);
    }

    onShown(): void {
        document.getElementById('name').focus();
    } 
    show(unitTypeId?: number): void {
        //document.getElementById('name').focus();
       // document.getElementById('name').focus();
        if (!unitTypeId) {
            this.unitType = new CreateOrEditUnitTypeDto();
            this.unitType.id = unitTypeId;

            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Unit Types';
            log.section = 'Unit Types';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._unitTypeServiceProxy.getUnitTypeForEdit(unitTypeId).subscribe(result => {
                this.unitType = result.unitType;


                this.active = true;
                this.modal.show();
                
let log = new UserActivityLogDto();
log.actionId = 79;
log.actionNote ='Open For Edit Unit Types : ' + this.unitType.name;
log.section = 'Unit Types';
this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
    .subscribe(() => {
});
            });
        }

    }

    // onShown(): void {
    //     document.getElementById('UnitType_Name').focus();
    // }


    save(): void {
        this.saving = true;


        this._unitTypeServiceProxy.createOrEdit(this.unitType)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.unitType.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Unit Types Updated : '+ this.unitType.name;
                    log.section = 'Unit Types';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Unit Types Created : '+ this.unitType.name;
                    log.section = 'Unit Types';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
            });
    }







    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
