﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { UnitTypesServiceProxy, GetUnitTypeForViewDto, UnitTypeDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
    selector: 'viewUnitType',
    templateUrl: './view-unitType.component.html',
    animations: [appModuleAnimation()]
})
export class ViewUnitTypeComponent extends AppComponentBase implements OnInit {

    active = false;
    saving = false;

    item: GetUnitTypeForViewDto;


    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
         private _unitTypesServiceProxy: UnitTypesServiceProxy
    ) {
        super(injector);
        this.item = new GetUnitTypeForViewDto();
        this.item.unitType = new UnitTypeDto();        
    }

    ngOnInit(): void {
        this.show(this._activatedRoute.snapshot.queryParams['id']);
    }

    show(unitTypeId: number): void {
      this._unitTypesServiceProxy.getUnitTypeForView(unitTypeId).subscribe(result => {      
                 this.item = result;
                this.active = true;
            });       
    }
}
