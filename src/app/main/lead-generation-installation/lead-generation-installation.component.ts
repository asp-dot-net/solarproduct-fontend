﻿import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { ActivatedRoute , Router} from '@angular/router';
import { AbpSessionService, NotifyService, PermissionCheckerService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLookupDto, CommonLookupServiceProxy, InstallationCalendarDto, InstallationServiceProxy, InstallerServiceProxy, LeadsServiceProxy, LeadStateLookupTableDto, OrganizationUnitDto, TokenAuthServiceProxy, UserServiceProxy, LeadGenerationServiceProxy, LeadsAppointmentCalendarDto, UserActivityLogServiceProxy, UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
//import { PendingInstallationSmsEmailComponent } from './pendinginstallation-sms-email-model/pendinginstallatioin-smsemail.component';
import { ViewMyLeadComponent } from '@app/main/myleads/myleads/view-mylead.component';
import * as moment from 'moment';
import { Title } from '@angular/platform-browser';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './lead-geninstallation.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})

export class LeadGenInstallationComponent extends AppComponentBase {
    advancedFiltersAreShown = false;
    filterText = '';
    cancelReasonNameFilter = '';
    installerId = 0;
    calendaruserid = 0;
    // date= moment(new Date(), 'DD/MM/YYYY');
    date = moment().startOf('isoWeek').toDate();
    recordStartDate = moment(this.date, 'DD/MM/YYYY');
    InstallationList: LeadsAppointmentCalendarDto[];
    InstallerList: CommonLookupDto[];
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnit = 0;
    organizationUnitlength: number = 0;
    areaNameFilter = '';
    stateNameFilter = '';
    allStates: LeadStateLookupTableDto[];
    SelectedLeadId: number = 0;
    OpenRecordId: number = 0;
    ExpandedView: boolean = true;
    appointmentfor = 0;
    createBy = 0;
    appoForList = [];
    //@ViewChild('pendinginstallationTrackerSmsEmailModal', { static: true }) pendinginstallationTrackerSmsEmailModal: PendingInstallationSmsEmailComponent;
    @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
    users: CommonLookupDto[];

    constructor(
        injector: Injector,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _leadGenerationServiceProxy: LeadGenerationServiceProxy,
        private _permissionChecker: PermissionCheckerService,
        private _installerServiceProxy: InstallerServiceProxy,
        private _sessionService: AbpSessionService,
        private _activatedRoute: ActivatedRoute,
        private _userServiceProxy : UserServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _leadsServiceProxy : LeadsServiceProxy,
        private titleService: Title,
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Lead Generation Installation");
    }

    roleName = ['Leadgen Manager', 'Leadgen SalesRep', 'Sales Rep'];

    ngOnInit(): void {
        this._leadsServiceProxy.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
        
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Leads Generation Installation';
            log.section = 'Leads Generation Installation';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.getInstaller();
            this.getInstallations();
        });
       
    }

    getInstaller(): void {
        
        this._commonLookupService.getAllUsersByRoleNameContainsTableDropdown(this.roleName, this.organizationUnit).subscribe(result => {
            this.users = result;

            this._commonLookupService.getAllUsersForAppointment(this.organizationUnit).subscribe(result => {
                this.appoForList = result;
            });

            if (this._permissionChecker.isGranted('Pages.Tenant.InstallerManager.InstallerCalendar')) {
            
                this._installerServiceProxy.getAllInstallers(this.organizationUnit).subscribe(result => {
                    this.InstallerList = result;
                });
            }
            else
            {
                this.calendaruserid=this._sessionService.userId;
            }
            this.getInstallations();

        });
       
    };

    previousDate(): void {
        this.recordStartDate = moment(this.recordStartDate).add(-7, 'days');
        this.getInstallations();
    };

    nextDate(): void {
        this.recordStartDate = moment(this.recordStartDate).add(7, 'days');
        this.getInstallations();
    };

    setOpenRecord(id) {
        if (id === this.OpenRecordId) { this.OpenRecordId = 0; return; }
        this.OpenRecordId = id;
    }

    getInstallations(): void {
        this.showMainSpinner();
        if (this._permissionChecker.isGranted('Pages.Tenant.InstallerManager.InstallerCalendar')) {
          this.calendaruserid=this.installerId;
        }
        this._leadGenerationServiceProxy.getAppointmentCalendar(this.appointmentfor,this.recordStartDate, this.organizationUnit,this.areaNameFilter,this.stateNameFilter,this.createBy).subscribe(result => {
          this.InstallationList = result;
          
          this.hideMainSpinner();
        }, e => {
            this.hideMainSpinner();
        });
    }

    navigateToLeadDetail(leadid): void {
        this.ExpandedView = !this.ExpandedView;
        this.ExpandedView = false;
        this.SelectedLeadId = leadid;
        this.viewLeadDetail.showDetail(leadid, null, 11,0,'Leads Generation Installation');
    }

    installerSearchResult: any [];
    filterInstaller(event): void {
        this.installerSearchResult = Object.assign([], this.InstallerList).filter(
        item => item.displayName.toLowerCase().indexOf(event.query.toLowerCase()) > -1
        )
    }

    selectInstaller(event): void {
        this.installerId = event.id;
        this.getInstallations();
    }

    deleteAppoiment(installData): void {
        this.message.confirm(
            this.l('AreYouSureWanttoDelete'),
            this.l('Delete'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._leadGenerationServiceProxy.delete(installData.id)
                        .subscribe(() => {
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Appoinment Deleted' ;
                            log.section = 'Leads Generation Installation';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            this.getInstallations();
                        });
                }
            }
        );
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Leads Generation Installation';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Leads Generation Installation';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }

}