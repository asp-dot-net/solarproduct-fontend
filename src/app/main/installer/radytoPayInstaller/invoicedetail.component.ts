import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { CreateOrEditJobInstallerInvoiceDto, GetLeadSourceForViewDto, JobInstallerInvoicesServiceProxy, LeadSourceDto, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'invoiceDetailModal',
    templateUrl: './invoicedetail.component.html'
})
export class InvoiceDetailModalComponent extends AppComponentBase {

    @ViewChild('invoiceDetailModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: CreateOrEditJobInstallerInvoiceDto = new CreateOrEditJobInstallerInvoiceDto();


    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _jobInstallerInvoiceServiceProxy: JobInstallerInvoicesServiceProxy,
    ) {
        super(injector);
    }

  SectionName = '';
  show(Id : number, section = ''): void {
        
        this.showMainSpinner();
        this.SectionName = section;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Open Deatils For View Installer Invoice';
        log.section = section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
        this.item = new CreateOrEditJobInstallerInvoiceDto();
        this._jobInstallerInvoiceServiceProxy.getDataForEdit(Id).subscribe(result => {
            this.item = result;
            this.hideMainSpinner();
            this.modal.show();
            
        }, e => { 
          this.hideMainSpinner();
        });
    }

    close(): void {
        //this.active = false;
        this.modal.hide();
    }
}
