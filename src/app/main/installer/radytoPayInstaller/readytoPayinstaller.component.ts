import { Component, Injector, ViewChild, ViewEncapsulation, HostListener } from '@angular/core';
import { CommonLookupDto, CommonLookupServiceProxy, InstallerPaidDto, InstallerServiceProxy, JobInstallerInvoicesServiceProxy, JobStatusTableDto, LeadDto, LeadSourceLookupTableDto, LeadsServiceProxy, LeadStateLookupTableDto, LeadStatusLookupTableDto, OrganizationUnitDto, TokenAuthServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy, UserServiceProxy } from '@shared/service-proxies/service-proxies';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { AppConsts } from '@shared/AppConsts';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { finalize } from 'rxjs/operators';
import { FileUpload } from 'primeng/fileupload';
import { OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ViewApplicationModelComponent } from '@app/main/jobs/jobs/view-application-model/view-application-model.component';
import { AddActivityModalComponent } from '@app/main/myleads/myleads/add-activity-model.component';
import { JobSmsEmailModelComponent } from '@app/main/jobs/jobs/job-sms-email-model/job-sms-email-model.component';
import { EditupdateNewinvoiceComponent } from '../newinstaller/editupdate-newinvoice/editupdate-newinvoice.component';
import { Title } from '@angular/platform-browser';
import { FileItem, FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { InvoiceDetailModalComponent } from './invoicedetail.component';

@Component({
    selector: 'app-readytoPayinstaller',
    templateUrl: './readytoPayinstaller.component.html',
    styleUrls: ['./readytoPayinstaller.component.css'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ReadytoPayinstallerComponent extends AppComponentBase implements OnInit {
    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    

    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    @ViewChild('ExcelFileUpload', { static: false }) excelFileUpload: FileUpload;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('viewApplicationModal', { static: true }) viewApplicationModal: ViewApplicationModelComponent;
    @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    @ViewChild('addSmsEmailModal', { static: true }) addSmsEmailModal: JobSmsEmailModelComponent;
    @ViewChild('editupdatenewinvoice', { static: true }) editupdatenewinvoice: EditupdateNewinvoiceComponent;
    @ViewChild('invoiceDetailModal', { static: true }) invoiceDetailModal: InvoiceDetailModalComponent;
    
    FiltersData = true;
    isSummaryAll=false;
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnit = 0;
    organizationUnitlength: number = 0;
    allStates: any[]

    invoiceStatus = 2;
    invoiceType='';
    filterText = '';
    installerId: number = 0;
    suburbFilter = '';
    dateFilterType = 'CreationDate';
    date = new Date();
    // firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    // startDate = moment(this.firstDay);
    // endDate = moment().endOf('day');
    
    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);
    InstallerList: CommonLookupDto[];
    // stateNameFilter = '';
    stateList = [];
    areaNameFilter = '';

    firstrowcount = 0;
    last = 0;

    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 330;
    toggle: boolean = true;
    isinvoice = "Yes";
    path = AppConsts.docUrl;
    uploadUrlstc: string;
    filterName = 'JobNumber';
    orgCode = '';
    batteryFilter = 0;

    total = 0;
    paid = 0;
    unPaid = 0;
    totalM_Price = 0;
    totalM_Kw = 0;
    M_price = 0;
    totalR_Price = 0;
    totalR_Kw = 0;
    R_price = 0;

    totalInspection = 0;
    paidInspection = 0;
    unPaidInspection = 0;
    totalM_PriceInspection = 0;
    totalM_KwInspection = 0;
    M_priceInspection = 0;
    totalR_PriceInspection = 0;
    totalR_KwInspection= 0;
    R_priceInspection = 0;

    totalMaintenance = 0;
    paidMaintenance = 0;
    unPaidMaintenance = 0;
    totalM_PriceMaintenance = 0;
    totalM_KwMaintenance = 0;
    M_priceMaintenance = 0;
    totalR_PriceMaintenance = 0;
    totalR_KwMaintenance = 0;
    R_priceMaintenance = 0;

    totalInstallation = 0;
    paidInstallation = 0;
    unPaidInstallation = 0;
    totalM_PriceInstallation = 0;
    totalM_KwInstallation = 0;
    M_priceInstallation = 0;
    totalR_PriceInstallation = 0;
    totalR_KwInstallation = 0;
    R_priceInstallation = 0;
    change() {
        this.toggle = !this.toggle;
    }
     
    constructor(
        injector: Injector,
        private _commonLookupService: CommonLookupServiceProxy,
        private _installerServiceProxy: InstallerServiceProxy,
        private _jobInstallerInvoiceServiceProxy: JobInstallerInvoicesServiceProxy,
        private titleService: Title,
        private _tokenService: TokenService,
        private _httpClient: HttpClient,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " | Ready To Pay Installer Invoice");
        this.uploadUrlstc = AppConsts.remoteServiceBaseUrl + '/Users/ImportInstallationInvoiceFromExcel';

    }
    
    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 

        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
        this.getInstaller();
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Ready To Pay';
            log.section = 'Ready To Pay';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.orgCode =this.allOrganizationUnits[0].code;
            this.getInstallerApproved();
            
        });
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }

        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }
    toggleFiltersData(): void {
        this.FiltersData = !this.FiltersData;
        if (this.FiltersData) {
            this.isSummaryAll = false; // Ensure the other div is hidden
        }
        this.testHeightSize();
    }
    
    toggleSummaryAll(): void {
        this.isSummaryAll = !this.isSummaryAll;
        if (this.isSummaryAll) {
            this.FiltersData = false; // Ensure the other div is hidden
        }
        this.testHeightSize();
    }
    clear() {
        this.installerId = 0;
        this.suburbFilter = '';
        // this.stateNameFilter = '';
        this.stateList = [];

        // let date = new Date();
        // this.startDate = moment(new Date(this.date.getFullYear(), this.date.getMonth(), 1));
        // this.endDate = moment().endOf('day');
        this.startDate =  moment(this.date);
        this.endDate =  moment(this.date);
        this.getInstallerApproved();
    }
    changeOrgCode() {
        this.orgCode = this.allOrganizationUnits.find(x => x.id == this.organizationUnit).code;
    }
    getInstallerApproved(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        
        this.primengTableHelper.showLoadingIndicator();
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._jobInstallerInvoiceServiceProxy.getInstallerApprovedList(
            this.filterName,
            this.organizationUnit,
            this.invoiceStatus,
            this.invoiceType,
            filterText_,
            this.installerId,
            this.suburbFilter,
            this.stateList,
            this.dateFilterType,
            this.startDate,
            this.endDate,
            this.areaNameFilter,
            this.batteryFilter,
            this.isinvoice,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            this.shouldShow = false;
            if (result.totalCount > 0) {
                this.total = result.items[0].summaryCount.total;
                this.paid = result.items[0].summaryCount.paid;
                this.unPaid = result.items[0].summaryCount.unPaid;
                this.totalM_Kw = result.items[0].summaryCount.totalMetroKw;
                this.totalM_Price = result.items[0].summaryCount.totalMetroPrice;
                this.totalR_Kw = result.items[0].summaryCount.totalRegionalKw;
                this.totalR_Price = result.items[0].summaryCount.totalRegionalPrice;
                this.M_price = result.items[0].summaryCount.metroPricePerKw;
                this.R_price = result.items[0].summaryCount.regionalPricePerKw;

                this.totalInspection = result.items[0].summaryCount.totalInspection;
                this.paidInspection = result.items[0].summaryCount.paidInspection;
                this.unPaidInspection = result.items[0].summaryCount.unPaidInspection;
                this.totalM_KwInspection = result.items[0].summaryCount.totalMetroKwInspection;
                this.totalM_PriceInspection = result.items[0].summaryCount.totalMetroPriceInspection;
                this.totalR_KwInspection = result.items[0].summaryCount.totalRegionalKwInspection;
                this.totalR_PriceInspection = result.items[0].summaryCount.totalRegionalPriceInspection;
                this.M_priceInspection = result.items[0].summaryCount.metroPricePerKwInspection;
                this.R_priceInspection = result.items[0].summaryCount.regionalPricePerKwInspection;

                this.totalInstallation = result.items[0].summaryCount.totalInstallation;
                this.paidInstallation = result.items[0].summaryCount.paidInstallation;
                this.unPaidInstallation = result.items[0].summaryCount.unPaidInstallation;
                this.totalM_KwInstallation = result.items[0].summaryCount.totalMetroKwInstallation;
                this.totalM_PriceInstallation = result.items[0].summaryCount.totalMetroPriceInstallation;
                this.totalR_KwInstallation = result.items[0].summaryCount.totalRegionalKwInstallation;
                this.totalR_PriceInstallation = result.items[0].summaryCount.totalRegionalPriceInstallation;
                this.M_priceInstallation = result.items[0].summaryCount.metroPricePerKwInstallation;
                this.R_priceInstallation = result.items[0].summaryCount.regionalPricePerKwInstallation;


                this.totalMaintenance = result.items[0].summaryCount.totalMaintenance;
                this.paidMaintenance = result.items[0].summaryCount.paidMaintenance;
                this.unPaidMaintenance = result.items[0].summaryCount.unPaidMaintenance;
                this.totalM_KwMaintenance = result.items[0].summaryCount.totalMetroKwMaintenance;
                this.totalM_PriceMaintenance = result.items[0].summaryCount.totalMetroPriceMaintenance;
                this.totalR_KwMaintenance = result.items[0].summaryCount.totalRegionalKwMaintenance;
                this.totalR_PriceMaintenance = result.items[0].summaryCount.totalRegionalPriceMaintenance;
                this.M_priceMaintenance = result.items[0].summaryCount.metroPricePerKwMaintenance;
                this.R_priceMaintenance = result.items[0].summaryCount.regionalPricePerKwMaintenance;


            } else {
                this.total = this.paid = this.unPaid = this.totalM_Kw = this.totalM_Price = this.totalR_Kw = this.totalR_Price = this.M_price = this.R_price = 0;
            }
        }, e => {
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    // filterCountries(event): void {
    //     this._commonLookupService.getAllLeadStatusForTableDropdown(event.query).subscribe(result => {
    //         this.allLeadStatus = result;
    //     });
    // }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }
    SummaryAll(){
        this.FiltersData=false;
        this.isSummaryAll=true;
    }
    // navigateToLeadDetail(leadid): void {
    //     this._router.navigate(['/app/main/closedlead/closedlead/viewclosedlead'], { queryParams: { LeadId: leadid } });
    // }

    // deleteLead(lead: LeadDto): void {
    //     this.message.confirm(
    //         '',
    //         this.l('AreYouSure'),
    //         (isConfirmed) => {
    //             if (isConfirmed) {
    //                 // this._leadsServiceProxy.delete(lead.id)
    //                 //     .subscribe(() => {
    //                 //         this.reloadPage();
    //                 //         this.notify.success(this.l('SuccessfullyDeleted'));
    //                 //     });
    //             }
    //         }
    //     );
    // }

    revertPaymet(installer: InstallerPaidDto){
        let invNo = installer.invoiceNo;

        this.message.confirm(
            this.l('AreYouSureYouWantToUnApprovedInvoice', invNo),
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._jobInstallerInvoiceServiceProxy.revertInvoicePayment(installer.id,30).subscribe(() => {
                        let log = new UserActivityLogDto();
                        log.actionId = 26;
                        log.actionNote = 'Revert Installer Invoice Payment';
                        log.section = 'Ready To Pay';
                        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                            .subscribe(() => {
                        }); 
                        this.reloadPage();
                        this.notify.success(this.l('Successfullyreverted'));
                    });
                }
            }
        );
    }


    revertInvoice(installer: InstallerPaidDto): void {
        let invNo = installer.invoiceNo;
        this.message.confirm(
            this.l('AreYouSureYouWantToUnApprovedInvoice', invNo),
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._jobInstallerInvoiceServiceProxy.revertApprovedInvoice(installer.id).subscribe(() => {
                        let log = new UserActivityLogDto();
                        log.actionId = 26;
                        log.actionNote = 'Installer Invoice Unapproved';
                        log.section = 'Ready To Pay';
                        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                            .subscribe(() => {
                        }); 
                        this.reloadPage();
                        this.notify.success(this.l('Successfullyreverted'));
                    });
                }
            }
        );
    }
   
    getInstaller() {
        this._installerServiceProxy.getAllInstallers(this.organizationUnit).subscribe(result => {
            this.InstallerList = result;
        });
    }

    installerSearchResult: any [];
    filterInstaller(event): void {
        this.installerSearchResult = Object.assign([], this.InstallerList).filter(
            item => item.displayName.toLowerCase().indexOf(event.query.toLowerCase()) > -1
        )
    }

    selectInstaller(event): void {
        this.installerId = event.id;
        this.getInstallerApproved();
    }

    suburbSuggestions: string[];
    filtersuburbs(event): void {
        this._commonLookupService.getOnlySuburb(event.query).subscribe(output => {
            this.suburbSuggestions = output;
        });
    }

    
    uploadExcelstc(data: { files: File }): void {
        
        var headers_object = new HttpHeaders().set("Authorization", "Bearer " + this._tokenService.getToken());

        const httpOptions = {
            headers: headers_object
        };
        const formData: FormData = new FormData();
        const file = data.files[0];
        formData.append('file' + ',' + this.organizationUnit, file, file.name);
        this._httpClient
            .post<any>(this.uploadUrlstc, formData, httpOptions)
            .pipe(finalize(() => this.excelFileUpload.clear()))
            .subscribe(response => {
                if (response.success) {
                    this.notify.success(this.l('ImportInstallerInvoicePaymentProcessStart'));
                } else if (response.error != null) {
                    this.notify.error(this.l('ImportInstallerInvoicePaymentUploadFailed'));
                }
            });
    }

    onUploadExcelErrorstc(): void {
        this.notify.error(this.l('ImportInstallerInvoicePaymentUploadFailed'));
    }
   
    exportToExcel(excelorcsv): void {
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._jobInstallerInvoiceServiceProxy.getInstallerApprovedListToExcel(
            this.filterName,
            this.organizationUnit,
            this.invoiceStatus,
            this.invoiceType,
            filterText_,
            this.installerId,
            this.suburbFilter,
            this.stateList,
            this.dateFilterType,
            this.startDate,
            this.endDate,
            excelorcsv,
            this.areaNameFilter,
            this.batteryFilter
        )
        .subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
         });
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Ready To Pay';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Ready To Pay';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }

}
