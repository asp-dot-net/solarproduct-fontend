import { Component, Injector, ViewChild, ViewEncapsulation, HostListener } from '@angular/core';
import { CommonLookupServiceProxy, JobInstallerInvoicesServiceProxy, LeadDto, LeadSourceLookupTableDto, LeadsServiceProxy, LeadStateLookupTableDto, LeadStatusLookupTableDto, OrganizationUnitDto, TokenAuthServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy, UserServiceProxy } from '@shared/service-proxies/service-proxies';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { AppConsts } from '@shared/AppConsts';
import { HttpClient } from '@angular/common/http';
import { finalize } from 'rxjs/operators';
import { FileUpload } from 'primeng/fileupload';
import { OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Title } from '@angular/platform-browser';

@Component({
    selector: 'app-installerInvoiceFileList',
    templateUrl: './installerInvoiceFileList.component.html',
    styleUrls: ['./installerInvoiceFileList.component.css'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class InstallerInvoiceFileListComponent extends AppComponentBase implements OnInit {

    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    organizationUnitlength: number=0;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };
    @ViewChild('ExcelFileUpload', { static: false }) excelFileUpload: FileUpload;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    uploadUrl: string;
    advancedFiltersAreShown = false;
    filterText = '';
    copanyNameFilter = '';
    emailFilter = '';
    phoneFilter = '';
    mobileFilter = '';
    addressFilter = '';
    requirementsFilter = '';
    postCodeSuburbFilter = '';
    stateNameFilter = '';
    streetNameFilter = '';
    postCodePostalCode2Filter = '';
    leadSourceNameFilter = '';
    leadSourceIdFilter = 0;
    //leadSubSourceNameFilter = '';
    leadStatusName = '';
    typeNameFilter = '';
    areaNameFilter = '';
    leadStatus: any;
    allLeadStatus: LeadStatusLookupTableDto[];
    leadStatusId: number = 0;
    StartDate: moment.Moment;
    EndDate: moment.Moment;
    PageNo: number;
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnit = 0;

    suburbSuggestions: string[];
    sampleDateRange: moment.Moment[] = [moment().add(-7, 'days').endOf('day'), moment().startOf('day')];
    dateFilterType = 'Creation';
    firstrowcount = 0;
    last = 0;

    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 260;
    toggle: boolean = true;

    change() {
        this.toggle = !this.toggle;
    }

    constructor(
        injector: Injector,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _tokenAuth: TokenAuthServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _httpClient: HttpClient,
        private _userServiceProxy: UserServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _jobInstallerInvoiceServiceProxy: JobInstallerInvoicesServiceProxy,
        private _router: Router,
        private titleService: Title
    ) {
        super(injector);
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/Users/ImportLeadFromExcel';
        this.titleService.setTitle(this.appSession.tenancyName + " |  Invoice File List");
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Invoice File List';
            log.section = 'Invoice File List';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.getinstallerinvoicefilelist();
        });
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  

    clear() {
        this.filterText = '';
        this.postCodeSuburbFilter = '';
        this.stateNameFilter = '';
        this.leadSourceIdFilter = 0;
        this.postCodePostalCode2Filter = '';
        this.leadSourceNameFilter = '';
        this.sampleDateRange = [moment().add(-7, 'days').endOf('day'), moment().startOf('day')];
        this.getinstallerinvoicefilelist();
    }

    filtersuburbs(event): void {
        this._commonLookupService.getOnlySuburb(event.query).subscribe(output => {
            this.suburbSuggestions = output;
        });
    }

    getinstallerinvoicefilelist(event?: LazyLoadEvent) {
        this.StartDate = this.sampleDateRange[0];
        this.EndDate = this.sampleDateRange[1];
        this.primengTableHelper.showLoadingIndicator();

        this._jobInstallerInvoiceServiceProxy.getAllInvoiceFiles(
            this.organizationUnit,
            this.organizationUnit,
            this.StartDate,
            this.EndDate,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            this.shouldShow = false;
        });
    }

    downloadfile(docpath: string, filename: string): void {
        let FileName = AppConsts.docUrl + "/" + docpath + filename;
        window.open(FileName, "_blank");
    }

    // exportToExcel(): void {
    //     this._leadsServiceProxy.getLeadsToExcel(
    //         this.filterText,
    //         this.copanyNameFilter,
    //         this.emailFilter,
    //         this.phoneFilter,
    //         this.mobileFilter,
    //         this.addressFilter,
    //         this.requirementsFilter,
    //         this.postCodeSuburbFilter,
    //         this.stateNameFilter,
    //         this.streetNameFilter,
    //         this.postCodePostalCode2Filter,
    //         this.leadSourceNameFilter,
    //         //this.leadSubSourceNameFilter,
    //     )
    //         .subscribe(result => {
    //             this._fileDownloadService.downloadTempFile(result);
    //         });
    // }
}
