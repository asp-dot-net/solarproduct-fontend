import { EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CreateOrEditJobInstallerInvoiceDto, JobInstallerInvoicesServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import * as moment from 'moment';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/internal/operators/finalize';

@Component({
  selector: 'readyTopay',
  templateUrl: './readyTopay.component.html',
  styleUrls: ['./readyTopay.component.css']
})
export class ReadyTopayComponent extends AppComponentBase {


    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    saving = false;
    distAppliedDate: moment.Moment;
    MeterNumber = "";
    ApprovalRefNo = "";
    item: CreateOrEditJobInstallerInvoiceDto = new CreateOrEditJobInstallerInvoiceDto();
    prorityId = 0;
    comment = "";
    remarks: string;
    date: moment.Moment;

    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _jobInstallerInvoiceServiceProxy: JobInstallerInvoicesServiceProxy,
        private _router: Router
    ) {
        super(injector);
    }

  SectionName = '';
  show(InstallerInvoiceId?: number, sectionId?: number,section = ''): void {
        this.SectionName = section;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Open ApprovedDate For Pending Installer Invoice';
        log.section = section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
        this.showMainSpinner();
        const date = new Date();
        this._jobInstallerInvoiceServiceProxy.getDataForEdit(InstallerInvoiceId).subscribe(result => {
            this.item = new CreateOrEditJobInstallerInvoiceDto();
            this.item = result;
            this.item.approedDate = moment(date);
            this.prorityId = this.item.priorityId;
            this.item.sectionId = sectionId;
            this.modal.show();
            this.hideMainSpinner();
        }, e => {
            this.hideMainSpinner();
        });
    }

    save(): void {
        this.saving = true;
        this.item.isPaid = true;
        this.item.priorityId = this.prorityId;
       
        this._jobInstallerInvoiceServiceProxy.updateApprovedInvoice(this.item)
        .pipe(finalize(() => { this.saving = false;}))
        .subscribe(() => {
            let log = new UserActivityLogDto();
            log.actionId = 26;
            log.actionNote ='Installer Invoice Approved';
            log.section = this.SectionName;
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.modal.hide();
            this.remarks = "";
            this.date = null;
            this.notify.info(this.l('SavedSuccessfully'));
            this.modalSave.emit(null);
            
        });
    }

    close(): void {
        this.modal.hide();
    }
}
