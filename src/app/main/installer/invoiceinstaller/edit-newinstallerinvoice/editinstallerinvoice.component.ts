import { Component, ViewChild, Injector, Output, EventEmitter, Optional } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { CommonLookupDto, CommonLookupServiceProxy, CreateOrEditJobInstallerInvoiceDto, GetIncomeStatisticsDataOutput, GetInstallerInvoiceForViewDto, InstallerInvoicePriceListDto, InstallerServiceProxy, JobInstallerInvoicesServiceProxy, JobJobTypeLookupTableDto, UpdateProfilePictureInput, UploadDocumentInput, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { FileItem, FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { FileUpload } from 'primeng';
import { AppConsts } from '@shared/AppConsts';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';

@Component({
  selector: 'editinstallerinvoice',
  templateUrl: './editinstallerinvoice.component.html',
  styleUrls: ['./editinstallerinvoice.component.css']
})
export class EditinstallerinvoiceComponent extends AppComponentBase {

  @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  public uploader: FileUploader;

  active = false;
  saving = false;
  private _uploaderOptions: FileUploaderOptions = {};
  installerInv: CreateOrEditJobInstallerInvoiceDto = new CreateOrEditJobInstallerInvoiceDto();

  sampleDateRange: moment.Moment[] = [
    moment().add(-7, 'days').endOf('day'),
    moment().startOf('day')];

  // leadSourceSourceName = '';
  // Installeruser: JobJobTypeLookupTableDto[];
  jobnumberlist: CommonLookupDto[];
  public maxfileBytesUserFriendlyValue = 5;
  public temporaryPictureUrl: string;
  public useGravatarProfilePicture = false;
  filenName = [];

  organizationId = 0;
  InstallerList: CommonLookupDto[];
  searchResult = [];
  priceItemList: any[];
  installerInvoiceType : any[];

  constructor(
    injector: Injector,
    private _jobInstallerInvoiceServiceProxy: JobInstallerInvoicesServiceProxy,
    private _tokenService: TokenService,
    private _installerServiceProxy: InstallerServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _commonLookupServiceProxy : CommonLookupServiceProxy
  ) {
    super(injector);
  }

  SectionName = '';
  show(orgId: number, insatllerInvoiceId?: number, section = ''): void {
    this.SectionName = section;
    let log = new UserActivityLogDto();
    log.actionId = 79;
    log.actionNote ='Open For Edit Installer Invoice';
    log.section = section;
    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {
    });
    this.showMainSpinner();

    this.organizationId = orgId;
    this._installerServiceProxy.getAllInstallers(orgId).subscribe(result => {
      this.InstallerList = [];
      this.InstallerList = result;
    });
    this._commonLookupServiceProxy.getPriceItemListDropdown().subscribe(result => {
      this.priceItemList = result;
    })
    this._commonLookupServiceProxy.getAllInstallerInvoiceType().subscribe(result => {
      this.installerInvoiceType = result
    })
    this._jobInstallerInvoiceServiceProxy.getDataForEdit(insatllerInvoiceId).subscribe(result => {
      this.installerInv = new CreateOrEditJobInstallerInvoiceDto();
      this.installerInv = result;
      if(result.installerInvoicePriceListDtos.length == 0){
        this.installerInv.installerInvoicePriceListDtos = [];
        this.installerInv.installerInvoicePriceListDtos.push(new InstallerInvoicePriceListDto);
      }

      this.active = true;
      this.initializeModal();
      this.modal.show();
      this.hideMainSpinner();
    }, e => {
      this.hideMainSpinner();
    });
  }

  onShown(): void {
  }


  initializeModal(): void {
    this.active = true;
    this.temporaryPictureUrl = '';
    this.initFileUploader();
  }


  filechangeEvent(event: any): void {
    debugger
    if (event.target.files[0].size > 5242880) { //5MB
      this.message.warn(this.l('ProfilePicture_Warn_SizeLimit', this.maxfileBytesUserFriendlyValue));
      return;
    }
    this.uploader.clearQueue();
    this.uploader.addToQueue([<File>event.target.files[0]]);
  }

  initFileUploader(): void {
    this.uploader = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Profile/UploadFile' });
    this._uploaderOptions.autoUpload = false;
    this._uploaderOptions.authToken = 'Bearer ' + this._tokenService.getToken();
    this._uploaderOptions.removeAfterUpload = true;
    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
    };

    this.uploader.onBuildItemForm = (fileItem: FileItem, form: any) => {
      form.append('FileType', fileItem.file.type);
      form.append('FileName', 'ProfilePicture');
      form.append('FileToken', this.guid());
      this.filenName.push(fileItem.file.name);
    };

    this.uploader.onSuccessItem = (item, response, status) => {
      const resp = <IAjaxResponse>JSON.parse(response);
      if (resp.success) {
        this.updateInstallerInvoiceFile(resp.result.fileToken, resp.result.fileName, resp.result.fileType, resp.result.filePath);
      } else {
        this.message.error(resp.error.message);
      }
    };

    this.uploader.setOptions(this._uploaderOptions);
  }
  updateInstallerInvoiceFile(fileToken: string, fileName: string, fileType: string, filePath: string): void {
    this.installerInv.fileToken = fileToken;
    this.installerInv.fileName = this.filenName[0];
    
    this.SaveData();
  }

  guid(): string {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
  }

  saveJobInstallerInvoic(): void {
   
    this.saving = true; if(this.uploader.queue.length == 0)
    {
      this.SaveData();
    }
    else{
      this.uploader.uploadAll();
    }
  };
  
  SaveData(): void {
    this._jobInstallerInvoiceServiceProxy.checkInstallerDetail(this.installerInv).subscribe(result => {
      
      if(!result)
      {
          this.installerInv.sectionId = 20 ;
          this._jobInstallerInvoiceServiceProxy.createOrEdit(this.installerInv).pipe(finalize(() => 
          { this.saving = false; })).subscribe(() => {
            let log = new UserActivityLogDto();
            log.actionId = 26;
            log.actionNote ='Installer Invoice Modified';
            log.section = this.SectionName;
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.notify.info(this.l('SavedSuccessfully'));
            this.modal.hide();
            this.active = false;
            this.modalSave.emit(null);
          }
          );
      }
      else{ 
        this.saving = false;
        document.getElementById('invoiceNo').focus();
        this.notify.warn(this.l('InvoiceNoExists'));
      }
  });
   

  }

  close(): void {
    this.active = false;
    this.modal.hide();
  }

  filterjobnumber(event): void {
    this._jobInstallerInvoiceServiceProxy.getSearchFilterByOrg(event.query, this.installerInv.installation_Maintenance_Inspection, this.organizationId).subscribe(result => {
        this.searchResult = result;
    });
  }

  OnSelectDetails(event): void {

    this.installerInv.jobId = event.jobId;
    this.installerInv.jobNumber = event.jobNumber;

  }

  installerSearchResult: any [];
  filterInstaller(event): void {
    this.installerSearchResult = Object.assign([], this.InstallerList).filter(
      item => item.displayName.toLowerCase().indexOf(event.query.toLowerCase()) > -1
    )
  }

  selectInstaller(event): void {
    this.installerInv.installerId = event.id;
    this.installerInv.installerName = event.displayName;
  }

  onInvoiceNoChange(): void {  
    
  }
  addPriceList(): void {
    this.installerInv.installerInvoicePriceListDtos.push(new InstallerInvoicePriceListDto);
  }

  removePriceList(rowitem): void {
 
    if (this.installerInv.installerInvoicePriceListDtos.length == 1)
        return;
    // this.JobVariations = this.JobVariations.filter(item => item.variationId != JobVariation.variationId);

    if (this.installerInv.installerInvoicePriceListDtos.indexOf(rowitem) === -1) {
        // this.JobVariations.push(rowitem);
    } else {

        this.installerInv.installerInvoicePriceListDtos.splice(this.installerInv.installerInvoicePriceListDtos.indexOf(rowitem), 1);
        this.calculateRate();
    }

  }

  calculateRate() : void{
    
    let totalamount = 0;
    this.installerInv.installerInvoicePriceListDtos.forEach(function (item) {
      if(item.amount > 0){
        totalamount = parseFloat(totalamount.toString()) + parseFloat(item.amount.toString()); 
      }
  })
  
  this.installerInv.amount = totalamount ; 
  if(this.installerInv.isGSTInclude){
    this.installerInv.amount = totalamount + (totalamount *10 /100)
  }
  }

}
