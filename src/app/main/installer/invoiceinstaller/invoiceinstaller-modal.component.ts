import { Component, ElementRef, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { JobsServiceProxy, GetJobForEditOutput,GetInstallerInvoiceForViewDto, CreateOrEditJobDto, JobElecDistributorLookupTableDto, JobElecRetailerLookupTableDto, JobRoofAngleLookupTableDto, CommonLookupDto, LeadsServiceProxy, InstallerServiceProxy, JobInstallerInvoicesServiceProxy, CreateOrEditJobInstallerInvoiceDto, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import * as _ from 'lodash';
import * as moment from 'moment';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';
@Component({
  selector: 'invoiceinstallerModal',
  templateUrl: './invoiceinstaller-modal.component.html',
  styleUrls: ['./invoiceinstaller-modal.component.css']
})
export class InvoiceinstallerModalComponent  extends AppComponentBase {
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
     
    @ViewChild('addModal') modal: ModalDirective;
   
    saving = false;
    distAppliedDate: moment.Moment;
    MeterNumber = "";
    ApprovalRefNo = "";
    item: CreateOrEditJobInstallerInvoiceDto = new CreateOrEditJobInstallerInvoiceDto();
    comment = "";
    jobElecDistributors: JobElecDistributorLookupTableDto[];
    jobElecRetailers: JobElecRetailerLookupTableDto[];
    jobRoofAngles: JobRoofAngleLookupTableDto[];
    allUsers: CommonLookupDto[];
    currentElecDistId: any;
    currentdistApproveBy: any;
    elecRetailerId: any;
    leadCompanyName: any;
    distExpiryDate: moment.Moment;
    updateExpiryDate: moment.Moment;
    ischange=false;
    sectionId = 0;
    
    constructor(
        injector: Injector,
        private _jobsServiceProxy: JobsServiceProxy,
        private _router: Router,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _installerServiceProxy: InstallerServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _jobInstallerInvoiceServiceProxy: JobInstallerInvoicesServiceProxy,
    ) {
        super(injector);
    }

  SectionName = '';
  show(id: number, sectionId?: number,section =''): void {
      this.SectionName = section;
      let log = new UserActivityLogDto();
      log.actionId = 79;
      log.actionNote ='Open Notes For Pending Installer Invoice';
      log.section = section;
      this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
          .subscribe(() => {
      });
      this.sectionId = sectionId;
      this._jobInstallerInvoiceServiceProxy.getInstallerInvoiceForEdit(id).subscribe(result => {
          this.item = new CreateOrEditJobInstallerInvoiceDto();
          this.item = result;
          this.item.sectionId = this.sectionId;
          this.modal.show();
      });
    }

    save(): void {
      this.saving = true;
      this._jobInstallerInvoiceServiceProxy.updateAddNotes(this.item)
          .pipe(finalize(() => { this.saving = false; }))
          .subscribe(() => {
            let log = new UserActivityLogDto();
            log.actionId = 26;
            log.actionNote = this.item.isVerify == true ? "Installer Invoice Verified" : "Updated Installer Invoice Notes";
            log.section = this.SectionName;
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
              this.notify.info(this.l('SavedSuccessfully'));
              this.modalSave.emit(null);
              this.modal.hide();
            }
          );
    }

  close(): void {
    this.modal.hide();
  }

}