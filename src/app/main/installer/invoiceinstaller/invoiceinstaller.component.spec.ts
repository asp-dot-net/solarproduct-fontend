import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceinstallerComponent } from './invoiceinstaller.component';

describe('InvoiceinstallerComponent', () => {
  let component: InvoiceinstallerComponent;
  let fixture: ComponentFixture<InvoiceinstallerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceinstallerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceinstallerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
