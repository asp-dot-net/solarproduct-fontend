import { Component, Injector, ViewEncapsulation, ViewChild, OnInit, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLookupDto, CommonLookupServiceProxy, GetInstallerInvoiceForViewDto, GetJobForViewDto, InstallerPaidDto, InstallerServiceProxy, InvoicePaymentInvoicePaymentMethodLookupTableDto, InvoicePaymentsServiceProxy, JobFinanceOptionLookupTableDto, JobInstallerInvoicesServiceProxy, JobPaymentOptionLookupTableDto, JobsServiceProxy, JobStatusTableDto, LeadsServiceProxy, LeadStateLookupTableDto, OrganizationUnitDto, TokenAuthServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy, UserServiceProxy } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { ViewApplicationModelComponent } from '@app/main/jobs/jobs/view-application-model/view-application-model.component';
import { AddActivityModalComponent } from '@app/main/myleads/myleads/add-activity-model.component';
import { ViewMyLeadComponent } from '@app/main/myleads/myleads/view-mylead.component';
import { CreateOrEditJobInvoiceModalComponent } from '@app/main/jobs/jobs/edit-jobinvoice-model.component';
import { InvoiceinstallerModalComponent } from './invoiceinstaller-modal.component';
import { EditinstallerinvoiceComponent } from './edit-newinstallerinvoice/editinstallerinvoice.component';
import { ReadyTopayComponent } from '../paidinstaller/readyTopay.component';
import { AppConsts } from '@shared/AppConsts';
import { InvoiceInstallerSmsemailModelComponent } from './invoiceinstaller-smsemail-model/invoiceinstaller-smsemail-model.component';
import { Title } from '@angular/platform-browser';
import { NotifyModelComponent } from '@app/main/activitylog/notify-modal.component';
import { CommentModelComponent } from '@app/main/activitylog/comment-modal.component';
import { ReminderModalComponent } from '@app/main/activitylog/reminder-modal.component';
import { ToDoModalComponent } from '@app/main/activitylog/todo-modal.component';
import { finalize } from 'rxjs/operators';
import { EmailModelComponent } from '@app/main/activitylog/email-modal.component';
import { InvoiceDetailModalComponent } from '../radytoPayInstaller/invoicedetail.component';

@Component({
    templateUrl: './invoiceinstaller.component.html',
    styleUrls: ['./invoiceinstaller.component.css']
})
export class InvoiceinstallerComponent extends AppComponentBase implements OnInit {
    @ViewChild('viewApplicationModal', { static: true }) viewApplicationModal: ViewApplicationModelComponent;
    @ViewChild('invoiceinstallerModal', { static: true }) invoiceinstallerModal: InvoiceinstallerModalComponent;
    @ViewChild('addActivityModal') addActivityModal: AddActivityModalComponent;
    @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
    @ViewChild('editJobInvoiceModal', { static: true }) editJobInvoiceModal: CreateOrEditJobInvoiceModalComponent;
    
    @ViewChild('editinstallerinvoice', { static: true }) editinstallerinvoice: EditinstallerinvoiceComponent;
    @ViewChild('readyTopay', { static: true }) readyTopay: ReadyTopayComponent;
    @ViewChild('invoiceinstallersmsEmailModel', { static: true }) invoiceinstallersmsEmailModel: InvoiceInstallerSmsemailModelComponent;

    @ViewChild('notifyModel', { static: true }) notifyModel: NotifyModelComponent;
    @ViewChild('commentModel', { static: true }) commentModel: CommentModelComponent;
    @ViewChild('todoModal', { static: true }) todoModal: ToDoModalComponent;
    @ViewChild('ReminderModal', { static: true }) ReminderModal: ReminderModalComponent;
    @ViewChild('emailModel', { static: true }) emailModel: EmailModelComponent;
    @ViewChild('invoiceDetailModal', { static: true }) invoiceDetailModal: InvoiceDetailModalComponent;
    
    FiltersData = false;
    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    organizationUnitlength: number = 0;
    
    id: number;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    organizationUnit = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    
    allStates: LeadStateLookupTableDto[];
    suburbSuggestions: string[];

    allInvoicePaymentMethods: InvoicePaymentInvoicePaymentMethodLookupTableDto[];
    suburbFilter = '';
    stateNameFilter = '';
    
    installerId: number = 0;
    dateFilterType = 'CreationDate';
    date = new Date();
    // firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    // startDate = moment(this.firstDay);
    // endDate = moment().endOf('day');
    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);
    InstallerList: CommonLookupDto[];
    total = 0;
    pending = 0;
    verified = 0;
    
    ExpandedView: boolean = true;
    SelectedLeadId: number = 0;
    invoiceStatus = 0; 
    invoiceType = '';
    jobPaymentOptions: JobPaymentOptionLookupTableDto[];
    
    firstrowcount = 0;
    last = 0;

    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 330;
    toggle: boolean = true;
    filterName = 'JobNumber';
    orgCode = '';

    change() {
        this.toggle = !this.toggle;
    }

    constructor(
        injector: Injector,
        private _invoicePaymentsServiceProxy: InvoicePaymentsServiceProxy,
        private _userServiceProxy: UserServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _installerServiceProxy: InstallerServiceProxy,
        private _jobsServiceProxy: JobsServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _jobInstallerInvoiceServiceProxy: JobInstallerInvoicesServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private titleService: Title
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Pending Installer Invoice");
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 

        this.getInstaller();
       
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Pending Installer Invoice';
            log.section = 'Pending Installer Invoice';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.orgCode = this.allOrganizationUnits[0].code;
            this.getInstallerInvoice();
            //this.getCount(this.organizationUnit);
        });

        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ; // -82
        }
        else {
            this.testHeight = this.testHeight - -82 ; // -82
        }
    }
    
    getInstaller() {
        this._installerServiceProxy.getAllInstallers(this.organizationUnit).subscribe(result => {
            this.InstallerList = result;
        });
    }

    filtersuburbs(event): void {
        this._commonLookupService.getOnlySuburb(event.query).subscribe(output => {
            this.suburbSuggestions = output;
        });
    }

    reloadPages(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    revertInvoice(installerInvoice: GetInstallerInvoiceForViewDto): void {
        debugger;
        let invono = installerInvoice.invoiceNo;
        this.message.confirm(
            this.l('AreYouSureYouWantToUnVerifiedInvoice',invono),
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._jobInstallerInvoiceServiceProxy.revertVerifiedInvoice(installerInvoice.id)
                        .subscribe(() => {
                            let log = new UserActivityLogDto();
                            log.actionId = 26;
                            log.actionNote ='Installer Invoice Unverified';
                            log.section = 'Pending Installer Invoice';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });
                            this.reloadPages();
                            this.notify.success(this.l('Successfullyreverted'));

                        });
                }
            }
        );
    }
    changeOrgCode() {
        this.orgCode = this.allOrganizationUnits.find(x => x.id == this.organizationUnit).code;
    }
    getInstallerInvoice(event?: LazyLoadEvent) {

        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        
        this.primengTableHelper.showLoadingIndicator();
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._jobInstallerInvoiceServiceProxy.getPendingInstallerInvoice(
            this.filterName,
            this.organizationUnit,
            this.invoiceStatus, 
            this.invoiceType,
            filterText_,
            this.installerId,
            this.suburbFilter,
            this.stateNameFilter,
            this.dateFilterType,
            this.startDate,
            this.endDate,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
           
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.tableRecords = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            this.shouldShow = false;
            if (result.totalCount > 0) {
                this.total = result.items[0].summaryCount.total;
                this.pending = result.items[0].summaryCount.pending;
                this.verified = result.items[0].summaryCount.verified;
            } else {
                this.total = 0;
                this.pending = 0;
                this.verified = 0;
            }
        });
    }

    reloadPage($event): void {
        this.ExpandedView = true;
        this.paginator.changePage(this.paginator.getPage());
    }

    clear() {
        this.installerId = 0;
        this.suburbFilter = '';
        this.stateNameFilter = '';

        // let date = new Date();
        // this.startDate = moment(new Date(this.date.getFullYear(), this.date.getMonth(), 1));
        // this.endDate = moment().endOf('day');
        this.startDate =  moment(this.date);
        this.endDate =  moment(this.date);
        this.getInstallerInvoice();
    }

    exportToExcel(): void {
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._jobInstallerInvoiceServiceProxy.getPendingInstallerInvoiceExcel(
            this.filterName,
            this.organizationUnit,
            this.invoiceStatus, 
            this.invoiceType,
            filterText_,
            this.installerId,
            this.suburbFilter,
            this.stateNameFilter,
            this.dateFilterType,
            this.startDate,
            this.endDate,
            0
        )
        .subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
         });
    }

    expandGrid() {
        this.ExpandedView = true;
    }

    navigateToLeadDetail(leadid): void {
        this.ExpandedView = !this.ExpandedView;
        this.ExpandedView = false;
        this.SelectedLeadId = leadid;
        this.viewLeadDetail.showDetail(leadid, '', 29,0,'Pending Installer Invoice');
    }

    // addNotes(jobid, trackerid?): void {
    //     this.invoiceinstallerModal.show(jobid, trackerid);
    // }

    delete(installerInvoice: GetInstallerInvoiceForViewDto): void {
        let invono = installerInvoice.invoiceNo;
        this.message.confirm(
            this.l('InstallerInvoiceDeleteWarningMessage', invono),
            this.l('AreYouSure'),
            isConfirmed => {
                if (isConfirmed) {
                    this._jobInstallerInvoiceServiceProxy.delete(installerInvoice.id).subscribe(() => {
                        let log = new UserActivityLogDto();
                        log.actionId = 26;
                        log.actionNote = 'Installer Invoices Deleted';
                        log.section = 'Pending Installer Invoice';
                        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                            .subscribe(() => {
                        }); 
                        this.getInstallerInvoice();
                        this.notify.success(this.l('SuccessfullyDeleted'));
                    });
                }
            }
        );
    }

    installerSearchResult: any [];
    filterInstaller(event): void {
        this.installerSearchResult = Object.assign([], this.InstallerList).filter(
            item => item.displayName.toLowerCase().indexOf(event.query.toLowerCase()) > -1
        )
    }

    selectInstaller(event): void {
        this.installerId = event.id;
        this.getInstallerInvoice();
    }

    saving = false;
    count: number = 0;
    tableRecords: any;

    checkAll(ev) {
        this.tableRecords.forEach(x => x.isSelected = ev.target.checked);
    }

    isAllChecked() {
        if (this.tableRecords)
            return this.tableRecords.every(_ => _.isSelected);
    }

    verify(): void {
        this.showMainSpinner();
        let selectedids = [];
        this.saving = true;

        this.primengTableHelper.records.forEach(function (record) {
            if (record.isSelected) {
                selectedids.push(record.id);
            }
        });
        if (selectedids.length == 0) {
            this.notify.warn(this.l('NoDataSelected'));
            this.saving = false;
            return;
        }

        this._jobInstallerInvoiceServiceProxy.verifyOrReadyToPay(this.invoiceStatus, selectedids)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {

                this.getInstallerInvoice();
                this.hideMainSpinner();
                this.saving = false;
                let log = new UserActivityLogDto();
                log.actionId = 26;
                log.actionNote = this.invoiceStatus == 1 ? 'Installer Invoice Verify' : 'Installer Invoice Approved';
                log.section = 'Pending Installer Invoice';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
                if(this.invoiceStatus == 1)
                {
                    this.notify.info(selectedids.length + " Invoice Verified");
                }
                else {
                    this.notify.info(selectedids.length + " Invoice Ready To Pay");
                }
                
            }, err => { this.hideMainSpinner(); }
        );
    }

    verifybtn: boolean = false;
    verifyShow() {
        if(this.invoiceStatus == 1 && this.permission.isGranted('Pages.Installer.Verify'))
        {
            this.verifybtn = true;
        }
        else if(this.invoiceStatus == 2 && this.permission.isGranted('Pages.Installer.ReadyToPay'))
        {
            this.verifybtn = true;
        }
        else{
            this.verifybtn = false;
        }
        
    }
    
    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Pending Installer Invoice';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Pending Installer Invoice';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }

}
