import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceinstallerModalComponent } from './invoiceinstaller-modal.component';

describe('InvoiceinstallerModalComponent', () => {
  let component: InvoiceinstallerModalComponent;
  let fixture: ComponentFixture<InvoiceinstallerModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceinstallerModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceinstallerModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
