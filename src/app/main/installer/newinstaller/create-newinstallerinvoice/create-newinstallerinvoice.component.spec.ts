import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateNewinstallerinvoiceComponent } from './create-newinstallerinvoice.component';

describe('CreateNewinstallerinvoiceComponent', () => {
  let component: CreateNewinstallerinvoiceComponent;
  let fixture: ComponentFixture<CreateNewinstallerinvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateNewinstallerinvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateNewinstallerinvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
