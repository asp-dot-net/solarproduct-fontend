import { Component, ViewChild, Injector, Output, EventEmitter, Optional } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { CommonLookupDto,CommonLookupInstaller, CreateOrEditJobInstallerInvoiceDto, JobInstallerInvoicesServiceProxy, JobJobTypeLookupTableDto, UpdateProfilePictureInput, UploadDocumentInput,NameValueOfString, NameValueOfInt32, InstallationServiceProxy, InstallerServiceProxy, CommonLookupServiceProxy, InstallerInvoicePriceListDto, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { FileItem, FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { FileUpload } from 'primeng';
import { AppConsts } from '@shared/AppConsts';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { datepickerAnimation } from 'ngx-bootstrap/datepicker/datepicker-animations';
import { result } from 'lodash';

@Component({
  selector: 'createnewinstallerinvoice',
  templateUrl: './create-newinstallerinvoice.component.html',
  styleUrls: ['./create-newinstallerinvoice.component.css']
})
export class CreateNewinstallerinvoiceComponent extends AppComponentBase {

  @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  
  public uploader: FileUploader;
  active = false;
  saving = false;
  private _uploaderOptions: FileUploaderOptions = {};
  installerInv: CreateOrEditJobInstallerInvoiceDto = new CreateOrEditJobInstallerInvoiceDto();
  sampleDateRange: moment.Moment[] = [
    moment().add(-7, 'days').endOf('day'),
    moment().startOf('day')];

  leadSourceSourceName = '';
  filteredinstaller: string[];
  
  jobIds: string;
  installerIds: string;
  // Installeruser: JobJobTypeLookupTableDto[];
  jobnumberlist: CommonLookupDto[];
  public maxfileBytesUserFriendlyValue = 5;
  public temporaryPictureUrl: string;
  public useGravatarProfilePicture = false;
  filenName = [];

  organizationId = 0;
  searchResult: any [];
  installerInvoiceType : any[];
  InstallerList: CommonLookupInstaller[];
  priceItemList: any[];
  constructor(
    injector: Injector,
    private _jobInstallerInvoiceServiceProxy: JobInstallerInvoicesServiceProxy,
    private _tokenService: TokenService,
    private _installerServiceProxy : InstallerServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _commonLookupServiceProxy :CommonLookupServiceProxy
  ) {
    super(injector);
  }

  SectionName = '';
  show(orgId: number, InvoiceId?: number, jobNumber? : string,section =''): void {
    debugger;
    this.organizationId = orgId;
    this.SectionName = section;
    let log = new UserActivityLogDto();
    log.actionId = 79;
    log.actionNote ='Open For Create New Installer Invoice';
    log.section = section;
    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {
    });
    this._installerServiceProxy.getAllInstallersWithCompnayName(orgId).subscribe(result => {
      this.InstallerList = [];
      this.InstallerList = result;
    });
    this._commonLookupServiceProxy.getPriceItemListDropdown().subscribe(result => {
      this.priceItemList = result;
    })
    
    this._commonLookupServiceProxy.getAllInstallerInvoiceType().subscribe(result => {
      this.installerInvoiceType = result
    })

    this.initializeModal();

    this.installerInv = new CreateOrEditJobInstallerInvoiceDto();
      
    const date = new Date();
    this.installerInv.invDate = moment(date);
    this.installerInv.invoiceIssuedDate = moment(date);
    //this.installerInv.jobNumber = jobNumber;
    if(jobNumber){
      debugger;
      let event ={query : ''};
      event.query = jobNumber
      this.filterjobnumber(event);
      this.OnSelectDetails(this.searchResult[0])
    }
    this.active = true;
    this.modal.show();
    this.installerInv.installerInvoicePriceListDtos = [];
    this.installerInv.installerInvoicePriceListDtos.push(new InstallerInvoicePriceListDto);
  }

  onShown(): void {
    document.getElementById('jobIds').focus();
  }

  initializeModal(): void {
    this.active = true;
    this.temporaryPictureUrl = '';
    this.initFileUploader();
  }

  filechangeEvent(event: any): void {
    debugger;
    if (event.target.files[0].size > 5242880) { //5MB
      this.message.warn(this.l('ProfilePicture_Warn_SizeLimit', this.maxfileBytesUserFriendlyValue));
      return;
    }
    this.uploader.clearQueue();
    this.uploader.addToQueue([<File>event.target.files[0]]);
  }
  
  initFileUploader(): void {
    debugger;
    this.uploader = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Profile/UploadFile' });
    this._uploaderOptions.autoUpload = false;
    this._uploaderOptions.authToken = 'Bearer ' + this._tokenService.getToken();
    this._uploaderOptions.removeAfterUpload = true;
    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
    };

    this.uploader.onBuildItemForm = (fileItem: FileItem, form: any) => {
      form.append('FileType', fileItem.file.type);
      form.append('FileName', 'ProfilePicture');
      form.append('FileToken', this.guid());
      this.filenName.push(fileItem.file.name);
    };

    this.uploader.onSuccessItem = (item, response, status) => {
       
      const resp = <IAjaxResponse>JSON.parse(response);
      if (resp.success) {
        this.updateInstallerInvoiceFile(resp.result.fileToken, resp.result.fileName, resp.result.fileType, resp.result.filePath);
      } else {
        this.message.error(resp.error.message);
      }
    };

    this.uploader.setOptions(this._uploaderOptions);
  }
  
  updateInstallerInvoiceFile(fileToken: string, fileName: string, fileType: string, filePath: string): void {
    debugger;
    this.installerInv.fileToken = fileToken;
    this.installerInv.fileName = this.filenName[0];
     
    // this.installerInv.jobdetails=this.jobIds;
    // this.installerInv.installers=this.installerIds;

    this._jobInstallerInvoiceServiceProxy.createOrEdit(this.installerInv).pipe(finalize(() => { this.saving = false; }))
      .subscribe(() => {
        let log = new UserActivityLogDto();
        log.actionId = 26;
        log.actionNote ='Installer Invoices Created';
        log.section = this.SectionName;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {
        });
        this.notify.info(this.l('SavedSuccessfully'));
        this.modal.hide();
        this.active = false;                        
        this.modalSave.emit(null);
        // this.installerInv.invTypeId = null;
        // this.installerInv.jobId = null;
        // this.installerInv.invNo = null;
        // this.installerInv.amount = null;
        // this.installerInv.invDate = null;
        // this.installerInv.advanceAmount = null;
        // this.installerInv.lessDeductAmount = null;
        // this.installerInv.totalAmount = null;
        // this.installerInv.advancePayDate = null;
        // this.installerInv.payDate = null;
        // this.installerInv.paymentsTypeId = null;
        // this.installerInv.installerId = null;
        // this.installerInv.notes = null;
        // this.uploader = null;
      }
    );
  }

  guid(): string {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
  }

  saveJobInstallerInvoice(): void {
     
    // if (this.uploader.queue.length == 0) {
    //   this.notify.warn(this.l('SelectFileToUpload'));
    //   return;
    // }
   
    this._jobInstallerInvoiceServiceProxy.checkInstallerDetail(this.installerInv).subscribe(result => {
      
      if(!result)
      {
        this.saving = true; 
        if (this.uploader.queue.length != 0) {

          this.uploader.uploadAll();
        }
        else{
          this._jobInstallerInvoiceServiceProxy.createOrEdit(this.installerInv).pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
              this.notify.info(this.l('SavedSuccessfully'));
              this.modal.hide();
              this.active = false;
              this.modalSave.emit(null);
            }
          );
        }
      }
      else{ 
        document.getElementById('invoiceNo').focus();
        this.notify.warn(this.l('InvoiceNoExists'));
      }
  });
   
  };

  close(): void {
    this.active = false;
    this.modal.hide();
  }

  // filterinstaller(event): void {
  //   this._jobInstallerInvoiceServiceProxy.getInstallerForDropdown(event.query).subscribe(result => {
  //     this.filteredinstaller = result;
  //   });
  // }

  filterjobnumber(event): void {
    this._jobInstallerInvoiceServiceProxy.getSearchFilterByOrg(event.query, this.installerInv.installation_Maintenance_Inspection, this.organizationId).subscribe(result => {
        this.searchResult = result;
    });
  }

  OnSelectDetails(event): void {
    debugger;
    this.installerInv.jobId = event.jobId;
    this.installerInv.jobNumber = event.jobNumber;
    this.installerInv.installerId = event.installerId;
    event.installerName = this.InstallerList.find(item => item.id == event.installerId).installerName;
    this.installerInv.installerName = this.InstallerList.find(item => item.id == event.installerId).installerName; 

    this._jobInstallerInvoiceServiceProxy.checkDuplicateJobnumber(this.installerInv.installation_Maintenance_Inspection,event.jobId,this.installerInv.id).subscribe(result => {
      if(result){
        this.message.confirm(this.installerInv.installation_Maintenance_Inspection + ' with This Project No '+ event.jobNumber +' already Exist ',
        "Looks Like Duplicate Record",
        (isConfirmed) => {
             if (!isConfirmed) {
              this.installerInv.jobNumber = '';
              this.installerInv.jobId = 0;
              this.installerInv.installerId = 0;
              event.installerName = '';
              this.installerInv.installerName ='';
            }
        }
        );
      }
      // else{
      //   this.installerInv.jobId = event.jobId;
      //   this.installerInv.jobNumber = event.jobNumber;
      //   this.installerInv.installerId = event.installerId;
      //   event.installerName = this.InstallerList.find(item => item.id == event.installerId).installerName;
      //   this.installerInv.installerName = this.InstallerList.find(item => item.id == event.installerId).installerName;
      // }
    });

    
    // console.log(event);
  }

  installerSearchResult: any [];
  filterInstaller(event): void {
    debugger;
    this.installerSearchResult = Object.assign([], this.InstallerList).filter(
      item => item.installerName.toLowerCase().indexOf(event.query.toLowerCase()) > -1  || item.companyName?.toLowerCase().indexOf(event.query.toLowerCase())>-1

    )
  }

  selectInstaller(event): void {
    this.installerInv.installerId = event.id;
    this.installerInv.installerName = event.installerName;
  }

  onInvoiceNoChange(): void {  
    
  }

  selectInspection() : void{
    if(this.installerInv.jobNumber!=null){
      this._jobInstallerInvoiceServiceProxy.checkDuplicateJobnumber(this.installerInv.installation_Maintenance_Inspection,this.installerInv.jobId,this.installerInv.id).subscribe(result => {
        if(result){
          this.message.confirm(this.installerInv.installation_Maintenance_Inspection + ' with This Project No '+ this.installerInv.jobNumber +' already Exist ',
          "Looks Like Duplicate Record",
          (isConfirmed) => {
               if (!isConfirmed) {
                this.installerInv.jobNumber = '';
                this.installerInv.jobId = 0;
                this.installerInv.installerId = 0;
                this.installerInv.installerName ='';
              }
          }
          );
        }
        // else{
        //   this.installerInv.jobId = event.jobId;
        //   this.installerInv.jobNumber = event.jobNumber;
        //   this.installerInv.installerId = event.installerId;
        //   event.installerName = this.InstallerList.find(item => item.id == event.installerId).installerName;
        //   this.installerInv.installerName = this.InstallerList.find(item => item.id == event.installerId).installerName;
        // }
      });
    }
  }

  addPriceList(): void {
    this.installerInv.installerInvoicePriceListDtos.push(new InstallerInvoicePriceListDto);
  }

  removePriceList(rowitem): void {
    //debugger;
    if (this.installerInv.installerInvoicePriceListDtos.length == 1)
        return;
    // this.JobVariations = this.JobVariations.filter(item => item.variationId != JobVariation.variationId);

    if (this.installerInv.installerInvoicePriceListDtos.indexOf(rowitem) === -1) {
        // this.JobVariations.push(rowitem);
    } else {
        this.installerInv.installerInvoicePriceListDtos.splice(this.installerInv.installerInvoicePriceListDtos.indexOf(rowitem), 1);
        this.calculateRate();
    }

  }
  calculateRate() : void{
    let totalamount = 0;
    this.installerInv.installerInvoicePriceListDtos.forEach(function (item) {
      if(item.amount > 0){
        totalamount = parseFloat(totalamount.toString()) + parseFloat(item.amount.toString()); 
      }
  })
  
  this.installerInv.amount = totalamount ; 
  if(this.installerInv.isGSTInclude){
    this.installerInv.amount = totalamount + (totalamount *10 /100)
  }
  }
}
