import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewinstallerComponent } from './newinstaller.component';

describe('NewinstallerComponent', () => {
  let component: NewinstallerComponent;
  let fixture: ComponentFixture<NewinstallerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewinstallerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewinstallerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
