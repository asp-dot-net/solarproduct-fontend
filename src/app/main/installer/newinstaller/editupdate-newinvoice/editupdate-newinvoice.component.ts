import { EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CreateOrEditJobInstallerInvoiceDto, JobInstallerInvoicesServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import * as moment from 'moment';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/internal/operators/finalize';

@Component({
  selector: 'editupdatenewinvoice',
  templateUrl: './editupdate-newinvoice.component.html',
  styleUrls: ['./editupdate-newinvoice.component.css']
})
export class EditupdateNewinvoiceComponent extends AppComponentBase {

  @ViewChild('addModal', { static: true }) modal: ModalDirective;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  saving = false;
  distAppliedDate: moment.Moment;
  MeterNumber = "";
  ApprovalRefNo = "";
  item: CreateOrEditJobInstallerInvoiceDto = new CreateOrEditJobInstallerInvoiceDto();
  comment = "";
  remarks: string;
  date: moment.Moment;

  constructor(
      injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _jobInstallerInvoiceServiceProxy: JobInstallerInvoicesServiceProxy,
      private _router: Router
    ) {
      super(injector);
    }

  SectionName = '';
  show(installerInvId?: number, sectionId? : number, section = ''): void {
    this.SectionName = section;
    let log = new UserActivityLogDto();
    log.actionId = 79;
    log.actionNote ='Open For Edit Payment';
    log.section = section;
    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {
    });
        this.showMainSpinner();
          this.item = new CreateOrEditJobInstallerInvoiceDto();
          this._jobInstallerInvoiceServiceProxy.getDataForEdit(installerInvId).subscribe(result => {
              this.item = result;
              this.item.sectionId = sectionId;
              this.hideMainSpinner();
              this.modal.show();
              
          }, e => { 
            this.hideMainSpinner();
          });
    }

    save(): void {
        this.saving = true;
        this._jobInstallerInvoiceServiceProxy.updateInvoicePayment(this.item)
        .pipe(finalize(() => { this.saving = false;}))
        .subscribe(() => {
          let log = new UserActivityLogDto();
          log.actionId = 26;
          log.actionNote ='Installer Invoice Paid';
          log.section = this.SectionName;
          this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
              .subscribe(() => {
          });
            this.modal.hide();
            this.notify.info(this.l('SavedSuccessfully'));
            this.modalSave.emit(null);
        });
    }

  close(): void {
      this.modal.hide();
  }


}
