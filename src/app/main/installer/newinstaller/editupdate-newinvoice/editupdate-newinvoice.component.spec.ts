import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditupdateNewinvoiceComponent } from './editupdate-newinvoice.component';

describe('EditupdateNewinvoiceComponent', () => {
  let component: EditupdateNewinvoiceComponent;
  let fixture: ComponentFixture<EditupdateNewinvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditupdateNewinvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditupdateNewinvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
