import { Component, Injector, ViewChild, ViewEncapsulation, HostListener } from '@angular/core';
import { CommonLookupDto,CommonLookupInstaller, CommonLookupServiceProxy, GetInstallerInvoiceForViewDto, InstallerNewDto, InstallerServiceProxy, JobInstallerInvoicesServiceProxy, LeadDto, LeadSourceLookupTableDto, LeadsServiceProxy, LeadStateLookupTableDto, LeadStatusLookupTableDto, OrganizationUnitDto, TokenAuthServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy, UserServiceProxy } from '@shared/service-proxies/service-proxies';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { AppConsts } from '@shared/AppConsts';
import { HttpClient } from '@angular/common/http';
import { finalize } from 'rxjs/operators';
import { FileUpload } from 'primeng/fileupload';
import { OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { EditupdateNewinvoiceComponent } from './editupdate-newinvoice/editupdate-newinvoice.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { EditinstallerinvoiceComponent } from '../invoiceinstaller/edit-newinstallerinvoice/editinstallerinvoice.component';
import { Title } from '@angular/platform-browser';
import { ViewApplicationModelComponent } from '@app/main/jobs/jobs/view-application-model/view-application-model.component';
import { iterate } from 'localforage';

@Component({
    selector: 'app-newinstaller',
    templateUrl: './newinstaller.component.html',
    styleUrls: ['./newinstaller.component.css'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class NewinstallerComponent extends AppComponentBase implements OnInit {

    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    organizationUnitlength: number=0;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };
    @ViewChild('ExcelFileUpload', { static: false }) excelFileUpload: FileUpload;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('viewApplicationModal', { static: true }) viewApplicationModal: ViewApplicationModelComponent;
    @ViewChild('editupdatenewinvoice', { static: true }) editupdatenewinvoice: EditupdateNewinvoiceComponent;
    @ViewChild('editinstallerinvoice', { static: true }) editinstallerinvoice: EditinstallerinvoiceComponent;
    uploadUrl: string;
    advancedFiltersAreShown = false;
    filterText = '';
    
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnit = 0;
    invoiceType='';

    //sampleDateRange: moment.Moment[] = [moment().add(-7, 'days').endOf('day'), moment().startOf('day')];
    
    dateFilterType = 'CreationDate';
    date = new Date();
    firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    // startDate = moment(this.firstDay);
    // endDate = moment().endOf('day');
    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);
    installerId: number;
    
    firstrowcount = 0;
    last = 0;
    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 255;
    toggle: boolean = true;    
    change() {
        this.toggle = !this.toggle;
    }
    filterName = 'JobNumber';
    orgCode = '';
    batteryFilter = 0;
    
    constructor(
        injector: Injector,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _httpClient: HttpClient,
        private _userServiceProxy: UserServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _jobInstallerInvoiceServiceProxy: JobInstallerInvoicesServiceProxy,
        private _router: Router,
        private titleService: Title,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _installerServiceProxy: InstallerServiceProxy
    ) {
        super(injector);
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/Users/ImportLeadFromExcel';
        this.titleService.setTitle(this.appSession.tenancyName + " |  New Installer");
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open New Installer Invoice';
            log.section = 'New Installer Invoice';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.orgCode = this.allOrganizationUnits[0].code;
            this.getnewinstaller();
            this.bindInstaller();
        });
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    
    InstallerList: CommonLookupInstaller[];
    bindInstaller(){
        this.InstallerList = [];
        this._installerServiceProxy.getAllInstallersWithCompnayName(this.organizationUnit).subscribe(result => {
            this.InstallerList = result;
        });
    }

    delete(installerInvoice: InstallerNewDto): void {
        debugger;
        this.message.confirm(

            this.l('InstallerInvoiceDeleteWarningMessage', installerInvoice.invNo),
            this.l('AreYouSure'),
            isConfirmed => {
                if (isConfirmed) {
                    this._jobInstallerInvoiceServiceProxy.delete(installerInvoice.invoiceinstallerId).subscribe(() => {
                        let log = new UserActivityLogDto();
                        log.actionId = 26;
                        log.actionNote ='Installer Invoices Deleted';
                        log.section = 'New Installer Invoice';
                        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                            .subscribe(() => {
                        });
                        this.getnewinstaller();
                        this.notify.success(this.l('SuccessfullyDeleted'));
                    });
                }
            }
        );
    }

    clear() {
        this.filterText = '';
        // let date = new Date();
        // this.startDate = moment(new Date(this.date.getFullYear(), this.date.getMonth(), 1));
        // this.endDate = moment().endOf('day');
        this.startDate =  moment(this.date);
        this.endDate =  moment(this.date);
        this.getnewinstaller();
    }

    changeOrgCode() {
        this.orgCode = this.allOrganizationUnits.find(x => x.id == this.organizationUnit).code;
    }

    getnewinstaller(event?: LazyLoadEvent) {
        this.primengTableHelper.showLoadingIndicator();
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this._jobInstallerInvoiceServiceProxy.getAll(
            this.filterName,
            this.organizationUnit,
            filterText_,
            this.invoiceType,
            this.installerId,
            this.dateFilterType,
            this.startDate,
            this.endDate,
            this.batteryFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            console.log(result)
            this.shouldShow = false;
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    getToolTipData(records: InstallerNewDto): string {
        return records.address + ', ' + records.suburb + ', ' + records.state + '-' + records.pCode;
    }

    downloadfile1(filename,filepath): void {
        debugger
          let FileName = AppConsts.docUrl + "/" + filepath + filename;
        window.open(FileName, "_blank");

    }

    installerSearchResult: any [];
    filterInstaller(event): void {
        this.installerSearchResult = Object.assign([], this.InstallerList).filter(
            item => item.installerName.toLowerCase().indexOf(event.query.toLowerCase()) > -1  || item.companyName?.toLowerCase().indexOf(event.query.toLowerCase())>-1
        )
        
    }

    selectInstaller(event): void {
        this.installerId = event.id;
        this.getnewinstaller();
    }
    
    exportToExcel(): void {
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._jobInstallerInvoiceServiceProxy.getAllToExcel(
            this.filterName,
            this.organizationUnit,
            filterText_,
            this.invoiceType,
            this.installerId,
            this.dateFilterType,
            this.startDate,
            this.endDate
        )
        .subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
         });
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'New Installer Invoice';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'New Installer Invoice';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }

    exportToExcelPer(): void {
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._jobInstallerInvoiceServiceProxy.getAllToExcelPerc(
            this.filterName,
            this.organizationUnit,
            filterText_,
            this.invoiceType,
            this.installerId,
            this.dateFilterType,
            this.startDate,
            this.endDate
        )
        .subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
         });
    }

}
