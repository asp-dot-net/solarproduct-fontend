﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { HoldReasonsServiceProxy, CreateOrEditHoldReasonDto, CreateOrEditServiceCategoryDto, ServiceCategorysServiceProxy, CreateOrEditServiceSubCategoryDto, ServiceSubCategorysServiceProxy, CommonLookupServiceProxy, CommonLookupDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';


@Component({
    selector: 'createOrEditserviceSubCategoryModal',
    templateUrl: './create-or-edit-serviceSubCategory-modal.component.html'
})
export class CreateOrEditServiceSubCategoryModalComponent extends AppComponentBase {
   
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    allserviceCategory:CommonLookupDto[];
    serviceSubCategory: CreateOrEditServiceSubCategoryDto = new CreateOrEditServiceSubCategoryDto();

    constructor(
        injector: Injector,
        private _serviceSubCategoryServiceProxy: ServiceSubCategorysServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }
    
    show(serviceSubCategoryId?: number): void {
        if (!serviceSubCategoryId) {
            this._commonLookupService.getServieCategoryDropdown().subscribe(result => {
                this.allserviceCategory = result;
            });
            this.serviceSubCategory = new CreateOrEditServiceSubCategoryDto();
            this.serviceSubCategory.id = serviceSubCategoryId;

            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Service SubCategory';
            log.section = 'Service SubCategory';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._commonLookupService.getServieCategoryDropdown().subscribe(result => {
                this.allserviceCategory = result;
            });
            this._serviceSubCategoryServiceProxy.getServiceSubCategoryForEdit(serviceSubCategoryId).subscribe(result => {
                this.serviceSubCategory = result.serviceSubCategorys;


                this.active = true;
                this.modal.show();
                
let log = new UserActivityLogDto();
log.actionId = 79;
log.actionNote ='Open For Edit Service SubCategory : ' + this.serviceSubCategory.name;
log.section = 'Service SubCategory';
this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
    .subscribe(() => {
});
            });
        }
        
    }
    onShown(): void {
        
        document.getElementById('ServiceType_ServiceType').focus();
    }
    save(): void {
            this.saving = true;
            this._serviceSubCategoryServiceProxy.createOrEdit(this.serviceSubCategory)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.serviceSubCategory.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Service SubCategory Updated : '+ this.serviceSubCategory.name;
                    log.section = 'Service SubCategory';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Service SubCategory Created : '+ this.serviceSubCategory.name;
                    log.section = 'Service SubCategory';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
