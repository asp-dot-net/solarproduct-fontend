import { Component, ViewChild, Injector, Output, EventEmitter, OnInit, ElementRef } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {
    ProductItemsServiceProxy, CreateOrEditProductItemDto, ProductItemProductTypeLookupTableDto, DemoUiComponentsServiceProxy, UploadDocumentInput, ServicesServiceProxy, GetMyServiceDocs, ServiceSubCategorysServiceProxy, GetSubCategoryDocs
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Observable } from "@node_modules/rxjs";
import { FileItem, FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { AppConsts } from '@shared/AppConsts';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';

@Component({
    selector: 'addsubcategoryAttachmentModal',
    templateUrl: './add-subcategoryattachment.component.html',
    animations: [appModuleAnimation()]
})
export class SubCategoryAttachmentComponent extends AppComponentBase {

    @ViewChild('addAttachment', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('myInput') myInputVariable: ElementRef;
    active = false;
    saving = false;
    servicedocs: GetMyServiceDocs[];
    productTypeName = '';
    allProductTypes: ProductItemProductTypeLookupTableDto[];
    public uploader: FileUploader;
    public maxfileBytesUserFriendlyValue = 5;
    private _uploaderOptions: FileUploaderOptions = {};
    filenName = [];
    uploadUrl: string;
    uploadedFiles: any[] = [];
    public fileupload: FileUploader;
    private _fileuploadoption: FileUploaderOptions = {};
    serviceid = 0;
    filetokens=[];
    fileNames = [];
    subcategorydocs:GetSubCategoryDocs[];
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _tokenService: TokenService,
        private _productItemsServiceProxy: ProductItemsServiceProxy,
        private _serviceSubCategoryServiceProxy: ServiceSubCategorysServiceProxy,
        private _router: Router
    ) {
        super(injector);
    }

    show(serviceid: number): void {
        this.serviceid= serviceid;
        this._serviceSubCategoryServiceProxy.getallSubCategorydoc(serviceid).subscribe(result => {
            this.subcategorydocs = result;
            this.active = true;
            this.active = true;
            this.initializeModal();
            this.modal.show();
        });
    }

    downloadfile(file): void {

        let FileName = AppConsts.docUrl + "/" + file.filePath + file.fileName;
        window.open(FileName, "_blank");
    }
    deletefile(serviceid){
        this._serviceSubCategoryServiceProxy.deleteSubCategoryDoc(serviceid).subscribe(result => {
            this._serviceSubCategoryServiceProxy.getallSubCategorydoc(this.serviceid).subscribe(result => {
                this.notify.info(this.l('SuccessfullyDeleted'));
                this.subcategorydocs = result;
            });
           
        }); 
    }
    initializeModal(): void {
        this.active = true;
        this.initFileUploader();
      }
    

    // upload completed event
    fileChangeEvent(event: any): void {
        if (event.target.files[0].size > 5242880) { //5MB
            this.message.warn(this.l('ProfilePicture_Warn_SizeLimit', this.maxfileBytesUserFriendlyValue));
            return;
        }
        this.uploader.clearQueue();
        this.uploader.addToQueue([<File>event.target.files[0]]);
        this.uploader.uploadAll();
    }
    guid(): string {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }

    guid1(): string {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }
    
    
    initFileUploader(): void {
        debugger;
        this.uploader = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Profile/UploadFile' });
        this._uploaderOptions.autoUpload = false;
        this._uploaderOptions.authToken = 'Bearer ' + this._tokenService.getToken();
        this._uploaderOptions.removeAfterUpload = true;
        this.uploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };
        this.fileupload = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Profile/UploadFile' });
        this._fileuploadoption.autoUpload = false;
        this._fileuploadoption.authToken = 'Bearer ' + this._tokenService.getToken();
        this._fileuploadoption.removeAfterUpload = true;
        this.fileupload.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

        this.uploader.onBuildItemForm = (fileItem: FileItem, form: any) => {
            form.append('FileType', fileItem.file.type);
            form.append('FileName', fileItem.file.name);
            form.append('FileToken', this.guid());
            this.filenName.push(fileItem.file.name);

        };
        this.fileupload.onBuildItemForm = (fileItem: FileItem, form: any) => {
            form.append('FileType', fileItem.file.type);
            form.append('FileName', fileItem.file.name);
            form.append('FileToken', this.guid1());
            this.filenName.push(fileItem.file.name);

        };
        this.uploader.onSuccessItem = (item, response, status) => {
            debugger;
            const resp = <IAjaxResponse>JSON.parse(response);
            if (resp.success) {
                this.savedocument(resp.result.fileToken, resp.result.fileName, this.serviceid);
                this.filetokens.push(resp.result.fileToken);
                this.fileNames.push(resp.result.fileName);
            } else {
                this.message.error(resp.error.message);
            }
        };
        this.fileupload.onSuccessItem = (item, response, status) => {
            debugger;
            const resp = <IAjaxResponse>JSON.parse(response);
        };
        this.uploader.setOptions(this._uploaderOptions);
        this.fileupload.setOptions(this._fileuploadoption);
    }
    cancel(): void {
        this.modal.hide();
    }
    savedocument(fileToken: string, fileName: string, id: number): void {
            debugger;
            this.notify.info("File Uploading Start");
            this.saving = true;
            this._serviceSubCategoryServiceProxy.saveSubCategoryDocument(fileToken,fileName,id)
                .pipe(finalize(() => { this.saving = false; }))
                .subscribe(() => {
                    this.myInputVariable.nativeElement.value = "";
                    this._serviceSubCategoryServiceProxy.getallSubCategorydoc(this.serviceid).subscribe(result => {
                        this.subcategorydocs = result;
                    });
                    this.notify.info(this.l('SavedSuccessfully'));
                    this.saving = false;
                });
        }
}
