﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetHoldReasonForViewDto, GetServiceCategoryForViewDto, GetServiceSubCategoryForViewDto, HoldReasonDto, ServiceCategoryDto, ServiceSubCategoryDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
@Component({
    selector: 'viewserviceSubCategoryModal',
    templateUrl: './view-serviceSubCategory-modal.component.html'
})
export class ViewServiceSubCategoryModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetServiceSubCategoryForViewDto;


    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
        this.item = new GetServiceSubCategoryForViewDto();
        this.item.serviceSubCategory = new ServiceSubCategoryDto();
    }

    show(item: GetServiceSubCategoryForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
        
let log = new UserActivityLogDto();
log.actionId = 79;
log.actionNote ='Opened Service SubCategory View : ' + this.item.serviceSubCategory.name;
log.section = 'Service SubCategory';
this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
.subscribe(() => {            
 });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
