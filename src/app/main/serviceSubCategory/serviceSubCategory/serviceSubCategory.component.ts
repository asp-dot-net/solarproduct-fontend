﻿import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute , Router} from '@angular/router';
import { HoldReasonsServiceProxy, HoldReasonDto, ServiceCategorysServiceProxy, ServiceCategoryDto, ServiceSubCategoryDto, ServiceSubCategorysServiceProxy, CommonLookupServiceProxy, CommonLookupDto  } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { Title } from '@angular/platform-browser';
import { ViewServiceSubCategoryModalComponent } from './view-serviceSubCategory-modal.component';
import { CreateOrEditServiceSubCategoryModalComponent } from './create-or-edit-serviceSubCategory-modal.component';
import { SubCategoryAttachmentComponent } from './add-subcategoryattachment.component';


@Component({
    templateUrl: './serviceSubCategory.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ServiceSubCategoryComponent extends AppComponentBase {
    
    @ViewChild('createOrEditserviceSubCategoryModal', { static: true }) createOrEditserviceSubCategoryModal: CreateOrEditServiceSubCategoryModalComponent;
    @ViewChild('viewserviceSubCategoryModal', { static: true }) viewserviceSubCategoryModal: ViewServiceSubCategoryModalComponent;   
    @ViewChild('addsubcategoryAttachmentModal', { static: true }) addsubcategoryAttachmentModal: SubCategoryAttachmentComponent;   
    
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    holdReasonFilter = '';
    serviceCategoryFilter = 0;
    firstrowcount = 0;
    last = 0;
    allserviceCategory:CommonLookupDto[];
    public screenHeight: any;  
    testHeight = 250;
    constructor(
        injector: Injector,
        private _serviceSubCategoryServiceProxy: ServiceSubCategorysServiceProxy,
        private titleService: Title,
        private _commonLookupService: CommonLookupServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Service Sub Category");
        this._commonLookupService.getServieCategoryDropdown().subscribe(result => {
            this.allserviceCategory = result;
        });
    }
    searchLog() : void {
        
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Service SubCategory';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
       
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Service SubCategory';
        log.section = 'Service SubCategory';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
    }
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }
    getserviceSubCategory(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._serviceSubCategoryServiceProxy.getAll(
            this.filterText,
            this.serviceCategoryFilter,
            this.holdReasonFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createServiceSubCategory(): void {
        this.createOrEditserviceSubCategoryModal.show();        
    }


    deleteserviceSubcategory(serviceSubCategory: ServiceSubCategoryDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._serviceSubCategoryServiceProxy.delete(serviceSubCategory.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete Service SubCategory: ' + serviceSubCategory.name;
                            log.section = 'Service SubCategory';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });
                        });
                }
            }
        );
    }

    exportToExcel(): void {
        // this._serviceCategoryServiceProxy.getHoldReasonsToExcel(
        // this.filterText,
        //     this.holdReasonFilter,
        // )
        // .subscribe(result => {
        //     this._fileDownloadService.downloadTempFile(result);
        //  });
    }
    
    
    
    
}
