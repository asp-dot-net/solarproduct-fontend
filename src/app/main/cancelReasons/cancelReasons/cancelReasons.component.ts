﻿import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { CancelReasonsServiceProxy, CancelReasonDto ,UserActivityLogDto,UserActivityLogServiceProxy} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CreateOrEditCancelReasonModalComponent } from './create-or-edit-cancelReason-modal.component';
import { ViewCancelReasonModalComponent } from './view-cancelReason-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import { finalize } from 'rxjs/operators';
import { Title } from '@angular/platform-browser';

@Component({
    templateUrl: './cancelReasons.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class CancelReasonsComponent extends AppComponentBase {

    @ViewChild('createOrEditCancelReasonModal', { static: true }) createOrEditCancelReasonModal: CreateOrEditCancelReasonModalComponent;
    @ViewChild('viewCancelReasonModalComponent', { static: true }) viewCancelReasonModal: ViewCancelReasonModalComponent;

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    cancelReasonNameFilter = '';
    firstrowcount = 0;
    last = 0;
    public screenHeight: any;  
    testHeight = 250;

    constructor(
        injector: Injector,
        private _cancelReasonsServiceProxy: CancelReasonsServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private titleService: Title
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Cancel Reasons");
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Cancel Reason';
        log.section = 'Cancel Reason';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });       
    }
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight; 
        
    } 
    getCancelReasons(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._cancelReasonsServiceProxy.getAll(
            this.filterText,
            this.cancelReasonNameFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }
    searchLog() : void {
        debugger;
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Cancel Reason';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createCancelReason(): void {
        this.createOrEditCancelReasonModal.show();
    }


    deleteCancelReason(cancelReason: CancelReasonDto): void {
        this.message.confirm(
            this.l('AreYouSureWanttoDelete'),
            this.l('Delete'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._cancelReasonsServiceProxy.delete(cancelReason.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete Cancel Reason: ' + cancelReason.cancelReasonName;
                            log.section = 'Cancel Reason';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            }); 
                        });
                }
            }
        );
    }

    exportToExcel(): void {
        this._cancelReasonsServiceProxy.getCancelReasonsToExcel(
            this.filterText,
            this.cancelReasonNameFilter,
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }
}
