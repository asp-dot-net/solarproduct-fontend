﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { CancelReasonsServiceProxy, CreateOrEditCancelReasonDto ,UserActivityLogServiceProxy,UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';

@Component({
    selector: 'createOrEditCancelReasonModal',
    templateUrl: './create-or-edit-cancelReason-modal.component.html'
})
export class CreateOrEditCancelReasonModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    cancelReason: CreateOrEditCancelReasonDto = new CreateOrEditCancelReasonDto();

    constructor(
        injector: Injector,
        private _cancelReasonsServiceProxy: CancelReasonsServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }

    show(cancelReasonId?: number): void {

        if (!cancelReasonId) {
            this.cancelReason = new CreateOrEditCancelReasonDto();
            this.cancelReason.id = cancelReasonId;
            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Cancel Reason';
            log.section = 'Cancel Reason';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
        } else {
            this._cancelReasonsServiceProxy.getCancelReasonForEdit(cancelReasonId).subscribe(result => {
                this.cancelReason = result.cancelReason;
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Cancel Reason : ' + this.cancelReason.cancelReasonName;
                log.section = 'Cancel Reason';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            });
        }
        
    }
    onShown(): void {
        
        document.getElementById('CancelReason_CancelReasonName').focus();
    }
    save(): void {
            this.saving = true;

			
            this._cancelReasonsServiceProxy.createOrEdit(this.cancelReason)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.cancelReason.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Cancel Reason Updated : '+ this.cancelReason.cancelReasonName;
                    log.section = 'Cancel Reason';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Cancel Reason Created : '+ this.cancelReason.cancelReasonName;
                    log.section = 'Cancel Reason';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }







    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
