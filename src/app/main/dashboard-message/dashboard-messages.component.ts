﻿import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import {  DashboardMessageServiceProxy, OrganizationUnitDto, CreateOrEditDashboardMessageDto,UserActivityLogServiceProxy,UserActivityLogDto, CommonLookupServiceProxy  } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { createOrEditDashboardMessageModalComponent } from './create-edit-dashboard-messages.component';
import { viewdashboardMessageModalComponent } from './view-dashboard-messages-modal.component';
import { Title } from '@angular/platform-browser';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './dashboard-messages.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class DashboardMessageComponent extends AppComponentBase {
    
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('createOrEditDashboardMessageModalComponent', { static: true }) createOrEditDashboardMessageModal: createOrEditDashboardMessageModalComponent;
    @ViewChild('viewdashboardMessageModalComponent', { static: true }) viewdashboardMessageModal: viewdashboardMessageModalComponent;
    
    advancedFiltersAreShown = false;
    filterText = '';
    nameFilter = '';
    firstrowcount = 0;
    last = 0;
    shouldShow: boolean = false;
    date = new Date();
    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);
    public screenHeight: any;  
    testHeight = 250;
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnitlength = 0;
    organizationUnit = 0;
    dasboardType ='';

    constructor(
        injector: Injector,
        private _dashboardMessageServiceProxy : DashboardMessageServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _commonLookupService : CommonLookupServiceProxy,
        private titleService: Title,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
            ) {
                super(injector);
                this.titleService.setTitle(this.appSession.tenancyName + " |  Dashboard Message");
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.getCommissionRanges();
        });
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Dashboard Message';
        log.section = 'Dashboard Message';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    searchLog() : void {
        debugger;
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Dashboard Message';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    getCommissionRanges(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();
        debugger;
        this._dashboardMessageServiceProxy.getAll(
            this.filterText,
            this.dasboardType,
            this.organizationUnit,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    // createLeadSource(): void {
    //     this._router.navigate(['/app/main/leadSources/leadSources/createOrEdit']);        
    // }


    delete(createOrEdit: CreateOrEditDashboardMessageDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._dashboardMessageServiceProxy.delete(createOrEdit.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete Dashboard Message: ' + createOrEdit.message;
                            log.section = 'Dashboard Message';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            }); 
                        });
                }
            }
        );
    }
  
}
