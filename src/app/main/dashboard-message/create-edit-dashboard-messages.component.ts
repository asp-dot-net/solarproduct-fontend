﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { OrganizationUnitDto, CommonLookupServiceProxy, UserActivityLogServiceProxy, UserActivityLogDto, DashboardMessageServiceProxy, CreateOrEditDashboardMessageDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';

@Component({
    selector: 'createOrEditDashboardMessageModal',
    templateUrl: './create-edit-dashboard-messages.component.html'
})
export class createOrEditDashboardMessageModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    
    isActive = false;
    active = false;
    saving = false;

   DashboardMessage: CreateOrEditDashboardMessageDto = new CreateOrEditDashboardMessageDto();

    organizationUnit = [];
    allOrganizationUnits: OrganizationUnitDto[];

    constructor(
        injector: Injector,
        private _dashboardMessageServiceProxy : DashboardMessageServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
    }

    show(DashboardMessageId?: number): void { 
        debugger;
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;

            if (!DashboardMessageId) {
                this.DashboardMessage = new CreateOrEditDashboardMessageDto();
                this.DashboardMessage.id = DashboardMessageId;
                this.active = true;
                this.isActive = true;
                setTimeout  (() => {
                    this.modal.show();
                },2000);
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Create New Check Dashboard Message';
                log.section = 'Dashboard Message';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });

            } else {
                this._dashboardMessageServiceProxy.getDashboardMessageForView(DashboardMessageId).subscribe(result => {
                    this.DashboardMessage = result.dashboardMessageDto;
                    this.DashboardMessage.orgId = result.dashboardMessageDto.orgId;
                    this.active = true;
                    this.isActive = true;
                    this.modal.show();
                    let log = new UserActivityLogDto();
                    log.actionId = 79;
                    log.actionNote ='Open For Edit Dashboard Message : ' + this.DashboardMessage.message;
                    log.section = 'Dashboard Message';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    });
                });
            }   
            
        });

        
        
    }

    save(): void {
        this.saving = true;

        this._dashboardMessageServiceProxy.createOrEdit(this.DashboardMessage)
            .pipe(finalize(() => { this.saving = false;}))
            .subscribe(() => {
            this.notify.info(this.l('SavedSuccessfully'));
            this.close();
            this.modalSave.emit(null);
            if(this.DashboardMessage.id){
                let log = new UserActivityLogDto();
                log.actionId = 82;
                log.actionNote ='Dashboard Message Updated : '+ this.DashboardMessage.message;
                log.section = 'Dashboard Message';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }else{
                let log = new UserActivityLogDto();
                log.actionId = 81;
                log.actionNote ='Dashboard Message Created : '+ this.DashboardMessage.message;
                log.section = 'Dashboard Message';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }
        });
    }

    onShown(): void {
        
        document.getElementById('DashboardMessage').focus();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
