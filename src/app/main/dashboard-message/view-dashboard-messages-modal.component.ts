﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { UserActivityLogServiceProxy,UserActivityLogDto, CreateOrEditCommissionRangesDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
@Component({
    selector: 'viewdashboardMessageModal',
    templateUrl: './view-dashboard-messages-modal.component.html'
})
export class viewdashboardMessageModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    saving = false;

    item: CreateOrEditCommissionRangesDto;
    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
        this.item = new CreateOrEditCommissionRangesDto();
    }

    show(item: CreateOrEditCommissionRangesDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Opened Dashboard Message View : ' + item.range;
        log.section = 'Dashboard Message';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {            
         }); 
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
