import { Component, Injector, ViewEncapsulation, ViewChild, OnInit, Optional, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LeadsServiceProxy, LeadDto, LeadStatusLookupTableDto, LeadUsersLookupTableDto, GetLeadForAssignOrTransferOutput, GetLeadForChangeDuplicateStatusOutput, LeadSourceLookupTableDto, UserServiceProxy, OrganizationUnitDto, JobsServiceProxy, CommonLookupServiceProxy, WholeSaleLeadServiceProxy } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { AppConsts } from '@shared/AppConsts';
import { HttpClient } from '@angular/common/http';
import { finalize } from 'rxjs/operators';
import { ViewResultWholeSaleLeadComponent } from './view-wholesaleheaderresult.component';
import { ViewDetailsWholeSaleModelComponent } from './view-detail-model/view-wholesale-model.component';

@Component({
    selector: 'searchWholesaleResultList',
    templateUrl: './wholesaleheaderresult.component.html',
    styleUrls: ['./wholesaleheaderresult.component.less'],
    animations: [appModuleAnimation()]
})
export class WholeSaleLeadHeaderResultsComponent extends AppComponentBase implements OnInit {
    show: boolean = true;
    FiltersData = false;
    showchild: boolean = true;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    // @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
    @ViewChild('viewResultWholeSaleLead', { static: true }) viewResultWholeSaleLead: ViewResultWholeSaleLeadComponent;
    @ViewChild('viewDetailsWholeSaleModal', { static: true }) viewDetailsWholeSaleModal: ViewDetailsWholeSaleModelComponent;

    saving = false;
    filterText = '';
    copanyNameFilter = '';
    emailFilter = '';
    phoneFilter = '';
    mobileFilter = '';
    addressFilter = '';
    requirementsFilter = '';
    postCodeSuburbFilter = '';
    stateNameFilter = '';
    streetNameFilter = '';
    postCodePostalCode2Filter = '';
    leadSourceNameFilter = '';
    //leadSubSourceNameFilter = '';
    leadStatusName = '';
    typeNameFilter = '';
    areaNameFilter = '';
    leadStatus: any;
    allLeadStatus: LeadStatusLookupTableDto[];
    leadStatusId: number = 0;
    StartDate: moment.Moment;
    EndDate: moment.Moment;
    allUsers: LeadUsersLookupTableDto[];
    allLeadSources: LeadSourceLookupTableDto[];
    assignLead: number;
    leadAction: number = 0;
    title: String;
    names: any;
    selectedAll: any;
    tableRecords: any;
    Ids: any;
    organizationUnit = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    ExpandedView: boolean = true;
    ExpandedView1: boolean = true;
    SelectedLeadId: number = 0;
    filter ='';

    leadId = 0;
    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 330;
    roles = [];
    constructor(
        injector: Injector,
        private _leadsServiceProxy: WholeSaleLeadServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _activatedRoute: ActivatedRoute,
        ///@Optional() private viewResultLead? : ViewResultLeadComponent
    ) {
        super(injector);
        this._commonLookupService.getAllLeadSourceForTableDropdown().subscribe(result => {
            this.allLeadSources = result;
        });
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
        this.leadId = this._activatedRoute.snapshot.queryParams['Id'];
        // this.filterText = this._activatedRoute.snapshot.queryParams['filterText'];
        // this._commonLookupService.getOrganizationUnit().subscribe(output => {
        //     this.allOrganizationUnits = output;
        //     this.getLeads();
        // });
        this.getLeads();

    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }

        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }

    showDetail(Text: string): void {
        this.filterText = Text;
        this.ngOnInit();
    }

    expandGrid() {

        this.ExpandedView = true;
        this.ExpandedView1 = true;

    }

    goBack() {
        window.history.back();
    }

    getLeads(event?: LazyLoadEvent) {
        
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this._commonLookupService.getCurrentUserRoles().subscribe(rol => {
            this.roles = rol;
        });

        this.primengTableHelper.showLoadingIndicator();

        this._leadsServiceProxy.getAllWholeSaleLeadSearch(
            this.leadId,
            // this.filterText,
            // this.primengTableHelper.getSorting(this.dataTable),
            // this.primengTableHelper.getSkipCount(this.paginator, event),
            // this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.tableRecords = result.items;
            this.primengTableHelper.hideLoadingIndicator();

            if (this.ExpandedView == false || this.ExpandedView1 == false && result.totalCount != 0) {
                this.navigateToResultView(result.items[0].createOrEditWholeSaleLeadDto.id)
            }
            else {
                this.ExpandedView = true;
                this.ExpandedView1 = true;
            }
        });
    }

    quickview(jobid): void {
        // this.viewDetailsWholeSaleModal.show(jobid);
    }

    reloadPage(): void {
        this.ExpandedView = true;
        this.ExpandedView1 = true;
        this.paginator.changePage(this.paginator.getPage());
    }

    submit(): void {
        if (this.leadAction == 3) {
            let selectedids = [];
            this.primengTableHelper.records.forEach(function (lead) {
                if (lead.lead.isSelected) {
                    selectedids.push(lead.lead.id);
                }
            });

            let assignleads: GetLeadForAssignOrTransferOutput = new GetLeadForAssignOrTransferOutput();
            assignleads.assignToUserID = this.assignLead;
            assignleads.leadIds = selectedids;
            // if (selectedids.length != 0) {
            //     this._leadsServiceProxy.assignOrTransferLead(assignleads)
            //         .pipe(finalize(() => { this.saving = false; }))
            //         .subscribe(() => {
            //             this.reloadPage();
            //             this.notify.info(this.l('AssignedSuccessfully'));
            //         });
            // }
            // else {
            //     this.notify.warn(this.l('NoLeadsSelected'));
            // }
        }

        // if (this.leadAction == 1) {
        //     let selectedids = [];
        //     this.primengTableHelper.records.forEach(function (lead) {
        //         if (lead.lead.isSelected) {
        //             selectedids.push(lead.lead.id);
        //         }
        //     });
        //     if (selectedids.length != 0) {
        //         this._leadsServiceProxy.delete(selectedids)
        //             .subscribe(() => {
        //                 this.reloadPage();
        //                 this.notify.info(this.l('DeleteSuccessfully'));
        //             });
        //     }
        //     else {
        //         this.notify.warn(this.l('NoLeadsSelected'));
        //     }
        // }

    }

    navigateToResultView(leadid): void {
        // this._jobServiceProxy.checkjobcreatepermission(leadid)
        //     .subscribe(result1 => {
        //         if (result1 == 1) {
        //             this.ExpandedView = !this.ExpandedView;
        //             this.ExpandedView = false;
        //             this.ExpandedView1 = true;
        //             this.SelectedLeadId = leadid;
        //             this.viewLeadDetail.showDetail(leadid, "result", 30);
        //         }
        //         else {
        //             this.ExpandedView1 = !this.ExpandedView1;
        //             this.ExpandedView1 = false;
        //             this.ExpandedView = true;
        //             this.SelectedLeadId = leadid;
        //             this.viewResultLead.showDetail(leadid, 30);
        //         }

        // });


        this.ExpandedView1 = !this.ExpandedView1;
        this.ExpandedView1 = false;
        this.ExpandedView = true;
        this.SelectedLeadId = leadid;
        this.viewResultWholeSaleLead.showDetail(leadid);

    }

    requestToTransferLead(leadid): void{
        this._leadsServiceProxy.requestToTransfer(leadid)
        .pipe(finalize(() => { this.saving = false; }))
        .subscribe(() => {
            // this.viewLeadDetail.reloadLead.emit(false);
            
            this.saving = false; 
            this.notify.info("Successfully Requested");
        });
    }

    requestToMoveLead(leadid): void{
        this._leadsServiceProxy.requestToMove(leadid,41)
        .pipe(finalize(() => { this.saving = false; }))
        .subscribe(() => {
            // this.viewLeadDetail.reloadLead.emit(false);
            
            this.saving = false; 
            this.notify.info("Successfully Requested");
        });
    }



}