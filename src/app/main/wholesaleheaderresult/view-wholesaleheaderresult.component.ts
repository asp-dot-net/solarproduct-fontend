import { Component, ViewChild, Injector, Input, Output, EventEmitter, OnInit, Optional } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { LeadsServiceProxy, GetLeadForViewDto, LeadDto, GetActivityLogViewDto, GetLeadForChangeStatusOutput, WholeSaleLeadServiceProxy, GetWholeSaleLeadForViewDto, CreateOrEditWholeSaleLeadDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import * as moment from 'moment';
import { ActivityLogMyLeadComponent } from '@app/main/myleads/myleads/activity-log-mylead.component';
import { PromotionStopResponceComponent } from '@app/main/promotions/promotionUsers/stop-responce.component';
import { WholeSaleActivityLogComponent } from '../wholesale/leads/wholesale-activity-log.component';
@Component({
    selector: 'viewResultWholeSaleLead',
    templateUrl: './view-wholesaleheaderresult.component.html',
    animations: [appModuleAnimation()]
})
export class ViewResultWholeSaleLeadComponent extends AppComponentBase implements OnInit {

    @Input() SelectedLeadId: number = 0;
    @Output() reloadLead = new EventEmitter();
    active = false;
    saving = false;
    @ViewChild('promoStopRespmodel', { static: true }) promot: PromotionStopResponceComponent;
    // @ViewChild('activityLog', { static: true }) activityLogLead: ActivityLogMyLeadComponent;
    @ViewChild('wholeSaleactivityLog', { static: true }) wholeSaleactivityLog: WholeSaleActivityLogComponent;

    item: GetWholeSaleLeadForViewDto;
    activityLog: GetActivityLogViewDto[];
    showforresult = false;
    activeTabIndex: number = 0;
    activityDatabyDate: any[] = [];
    sectionId = 0;
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _leadsServiceProxy: WholeSaleLeadServiceProxy,
        private _router: Router,
    ) {
        super(injector);
        this.item = new GetWholeSaleLeadForViewDto();
        this.item.createOrEditWholeSaleLeadDto = new CreateOrEditWholeSaleLeadDto();
    }

    ngOnInit(): void {
        //this.showDetail(this._activatedRoute.snapshot.queryParams['LeadId']);
    }

    goBack() {
        window.history.back();
    }

    showDetail(leadId: number): void {
debugger;
       this.showMainSpinner();
        //this.sectionId = sectionId;
        if (leadId != null) {
            //let that = this;
            this._leadsServiceProxy.getWholeSaleLeadForView(leadId).subscribe(result => {
                this.item = result;
                this.hideMainSpinner();
            });
            this.wholeSaleactivityLog.show(leadId, 0,0);
        }
    }
}
