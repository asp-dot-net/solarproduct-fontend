import { Component, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CreateOrEditJobDto, CreateOrEditWholeSaleLeadDto, GetWholeSaleLeadForViewDto, JobApplicationViewDto, JobsServiceProxy, WholeSaleLeadServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'viewDetailsWholeSaleModal',
  templateUrl: './view-wholesale-model.component.html',
})
export class ViewDetailsWholeSaleModelComponent extends AppComponentBase {
  @ViewChild('viewModel', { static: true }) modal: ModalDirective;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  active = false;
  jobid: number;
  finance = false;
  invoiceinstaller=0;

  mapSrc: any = '';

  billToPay: number = 0;
  item: GetWholeSaleLeadForViewDto;
  

  constructor(
    injector: Injector,
    private _jobsServiceProxy: JobsServiceProxy,
        private _leadsServiceProxy: WholeSaleLeadServiceProxy,
        private spinner: NgxSpinnerService
  ) {
    super(injector);
    this.item = new GetWholeSaleLeadForViewDto();
        this.item.createOrEditWholeSaleLeadDto = new CreateOrEditWholeSaleLeadDto();
  }

  show(id: number): void {
    debugger;
    this.spinner.show();
    this._leadsServiceProxy.getWholeSaleLeadForView(id).subscribe(result => {
        this.item = result;
       this.modal.show();
       this.spinner.hide();
     });
  }
  

  close(): void {
    this.active = false;
    this.modal.hide();
  }
}
