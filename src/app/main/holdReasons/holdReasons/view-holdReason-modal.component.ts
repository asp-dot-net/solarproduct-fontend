﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetHoldReasonForViewDto, HoldReasonDto ,UserActivityLogServiceProxy,UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
@Component({
    selector: 'viewHoldReasonModal',
    templateUrl: './view-holdReason-modal.component.html'
})
export class ViewHoldReasonModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    saving = false;
    item: GetHoldReasonForViewDto;
    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
        this.item = new GetHoldReasonForViewDto();
        this.item.jobHoldReason = new HoldReasonDto();
    }

    show(item: GetHoldReasonForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Opened Hold Reasons View : ' + item.jobHoldReason.jobHoldReason;
        log.section = 'Hold Reasons';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {            
         }); 
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
