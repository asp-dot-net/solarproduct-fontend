﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { HoldReasonsServiceProxy, CreateOrEditHoldReasonDto,UserActivityLogServiceProxy,UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
@Component({
    selector: 'createOrEditHoldReasonModal',
    templateUrl: './create-or-edit-holdReason-modal.component.html'
})
export class CreateOrEditHoldReasonModalComponent extends AppComponentBase {
   
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    saving = false;
    holdReason: CreateOrEditHoldReasonDto = new CreateOrEditHoldReasonDto();
    constructor(
        injector: Injector,
        private _holdReasonsServiceProxy: HoldReasonsServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
    }
    
    show(holdReasonId?: number): void {
        if (!holdReasonId) {
            this.holdReason = new CreateOrEditHoldReasonDto();
            this.holdReason.id = holdReasonId;
            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Hold Reasons';
            log.section = 'Hold Reasons';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 


        } else {
            this._holdReasonsServiceProxy.getHoldReasonForEdit(holdReasonId).subscribe(result => {
                this.holdReason = result.jobHoldReason;
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Hold Reasons : ' + this.holdReason.jobHoldReason;
                log.section = 'Hold Reasons';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        } 
    }
    onShown(): void {
        
        document.getElementById('HoldReason_HoldReason').focus();
    }
    save(): void {
            this.saving = true;
            this._holdReasonsServiceProxy.createOrEdit(this.holdReason)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.holdReason.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Hold Reasons Updated : '+ this.holdReason.jobHoldReason;
                    log.section = 'Hold Reasons';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Hold Reasons Created : '+ this.holdReason.jobHoldReason;
                    log.section = 'Hold Reasons';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
