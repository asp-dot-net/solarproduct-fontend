import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { LeadsServiceProxy, GetDuplicateLeadPopupDto, LeadDto, ServicesServiceProxy, GetDuplicateServicePopupDto, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'viewServiceDuplicateModal',
    templateUrl: './view-duplicate-service.component.html'
})
export class ViewServiceDuplicateModalComponent extends AppComponentBase {

    @ViewChild('duplicateModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    saving = false;
    item: GetDuplicateServicePopupDto[];
    DupCount:number = 0;

    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _servicesServiceProxy: ServicesServiceProxy,
    ) {
        super(injector);
    }

  SectionName = '';
    show(Id: number , organizationId: number, section = ''): void {
        this.SectionName = section;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='View Duplicate Record';
        log.section = section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
        this._servicesServiceProxy.getDuplicateServiceForView(Id,organizationId).subscribe(result => {    
            this.item = result;
            this.DupCount = this.item.length;
            this.modal.show();
        }); 
        
    }

    close(): void {
        this.modal.hide();
    }
}
