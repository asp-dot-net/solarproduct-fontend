﻿import { Component, Injector, ViewEncapsulation, ViewChild, Input, OnInit, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLookupDto, CommonLookupServiceProxy, LeadStateLookupTableDto, LeadUsersLookupTableDto, OrganizationUnitDto, ServicesServiceProxy, TokenAuthServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import * as _ from 'lodash';
import * as moment from 'moment';
import { Title } from '@angular/platform-browser';
import { ViewMyLeadComponent } from '@app/main/myleads/myleads/view-mylead.component';
import { finalize } from 'rxjs/operators';
import { ViewApplicationModelComponent } from '@app/main/jobs/jobs/view-application-model/view-application-model.component';
import { AddActivityModalComponent } from '@app/main/myleads/myleads/add-activity-model.component';
import { ViewServiceDuplicateModalComponent } from './view-duplicate-service.component';

@Component({
    templateUrl: './manageservice.component.html',
    styleUrls: ['./manageservice.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ManageServicesComponent extends AppComponentBase implements OnInit {
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
    @ViewChild('viewApplicationModal', { static: true }) viewApplicationModal: ViewApplicationModelComponent;
    @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    @ViewChild('viewServiceDuplicateModal', { static: true }) viewServiceDuplicateModal: ViewServiceDuplicateModalComponent;
    
    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;

    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };
    allOrganizationUnits: OrganizationUnitDto[];
    allStates: LeadStateLookupTableDto[];
    allServieCategorys:CommonLookupDto[];
    allServiceUsers:LeadUsersLookupTableDto[];
    organizationUnit = 0;
    organizationUnitlength: number = 0;
    sampleDateRange: moment.Moment[] = [moment().add(-7, 'days').endOf('day'), moment().add(0, 'days').startOf('day')];
    StartDate: moment.Moment;
    EndDate: moment.Moment;
    filterText = '';
    serviceCategory='';
    stateNameFilter='';
    adressFilter='';
    firstrowcount = 0;
    FiltersData = false;
    last = 0;
    ExpandedView: boolean = true;
    SelectedLeadId: number = 0;
    assignservice = 0;
    tableRecords: any;
    totalcount = 0;
    count = 0;
    leadAction = 0;

    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 250;
    toggle: boolean = true;
    filterName = "JobNumber";
    orgCode = '';

    change() {
        this.toggle = !this.toggle;
    }

    constructor(
        injector: Injector,
        private _servicesServiceProxy: ServicesServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _router: Router,
        private titleService: Title
        ) {
            super(injector);
            this.titleService.setTitle(this.appSession.tenancyName + " |  Manage Service");
    }
    
    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        debugger;
        this._commonLookupService.getServieCategoryDropdown().subscribe(result => {
            this.allServieCategorys = result;
        });
       
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Manage Service';
            log.section = 'Manage Service';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.orgCode = this.allOrganizationUnits[0].code;
            this.getServices();
            this.getusers();
        });
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + 82 ;
        }
        else {
            this.testHeight = this.testHeight - 82 ;
        }
    }
    
    getusers(){
        this._commonLookupService.getServiceUserForFilter(this.organizationUnit).subscribe(result => {
            this.allServiceUsers = result;
        });
    }

    changeOrgCode() {
        this.orgCode = this.allOrganizationUnits.find(x => x.id == this.organizationUnit).code;
    }

    getServices(event?: LazyLoadEvent) {
        debugger;
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            
            return;
        }
        if (this.sampleDateRange != null) {
            this.StartDate = this.sampleDateRange[0];
            this.EndDate = this.sampleDateRange[1];
        } else {
            this.StartDate = null;
            this.EndDate = null;
        }

        this.primengTableHelper.showLoadingIndicator();

        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }

        this._servicesServiceProxy.getAll(
            this.filterName,
            filterText_,
            this.StartDate,
            this.EndDate,
            this.organizationUnit,
            this.serviceCategory,
            this.stateNameFilter,
            this.adressFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.tableRecords = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            this.shouldShow = false;
        });
    }

    clear() {
        this.stateNameFilter = '';
        this.organizationUnit = 0;
        this.filterText = '';
        this.serviceCategory = '';
        this.adressFilter = '';
        this.stateNameFilter = '';
        this.sampleDateRange = [moment().add(-7, 'days').endOf('day'), moment().add(0, 'days').startOf('day')];
        this.getServices();
    }

    expandGrid() {
        this.ExpandedView = true;
    }

    navigateToLeadDetail(leadid): void {
        this.ExpandedView = !this.ExpandedView;
        this.ExpandedView = false;
        this.SelectedLeadId = leadid;
        this.viewLeadDetail.showDetail(leadid,null,1,0,'Manage Service');
    }

    reloadPage($event): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    isAllChecked() {
        if (this.tableRecords)
            return this.tableRecords.every(_ => _.isSelected);
    }
    checkAll(ev) {

        this.tableRecords.forEach(x => x.isSelected = ev.target.checked);
        this.oncheckboxCheck();
        /// this.totalcount = this.tableRecords.forEach(x => x.lead.isSelected = ev.target.checked);
    }
    
    oncheckboxCheck() {

        this.count = 0;
        this.tableRecords.forEach(item => {
            if (item.isSelected == true) {
                this.count = this.count + 1;
            }
        })
    }
    submit(): void {
        debugger;
        let selectedids = [];
        // this.saving = true;
        this.primengTableHelper.records.forEach(function (job) {
            if (job.isSelected) {
                selectedids.push(job.service.id);
            }
        });
        if (selectedids.length == 0) {
            this.notify.warn(this.l('PleaseSelectRecord'));
            return;
        }
        if (this.assignservice == 0 && this.leadAction == 2) {
            this.notify.warn(this.l('PleaseSelectUser'));
            return;
        }
        if(this.leadAction == 1)
    {
        this._servicesServiceProxy.deleteManageServices(selectedids)
        .pipe(finalize(() => { }))
        .subscribe(() => {
            let log = new UserActivityLogDto();
            log.actionId = 83;
            log.actionNote ='Service Deleted';
            log.section = 'Manage Service';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
            this.reloadPage(null);
            this.notify.info(this.l('DeletedSuccessfully'));
        });

    } else if(this.leadAction == 2)
    {
        this._servicesServiceProxy.updateAssignUserID(this.assignservice, selectedids)
        .pipe(finalize(() => { }))
        .subscribe(() => {
            let log = new UserActivityLogDto();
            log.actionId = 27;
            log.actionNote ='Job Assigned' ;
            log.section = 'Manage Service';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
            this.reloadPage(null);
            this.notify.info(this.l('AssignedSuccessfully'));
        });
    }
        
    }

    duplicate(id,status): void {
        this.message.confirm('',
            "Are You Sure You want To Duplicate Record?",
            (isConfirmed) => {
                if (isConfirmed) {
                    this._servicesServiceProxy.changeDuplicateStatus(id,status)
                        .subscribe(() => {
                            let log = new UserActivityLogDto();
                            log.actionId = 5;
                            log.actionNote = "Service Status Changed To Duplicate";
                            log.section = 'Manage Service';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            }); 
                            this.reloadPage(null);
                            this.notify.success(this.l('StatusChangeToDuplicate'));
                        });
                }
            }
        );
    }

    notduplicate(id,status): void {
        this.message.confirm('',
        "Are You Sure You want To Duplicate Record?",
            (isConfirmed) => {
                if (isConfirmed) {
                    this._servicesServiceProxy.changeDuplicateStatus(id,status)
                        .subscribe(() => {
                            let log = new UserActivityLogDto();
                            log.actionId = 5;
                            log.actionNote = "Service Status Changed To Not Duplicate";
                            log.section = 'Manage Service';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            }); 
                            this.reloadPage(null);
                            this.notify.success(this.l('StatusChangeToDuplicate'));
                        });
                }
            }
        );
    }

    deleteLead(id): void {
        this.message.confirm('',
            this.l('AreYouSureYouWantToDeleteExistingService'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._servicesServiceProxy.deleteDuplicateServices(id)
                        .subscribe(() => {
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Previous Service Deleted';
                            log.section = 'Manage Service';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            }); 
                            this.reloadPage(null);
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    mergeRecord(id): void {
        this.message.confirm('',
            "Are You Sure You Want To Merge Record",
            (isConfirmed) => {
                if (isConfirmed) {
                    this._servicesServiceProxy.mergeServices(id)
                        .subscribe(() => {
                            this._servicesServiceProxy.deletemergeRecords(id)
                            .subscribe(() => {
                                let log = new UserActivityLogDto();
                                log.actionId = 82;
                                log.actionNote ='Merged Records' ;
                                log.section = 'Manage Service';
                                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                    .subscribe(() => {
                                }); 
                            this.reloadPage(null);
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                    });
                }
            }
        );
    }
    
    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Manage Service';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Manage Service';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }

}