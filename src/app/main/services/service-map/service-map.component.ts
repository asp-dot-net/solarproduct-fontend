import { Component, Injector, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ViewLalLongMissingJobComponent } from '@app/main/intaller-map/view-latlongmissingdata.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLookupServiceProxy, GetInstallerMapDto, GetServiceMapDto, InstallationServiceProxy, JobHouseTypeLookupTableDto, JobPaymentOptionLookupTableDto, JobRoofTypeLookupTableDto, JobsServiceProxy, JobStatusTableDto, LeadsServiceProxy, LeadStateLookupTableDto, OrganizationUnitDto, ServicesServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy, UserServiceProxy } from '@shared/service-proxies/service-proxies';
import { ServiceProxyModule } from '@shared/service-proxies/service-proxy.module';
import * as moment from 'moment';
import { LazyLoadEvent } from 'primeng';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-service-map',
  templateUrl: './service-map.component.html',
  styleUrls: ['./service-map.component.css'],
  encapsulation: ViewEncapsulation.None,
  animations: [appModuleAnimation()]
})
export class ServiceMapComponent extends AppComponentBase implements OnInit {

  
  lt = -27.137368;
  long = 134.789810;
  total = 0;
  filterText = "";
  housetypeId = 0;
  rooftypeid = 0;
  postalcode = "";
  jobstatusid = 0;
  panelmodel = "";
  invertmodel = "";
  paymenttypeid = 0;
  invertSuggestions: string[];
  panelSuggestions: string[];
  show: boolean = true;
  showchild: boolean = true;
  shouldShow: boolean = false;
  jobHouseTypes: JobHouseTypeLookupTableDto[];
  jobRoofTypes: JobRoofTypeLookupTableDto[];
  alljobstatus: JobStatusTableDto[];
  jobPaymentOptions: JobPaymentOptionLookupTableDto[];
  allStates: LeadStateLookupTableDto[];
  stateNameFilter = "";
  dateFilterType = 'All';
  areaNameFilter = "";
  steetAddressFilter = "";
  postalcodefrom = "";
  postalcodeTo = "";
  priority = 0;
  // StartDate: moment.Moment;
  // EndDate: moment.Moment;
  totalMapData = 0;
  totalDepRcvMapData = 0;
  totalActiveMapData = 0;
  totalJobBookedMapData = 0;
  totalJobInstallMapData = 0;
  MissingDatas = [];
  allOrganizationUnits: OrganizationUnitDto[];
  organizationUnit = 0;
  other = 0;
  organizationUnitlength: number = 0;
  date = new Date();
  toggle: boolean = true;
  filterName = "JobNumber";
  startDate: moment.Moment = moment(this.date);
  endDate: moment.Moment = moment(this.date);
    change() {
        this.toggle = !this.toggle;
      }
  // public sampleDateRange: moment.Moment[] = [moment(this.date), moment().endOf('day')];
  icon: 'https://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png';
  //   icon = {
  //     url: 'https://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png',
  //     size: [100, 100],
  //     scaledSize: [50, 50],
  //     anchor: [25, 25]
  // };
  public customestyles = [{
    "featureType": "poi",
    "elementType": "labels",
    "stylers": [{
      visibility: "off",
    }]
  },];
  markers: GetServiceMapDto[
    // {
    //   lt: 21.1594627,
    //   long: 72.6822083,
    //   label: 'Surat'
    // },
    // {
    //   lt: 23.0204978,
    //   long: 72.4396548,
    //   label: 'Ahmedabad'
    // },
    // {
    //   lt: 22.2736308,
    //   long: 70.7512555,
    //   label: 'Rajkot'
    // }
  ];
  @ViewChild('viewLatLongMissingModal', { static: true }) viewLatLongMissingModal: ViewLalLongMissingJobComponent;
  constructor(
    injector: Injector,
    private _insallationServiceProxy: InstallationServiceProxy,
    private _commonLookupService: CommonLookupServiceProxy,
    private _jobsServiceProxy: JobsServiceProxy,
    private _leadsServiceProxy: LeadsServiceProxy,
    private _userServiceProxy: UserServiceProxy,
    private titleService: Title,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _servicesServiceProxy: ServicesServiceProxy,
  ) { 
    super(injector);

  }

  ngOnInit(): void {
    this.titleService.setTitle(this.appSession.tenancyName + " |  Service Map");
    this._commonLookupService.getOrganizationUnit().subscribe(output => {
      let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Service Map';
            log.section = 'Service Map';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
      this.allOrganizationUnits = output;
      this.organizationUnit = this.allOrganizationUnits[0].id;
      this.organizationUnitlength = this.allOrganizationUnits.length;
      this.getMapDetail();
    });
    this._jobsServiceProxy.getAllRoofTypeForTableDropdown().subscribe(result => {
      this.jobRoofTypes = result;
    });
    this._jobsServiceProxy.getAllHouseTypeForTableDropdown().subscribe(result => {
      this.jobHouseTypes = result;
    });

    this._commonLookupService.getAllJobStatusTableDropdown().subscribe(result => {
      this.alljobstatus = result;
    });
    this._commonLookupService.getAllPaymentOptionForTableDropdown().subscribe(result => {
      this.jobPaymentOptions = result;
    });
    this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
      this.allStates = result;
    });
  }

  mapStyle:string = 'customestyles';
  checkedStyle (event: any) {
    if (event.target.checked){
      this.mapStyle = 'darkStyle';
    }
    else{
      this.mapStyle = 'customestyles';
    }
    
  }

  public darkStyle = [
    
    { elementType: "geometry", stylers: [{ color: "#242f3e" }] },
    { elementType: "labels.text.stroke", stylers: [{ color: "#242f3e" }] },
    { elementType: "labels.text.fill", stylers: [{ color: "#746855" }] },
    {
      featureType: "administrative.locality",
      elementType: "labels.text.fill",
      stylers: [{ color: "#d59563" }],
    },
    {
      featureType: "poi",
      elementType: "labels.text.fill",
      stylers: [{ color: "#d59563" }],
    },
    {
      featureType: "poi.park",
      elementType: "geometry",
      stylers: [{ color: "#263c3f" }],
    },
    {
      featureType: "poi.park",
      elementType: "labels.text.fill",
      stylers: [{ color: "#6b9a76" }],
    },
    {
      featureType: "road",
      elementType: "geometry",
      stylers: [{ color: "#38414e" }],
    },
    {
      featureType: "road",
      elementType: "geometry.stroke",
      stylers: [{ color: "#212a37" }],
    },
    {
      featureType: "road",
      elementType: "labels.text.fill",
      stylers: [{ color: "#9ca5b3" }],
    },
    {
      featureType: "road.highway",
      elementType: "geometry",
      stylers: [{ color: "#746855" }],
    },
    {
      featureType: "road.highway",
      elementType: "geometry.stroke",
      stylers: [{ color: "#1f2835" }],
    },
    {
      featureType: "road.highway",
      elementType: "labels.text.fill",
      stylers: [{ color: "#f3d19c" }],
    },
    {
      featureType: "transit",
      elementType: "geometry",
      stylers: [{ color: "#2f3948" }],
    },
    {
      featureType: "transit.station",
      elementType: "labels.text.fill",
      stylers: [{ color: "#d59563" }],
    },
    {
      featureType: "water",
      elementType: "geometry",
      stylers: [{ color: "#17263c" }],
    },
    {
      featureType: "water",
      elementType: "labels.text.fill",
      stylers: [{ color: "#515c6d" }],
    },
    {
      featureType: "water",
      elementType: "labels.text.stroke",
      stylers: [{ color: "#17263c" }],
    },
  
];

  filterPanelModel(event): void {
    this._jobsServiceProxy.getpanelmodel(event.query).subscribe(output => {
      this.panelSuggestions = output;
    });
  }

  filterInvertModel(event): void {
    this._jobsServiceProxy.getinvertmodel(event.query).subscribe(output => {
      this.invertSuggestions = output;
    });
  }

  getMapDetail(event?: LazyLoadEvent) {
    // debugger;
    // if (this.sampleDateRange != null) {
    //   this.StartDate = this.sampleDateRange[0];
    //   this.EndDate = this.sampleDateRange[1];
    // } else {
    //   this.StartDate = null;
    //   this.EndDate = null;
    // }

    this._servicesServiceProxy.getServiceMap(
      this.filterName,
      this.organizationUnit,
      this.filterText,
      this.housetypeId,
      this.rooftypeid,
      this.jobstatusid,
      this.panelmodel,
      this.invertmodel,
      this.paymenttypeid,
      this.areaNameFilter,
      this.dateFilterType,
      this.stateNameFilter,
      this.steetAddressFilter,
      this.postalcodefrom,
      this.postalcodeTo,
      this.startDate,
      this.endDate,
      this.priority,

    ).subscribe(result => {
      debugger;
      this.markers = result;
      this.total = result.length;
      this.shouldShow = false;
      if (this.total > 0) {
        this.totalMapData = result[0].totalMapData;
      } else {
        this.totalMapData = 0;
      }
    });
  }

  onmodel(MissingDatas): void {
    this.viewLatLongMissingModal.showDetail(MissingDatas);
}

addSearchLog(filter): void {
  let log = new UserActivityLogDto();
      log.actionId = 80;
      log.actionNote ='Searched by '+ filter ;
      log.section = 'Service Map';
      this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
          .subscribe(() => {
      }); 
}

RereshLog():void{
  let log = new UserActivityLogDto();
  log.actionId = 80;
  log.actionNote ='Refresh the data' ;
  log.section = 'Service Map';
  this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
      .subscribe(() => {
  }); 
}

}
