﻿import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { ActivatedRoute , Router} from '@angular/router';
import { AbpSessionService, NotifyService, PermissionCheckerService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLookupDto, CommonLookupServiceProxy, InstallationCalendarDto, InstallationServiceProxy, InstallerServiceProxy, LeadsServiceProxy, LeadStateLookupTableDto, OrganizationUnitDto, ServiceInstallationCalendarDto, ServicesServiceProxy, TokenAuthServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy, UserServiceProxy } from '@shared/service-proxies/service-proxies';
import { MyServiceSmsemailModelComponent } from '../myservices/myservice-smsemail-model/myservice-smsemail-model.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import * as moment from 'moment';
import { Title } from '@angular/platform-browser';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './serviceInstallation.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})

export class ServiceInstallationComponent extends AppComponentBase {
    @ViewChild('myserviceSmsEmailModel', { static: true }) myserviceSmsEmailModel: MyServiceSmsemailModelComponent;
    
    advancedFiltersAreShown = false;
    filterText = '';
    cancelReasonNameFilter = '';
    installerId = 0;
    calendaruserid = 0;
    // date= moment(new Date(), 'DD/MM/YYYY');
    date = moment().startOf('isoWeek').toDate();
    recordStartDate = moment(this.date, 'DD/MM/YYYY');
    InstallationList: ServiceInstallationCalendarDto[];
    InstallerList: CommonLookupDto[];
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnit = 0;
    organizationUnitlength: number = 0;
    areaNameFilter = '';
    stateNameFilter = '';
    allStates: LeadStateLookupTableDto[];
    toggle: boolean = true;

    change() {
        this.toggle = !this.toggle;
      }
    constructor(
        injector: Injector,
        private _commonLookupService: CommonLookupServiceProxy,
        private _installationServiceProxy: InstallationServiceProxy,
        private _permissionChecker: PermissionCheckerService,
        private _installerServiceProxy: InstallerServiceProxy,
        private _sessionService: AbpSessionService,
        private _leadsServiceProxy : LeadsServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _servicesServiceProxy: ServicesServiceProxy,
        private titleService: Title
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Service Installation");
    }
    ngOnInit(): void {
        this._leadsServiceProxy.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
        if (this._permissionChecker.isGranted('Pages.Service.ServiceInstaller')) {
            this.getInstaller();
        }
        else
        {
            this.calendaruserid=this._sessionService.userId;
        }
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Service Installation';
            log.section = 'Service Installation';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.getInstallations();
        });
       
    }

    getInstaller(): void {
        this._installerServiceProxy.getAllInstallers(this.organizationUnit).subscribe(result => {
          this.InstallerList = result;
      });
    };

    previousDate(): void {
        this.recordStartDate = moment(this.recordStartDate).add(-7, 'days');
        this.getInstallations();
    };

    nextDate(): void {
        this.recordStartDate = moment(this.recordStartDate).add(7, 'days');
        this.getInstallations();
    };

    getInstallations(): void {
        if (this._permissionChecker.isGranted('Pages.Service.ServiceInstaller')) {
          this.calendaruserid=this.installerId;
        }
        this._servicesServiceProxy.getServiceInstallation(this.calendaruserid,this.recordStartDate, this.organizationUnit,this.areaNameFilter,this.stateNameFilter).subscribe(result => {
          this.InstallationList=result;
        });
    }

    installerSearchResult: any [];
    filterInstaller(event): void {
        this.installerSearchResult = Object.assign([], this.InstallerList).filter(
        item => item.displayName.toLowerCase().indexOf(event.query.toLowerCase()) > -1
        )
    }

    selectInstaller(event): void {
        this.installerId = event.id;
        this.getInstallations();
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Service Installation';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
      }
      
}