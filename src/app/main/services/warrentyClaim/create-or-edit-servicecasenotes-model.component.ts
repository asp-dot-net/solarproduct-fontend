import { Component, ElementRef, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ServicesServiceProxy, GetMyServiceForEditOutput, CreateOrEditServiceDto, UserActivityLogDto, UserActivityLogServiceProxy,} from '@shared/service-proxies/service-proxies';
import * as _ from 'lodash';
import * as moment from 'moment';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'serviceCaseNotesModal',
    templateUrl: './create-or-edit-servicecasenotes-model.component.html'
})
export class ServiceCaseNotesModalComponent extends AppComponentBase {

    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    leadCompanyName: any;
    saving = false;
    item: GetMyServiceForEditOutput;
    caseNo = "";
    caseNotes = "";
    caseStatus = "";
    trackingNo = "";
    trackingNotes = "";
    stockArrivalDate : moment.Moment;
    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _ServicesServiceProxy: ServicesServiceProxy,
    ) {
        super(injector);
        this.item = new GetMyServiceForEditOutput();
        this.item.service = new CreateOrEditServiceDto();
    }

  SectionName = '';
    show(Service: number, section = ''): void {
        debugger;
        this.SectionName = section;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote = "Open For Add Notes";
        log.section = section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
        // if (this.holdReasons == null) {
        //     this._ServicesServiceProxy.getAllHoldReasonForTableDropdown().subscribe(result => {
        //         this.holdReasons = result;
        //     });
        // }
        this._ServicesServiceProxy.getserviceForEdit(Service).subscribe(result => {
            this.item.service = result.service;
            this.caseNotes = this.item.service.caseNotes;
            this.caseNo = this.item.service.caseNo;
            this.caseStatus = this.item.service.caseStatus;
            this.trackingNotes = this.item.service.trackingNotes;
            this.trackingNo = this.item.service.trackingNo;
            this.modal.show();
        });
    }

    save(): void {
        debugger;
        this.saving = true;
        this.item.service.caseNo =  this.caseNo;
        this.item.service.caseNotes =  this.caseNotes;
        this.item.service.caseStatus =  this.caseStatus;
        this.item.service.trackingNo =  this.trackingNo;
        this.item.service.trackingNotes =  this.trackingNotes;
        this._ServicesServiceProxy.createOrEditService(33,this.SectionName,this.item.service)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.close();
                this.modalSave.emit(null);
                this.notify.info(this.l('SavedSuccessfully'));
            });
    }

    close(): void {
        this.modal.hide();
    }

    onStatusChange(event) :void{
        this.caseStatus =  event.target.value;
        if(this.caseStatus == 'Panding'){
            this.trackingNotes = '';
            this.trackingNo = '';
        }
    }
}