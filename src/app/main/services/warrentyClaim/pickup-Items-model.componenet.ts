import { Component, ElementRef, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ServicesServiceProxy, GetMyServiceForEditOutput, CreateOrEditServiceDto, UserActivityLogDto, UserActivityLogServiceProxy,} from '@shared/service-proxies/service-proxies';
import * as _ from 'lodash';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'itemPickupModal',
    templateUrl: './pickup-Items-model.componenet.html'
})
export class ItemPickupModalComponent extends AppComponentBase {

    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    leadCompanyName: any;
    saving = false;
    item: GetMyServiceForEditOutput;
    pickBy = "";
    pickupdate : moment.Moment;
    pickupMethod = "";
    pickupNotes = "";
    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _ServicesServiceProxy: ServicesServiceProxy,
    ) {
        super(injector);
        this.item = new GetMyServiceForEditOutput();
        this.item.service = new CreateOrEditServiceDto();
    }

  SectionName = '';
  show(Service: number, section = ''): void {
        debugger;
        this.SectionName = section;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote = "Open For Add Pickup Item";
        log.section = section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
        // if (this.holdReasons == null) {
        //     this._ServicesServiceProxy.getAllHoldReasonForTableDropdown().subscribe(result => {
        //         this.holdReasons = result;
        //     });
        // }
        this._ServicesServiceProxy.getserviceForEdit(Service).subscribe(result => {
            this.item.service = result.service;
            this.pickBy = this.item.service.pickby;
            this.pickupdate = this.item.service.pickUpDate;
            this.pickupMethod = this.item.service.pickupMethod;
            this.pickupNotes = this.item.service.pickupNotes;
            this.modal.show();
        });
    }

    save(): void {
        debugger;
        this.saving = true;
        this.item.service.pickby =  this.pickBy;
        this.item.service.pickUpDate =  this.pickupdate;
        this.item.service.pickupMethod =  this.pickupMethod;
        this.item.service.pickupNotes =  this.pickupNotes;
        this._ServicesServiceProxy.createOrEditService(33, this.SectionName,this.item.service)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.close();
                this.modalSave.emit(null);
                this.notify.info(this.l('SavedSuccessfully'));
            });
    }

    close(): void {
        this.modal.hide();
    }

    
}