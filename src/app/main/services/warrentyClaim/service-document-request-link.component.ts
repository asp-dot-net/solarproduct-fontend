import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import {ServiceDocumentRequestServiceProxy,  SendServiceDocumentRequestInputDto, CommonLookupDto,GetServiceDocumentForView, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
import * as moment from 'moment';
import { AppConsts } from '@shared/AppConsts';

@Component({
    selector: 'serviceDocumentRequestLinkModal',
    templateUrl: './service-document-request-link.component.html'
})

export class ServiceDocumentRequestLinkModalComponent extends AppComponentBase {

    @ViewChild('serviceDocumentrequestlinkModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    saving = false;
    DocumentTypes: CommonLookupDto[];
    jobId:number;
    documentTypeId:number;
    jobNumber: string;
    sectionId: number;
    serviceId: number;
    serviceDocuments: GetServiceDocumentForView[];
    organizationUnit : number;
    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _serviceDocumentRequestServiceProxy: ServiceDocumentRequestServiceProxy
    ) {
        super(injector);
    }

  SectionName = '';
  show(jobId: number, sectionId: any, serviceId :any, jobNumber : any, organizationUnit : any, section = ''): void {
        debugger;
        this.SectionName = section;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote = "Open For Document Request";
        log.section = section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
        this.showMainSpinner();
        this.jobId = jobId;
        this.sectionId = sectionId;
        this.serviceId = serviceId;
        this.jobNumber = jobNumber;
        this.organizationUnit = organizationUnit ;
        this._serviceDocumentRequestServiceProxy.getActiveDocumentType().subscribe(result => {
            this.DocumentTypes = result;

            this.modal.show();
            this.hideMainSpinner();

        }, e => { this.hideMainSpinner(); });
        this.getServiceDocuments();
    }

    save(sendMode: any): void {
        this.saving = true;

        let sendDocumentRequestInputDto = new SendServiceDocumentRequestInputDto;
        sendDocumentRequestInputDto.jobId = this.jobId;
        sendDocumentRequestInputDto.serviceDocTypeId = this.documentTypeId;
        sendDocumentRequestInputDto.sectionId = this.sectionId;
        sendDocumentRequestInputDto.serviceId = this.serviceId;
        sendDocumentRequestInputDto.sendMode = sendMode

        let str = sendMode == 'Email' ? "Email Send Successfully" : "SMS Send Successfully"

        this._serviceDocumentRequestServiceProxy.sendServiceDocumentRequestForm(sendDocumentRequestInputDto).pipe(finalize(() => { this.saving = false;}))
        .subscribe(() => {
            let log = new UserActivityLogDto();
        log.actionId = sendMode == 'Email' ? 7 : 6;
        log.actionNote = sendMode == 'Email' ? "Document Request Sent on Email" : "Document Request Sent on SMS";
        log.section = this.SectionName;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
            this.notify.info(str);
            this.close();
            this.modalSave.emit(null);
        });  
    }

    getServiceDocuments(): void {
        this._serviceDocumentRequestServiceProxy.getServiceDocumentsByServiceId(this.serviceId).subscribe(result => {
            this.serviceDocuments = result;
        })
    }

    downloadfile(file): void {

        var date = moment(file.creationTime).toDate()
        var MigrationDate = new Date('2022/09/17')

        if(this.organizationUnit == 7) {
            MigrationDate = new Date('2023/01/04')
        }

        if(this.organizationUnit == 8 || this.organizationUnit == 7)
        {
            if(date < MigrationDate)
            {
                let FileName = AppConsts.oldDocUrl + "/" + file.serviceDocumentPath + file.fileName;
                window.open(FileName, "_blank");
            }
            else{
                let FileName = AppConsts.docUrl + "/" + file.serviceDocumentPath + file.fileName;
                window.open(FileName, "_blank");
            }
        }
        else{
            let FileName = AppConsts.docUrl + "/" + file.serviceDocumentPath + file.fileName;
            window.open(FileName, "_blank");
        }
    }

    deleteDocument(documentId) : void {
        this.message.confirm(
            this.l('AreYouSureWanttoDelete'),
            this.l('Delete'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._serviceDocumentRequestServiceProxy.deleteServiceDocument(documentId,  this.sectionId)
                        .subscribe(() => {
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            this.getServiceDocuments();
                        });
                }
            }
        );
    }
    close(): void {
        this.modal.hide();
    }
}