import { Component, Injector, ViewEncapsulation, ViewChild, Input, OnInit, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLookupDto, CommonLookupServiceProxy, GetServiceDocumentForView, LeadsServiceProxy, LeadStateLookupTableDto, LeadUsersLookupTableDto, OrganizationUnitDto, ServiceIncoiceAppServicesServiceProxy, TokenAuthServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import * as _ from 'lodash';
import * as moment from 'moment';
import { Title } from '@angular/platform-browser';
import { ViewMyLeadComponent } from '@app/main/myleads/myleads/view-mylead.component';
import { finalize } from 'rxjs/operators';
import { ViewApplicationModelComponent } from '@app/main/jobs/jobs/view-application-model/view-application-model.component';
import { CreateOrEditMyServiceOptionModalComponent } from '../myservices/create-or-edit-myservice-modal.component';
import { MyServiceSmsemailModelComponent } from '../myservices/myservice-smsemail-model/myservice-smsemail-model.component';
import { AddActivityModalComponent } from '@app/main/myleads/myleads/add-activity-model.component';
import { AddServiceAttachmentComponent } from '../myservices/add-serviceattachment.component';
import { ServiceDocumentRequestLinkModalComponent } from './service-document-request-link.component';
import { ServiceCaseNotesModalComponent } from './create-or-edit-servicecasenotes-model.component';
import { ItemPickupModalComponent } from './pickup-Items-model.componenet';
import { CreateOrEditServiceInvoiceModalComponent } from './create-or-edit-serviceInvoice-model.component';
@Component({
    templateUrl: './serviceInvoice.component.html',
    styleUrls: ['../myservices/myservice.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ServiceInvoiceModelComponent extends AppComponentBase implements OnInit {
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('createOrEditServiceInvoiceModal', { static: true }) createOrEditServiceInvoiceModal : CreateOrEditServiceInvoiceModalComponent;
    @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;

    // @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
    // @ViewChild('viewApplicationModal', { static: true }) viewApplicationModal: ViewApplicationModelComponent;
    // @ViewChild('createOrEditMyServiceOptionModal', { static: true }) createOrEditMyServiceOptionModal: CreateOrEditMyServiceOptionModalComponent;
    // @ViewChild('myserviceSmsEmailModel', { static: true }) myserviceSmsEmailModel: MyServiceSmsemailModelComponent;
    // @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    // @ViewChild('addserviceinstallerModal', { static: true }) addserviceinstallerModal: AddActivityModalComponent;
    // @ViewChild('addserviceAttachmentModal', { static: true }) addserviceAttachmentModal: AddServiceAttachmentComponent;
    // @ViewChild('serviceDocumentRequestLinkModal', { static: true }) serviceDocumentRequestLinkModal: ServiceDocumentRequestLinkModalComponent;
    // @ViewChild('serviceCaseNotesModal', { static: true }) serviceCaseNotesModal : ServiceCaseNotesModalComponent;
    // @ViewChild('itemPickupModal', { static: true }) itemPickupModal : ItemPickupModalComponent;
    
    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;  
    FiltersData1 = true;  

    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };
    allOrganizationUnits: OrganizationUnitDto[];
    allStates: LeadStateLookupTableDto[];
    allServieCategorys: CommonLookupDto[];
    allServieSource: CommonLookupDto[];
    allServieStatus: CommonLookupDto[];
    allServiePrioritys: CommonLookupDto[];
    allServiceUsers: LeadUsersLookupTableDto[];
    organizationUnit = 0;
    organizationUnitlength: number = 0;
    sampleDateRange: moment.Moment[] = [moment().add(-7, 'days').endOf('day'), moment().add(0, 'days').startOf('day')];
    StartDate: moment.Moment;
    EndDate: moment.Moment;
    FiltersData = true;
    filterText = '';
    serviceCategory = '';
    stateNameFilter = '';
    adressFilter = '';
    firstrowcount = 0;
    last = 0;
    ExpandedView: boolean = true;
    SelectedLeadId: number = 0;
    assignservice = 0;
    tableRecords: any;
    totalcount = 0;
    count = 0;
    role: string = '';
    servicestatusFilter = 4;
    servicepriority = 0;
    serviceSource = '';
    allServiceManagerUsers: LeadUsersLookupTableDto[];
    total = 0;
    open = 0;
    pending = 0;
    assign = 0;
    resolved = 0;
    closed = 0;
    datefilter = "All";    

    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 410;
    toggle: boolean = true;

    change() {
        this.toggle = !this.toggle;
    }

    constructor(
        injector: Injector,
        private _serviceIncoiceAppServicesServiceProxy: ServiceIncoiceAppServicesServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _router: Router,
        private titleService: Title,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy

    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Service Invoice");
    }

    ngOnInit(): void {
        // this.screenHeight = window.innerHeight;
        // debugger;
        // this._commonLookupService.getServieCategoryDropdown().subscribe(result => {
        //     this.allServieCategorys = result;
        // });
        // this._commonLookupService.getServieSourceDropdown().subscribe(result => {
        //     this.allServieSource = result;
        // });
        // this._commonLookupService.getServieStatusDropdown().subscribe(result => {
        //     this.allServieStatus = result;
        // });
        // this._commonLookupService.getServiePriorityDropdown().subscribe(result => {
        //     this.allServiePrioritys = result;
        // });
        // this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
        //     this.allStates = result;
        // });
        // this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
        //     this.role = result;
        // });
        // this._commonLookupService.getOrganizationUnit().subscribe(output => {
        //     this.allOrganizationUnits = output;
        //     this.organizationUnit = this.allOrganizationUnits[0].id;
        //     this.organizationUnitlength = this.allOrganizationUnits.length;
        //     this.getAllServiceInvoice();
        //     this.getusers();
        // });
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Open Service Invoice';
        log.section = 'Service Invoice';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
        this.getAllServiceInvoice();
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + 159 ;
        }

        else {
            this.testHeight = this.testHeight - 159 ;
        }
    }

    getusers() {
        this._commonLookupService.getServiceUserForFilter(this.organizationUnit).subscribe(result => {
            this.allServiceManagerUsers = result;
        });
    }
    getAllServiceInvoice(event?: LazyLoadEvent) {
        debugger;
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);

            return;
        }
        if (this.sampleDateRange != null) {
            this.StartDate = this.sampleDateRange[0];
            this.EndDate = this.sampleDateRange[1];
        } else {
            this.StartDate = null;
            this.EndDate = null;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._serviceIncoiceAppServicesServiceProxy.getAll(
            this.filterText,
            this.StartDate,
            this.EndDate,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            10
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.tableRecords = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            
             
            this.shouldShow = false;
        });
    }

   

    clear() {
        this.stateNameFilter = '';
        this.organizationUnit = 0;
        this.filterText = '';
        this.serviceCategory = '';
        this.adressFilter = '';
        this.stateNameFilter = '';
        this.servicestatusFilter = 0;
        this.servicepriority = 0;
        this.serviceSource = '';
        this.sampleDateRange = [moment().add(-7, 'days').endOf('day'), moment().add(0, 'days').startOf('day')];
        this.getAllServiceInvoice();
    }

    expandGrid() {
        this.ExpandedView = true;
    }

    navigateToLeadDetail(leadid,service): void {
        this.ExpandedView = !this.ExpandedView;
        this.ExpandedView = false;
        this.SelectedLeadId = leadid;
        this.viewLeadDetail.showDetail(leadid, null, 22, service,'Service Invoice');
    }

    reloadPage($event): void {
        this.paginator.changePage(this.paginator.getPage());
    }


    isAllChecked() {
        if (this.tableRecords)
            return this.tableRecords.every(_ => _.isSelected);
    }
    checkAll(ev) {

        this.tableRecords.forEach(x => x.isSelected = ev.target.checked);
        this.oncheckboxCheck();
        /// this.totalcount = this.tableRecords.forEach(x => x.lead.isSelected = ev.target.checked);
    }

    oncheckboxCheck() {

        this.count = 0;
        this.tableRecords.forEach(item => {
            if (item.isSelected == true) {
                this.count = this.count + 1;
            }
        })
    }
    

    createService(): void {
        //this.createOrEditMyServiceOptionModal.show(0, this.organizationUnit);
    }

    // delete(id): void {
    //     this.message.confirm('',
    //         this.l('AreYouSureYouWantToDeleteExistingService'),
    //         (isConfirmed) => {
    //             if (isConfirmed) {
    //                 this._servicesServiceProxy.deleteDuplicateServices(id)
    //                     .subscribe(() => {
    //                         this.reloadPage(null);
    //                         this.notify.success(this.l('SuccessfullyDeleted'));
    //                     });
    //             }
    //         }
    //     );
    // }

}