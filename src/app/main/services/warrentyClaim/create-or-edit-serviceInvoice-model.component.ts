import { Component, ElementRef, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ServiceIncoiceAppServicesServiceProxy, GetMyServiceForEditOutput, CreateOrEditServiceInvoDto, UserActivityLogDto, UserActivityLogServiceProxy} from '@shared/service-proxies/service-proxies';
import * as _ from 'lodash';
import * as moment from 'moment';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrEditServiceInvoiceModal',
    templateUrl: './create-or-edit-serviceInvoice-model.component.html'
})
export class CreateOrEditServiceInvoiceModalComponent extends AppComponentBase {

    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    leadCompanyName: any;
    saving = false;
    item: GetMyServiceForEditOutput;
    caseNo = "";
    caseNotes = "";
    caseStatus = "";
    trackingNo = "";
    trackingNotes = "";
    stockArrivalDate : moment.Moment;
    serviceinvo: CreateOrEditServiceInvoDto ;
    status :string;

    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _serviceIncoiceAppServicesServiceProxy: ServiceIncoiceAppServicesServiceProxy,
    ) {
        super(injector);
         this.serviceinvo = new CreateOrEditServiceInvoDto();
        // this.item.service = new CreateOrEditServiceInvoDto();
    }

  SectionName = '';
  show(Service: number, jobId : number,status : string, section = ''): void {
        this.SectionName = section;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote = Service > 0 ? "Open For Update Invoice" : "Open For Add Invoice";
        log.section = section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
        this.status = status;
        this._serviceIncoiceAppServicesServiceProxy.getserviceForEdit(Service).subscribe(result => {
            if(result.id != 0){
                this.serviceinvo = result;
                
            }
            else{
                this.serviceinvo = new CreateOrEditServiceInvoDto();
                this.serviceinvo.serviceID = Service;
                this.serviceinvo.jobID = jobId;
            } 
        });

        this.modal.show();
        
    }

    save(): void {
        debugger;
        this.saving = true;
        // this.item.service.caseNo =  this.caseNo;
        // this.item.service.caseNotes =  this.caseNotes;
        // this.item.service.caseStatus =  this.caseStatus;
        // this.item.service.trackingNo =  this.trackingNo;
        // this.item.service.trackingNotes =  this.trackingNotes;
        this._serviceIncoiceAppServicesServiceProxy.createOrEditService(33,this.SectionName,this.serviceinvo)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.close();
                this.modalSave.emit(null);
                this.notify.info(this.l('SavedSuccessfully'));
            });
    }

    close(): void {
        this.modal.hide();
    }

    onStatusChange(event) :void{
        this.caseStatus =  event.target.value;
        if(this.caseStatus == 'Panding'){
            this.trackingNotes = '';
            this.trackingNo = '';
        }
    }
}