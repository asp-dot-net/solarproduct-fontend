﻿import { Component, Injector, ViewEncapsulation, ViewChild, Input, OnInit, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivityLogServiceProxy, CommonLookupDto, CommonLookupServiceProxy, LeadsServiceProxy, LeadStateLookupTableDto, LeadUsersLookupTableDto, OrganizationUnitDto, ServicesServiceProxy, TokenAuthServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import * as _ from 'lodash';
import * as moment from 'moment';
import { Title } from '@angular/platform-browser';
import { ViewMyLeadComponent } from '@app/main/myleads/myleads/view-mylead.component';
import { finalize } from 'rxjs/operators';
import { ViewApplicationModelComponent } from '@app/main/jobs/jobs/view-application-model/view-application-model.component';
import { CreateOrEditMyServiceOptionModalComponent } from './create-or-edit-myservice-modal.component';
import { MyServiceSmsemailModelComponent } from './myservice-smsemail-model/myservice-smsemail-model.component';
import { AddActivityModalComponent } from '@app/main/myleads/myleads/add-activity-model.component';
import { AddServiceAttachmentComponent } from './add-serviceattachment.component';
import { CommentModelComponent } from '@app/main/activitylog/comment-modal.component';
import { NotifyModelComponent } from '@app/main/activitylog/notify-modal.component';
import { ReminderModalComponent } from '@app/main/activitylog/reminder-modal.component';
import { ToDoModalComponent } from '@app/main/activitylog/todo-modal.component';

@Component({
    templateUrl: './myservice.component.html',
    styleUrls: ['./myservice.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class MyServicesComponent extends AppComponentBase implements OnInit {
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
    @ViewChild('viewApplicationModal', { static: true }) viewApplicationModal: ViewApplicationModelComponent;
    @ViewChild('createOrEditMyServiceOptionModal', { static: true }) createOrEditMyServiceOptionModal: CreateOrEditMyServiceOptionModalComponent;
    @ViewChild('myserviceSmsEmailModel', { static: true }) myserviceSmsEmailModel: MyServiceSmsemailModelComponent;
    @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    @ViewChild('addserviceinstallerModal', { static: true }) addserviceinstallerModal: AddActivityModalComponent;
    @ViewChild('addserviceAttachmentModal', { static: true }) addserviceAttachmentModal: AddServiceAttachmentComponent;
    @ViewChild('notifyModel', { static: true }) notifyModel: NotifyModelComponent;
    @ViewChild('commentModel', { static: true }) commentModel: CommentModelComponent;
    @ViewChild('todoModal', { static: true }) todoModal: ToDoModalComponent;
    @ViewChild('ReminderModal', { static: true }) ReminderModal: ReminderModalComponent;
    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;  
    FiltersData1 = true;  
    filterName = "JobNumber";
    orgCode = '';

    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };
    allOrganizationUnits: OrganizationUnitDto[];
    allStates: LeadStateLookupTableDto[];
    allServieCategorys: CommonLookupDto[];
    allServieSource: CommonLookupDto[];
    allServieStatus: CommonLookupDto[];
    allServiePrioritys: CommonLookupDto[];
    allServiceUsers: LeadUsersLookupTableDto[];
    organizationUnit = 0;
    organizationUnitlength: number = 0;
    // sampleDateRange: moment.Moment[] = [moment().add(-7, 'days').endOf('day'), moment().add(0, 'days').startOf('day')];
    // StartDate: moment.Moment;
    // EndDate: moment.Moment;
    date = new Date();
    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);
    FiltersData = true;
    filterText = '';
    serviceCategory = '';
    stateNameFilter = '';
    adressFilter = '';
    firstrowcount = 0;
    last = 0;
    ExpandedView: boolean = true;
    SelectedLeadId: number = 0;
    assignservice = 0;
    tableRecords: any;
    totalcount = 0;
    count = 0;
    role: string = '';
    servicestatusFilter = 4;
    servicepriority = 0;
    serviceSource = '';
    allServiceManagerUsers: LeadUsersLookupTableDto[];
    total = 0;
    open = 0;
    pending = 0;
    assign = 0;
    resolved = 0;
    closed = 0;
    datefilter = "All";    

    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 410;
    toggle: boolean = true;

    change() {
        this.toggle = !this.toggle;
    }

    constructor(
        injector: Injector,
        private _servicesServiceProxy: ServicesServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _router: Router,
        private titleService: Title,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy

    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  My Service");
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        debugger;
        this._commonLookupService.getServieCategoryDropdown().subscribe(result => {
            this.allServieCategorys = result;
        });
        this._commonLookupService.getServieSourceDropdown().subscribe(result => {
            this.allServieSource = result;
        });
        this._commonLookupService.getServieStatusDropdown().subscribe(result => {
            this.allServieStatus = result;
        });
        this._commonLookupService.getServiePriorityDropdown().subscribe(result => {
            this.allServiePrioritys = result;
        });
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
        this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
            this.role = result;
        });
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open My Service';
            log.section = 'My Service';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.orgCode = this.allOrganizationUnits[0].code;
            this.getServices();
            this.getusers();
        });
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + 159 ;
        }

        else {
            this.testHeight = this.testHeight - 159 ;
        }
    }

    getusers() {
        this._commonLookupService.getServiceUserForFilter(this.organizationUnit).subscribe(result => {
            this.allServiceManagerUsers = result;
        });
    }
    changeOrgCode() {
        this.orgCode = this.allOrganizationUnits.find(x => x.id == this.organizationUnit).code;
    }
    getServices(event?: LazyLoadEvent) {
        debugger;
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);

            return;
        }
        // if (this.sampleDateRange != null) {
        //     this.StartDate = this.sampleDateRange[0];
        //     this.EndDate = this.sampleDateRange[1];
        // } else {
        //     this.StartDate = null;
        //     this.EndDate = null;
        // }

        this.primengTableHelper.showLoadingIndicator();

        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }

        this._servicesServiceProxy.getAllForMyService(
            this.filterName,
            filterText_,
            this.startDate,
            this.endDate,
            this.organizationUnit,
            this.serviceCategory,
            this.stateNameFilter,
            this.adressFilter,
            this.servicestatusFilter,
            this.servicepriority,
            this.serviceSource,
            this.datefilter,
            '',
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            debugger;
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.tableRecords = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            if(result.totalCount > 0)
            {
                this.total = result.totalCount;
                this.getLeadSummaryCount();
            } else {
                this.total = 0;
                this.open = 0;
                this.pending = 0;
                this.assign = 0;
                this.resolved = 0;
                this.closed = 0;
            }
             
            this.shouldShow = false;
        });
    }

    getLeadSummaryCount(){
        debugger;
        // if (this.sampleDateRange != null) {
        //     this.StartDate = this.sampleDateRange[0];
        //     this.EndDate = this.sampleDateRange[1];
        // } else {
        //     this.StartDate = null;
        //     this.EndDate = null;
        // }

        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }

        this._servicesServiceProxy.getAllForMyServiceCount(
            this.filterName,
            filterText_,
            this.startDate,
            this.endDate,
            this.organizationUnit,
            this.serviceCategory,
            this.stateNameFilter,
            this.adressFilter,
            this.servicestatusFilter,
            this.servicepriority,
            this.serviceSource,
            this.datefilter,
            '',
            undefined,
            undefined,
            undefined
        ).subscribe(result => {
            debugger;
            if (result) {
                this.open = parseInt(result.open);
                this.pending = parseInt(result.pending);
                this.assign = parseInt(result.assign);
                this.resolved = parseInt(result.resolved);
                this.closed = parseInt(result.closed);
            }
        });
    }

    clear() {
        this.stateNameFilter = '';
        this.organizationUnit = 0;
        this.filterText = '';
        this.serviceCategory = '';
        this.adressFilter = '';
        this.stateNameFilter = '';
        this.servicestatusFilter = 0;
        this.servicepriority = 0;
        this.serviceSource = '';
        this.startDate =  moment(this.date);
        this.endDate =  moment(this.date);
        // this.sampleDateRange = [moment().add(-7, 'days').endOf('day'), moment().add(0, 'days').startOf('day')];
        this.getServices();
    }

    expandGrid() {
        this.ExpandedView = true;
    }

    navigateToLeadDetail(leadid, service): void {
        this.ExpandedView = !this.ExpandedView;
        this.ExpandedView = false;
        this.SelectedLeadId = leadid;
        this.viewLeadDetail.showDetail(leadid, null, 22, service,'My Service');
    }

    reloadPage($event): void {
        this.paginator.changePage(this.paginator.getPage());
    }


    isAllChecked() {
        if (this.tableRecords)
            return this.tableRecords.every(_ => _.isSelected);
    }
    checkAll(ev) {

        this.tableRecords.forEach(x => x.isSelected = ev.target.checked);
        this.oncheckboxCheck();
        /// this.totalcount = this.tableRecords.forEach(x => x.lead.isSelected = ev.target.checked);
    }

    oncheckboxCheck() {

        this.count = 0;
        this.tableRecords.forEach(item => {
            if (item.isSelected == true) {
                this.count = this.count + 1;
            }
        })
    }
    submit(): void {
        debugger;
        let selectedids = [];
        // this.saving = true;
        this.primengTableHelper.records.forEach(function (job) {
            if (job.isSelected) {
                selectedids.push(job.service.id);
            }
        });
        if (selectedids.length == 0) {
            this.notify.warn(this.l('PleaseSelectRecord'));
            return;
        }
        if (this.assignservice == 0) {
            this.notify.warn(this.l('PleaseSelectUser'));
            return;
        }
        this._servicesServiceProxy.updateAssignUserID(this.assignservice, selectedids)
            .pipe(finalize(() => { }))
            .subscribe(() => {
                let log = new UserActivityLogDto();
                log.actionId = 27;
                log.actionNote ='Job Assigned' ;
                log.section = 'My Service';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
                }); 
                this.reloadPage(null);
                this.notify.info(this.l('AssignedSuccessfully'));
            });
    }

    createService(): void {
        this.createOrEditMyServiceOptionModal.show(0, this.organizationUnit,22,'My Service');
    }

    delete(id): void {
        this.message.confirm('',
            this.l('AreYouSureYouWantToDeleteExistingService'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._servicesServiceProxy.deleteDuplicateServices(id)
                        .subscribe(() => {
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Service Deleted';
                            log.section = 'My Service';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            }); 
                            this.reloadPage(null);
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'My Service';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'My Service';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }

}