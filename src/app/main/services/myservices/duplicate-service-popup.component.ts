import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetDuplicateLeadPopupDto, GetDuplicateServicePopupDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'duplicateservicepopupModal',
    templateUrl: './duplicate-service-popup.component.html'
})
export class ViewDuplicateServicePopUpModalComponent extends AppComponentBase {

    @ViewChild('duplicateLeadPOPModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    result: GetDuplicateServicePopupDto[];

    constructor(
        injector: Injector
    ) {
        super(injector);
    }

    show(item: GetDuplicateServicePopupDto[]): void {       
        this.result = item;
        this.modal.show();
    }

    close(): void {
        this.modal.hide();
    }
}
