﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { FinanceOptionsServiceProxy, CreateOrEditFinanceOptionDto, CommonLookupServiceProxy, ServicesServiceProxy, CreateOrEditServiceDto, CommonLookupDto, JobInstallerInvoicesServiceProxy, LeadsServiceProxy, GetDuplicateServicePopupDto, LeadUsersLookupTableDto, UserActivityLogServiceProxy, UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { ViewDuplicateServicePopUpModalComponent } from './duplicate-service-popup.component';

@Component({
    selector: 'createOrEditMyServiceOptionModal',
    templateUrl: './create-or-edit-myservice-modal.component.html'
})
export class CreateOrEditMyServiceOptionModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @ViewChild('duplicateservicepopupModal', { static: true }) duplicateservicepopupModal: ViewDuplicateServicePopUpModalComponent;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    allCategory: CommonLookupDto[];
    allSubCategory: CommonLookupDto[];
    allpriority: CommonLookupDto[];
    allSource: CommonLookupDto[];
    allStatus: CommonLookupDto[];
    allStates: CommonLookupDto[];
    service: CreateOrEditServiceDto = new CreateOrEditServiceDto();
    filteredLeadSource: any[];
    filteredinstaller: string[];
    role: string = '';
    item: GetDuplicateServicePopupDto[];
    allServiceManagerUsers: LeadUsersLookupTableDto[];
    sectionId = 0;

    constructor(
        injector: Injector,
        private _servicesServiceProxy: ServicesServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _jobInstallerInvoiceServiceProxy: JobInstallerInvoicesServiceProxy
    ) {
        super(injector);
        this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
            this.role = result;
        });
    }

  SectionName = '';
  show(serviceid?: number, orgId?: number, sectionId? : number, section = ''): void {
        debugger;
        this.SectionName = section;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote = serviceid > 0 ? "Open For " + this.l("EditService") : "Open For " + this.l("CreateNewService");
        log.section = section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
        this.sectionId = sectionId;
        if (this.allCategory == null) {
            this._commonLookupService.getServieCategoryDropdown().subscribe(result => {
                this.allCategory = result;
            });
        }
        if (this.allpriority == null) {
            this._commonLookupService.getServiePriorityDropdown().subscribe(result => {
                this.allpriority = result;
            });
        }
        if (this.allStatus == null) {
            this._commonLookupService.getServieStatusDropdown().subscribe(result => {
                this.allStatus = result;
            });
        }
        if (this.allSource == null) {
            this._commonLookupService.getServieSourceDropdown().subscribe(result => {
                this.allSource = result;
            });
        }
        if (this.allStates == null) {
            this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
                this.allStates = result;
            });
        }
        if (!serviceid) {
            
            this._leadsServiceProxy.getCurrentUserId().subscribe(result1 => {
                this._commonLookupService.getServiceUserForFilter(orgId).subscribe(result => {
                    this.allServiceManagerUsers = result;
                    this.service.serviceAssignId = result1;
                });
            });
            this.service = new CreateOrEditServiceDto();
            this.service.id = serviceid;
            this.service.isExternalLead = 2;
            this.service.organizationId = orgId;
            this.active = true;
            this.modal.show();
        } 
        else {
            this._servicesServiceProxy.getserviceForEdit(serviceid).subscribe(result => {
                // this._commonLookupService.getServiceUserForFilter(orgId).subscribe(results => {
                //     this.allServiceManagerUsers = results;
                //     //this.service.serviceAssignId = result.service.serviceAssignId;
                // });
                this.service = result.service;

                this.active = true;
                this.modal.show();
                this.getSubcategory(this.service.serviceCategoryName)
            });
        }
    }
    onShown(): void {

        document.getElementById('service_Name').focus();
    }
    getSubcategory(name) {
        this._commonLookupService.getServieSubCategoryDropdown(name).subscribe(result => {
            this.allSubCategory = result;
        });
    }

    OnSelectDetails(event): void {
    
        this.service.jobId = event.jobId;
        this.service.jobNumber = event.jobNumber;
        this.service.name = event.customerName;
        this.service.mobile = event.mobile;
        this.service.email = event.email;
        this.service.address = event.address;
        this.service.state = event.state;
    }

    filterjobnumber(event): void {

        this._jobInstallerInvoiceServiceProxy.getAllJobNumberForMyService(event.query).subscribe(result => {
            this.filteredLeadSource = result;

        });
    }

    save(): void {
        this.saving = true;
        var id = this.service.id != null ? this.service.id : 0;
        this._servicesServiceProxy.checkExistServiceList(this.service)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(result => {
                this.item = result;
                if (result.length > 0 && id == 0 ) {
                    if (this.role == 'Admin' || this.role == 'Manager Srrvice') {
                        this.message.confirm('Do You Still Want to Proceed',
                            "Looks Like Duplicate Record",
                            (isConfirmed) => {
                                if (isConfirmed) {
                                    this._servicesServiceProxy.createOrEditService(this.sectionId,this.SectionName,this.service)
                                        .pipe(finalize(() => { this.saving = false; }))
                                        .subscribe(() => {
                                            this.notify.info(this.l('SavedSuccessfully'));
                                            this.close();
                                            this.modalSave.emit(null);
                                        });
                                } else {
                                    this.saving = false;
                                }
                            }
                        );
                    }
                    else {
                        let element: HTMLElement = document.getElementById('auto_trigger') as HTMLElement;
                        element.click();
                        this.saving = false;
                    }
                }
                else {
                    this._servicesServiceProxy.createOrEditService(this.sectionId,this.SectionName,this.service)
                        .pipe(finalize(() => { this.saving = false; }))
                        .subscribe(() => {
                            this.notify.info(this.l('SavedSuccessfully'));
                            this.close();
                            this.modalSave.emit(null);
                        });

                }
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
