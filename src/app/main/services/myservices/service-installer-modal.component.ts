﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { FinanceOptionsServiceProxy, CreateOrEditFinanceOptionDto, CommonLookupServiceProxy, ServicesServiceProxy, CreateOrEditServiceDto, CommonLookupDto, JobInstallerInvoicesServiceProxy, LeadsServiceProxy, GetDuplicateServicePopupDto, LeadUsersLookupTableDto, InstallerServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { ViewDuplicateServicePopUpModalComponent } from './duplicate-service-popup.component';

@Component({
    selector: 'addserviceinstallerModal',
    templateUrl: './service-installer-modal.component.html'
})
export class ServiceInstallerModalComponent extends AppComponentBase {
   
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    InstallerList: CommonLookupDto[];
    active = false;
    saving = false;
    service: CreateOrEditServiceDto = new CreateOrEditServiceDto();
    sectionId = 0;
    constructor(
        injector: Injector,
        private _servicesServiceProxy: ServicesServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _installerServiceProxy: InstallerServiceProxy,
    ) {
        super(injector);
    }

  SectionName = '';
  show(serviceid?: number, orgId?: number,sectionId? : number, section = ''): void {
        debugger;
        this.SectionName = section;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote = "Open For Add Installer";
        log.section = section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
        this.sectionId = sectionId ;
        if (this.InstallerList == null) {
            this._installerServiceProxy.getAllInstallers(orgId).subscribe(result => {
                this.InstallerList = result;
            });
        }
        this._servicesServiceProxy.getserviceForEdit(serviceid).subscribe(result => {
            this.service = result.service;
            this.active = true;
            this.modal.show();
        });

    }
    onShown(): void {

        document.getElementById('service_Name').focus();
    }

    save(): void {
        this.saving = true;
        this._servicesServiceProxy.createOrEditService(this.sectionId, this.SectionName,this.service)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
