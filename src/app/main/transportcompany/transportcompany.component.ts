import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { ActivatedRoute , Router} from '@angular/router';
import { TransportCompaniesServiceProxy, TransportCompaniesDto  } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { CreateOrEditTransportCompanyModalComponent } from '../transportcompany/create-or-edit-transportcompany-modal.component';
import { ViewDepartmentModalComponent } from '../departments/departments/view-department-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { Title } from '@angular/platform-browser';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
@Component({
    templateUrl: './transportcompany.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class TransportcompanyComponent extends AppComponentBase {
    @ViewChild('CreateOrEditTransportCompanyModalComponent', { static: true }) CreateOrEditTransportCompanyModalComponent: CreateOrEditTransportCompanyModalComponent;
    @ViewChild('viewDepartmentModalComponent', { static: true }) viewDepartmentModal: ViewDepartmentModalComponent;   
    
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    public screenHeight: any;  
    testHeight = 250;
    constructor(
        injector: Injector,
        private _TransportCompaniesServiceProxy: TransportCompaniesServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private titleService: Title
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Transport Company");
        
    }

    getAll(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        

        this.primengTableHelper.showLoadingIndicator();

        this. _TransportCompaniesServiceProxy.getAll(
            this.filterText,
            this.primengTableHelper.getSorting(this.dataTable),
           
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            //result.items[0].transportCompanies.companyName,
            this.primengTableHelper.hideLoadingIndicator();
        }), err => { this.primengTableHelper.hideLoadingIndicator(); };
    }
    searchLog() : void {
        
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Inventory Transport Company';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
       
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Inventory Transport Company';
        log.section = 'Inventory Transport Company';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }


    createTransportcompany(): void {
        this.CreateOrEditTransportCompanyModalComponent.show();        
    }


    delete(transportcompany: TransportCompaniesDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this. _TransportCompaniesServiceProxy.delete(transportcompany.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete Inventory Transport Company: ' + transportcompany.companyName;
                            log.section = 'Inventory Transport Company';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });

                        });
                }
            }
        );
    }

      exportToExcel(): void {
         this._TransportCompaniesServiceProxy.getTransportCompanyToExcel(
          this.filterText,
         )
          .subscribe(result => {
             this._fileDownloadService.downloadTempFile(result);
           });
      }
}
