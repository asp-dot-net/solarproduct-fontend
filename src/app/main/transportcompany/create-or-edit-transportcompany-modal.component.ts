import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { TransportCompaniesServiceProxy, CreateOrEditTransportCompaniesDto} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
@Component({
    selector: 'createOrEditTransportCompanyModal',
    templateUrl: './create-or-edit-transportcompany-modal.component.html'
})
export class CreateOrEditTransportCompanyModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    transportcompany: CreateOrEditTransportCompaniesDto = new CreateOrEditTransportCompaniesDto();

    constructor(
        injector: Injector,
        private _TransportCompaniesServiceProxy: TransportCompaniesServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }

    show(transportcompanyid?: number): void {

        if (!transportcompanyid) {
            this.transportcompany = new CreateOrEditTransportCompaniesDto();
            this.transportcompany.id = transportcompanyid;

            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Inventory Transport Company';
            log.section = 'Inventory Transport Company';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._TransportCompaniesServiceProxy.getTransportCompaniesForEdit(transportcompanyid).subscribe(result => {
                this.transportcompany = result.transportcompanies;

                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Inventory Transport Company : ' + this.transportcompany.companyName;
                log.section = 'Inventory Transport Company';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        
    }
    save(): void {
            this.saving = true;
			
            this._TransportCompaniesServiceProxy.createOrEdit(this.transportcompany)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.transportcompany.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Inventory Transport Company Updated : '+ this.transportcompany.companyName;
                    log.section = 'Inventory Transport Company';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Inventory Transport Company Created : '+ this.transportcompany.companyName;
                    log.section = 'Inventory Transport Company';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
