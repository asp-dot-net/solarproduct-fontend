"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.LeadTrackerComponent = void 0;
var core_1 = require("@angular/core");
var service_proxies_1 = require("@shared/service-proxies/service-proxies");
var app_component_base_1 = require("@shared/common/app-component-base");
var routerTransition_1 = require("@shared/animations/routerTransition");
var moment = require("moment");
var AppConsts_1 = require("@shared/AppConsts");
var operators_1 = require("rxjs/operators");
var LeadTrackerComponent = /** @class */ (function (_super) {
    __extends(LeadTrackerComponent, _super);
    function LeadTrackerComponent(injector, _leadsServiceProxy, _notifyService, _tokenAuth, _activatedRoute, _httpClient, _userServiceProxy, _fileDownloadService, _router) {
        var _this = _super.call(this, injector) || this;
        _this._leadsServiceProxy = _leadsServiceProxy;
        _this._notifyService = _notifyService;
        _this._tokenAuth = _tokenAuth;
        _this._activatedRoute = _activatedRoute;
        _this._httpClient = _httpClient;
        _this._userServiceProxy = _userServiceProxy;
        _this._fileDownloadService = _fileDownloadService;
        _this._router = _router;
        _this.show = true;
        _this.showchild = true;
        _this.shouldShow = false;
        _this.flag = true;
        _this.organizationUnitlength = 0;
        _this.SelectedLeadId = 0;
        _this.reloadLead = new core_1.EventEmitter();
        _this.advancedFiltersAreShown = false;
        _this.filterText = '';
        _this.copanyNameFilter = '';
        _this.emailFilter = '';
        _this.phoneFilter = '';
        _this.mobileFilter = '';
        _this.addressFilter = '';
        _this.requirementsFilter = '';
        _this.postCodeSuburbFilter = '';
        _this.stateNameFilter = '';
        _this.streetNameFilter = '';
        _this.postCodeFilter = '';
        _this.leadSourceNameFilter = '';
        _this.leadSourceIdFilter = [];
        _this.showJobStatus = false;
        //leadSubSourceNameFilter = '';
        _this.leadStatusName = '';
        _this.typeNameFilter = '';
        _this.areaNameFilter = '';
        _this.jobStatusIDFilter = 0;
        _this.dateFilterType = 'Assign';
        _this.leadStatusId = 0;
        _this.ExpandedView = true;
        _this.flafValue = true;
        _this.date = '01/01/2021';
        _this.sampleDateRange = [moment(_this.date), moment().endOf('day')];
        _this.assignLead = 0;
        _this.assignLead1 = 0;
        _this.saving = false;
        _this.totalLeads = 0;
        _this["new"] = 0;
        _this.unHandledLeads = 0;
        _this.upgrade = 0;
        _this.canceled = 0;
        _this.cold = 0;
        _this.warm = 0;
        _this.hot = 0;
        _this.closed = 0;
        _this.assigned = 0;
        _this.reAssigned = 0;
        _this.rejected = 0;
        _this.showOrganizationUnits = false;
        _this.organizationUnit = 0;
        _this.organizationChangeUnit = 0;
        _this.isChange = false;
        _this.isAssign = false;
        _this.teamId = 0;
        _this.salesManagerId = 0;
        _this.salesRepId = 0;
        _this.role = '';
        _this.currentUserId = 0;
        _this.projectNumberFilter = '';
        _this.totalcount = 0;
        _this.count = 0;
        _this.cancelrequestfilter = 0;
        _this.jobStatusID = [];
        _this.uploadUrl = AppConsts_1.AppConsts.remoteServiceBaseUrl + '/Users/ImportLeadFromExcel';
        return _this;
    }
    LeadTrackerComponent.prototype.toggleBlock = function () {
        this.show = !this.show;
    };
    ;
    LeadTrackerComponent.prototype.toggleBlockChild = function () {
        this.showchild = !this.showchild;
    };
    ;
    LeadTrackerComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._commonLookupService.getAllJobStatusTableDropdown().subscribe(function (result) {
            _this.alljobstatus = result;
        });
        this._leadsServiceProxy.getCurrentUserRole().subscribe(function (result) {
            _this.role = result;
            if (_this.role == 'Sales Rep') {
                _this.leadStatusId = 10;
            }
        });
        this._commonLookupService.getAllLeadSourceForTableDropdown().subscribe(function (result) {
            _this.allLeadSources = result;
        });
        // this._leadsServiceProxy.getAllTeamsForFilter().subscribe(teams => {
        //     this.filteredTeams = teams;
        // });
        this._commonLookupService.getAllStateForTableDropdown().subscribe(function (result) {
            _this.allStates = result;
        });
        this._commonLookupService.getTeamForFilter().subscribe(function (teams) {
            _this.filteredTeams = teams;
        });
        this._commonLookupService.getOrganizationUnit().subscribe(function (output) {
            _this.allOrganizationUnits = output;
            _this.organizationUnit = _this.allOrganizationUnits[0].id;
            _this.organizationUnitlength = _this.allOrganizationUnits.length;
            _this.allOrganizationUnitsList = _this.allOrganizationUnits.filter(function (item) { return item.id != _this.organizationUnit; });
            _this._leadsServiceProxy.getSalesManagerUser(_this.organizationUnit).subscribe(function (result) {
                _this.allUsers = result;
            });
            _this._commonLookupService.getSalesManagerForFilter(_this.organizationUnit, undefined).subscribe(function (manager) {
                _this.filteredManagers = manager;
            });
            _this._leadsServiceProxy.getSalesRepForFilter(_this.organizationUnit, undefined).subscribe(function (rep) {
                _this.filteredReps = rep;
            });
            _this._leadsServiceProxy.getCurrentUserId().subscribe(function (result) {
                ;
                if (_this.role == 'Sales Manager') {
                    _this.currentUserId = result;
                    _this.salesManagerId = _this.currentUserId;
                }
                _this.getLeads();
            });
        });
    };
    LeadTrackerComponent.prototype.expandGrid = function () {
        this.ExpandedView = true;
    };
    LeadTrackerComponent.prototype.clear = function () {
        this.filterText = '';
        this.postCodeSuburbFilter = '';
        this.stateNameFilter = '';
        this.postCodeFilter = '';
        this.typeNameFilter = '';
        this.areaNameFilter = '';
        this.addressFilter = '';
        this.leadSourceNameFilter = '';
        this.leadSourceIdFilter = [];
        this.jobStatusIDFilter = 0;
        this.teamId = 0;
        this.onlyAssignLead = false;
        this.salesRepId = 0;
        this.projectNumberFilter = '';
        this.cancelrequestfilter = 0;
        this.sampleDateRange = [moment(this.date), moment().endOf('day')];
        this.salesManagerId = this.currentUserId;
        this.getLeads();
    };
    LeadTrackerComponent.prototype.oncheck = function (event) {
        if (event.target.checked == true) {
            if (this.allOrganizationUnitsList.length > 0) {
                this.isChange == true;
                this.showOrganizationUnits = true;
            }
            else {
                this.isChange == false;
                this.showOrganizationUnits = false;
                this.notify.warn(this.l('NoOrganizationAvailable'));
            }
        }
        else {
            this.isChange == false;
            this.showOrganizationUnits = false;
        }
    };
    LeadTrackerComponent.prototype.oncheckboxCheck = function () {
        var _this = this;
        this.count = 0;
        this.tableRecords.forEach(function (item) {
            if (item.lead.isSelected == true) {
                _this.count = _this.count + 1;
            }
        });
    };
    LeadTrackerComponent.prototype.filtersuburbs = function (event) {
        var _this = this;
        this._commonLookupService.getOnlySuburb(event.query).subscribe(function (output) {
            _this.suburbSuggestions = output;
            console.log(_this.suburbSuggestions);
        });
    };
    LeadTrackerComponent.prototype.getOrganizationWiseUser = function () {
        var _this = this;
        this._leadsServiceProxy.getSalesManagerUser(this.organizationChangeUnit).subscribe(function (result) {
            _this.allUsers = result;
        });
    };
    LeadTrackerComponent.prototype.getsalerep = function (event) {
        var _this = this;
        var id = event.target.value;
        if (id != 0) {
            if (this.organizationChangeUnit != 0) {
                this._leadsServiceProxy.getSalesRepBySalesManagerid(id, this.organizationChangeUnit).subscribe(function (result) {
                    _this.allSalesrepUsers = result;
                });
            }
            else {
                this._leadsServiceProxy.getSalesRepBySalesManagerid(id, this.organizationUnit).subscribe(function (result) {
                    _this.allSalesrepUsers = result;
                });
            }
        }
        this.assignLead1 = 0;
    };
    LeadTrackerComponent.prototype.checkAll = function (ev) {
        this.tableRecords.forEach(function (x) { return x.lead.isSelected = ev.target.checked; });
        this.oncheckboxCheck();
        /// this.totalcount = this.tableRecords.forEach(x => x.lead.isSelected = ev.target.checked);
    };
    LeadTrackerComponent.prototype.isAllChecked = function () {
        if (this.tableRecords)
            return this.tableRecords.every(function (_) { return _.lead.isSelected; });
    };
    LeadTrackerComponent.prototype.getLeads = function (event) {
        var _this = this;
        this.allOrganizationUnitsList = this.allOrganizationUnits.filter(function (item) { return item.id != _this.organizationUnit; });
        this.PageNo = this.primengTableHelper.getSkipCount(this.paginator, event);
        if (this.sampleDateRange != null) {
            this.StartDate = this.sampleDateRange[0];
            this.EndDate = this.sampleDateRange[1];
        }
        else {
            this.StartDate = null;
            this.EndDate = null;
        }
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();
        if (Number(this.leadStatusId) === 6) {
            this.showJobStatus = true;
        }
        else {
            this.showJobStatus = false;
        }
        this._leadsServiceProxy.getLeadTrackerData(this.filterText, this.copanyNameFilter, this.emailFilter, this.phoneFilter, this.mobileFilter, this.addressFilter, this.requirementsFilter, this.postCodeSuburbFilter, this.stateNameFilter, this.streetNameFilter, this.postCodeFilter, this.leadSourceNameFilter, this.leadSourceIdFilter, this.leadStatusName, this.typeNameFilter, this.areaNameFilter, (this.leadStatusId == 0) ? undefined : this.leadStatusId, 0, this.StartDate, this.EndDate, this.organizationUnit, this.teamId == 0 ? undefined : this.teamId, this.salesManagerId == 0 ? undefined : this.salesManagerId, this.salesRepId == 0 ? undefined : this.salesRepId, this.dateFilterType, undefined, this.onlyAssignLead, this.leadStatusId == 6 ? this.jobStatusIDFilter == 0 ? undefined : this.jobStatusIDFilter : undefined, undefined, undefined, this.projectNumberFilter, this.cancelrequestfilter, this.jobStatusID, this.primengTableHelper.getSorting(this.dataTable), this.primengTableHelper.getSkipCount(this.paginator, event), this.primengTableHelper.getMaxResultCount(this.paginator, event)).subscribe(function (result) {
            _this.primengTableHelper.totalRecordsCount = result.totalCount;
            _this.primengTableHelper.records = result.items;
            _this.tableRecords = result.items;
            _this.primengTableHelper.hideLoadingIndicator();
            _this.shouldShow = false;
            if (result.totalCount > 0) {
                _this.totalLeads = parseInt(result.items[0].total);
                _this["new"] = parseInt(result.items[0]["new"]);
                _this.unHandledLeads = parseInt(result.items[0].unHandled);
                _this.cold = parseInt(result.items[0].cold);
                _this.warm = parseInt(result.items[0].warm);
                _this.hot = parseInt(result.items[0].hot);
                _this.upgrade = parseInt(result.items[0].upgrade);
                _this.rejected = parseInt(result.items[0].rejected);
                _this.canceled = parseInt(result.items[0].cancelled);
                _this.closed = parseInt(result.items[0].closed);
                _this.assigned = parseInt(result.items[0].assigned);
                _this.reAssigned = parseInt(result.items[0].reAssigned);
            }
            else {
                _this.totalLeads = 0;
                _this["new"] = 0;
                _this.unHandledLeads = 0;
                _this.cold = 0;
                _this.warm = 0;
                _this.hot = 0;
                _this.upgrade = 0;
                _this.rejected = 0;
                _this.canceled = 0;
                _this.closed = 0;
                _this.assigned = 0;
                _this.reAssigned = 0;
            }
            if (_this.flag == false && result.totalCount != 0 && _this.ExpandedView == false) {
                _this.navigateToLeadDetail(_this.SelectedLeadId);
            }
            else {
                if (_this.ExpandedView == false && result.totalCount != 0) {
                    _this.navigateToLeadDetail(result.items[0].lead.id);
                }
                else {
                    _this.ExpandedView = true;
                }
            }
        });
    };
    LeadTrackerComponent.prototype.filterCountries = function (event) {
        var _this = this;
        this._commonLookupService.getAllLeadStatusForTableDropdown(event.query).subscribe(function (result) {
            _this.allLeadStatus = result;
        });
    };
    LeadTrackerComponent.prototype.reloadPage = function (flafValue) {
        ;
        this.ExpandedView = true;
        this.flag = flafValue;
        this.paginator.changePage(this.paginator.getPage());
        if (!flafValue) {
            this.navigateToLeadDetail(this.SelectedLeadId);
        }
    };
    LeadTrackerComponent.prototype.createLead = function () {
        this._router.navigate(['/app/main/leads/leads/createOrEdit'], { queryParams: { from: "mylead" } });
    };
    LeadTrackerComponent.prototype.navigateToLeadDetail = function (leadid) {
        this.ExpandedView = !this.ExpandedView;
        this.ExpandedView = false;
        this.SelectedLeadId = leadid;
        //this.viewLead.showDetail(leadid);
        this.leadName = "leadTracker";
        this.viewLeadDetail.showDetail(leadid, this.leadName);
    };
    LeadTrackerComponent.prototype.deleteLead = function (lead) {
        this.message.confirm('', this.l('AreYouSure'), function (isConfirmed) {
            if (isConfirmed) {
                // this._leadsServiceProxy.delete(lead.id)
                //     .subscribe(() => {
                //         this.reloadPage();
                //         this.notify.success(this.l('SuccessfullyDeleted'));
                //     });
            }
        });
    };
    LeadTrackerComponent.prototype.exportToExcel = function () {
        var _this = this;
        if (this.sampleDateRange != null) {
            this.StartDate = this.sampleDateRange[0];
            this.EndDate = this.sampleDateRange[1];
        }
        else {
            this.StartDate = null;
            this.EndDate = null;
        }
        this._leadsServiceProxy.getLeadTrackerToExcel(this.filterText, this.copanyNameFilter, this.emailFilter, this.phoneFilter, this.mobileFilter, this.addressFilter, this.requirementsFilter, this.postCodeSuburbFilter, this.stateNameFilter, this.streetNameFilter, this.postCodeFilter, this.leadSourceNameFilter, this.leadSourceIdFilter, this.leadStatusName, this.typeNameFilter, this.areaNameFilter, (this.leadStatusId == 0) ? undefined : this.leadStatusId, 0, this.StartDate, this.EndDate, this.organizationUnit, this.teamId == 0 ? undefined : this.teamId, this.salesManagerId == 0 ? undefined : this.salesManagerId, this.salesRepId == 0 ? undefined : this.salesRepId, this.dateFilterType, undefined, this.onlyAssignLead, this.leadStatusId == 6 ? this.jobStatusIDFilter == 0 ? undefined : this.jobStatusIDFilter : undefined, undefined, undefined)
            .subscribe(function (result) {
            _this._fileDownloadService.downloadTempFile(result);
        });
    };
    LeadTrackerComponent.prototype.uploadExcel = function (data) {
        var _this = this;
        var formData = new FormData();
        var file = data.files[0];
        formData.append('file', file, file.name);
        this._httpClient
            .post(this.uploadUrl, formData)
            .pipe(operators_1.finalize(function () { return _this.excelFileUpload.clear(); }))
            .subscribe(function (response) {
            if (response.success) {
                _this.notify.success(_this.l('ImportLeadsProcessStart'));
            }
            else if (response.error != null) {
                _this.notify.error(_this.l('ImportLeadUploadFailed'));
            }
        });
    };
    LeadTrackerComponent.prototype.onUploadExcelError = function () {
        this.notify.error(this.l('ImportLeadUploadFailed'));
    };
    LeadTrackerComponent.prototype.submit = function () {
        var _this = this;
        ;
        var selectedids = [];
        this.saving = true;
        if (this.isChange) {
            this.primengTableHelper.records.forEach(function (lead) {
                if (lead.lead.isSelected) {
                    selectedids.push(lead.lead.id);
                }
            });
            var assignleads = new service_proxies_1.GetLeadForAssignOrTransferOutput();
            assignleads.assignToUserID = this.assignLead;
            assignleads.leadIds = selectedids;
            assignleads.organizationID = this.organizationChangeUnit;
            if (selectedids.length == 0) {
                this.notify.warn(this.l('NoLeadsSelected'));
                this.saving = false;
                return;
            }
            if (this.assignLead == 0) {
                this.notify.warn(this.l('PleaseSelectUser'));
                this.saving = false;
                return;
            }
            if (this.isAssign) {
                if (this.assignLead1 == 0) {
                    this.notify.warn(this.l('PleaseSelctSalesRep'));
                    this.saving = false;
                    return;
                }
                else {
                    assignleads.assignToUserID = this.assignLead1;
                }
            }
            this._leadsServiceProxy.transferLeadToOtherOrganization(assignleads)
                .pipe(operators_1.finalize(function () { _this.saving = false; }))
                .subscribe(function () {
                _this.reloadPage(true);
                _this.isChange = false;
                _this.saving = false;
                _this.notify.info(_this.l('AssignedSuccessfully'));
            });
        }
        else {
            this.primengTableHelper.records.forEach(function (lead) {
                if (lead.lead.isSelected) {
                    selectedids.push(lead.lead.id);
                }
            });
            var assignleads = new service_proxies_1.GetLeadForAssignOrTransferOutput();
            assignleads.assignToUserID = this.assignLead;
            assignleads.leadIds = selectedids;
            if (selectedids.length == 0) {
                this.notify.warn(this.l('NoLeadsSelected'));
                this.saving = false;
                return;
            }
            if (this.assignLead == 0) {
                this.notify.warn(this.l('PleaseSelectUser'));
                this.saving = false;
                return;
            }
            if (this.isAssign) {
                if (this.assignLead1 == 0) {
                    this.notify.warn(this.l('PleaseSelctSalesRep'));
                    this.saving = false;
                    return;
                }
                else {
                    assignleads.assignToUserID = this.assignLead1;
                }
            }
            this._leadsServiceProxy.assignOrTransferLead(assignleads)
                .pipe(operators_1.finalize(function () { _this.saving = false; }))
                .subscribe(function () {
                _this.reloadPage(true);
                _this.saving = false;
                _this.notify.info(_this.l('AssignedSuccessfully'));
            });
        }
    };
    LeadTrackerComponent.prototype.assignIndividualLead = function (item) {
        var _this = this;
        this.saving = true;
        var selectedids = [];
        selectedids.push(item.id);
        var assignleads = new service_proxies_1.GetLeadForAssignOrTransferOutput();
        assignleads.assignToUserID = item.assignToUserID;
        assignleads.leadIds = selectedids;
        this._leadsServiceProxy.assignOrTransferLead(assignleads)
            .pipe(operators_1.finalize(function () { _this.saving = false; }))
            .subscribe(function () {
            _this.reloadPage(true);
            _this.saving = false;
            _this.notify.info(_this.l('AssignedSuccessfully'));
        });
    };
    __decorate([
        core_1.ViewChild('ExcelFileUpload', { static: false })
    ], LeadTrackerComponent.prototype, "excelFileUpload");
    __decorate([
        core_1.ViewChild('dataTable', { static: true })
    ], LeadTrackerComponent.prototype, "dataTable");
    __decorate([
        core_1.ViewChild('paginator', { static: true })
    ], LeadTrackerComponent.prototype, "paginator");
    __decorate([
        core_1.ViewChild('viewLeadDetail', { static: true })
    ], LeadTrackerComponent.prototype, "viewLeadDetail");
    __decorate([
        core_1.ViewChild('addActivityModal', { static: true })
    ], LeadTrackerComponent.prototype, "addActivityModal");
    __decorate([
        core_1.Input()
    ], LeadTrackerComponent.prototype, "SelectedLeadId");
    __decorate([
        core_1.Output()
    ], LeadTrackerComponent.prototype, "reloadLead");
    LeadTrackerComponent = __decorate([
        core_1.Component({
            templateUrl: './lead-tracker.component.html',
            styleUrls: ['./leads.component.less'],
            encapsulation: core_1.ViewEncapsulation.None,
            animations: [routerTransition_1.appModuleAnimation()]
        })
    ], LeadTrackerComponent);
    return LeadTrackerComponent;
}(app_component_base_1.AppComponentBase));
exports.LeadTrackerComponent = LeadTrackerComponent;
