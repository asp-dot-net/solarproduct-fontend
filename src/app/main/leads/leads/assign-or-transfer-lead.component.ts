import { Component,  Injector, OnInit} from '@angular/core';
import { LeadsServiceProxy, GetLeadForAssignOrTransferOutput 
    ,LeadUsersLookupTableDto
					} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { finalize } from 'rxjs/operators';
import { FormsModule } from '@angular/forms';


@Component({
    selector: 'assignOrTransferLeadModel',
    templateUrl: './assign-or-transfer-lead.component.html',
    animations: [appModuleAnimation()]
})
export class AssignOrTransferLeadComponent extends AppComponentBase implements OnInit {

    lead:GetLeadForAssignOrTransferOutput = new GetLeadForAssignOrTransferOutput();
    allUsers:LeadUsersLookupTableDto[];

    saving: boolean;
					
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,        
        private _leadsServiceProxy: LeadsServiceProxy
    ) {
        super(injector);
    }

     ngOnInit(): void {
         this.show(this._activatedRoute.snapshot.queryParams['id']);
     }

    show(leadId?: number,sectionId? : number): void {        
        this._leadsServiceProxy.getLeadForAssignOrTransfer(leadId).subscribe(result => {
            this.lead = result;
            this.lead.sectionId =sectionId;
        });
        // this._leadsServiceProxy.getAllUserLeadAssignDropdown().subscribe(result => {						
        //     this.allUsers = result;
        // });
    }

    save(): void {
        this.saving = true;
       
        this._leadsServiceProxy.assignOrTransferLead(this.lead)
         .pipe(finalize(() => { this.saving = false;}))
         .subscribe(() => {
            this.notify.info(this.l('SavedSuccessfully'));
         });
}
}
