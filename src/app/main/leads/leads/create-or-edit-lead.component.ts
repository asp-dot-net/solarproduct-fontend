﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit, ElementRef, Directive } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {LeadUsersLookupTableDto,
    LeadsServiceProxy, CreateOrEditLeadDto
    , LeadStateLookupTableDto
    , LeadSourceLookupTableDto
    , LeadStatusLookupTableDto,
    CheckExistLeadDto,
    GetDuplicateLeadPopupDto,
    CommonLookupServiceProxy,
    PostCodeRangeServiceProxy,
    JobInstallerInvoicesServiceProxy,
    UserActivityLogDto,
    UserActivityLogServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AgmCoreModule } from '@agm/core';
import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';
import PlaceResult = google.maps.places.PlaceResult;
import { Appearance, GermanAddress, Location } from '@angular-material-extensions/google-maps-autocomplete';
import { ViewDuplicatePopUpModalComponent } from './duplicate-lead-popup.component';


@Component({
    selector: 'createOrEditLead',
    templateUrl: './create-or-edit-lead.component.html',
    styleUrls: ['./create-or-edit-lead.component.less'],
    animations: [appModuleAnimation()]
})
export class CreateOrEditLeadComponent extends AppComponentBase implements OnInit {
    @ViewChild('duplicatepopupModal', { static: true }) duplicatepopupModal: ViewDuplicatePopUpModalComponent;

    active = false;
    saving = false;
    lead: CreateOrEditLeadDto = new CreateOrEditLeadDto();
    postCodeSuburb = '';
    stateName = '';
    leadSourceName = '';
    LeadStatusName = '';
    latitude = '';
    longitude = '';
    leadStatus: any;
    allStates: LeadStateLookupTableDto[];
    allLeadSources: LeadSourceLookupTableDto[];
    leadSources: LeadSourceLookupTableDto[];
    allLeadStatus: LeadStatusLookupTableDto[];
    ShowAddress: boolean = true;
    unitType: any;
    streetType: any;
    streetName: any;
    suburb: any;
    postalUnitType: any;
    postalStreetType: any;
    postalStreetName: any;
    postalSuburb: any;
    filteredunitTypes: LeadStatusLookupTableDto[];
    filteredstreettypes: LeadStatusLookupTableDto[];
    filteredstreetnames: LeadStatusLookupTableDto[];
    filteredsuburbs: LeadStatusLookupTableDto[];
    output: LeadStatusLookupTableDto[] = new Array<LeadStatusLookupTableDto>();
    unitTypesSuggestions: string[];
    streetTypesSuggestions: string[];
    streetNamesSuggestions: string[];
    suburbSuggestions: string[];
    postalunitTypesSuggestions: string[];
    postalstreetTypesSuggestions: string[];
    postalstreetNamesSuggestions: string[];
    postalsuburbSuggestions: string[];
    selectAddress: boolean = true;
    selectAddress2: boolean = true;
    from: string;
    item: GetDuplicateLeadPopupDto[];
    role: string = '';
    sectionId = 0;
    userList : LeadUsersLookupTableDto[];
    NoData = false;
    filterdata = false;
    section = '';
    organizationUnit = 0;
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _router: Router,
        private _el: ElementRef,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _postCodeRangeServiceProxy: PostCodeRangeServiceProxy,
        private _jobInstallerInvoiceServiceProxy: JobInstallerInvoicesServiceProxy
    ) {
        super(injector);
        this._el.nativeElement.setAttribute('autocomplete', 'off');
        this._el.nativeElement.setAttribute('autocorrect', 'off');
        this._el.nativeElement.setAttribute('autocapitalize', 'none');
        this._el.nativeElement.setAttribute('spellcheck', 'false');
    }

    ngOnInit(): void {

        this.show(this._activatedRoute.snapshot.queryParams['OId'], this._activatedRoute.snapshot.queryParams['id'],this._activatedRoute.snapshot.queryParams['SectionId'],this._activatedRoute.snapshot.queryParams['Section']);
        this.from = this._activatedRoute.snapshot.queryParams['from'];

        this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
            this.role = result;
        });
        
    }

    onShown(){
        
    }

    // get UnitType
    filterunitTypes(event): void {
        this._leadsServiceProxy.getAllUnitType(event.query).subscribe(output => {
            this.unitTypesSuggestions = output;
        });
    }

    // get StreetType
    filterstreettypes(event): void {
        this._leadsServiceProxy.getAllStreetType(event.query).subscribe(output => {
            this.streetTypesSuggestions = output;
        });
    }

    // get StreetName
    filterstreetnames(event): void {
        this._leadsServiceProxy.getAllStreetName(event.query).subscribe(output => {
            this.streetNamesSuggestions = output;
        });
    }

    // get Suburb
    filtersuburbs(event): void {
        this._leadsServiceProxy.getAllSuburb(event.query).subscribe(output => {
            this.suburbSuggestions = output;
        });
    }
    //get state & Postcode
    fillstatepostcode(): void {
        var splitted = this.suburb.split("||");
        this.lead.suburb = splitted[0];
        this.lead.state = splitted[1].trim();
        this.lead.postCode = splitted[2].trim();
        this.lead.country = "AUSTRALIA";
        // this._leadsServiceProxy.getareaBysuburbPostandstate(this.lead.suburb, this.lead.state, this.lead.postCode).subscribe(result => {
        //     if (result != null) {
        //         this.lead.area = result;
        //     }
        // });

        this.bindArea();
    }

    onAutocompleteSelected(result: PlaceResult) {
        debugger;
        if (result.address_components.length == 7) {
            this.lead.unitNo = "";
            this.unitType = "";
            this.lead.unitType = "";
            this.lead.streetNo = "";
            this.streetName = "";
            this.streetType = "";
            this.suburb = "";
            this.lead.state = "";
            this.lead.suburbId = 0;
            this.lead.stateId = 0;
            this.lead.postCode = "";
            this.lead.address = "";
            this.lead.country = "";

            this.lead.streetNo = result.address_components[0].long_name.toUpperCase();
            var str = result.address_components[1].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.streetType = splitted[3].toUpperCase().trim();
            }
            else if (splitted.length == 3) {
                this.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.streetType = splitted[2].toUpperCase().trim();
            }
            else {
                this.streetName = splitted[0].toUpperCase().trim();
                this.streetType = splitted[1].toUpperCase().trim();
            }
            this.suburb = result.address_components[2].long_name.toUpperCase().trim();
            this.lead.state = result.address_components[4].short_name;
            this._leadsServiceProxy.getSuburbId(result.address_components[6].long_name).subscribe(result => {
                this.lead.suburbId = result;
            });
            this._leadsServiceProxy.stateId(result.address_components[4].short_name).subscribe(result => {
                this.lead.stateId = result;
            });
            this.lead.postCode = result.address_components[6].long_name.toUpperCase();
            this.lead.address = this.lead.unitNo + " " + this.lead.unitType + " " + this.lead.streetNo + " " + this.streetName + " " + this.streetType;  //result.formatted_address;
            this.lead.country = result.address_components[5].long_name.toUpperCase();

            this._leadsServiceProxy.getareaBysuburbPostandstate(this.suburb, this.lead.state, this.lead.postCode).subscribe(result => {
                if (result != null) {
                    this.lead.area = result;
                }
            });
        }
        else if (result.address_components.length == 6) {
            this.lead.unitNo = "";
            this.unitType = "";
            this.lead.unitType = "";
            this.lead.streetNo = "";
            this.streetName = "";
            this.streetType = "";
            this.suburb = "";
            this.lead.state = "";
            this.lead.suburbId = 0;
            this.lead.stateId = 0;
            this.lead.postCode = "";
            this.lead.address = "";
            this.lead.country = "";

            this.lead.streetNo = result.address_components[0].long_name.toUpperCase();
            var str = result.address_components[1].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.streetType = splitted[3].toUpperCase().trim();
            }
            else if (splitted.length == 3) {
                this.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.streetType = splitted[2].toUpperCase().trim();
            }
            else {
                this.streetName = splitted[0].toUpperCase().trim();
                this.streetType = splitted[1].toUpperCase().trim();
            }
            this.suburb = result.address_components[2].long_name.toUpperCase().trim();
            this.lead.state = result.address_components[3].short_name;
            this._leadsServiceProxy.getSuburbId(result.address_components[5].long_name).subscribe(result => {
                this.lead.suburbId = result;
            });
            this._leadsServiceProxy.stateId(result.address_components[3].short_name).subscribe(result => {
                this.lead.stateId = result;
            });
            this.lead.postCode = result.address_components[5].long_name.toUpperCase();
            this.lead.address = this.lead.unitNo + " " + this.lead.unitType + " " + this.lead.streetNo + " " + this.streetName + " " + this.streetType;  //result.formatted_address;
            this.lead.country = result.address_components[4].long_name.toUpperCase();
            this._leadsServiceProxy.getareaBysuburbPostandstate(this.suburb, this.lead.state, this.lead.postCode).subscribe(result => {
                if (result != null) {
                    this.lead.area = result;
                }
            });
        }
        else {
            this.lead.unitNo = "";
            this.unitType = "";
            this.lead.unitType = "";
            this.lead.streetNo = "";
            this.streetName = "";
            this.streetType = "";
            this.suburb = "";
            this.lead.state = "";
            this.lead.suburbId = 0;
            this.lead.stateId = 0;
            this.lead.postCode = "";
            this.lead.address = "";
            this.lead.country = "";

            var str1 = result.address_components[0].long_name.toUpperCase();
            var splitted1 = str1.split(" ");
            if (splitted1.length == 1) {
                this.lead.unitNo = splitted1[0].toUpperCase().trim();
            }
            else {
                this.lead.unitNo = splitted1[1].toUpperCase().trim();
                this.unitType = splitted1[0].toUpperCase().trim();
                this.lead.unitType = splitted1[0].toUpperCase().trim();
            }
            this.lead.streetNo = result.address_components[1].long_name.toUpperCase();
            var str = result.address_components[2].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.streetType = splitted[3].toUpperCase().trim();
            }
            else if (splitted.length == 3) {
                this.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.streetType = splitted[2].toUpperCase().trim();
            }
            else {
                this.streetName = splitted[0].toUpperCase().trim();
                this.streetType = splitted[1].toUpperCase().trim();
            }
            this.suburb = result.address_components[3].long_name.toUpperCase().trim();
            this.lead.state = result.address_components[5].short_name;
            this._leadsServiceProxy.getSuburbId(result.address_components[7].long_name).subscribe(result => {
                this.lead.suburbId = result;
            });
            this._leadsServiceProxy.stateId(result.address_components[5].short_name).subscribe(result => {
                this.lead.stateId = result;
            });
            this.lead.postCode = result.address_components[7].long_name.toUpperCase();
            this.lead.address = this.lead.unitNo + " " + this.lead.unitType + " " + this.lead.streetNo + " " + this.streetName + " " + this.streetType;  //result.formatted_address;
            this.lead.country = result.address_components[6].long_name.toUpperCase();

            this._leadsServiceProxy.getareaBysuburbPostandstate(this.suburb, this.lead.state, this.lead.postCode).subscribe(result => {
                if (result != null) {
                    this.lead.area = result;
                }
            });
        }
        this.bindArea();
    }

    onLocationSelected(location: Location) {
        this.lead.latitude = location.latitude.toString();
        this.lead.longitude = location.longitude.toString();
    }

    show(OrganizationId?: number, leadId?: number, sectionId? : number,section? : string): void {
        debugger;
        this._commonLookupService.getAllUsersTableDropdownForUserCallHistory(OrganizationId).subscribe(rep => {
            this.userList = rep;
        });
        this.section = section;
        let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote = leadId > 0 ? 'Open For Edit Leads' : 'Open For Create New Leads';
            log.section = section;
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
        this.sectionId = sectionId;
        if (!leadId) {
            this.lead = new CreateOrEditLeadDto();
            this.lead.id = leadId;
            this.postalSuburb = '';
            this.stateName = '';
            this.leadSourceName = '';
            this.LeadStatusName = '';
            this.lead.leadSource = "";
            this.lead.area = "";
            this.lead.type = "";
            this.lead.state = "";
            this.lead.organizationId = OrganizationId;
            this.lead.isGoogle = "Google";
            this.lead.latitude = "";
            this.lead.longitude = "";
            this.active = true;
            this.lead.address = '';
           
        } else {
            this._leadsServiceProxy.getLeadForEdit(leadId).subscribe(result => {

                if (this.from == "myleaddb" || this.from == "myleads" || this.from == "leadTracker") {
                    // if(result.currentUserId == result.lead.assignToUserID || result.currentUserId == 1)
                    if (result.ownTeamUser == true) {
                        this.lead = result.lead;
                        this.unitType = result.lead.unitType;
                        this.streetName = result.lead.streetName;
                        this.streetType = result.lead.streetType;
                        this.suburb = result.lead.suburb;
                        this.lead.latitude =  result.lead.latitude;
                    this.lead.longitude = result.lead.longitude;
                        this.active = true;
                    }
                    else {
                        this.notify.error(this.l('This Lead Is Not Assigned To Current User'));
                    }
                }
                else {
                    this.lead = result.lead;
                    this.unitType = result.lead.unitType;
                    this.streetName = result.lead.streetName;
                    this.streetType = result.lead.streetType;
                    this.suburb = result.lead.suburb;
                    this.lead.latitude =  result.lead.latitude;
                    this.lead.longitude = result.lead.longitude;
                    this.active = true;
                }
                if (this.lead.isGoogle == "Google") {
                    this.selectAddress = true;
                }
                else {
                    this.selectAddress = false;
                }
                this.lead.address = '';
            });

            if(this.lead.leadSource == 'Referral'){
 
                this.referal = true;
            }
            else{
                this.referal = false;
            }
        }
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
        // this._commonLookupService.getAllLeadSourceDropdown().subscribe(result => {
        //     this.allLeadSources = result;
        // });
        // this._commonLookupService.getAllLeadSourceForTableDropdown().subscribe(result => {
        //     this.leadSources = result;
        // });
        this._commonLookupService.getAllLeadSourceForTableDropdownByOrg(OrganizationId).subscribe(result => {
            this.leadSources = result;
            this.allLeadSources = result;
        });
    }

    save(): void {
        debugger;
        this.NoData = false;
        this.filterdata = false;
        if (this.lead.leadStatusID == 1 || this.lead.leadStatusID == 10 || this.lead.leadStatusID == 11) {
            this.notify.warn(this.l('Please ChangeLead Status'));
            return;
        }
        var mobileRegExp = /^(04)[\d]{8}/.test(this.lead.mobile);
        if(!mobileRegExp){
            this.notify.warn(this.l('Invalid Mobile No'));
            document.body.classList.add('removeAlerticon');
            return;
        }
        this.saving = true;
        this.lead.unitType = this.unitType;
        this.lead.suburb = this.suburb;
        this.lead.streetName = this.streetName;
        this.lead.streetType = this.streetType;
        if (!this.lead.id)
            if (this.from == "myleaddb" || this.from == "myleads") {
                this.lead.from = "myleads";
            }

        let input = {} as CheckExistLeadDto;
        input.email = this.lead.email;
        input.mobile = this.lead.mobile;
        input.unitNo = this.lead.unitNo;
        input.unitType = this.lead.unitType;
        input.streetNo = this.lead.streetNo;
        input.streetName = this.lead.streetName;
        input.streetType = this.lead.streetType;
        input.suburb = this.lead.suburb;
        input.state = this.lead.state;
        input.postCode = this.lead.postCode;
        input.organizationId = this.lead.organizationId;
        input.id = this.lead.id;
        this.lead.sectionId = this.sectionId
        this._leadsServiceProxy.checkValidation(this.lead)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(result => {
                if (result) {
                    this._leadsServiceProxy.checkExistLeadList(input)
                        .pipe(finalize(() => { this.saving = false; }))
                        .subscribe(result => {
                            this.item = result;
                            if (result.length > 0) {
                                // if (this.role == 'Admin') {
                                //     this.message.confirm('Do You Still Want to Proceed',
                                //         "Looks Like Duplicate Record",
                                //         (isConfirmed) => {
                                //             if (isConfirmed) {
                                //                 if (this.lead.suburb == null) {
                                //                     isConfirmed = false;
                                //                     this.message.confirm('Are You Sure You Want To Proceed',
                                //                         "Address Not Entered",
                                //                         (isConfirmed) => {
                                //                             if (isConfirmed) {
                                //                                 this._leadsServiceProxy.createOrEdit(this.lead)
                                //                                     .pipe(finalize(() => { this.saving = false; }))
                                //                                     .subscribe(() => {
                                //                                         if (this.from == "myleaddb") {
                                //                                             this._router.navigate(['/app/main/dashboard']);
                                //                                         }
                                //                                         else if (this.from == "leadTracker") {
                                //                                             this._router.navigate(['/app/main/leads/leads/leadtracker']);
                                //                                         }
                                //                                         else if (this.from == "myleads") {
                                //                                             this._router.navigate(['/app/main/myleads/myleads']);
                                //                                         }
                                //                                         else {
                                //                                             this._router.navigate(['/app/main/leads/leads']);
                                //                                         }
                                //                                         this.notify.info(this.l('SavedSuccessfully'));
                                //                                         this.saving = false;

                                //                                     });
                                //                             }
                                //                             else {
                                //                                 this.saving = false;
                                //                             }
                                //                         }
                                //                     );
                                //                 }
                                //                 else {
                                //                     this._leadsServiceProxy.createOrEdit(this.lead)
                                //                         .pipe(finalize(() => { this.saving = false; }))
                                //                         .subscribe(() => {
                                //                             if (this.from == "myleaddb") {
                                //                                 this._router.navigate(['/app/main/dashboard']);
                                //                             }
                                //                             else if (this.from == "leadTracker") {
                                //                                 this._router.navigate(['/app/main/leads/leads/leadtracker']);
                                //                             }
                                //                             else if (this.from == "myleads") {
                                //                                 this._router.navigate(['/app/main/myleads/myleads']);
                                //                             }
                                //                             else {
                                //                                 this._router.navigate(['/app/main/leads/leads']);
                                //                             }
                                //                             this.notify.info(this.l('SavedSuccessfully'));
                                //                             this.saving = false;
                                //                         });
                                //                 }
                                //             } else {
                                //                 this.saving = false;
                                //             }
                                //         }
                                //     );
                                // }
                                // else {
                                //     let element: HTMLElement = document.getElementById('auto_trigger') as HTMLElement;
                                //     element.click();
                                //     this.saving = false;
                                // }
                                let element: HTMLElement = document.getElementById('auto_trigger') as HTMLElement;
                                    element.click();
                                    this.saving = false;
                            }
                            else {
                                if (this.lead.suburb == null) {
                                    this.message.confirm('Are You Sure You Want To Proceed',
                                        "Address Not Entered",
                                        (isConfirmed) => {
                                            if (isConfirmed) {
                                                this._leadsServiceProxy.createOrEdit(this.lead)
                                                    .pipe(finalize(() => { this.saving = false; }))
                                                    .subscribe(() => {
                                                        this.saveLeadLog();
                                                        if (this.from == "myleaddb") {
                                                            this._router.navigate(['/app/main/dashboard']);
                                                        }
                                                        else if (this.from == "leadTracker") {
                                                            this._router.navigate(['/app/main/leads/leads/leadtracker']);
                                                        }
                                                        else if (this.from == "myleads") {
                                                            this._router.navigate(['/app/main/myleads/myleads']);
                                                        }
                                                        else {
                                                            this._router.navigate(['/app/main/leads/leads']);
                                                        }
                                                        this.notify.info(this.l('SavedSuccessfully'));
                                                        this.saving = false;
                                                        
                                                    });
                                            }
                                            else {
                                                this.saving = false;
                                            }
                                        }
                                    );
                                }
                                else {
                                    this._leadsServiceProxy.createOrEdit(this.lead)
                                        .pipe(finalize(() => { this.saving = false; }))
                                        .subscribe(() => {
                                                        this.saveLeadLog();
                                            if (this.from == "myleaddb") {
                                                this._router.navigate(['/app/main/dashboard']);
                                            }
                                            else if (this.from == "leadTracker") {
                                                this._router.navigate(['/app/main/leads/leads/leadtracker']);
                                            }
                                            else if (this.from == "myleads") {
                                                this._router.navigate(['/app/main/myleads/myleads']);
                                            }
                                            else {
                                                this._router.navigate(['/app/main/leads/leads']);
                                            }
                                            this.notify.info(this.l('SavedSuccessfully'));
                                            this.saving = false;
                                        });
                                }

                            }
                            //this.modal.show();
                        });
                }
                else {
                    this.saving = false;
                }
            });
        //  this._leadsServiceProxy.checkExistLeadList(input)
        //         .subscribe(result => {      
        //             this.item = result;
        //             if(result.length > 0){
        //                 let element:HTMLElement = document.getElementById('auto_trigger') as HTMLElement;
        //                 element.click();
        //                 this.saving = false;
        //             }
        //             else{
        //                 this._leadsServiceProxy.createOrEdit(this.lead)
        //                     .pipe(finalize(() => { this.saving = false;}))
        //                     .subscribe(() => {
        //                     if(this.from == "myleaddb"){
        //                         this._router.navigate(['/app/main/dashboard']);
        //                     }
        //                     else if(this.from == "myleads"){
        //                         this._router.navigate(['/app/main/myleads/myleads']);
        //                     }
        //                     else{
        //                         this._router.navigate(['/app/main/leads/leads']);
        //                     }
        //                     this.notify.info(this.l('SavedSuccessfully'));
        //                     });
        //             }
        //             //this.modal.show();
        //         });
    }

    createEdit() : void{
        this._leadsServiceProxy.createOrEdit(this.lead)
                                                    .pipe(finalize(() => { this.saving = false; }))
                                                    .subscribe(() => {
                                                        this.saveLeadLog();
                                                        if (this.from == "myleaddb") {
                                                            this._router.navigate(['/app/main/dashboard']);
                                                        }
                                                        else if (this.from == "leadTracker") {
                                                            this._router.navigate(['/app/main/leads/leads/leadtracker']);
                                                        }
                                                        else if (this.from == "myleads") {
                                                            this._router.navigate(['/app/main/myleads/myleads']);
                                                        }
                                                        else {
                                                            this._router.navigate(['/app/main/leads/leads']);
                                                        }
                                                        this.notify.info(this.l('SavedSuccessfully'));
                                                        this.saving = false;
                                                    });
    }

    google(): void {
        this.selectAddress = true;
    }

    database(): void {
        this.selectAddress = false;
    }

    Reset(): void {
        this.selectAddress = true;
        this.lead.companyName = undefined;
        this.lead.leadSource = "";
        this.lead.area = "";
        this.lead.type = "";
        this.lead.email = undefined;
        this.lead.phone = undefined;
        this.lead.mobile = undefined;
        this.lead.altPhone = undefined;
        this.lead.requirements = undefined;
        this.lead.solarType = undefined;
        this.lead.address = undefined;
        this.lead.unitNo = undefined;
        this.unitType = undefined;
        this.lead.streetNo = undefined;
        this.streetName = undefined;
        this.streetType = undefined;
        this.suburb = undefined;
        this.lead.state = "";
        this.lead.postCode = undefined;
        this.lead.country = undefined;
        this.lead.abn = undefined;
        this.lead.fax = undefined;
        this.lead.isGoogle = "Google";
    }

    cancel(): void {
        this.Reset();
        if (this.from == "myleaddb") {
            this._router.navigate(['/app/main/dashboard']);
        }

        else if (this.from == "leadTracker") {
            this._router.navigate(['/app/main/leads/leads/leadtracker']);
        }

        else if (this.from == "myleads") {
            this._router.navigate(['/app/main/myleads/myleads']);
        }

        else {
            this._router.navigate(['/app/main/leads/leads']);
        }
    }

    bindArea(): void {
        
        if(this.lead.postCode != null && this.lead.postCode != "" && this.lead.postCode != undefined)
        {
            this._postCodeRangeServiceProxy.getAreaByPostCodeRange(this.lead.postCode).subscribe((result) => 
            {
                this.lead.area = result;
            });
        }
    }

    //Referral Details Bind
    searchResult: any [];
    filterjobnumber(event): void {
        this._jobInstallerInvoiceServiceProxy.getSearchFilter(event,'cust').subscribe(result => {
            this.searchResult = result;
            if(result.length > 0){
                this.NoData = false;
                this.filterdata = true;
              }
              else{
                this.NoData = true;
                this.filterdata = false;
              }
        });
    }

    OnSelectDetails(event): void {
        // alert(event.leadId);
        this.lead.referralLeadId = event.leadId;
        this.lead.referralName = event.customerName;
        this.NoData = false;
        this.filterdata = false;
    }
    // End Referral Details Code

    referal: boolean = false;
    LeadSourceOnChange() {
        if(this.lead.leadSource == 'Referral'){

            this.referal = true;
        }
        else{
            this.referal = false;
        }
    }

    saveLeadLog(): void {
        if(this.lead.id > 0){
            let log = new UserActivityLogDto();
            log.actionId = 2;
            log.actionNote = 'Lead Modified';
            log.section = this.section;
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
        }
        else{
            let log = new UserActivityLogDto();
            log.actionId = 1;
            log.actionNote = 'Lead Created';
            log.section = this.section;
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
        }
    }
}
