﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit, Input } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { LeadsServiceProxy, GetLeadForViewDto, LeadDto, GetLeadForChangeStatusOutput, GetActivityLogViewDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AddActivityModalComponent } from '@app/main/myleads/myleads/add-activity-model.component';
import { JobsComponent } from '@app/main/jobs/jobs/jobs.component';
import * as moment from 'moment';

@Component({
    selector: 'viewLead',
    templateUrl: './view-lead.component.html',
    styleUrls: ['view-lead.component.css'],
    animations: [appModuleAnimation()]
})
export class ViewLeadComponent extends AppComponentBase implements OnInit {

    @ViewChild('addActivityModal', {static: true}) addActivityModal: AddActivityModalComponent;
    @ViewChild('jobCreateOrEdit', {static: true}) jobCreateOrEdit: JobsComponent;

    @Input() SelectedLeadId: number = 0;
    @Output() reloadLead = new EventEmitter();
    active = false;
    saving = false;
    lat:number;
    lng:number;
    zoom: 20;
    item: GetLeadForViewDto;
    activityLog:GetActivityLogViewDto[];
    activeTabIndex: number = 0;
    activityDatabyDate : any[] =[];

    constructor(
        injector: Injector,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _router: Router
    ) {
        super(injector);
        this.item = new GetLeadForViewDto();
        this.item.lead = new LeadDto();        
    }

    ngOnInit(): void {
        if(this.SelectedLeadId > 0)
            this.showDetail(this.SelectedLeadId);
            this.registerToEvents();
    }

    getParsedDate(strDate) {//get date formate
        if (strDate == "" || strDate == null || strDate == undefined) {
            return;
        }
        let month_names = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
      
        var strSplitDate = String(strDate._i).split('T');
       
        var dateArray = strSplitDate[0].split('-');
        let monthint = parseInt(dateArray[1]);
        let date = month_names[monthint - 1] + " " + dateArray[2];
        return date;
        
      }

    showDetail(leadId: number): void {
        
        let that = this;
        this._leadsServiceProxy.getLeadForView(leadId,0).subscribe(result => {      
            this.item = result;
            this.lng = parseFloat(result.longitude);
            this.lat = parseFloat(result.latitude);
            this.jobCreateOrEdit.getJobDetailByLeadId(this.item.lead,0);
        });

        this._leadsServiceProxy.getLeadActivityLog(leadId,0,0,false,false, 0, false,0).subscribe(result => {
            let lastdatetime !: moment.Moment;
            this.activityLog = result;
            
            let obj = {
                activityDate:"",
                DatewiseActivity:[]
            }

            this.activityDatabyDate = [];
            this.activityLog.forEach(function(log){
                if(that.getParsedDate(log.creationTime) == that.getParsedDate(lastdatetime)){
                    log.logDate = "";
                    let item1 = this.activityDatabyDate.find(activityDate => activityDate.activityDate === that.getParsedDate(lastdatetime));
                    item1.DatewiseActivity.push({actionNote:log.actionNote});
                }
                else{
                    log.logDate = that.getParsedDate(log.creationTime);
                    obj.activityDate=log.logDate;
                    obj.DatewiseActivity.push(log.actionNote);
                    this.activityDatabyDate.push({activityDate:log.logDate,
                    DatewiseActivity:[{actionNote:log.actionNote}]});
                }
                lastdatetime = log.creationTime;
            }.bind(that));
            
        });
      
    }

    createLead(): void {
        this._router.navigate(['/app/main/leads/leads/createOrEdit'], { queryParams: {from:"mylead" }});
    }

    
    deleteLead(lead: LeadDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._leadsServiceProxy.deleteLeads(lead.id)
                        .subscribe(() => {
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            this.reloadLead.emit();
                        });
                }
            }
        );
    }

    warm(lead: LeadDto):void{
        let status :GetLeadForChangeStatusOutput = new GetLeadForChangeStatusOutput();
        status.id = lead.id;
        status.leadStatusID = 4;
        this.message.confirm('',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._leadsServiceProxy.changeStatus(status)
                        .subscribe(() => {
                            this.notify.success(this.l('LeadStatusChangedToWarm'));
                            //this._router.navigate(['/app/main/myleads/myleads']);
                            this.showDetail(lead.id);
                        });
                }
            }
        );
    }

    cold(lead: LeadDto):void{
        let status :GetLeadForChangeStatusOutput = new GetLeadForChangeStatusOutput();
        status.id = lead.id;
        status.leadStatusID = 3;
        this.message.confirm('',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._leadsServiceProxy.changeStatus(status)
                        .subscribe(() => {
                            this.notify.success(this.l('LeadStatusChangedToCold'));
                            //this._router.navigate(['/app/main/myleads/myleads']);
                            this.showDetail(lead.id);
                        });
                }
            }
        );
    }

    hot(lead: LeadDto):void{
        let status :GetLeadForChangeStatusOutput = new GetLeadForChangeStatusOutput();
        status.id = lead.id;
        status.leadStatusID = 5;
        this.message.confirm('',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._leadsServiceProxy.changeStatus(status)
                        .subscribe(() => {
                            this.notify.success(this.l('LeadStatusChangedToHot'));
                            //this._router.navigate(['/app/main/myleads/myleads']);
                            this.showDetail(lead.id);
                        });
                }
            }
        );
    }

    cancel(lead: LeadDto):void{

        abp.event.trigger('app.show.cancelLeadModal', lead.id);
        // let status :GetLeadForChangeStatusOutput = new GetLeadForChangeStatusOutput();
        // status.id = lead.id;
        // status.leadStatusID = 8;
        // this.message.confirm('',
        //     this.l('AreYouSure'),
        //     (isConfirmed) => {
        //         if (isConfirmed) {
        //             this._leadsServiceProxy.changeStatus(status)
        //                 .subscribe(() => {
        //                     this.notify.success(this.l('LeadStatusChangedToCancel'));
        //                     this._router.navigate(['/app/main/myleads/myleads']);
        //                 });
        //         }
        //     }
        // );
    }

    registerToEvents(){
        abp.event.on('app.onCancelModalSaved', () => {
            this.showDetail(this.SelectedLeadId);
        });
      }

    addActivitySuccess(){
          
       this.showDetail(this.SelectedLeadId);
    }

}
