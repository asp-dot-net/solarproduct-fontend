﻿import { Component, Injector, ViewEncapsulation, ViewChild, OnInit, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LeadsServiceProxy, LeadDto, LeadStatusLookupTableDto, LeadUsersLookupTableDto, GetLeadForChangeFakeLeadStatusInput, GetLeadForChangeDuplicateStatusOutput, LeadSourceLookupTableDto, OrganizationUnitDto, UserServiceProxy, NameValueOfString, LeadStateLookupTableDto, JobStatusTableDto, CommonLookupServiceProxy, LeadsSummaryCount, AssignOrTransferLeadInput, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AbpSessionService, NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { AppConsts } from '@shared/AppConsts';
import { HttpClient } from '@angular/common/http';
import { finalize } from 'rxjs/operators';
import { FileUpload } from 'primeng/fileupload';
// import { AssignOrTransferLeadComponent } from './assign-or-transfer-lead.component';
// import { ViewLeadComponent } from './view-lead.component';
import { ViewMyLeadComponent } from '@app/main/myleads/myleads/view-mylead.component';
import { AddActivityModalComponent } from '@app/main/myleads/myleads/add-activity-model.component';
import { Title } from '@angular/platform-browser';

@Component({
    templateUrl: './lead-tracker.component.html',
    styleUrls: ['./leads.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class LeadTrackerComponent extends AppComponentBase implements OnInit {

    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    FiltersData = false;
    flag = true;
    organizationUnitlength: number = 0;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    @ViewChild('ExcelFileUpload', { static: false }) excelFileUpload: FileUpload;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    // @ViewChild('viewLead', { static: true }) viewLead: ViewLeadComponent;
    @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
    @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    @Input() SelectedLeadId: number = 0;
    @Output() reloadLead = new EventEmitter();
    uploadUrl: string;
    advancedFiltersAreShown = false;
    
    copanyNameFilter = '';
    emailFilter = '';
    phoneFilter = '';
    mobileFilter = '';
    addressFilter = '';
    requirementsFilter = '';
    postCodeSuburbFilter = '';
    excelorcsvfile = 0;
    stateNameFilter = '';
    streetNameFilter = '';
    postCodeFilter = '';
    leadSourceNameFilter = '';
    leadSourceIdFilter = [];
    showJobStatus = false;
    
    leadStatusName = '';
    typeNameFilter = '';
    areaNameFilter = '';
    jobStatusIDFilter = 0;
    dateFilterType = 'Creation';
    leadStatus: any;
    allLeadStatus: LeadStatusLookupTableDto[];
    leadStatusId: number = 0;
    
    filterText = '';
    // StartDate = moment().add(0, 'days').endOf('day');
    // EndDate = moment().add(0, 'days').endOf('day');
    date = new Date();
    StartDate: moment.Moment = moment(this.date);
    EndDate: moment.Moment = moment(this.date);
    LastActivityDate: moment.Moment;

    PageNo: number;
    ExpandedView: boolean = true;
    flafValue: boolean = true;
    
    assignLead: number = 0;
    assignLead1: number = 0;
    tableRecords: any;
    saving = false;
    allUsers: LeadUsersLookupTableDto[];
    allUsersss: LeadUsersLookupTableDto[];
    allLeadSources: LeadSourceLookupTableDto[];
    alljobstatus: JobStatusTableDto[];
    filteredTeams: LeadUsersLookupTableDto[];
    filteredManagers: LeadUsersLookupTableDto[];
    filteredReps: LeadUsersLookupTableDto[];
    filteredUsers: LeadUsersLookupTableDto[];
    allStates: LeadStateLookupTableDto[];
    suburbSuggestions: string[];
    allSalesrepUsers: LeadUsersLookupTableDto[];
    totalLeads = 0;
    new = 0;
    unHandledLeads = 0;
    upgrade = 0;
    canceled = 0;
    cold = 0;
    warm = 0;
    hot = 0;
    closed = 0;
    assigned = 0;
    reAssigned = 0;
    sold = 0;
    rejected = 0;
    showOrganizationUnits = false;
    allOrganizationUnits: OrganizationUnitDto[];
    allOrganizationUnitsList: OrganizationUnitDto[];
    leadstatuss: LeadStatusLookupTableDto[];
    organizationUnit = 0;
    organizationChangeUnit = 0;
    isChange = false;
    isAssign = false;
    teamId = 0;
    salesManagerId: number = 0;
    salesRepId = 0;
    userId = 0;
    onlyAssignLead: boolean;
    role: string = '';
    currentUserId: number = 0;
    projectNumberFilter = '';
    leadName: string;
    totalcount = 0;
    count = 0;
    cancelrequestfilter = 0;
    jobStatusID = [];
    forall: boolean = true;
    leadcancel: boolean = false;
    leadreject: boolean = false;
    leadStatusIDS = [];
    firstrowcount = 0;
    last = 0;
    onlyoncheck = false;
    isChangetrue = false;
    isChangefalse = false;

    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 330;

    summaryCount: LeadsSummaryCount[];

    // isTransferChange = false;

    changeOrganization = 0;
    action = 0;
    assignUserId = 0;
    assignManagerId = 0;
    PlaceHolderVal = '';
    assignUser: any = [];
    assignManager: any = [];
    toggle: boolean = true;
    filterName = 'CompanyName';
    orgCode = '';

    change() {
        this.toggle = !this.toggle;
    }

    constructor(
        injector: Injector,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _httpClient: HttpClient,
        private _fileDownloadService: FileDownloadService,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _router: Router,
        private titleService: Title
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Lead Tracker");
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/Users/ImportLeadFromExcel';
    }

    
    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
        this.isChangefalse = true;

        this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
            this.role = result;
            if (this.role == 'Sales Rep') {
                this.leadStatusId = 10;
                this.dateFilterType = 'Assign';
            }
            
        });

        if(this.appSession.userId)
        {
            if (this.role == 'Sales Manager') {
                this.currentUserId = this.appSession.userId;
            }
        }

        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Lead Tracker';
            log.section = 'Lead Tracker';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.changeOrganization = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.allOrganizationUnitsList = this.allOrganizationUnits.filter(item => item.id != this.organizationUnit)
            this.orgCode = this.allOrganizationUnits[0].code;

            // console.log(abp.session.userId);
            // console.log(this.appSession.userId);

            // this._leadsServiceProxy.getCurrentUserId().subscribe(result => {
            //     console.log(result);
            //     if (this.role == 'Sales Manager') {
            //         this.currentUserId = result;
            //         this.salesManagerId = this.currentUserId;
            //         this.PlaceHolder = "Salect Sales Rep"
            //     }
            //     else{
            //         this.PlaceHolder = "Salect Sales Manager"
            //     }
            //     this.getLeads();
            // });
           
            this.getLeads();
            this.bindLeadSource(this.organizationUnit)
            this.onsalesmanger();
            this.onsalesrep();
            this.onUsers();
            // this._leadsServiceProxy.getSalesManagerUser(this.organizationUnit).subscribe(result => {
            //     this.allUsers = result;
            // });
        });

        this._commonLookupService.getAllLeadStatusForTableDropdown("").subscribe(result => {
            this.leadstatuss = result;
        });

        this._commonLookupService.getAllJobStatusTableDropdown().subscribe(result => {
            this.alljobstatus = result;
        });

        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
        this._commonLookupService.getTeamForFilter().subscribe(teams => {
            this.filteredTeams = teams;
        });

        // this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
        //     this.role = result;
        //     if (this.role == 'Sales Rep') {
        //         this.leadStatusId = 10;
        //         this.dateFilterType = 'Assign';
        //     }
        // });

        
        // // this._commonLookupService.getAllLeadSourceForTableDropdown().subscribe(result => {
        // //     this.allLeadSources = result;
        // // });

        // // this._leadsServiceProxy.getAllTeamsForFilter().subscribe(teams => {
        // //     this.filteredTeams = teams;
        // // });
        // this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
        //     this.allStates = result;
        // });
        // this._commonLookupService.getTeamForFilter().subscribe(teams => {
        //     this.filteredTeams = teams;
        // });

        // this._commonLookupService.getOrganizationUnit().subscribe(output => {
        //     this.allOrganizationUnits = output;
        //     this.organizationUnit = this.allOrganizationUnits[0].id;
        //     this.organizationUnitlength = this.allOrganizationUnits.length;
        //     this.allOrganizationUnitsList = this.allOrganizationUnits.filter(item => item.id != this.organizationUnit)
        //     this._leadsServiceProxy.getSalesManagerUser(this.organizationUnit).subscribe(result => {
        //         this.allUsers = result;
        //     });
        //     this.onsalesmanger()
        //     this.onsalesrep()
        //     this.bindLeadSource(this.organizationUnit)

        //     this._leadsServiceProxy.getCurrentUserId().subscribe(result => {
        //         ;
        //         if (this.role == 'Sales Manager') {
        //             this.currentUserId = result;
        //             this.salesManagerId = this.currentUserId;
        //         }
        //         this.getLeads();

        //     });
        // });
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + 80 ;
        }

        else {
            this.testHeight = this.testHeight - 80 ;
        }
    }

    bindLeadSource(id) {
        this._commonLookupService.getAllLeadSourceForTableDropdownOnlyOrg(id).subscribe(result => {
            this.allLeadSources = result;
        });
    }

    onsalesmanger() {
        this._commonLookupService.getSalesManagerForFilter(this.organizationUnit, 0).subscribe(manager => {
            this.filteredManagers = manager;
        });
    }
    onsalesrep() {
        this._commonLookupService.getSalesRepForFilter(this.organizationUnit, undefined).subscribe(rep => {
            this.filteredReps = rep;
        });
    }

    onUsers(){
        let list = ["Sales Manager","Sales Rep", "Installer"]
        this._commonLookupService.getAllUsersByRoleNameNotContainsTableDropdown(list,this.organizationUnit).subscribe(u => {
            this.filteredUsers = u;
        })
    }

    expandGrid() {
        this.ExpandedView = true;
    }

    clear() {
        this.filterText = '';
        this.postCodeSuburbFilter = '';
        this.stateNameFilter = '';
        this.postCodeFilter = '';
        this.typeNameFilter = '';
        this.areaNameFilter = '';
        this.addressFilter = '';
        this.leadSourceNameFilter = '';
        this.leadSourceIdFilter = [];
        this.jobStatusIDFilter = 0;
        this.teamId = 0;
        this.onlyAssignLead = false;
        this.salesRepId = 0;
        this.userId = 0;
        this.projectNumberFilter = '';
        this.cancelrequestfilter = 0;

        this.StartDate = moment(this.date);
        this.EndDate = moment(this.date);
        this.LastActivityDate = null;

        this.salesManagerId = this.currentUserId;
        this.getLeads();
    }

    oncheck(event) {
        if (event.target.checked == true) {
            if (this.allOrganizationUnitsList.length > 0) {
                this.isChange == true;
                this.showOrganizationUnits = true;
                this.isChangetrue = true;
                this.isChangefalse = false
                this.getOrganizationWiseUser();
            } else {
                this.isChange == false;
                this.showOrganizationUnits = false;
                this.notify.warn(this.l('NoOrganizationAvailable'));

            }
        } else {
            this.isChange == false;
            this.showOrganizationUnits = false;
            this.isChangetrue = false;
            this.isChangefalse = true;
            this.getOrganizationuser();
        }
    }

    oncheckboxCheck() {

        
        this.count = 0;
        this.tableRecords.forEach(item => {

            let transfer = this.hideTransfer(item);
            if (item.lead.isSelected == true && transfer == true) {
                this.count = this.count + 1;
            }
        })
    }

    filtersuburbs(event): void {
        this._commonLookupService.getOnlySuburb(event.query).subscribe(output => {
            this.suburbSuggestions = output;
            // console.log(this.suburbSuggestions)
        });
    }
    changeOrgCode() {
        this.orgCode = this.allOrganizationUnits.find(x => x.id == this.organizationUnit).code;
        this.changeOrganization = this.organizationUnit;
    }
    getOrganizationWiseUser() {
        if (this.isChange == true) {
            this._leadsServiceProxy.getSalesManagerUser(this.organizationChangeUnit).subscribe(result => {
                this.allUsersss = result;
            });
        }
    }
    getOrganizationuser() {
        if (this.isChange == false) {
            this._leadsServiceProxy.getSalesManagerUser(this.organizationUnit).subscribe(result => {
                this.allUsers = result;
            });
        }
    }
    getsalerep(event) {
        let id = event.target.value;

        // if(this.role == "Sales Manager")
        // {
        //     id = this.salesManagerId;
        // }

        if (id != 0) {
            if (this.organizationChangeUnit != 0) {
                this._leadsServiceProxy.getSalesRepBySalesManagerid(id, this.organizationChangeUnit).subscribe(result => {
                    this.allSalesrepUsers = result;
                });
            } else {
                this._leadsServiceProxy.getSalesRepBySalesManagerid(id, this.organizationUnit).subscribe(result => {
                    this.allSalesrepUsers = result;
                });
            }
        }
        this.assignLead1 = 0;
    }
    leadstatuschange() {
        if (this.leadStatusIDS.includes(6)) {
            this.showJobStatus = true;
        } else {
            this.showJobStatus = false;
            this.jobStatusID = [];
        }
    }

    checkAll(ev) {

        this.tableRecords.forEach(x => x.lead.isSelected = ev.target.checked);
        this.oncheckboxCheck();
    }

    isAllChecked() {
        if (this.tableRecords)
            return this.tableRecords.every(_ => _.lead.isSelected);
    }

    getLeads(event?: LazyLoadEvent) {
        this.primengTableHelper.showLoadingIndicator();

        this.allOrganizationUnitsList = this.allOrganizationUnits.filter(item => item.id != this.organizationUnit)
        this.PageNo = this.primengTableHelper.getSkipCount(this.paginator, event);

        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        if (this.leadStatusIDS.includes(7)) {
            this.forall = false;
            this.leadreject = true;
            this.leadcancel = false;
        }
        if (this.leadStatusIDS.includes(8)) {
            this.forall = false;
            this.leadcancel = true;
            this.leadreject = false;
        }
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._leadsServiceProxy.getLeadTrackerData(
            this.filterName,
            filterText_,
            this.copanyNameFilter,
            this.emailFilter,
            this.phoneFilter,
            this.mobileFilter,
            this.addressFilter,
            this.requirementsFilter,
            this.postCodeSuburbFilter,
            this.stateNameFilter,
            this.streetNameFilter,
            this.postCodeFilter,
            this.leadSourceNameFilter,
            this.leadSourceIdFilter,
            this.leadStatusName,
            this.typeNameFilter,
            this.areaNameFilter,
            (this.leadStatusId == 0) ? undefined : this.leadStatusId,
            0,
            this.StartDate,
            this.EndDate,
            this.organizationUnit,
            this.teamId == 0 ? undefined : this.teamId,
            this.salesManagerId == 0 ? undefined : this.salesManagerId,
            this.salesRepId == 0 ? undefined : this.salesRepId,
            this.dateFilterType,
            undefined,
            this.onlyAssignLead,
            this.leadStatusIDS.includes(6) ? this.jobStatusIDFilter == 0 ? undefined : this.jobStatusIDFilter : undefined,
            undefined,
            undefined,
            this.projectNumberFilter,
            this.cancelrequestfilter,
            this.jobStatusID,
            this.leadStatusIDS,
            this.LastActivityDate,
            this.userId == 0 ? undefined : this.userId,
            "",
            undefined,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            //debugger;
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            this.tableRecords = result.items;

            this.totalLeads = result.totalCount;

            if (result.totalCount > 0) {
                //this.getLeadSummaryCount();

                this.new = parseInt(result.items[0].summaryCount.new);
                this.unHandledLeads = parseInt(result.items[0].summaryCount.unHandled);
                this.cold = parseInt(result.items[0].summaryCount.cold);
                this.warm = parseInt(result.items[0].summaryCount.warm);
                this.hot = parseInt(result.items[0].summaryCount.hot);
                this.upgrade = parseInt(result.items[0].summaryCount.upgrade);
                this.rejected = parseInt(result.items[0].summaryCount.rejected);
                this.canceled = parseInt(result.items[0].summaryCount.cancelled);
                this.closed = parseInt(result.items[0].summaryCount.closed);
                this.assigned = parseInt(result.items[0].summaryCount.assigned);
                this.reAssigned = parseInt(result.items[0].summaryCount.reAssigned);
                this.sold = parseInt(result.items[0].summaryCount.sold);

            } else {
                this.totalLeads = 0;
                this.new = 0;
                this.unHandledLeads = 0;
                this.cold = 0;
                this.warm = 0;
                this.hot = 0;
                this.upgrade = 0;
                this.rejected = 0;
                this.canceled = 0;
                this.closed = 0;
                this.assigned = 0;
                this.reAssigned = 0;
                this.sold = 0;
            }

            this.primengTableHelper.hideLoadingIndicator();
            this.shouldShow = false;
            if (this.flag == false && result.totalCount != 0 && this.ExpandedView == false) {
                this.navigateToLeadDetail(this.SelectedLeadId)
            }
            else {
                if (this.ExpandedView == false && result.totalCount != 0) {
                    this.navigateToLeadDetail(result.items[0].lead.id)
                }
                else {
                    this.ExpandedView = true;
                }
            }

        },
        err => {
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    filterCountries(event): void {
        this._commonLookupService.getAllLeadStatusForTableDropdown(event.query).subscribe(result => {
            this.allLeadStatus = result;
        });
    }

    reloadPage(flafValue: boolean): void {
        ;
        this.ExpandedView = true;
        this.flag = flafValue;
        this.paginator.changePage(this.paginator.getPage());
        if (!flafValue) {
            this.navigateToLeadDetail(this.SelectedLeadId);
        }
    }

    createLead(): void {
        this._router.navigate(['/app/main/leads/leads/createOrEdit'], { queryParams: { from: "mylead" } });
    }

    navigateToLeadDetail(leadid): void {
        this.ExpandedView = !this.ExpandedView;
        this.ExpandedView = false;
        this.SelectedLeadId = leadid;
        this.leadName = "leadTracker"
        this.viewLeadDetail.showDetail(leadid, this.leadName, 14,0,'Lead Tracker');
    }

    // deleteLead(lead: LeadDto): void {
    //     this.message.confirm(
    //         '',
    //         this.l('AreYouSure'),
    //         (isConfirmed) => {
    //             if (isConfirmed) {
    //                 // this._leadsServiceProxy.delete(lead.id)
    //                 //     .subscribe(() => {
    //                 //         this.reloadPage();
    //                 //         this.notify.success(this.l('SuccessfullyDeleted'));
    //                 //     });
    //             }
    //         }
    //     );
    // }

    exportToExcel(excelorcsv): void {
        this.excelorcsvfile = excelorcsv;
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._leadsServiceProxy.getLeadTrackerToExcel(
            this.filterName,
            filterText_,
            this.copanyNameFilter,
            this.emailFilter,
            this.phoneFilter,
            this.mobileFilter,
            this.addressFilter,
            this.requirementsFilter,
            this.postCodeSuburbFilter,
            this.stateNameFilter,
            this.streetNameFilter,
            this.postCodeFilter,
            this.leadSourceNameFilter,
            this.leadSourceIdFilter,
            this.leadStatusName,
            this.typeNameFilter,
            this.areaNameFilter,
            (this.leadStatusId == 0) ? undefined : this.leadStatusId,
            0,
            this.StartDate,
            this.EndDate,
            this.organizationUnit,
            this.teamId == 0 ? undefined : this.teamId,
            this.salesManagerId == 0 ? undefined : this.salesManagerId,
            this.salesRepId == 0 ? undefined : this.salesRepId,
            this.dateFilterType,
            undefined,
            this.onlyAssignLead,
            this.leadStatusId == 6 ? this.jobStatusIDFilter == 0 ? undefined : this.jobStatusIDFilter : undefined,
            undefined,
            undefined,
            this.excelorcsvfile,
            this.jobStatusID,
            this.leadStatusIDS,
            this.cancelrequestfilter
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    uploadExcel(data: { files: File }): void {
        const formData: FormData = new FormData();
        const file = data.files[0];
        formData.append('file', file, file.name);

        this._httpClient
            .post<any>(this.uploadUrl, formData)
            .pipe(finalize(() => this.excelFileUpload.clear()))
            .subscribe(response => {
                if (response.success) {
                    this.notify.success(this.l('ImportLeadsProcessStart'));
                } else if (response.error != null) {
                    this.notify.error(this.l('ImportLeadUploadFailed'));
                }
            });
    }

    onUploadExcelError(): void {
        this.notify.error(this.l('ImportLeadUploadFailed'));
    }

    // submit(): void {
        
    //     let selectedids = [];
    //     this.saving = true;
    //     if (this.isChange) {
    //         this.primengTableHelper.records.forEach(function (lead) {
    //             if (lead.lead.isSelected) {
    //                 selectedids.push(lead.lead.id);
    //             }
    //         });
    //         let assignleads: GetLeadForAssignOrTransferOutput = new GetLeadForAssignOrTransferOutput();
    //         assignleads.assignToUserID = this.assignLead;
    //         assignleads.leadIds = selectedids;
    //         assignleads.organizationID = this.organizationChangeUnit;
    //         if (this.organizationChangeUnit == 0) {
    //             this.notify.warn(this.l('PleaseSelectOrganization'));
    //             this.saving = false;
    //             return;
    //         }
    //         if (selectedids.length == 0) {
    //             this.notify.warn(this.l('NoLeadsSelected'));
    //             this.saving = false;
    //             return;
    //         }
    //         if (this.assignLead == 0) {
    //             this.notify.warn(this.l('PleaseSelectUser'));
    //             this.saving = false;
    //             return;
    //         }
    //         if (this.isAssign) {
    //             if (this.assignLead1 == 0) {
    //                 this.notify.warn(this.l('PleaseSelctSalesRep'));
    //                 this.saving = false;
    //                 return;
    //             }
    //             else {
    //                 assignleads.assignToUserID = this.assignLead1;
    //             }
    //         }
    //         this._leadsServiceProxy.transferLeadToOtherOrganization(assignleads)
    //             .pipe(finalize(() => { this.saving = false; }))
    //             .subscribe(() => {
    //                 this.reloadPage(true);
    //                 this.isChange = false;
    //                 this.saving = false;
    //                 this.notify.info(this.l('AssignedSuccessfully'));
    //             });
    //     }
    //     else {
    //         this.primengTableHelper.records.forEach(function (lead) {
    //             if (lead.lead.isSelected) {
    //                 selectedids.push(lead.lead.id);
    //             }
    //         });
    //         let assignleads: GetLeadForAssignOrTransferOutput = new GetLeadForAssignOrTransferOutput();
    //         assignleads.assignToUserID = this.assignLead;
    //         assignleads.leadIds = selectedids;
    //         assignleads.organizationID = this.organizationUnit;
    //         if (selectedids.length == 0) {
    //             this.notify.warn(this.l('NoLeadsSelected'));
    //             this.saving = false;
    //             return;
    //         }
    //         if (this.assignLead == 0) {
    //             this.notify.warn(this.l('PleaseSelectUser'));
    //             this.saving = false;
    //             return;
    //         }
    //         if (this.isAssign) {
    //             if (this.assignLead1 == 0) {
    //                 this.notify.warn(this.l('PleaseSelctSalesRep'));
    //                 this.saving = false;
    //                 return;
    //             }
    //             else {
    //                 assignleads.assignToUserID = this.assignLead1;
    //             }
    //         }

    //         this._leadsServiceProxy.assignOrTransferLead(assignleads)
    //             .pipe(finalize(() => { this.saving = false; }))
    //             .subscribe(() => {
    //                 this.reloadPage(true);
    //                 this.saving = false;
    //                 this.notify.info(this.l('AssignedSuccessfully'));
    //             });
    //     }
    // }

    changeAction() : void {
        
        if(this.action == 2)
        {
            this.PlaceHolderVal = 'Select Sales Rep';

            this.spinnerService.show();
            this.assignUser = [];
            this.assignUserId = 0;
            this._commonLookupService.getAllUsersByRoleNameTableDropdown("Sales Manager", this.changeOrganization).subscribe(result => {
                this.assignManager = result;
                this.spinnerService.hide();
            }, e => { this.spinnerService.hide(); });
        } 
        if (this.action == 3)
        {
            debugger;
            this.PlaceHolderVal = 'Select Users';

            this.assignUserId = 0;
            
            this.spinnerService.show();
            this._commonLookupService.getAllUsersByRoleNameTableDropdown("Without Installer", this.changeOrganization).subscribe(result => {
                this.assignUser = result;
                this.spinnerService.hide();
            }, e => { this.spinnerService.hide(); });
        }
        if (this.action == 4)
        {
            this.PlaceHolderVal = 'Select Lead Gen Manager';

            this.assignUserId = 0;

            this.spinnerService.show();
            this._commonLookupService.getAllUsersByRoleNameTableDropdown("Leadgen Manager", this.changeOrganization).subscribe(result => {
                this.assignUser = result;
                this.spinnerService.hide();
            }, e => { this.spinnerService.hide(); });
        } 
        if(this.action == 1) {
            this.PlaceHolderVal = 'Select Sales Manager';

            this.assignUserId = 0;
            
            this.spinnerService.show();
            this._commonLookupService.getAllUsersByRoleNameTableDropdown("Sales Manager", this.changeOrganization).subscribe(result => {
                this.assignUser = result;
                this.spinnerService.hide();
            }, e => { this.spinnerService.hide(); });
        }
        
    }

    getSaleRep() : void {
        this.spinnerService.show();
        this._leadsServiceProxy.getSalesRepBySalesManagerid(this.assignManagerId, this.changeOrganization).subscribe(result => {
            this.assignUser = result;
            this.spinnerService.hide();
        }, e => { this.spinnerService.hide(); });
    }

    submit() : void {
        let selectedids = [];
        this.saving = true;
        debugger
        let transferPermission = this.permission.isGranted("Pages.Leads.LeadTracker.Action.TransferLeftSalesRepLead");

        let action = this.action;

        this.primengTableHelper.records.forEach(function (lead) {
            
            let rtVal = false;
            if(lead.jobStatusId < 4)
            {
                rtVal = true;
            }
            else if(lead.jobStatusId == 4 || lead.jobStatusId == 5)
            {
                if(lead.isLeftSalesRep == false && transferPermission == true)
                {
                    rtVal = true;
                }
            }

            if(action == 4)
            {
                if (lead.lead.isSelected && rtVal == true) {
                    selectedids.push(lead.lead.id);
                }
            }
            else {
                if (lead.lead.isSelected && rtVal == true) {
                    selectedids.push(lead.lead.id);
                }
            }
        });
            
        let leads: AssignOrTransferLeadInput = new AssignOrTransferLeadInput();
        leads.assignToUserID = this.assignUserId;
        leads.leadIds = selectedids;
        leads.organizationID = this.changeOrganization;
        leads.leadActionId = this.action;
        leads.sectionId = 14;
        leads.section = 'Lead Tracker'
        
        if (this.changeOrganization == 0) {
            this.notify.warn(this.l('PleaseSelectOrganization'));
            this.saving = false;
            return;
        }

        // if(this.assignUserId == 0)
        // {
        //     this.notify.warn("Please " + this.PlaceHolderVal);
        //     this.saving = false;
        //     return;
        // }

        if(selectedids.length == 0)
        {       
            this.notify.warn(this.l('NoLeadsSelected'));
            this.saving = false;
            return;
        }
       if(action ==5){
        let status: GetLeadForChangeFakeLeadStatusInput = new GetLeadForChangeFakeLeadStatusInput();
        status.leadId = selectedids;
        status.isFakeLead = true;
        status.sectionId = 14;
        this._leadsServiceProxy.updateFakeLeadFlag(status)
        .pipe(finalize(() => { this.saving = false; }))
        .subscribe(() => {
          this.getLeads();
          this.saving = false;
          this.notify.info(this.l('StatusChangeToFake'));
         }, e => { this.saving = false; });
       } 
       else{
       this._leadsServiceProxy.asssignOrTransferLeads(leads)
      .pipe(finalize(() => { this.saving = false; }))
      .subscribe(() => {
        this.getLeads();
        this.saving = false;
        this.notify.info(this.l('AssignedSuccessfully'));
       }, e => { this.saving = false; });
}
        
    }

    hideTransfer(record: any) : Boolean {
        debugger;
        let rtVal = false;
        
        if(!record.isTrasferAssignCancel){
            rtVal = false;
        }
        else if(record.jobStatusId < 4)
        {
            rtVal = true;
        }
        else if(record.jobStatusId == 4 || record.jobStatusId == 5)
        {
            if(record.isLeftSalesRep == false && this.permission.isGranted("Pages.Leads.LeadTracker.Action.TransferLeftSalesRepLead") == true)
            {
                rtVal = true;
            }
        }

        

        return rtVal;
    }

    UserSearchResult : any [];
    filterUserSearchResult(event): void {
      this.UserSearchResult = Object.assign([], this.assignUser).filter(
        item => item.displayName.toLowerCase().indexOf(event.query.toLowerCase()) > -1
      )
    }

    selectAssignUser(event): void {
        this.assignUserId = event.id == undefined ? 0 : event.id;
    }
    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Lead Tracker';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Lead Tracker';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }
}