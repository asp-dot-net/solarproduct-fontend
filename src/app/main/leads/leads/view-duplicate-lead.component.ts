import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { LeadsServiceProxy, GetDuplicateLeadPopupDto, LeadDto, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'viewDuplicateModal',
    templateUrl: './view-duplicate-lead.component.html'
})
export class ViewDuplicateModalComponent extends AppComponentBase {

    @ViewChild('duplicateModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    saving = false;
    item: GetDuplicateLeadPopupDto[];
    DupCount:number = 0;
    leadId : number;
    organizationId: number;
    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _leadsServiceProxy:LeadsServiceProxy
    ) {
        super(injector);
    }

    show(leadId: number , organizationId: number): void {
        this.leadId = leadId;
        this.organizationId = organizationId;
        let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='View Duplicate Lead';
            log.section = 'Manage Leads';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
        this._leadsServiceProxy.getDuplicateLeadForView(leadId,organizationId).subscribe(result => {    
            this.item = result;
            this.DupCount = this.item.length;
            this.modal.show();
        }); 
        
    }

    close(): void {
        this.modal.hide();
    }

    delete(id : number): void{
        this.message.confirm('',
            this.l('AreYouSureYouWantToDeleteExistingLead'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._leadsServiceProxy.deleteLeads(id)
                        .subscribe(() => {
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            var log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote = "Duplicate Lead Deleted";
                            log.section = "Manage Leads";
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                            .subscribe(() => {
                        }); 
                            this._leadsServiceProxy.getDuplicateLeadForView(this.leadId,this.organizationId).subscribe(result => {    
                                this.item = result;
                                this.DupCount = this.item.length;
                            });
                        });
                }
            }
        );
    }
}
