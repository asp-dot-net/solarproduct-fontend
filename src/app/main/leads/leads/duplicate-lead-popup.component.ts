import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetDuplicateLeadPopupDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'duplicatepopupModal',
    templateUrl: './duplicate-lead-popup.component.html'
})
export class ViewDuplicatePopUpModalComponent extends AppComponentBase {

    @ViewChild('duplicateLeadPOPModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    result: GetDuplicateLeadPopupDto[];
    role: string = '';
    saveBtnShow: boolean = false;

    constructor(
        injector: Injector
    ) {
        super(injector);
    }

    show(item: GetDuplicateLeadPopupDto[], role = ''): void { 
        debugger;      
        this.result = item;
        this.role = role;
        if(role == "Admin" || item[0].dupLead == true) {
            this.saveBtnShow = true;
        } else {
            this.saveBtnShow = false;
        }
        this.modal.show();
    }

    close(): void {
        this.modal.hide();
    }

    save() : void{
        this.modal.hide();
        this.modalSave.emit();
    }

    // saveBtn() {
        
    // }
}