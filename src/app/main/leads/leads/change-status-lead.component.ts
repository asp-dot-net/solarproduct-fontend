import { Component, ViewChild, Injector, Output, EventEmitter, OnInit} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { LeadsServiceProxy, GetLeadForChangeStatusOutput 
                    ,LeadStatusLookupTableDto,
                    CommonLookupServiceProxy
					} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AgmCoreModule } from '@agm/core';
import PlaceResult = google.maps.places.PlaceResult;
import {Appearance, GermanAddress, Location} from '@angular-material-extensions/google-maps-autocomplete';


@Component({
    selector: 'changeLeadStatus',
    templateUrl: './change-status-lead.component.html',
    animations: [appModuleAnimation()]
})
export class ChangeStatusLeadComponent extends AppComponentBase implements OnInit {
    active = false;
    saving = false;
    
    lead: GetLeadForChangeStatusOutput = new GetLeadForChangeStatusOutput();

    allLeadStatus:LeadStatusLookupTableDto[];
                        
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,        
        private _leadsServiceProxy: LeadsServiceProxy,
        private _router: Router,
        private _commonLookupService: CommonLookupServiceProxy
        ) {
        super(injector);
    }

    ngOnInit(): void {
        this.show(this._activatedRoute.snapshot.queryParams['id']);
    }

     
    show(leadId?: number): void {
            this._leadsServiceProxy.getLeadForChangeStatus(leadId).subscribe(result => {
                this.lead = result;
          
            });
                this._commonLookupService.getAllLeadStatusForTableDropdown("").subscribe(result => {						
            this.allLeadStatus = result;
        });
    }

    save(): void {
            this.saving = true;			
            this._leadsServiceProxy.changeStatus(this.lead)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this._router.navigate(['/app/main/leads/leads']);
                this.notify.info(this.l('SavedSuccessfully'));
             });
    }
}
