import { Component,  Injector, OnInit} from '@angular/core';
import { LeadsServiceProxy, GetActivityLogViewDto 
					} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import PlaceResult = google.maps.places.PlaceResult;


@Component({
    selector: 'activityLead',
    templateUrl: './activity-log-lead.component.html',
    animations: [appModuleAnimation()]
})
export class ActivityLogLeadComponent extends AppComponentBase implements OnInit {

    activityLog:GetActivityLogViewDto[];
					
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,        
        private _leadsServiceProxy: LeadsServiceProxy
    ) {
        super(injector);
    }

     ngOnInit(): void {
         this.show(this._activatedRoute.snapshot.queryParams['id']);
     }

    show(leadId?: number): void {        
            this._leadsServiceProxy.getLeadActivityLog(leadId,0,0,false,false, 0, false, 0).subscribe(result => {
                this.activityLog = result;
            });
    }
}
