﻿import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LeadsServiceProxy, LeadDto, LeadStatusLookupTableDto, LeadUsersLookupTableDto, GetLeadForAssignOrTransferOutput, GetLeadForChangeDuplicateStatusOutput, LeadSourceLookupTableDto, OrganizationUnitDto, UserServiceProxy, LeadStateLookupTableDto, CommonLookupServiceProxy, GetLeadForChangeFakeLeadStatusInput, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { NotifyService, TokenService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { AppConsts } from '@shared/AppConsts';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { finalize } from 'rxjs/operators';
import { FileUpload } from 'primeng/fileupload';
import { AssignOrTransferLeadComponent } from './assign-or-transfer-lead.component';
import { AddActivityModalComponent } from '@app/main/myleads/myleads/add-activity-model.component';
import { OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
    templateUrl: './leads.component.html',
    styleUrls: ['./leads.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class LeadsComponent extends AppComponentBase implements OnInit {
    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    excelorcsvfile = 0;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    @ViewChild('assignOrTransferLeadModel', { static: true }) assignOrTransferLeadModel: AssignOrTransferLeadComponent;
    @ViewChild('ExcelFileUpload', { static: false }) excelFileUpload: FileUpload;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;

    FiltersData = true;
    saving = false;
    uploadUrl: string;
    advancedFiltersAreShown = false;
    filterText = '';
    copanyNameFilter = '';
    emailFilter = '';
    phoneFilter = '';
    mobileFilter = '';
    addressFilter = '';
    requirementsFilter = '';
    postCodeSuburbFilter = '';
    stateNameFilter = '';
    streetNameFilter = '';
    postCodeFilter = '';
    leadSourceNameFilter = '';
    leadSourceIdFilter = [];
    //leadSubSourceNameFilter = '';
    leadStatusName = '';
    typeNameFilter = '';
    areaNameFilter = '';
    leadStatus: any;
    allLeadStatus: LeadStatusLookupTableDto[];
    leadStatusId: number = 0;
    StartDate: moment.Moment;
    EndDate: moment.Moment;
    allUsers: LeadUsersLookupTableDto[];
    allLeadSources: LeadSourceLookupTableDto[];
    assignLead: number = 0;
    leadAction: number = 0;
    title: String;
    names: any;
    selectedAll: any;
    tableRecords: any;
    Ids: any;
    date = new Date();

    sampleDateRange: moment.Moment[] = [moment(this.date), moment(this.date)];
    Length: number = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    allOrganizationUnitsList: OrganizationUnitDto[];
    organizationUnit = 0;
    organizationChangeUnit = 0;
    allStates: LeadStateLookupTableDto[];
    suburbSuggestions: string[];
    duplicateFilter = '';
    selectedidslength: number = 0;
    showOrganizationUnits = false;
    organizationUnitlength: number;
    firstrowcount = 0;
    last = 0;
    filterName = "CompanyName";
    orgCode = 'AS';
    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 330;

    toggle: boolean = true;
    toggle1: boolean = true;
    EnableAutoAssignLead : boolean = false;
    change() {
        this.toggle = !this.toggle;
      }
      changeFilter() {
        this.toggle1 = !this.toggle1;
    }

    constructor(
        injector: Injector,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _notifyService: NotifyService,
        private _commonLookupService: CommonLookupServiceProxy,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _httpClient: HttpClient,
        private _userServiceProxy: UserServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _router: Router,
        private _tokenService: TokenService,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private titleService: Title
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Leads");
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/Users/ImportLeadFromExcel';
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
        this._commonLookupService.getAllLeadSourceDropdown().subscribe(result => {
            this.allLeadSources = result;
        });
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });

        this._commonLookupService.getOrganizationUnit().subscribe(output => {

            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            //    if(this.allOrganizationUnits.length > 1) 
            //    {

            //    }
            this.EnableAutoAssignLead = this.allOrganizationUnits[0].enableAutoAssignLead;
            this.orgCode = this.allOrganizationUnits[0].code;

            this.allOrganizationUnitsList = this.allOrganizationUnits.filter(item => item.id != this.organizationUnit)
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Manage Leads';
            log.section = 'Manage Leads';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
            this.getLeads();
        });
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + 77 ;
        }

        else {
            this.testHeight = this.testHeight - 77 ;
        }
    }

    clear() {
        this.filterText = '';
        this.postCodeSuburbFilter = '';
        this.stateNameFilter = '';
        this.postCodeFilter = '';
        this.typeNameFilter = '';
        this.areaNameFilter = '';
        this.leadSourceIdFilter = [];
        this.duplicateFilter = '';
        this.sampleDateRange = [moment(this.date),moment(this.date)]
        this.getLeads();
    }

    filtersuburbs(event): void {
        this._commonLookupService.getOnlySuburb(event.query).subscribe(output => {
            this.suburbSuggestions = output;
        });
    }

    checkAll(ev) {

        if (this.duplicateFilter == "web" || this.duplicateFilter == "db") {
            this.tableRecords.forEach(function (item) {
                // if (item.duplicate == false && item.webDuplicate == false)
                item.lead.isSelected = ev.target.checked;
            });
        }
        else {
            this.tableRecords.forEach(function (item) {
                if (item.duplicate == false && item.webDuplicate == false)
                    item.lead.isSelected = ev.target.checked;
            });
        }
    }

    isAllChecked() {
        if (this.tableRecords)
            return this.tableRecords.every(_ => _.lead.isSelected)
    }

    getLeads(event?: LazyLoadEvent) {
        debugger;
        this.StartDate = this.sampleDateRange[0];
        this.EndDate = this.sampleDateRange[1];
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();
        if (this.leadAction == 2) {
            this.allOrganizationUnitsList = this.allOrganizationUnits.filter(item => item.id != this.organizationUnit);
            this.organizationChangeUnit = this.allOrganizationUnitsList[0].id;
            this._leadsServiceProxy.getSalesManagerUser(this.organizationChangeUnit).subscribe(result => {
                this.allUsers = result;
            });
        }
        if (this.leadAction == 4) {
            this.allOrganizationUnitsList = this.allOrganizationUnits.filter(item => item.id != this.organizationUnit);
            this.organizationChangeUnit = this.allOrganizationUnitsList[0].id;
        }
        if (this.leadAction == 3) {
            this._leadsServiceProxy.getSalesManagerUser(this.organizationUnit).subscribe(result => {
                this.allUsers = result;
            });
        }
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }

        this._leadsServiceProxy.getAll(
            this.filterName,
            filterText_,
            this.copanyNameFilter,
            this.emailFilter,
            this.phoneFilter,
            this.mobileFilter,
            this.addressFilter,
            this.requirementsFilter,
            this.postCodeSuburbFilter,
            this.stateNameFilter,
            this.streetNameFilter,
            this.postCodeFilter,
            this.leadSourceNameFilter,
            this.leadSourceIdFilter,
            //this.leadSubSourceNameFilter,
            this.leadStatusName,
            this.typeNameFilter,
            this.areaNameFilter,
            (this.leadStatusId == 0) ? undefined : this.leadStatusId,
            0,
            // this.StartDate,
            // this.EndDate,
            undefined,
            undefined,
            this.organizationUnit,
            undefined,
            undefined,
            undefined,
            undefined,
            this.duplicateFilter,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            0,
            "",
            undefined,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.tableRecords = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            this.shouldShow = false;
        });
    }

    filterCountries(event): void {
        this._commonLookupService.getAllLeadStatusForTableDropdown(event.query).subscribe(result => {
            this.allLeadStatus = result;
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createLead(): void {
        this._router.navigate(['/app/main/leads/leads/createOrEdit'], { queryParams: { OId: this.organizationUnit , SectionId : 31, Section : 'Manage Leads' } });
    }

    deleteLead(lead: LeadDto): void {
        this.message.confirm('',
            this.l('AreYouSureYouWantToDeleteExistingLead'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._leadsServiceProxy.deleteDuplicateLeads(lead.id)
                        .subscribe(() => {
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote = 'Delete Duplicate Leads';
                            log.section = 'Manage Leads';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            }); 
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    duplicate(lead: LeadDto): void {
        let status: GetLeadForChangeDuplicateStatusOutput = new GetLeadForChangeDuplicateStatusOutput();
        status.id = lead.id;
        status.isDuplicate = true;
        this.message.confirm('',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._leadsServiceProxy.changeDuplicateStatus(status)
                        .subscribe(() => {
                            this.reloadPage();
                            document.body.classList.add('removeAlerticon');
                            this.notify.success(this.l('Status Change To Duplicate'));
                        });
                }
            }
        );
    }

    notduplicate(lead: LeadDto, webDup?: any): void {
        let status: GetLeadForChangeDuplicateStatusOutput = new GetLeadForChangeDuplicateStatusOutput();
        status.id = lead.id;
        status.isDuplicate = false;
        status.isWebDuplicate = webDup;
        this.message.confirm('',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._leadsServiceProxy.changeDuplicateStatus(status)
                        .subscribe(() => {
                            let log = new UserActivityLogDto();
                            log.actionId = 5;
                            log.actionNote ='Lead Status Changed To Not Database Duplicate';
                            log.section = 'Manage Leads';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            }); 
                            this.reloadPage();
                            document.body.classList.add('removeAlerticon');
                            this.notify.success(this.l('Status Change To Not Duplicate'));
                        });
                }
            }
        );
    }

    webDuplicate(lead: LeadDto): void {
        let status: GetLeadForChangeDuplicateStatusOutput = new GetLeadForChangeDuplicateStatusOutput();
        status.id = lead.id;
        status.isWebDuplicate = true;
        this.message.confirm('',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._leadsServiceProxy.changeDuplicateStatus(status)
                        .subscribe(() => {
                            let log = new UserActivityLogDto();
                            log.actionId = 5;
                            log.actionNote = status.isDuplicate ? 'Lead Status Changed To Database Duplicate' : 'Lead Status Changed To Not Web Duplicate';
                            log.section = 'Manage Leads';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            }); 
                            this.reloadPage();
                            document.body.classList.add('removeAlerticon');
                            this.notify.success(this.l('Status Change To Duplicate'));
                        });
                }
            }
        );
    }

    webNotduplicate(lead: LeadDto, dup?: any): void {
        let status: GetLeadForChangeDuplicateStatusOutput = new GetLeadForChangeDuplicateStatusOutput();
        status.id = lead.id;
        status.isDuplicate = dup;
        status.isWebDuplicate = false;
        this.message.confirm('',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._leadsServiceProxy.changeDuplicateStatus(status)
                        .subscribe(() => {
                            let log = new UserActivityLogDto();
                            log.actionId = 5;
                            log.actionNote = status.isDuplicate ? 'Lead Status Changed To Database Duplicate' : 'Lead Status Changed To Not Web Duplicate';
                            log.section = 'Manage Leads';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            }); 
                            this.reloadPage();
                            document.body.classList.add('removeAlerticon');
                            this.notify.success(this.l('Status Change To Not Duplicate'));
                        });
                }
            }
        );
    }
    changeOrgCode() {
        this.orgCode = this.allOrganizationUnits.find(x => x.id == this.organizationUnit).code;
        this.EnableAutoAssignLead = this.allOrganizationUnits.find(x => x.id == this.organizationUnit).enableAutoAssignLead;

    }
    exportToExcel(excelorcsv): void {
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }

        this.excelorcsvfile = excelorcsv;
        this._leadsServiceProxy.getLeadsToExcel(
            this.filterName,
            filterText_,
            this.copanyNameFilter,
            this.emailFilter,
            this.phoneFilter,
            this.mobileFilter,
            this.addressFilter,
            this.requirementsFilter,
            this.postCodeSuburbFilter,
            this.stateNameFilter,
            this.streetNameFilter,
            this.postCodeFilter,
            this.leadSourceNameFilter,
            this.excelorcsvfile,
            // this.StartDate,
            // this.EndDate,
            undefined,
            undefined,
            this.leadSourceIdFilter,
            this.leadStatusName,
            (this.leadStatusId == 0) ? undefined : this.leadStatusId,
            this.typeNameFilter,
            this.areaNameFilter,
            this.organizationUnit,
            this.duplicateFilter
            //this.leadSubSourceNameFilter,
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    uploadExcel(data: { files: File }): void {
        var headers_object = new HttpHeaders().set("Authorization", "Bearer " + this._tokenService.getToken());

        const httpOptions = {
            headers: headers_object
        };

        const formData: FormData = new FormData();
        const file = data.files[0];
        formData.append('file' + ',' + this.organizationUnit, file, file.name);

        this._httpClient
            .post<any>(this.uploadUrl, formData, httpOptions)
            .pipe(finalize(() => this.excelFileUpload.clear()))
            .subscribe(response => {
                if (response.success) {
                    this.notify.success(this.l('Import Leads Process Start'));
                } else if (response.error != null) {
                    this.notify.error(this.l('Import Lead Upload Failed'));
                }
            });
    }

    onUploadExcelError(): void {
        this.notify.error(this.l('Import Lead Upload Failed'));
    }

    submit(): void {
        this.saving = true;
        if (this.leadAction == 1) {
            let selectedids = [];
            this.primengTableHelper.records.forEach(function (lead) {
                if (lead.lead.isSelected) {
                    selectedids.push(lead.lead.id);
                }
            });
            if (selectedids.length != 0) {
                this._leadsServiceProxy.delete(selectedids)
                    .subscribe(() => {
                        this.reloadPage();
                        var log = new UserActivityLogDto();
                        log.actionId = 83;
                        log.actionNote = "Lead Deleted";
                        log.section = "Manage Leads";
                        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                        this.notify.info(this.l('DeleteSuccessfully'));
                        this.saving = false;
                    });
            }
            else {
                this.notify.warn(this.l('NoLeadsSelected'));
                this.saving = false;
            }
        }
        else if (this.leadAction == 2) {
            let selectedids = [];
            this.primengTableHelper.records.forEach(function (lead) {
                if (lead.lead.isSelected) {
                    selectedids.push(lead.lead.id);
                }
            });
            if (this.organizationChangeUnit == 0 || this.organizationChangeUnit == undefined || this.organizationChangeUnit == null) {
                this.notify.warn(this.l('PleaseSelectOrganization'));
                this.saving = false;
                return;
            }
            let assignleads: GetLeadForAssignOrTransferOutput = new GetLeadForAssignOrTransferOutput();
            assignleads.assignToUserID = this.assignLead;
            assignleads.leadIds = selectedids;
            assignleads.organizationID = this.organizationChangeUnit;
            assignleads.sectionId = 31;
            assignleads.section = 'Manage Leads';
            if (selectedids.length != 0) {
                this._leadsServiceProxy.assignOrTransferLead(assignleads)
                    .pipe(finalize(() => { this.saving = false; }))
                    .subscribe(() => {
                       
                        this.reloadPage();
                        this.notify.info(this.l('AssignedSuccessfully'));
                        this.saving = false;
                    }, err => {
                        this.saving = false;
                    });
            }
            else {
                this.notify.warn(this.l('NoLeadsSelected'));
                this.saving = false;
            }
        }
        else if (this.leadAction == 3) {
            let selectedids = [];
            this.primengTableHelper.records.forEach(function (lead) {
                if (lead.lead.isSelected) {
                    selectedids.push(lead.lead.id);
                }
            });
            if (this.assignLead == 0) {
                this.notify.warn(this.l('NoUserSelected'));
                this.saving = false;
            }

            let assignleads: GetLeadForAssignOrTransferOutput = new GetLeadForAssignOrTransferOutput();
            assignleads.assignToUserID = this.assignLead;
            assignleads.leadIds = selectedids;
            assignleads.sectionId = 31;
            assignleads.section = 'Manage Leads';

            if (selectedids.length != 0) {
                this._leadsServiceProxy.assignOrTransferLead(assignleads)
                    .pipe(finalize(() => { this.saving = false; }))
                    .subscribe(() => {
                        this.reloadPage();
                        this.notify.info(this.l('AssignedSuccessfully'));
                        this.saving = false;
                    });
            }
            else {
                this.notify.warn(this.l('NoLeadsSelected'));
                this.saving = false;
            }
        }
        else if (this.leadAction == 4) {
            let selectedids = [];
            this.primengTableHelper.records.forEach(function (lead) {
                if (lead.lead.isSelected) {
                    selectedids.push(lead.lead.id);
                }
            });

            if (selectedids.length == 1) {
                if (this.organizationChangeUnit) {
                    this._leadsServiceProxy.checkCopyExist(this.organizationChangeUnit, selectedids[0]).subscribe(result => {
                        if (!result) {
                            this._leadsServiceProxy.copyLead(this.organizationChangeUnit, selectedids[0],31)
                                .pipe(finalize(() => { this.saving = false; }))
                                .subscribe(() => {
                                    this.reloadPage();
                                    this.notify.info(this.l('CoppiedSuccessfully'));
                                    this.saving = false;
                                });
                        }
                        else {
                            
                            this.notify.warn(this.l('LeadAlreadyCopied'));
                            this.saving = false;
                        }
                    });
                }
                else {
                    this.notify.warn(this.l('PleaseSelectOrganization'));
                    this.saving = false;
                }
            }
            else {
                this.notify.warn(this.l('OnlyOneLead'));
                this.saving = false;
            }
        }
        else if (this.leadAction == 5) {
            let selectedids = [];
            this.primengTableHelper.records.forEach(function (lead) {
                if (lead.lead.isSelected) {
                    if(lead.duplicate == true || lead.webDuplicate == true)
                    {
                        selectedids.push(lead.lead.id);
                    }
                }
            });
            if (selectedids.length != 0) {
                let status: GetLeadForChangeDuplicateStatusOutput = new GetLeadForChangeDuplicateStatusOutput();
                status.leadId = selectedids;
                status.hideDublicate = true;
                this.message.confirm('',
                    this.l('AreYouSure'),
                    (isConfirmed) => {
                        if (isConfirmed) {
                            this._leadsServiceProxy.changeDuplicateStatusForMultipleLeadsHide(status)
                                .subscribe(() => {
                                    // this.reloadPage();
                                    status.sectionId =31;
                                    this._leadsServiceProxy.updateWebDupLeads(status)
                                        .subscribe(() => {
                                        this.saving = false;
                                        this.reloadPage();
                                        this.notify.success(this.l('StatusChangeToDuplicate'));
                                    });
                                    
                                    // this.notify.success(this.l('StatusChangeToDuplicate'));
                                });
                        } else {
                            this.saving = false;
                        }
                    }
                );

            }
            else {
                this.notify.warn(this.l('NoLeadsSelected'));
                this.saving = false;
            }
        }
        else if (this.leadAction == 6) {
            this._leadsServiceProxy.updateFacebookLead()
                .subscribe(() => {
                    this.saving = false;
                    this.reloadPage();
                    this.notify.success(this.l('DataUpdatedSuccessfully'));
                }, err => { this.saving = false; });
        }
        else if (this.leadAction == 7) {
            let selectedids = [];
            this.primengTableHelper.records.forEach(function (lead) {
                if (lead.lead.isSelected) {
                    
                        selectedids.push(lead.lead.id);
                   
                }
            });
            if (selectedids.length != 0) {
                let status: GetLeadForChangeFakeLeadStatusInput = new GetLeadForChangeFakeLeadStatusInput();
                status.leadId = selectedids;
                status.isFakeLead = true;
                this.message.confirm('',
                    this.l('AreYouSure'),
                    (isConfirmed) => {
                        if (isConfirmed) {
                            status.sectionId =31;

                            this._leadsServiceProxy.updateFakeLeadFlag(status)
                                .subscribe(() => {
                                    this.saving = false;
                                    this.reloadPage();
                                    this.notify.success(this.l('StatusChangeToFake'));
                                    // this.reloadPage();
                                    
                                    // this.notify.success(this.l('StatusChangeToDuplicate'));
                                });
                        } else {
                            this.saving = false;
                        }
                    }
                );

            }
            else {
                this.notify.warn(this.l('NoLeadsSelected'));
                this.saving = false;
            }
        }
        else {
            this.saving = false;
        }
    }

    getOrganizationWiseUser() {
        if (this.leadAction == 2) {
            this.allOrganizationUnitsList = this.allOrganizationUnits.filter(item => item.id != this.organizationUnit);
            if (this.allOrganizationUnitsList.length > 0) {
                this.showOrganizationUnits = true;
                this._leadsServiceProxy.getSalesManagerUser(this.organizationChangeUnit).subscribe(result => {
                    this.allUsers = result;
                });
            } else {
                this.showOrganizationUnits = false;
                this.notify.warn(this.l('NoOrganizationAvailable'));
                this._leadsServiceProxy.getSalesManagerUser(this.organizationUnit).subscribe(result => {
                    this.allUsers = result;
                });
            }

        }
        if (this.leadAction == 3) {
            this._leadsServiceProxy.getSalesManagerUser(this.organizationUnit).subscribe(result => {
                this.allUsers = result;
            });
        }
        if (this.leadAction == 4) {

            this.allOrganizationUnitsList = this.allOrganizationUnits.filter(item => item.id != this.organizationUnit);
            if (this.allOrganizationUnitsList.length > 0) {
                this.organizationChangeUnit = this.allOrganizationUnitsList[0].id;
                this.showOrganizationUnits = true;
            } else {
                this.showOrganizationUnits = false;
                this.notify.warn(this.l('NoOrganizationAvailable'));
            }
        }
    }

    changeEnableAutoAssignLead(){
        this._leadsServiceProxy.updateEnableAutoAssignLead(this.organizationUnit,this.EnableAutoAssignLead)
        .pipe(finalize(() => { this.saving = false; }))
        .subscribe(() => {
            this.notify.info("Successfully AutoAssignLead Updated");
            this.saving = false;
            this.allOrganizationUnits.find(x => x.id == this.organizationUnit).enableAutoAssignLead = this.EnableAutoAssignLead;

        });
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Manage Leads';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Manage Leads';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }

}