﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit, ElementRef, Directive, Optional } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {
    LeadsServiceProxy, CreateOrEditLeadDto
    , LeadStateLookupTableDto
    , LeadSourceLookupTableDto
    , LeadStatusLookupTableDto,
    CheckExistLeadDto,
    GetDuplicateLeadPopupDto,
    CommonLookupServiceProxy,
    PostCodeRangeServiceProxy,
    JobInstallerInvoicesServiceProxy,
    UserActivityLogServiceProxy,
    UserActivityLogDto
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AgmCoreModule } from '@agm/core';
import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';
import PlaceResult = google.maps.places.PlaceResult;
import { Appearance, GermanAddress, Location } from '@angular-material-extensions/google-maps-autocomplete';
import { ViewDuplicatePopUpModalComponent } from './duplicate-lead-popup.component';
import { ViewMyLeadComponent } from '@app/main/myleads/myleads/view-mylead.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { result } from 'lodash';


@Component({
    selector: 'createEditLead',
    templateUrl: './create-edit-lead.component.html',
    styleUrls: ['./create-edit-lead.component.less']
})
export class CreateEditLeadComponent extends AppComponentBase implements OnInit {
    @ViewChild("myNameElem") myNameElem: ElementRef;
    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @ViewChild('SampleDatePicker', { static: true }) sampleDatePicker: ElementRef;
    @ViewChild('duplicatepopupModal', { static: true }) duplicatepopupModal: ViewDuplicatePopUpModalComponent;

    active = false;
    saving = false;
    lead: CreateOrEditLeadDto = new CreateOrEditLeadDto();
    postCodeSuburb = '';
    stateName = '';
    leadSourceName = '';
    LeadStatusName = '';
    latitude = '';
    longitude = '';
    leadStatus: any;
    allStates: LeadStateLookupTableDto[];
    allLeadSources: LeadSourceLookupTableDto[];
    allLeadStatus: LeadStatusLookupTableDto[];
    ShowAddress: boolean = true;
    unitType: any;
    streetType: any;
    streetName: any;
    suburb: any;
    postalUnitType: any;
    postalStreetType: any;
    postalStreetName: any;
    postalSuburb: any;
    filteredunitTypes: LeadStatusLookupTableDto[];
    filteredstreettypes: LeadStatusLookupTableDto[];
    filteredstreetnames: LeadStatusLookupTableDto[];
    filteredsuburbs: LeadStatusLookupTableDto[];
    output: LeadStatusLookupTableDto[] = new Array<LeadStatusLookupTableDto>();
    unitTypesSuggestions: string[];
    streetTypesSuggestions: string[];
    streetNamesSuggestions: string[];
    suburbSuggestions: string[];
    postalunitTypesSuggestions: string[];
    postalstreetTypesSuggestions: string[];
    postalstreetNamesSuggestions: string[];
    postalsuburbSuggestions: string[];
    selectAddress: boolean = true;
    selectAddress2: boolean = true;
    from: string;
    item: GetDuplicateLeadPopupDto[];
    role: string = '';
    sectionId : number = 0;
    JobId = 0;
    isMyLeadsPage: boolean = false;
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _router: Router,
        private _el: ElementRef,
        private spinner: NgxSpinnerService,
        private _postCodeRangeServiceProxy: PostCodeRangeServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _jobInstallerInvoiceServiceProxy: JobInstallerInvoicesServiceProxy,
        @Optional() private viewLeadDetail?: ViewMyLeadComponent,
        
    ) {
        super(injector);
        this._el.nativeElement.setAttribute('autocomplete', 'off');
        this._el.nativeElement.setAttribute('autocorrect', 'off');
        this._el.nativeElement.setAttribute('autocapitalize', 'none');
        this._el.nativeElement.setAttribute('spellcheck', 'false');
    }
 
    ngOnInit(): void {

        // this.show(this._activatedRoute.snapshot.queryParams['OId'], this._activatedRoute.snapshot.queryParams['id']);
        // from = this._activatedRoute.snapshot.queryParams['from'];

        this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
            this.role = result;
        });
    }

    // get UnitType
    filterunitTypes(event): void {
        this._leadsServiceProxy.getAllUnitType(event.query).subscribe(output => {
            this.unitTypesSuggestions = output;
        });
    }

    // get StreetType
    filterstreettypes(event): void {
        this._leadsServiceProxy.getAllStreetType(event.query).subscribe(output => {
            this.streetTypesSuggestions = output;
        });
    }

    // get StreetName
    filterstreetnames(event): void {
        this._leadsServiceProxy.getAllStreetName(event.query).subscribe(output => {
            this.streetNamesSuggestions = output;
        });
    }

    // get Suburb
    filtersuburbs(event): void {
        this._leadsServiceProxy.getAllSuburb(event.query).subscribe(output => {
            this.suburbSuggestions = output;
        });
    }
    //get state & Postcode
    fillstatepostcode(): void {
        var splitted = this.suburb.split("||");
        this.lead.suburb = splitted[0];
        this.lead.state = splitted[1].trim();
        this.lead.postCode = splitted[2].trim();
        this.lead.country = "AUSTRALIA";

        this.bindArea();
    }

    onAutocompleteSelected(result: PlaceResult) {
        debugger;
        if (result.address_components.length == 7) {
            this.lead.unitNo = "";
            this.unitType = "";
            this.lead.unitType = "";
            this.lead.streetNo = "";
            this.streetName = "";
            this.streetType = "";
            this.suburb = "";
            this.lead.state = "";
            this.lead.suburbId = 0;
            this.lead.stateId = 0;
            this.lead.postCode = "";
            this.lead.address = "";
            this.lead.country = "";

            this.lead.streetNo = result.address_components[0].long_name.toUpperCase();
            var str = result.address_components[1].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.streetType = splitted[3].toUpperCase().trim();
            }
            else if (splitted.length == 3) {
                this.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.streetType = splitted[2].toUpperCase().trim();
            }
            else {
                this.streetName = splitted[0].toUpperCase().trim();
                this.streetType = splitted[1].toUpperCase().trim();
            }
            this.suburb = result.address_components[2].long_name.toUpperCase().trim();
            this.lead.state = result.address_components[4].short_name;
            this._leadsServiceProxy.getSuburbId(result.address_components[6].long_name).subscribe(result => {
                this.lead.suburbId = result;
            });
            this._leadsServiceProxy.stateId(result.address_components[4].short_name).subscribe(result => {
                this.lead.stateId = result;
            });
            this.lead.postCode = result.address_components[6].long_name.toUpperCase();
            this.lead.address = this.lead.unitNo + " " + this.lead.unitType + " " + this.lead.streetNo + " " + this.streetName + " " + this.streetType;  //result.formatted_address;
            this.lead.country = result.address_components[5].long_name.toUpperCase();
        }
        else if (result.address_components.length == 6) {
            this.lead.unitNo = "";
            this.unitType = "";
            this.lead.unitType = "";
            this.lead.streetNo = "";
            this.streetName = "";
            this.streetType = "";
            this.suburb = "";
            this.lead.state = "";
            this.lead.suburbId = 0;
            this.lead.stateId = 0;
            this.lead.postCode = "";
            this.lead.address = "";
            this.lead.country = "";

            this.lead.streetNo = result.address_components[0].long_name.toUpperCase();
            var str = result.address_components[1].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.streetType = splitted[3].toUpperCase().trim();
            }
            else if (splitted.length == 3) {
                this.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.streetType = splitted[2].toUpperCase().trim();
            }
            else {
                this.streetName = splitted[0].toUpperCase().trim();
                this.streetType = splitted[1].toUpperCase().trim();
            }
            this.suburb = result.address_components[2].long_name.toUpperCase().trim();
            this.lead.state = result.address_components[3].short_name;
            this._leadsServiceProxy.getSuburbId(result.address_components[5].long_name).subscribe(result => {
                this.lead.suburbId = result;
            });
            this._leadsServiceProxy.stateId(result.address_components[3].short_name).subscribe(result => {
                this.lead.stateId = result;
            });
            this.lead.postCode = result.address_components[5].long_name.toUpperCase();
            this.lead.address = this.lead.unitNo + " " + this.lead.unitType + " " + this.lead.streetNo + " " + this.streetName + " " + this.streetType;  //result.formatted_address;
            this.lead.country = result.address_components[4].long_name.toUpperCase();
        }
        else {
            this.lead.unitNo = "";
            this.unitType = "";
            this.lead.unitType = "";
            this.lead.streetNo = "";
            this.streetName = "";
            this.streetType = "";
            this.suburb = "";
            this.lead.state = "";
            this.lead.suburbId = 0;
            this.lead.stateId = 0;
            this.lead.postCode = "";
            this.lead.address = "";
            this.lead.country = "";

            var str1 = result.address_components[0].long_name.toUpperCase();
            var splitted1 = str1.split(" ");
            if (splitted1.length == 1) {
                this.lead.unitNo = splitted1[0].toUpperCase().trim();
            }
            else {
                this.lead.unitNo = splitted1[1].toUpperCase().trim();
                this.unitType = splitted1[0].toUpperCase().trim();
                this.lead.unitType = splitted1[0].toUpperCase().trim();
            }
            this.lead.streetNo = result.address_components[1].long_name.toUpperCase();
            var str = result.address_components[2].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.streetType = splitted[3].toUpperCase().trim();
            }
            else if (splitted.length == 3) {
                this.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.streetType = splitted[2].toUpperCase().trim();
            }
            else {
                this.streetName = splitted[0].toUpperCase().trim();
                this.streetType = splitted[1].toUpperCase().trim();
            }
            this.suburb = result.address_components[3].long_name.toUpperCase().trim();
            this.lead.state = result.address_components[5].short_name;
            this._leadsServiceProxy.getSuburbId(result.address_components[7].long_name).subscribe(result => {
                this.lead.suburbId = result;
            });
            this._leadsServiceProxy.stateId(result.address_components[5].short_name).subscribe(result => {
                this.lead.stateId = result;
            });
            this.lead.postCode = result.address_components[7].long_name.toUpperCase();
            this.lead.address = this.lead.unitNo + " " + this.lead.unitType + " " + this.lead.streetNo + " " + this.streetName + " " + this.streetType;  //result.formatted_address;
            this.lead.country = result.address_components[6].long_name.toUpperCase();
        }
        this.bindArea();
    }

    onLocationSelected(location: Location) {
        debugger;
        this.lead.latitude = location.latitude.toString();
        this.lead.longitude = location.longitude.toString();
    }

    SectionName = '';
    show(OrganizationId?: number, leadId?: number, from?: string, sectionId? : number, section = ''): void {
        debugger;
        // from = this._activatedRoute.snapshot.queryParams['from'];
        this.SectionName = section;
        this.isMyLeadsPage = (this.SectionName.toLowerCase() === 'my lead');
        let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote = leadId > 0 ? 'Open For Edit Leads' : 'Open For Create New Leads';
                log.section = this.SectionName;
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
debugger;
    this.sectionId = sectionId;
        if (!leadId) {
            this.lead = new CreateOrEditLeadDto();
            this.lead.id = leadId;
            this.postalSuburb = '';
            this.stateName = '';
            this.leadSourceName = '';
            this.LeadStatusName = '';
            this.lead.leadSource = "";
            this.lead.area = "";
            this.lead.type = "";
            this.lead.state = "";
            this.lead.organizationId = OrganizationId;
            this.lead.isGoogle = "Google";
            this.lead.latitude = "";
            this.lead.longitude = "";
            this.active = true;
            this.lead.address = undefined;
        } else {
            console.log(this.lead.address);
            this.lead.address = undefined;
            if (this.allStates == null) {
                this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
                    this.allStates = result;
                });
            }
            if (this.allLeadSources == null) {
                // this._leadsServiceProxy.getLeadSourceDrpdwnForLeadEditInsert().subscribe(result => {
                //     this.allLeadSources = result;
                // });
                this._commonLookupService.getAllLeadSourceForTableDropdownByOrg(OrganizationId).subscribe(result => {
                    debugger;
                    console.log(result);
                    this.allLeadSources = result;
                });

                console.log(this.allLeadSources);
            }
            this.spinner.show();
            this._leadsServiceProxy.getLeadForEdit(leadId).subscribe(result => {
                this.JobId = result.jobId;
                if (from == "myleaddb" || from == "myleads" || from == "leadTracker") {
                    // if(result.currentUserId == result.lead.assignToUserID || result.currentUserId == 1)
                    if (result.ownTeamUser == true) {
                        this.lead = new CreateOrEditLeadDto();
                        this.lead = result.lead;
                        this.unitType = result.lead.unitType;
                        this.streetName = result.lead.streetName;
                        this.streetType = result.lead.streetType;
                        this.suburb = result.lead.suburb;
                        this.lead.latitude =  result.lead.latitude;
                        this.lead.longitude = result.lead.longitude;
                        
                        this.active = true;
                        this.modal.show();
                        this.spinner.hide();
                    }
                    else {
                        this.notify.error(this.l('ThisLeadIsNotAssignedToCurrentUser'));
                    }
                }
                else {
                    this.lead = new CreateOrEditLeadDto();
                    this.lead = result.lead;
                    this.unitType = result.lead.unitType;
                    this.streetName = result.lead.streetName;
                    this.streetType = result.lead.streetType;
                    this.suburb = result.lead.suburb;
                    this.lead.latitude =  result.lead.latitude;
                    this.lead.longitude = result.lead.longitude;
                    this.active = true;
                    this.modal.show();
                    this.spinner.hide();
                }
                if (this.lead.isGoogle == "Google") {
                    this.selectAddress = true;
                }
                else {
                    this.selectAddress = false;
                }

                if(this.lead.leadSource == 'Referral'){
 
                    this.referal = true;
                }
                else{
                    this.referal = false;
                }
               
            });
        }

        

    }

    save(): void {
        debugger;
        if (this.lead.leadStatusID == 1 || this.lead.leadStatusID == 10 || this.lead.leadStatusID == 11) {
            this.notify.warn(this.l('PleaseChangeLeadStatus'));
            return;
        }

        var mobileRegExp = /^(04)[\d]{8}/.test(this.lead.mobile);
        if(!mobileRegExp){
            this.notify.warn(this.l('InvalidMobileNo'));
            return;
        }

        this.saving = true;
        this.lead.isExternalLead = 1;
        this.lead.unitType = this.unitType;
        this.lead.suburb = this.suburb;
        this.lead.streetName = this.streetName;
        this.lead.streetType = this.streetType;
        if (!this.lead.id)
            if (this.from == "myleaddb" || this.from == "myleads") {
                this.lead.from = "myleads";
            }

        let input = {} as CheckExistLeadDto;
        input.email = this.lead.email;
        input.mobile = this.lead.mobile;
        input.unitNo = this.lead.unitNo;
        input.unitType = this.lead.unitType;
        input.streetNo = this.lead.streetNo;
        input.streetName = this.lead.streetName;
        input.streetType = this.lead.streetType;
        input.suburb = this.lead.suburb;
        input.state = this.lead.state;
        input.postCode = this.lead.postCode;
        input.organizationId = this.lead.organizationId;
        input.id = this.lead.id;
        this.lead.sectionId = this.sectionId
        this._leadsServiceProxy.checkValidation(this.lead)
        .pipe(finalize(() => { this.saving = false; }))
            .subscribe(result => {
                if (result) {
                    this._leadsServiceProxy.checkExistLeadList(input)
                    .pipe(finalize(() => { this.saving = false; }))
                        .subscribe(result => {
                            this.item = result;
                            if (result.length > 0) {
                                // if (this.role == 'Admin') {
                                //     this.message.confirm('Do You Still Want to Proceed',
                                //         "Looks Like Duplicate Record",
                                //         (isConfirmed) => {
                                //             if (isConfirmed) {
                                //                 if (this.lead.suburb == null) {
                                //                     isConfirmed = false;
                                //                     this.message.confirm('Are You Sure You Want To Proceed',
                                //                         "Address Not Entered",
                                //                         (isConfirmed) => {
                                //                             if (isConfirmed) {
                                                                
                                //                                 this._leadsServiceProxy.createOrEdit(this.lead)
                                //                                     .pipe(finalize(() => { this.saving = false; }))
                                //                                     .subscribe(() => {
                                //                                         this.viewLeadDetail.reloadLead.emit(false);
                                //                                         this.saving = false; 
                                //                                         this.modal.hide();
                                //                                         this.notify.info(this.l('SavedSuccessfully'));
                                //                                     });
                                //                             }
                                //                             else {
                                //                                 this.saving = false;
                                //                             }
                                //                         }
                                //                     );
                                //                 }
                                //                 else {
                                //                     if(this.JobId > 0){
                                //                         this.message.confirm('Are You Sure You Want To Change Address in Job',
                                //                         "",
                                //                            (isConfirmed) => {
                                //                                this.lead.isJobAddresschange = isConfirmed;
                                //                                this._leadsServiceProxy.createOrEdit(this.lead)
                                //                                    .pipe(finalize(() => { this.saving = false; }))
                                //                                    .subscribe(() => {
                                //                                        this.viewLeadDetail.reloadLead.emit(false);
                                //                                        this.saving = false; 
                                //                                        this.modal.hide();
                                //                                        this.notify.info(this.l('SavedSuccessfully'));
                                //                                    });
                                //                            });
                                //                     }
                                //                     else{
                                //                         this.lead.isJobAddresschange = false;
                                //                         this._leadsServiceProxy.createOrEdit(this.lead)
                                //                             .pipe(finalize(() => { this.saving = false; }))
                                //                             .subscribe(() => {
                                //                                 this.viewLeadDetail.reloadLead.emit(false);
                                //                                 this.saving = false; 
                                //                                 this.modal.hide();
                                //                                 this.notify.info(this.l('SavedSuccessfully'));
                                //                             });
                                //                     }
                                                    
                                                    
                                //                 }
                                //             } else {
                                //                 this.saving = false;
                                //             }
                                //         } 
                                //     );
                                // }
                                // else {
                                    let element: HTMLElement = document.getElementById('auto_trigger') as HTMLElement;
                                    element.click();
                                    this.saving = false;
                                //}
                            }
                            else {
                                if (this.lead.suburb == null) {
                                    this.message.confirm('Are You Sure You Want To Proceed',
                                        "Address Not Entered",
                                        (isConfirmed) => {
                                            if (isConfirmed) {
                                                this._leadsServiceProxy.createOrEdit(this.lead)
                                                    .pipe(finalize(() => { this.saving = false; }))
                                                    .subscribe(() => {
                                                        this.saveLeadLog();
                                                        this.saving = false; 
                                                        this.viewLeadDetail.reloadLead.emit(false);
                                                        this.modal.hide();
                                                        this.notify.info(this.l('SavedSuccessfully'));
                                                    });
                                            }
                                            else {
                                                this.saving = false;
                                            }
                                        }
                                    );
                                }
                                else {
                                    if(this.JobId > 0){
                                        this.message.confirm('Are You Sure You Want To Change Address in Job',
                                        "",
                                           (isConfirmed) => {
                                               this.lead.isJobAddresschange = isConfirmed;
                                               this._leadsServiceProxy.createOrEdit(this.lead)
                                                   .pipe(finalize(() => { this.saving = false; }))
                                                   .subscribe(() => {
                                                        this.saveLeadLog();
                                                        this.viewLeadDetail.reloadLead.emit(false);
                                                       this.saving = false; 
                                                       this.modal.hide();
                                                       this.notify.info(this.l('SavedSuccessfully'));
                                                   });
                                           });
                                    }
                                    else{
                                            this.lead.isJobAddresschange = false;
                                            this._leadsServiceProxy.createOrEdit(this.lead)
                                            .pipe(finalize(() => { this.saving = false; }))
                                            .subscribe(() => {
                                                    this.saveLeadLog();
                                                    this.viewLeadDetail.reloadLead.emit(false);
                                                    this.saving = false; 
                                                    this.modal.hide();
                                                    this.notify.info(this.l('SavedSuccessfully'));
                                                });
                                    }
                                    
                                }

                            }
                        });
                }
                else {
                    this.saving = false;
                }
            });
    }

    google(): void {
        debugger;
        this.selectAddress = true;
    }

    database(): void {
        this.selectAddress = false;
    }

    cancel(): void {
        debugger;
        this.Reset();
        this.modal.hide();
        this.saving = false;
    }

    close(): void {
        this.Reset();
        this.modal.hide();
        this.saving = false;
    }

    bindArea(): void {
        
        if(this.lead.postCode != null && this.lead.postCode != "" && this.lead.postCode != undefined)
        {
            this._postCodeRangeServiceProxy.getAreaByPostCodeRange(this.lead.postCode).subscribe((result) => 
            {
                this.lead.area = result;
            });
        }
    }

     //Referral Details Bind
     searchResult: any [];
     filterText: any;
     filterjobnumber(event): void {
         this._jobInstallerInvoiceServiceProxy.getSearchFilter(event.query,'cust').subscribe(result => {
             this.searchResult = result;
         });
     }
 
     OnSelectDetails(event): void {
         // alert(event.leadId);
         this.lead.referralLeadId = event.leadId;
         this.lead.referralName = event.customerName;
     }
     // End Referral Details Code
 
     referal: boolean = false;
     LeadSourceOnChange() {
         if(this.lead.leadSource == 'Referral'){
 
             this.referal = true;
         }
         else{
             this.referal = false;
         }
     }

     Reset(): void {
        this.selectAddress = true;
        this.lead.companyName = undefined;
        this.lead.leadSource = "";
        this.lead.area = "";
        this.lead.type = "";
        this.lead.email = undefined;
        this.lead.phone = undefined;
        this.lead.mobile = undefined;
        this.lead.altPhone = undefined;
        this.lead.requirements = undefined;
        this.lead.solarType = undefined;
        this.lead.address = undefined;
        this.lead.address = null;
        this.lead.unitNo = undefined;
        this.unitType = undefined;
        this.lead.streetNo = undefined;
        this.streetName = undefined;
        this.streetType = undefined;
        this.suburb = undefined;
        this.lead.state = "";
        this.lead.postCode = undefined;
        this.lead.country = undefined;
        this.lead.abn = undefined;
        this.lead.fax = undefined;
        this.lead.isGoogle = "Google";
    }

    createEdit() : void{
        if(this.JobId > 0){
            this.message.confirm('Are You Sure You Want To Change Address in Job',
            "",
               (isConfirmed) => {
                   this.lead.isJobAddresschange = isConfirmed;
                   this._leadsServiceProxy.createOrEdit(this.lead)
                       .pipe(finalize(() => { this.saving = false; }))
                       .subscribe(() => {
                            this.saveLeadLog();
                           this.viewLeadDetail.reloadLead.emit(false);
                           this.saving = false; 
                           this.modal.hide();
                           this.notify.info(this.l('SavedSuccessfully'));
                       });
               });
        }
        else{
                this.lead.isJobAddresschange = false;
                this._leadsServiceProxy.createOrEdit(this.lead)
                .pipe(finalize(() => { this.saving = false; }))
                .subscribe(() => {
                            this.saveLeadLog();
                            this.viewLeadDetail.reloadLead.emit(false);
                        this.saving = false; 
                        this.modal.hide();
                        this.notify.info(this.l('SavedSuccessfully'));
                    });
        }
    }

    saveLeadLog(): void {
        if(this.lead.id > 0){
            let log = new UserActivityLogDto();
            log.actionId = 2;
            log.actionNote = 'Lead Modified';
            log.section = this.SectionName;
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
        }
        else{
            let log = new UserActivityLogDto();
            log.actionId = 1;
            log.actionNote = 'Lead Created';
            log.section = this.SectionName;
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
        }
    }
}
