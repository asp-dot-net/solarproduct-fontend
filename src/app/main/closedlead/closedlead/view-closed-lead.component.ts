import { Component, ViewChild, Injector,Input, Output, EventEmitter, OnInit } from '@angular/core';
import { ActivatedRoute , Router} from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { LeadsServiceProxy, GetLeadForViewDto, LeadDto, GetActivityLogViewDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import * as moment from 'moment';
@Component({
    selector: 'viewClosedLead',
    templateUrl: './view-closed-lead.component.html',
    animations: [appModuleAnimation()]
})
export class ViewClosedLeadComponent extends AppComponentBase implements OnInit {

    @Input() SelectedLeadId: number = 0;
    @Output() reloadLead = new EventEmitter();
    active = false;
    saving = false;

    item: GetLeadForViewDto;
    activityLog:GetActivityLogViewDto[];
    activeTabIndex: number = 0;
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _router: Router
    ) {
        super(injector);
        this.item = new GetLeadForViewDto();
        this.item.lead = new LeadDto();        
    }

    ngOnInit(): void {
        this.showDetail(this._activatedRoute.snapshot.queryParams['LeadId']);
    }

    getParsedDate(strDate) {//get date formate
        if (strDate == "" || strDate == null || strDate == undefined) {
            return;
        }
        let month_names = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
      
        var strSplitDate = String(strDate._i).split('T');
       
        var dateArray = strSplitDate[0].split('-');
        let monthint = parseInt(dateArray[1]);
        let date = month_names[monthint - 1] + " " + dateArray[2];
        return date;
        
      }

    showDetail(leadId: number): void {
        let that = this;
        this._leadsServiceProxy.getLeadForView(leadId,0).subscribe(result => {      
                   this.item = result;
              });
              this._leadsServiceProxy.getLeadActivityLog(leadId,0,0,false,false, 0, true, 0).subscribe(result => {
                  let lastdatetime !: moment.Moment;
                  this.activityLog = result;
                  this.activityLog.forEach(function(log){
                      
                      if(that.getParsedDate(log.creationTime) == that.getParsedDate(lastdatetime)){
                          log.logDate = "";
                      }
                      else{
                          log.logDate = that.getParsedDate(log.creationTime);
                      }
                      lastdatetime = log.creationTime
                  });
                  
              });
    }

    createLead(): void {
        this._router.navigate(['/app/main/leads/leads/createOrEdit']);
    }

    deleteLead(lead: LeadDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._leadsServiceProxy.deleteLeads(lead.id)
                        .subscribe(() => {
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            this.reloadLead.emit();
                        });
                }
            }
        );
    }

}
