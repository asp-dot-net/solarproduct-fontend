﻿import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LeadsServiceProxy, LeadDto, LeadStatusLookupTableDto, OrganizationUnitDto, UserServiceProxy, LeadStateLookupTableDto, CommonLookupServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { AppConsts } from '@shared/AppConsts';
import { HttpClient } from '@angular/common/http';
import { finalize } from 'rxjs/operators';
import { FileUpload } from 'primeng/fileupload';
import { OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';


@Component({
    templateUrl: './closedleads.component.html',
    styleUrls: ['./closedleads.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ClosedleadsComponent extends AppComponentBase implements OnInit {
    FiltersData = false;
    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    organizationUnitlength: number=0;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    @ViewChild('ExcelFileUpload', { static: false }) excelFileUpload: FileUpload;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    excelorcsvfile = 0;
    uploadUrl: string;
    advancedFiltersAreShown = false;
    filterText = '';
    copanyNameFilter = '';
    emailFilter = '';
    phoneFilter = '';
    mobileFilter = '';
    addressFilter = '';
    requirementsFilter = '';
    postCodeSuburbFilter = '';
    stateNameFilter = '';
    streetNameFilter = '';
    postCodePostalCode2Filter = '';
    leadSourceNameFilter = '';
    //leadSubSourceNameFilter = '';
    leadStatusName = '';
    typeNameFilter = '';
    areaNameFilter = '';
    leadStatus: any;
    allLeadStatus: LeadStatusLookupTableDto[];
    leadStatusId: number = 0;
    StartDate: moment.Moment;
    EndDate: moment.Moment;
    PageNo: number;
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnit = 0;
    allStates: LeadStateLookupTableDto[];
    suburbSuggestions: string[];
    firstrowcount = 0;
    last = 0;

    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 250;

    constructor(
        injector: Injector,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _notifyService: NotifyService,
        private _commonLookupService: CommonLookupServiceProxy,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _httpClient: HttpClient,
        private _userServiceProxy: UserServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _router: Router,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private titleService: Title
    ) {
        super(injector);
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/Users/ImportLeadFromExcel';
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        this.titleService.setTitle(this.appSession.tenancyName + " |  Closed Leads");
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Closed Lead';
            log.section = 'Closed Lead';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.getLeads();
        });
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + 82 ;
        }

        else {
            this.testHeight = this.testHeight - 82 ;
        }
    }

    clear() {
        this.filterText = '';
        this.postCodeSuburbFilter = '';
        this.stateNameFilter = '';
        this.postCodePostalCode2Filter = '';
        this.leadSourceNameFilter = '';
        this.getLeads();
    }

    filtersuburbs(event): void {
        this._commonLookupService.getOnlySuburb(event.query).subscribe(output => {
            this.suburbSuggestions = output;
        });
    }

    getLeads(event?: LazyLoadEvent) {

        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._leadsServiceProxy.getAllForMyClosedLead(
            "",
            this.filterText,
            this.copanyNameFilter,
            this.emailFilter,
            this.phoneFilter,
            this.mobileFilter,
            this.addressFilter,
            this.requirementsFilter,
            this.postCodeSuburbFilter,
            this.stateNameFilter,
            this.streetNameFilter,
            this.postCodePostalCode2Filter,
            this.leadSourceNameFilter,
            undefined,
            //this.leadSubSourceNameFilter,
            this.leadStatusName,
            this.typeNameFilter,
            this.areaNameFilter,
            (this.leadStatusId == 0) ? undefined : this.leadStatusId,
            0,
            this.StartDate,
            this.EndDate,
            this.organizationUnit,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            0,
            "",
            undefined,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            this.shouldShow = false;
        });
    }

    filterCountries(event): void {
        this._commonLookupService.getAllLeadStatusForTableDropdown(event.query).subscribe(result => {
            this.allLeadStatus = result;
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }


    createLead(): void {
        this._router.navigate(['/app/main/leads/leads/createOrEdit']);
    }

    navigateToLeadDetail(leadid): void {
        this._router.navigate(['/app/main/closedlead/closedlead/viewclosedlead'], { queryParams: { LeadId: leadid } });
    }

    deleteLead(lead: LeadDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    // this._leadsServiceProxy.delete(lead.id)
                    //     .subscribe(() => {
                    //         this.reloadPage();
                    //         this.notify.success(this.l('SuccessfullyDeleted'));
                    //     });
                }
            }
        );
    }

    exportToExcel(excelorcsv): void {
        this.excelorcsvfile = excelorcsv;
        this._leadsServiceProxy.getClosedLeadsToExcel(
            '',
            this.filterText,
            this.copanyNameFilter,
            this.emailFilter,
            this.phoneFilter,
            this.mobileFilter,
            this.addressFilter,
            this.requirementsFilter,
            this.postCodeSuburbFilter,
            this.stateNameFilter,
            this.streetNameFilter,
            this.postCodePostalCode2Filter,
            this.leadSourceNameFilter,
            undefined,
            //this.leadSubSourceNameFilter,
            this.leadStatusName,
            this.typeNameFilter,
            this.areaNameFilter,
            (this.leadStatusId == 0) ? undefined : this.leadStatusId,
            0,
            this.StartDate,
            this.EndDate,
            this.organizationUnit,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            this.excelorcsvfile,
            undefined,
            undefined,
            undefined
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    uploadExcel(data: { files: File }): void {
        const formData: FormData = new FormData();
        const file = data.files[0];
        formData.append('file', file, file.name);

        this._httpClient
            .post<any>(this.uploadUrl, formData)
            .pipe(finalize(() => this.excelFileUpload.clear()))
            .subscribe(response => {
                if (response.success) {
                    this.notify.success(this.l('ImportLeadsProcessStart'));
                } else if (response.error != null) {
                    this.notify.error(this.l('ImportLeadUploadFailed'));
                }
            });
    }

    onUploadExcelError(): void {
        this.notify.error(this.l('ImportUsersUploadFailed'));
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Closed Lead';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Closed Lead';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }

}