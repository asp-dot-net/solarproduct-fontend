﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetHoldReasonForViewDto, GetServiceCategoryForViewDto, GetServiceTypeForViewDto, HoldReasonDto, ServiceCategoryDto, ServiceTypeDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
@Component({
    selector: 'viewserviceTypeModal',
    templateUrl: './view-serviceType-modal.component.html'
})
export class ViewServiceTypeModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetServiceTypeForViewDto;


    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
        this.item = new GetServiceTypeForViewDto();
        this.item.serviceTypes = new ServiceTypeDto();
    }

    show(item: GetServiceTypeForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Opened Service Type View : ' + this.item.serviceTypes.name;
        log.section = 'Service Type';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {            
         });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
