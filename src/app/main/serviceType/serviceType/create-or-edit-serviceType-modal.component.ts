﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { HoldReasonsServiceProxy, CreateOrEditHoldReasonDto, CreateOrEditServiceCategoryDto, ServiceCategorysServiceProxy, CreateOrEditServiceTypeDto, ServiceTypesServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';


@Component({
    selector: 'createOrEditserviceTypeModal',
    templateUrl: './create-or-edit-serviceType-modal.component.html'
})
export class CreateOrEditServiceTypeModalComponent extends AppComponentBase {
   
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    serviceTypes: CreateOrEditServiceTypeDto = new CreateOrEditServiceTypeDto();



    constructor(
        injector: Injector,
        private _serviceTypesServiceProxy: ServiceTypesServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }
    
    show(serviceTypesId?: number): void {
    

        if (!serviceTypesId) {
            this.serviceTypes = new CreateOrEditServiceTypeDto();
            this.serviceTypes.id = serviceTypesId;

            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Service Type';
            log.section = 'Service Type';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            
        } else {
            this._serviceTypesServiceProxy.getServiceTypeForEdit(serviceTypesId).subscribe(result => {
                this.serviceTypes = result.serviceTypes;

                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Service Type : ' + this.serviceTypes.name;
                log.section = 'Service Type';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        
    }
    onShown(): void {
        
        document.getElementById('ServiceType_ServiceType').focus();
    }
    save(): void {
            this.saving = true;
            this._serviceTypesServiceProxy.createOrEdit(this.serviceTypes)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.serviceTypes.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Service Type Updated : '+ this.serviceTypes.name;
                    log.section = 'Service Type';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Service Type Created : '+ this.serviceTypes.name;
                    log.section = 'Service Type';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
