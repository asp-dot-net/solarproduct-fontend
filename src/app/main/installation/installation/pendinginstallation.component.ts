import { Component, Injector, ViewEncapsulation, ViewChild, Input , HostListener} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JobsServiceProxy, JobDto, GetLeadForViewDto, LeadsServiceProxy, InstallationServiceProxy, LeadUsersLookupTableDto, CommonLookupDto, OrganizationUnitDto, UserServiceProxy, LeadStateLookupTableDto, JobHouseTypeLookupTableDto, JobRoofTypeLookupTableDto, JobPaymentOptionLookupTableDto, JobJobTypeLookupTableDto, CommonLookupServiceProxy, GetJobForViewDto, UserActivityLogServiceProxy, UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { EntityTypeHistoryModalComponent } from '@app/shared/common/entityHistory/entity-type-history-modal.component';
import * as _ from 'lodash';
import * as moment from 'moment';
// import { JobDetailModalComponent } from '@app/main/jobs/jobs/job-detail-model.component';
import { PendingInstallationDetailComponent } from './pendinginstallationdetail.component';
import { finalize } from 'rxjs/operators';
import { ViewApplicationModelComponent } from '@app/main/jobs/jobs/view-application-model/view-application-model.component';
// import { AddActivityModalComponent } from '@app/main/myleads/myleads/add-activity-model.component';
// import { PendingInstallationSmsEmailComponent } from './pendinginstallation-sms-email-model/pendinginstallatioin-smsemail.component';
// import { ViewMyLeadComponent } from '@app/main/myleads/myleads/view-mylead.component';
import { Title } from '@angular/platform-browser';
import { CommentModelComponent } from '@app/main/activitylog/comment-modal.component';
import { NotifyModelComponent } from '@app/main/activitylog/notify-modal.component';
import { ReminderModalComponent } from '@app/main/activitylog/reminder-modal.component';
import { SMSModelComponent } from '@app/main/activitylog/sms-modal.component';
import { ToDoModalComponent } from '@app/main/activitylog/todo-modal.component';

//import { JobsComponent } from './jobs.component';

@Component({
    templateUrl: './pendinginstallation.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class PendingInstallationComponent extends AppComponentBase {

    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    assignLead = 0;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };
    @ViewChild('viewApplicationModal', { static: true }) viewApplicationModal: ViewApplicationModelComponent;
    // @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    @ViewChild('entityTypeHistoryModal', { static: true }) entityTypeHistoryModal: EntityTypeHistoryModalComponent;

    @ViewChild('PendingInstallationDetail', { static: true }) PendingInstallationDetail: PendingInstallationDetailComponent;
    // @ViewChild('pendinginstallationTrackerSmsEmailModal', { static: true }) pendinginstallationTrackerSmsEmailModal: PendingInstallationSmsEmailComponent;

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    // @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
    
    @ViewChild('smsModel', { static: true }) smsModel: SMSModelComponent;
    // @ViewChild('emailModel', { static: true }) emailModel: EmailModelComponent;
    @ViewChild('notifyModel', { static: true }) notifyModel: NotifyModelComponent;
    @ViewChild('commentModel', { static: true }) commentModel: CommentModelComponent;
    @ViewChild('todoModal', { static: true }) todoModal: ToDoModalComponent;
    @ViewChild('ReminderModal', { static: true }) ReminderModal: ReminderModalComponent;
    

    ExpandedView: boolean = true;
    SelectedLeadId: number = 0;
    advancedFiltersAreShown = false;
    stausfilterSelect = 0;
    filterText = '';
    FiltersData = false;
    maxPanelOnFlatFilter: number;
    maxPanelOnFlatFilterEmpty: number;
    minPanelOnFlatFilter: number;
    minPanelOnFlatFilterEmpty: number;
    maxPanelOnPitchedFilter: number;
    maxPanelOnPitchedFilterEmpty: number;
    minPanelOnPitchedFilter: number;
    minPanelOnPitchedFilterEmpty: number;
    invertSuggestions: string[];
    panelSuggestions: string[];
    regPlanNoFilter = '';
    lotNumberFilter = '';
    peakMeterNoFilter = '';
    offPeakMeterFilter = '';
    enoughMeterSpaceFilter = -1;
    isSystemOffPeakFilter = -1;
    jobids = [];
    suburbFilter = '';
    stateFilter = '';
    jobTypeNameFilter = '';
    jobStatusNameFilter = '';
    roofTypeNameFilter = '';
    roofAngleNameFilter = '';
    elecDistributorNameFilter = '';
    leadCompanyNameFilter = '';
    elecRetailerNameFilter = '';
    paymentOptionNameFilter = '';
    depositOptionNameFilter = '';
    meterUpgradeNameFilter = '';
    meterPhaseNameFilter = '';
    promotionOfferNameFilter = '';
    houseTypeNameFilter = '';
    financeOptionNameFilter = '';
    tableRecords: any;
    OpenRecordId: number = 0;
    totalcount = 0;
    count = 0;
    stateNameFilter = "";
    dateFilterType = 'All';
    areaNameFilter = "";
    steetAddressFilter = "";
    postalcodefrom = "";
    postalcodeTo = "";
    housetypeId = 0;
    rooftypeid = 0;
    postalcode = "";
    jobstatusid = 0;
    panelmodel = "";
    invertmodel = "";
    paymenttypeid = 0;
    // startDate: moment.Moment;
    // endDate: moment.Moment;
    allUsers: CommonLookupDto[];
    allStates: LeadStateLookupTableDto[];
    allOrganizationUnits: OrganizationUnitDto[];
    jobTypes: JobJobTypeLookupTableDto[];
    _entityTypeFullName = 'TheSolarProduct.Jobs.Job';
    entityHistoryEnabled = false;
    organizationUnit = 0;
    organizationUnitlength: number = 0;
    status = 0;
    jobHouseTypes: JobHouseTypeLookupTableDto[];
    jobRoofTypes: JobRoofTypeLookupTableDto[];
    jobPaymentOptions: JobPaymentOptionLookupTableDto[];
    date = new Date();
    // public sampleDateRange: moment.Moment[] = [moment(this.date), moment().endOf('day')];
    // firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    // startDate = moment(this.firstDay);
    // endDate = moment().endOf('day');
    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);
    jobTypeId = 0;
    totalData = 0;
    totalDepRcvData = 0;
    totalActiveData = 0;
    totalJobBookedData = 0;
    jobAssignUser = 0;
    assignJob = 0;
    firstrowcount = 0;
    last = 0;

    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 330;
    toggle: boolean = true;

    filterName = 'JobNumber';
    orgCode = '';

    change() {
        this.toggle = !this.toggle;
    }
    
    constructor(
        injector: Injector,
        private _insallationServiceProxy: InstallationServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _router: Router,
        private _commonLookupService: CommonLookupServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _jobsServiceProxy: JobsServiceProxy,
        private _userServiceProxy: UserServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private titleService: Title
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        this.titleService.setTitle(this.appSession.tenancyName + " |  Job Assign");
        this.entityHistoryEnabled = this.setIsEntityHistoryEnabled();

        this._commonLookupService.getAllJobTypeForTableDropdown().subscribe(result => {
            this.jobTypes = result;
        });
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
        // this._jobsServiceProxy.getAllUsers().subscribe(result => {
        //     this.allUsers = result;
        // });
        
        this._jobsServiceProxy.getAllRoofTypeForTableDropdown().subscribe(result => {
            this.jobRoofTypes = result;
        });
        this._jobsServiceProxy.getAllHouseTypeForTableDropdown().subscribe(result => {
            this.jobHouseTypes = result;
        });

        this._commonLookupService.getAllPaymentOptionForTableDropdown().subscribe(result => {
            this.jobPaymentOptions = result;
        });
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Job Assign';
            log.section = 'Job Assign';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.orgCode = this.allOrganizationUnits[0].code;

            this.bindUser(this.organizationUnit);

            this.getJobs();
        });
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }
        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }

    bindUser(orgId: number): void{
        this.allUsers = [];
        this._commonLookupService.getAllUsersByRoleNameTableDropdown("Pre Installation", orgId).subscribe(result => {
            this.allUsers = result;
        });
    }

    private setIsEntityHistoryEnabled(): boolean {
        let customSettings = (abp as any).custom;
        return this.isGrantedAny('Pages.Administration.AuditLogs') && customSettings.EntityHistory && customSettings.EntityHistory.isEnabled && _.filter(customSettings.EntityHistory.enabledEntities, entityType => entityType === this._entityTypeFullName).length === 1;
    }

    changeOrgCode() {
        this.orgCode = this.allOrganizationUnits.find(x => x.id == this.organizationUnit).code;
    }

    filterPanelModel(event): void {
        this._jobsServiceProxy.getpanelmodel(event.query).subscribe(output => {
            this.panelSuggestions = output;
        });
    }

    filterInvertModel(event): void {
        this._jobsServiceProxy.getinvertmodel(event.query).subscribe(output => {
            this.invertSuggestions = output;
        });
    }
    getJobs(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        // if (this.sampleDateRange != null) {
        //     this.StartDate = this.sampleDateRange[0];
        //     this.EndDate = this.sampleDateRange[1];
        // } else {
        //     this.StartDate = null;
        //     this.EndDate = null;
        // }

        this.primengTableHelper.showLoadingIndicator();
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._insallationServiceProxy.getPendingInstallationList(
            this.filterName,
            this.organizationUnit,
            this.status,
            filterText_,
            this.stateFilter,
            this.steetAddressFilter,
            this.jobTypeNameFilter,
            this.jobStatusNameFilter,
            this.housetypeId,
            this.rooftypeid,
            this.jobstatusid,
            this.panelmodel,
            this.invertmodel,
            this.paymenttypeid,
            this.areaNameFilter,
            this.dateFilterType,
            this.postalcodefrom,
            this.postalcodeTo,
            this.startDate,
            this.endDate,
            this.jobTypeId,
            this.jobAssignUser,
            this.assignJob,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            debugger;
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.tableRecords = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            if (result.totalCount > 0) {
                this.getLeadSummaryCount();
            } else {
                this.totalData = 0;
                this.totalDepRcvData = 0;
                this.totalActiveData = 0;
                this.totalJobBookedData = 0;
            }
            this.primengTableHelper.hideLoadingIndicator();
            this.shouldShow = false;

        }, err => { this.primengTableHelper.hideLoadingIndicator() });
    }
    getLeadSummaryCount() {
        debugger;
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._insallationServiceProxy.getPendingInstallationcount(
            this.filterName,
            this.organizationUnit,
            this.status,
            filterText_,
            this.stateFilter,
            this.steetAddressFilter,
            this.jobTypeNameFilter,
            this.jobStatusNameFilter,
            this.housetypeId,
            this.rooftypeid,
            this.jobstatusid,
            this.panelmodel,
            this.invertmodel,
            this.paymenttypeid,
            this.areaNameFilter,
            this.dateFilterType,
            this.postalcodefrom,
            this.postalcodeTo,
            this.startDate,
            this.endDate,
            this.jobTypeId,
            this.jobAssignUser,
            this.assignJob,
            undefined,
            undefined,
            undefined,
        ).subscribe(result => {
            debugger;
            if (result) {
                this.totalData = result.totalData;
                this.totalDepRcvData = result.totalDepRcvData;
                this.totalActiveData = result.totalActiveData;
                this.totalJobBookedData = result.totalJobBookedData;

            }
        });
    }
    reloadPage($event): void {
        this.paginator.changePage(this.paginator.getPage());
    }


    setOpenRecord(id) {
        if (id === this.OpenRecordId) { this.OpenRecordId = 0; return; }
        this.OpenRecordId = id;
    }

    showHistory(job: JobDto): void {
        this.entityTypeHistoryModal.show({
            entityId: job.id.toString(),
            entityTypeFullName: this._entityTypeFullName,
            entityTypeDescription: ''
        });
    }
    checkAll(ev) {

        this.tableRecords.forEach(x => x.job.isSelected = ev.target.checked);
        this.oncheckboxCheck();
        /// this.totalcount = this.tableRecords.forEach(x => x.lead.isSelected = ev.target.checked);
    }

    oncheckboxCheck() {

        this.count = 0;
        this.tableRecords.forEach(item => {
            if (item.job.isSelected == true) {
                this.count = this.count + 1;
            }
        })
    }

    isAllChecked() {
        if (this.tableRecords)
            return this.tableRecords.every(_ => _.job.isSelected);
    }

    submit(): void {
        let selectedids = [];
        // this.saving = true;
        this.primengTableHelper.records.forEach(function (job) {
            if (job.job.isSelected) {
                selectedids.push(job.job.id);
            }
        });
        if (selectedids.length == 0) {
            this.notify.warn(this.l('PleaseSelectRecord'));
            return;
        }
        if (this.assignLead == 0) {
            this.notify.warn(this.l('PleaseSelectUser'));
            return;
        }
        this._jobsServiceProxy.updateBookingManagerID(this.assignLead, selectedids)
            .pipe(finalize(() => { }))
            .subscribe(() => {
                this.reloadPage(null);
                this.notify.info(this.l('AssignedSuccessfully'));
            });
    }

    expandGrid() {
        this.ExpandedView = true;
    }

    navigateToLeadDetail(leadid): void {
        // this.ExpandedView = !this.ExpandedView;
        // this.ExpandedView = false;
        // this.SelectedLeadId = leadid;
        // this.viewLeadDetail.showDetail(leadid, null, 11);
    }



    // exportToExcel(): void {
    //     this._jobsServiceProxy.getJobsToExcel(
    //     this.filterText,
    //         this.maxPanelOnFlatFilter == null ? this.maxPanelOnFlatFilterEmpty: this.maxPanelOnFlatFilter,
    //         this.minPanelOnFlatFilter == null ? this.minPanelOnFlatFilterEmpty: this.minPanelOnFlatFilter,
    //         this.maxPanelOnPitchedFilter == null ? this.maxPanelOnPitchedFilterEmpty: this.maxPanelOnPitchedFilter,
    //         this.minPanelOnPitchedFilter == null ? this.minPanelOnPitchedFilterEmpty: this.minPanelOnPitchedFilter,
    //         this.regPlanNoFilter,
    //         this.lotNumberFilter,
    //         this.peakMeterNoFilter,
    //         this.offPeakMeterFilter,
    //         this.enoughMeterSpaceFilter,
    //         this.isSystemOffPeakFilter,
    //         this.suburbFilter,
    //         this.stateFilter,
    //         this.jobTypeNameFilter,
    //         this.jobStatusNameFilter,
    //         this.roofTypeNameFilter,
    //         this.roofAngleNameFilter,
    //         this.elecDistributorNameFilter,
    //         this.leadCompanyNameFilter,
    //         this.elecRetailerNameFilter,
    //         this.paymentOptionNameFilter,
    //         this.depositOptionNameFilter,
    //         this.meterUpgradeNameFilter,
    //         this.meterPhaseNameFilter,
    //         this.promotionOfferNameFilter,
    //         this.houseTypeNameFilter,
    //         this.financeOptionNameFilter,
    //     )
    //     .subscribe(result => {
    //         this._fileDownloadService.downloadTempFile(result);
    //      });
    // }

    getToolTipData(records: GetJobForViewDto): string {
        return records.job.address + ', ' + records.job.suburb + ', ' + records.job.state + '-' + records.job.postalCode;
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Job Assign';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Job Assign';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }

}
