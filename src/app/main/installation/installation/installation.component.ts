﻿import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { ActivatedRoute , Router} from '@angular/router';
import { AbpSessionService, NotifyService, PermissionCheckerService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLookupDto, CommonLookupServiceProxy, InstallationCalendarDto, InstallationServiceProxy, InstallerServiceProxy, LeadsServiceProxy, LeadStateLookupTableDto, OrganizationUnitDto, TokenAuthServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy, UserServiceProxy } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { PendingInstallationSmsEmailComponent } from './pendinginstallation-sms-email-model/pendinginstallatioin-smsemail.component';
import { ViewMyLeadComponent } from '@app/main/myleads/myleads/view-mylead.component';
import * as moment from 'moment';
import { Title } from '@angular/platform-browser';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './installation.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})

export class InstallationComponent extends AppComponentBase {
    advancedFiltersAreShown = false;
    filterText = '';
    cancelReasonNameFilter = '';
    installerId = 0;
    calendaruserid = 0;
    // date= moment(new Date(), 'DD/MM/YYYY');
    date = moment().startOf('isoWeek').toDate();
    recordStartDate = moment(this.date, 'DD/MM/YYYY');
    InstallationList: InstallationCalendarDto[];
    InstallerList: CommonLookupDto[];
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnit = 0;
    organizationUnitlength: number = 0;
    areaNameFilter = '';
    stateNameFilter = '';
    allStates: LeadStateLookupTableDto[];
    SelectedLeadId: number = 0;
    OpenRecordId: number = 0;
    ExpandedView: boolean = true;
    toggle: boolean = true;

    @ViewChild('pendinginstallationTrackerSmsEmailModal', { static: true }) pendinginstallationTrackerSmsEmailModal: PendingInstallationSmsEmailComponent;
    @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;

    constructor(
        injector: Injector,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _installationServiceProxy: InstallationServiceProxy,
        private _permissionChecker: PermissionCheckerService,
        private _installerServiceProxy: InstallerServiceProxy,
        private _sessionService: AbpSessionService,
        private _activatedRoute: ActivatedRoute,
        private _userServiceProxy : UserServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _leadsServiceProxy : LeadsServiceProxy,
        private titleService: Title,
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Installation");
    }
    ngOnInit(): void {
        this._leadsServiceProxy.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
        if (this._permissionChecker.isGranted('Pages.Tenant.InstallerManager.InstallerCalendar')) {
            this.getInstaller();
        }
        else
        {
            this.calendaruserid=this._sessionService.userId;
        }
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Installation';
            log.section = 'Installation';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.getInstallations();
        });
       
    }

    change() {
        this.toggle = !this.toggle;
      }

    getInstaller(): void {
        this._installerServiceProxy.getAllInstallers(this.organizationUnit).subscribe(result => {
          this.InstallerList = result;
      });
    };

    previousDate(): void {
        this.recordStartDate = moment(this.recordStartDate).add(-7, 'days');
        this.getInstallations();
    };

    nextDate(): void {
        this.recordStartDate = moment(this.recordStartDate).add(7, 'days');
        this.getInstallations();
    };

    setOpenRecord(id) {
        if (id === this.OpenRecordId) { this.OpenRecordId = 0; return; }
        this.OpenRecordId = id;
    }

    getInstallations(): void {
        this.showMainSpinner();
        if (this._permissionChecker.isGranted('Pages.Tenant.InstallerManager.InstallerCalendar')) {
          this.calendaruserid=this.installerId;
        }
        this._installationServiceProxy.getInstallationCalendar(this.calendaruserid,this.recordStartDate, this.organizationUnit,this.areaNameFilter,this.stateNameFilter).subscribe(result => {
          this.InstallationList=result;
          console.log(this.InstallationList);
          this.hideMainSpinner();
        }, e => {
            this.hideMainSpinner();
        });
    }

    navigateToLeadDetail(leadid): void {
        this.ExpandedView = !this.ExpandedView;
        this.ExpandedView = false;
        this.SelectedLeadId = leadid;
        this.viewLeadDetail.showDetail(leadid, null, 11,null,'Installation');
    }

    installerSearchResult: any [];
    filterInstaller(event): void {
        this.installerSearchResult = Object.assign([], this.InstallerList).filter(
        item => item.displayName.toLowerCase().indexOf(event.query.toLowerCase()) > -1
        )
    }

    selectInstaller(event): void {
        this.installerId = event.id;
        this.getInstallations();
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Installation';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    
}