import { Component, ElementRef, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLookupDto, EmailTemplateServiceProxy, GetJobForEditOutput, GetLeadForViewDto, JobsServiceProxy, LeadDto, LeadsServiceProxy, SmsEmailDto, SmsTemplatesServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { EmailEditorComponent } from 'angular-email-editor';
import * as _ from 'lodash';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'pendinginstallationTrackerSmsEmailModal',
  templateUrl: './pendinginstallatioin-smsemail.component.html',
})
export class PendingInstallationSmsEmailComponent extends AppComponentBase implements OnInit {
  @ViewChild("myNameElem") myNameElem: ElementRef;
  @ViewChild('addModal', { static: true }) modal: ModalDirective;
  ///@ViewChild('viewLeadDetail', {static: true}) viewLeadDetail: ViewMyLeadComponent;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  saving = false;
  id = 0;
  allEmailTemplates: CommonLookupDto[];
  allSMSTemplates: CommonLookupDto[];
  installer: GetJobForEditOutput;
  emailData = '';
  smsData = '';
  emailTemplate: number;
  uploadUrl: string;
  uploadedFiles: any[] = [];
  myDate = new Date();
  activityType: number;
  item: GetLeadForViewDto;
  activityLog: SmsEmailDto = new SmsEmailDto();
  activityName = "";
  total = 0;
  credit = 0;
  customeTagsId = 0;
  role: string = '';
  leadCompanyName: any;
  subject = '';
  types = 1;
  email = "";
  mobile = "";
  ccbox = false;
  bccbox = false;
  // viewLeadDetail: ViewMyLeadComponent;
  constructor(
    injector: Injector,
    private _leadsServiceProxy: LeadsServiceProxy,
    private _emailTemplateServiceProxy: EmailTemplateServiceProxy,
    private _smsTemplateServiceProxy: SmsTemplatesServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _router: Router,
    private _jobsServiceProxy: JobsServiceProxy,
  ) {
    super(injector);
    this.item = new GetLeadForViewDto();
    this.item.lead = new LeadDto();
  }

  @ViewChild(EmailEditorComponent)
  private emailEditor: EmailEditorComponent;

  ///private viewLeadDetail: ViewMyLeadComponent;

  ngOnInit(): void {
    this.activityLog.emailTemplateId = 0;
    this.activityLog.smsTemplateId = 0;
    this.activityLog.customeTagsId = 0;
    this.activityLog.subject = '';

  }

 sectionName = ''; 
 show(leadId: number, id: number, trackerid?: number,section = ''): void {
    this.sectionName = section;
    let log = new UserActivityLogDto();
    log.actionId = 79;
    log.actionNote ='Open ' + (id == 1 ? 'SMS' : 'Email');
    log.section = section;
    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {
    }); 
this.showMainSpinner();
    this.activityLog.emailTemplateId = 0;
    this.activityLog.smsTemplateId = 0;
    this.activityLog.customeTagsId = 0;
    this.activityLog.subject = '';
    this.activityLog.body = "";
    this.activityLog.trackerId = trackerid;

    if (id == 1) {
      this.id = id;
      this.activityType = 1;
    }
    else if (id == 2) {
      this.id = id;
      this.activityType = 2;
    }
    this._leadsServiceProxy.getLeadForView(leadId,0).subscribe(result => {
      this.item = result;
      this.leadCompanyName = result.lead.companyName;
      this.mobile = result.lead.mobile;
      this.email = result.lead.email;
      this.selection();
      this.modal.show();
      this.hideMainSpinner();
    }, e => { this.hideMainSpinner(); });
    this._jobsServiceProxy.getJobdetailbylead(leadId).subscribe(result => {
      debugger;
      this.installer = result;
    });
    this._leadsServiceProxy.getallEmailTemplates(leadId).subscribe(result => {
      this.allEmailTemplates = result;
    });

    this._leadsServiceProxy.getallSMSTemplates(leadId).subscribe(result => {
      this.allSMSTemplates = result;
    });

    this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
      this.role = result;
    });

  }

  selection(): void {

    this.activityLog.body = '';
    this.activityLog.emailTemplateId = 0;
    this.activityLog.customeTagsId = 0;
    if (this.activityType == 1) {
      this.activityName = "SMS To ";
    }
    else if (this.activityType == 2) {
      this.activityName = "Email To ";
    }
  }

  ontypechange(event) {
    debugger;
    if (this.activityType == 1) {
      if (event.target.value == 1) {
        this.mobile = this.item.lead.mobile;
      } else {
        if (event.target.value == 2 && this.installer.installerID != null && this.installer.installerID != 0) {
          if (this.installer.installerMobile != null) {
            this.mobile = this.installer.installerMobile;
          } else {
            this.notify.warn('Installer MobileNumber Not Found');
            this.mobile = "";
          }

        } else {
          this.notify.warn('Installer Not Found');
          this.mobile = "";
        }
      }
    } else {
      if (event.target.value == 1) {
        this.email = this.item.lead.email;
      } else {
        if (event.target.value == 2 && this.installer.installerID != null && this.installer.installerID != 0) {
          if (this.installer.installerEmail != null) {
            this.email = this.installer.installerEmail;
          } else {
            this.notify.warn('Installer Email Not Found');
            this.email = "";
          }
        } else {
          this.notify.warn('No Installer Found');
          this.email = "";
        }
      }

    }
  }
  save(): void {
    debugger;
    this.activityLog.leadId = this.item.lead.id;
    this.email = this.installer.installerMobile;
    this.mobile =  this.installer.installerEmail;
    if (this.activityType == 1) {
      if (this.item.lead.mobile != null && this.item.lead.mobile != "") {
        this.saving = true;
        if (this.role != 'Admin') {
          if (this.total > 320) {
            this.notify.warn(this.l('You Can Not Add more than 320 characters'));
            this.saving = false;
          } else {
            this._jobsServiceProxy.sendSms(this.activityLog)
              .pipe(finalize(() => { this.saving = false; }))
              .subscribe(() => {
                let log = new UserActivityLogDto();
                log.actionId = 6;
                log.actionNote ='SMS Sent';
                log.section = this.sectionName;
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
                this.modal.hide();
                this.notify.info(this.l('SmsSendSuccessfully'));
                this.modalSave.emit(null);
                this.activityLog.body = "";
                this.activityLog.emailTemplateId = 0;
                this.activityLog.smsTemplateId = 0;
                this.activityLog.customeTagsId = 0;
                this.activityLog.subject = '';
              });
          }
        }
        else {
          this._jobsServiceProxy.sendSms(this.activityLog)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
              let log = new UserActivityLogDto();
                log.actionId = 6;
                log.actionNote ='SMS Sent';
                log.section = this.sectionName;
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
              this.modal.hide();
              this.notify.info(this.l('SmsSendSuccessfully'));
              this.modalSave.emit(null);
              this.activityLog.body = "";
              this.activityLog.emailTemplateId = 0;
              this.activityLog.smsTemplateId = 0;
              this.activityLog.customeTagsId = 0;
              this.activityLog.subject = '';
            });
        }
      }
      else {
        this.notify.warn(this.l('Mobile Number Not Found'));
      }
    }
    else {
      if (this.item.lead.email != null && this.item.lead.email != "") {
        this.saving = true;
        this._jobsServiceProxy.sendEmail(this.activityLog)
          .pipe(finalize(() => { this.saving = false; }))
          .subscribe(() => {
            let log = new UserActivityLogDto();
            log.actionId = 7;
            log.actionNote ='Email Sent';
            log.section = this.sectionName;
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
            this.notify.info(this.l('EmailSendSuccessfully'));
            this.modal.hide();
            this.modalSave.emit(null);
            this.activityLog.body = "";
            this.activityLog.emailTemplateId = 0;
            this.activityLog.smsTemplateId = 0;
            this.activityLog.customeTagsId = 0;
            this.activityLog.subject = '';
          });
      }
      else {
        this.notify.warn(this.l('Email Not Found'));
      }
    }
  }

  countCharcters(): void {

    if (this.role != 'Admin') {
      this.total = this.activityLog.body.length;
      this.credit = Math.ceil(this.total / 160);
      if (this.total > 320) {
        this.notify.warn(this.l('You Can Not Add more than 320 characters'));
      }
    }
    else {
      this.total = this.activityLog.body.length;
      this.credit = Math.ceil(this.total / 160);
    }


  }

  close(): void {
    this.modal.hide();
  }
  opencc(): void {
    this.ccbox = !this.ccbox;
  }
  openbcc(): void {
    this.bccbox = !this.bccbox;
  }

  onTagChange(event): void {

    const id = event.target.value;
    if (id == 1) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Customer.Name}}";
      } else {
        this.activityLog.body = "{{Customer.Name}}";
      }

    } else if (id == 2) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Customer.Mobile}}";
      } else {
        this.activityLog.body = "{{Customer.Mobile}}";
      }
    } else if (id == 3) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Customer.Phone}}";
      } else {
        this.activityLog.body = "{{Customer.Phone}}";
      }
    } else if (id == 4) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Customer.Email}}";
      } else {
        this.activityLog.body = "{{Customer.Email}}";
      }
    } else if (id == 5) {

      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Customer.Address}}";
      } else {
        this.activityLog.body = "{{Customer.Address}}";
      }
    } else if (id == 6) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Sales.Name}}";
      } else {
        this.activityLog.body = "{{Sales.Name}}";
      }
    } else if (id == 7) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Sales.Mobile}}";
      } else {
        this.activityLog.body = "{{Sales.Mobile}}";
      }
    } else if (id == 8) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Sales.Email}}";
      } else {
        this.activityLog.body = "{{Sales.Email}}";
      }
    }
    else if (id == 9) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Quote.ProjectNo}}";
      } else {
        this.activityLog.body = "{{Quote.ProjectNo}}";
      }
    }
    else if (id == 10) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Quote.SystemCapacity}}";
      } else {
        this.activityLog.body = "{{Quote.SystemCapacity}}";
      }
    }
    else if (id == 11) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Quote.AllproductItem}}";
      } else {
        this.activityLog.body = "{{Quote.AllproductItem}}";
      }
    }
    else if (id == 12) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Quote.TotalQuoteprice}}";
      } else {
        this.activityLog.body = "{{Quote.TotalQuoteprice}}";
      }
    }
    else if (id == 13) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Quote.InstallationDate}}";
      } else {
        this.activityLog.body = "{{Quote.InstallationDate}}";
      }
    }
    else if (id == 14) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Quote.InstallerName}}";
      } else {
        this.activityLog.body = "{{Quote.InstallerName}}";
      }
    }
  }


  saveDesign() {
    if (this.activityLog.emailTemplateId == 0) {
      this.saving = true;
      const emailHTML = this.activityLog.body;
      this.setHTML(emailHTML)
    }
    else {
      this.saving = true;
      this.emailEditor.editor.exportHtml((data) =>
        this.setHTML(data.html)
      );
    }
  }

  setHTML(emailHTML) {
    let htmlTemplate = this.getEmailTemplate(emailHTML);
    this.activityLog.body = htmlTemplate;
    this.save();
  }

  getEmailTemplate(emailHTML) {
    //let myTemplateStr = "Hello {{user.name}}, you are {{user.age.years}} years old";
    let myTemplateStr = emailHTML;
    let addressValue = this.item.lead.address + " " + this.item.lead.suburb + ", " + this.item.lead.state + "-" + this.item.lead.postCode;
    let myVariables = {
      Customer: {
        Name: this.item.lead.companyName,
        Address: addressValue,
        Mobile: this.item.lead.mobile,
        Email: this.item.lead.email,
        Phone: this.item.lead.phone,
        SalesRep: this.item.currentAssignUserName
      },
      Sales: {
        Name: this.item.currentAssignUserName,
        Mobile: this.item.currentAssignUserMobile,
        Email: this.item.currentAssignUserEmail
      },
      Quote: {
        ProjectNo: this.item.jobNumber,
        SystemCapacity: this.item.systemCapacity != null ? this.item.systemCapacity + " " + 'KW' : this.item.systemCapacity,
        AllproductItem: this.item.qunityAndModelList,
        TotalQuoteprice: this.item.totalQuotaion,
        InstallationDate: this.item.installationDate,
        InstallerName: this.item.installerName,
      }
    }

    // use custom delimiter {{ }}
    _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;

    // interpolate
    let compiled = _.template(myTemplateStr);
    let myTemplateCompiled = compiled(myVariables);
    return myTemplateCompiled;
  }

  editorLoaded() {

    this.activityLog.body = "";
    if ((this.activityLog.emailTemplateId != 0 && this.activityLog.emailTemplateId !== null && this.activityLog.emailTemplateId !== undefined)) {
      this._jobsServiceProxy.getEmailTemplateForEditForEmail(this.activityLog.emailTemplateId).subscribe(result => {
        this.activityLog.subject = result.emailTemplate.subject;
        this.emailData = result.emailTemplate.body;
        if (this.emailData != "") {
          this.emailData = this.getEmailTemplate(this.emailData);
          this.emailEditor.editor.loadDesign(JSON.parse(this.emailData));
        }
      });
    }

  }

  smseditorLoaded() {

    this.activityLog.body = "";
    if ((this.activityLog.smsTemplateId != 0 && this.activityLog.smsTemplateId !== null && this.activityLog.smsTemplateId !== undefined)) {
      this._jobsServiceProxy.getSmsTemplateForEditForSms(this.activityLog.smsTemplateId).subscribe(result => {
        this.smsData = result.smsTemplate.text;
        if (this.smsData != "") {
          this.setsmsHTML(this.smsData)
        }
      });
    }

  }


  setsmsHTML(smsHTML) {
    let htmlTemplate = this.getsmsTemplate(smsHTML);
    this.activityLog.body = htmlTemplate;
    this.countCharcters();
  }

  getsmsTemplate(smsHTML) {
    //let myTemplateStr = "Hello {{user.name}}, you are {{user.age.years}} years old";
    let myTemplateStr = smsHTML;
    let addressValue = this.item.lead.address + " " + this.item.lead.suburb + ", " + this.item.lead.state + "-" + this.item.lead.postCode;
    let myVariables = {
      Customer: {
        Name: this.item.lead.companyName,
        Address: addressValue,
        Mobile: this.item.lead.mobile,
        Email: this.item.lead.email,
        Phone: this.item.lead.phone,
        SalesRep: this.item.currentAssignUserName
      },
      Sales: {
        Name: this.item.currentAssignUserName,
        Mobile: this.item.currentAssignUserMobile,
        Email: this.item.currentAssignUserEmail
      },
      Quote: {
        ProjectNo: this.item.jobNumber,
        SystemCapacity: this.item.systemCapacity != null ? this.item.systemCapacity + " " + 'KW' : this.item.systemCapacity,
        AllproductItem: this.item.qunityAndModelList,
        TotalQuoteprice: this.item.totalQuotaion,
        InstallationDate: this.item.installationDate,
        InstallerName: this.item.installerName,
      }
    }

    // use custom delimiter {{ }}
    _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;

    // interpolate
    let compiled = _.template(myTemplateStr);
    let myTemplateCompiled = compiled(myVariables);
    return myTemplateCompiled;
  }
}




