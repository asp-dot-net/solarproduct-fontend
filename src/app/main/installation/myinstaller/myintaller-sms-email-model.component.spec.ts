import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MyInstallerSmsEmailModelComponent } from './myintaller-sms-email-model.component';


describe('MyInstallerSmsEmailModelComponent', () => {
  let component: MyInstallerSmsEmailModelComponent;
  let fixture: ComponentFixture<MyInstallerSmsEmailModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyInstallerSmsEmailModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyInstallerSmsEmailModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
