﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit, ElementRef, Directive, Optional } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { MyInstallerActivityHistoryDto, UserActivityLogDto, UserActivityLogServiceProxy, UserServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AgmCoreModule } from '@agm/core';
import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';
import PlaceResult = google.maps.places.PlaceResult;
import { Appearance, GermanAddress, Location } from '@angular-material-extensions/google-maps-autocomplete';




@Component({
    selector: 'myinstalleractivityloghistory',
    templateUrl: './myinstaller-activity-log-history.component.html',
})
export class MyinstallerActivityLogHistoryComponent extends AppComponentBase {
    @ViewChild("myNameElem") myNameElem: ElementRef;
    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @ViewChild('SampleDatePicker', { static: true }) sampleDatePicker: ElementRef;


    leadtrackerHistory: MyInstallerActivityHistoryDto[];
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _el: ElementRef,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _userServiceProxy: UserServiceProxy

    ) {
        super(injector);
        this._el.nativeElement.setAttribute('autocomplete', 'off');
        this._el.nativeElement.setAttribute('autocorrect', 'off');
        this._el.nativeElement.setAttribute('autocapitalize', 'none');
        this._el.nativeElement.setAttribute('spellcheck', 'false');
    }

    show(leadId?: number, leadorjob?: number,section = ''): void {
        let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open Installer Detail Log History';
                log.section = section; 
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
        debugger
        if (leadorjob == 1) {
            this._userServiceProxy.getMyInstallerActivityLogHistory(leadId, 2, 0, false, false, 0, true, 0).subscribe(result => {
                this.leadtrackerHistory = result;
                this.modal.show();
            });
        }
        if (leadorjob == 2) {
            this._userServiceProxy.getMyInstallerActivityLogHistory(leadId, 2, 0, false, false, 0, true, 0).subscribe(result => {
                this.leadtrackerHistory = result;
                this.modal.show();
            });
        }
    }
    close(): void {

        this.modal.hide();
    }
}
