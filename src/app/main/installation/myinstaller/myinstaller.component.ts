import { Component, Injector, ViewChild, ViewEncapsulation, AfterViewInit, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppConsts } from '@shared/AppConsts';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    EntityDtoOfInt64,
    UserListDto,
    UserServiceProxy,
    PermissionServiceProxy, FlatPermissionDto, OrganizationUnitDto, CommonLookupServiceProxy, LeadStateLookupTableDto, JobJobTypeLookupTableDto, LeadSourceLookupTableDto, JobStatusTableDto,
    UserActivityLogServiceProxy,
    UserActivityLogDto
    // , InstallerDetailsServiceProxy, CreateOrEditInstallerDetailDto
} from '@shared/service-proxies/service-proxies';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { LazyLoadEvent } from 'primeng/public_api';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { HttpClient } from '@angular/common/http';
import { FileUpload } from 'primeng/fileupload';
import { finalize } from 'rxjs/operators';
import { LocalStorageService } from '@shared/utils/local-storage.service';
import { FileUploader } from 'ng2-file-upload';
import { Title } from '@angular/platform-browser';
import * as moment from 'moment';
import { CreateOrEditMyInstallerModalComponent } from './create-or-edit-myinstaller-modal.component';
import { InstallerUploadDocComponent } from './myintaller-uploadDoc';
import { AddMyInstallerActivityModalComponent } from './add-myinstaller-activity-model.component';
import { MyInstallerSmsEmailModelComponent } from './myintaller-sms-email-model.component';
import { MyinstallerDetailComponent } from './myinstaller-detail-modal.component';
import { CreateOrEditPriceListModalComponent } from './create-or-edit-PriceList-modal.component';
import { ViewMyinstallerComponent } from './view-myintaller.component';
// import { OrganizationDocComponent } from './organization-doc.component';

@Component({
    selector: 'app-myinstaller',
    templateUrl: './myinstaller.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class MyinstallerComponent extends AppComponentBase implements AfterViewInit {
    show: boolean = true;
    showchild: boolean = true;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };


    @ViewChild('createOrEditMyInstallerModal', { static: true }) createOrEditMyInstallerModal: CreateOrEditMyInstallerModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('ExcelFileUpload', { static: false }) excelFileUpload: FileUpload;
    // @ViewChild('organizationDoc', { static: true }) organizationDoc: OrganizationDocComponent;
    @ViewChild('installerUploadDoc', { static: true }) installerUploadDoc: InstallerUploadDocComponent;
    @ViewChild('addmyinstalleractivity', { static: true }) addmyinstalleractivity: AddMyInstallerActivityModalComponent;
    @ViewChild('myInstallerSmsEmailModel', { static: true }) myInstallerSmsEmailModel: MyInstallerSmsEmailModelComponent;
    @ViewChild('myInstallerDetailModal', { static: true }) myInstallerDetailModal: MyinstallerDetailComponent;
    @ViewChild('createOrEditPriceListModal', { static: true }) createOrEditPriceListModal: CreateOrEditPriceListModalComponent;
    @ViewChild('viewmyInstallerDetail', { static: true }) viewmyInstallerDetail: ViewMyinstallerComponent;
    uploadUrl: string;
    //Filters
    allOrganizationUnits: OrganizationUnitDto[];
    advancedFiltersAreShown = false;
    filterText = '';
    ismyinstalleruser = true;
    role = '';
    organizationUnit = '';
    onlyLockedUsers = false;
    public fileupload: FileUploader;
    firstrowcount = 0;
    last = 0;
    shouldShow: boolean = false;
    addressFilter = "";
    suburb = '';
    stateFilter: number = 0;
    postalcodefrom: number = 0;
    postalcodeTo: number = 0;
    jobTypeNameFilter :number = 0;
    areaNameFilter: string = '';
    allStates: LeadStateLookupTableDto[];
    jobTypes: JobJobTypeLookupTableDto[];

    date = new Date();
    // firstDay = new Date(this.date.getFullYear(), 0, 1);
    // startDate = moment(this.firstDay);
    // endDate = moment().endOf('day');
    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);
    allLeadSources: LeadSourceLookupTableDto[];
    alljobstatus: JobStatusTableDto[];
    organizationUnitlength: number = 0;
    // leadSourceIdFilter = [];
    // jobStatusIDFilter: [];
    organizationUnitid = 0;
    ExpandedView: boolean = true;
    SelectedId: number = 0;
    isapprove=false;

    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 250;
    toggle: boolean = true;
    filterName = 'Name';

    change() {
        this.toggle = !this.toggle;
    }

    dateType= "CreationDate";
    constructor(
        injector: Injector,
        private _userServiceProxy: UserServiceProxy,
        // private _installerDetailsServiceProxy: InstallerDetailsServiceProxy ,
        private _commonLookupService: CommonLookupServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _activatedRoute: ActivatedRoute,
        private _httpClient: HttpClient,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _localStorageService: LocalStorageService,
        private _router: Router,
        private titleService: Title
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  My Installer");
        this.filterText = this._activatedRoute.snapshot.queryParams['filterText'] || '';
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/Users/ImportFromExcel';
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
        });
    }

    ngAfterViewInit(): void {
        this.primengTableHelper.adjustScroll(this.dataTable);
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        this._commonLookupService.getAllLeadSourceDropdown().subscribe(result => {
            this.allLeadSources = result;
        });
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
        this._commonLookupService.getAllJobStatusTableDropdown().subscribe(result => {
            this.alljobstatus = result;
        });
        this._commonLookupService.getAllJobTypeForTableDropdown().subscribe(result => {
            this.jobTypes = result;
        });
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open My Installer';
            log.section = 'My Installer';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnitid = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            //this.getUsers();
            this.onchangeOrganization(this.organizationUnitid);
        });
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    
    getUsers(event?: LazyLoadEvent) {

        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._userServiceProxy.getInstaller(
            this.filterName,
            this.filterText,
            undefined,
            this.role !== '' ? parseInt(this.role) : undefined,
            "Installer",
            this.organizationUnit !== '' ? parseInt(this.organizationUnit) : undefined,
            this.onlyLockedUsers,
            this.ismyinstalleruser,
            this.isapprove,
            this.suburb,
            this.stateFilter,
            this.postalcodefrom,
            this.postalcodeTo,
            this.jobTypeNameFilter,
            this.areaNameFilter,
            this.dateType,
            this.startDate,
            this.endDate,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getMaxResultCount(this.paginator, event),
            this.primengTableHelper.getSkipCount(this.paginator, event)
        ).pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator())).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
        }, e => {
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    unlockUser(record): void {
        this._userServiceProxy.unlockSalesRep(new EntityDtoOfInt64({ id: record.id })).subscribe(() => {
            this.notify.success(this.l('UnlockedTheUser', record.userName));
        });
    }

    getRolesAsString(roles): string {
        let roleNames = '';

        for (let j = 0; j < roles.length; j++) {
            if (roleNames.length) {
                roleNames = roleNames + ', ';
            }

            roleNames = roleNames + roles[j].roleName;
        }

        return roleNames;
    }

    reloadPage($event): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    uploadExcel(data: { files: File }): void {
        const formData: FormData = new FormData();
        const file = data.files[0];
        formData.append('file', file, file.name);

        this._httpClient
            .post<any>(this.uploadUrl, formData)
            .pipe(finalize(() => this.excelFileUpload.clear()))
            .subscribe(response => {
                if (response.success) {
                    this.notify.success(this.l('ImportUsersProcessStart'));
                } else if (response.error != null) {
                    this.notify.error(this.l('ImportUsersUploadFailed'));
                }
            });
    }

    

    onUploadExcelError(): void {
        this.notify.error(this.l('ImportUsersUploadFailed'));
    }

    exportToExcel(): void {
        this._userServiceProxy.getInstallerToExcel(
            this.filterName,
            this.filterText,
            undefined,
            this.role !== '' ? parseInt(this.role) : undefined,
            "Installer",
            this.organizationUnit !== '' ? parseInt(this.organizationUnit) : undefined,
            this.onlyLockedUsers,
            this.ismyinstalleruser,
            this.isapprove,
            this.primengTableHelper.getSorting(this.dataTable),
            this.suburb,
            this.stateFilter,
            this.postalcodefrom,
            this.postalcodeTo,
            this.jobTypeNameFilter,
            this.areaNameFilter,
            this.dateType,
            this.startDate,
            this.endDate,
            )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }


    createUser(): void {

        this.createOrEditMyInstallerModal.show(undefined, this.organizationUnit,'My Installer');
        // this._router.navigate(['/app/main/installer/createOrEdit']);
    }

    deleteUser(user: UserListDto): void {
        if (user.userName === AppConsts.userManagement.defaultAdminUserName) {
            this.message.warn(this.l('{0}UserCannotBeDeleted', AppConsts.userManagement.defaultAdminUserName));
            return;
        }

        this.message.confirm(
            this.l('UserDeleteWarningMessage', user.userName),
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._userServiceProxy.deleteSalesRep(user.id)
                        .subscribe(() => {
                            this.reloadPage(null);
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    setUsersProfilePictureUrl(users: UserListDto[]): void {
        for (let i = 0; i < users.length; i++) {
            let user = users[i];
            this._localStorageService.getItem(AppConsts.authorization.encrptedAuthTokenName, function (err, value) {
                let profilePictureUrl = AppConsts.remoteServiceBaseUrl + '/Profile/GetProfilePictureByUser?userId=' + user.id + '&' + AppConsts.authorization.encrptedAuthTokenName + '=' + encodeURIComponent(value.token);
                (user as any).profilePictureUrl = profilePictureUrl;
            });
        }
    }

    clear() {
        this.suburb = '';
        this.stateFilter = 0;
        this.postalcodefrom = 0;
        this.postalcodeTo = 0;
        this.jobTypeNameFilter = 0;
        this.areaNameFilter = "";
        this.dateType = "CreationDate";
        this.jobTypeNameFilter = 0;
        this.getUsers();
    }

    onchangeOrganization(organizationUnit) {
        this.organizationUnit = organizationUnit;
    }

    navigateToLeadDetail(myinstallerId): void {
        this.ExpandedView = !this.ExpandedView;
        this.ExpandedView = false;
        this.SelectedId = myinstallerId;
        this.viewmyInstallerDetail.showDetail(myinstallerId, '', 8,'My Installer');
    }

    expandGrid() {
        this.ExpandedView = true;
    }


    ApprovedInstaller(id): void {
        this.message.confirm('',
            this.l('AreYouSureYouWantToApproveInstaller?'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._userServiceProxy.approveInstaller(id)
                        .subscribe(() => {
                            let log = new UserActivityLogDto();
                            log.actionId = 32 ;
                            log.actionNote = 'My Installer Approved';
                            log.section = 'My Installer';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });
                            this.reloadPage(null);
                            this.notify.success(this.l('SuccessfullyApproveInstaller'));
                        });
                }
            }
        );
    }
    NotApprovedInstaller(id): void {
        this.message.confirm('',
            this.l('AreYouSureYouWantToUnApproveInstaller?'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._userServiceProxy.notApproveInstaller(id)
                        .subscribe(() => {
                            let log = new UserActivityLogDto();
                            log.actionId = 32 ;
                            log.actionNote = 'My Installer UnApproved';
                            log.section = 'My Installer';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });
                            this.reloadPage(null);
                            this.notify.success(this.l('SuccessfullyUnApproveInstaller'));
                        });
                }
            }
        );
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'My Installer';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'My Installer';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }

}
