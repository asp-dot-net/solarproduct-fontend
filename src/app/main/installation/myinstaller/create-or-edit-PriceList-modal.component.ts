﻿

import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {
    LeadExpensesServiceProxy, CreateOrEditLeadExpenseDto, LeadExpenseLeadSourceLookupTableDto, LeadStateLookupTableDto, LeadsServiceProxy, LeadExpenseAddDto, OrganizationUnitDto, UserServiceProxy, StateForLeadExpenseDto, CommonLookupServiceProxy, LeadSourceLookupTableDto, InstallerDetailsServiceProxy, MyInstallerPriceListDto, CommonLookupDto, CreateOrEditMyInstallerPriceListDto,
    UserActivityLogDto,
    UserActivityLogServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';

@Component({
    selector: 'createOrEditPriceListModal',
    templateUrl: './create-or-edit-PriceList-modal.component.html'
})
export class CreateOrEditPriceListModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    leadExpense: CreateOrEditLeadExpenseDto = new CreateOrEditLeadExpenseDto();
    date = new Date();
    //    sampleDateRange: moment.Moment[];
    firstDay = moment(new Date(this.date.getFullYear(), this.date.getMonth(), 1));
    lastday = moment().endOf('month');
    sampleDateRange: moment.Moment[] = [this.firstDay, this.lastday];
    leadSourceSourceName = '';
    totalamount = 0.00;
    myinstaller: MyInstallerPriceListDto[];
    priceitem: CommonLookupDto[];
    myinstallerlenght = 0;
    myinstallerdetail: CreateOrEditMyInstallerPriceListDto = new CreateOrEditMyInstallerPriceListDto();
    constructor(
        injector: Injector,
        private _leadExpensesServiceProxy: LeadExpensesServiceProxy,
        private _installerdetailService: InstallerDetailsServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
    ) {
        super(injector);
    }

    sectionName = '';
    show(id?: number,section = ''): void {
        debugger;
        this.sectionName = section;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Open For Price List Create or Edit'
        log.section = section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
        this._commonLookupService.getPriceItemListDropdown().subscribe(result => {
            this.priceitem = result;
        });
        this._installerdetailService.getPriceItemListEdit(id).subscribe(result => {
            debugger;
            this.myinstaller = [];
            this.myinstallerlenght = result.length;
            result.map((item) => {
                var myinstaller = new MyInstallerPriceListDto()
                myinstaller.id = item.id;
                myinstaller.priceItemListId = item.priceItemListId;
                myinstaller.cost = item.cost;
                myinstaller.gst = item.gst;
                myinstaller.priceINCGST = item.priceINCGST;
                myinstaller.myInstallerId = item.myInstallerId;
                this.myinstaller.push(myinstaller)
            })
            this.active = true;
            this.modal.show();
        });
    }

    pricecalculate(item , i)
    {
        debugger;
        this.myinstaller[i].gst =  parseInt((( item.cost / 10 )).toFixed(2));
        const gst = parseInt((( item.cost / 10 )).toFixed(2));
        this.myinstaller[i].priceINCGST = parseInt(((parseInt(item.cost) + gst)).toFixed(2));
    }

    save(): void {
        debugger;
        this.myinstallerdetail.pricelist = this.myinstaller;
        this._installerdetailService.createMyInstallerPriceList(this.myinstallerdetail)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                let log = new UserActivityLogDto();
                log.actionId = 82;
                log.actionNote ='Price List Updated'
                log.section = this.sectionName;
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
                this.notify.info(this.l('SavedSuccessfully'));
                this.active = false;
                this.modal.hide();
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
