import { AfterViewChecked, Component, ElementRef, EventEmitter, Injector, Optional, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    CreateOrUpdateInstallerInput, OrganizationUnitDto, PasswordComplexitySetting, ProfileServiceProxy,
    InstallerEditDto, UserRoleDto, UserServiceProxy,
    LeadStateLookupTableDto, LeadSourceLookupTableDto, LeadsServiceProxy, CreateOrEditLeadDto, InstallerAddressDto
    , CreateOrEditInstallerDetailDto, InstallerDetailDto, UpdateProfilePictureInput, UploadDocumentInput, QuotationsServiceProxy, CreateOrEditUserOrgDto, CommonLookupServiceProxy, JobJobTypeLookupTableDto,
    UserActivityLogDto,
    UserActivityLogServiceProxy
} from '@shared/service-proxies/service-proxies';

import { FileUpload } from 'primeng/fileupload';
import { NotifyService, TokenService, IAjaxResponse } from 'abp-ng2-module';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { HttpClient } from '@angular/common/http';
import * as _ from 'lodash';
import * as moment from 'moment';
import { finalize } from 'rxjs/operators';
import { forEach } from 'lodash';
import { debug } from 'console';
import { base64ToFile, ImageCroppedEvent } from 'ngx-image-cropper';
import { AgmCoreModule } from '@agm/core';
import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';
import PlaceResult = google.maps.places.PlaceResult;
import { Appearance, GermanAddress, Location } from '@angular-material-extensions/google-maps-autocomplete';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { FileUploader, FileUploaderOptions, FileItem } from 'ng2-file-upload';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'createOrEditMyInstallerModal',
    templateUrl: './create-or-edit-myinstaller-modal.component.html',
    styles: [`.user-edit-dialog-profile-image {
             margin-bottom: 20px;
        }`
    ],
    styleUrls: ['./create-or-edit-myinstaller-modal.component.less'],
    animations: [appModuleAnimation()]
})

export class CreateOrEditMyInstallerModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    canChangeUserName = true;
    isTwoFactorEnabled: boolean = this.setting.getBoolean('Abp.Zero.UserManagement.TwoFactorLogin.IsEnabled');
    isLockoutEnabled: boolean = this.setting.getBoolean('Abp.Zero.UserManagement.UserLockOut.IsEnabled');
    passwordComplexitySetting: PasswordComplexitySetting = new PasswordComplexitySetting();
    maxFilesize = 5;
    isInstaller = false;
    isElectrician = false;
    isDesigner = false
    user: InstallerEditDto = new InstallerEditDto();
    roles: UserRoleDto[];
    sendActivationEmail = true;
    setRandomPassword = true;
    passwordComplexityInfo = '';
    profilePicture: string;
    allOrganizationUnits: OrganizationUnitDto[];
    memberedOrganizationUnits: string[];
    userPasswordRepeat = '';
    allStates: LeadStateLookupTableDto[];
    allLeadSources: LeadSourceLookupTableDto[];
    postCodeSuburb = '';
    stateName = '';
    leadSourceName = '';
    LeadStatusName = '';
    latitude = '';
    longitude = '';
    leadStatus: any;
    unitType: any;
    streetType: any;
    streetName: any;
    suburb: any;
    postalUnitType: any;
    postalStreetType: any;
    postalStreetName: any;
    postalSuburb: any;
    unitTypesSuggestions: string[];
    streetTypesSuggestions: string[];
    streetNamesSuggestions: string[];
    suburbSuggestions: string[];
    postalunitTypesSuggestions: string[];
    postalstreetTypesSuggestions: string[];
    postalstreetNamesSuggestions: string[];
    postalsuburbSuggestions: string[];
    selectAddress: boolean = true;
    installer_uploadUrl: string;
    designer_uploadUrl: string;
    electrician_uploadUrl: string;
    other_uploadUrl: string;

    public filesUpload: FileUploader;
    private _uploaderOptions: FileUploaderOptions = {};
    public maxfileBytesUserFriendlyValue = 5;
    public documentPath = "";
    isInstDate: moment.Moment;
    isDesiDate: moment.Moment;
    isElecDate: moment.Moment;
    role: string = '';
    public instuploader: FileUploader;
    public desiuploader: FileUploader;
    public elecuploader: FileUploader;
    instfiletoken = [];
    instfilename = [];
    organizationUnit = null;
    desifiletoken = [];
    desifilename = [];
    areaName = "";
    elecfiletoken = [];
    elecfilename = [];
    fileinstname = "";
    filedesiname = "";
    fileelectname = "";
    userOrganization: CreateOrEditUserOrgDto[]
    editedOrganization: any;
    sourceType: JobJobTypeLookupTableDto[];
    constructor(
        injector: Injector,
        private _userService: UserServiceProxy,
        private _profileService: ProfileServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _tokenService: TokenService,
        private _httpClient: HttpClient,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _quotationsServiceProxy: QuotationsServiceProxy,
        private spinner: NgxSpinnerService
    ) {
        super(injector);
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
        this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
            this.role = result;
        });
    }

    ngOnInit(): void {
        this._commonLookupService.getAllLeadSourceDropdown().subscribe(result => {
            this.allLeadSources = result;
        });
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
        this._commonLookupService.getAllJobTypeForTableDropdown().subscribe(result => {
            this.sourceType = result;
        });
    }

    onShown(): void {
        document.getElementById('Name').focus();
    }

    google(): void {
        this.selectAddress = true;
    }

    database(): void {
        this.selectAddress = false;
    }

    // Old Code
    // onAutocompleteSelected(result: PlaceResult) {

    //     if (result.address_components.length == 7) {
    //         this.user.streetNo = result.address_components[0].long_name.toUpperCase();
    //         var str = result.address_components[1].long_name.toUpperCase();
    //         var splitted = str.split(" ");
    //         this.streetName = splitted[0].toUpperCase().trim();
    //         this.streetType = splitted[1].toUpperCase().trim();
    //         this.suburb = result.address_components[2].long_name.toUpperCase().trim();
    //         //this.installerAddress1.stateName = result.address_components[4].short_name;
    //         // this._leadsServiceProxy.getSuburbId(result.address_components[6].long_name).subscribe(result => {						
    //         //     this.installerAddress1.suburb = result;
    //         // });
    //         this._leadsServiceProxy.stateId(result.address_components[4].short_name).subscribe(result => {
    //             this.user.stateId = result;
    //         });
    //         this.user.postCode = result.address_components[6].long_name.toUpperCase();
    //         this.user.address = this.user.unit + " " + this.user.unitType + " " + this.user.streetNo + " " + this.streetName + " " + this.streetType;  //result.formatted_address;
    //     }
    //     else {
    //         var str1 = result.address_components[0].long_name.toUpperCase();
    //         var splitted1 = str1.split(" ");
    //         if (splitted1.length == 1) {
    //             this.user.unit = splitted1[0].toUpperCase().trim();
    //         }
    //         else {
    //             this.user.unit = splitted1[1].toUpperCase().trim();
    //             this.unitType = splitted1[0].toUpperCase().trim();
    //             this.user.unitType = splitted1[0].toUpperCase().trim();
    //         }
    //         this.user.streetNo = result.address_components[1].long_name.toUpperCase();
    //         var str = result.address_components[2].long_name.toUpperCase();
    //         var splitted = str.split(" ");
    //         this.streetName = splitted[0].toUpperCase().trim();
    //         this.streetType = splitted[1].toUpperCase().trim();
    //         this.suburb = result.address_components[3].long_name.toUpperCase().trim();
    //         //this.installerAddress1.stateName = result.address_components[5].short_name;
    //         // this._leadsServiceProxy.getSuburbId(result.address_components[7].long_name).subscribe(result => {						
    //         //     this.installerAddress1.suburbId = result;
    //         // });
    //         this._leadsServiceProxy.stateId(result.address_components[5].short_name).subscribe(result => {
    //             this.user.stateId = result;
    //         });
    //         this.user.postCode = result.address_components[7].long_name.toUpperCase();
    //         this.user.address = this.user.unit + " " + this.user.unitType + " " + this.user.streetNo + " " + this.streetName + " " + this.streetType;  //result.formatted_address;
    //     }
    // }

    // New Code
    onAutocompleteSelected(result: PlaceResult) {
        if (result.address_components.length == 7) {
            this.user.unit = "";
            this.unitType = "";
            this.user.unitType = "";
            this.user.streetNo = "";
            this.streetName = "";
            this.streetType = "";
            this.suburb = "";
            this.user.state = "";
            this.user.suburb = "";
            this.user.stateId = 0;
            this.user.postCode = "";
            this.user.address = "";
            // this.user.country = "";

            this.user.streetNo = result.address_components[0].long_name.toUpperCase();
            var str = result.address_components[1].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.streetType = splitted[3].toUpperCase().trim();
            }
            else if (splitted.length == 3) {
                this.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.streetType = splitted[2].toUpperCase().trim();
            }
            else {
                this.streetName = splitted[0].toUpperCase().trim();
                this.streetType = splitted[1].toUpperCase().trim();
            }
            this.suburb = result.address_components[2].long_name.toUpperCase().trim();
            this.user.state = result.address_components[4].short_name;
            // this._leadsServiceProxy.getSuburbId(result.address_components[6].long_name).subscribe(result => {
            //     this.user.suburbId = result;
            // });
            this._leadsServiceProxy.stateId(result.address_components[4].short_name).subscribe(result => {
                this.user.stateId = result;
            });
            this.user.postCode = result.address_components[6].long_name.toUpperCase();
            this.user.address = this.user.unit + " " + this.user.unitType + " " + this.user.streetNo + " " + this.streetName + " " + this.streetType;  //result.formatted_address;
            // this.user.country = result.address_components[5].long_name.toUpperCase();

            // this._leadsServiceProxy.getareaBysuburbPostandstate(this.suburb, this.user.state, this.user.postCode).subscribe(result => {
            //     if (result != null) {
            //         this.user.area = result;
            //     }
            // });
        }
        else if (result.address_components.length == 6) {
            this.user.unit = "";
            this.unitType = "";
            this.user.unitType = "";
            this.user.streetNo = "";
            this.streetName = "";
            this.streetType = "";
            this.suburb = "";
            this.user.state = "";
            this.user.suburb = "";
            this.user.stateId = 0;
            this.user.postCode = "";
            this.user.address = "";
            // this.user.country = "";

            this.user.streetNo = result.address_components[0].long_name.toUpperCase();
            var str = result.address_components[1].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.streetType = splitted[3].toUpperCase().trim();
            }
            else if (splitted.length == 3) {
                this.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.streetType = splitted[2].toUpperCase().trim();
            }
            else {
                this.streetName = splitted[0].toUpperCase().trim();
                this.streetType = splitted[1].toUpperCase().trim();
            }
            this.suburb = result.address_components[2].long_name.toUpperCase().trim();
            this.user.state = result.address_components[3].short_name;
            // this._leadsServiceProxy.getSuburbId(result.address_components[5].long_name).subscribe(result => {
            //     this.user.suburbId = result;
            // });
            this._leadsServiceProxy.stateId(result.address_components[3].short_name).subscribe(result => {
                this.user.stateId = result;
            });
            this.user.postCode = result.address_components[5].long_name.toUpperCase();
            this.user.address = this.user.unit + " " + this.user.unitType + " " + this.user.streetNo + " " + this.streetName + " " + this.streetType;  //result.formatted_address;
            // this.user.country = result.address_components[4].long_name.toUpperCase();
            // this._leadsServiceProxy.getareaBysuburbPostandstate(this.suburb, this.user.state, this.user.postCode).subscribe(result => {
            //     if (result != null) {
            //         this.user.area = result;
            //     }
            // });
        }
        else {
            this.user.unit = "";
            this.unitType = "";
            this.user.unitType = "";
            this.user.streetNo = "";
            this.streetName = "";
            this.streetType = "";
            this.suburb = "";
            this.user.state = "";
            this.user.suburb = "";
            this.user.stateId = 0;
            this.user.postCode = "";
            this.user.address = "";
            // this.user.country = "";

            var str1 = result.address_components[0].long_name.toUpperCase();
            var splitted1 = str1.split(" ");
            if (splitted1.length == 1) {
                this.user.unit = splitted1[0].toUpperCase().trim();
            }
            else {
                this.user.unit = splitted1[1].toUpperCase().trim();
                this.unitType = splitted1[0].toUpperCase().trim();
                this.user.unitType = splitted1[0].toUpperCase().trim();
            }
            this.user.streetNo = result.address_components[1].long_name.toUpperCase();
            var str = result.address_components[2].long_name.toUpperCase();
            var splitted = str.split(" ");
            if (splitted.length == 4) {
                this.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim() + " " + splitted[2].toUpperCase().trim();
                this.streetType = splitted[3].toUpperCase().trim();
            }
            else if (splitted.length == 3) {
                this.streetName = splitted[0].toUpperCase().trim() + " " + splitted[1].toUpperCase().trim();
                this.streetType = splitted[2].toUpperCase().trim();
            }
            else {
                this.streetName = splitted[0].toUpperCase().trim();
                this.streetType = splitted[1].toUpperCase().trim();
            }
            this.suburb = result.address_components[3].long_name.toUpperCase().trim();
            this.user.state = result.address_components[5].short_name;
            // this._leadsServiceProxy.getSuburbId(result.address_components[7].long_name).subscribe(result => {
            //     this.user.suburbId = result;
            // });
            this._leadsServiceProxy.stateId(result.address_components[5].short_name).subscribe(result => {
                this.user.stateId = result;
            });
            this.user.postCode = result.address_components[7].long_name.toUpperCase();
            this.user.address = this.user.unit + " " + this.user.unitType + " " + this.user.streetNo + " " + this.streetName + " " + this.streetType;  //result.formatted_address;
            // this.user.country = result.address_components[6].long_name.toUpperCase();

            // this._leadsServiceProxy.getareaBysuburbPostandstate(this.suburb, this.user.state, this.user.postCode).subscribe(result => {
            //     if (result != null) {
            //         this.user.area = result;
            //     }
            // });
        }

    }

    onLocationSelected(location: Location) {
        this.user.latitude = location.latitude.toString();
        this.user.longitude = location.longitude.toString();
    }

    onBeforeSend(event): void {
        event.xhr.setRequestHeader('Authorization', 'Bearer ' + abp.auth.getToken());
    }

    //get Address Drowns : get UnitType
    filterunitTypes(event): void {
        this._leadsServiceProxy.getAllUnitType(event.query).subscribe(output => {
            this.unitTypesSuggestions = output;
        });
    }

    // get StreetType
    filterstreettypes(event): void {
        this._leadsServiceProxy.getAllStreetType(event.query).subscribe(output => {
            this.streetTypesSuggestions = output;
        });
    }

    // get StreetName
    filterstreetnames(event): void {
        this._leadsServiceProxy.getAllStreetName(event.query).subscribe(output => {
            this.streetNamesSuggestions = output;
        });
    }

    // get Suburb
    filtersuburbs(event): void {
        this._leadsServiceProxy.getAllSuburb(event.query).subscribe(output => {
            this.suburbSuggestions = output;
        });
    }

    //get state & Postcode
    fillstatepostcode(): void {
        var splitted = this.suburb.split(" || ");
        this.user.suburb = splitted[0].trim();
        this._leadsServiceProxy.stateId(splitted[1].trim()).subscribe(result => {
            this.user.stateId = result;
        });
        this.user.postCode = splitted[2].trim();
    }

    sectionName = '';
    show(userId?: number, organizationUnit?: string,section = ''): void {
        this.sectionName = section;
        let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote = !userId ? 'Open For Create New Installer' : 'Open For Edit Installer';
            log.section = section;
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        if (!userId) {
            this.active = true;
            // this.setRandomPassword = true;
            // this.sendActivationEmail = true;
            this.user.isGoogle = "Google";
        }
        this.organizationUnit = organizationUnit;
        this.spinner.show();
        this._userService.getInstallerForEdit(userId).subscribe(userResult => {
            if (userResult.user.isGoogle == "Google") {
                this.selectAddress = true;
            }
            else {
                this.selectAddress = false;
            }
            this.user = userResult.user;
            this.roles = userResult.roles;
            this.canChangeUserName = this.user.userName !== AppConsts.userManagement.defaultAdminUserName;

            this.allOrganizationUnits = userResult.allOrganizationUnits;
            this.memberedOrganizationUnits = userResult.memberedOrganizationUnits;
            this.getProfilePicture(userId);

            this.streetType = this.user.streetType;
            this.streetName = this.user.streetName;
            this.unitType = this.user.unitType;
            this.suburb = this.user.suburb;
            this._userService.userWiseFromEmail(userId).subscribe(result => {
                debugger;
                this.userOrganization = [];
                this.editedOrganization = [];
                result.map((item) => {
                    var userwiseorgandemaildetail = new CreateOrEditUserOrgDto()
                    userwiseorgandemaildetail.id = item.userEmailDetail.id;
                    userwiseorgandemaildetail.userId = item.userEmailDetail.userId;
                    userwiseorgandemaildetail.organizationUnitId = item.userEmailDetail.organizationUnitId != null ? item.userEmailDetail.organizationUnitId : parseInt(this.organizationUnit);
                    userwiseorgandemaildetail.emailFromAdress = item.userEmailDetail.emailFromAdress != null ? item.userEmailDetail.emailFromAdress : '';
                    this.userOrganization.push(userwiseorgandemaildetail)
                    this.editedOrganization.push(userwiseorgandemaildetail.organizationUnitId)
                })
                if (result.length == 0) {
                    this._userService.loginuserwiseOrganization().subscribe(response => {
                        var userwiseorgandemaildetail = new CreateOrEditUserOrgDto()
                        userwiseorgandemaildetail.organizationUnitId = this.organizationUnit;
                        userwiseorgandemaildetail.emailFromAdress = response;
                        this.userOrganization.push(userwiseorgandemaildetail);
                        this.editedOrganization.push(userwiseorgandemaildetail.organizationUnitId)
                    });
                }
            });
            if (userId) {
                this.active = true;

                setTimeout(() => {
                    // this.setRandomPassword = false;
                }, 0);

                // this.sendActivationEmail = false;

                this.isInstDate = this.user.installerAccreditationExpiryDate;
                this.isDesiDate = this.user.designerLicenseExpiryDate;
                this.isElecDate = this.user.electricianLicenseExpiryDate;
                this.documentPath = this.user.filePath;
                this.fileinstname = this.user.docInstaller;
                this.filedesiname = this.user.docDesigner;
                this.fileelectname = this.user.docElectrician;
            }

            this._profileService.getPasswordComplexitySetting().subscribe(passwordComplexityResult => {
                this.passwordComplexitySetting = passwordComplexityResult.setting;
                this.setPasswordComplexityInfo();
                this.modal.show();
                this.spinner.hide();
                this.initializeModal();
            });
        });
    }
    adduseremailorgDetail(): void {
        this.userOrganization.push(new CreateOrEditUserOrgDto);
    }
    removeJoboldsysdetail(userOrganizationss): void {
        debugger;
        if (this.userOrganization.length == 1)
            return;
        this.userOrganization = this.userOrganization.filter(item => item.organizationUnitId != userOrganizationss.organizationUnitId);
    }
    setPasswordComplexityInfo(): void {

        this.passwordComplexityInfo = '<ul>';

        if (this.passwordComplexitySetting.requireDigit) {
            this.passwordComplexityInfo += '<li>' + this.l('PasswordComplexity_RequireDigit_Hint') + '</li>';
        }

        if (this.passwordComplexitySetting.requireLowercase) {
            this.passwordComplexityInfo += '<li>' + this.l('PasswordComplexity_RequireLowercase_Hint') + '</li>';
        }

        if (this.passwordComplexitySetting.requireUppercase) {
            this.passwordComplexityInfo += '<li>' + this.l('PasswordComplexity_RequireUppercase_Hint') + '</li>';
        }

        if (this.passwordComplexitySetting.requireNonAlphanumeric) {
            this.passwordComplexityInfo += '<li>' + this.l('PasswordComplexity_RequireNonAlphanumeric_Hint') + '</li>';
        }

        if (this.passwordComplexitySetting.requiredLength) {
            this.passwordComplexityInfo += '<li>' + this.l('PasswordComplexity_RequiredLength_Hint', this.passwordComplexitySetting.requiredLength) + '</li>';
        }

        this.passwordComplexityInfo += '</ul>';
    }

    getProfilePicture(userId: number): void {
        if (!userId) {
            this.profilePicture = this.appRootUrl() + 'assets/common/images/default-profile-picture.png';
            return;
        }
        this._profileService.getProfilePictureByUser(userId).subscribe(result => {
            if (result && result.profilePicture) {
                this.profilePicture = 'data:image/jpeg;base64,' + result.profilePicture;
            } else {
                this.profilePicture = this.appRootUrl() + 'assets/common/images/default-profile-picture.png';
            }
        });
    }



    initializeModal(): void {
        this.active = true;
        this.initFileUploader();
        this.initDesiFileUploader();
        this.initElecFileUploader();
    }

    filechangeEvent(event: any): void {
        if (event.target.files[0].size > 5242880) { //5MB
            this.message.warn(this.l('ProfilePicture_Warn_SizeLimit', this.maxfileBytesUserFriendlyValue));
            return;
        }
        this.instuploader.clearQueue();
        this.instuploader.addToQueue([<File>event.target.files[0]]);
        this.instuploader.uploadAll();
    }
    fileisDesichangeEvent(event: any): void {
        if (event.target.files[0].size > 5242880) { //5MB
            this.message.warn(this.l('ProfilePicture_Warn_SizeLimit', this.maxfileBytesUserFriendlyValue));
            return;
        }
        this.desiuploader.clearQueue();
        this.desiuploader.addToQueue([<File>event.target.files[0]]);
        this.desiuploader.uploadAll();
    }
    fileisElecchangeEvent(event: any): void {
        if (event.target.files[0].size > 5242880) { //5MB
            this.message.warn(this.l('ProfilePicture_Warn_SizeLimit', this.maxfileBytesUserFriendlyValue));
            return;
        }
        this.elecuploader.clearQueue();
        this.elecuploader.addToQueue([<File>event.target.files[0]]);
        this.elecuploader.uploadAll();
    }
    initFileUploader(): void {
        this.instuploader = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Profile/UploadFile' });
        this._uploaderOptions.autoUpload = false;
        this._uploaderOptions.authToken = 'Bearer ' + this._tokenService.getToken();
        this._uploaderOptions.removeAfterUpload = true;
        this.instuploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

        this.instuploader.onBuildItemForm = (fileItem: FileItem, form: any) => {
            form.append('FileType', fileItem.file.type);
            form.append('FileName', fileItem.file.name);
            form.append('FileToken', this.instguid());
        };

        this.instuploader.onSuccessItem = (item, response, status) => {
            const resp = <IAjaxResponse>JSON.parse(response);
            if (resp.success) {
                this.instfiletoken.push(resp.result.fileToken);
                this.instfilename.push(resp.result.fileName);
            } else {
                this.message.error(resp.error.message);
            }
        };

        this.instuploader.setOptions(this._uploaderOptions);
    }

    initDesiFileUploader(): void {
        this.desiuploader = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Profile/UploadFile' });
        this._uploaderOptions.autoUpload = false;
        this._uploaderOptions.authToken = 'Bearer ' + this._tokenService.getToken();
        this._uploaderOptions.removeAfterUpload = true;
        this.desiuploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

        this.desiuploader.onBuildItemForm = (fileItem: FileItem, form: any) => {
            form.append('FileType', fileItem.file.type);
            form.append('FileName', fileItem.file.name);
            form.append('FileToken', this.desiguid());
        };

        this.desiuploader.onSuccessItem = (item, response, status) => {
            const resp = <IAjaxResponse>JSON.parse(response);
            if (resp.success) {
                this.desifiletoken.push(resp.result.fileToken);
                this.desifilename.push(resp.result.fileName);
            } else {
                this.message.error(resp.error.message);
            }
        };

        this.desiuploader.setOptions(this._uploaderOptions);
    }

    initElecFileUploader(): void {
        this.elecuploader = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Profile/UploadFile' });
        this._uploaderOptions.autoUpload = false;
        this._uploaderOptions.authToken = 'Bearer ' + this._tokenService.getToken();
        this._uploaderOptions.removeAfterUpload = true;
        this.elecuploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

        this.elecuploader.onBuildItemForm = (fileItem: FileItem, form: any) => {
            form.append('FileType', fileItem.file.type);
            form.append('FileName', fileItem.file.name);
            form.append('FileToken', this.elecguid());
        };

        this.elecuploader.onSuccessItem = (item, response, status) => {
            const resp = <IAjaxResponse>JSON.parse(response);
            if (resp.success) {
                this.elecfiletoken.push(resp.result.fileToken);
                this.elecfilename.push(resp.result.fileName);
            } else {
                this.message.error(resp.error.message);
            }
        };

        this.elecuploader.setOptions(this._uploaderOptions);
    }

    instguid(): string {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }


    desiguid(): string {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }


    elecguid(): string {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }



    save(): void {

        let input = new CreateOrUpdateInstallerInput();
        input.assignedRoleNames =
            _.map(
                _.filter(this.roles,
                    {
                        isAssigned: true,
                        inheritedFromOrganizationUnit: false
                    }),
                role => role.roleName
            );

        input.assignedRoleNames.pop();
        input.assignedRoleNames.pop();
        input.assignedRoleNames.push("Installer");//Only Installer
        input.userEmailDetail = this.userOrganization;

        input.user = this.user;
        // input.setRandomPassword = this.setRandomPassword;
        // input.sendActivationEmail = this.sendActivationEmail;
        input.user.target = "N.A.";
        input.user.categoryId = 0;
        input.user.streetName = this.streetName;
        input.user.streetType = this.streetType;
        input.user.unitType = this.unitType;
        input.user.suburb = this.suburb;
        input.user.installerAccreditationExpiryDate = this.isInstDate;
        input.user.designerLicenseExpiryDate = this.isDesiDate;
        input.user.electricianLicenseExpiryDate = this.isElecDate;
        input.user.docInstaller = this.instfiletoken[0];
        input.user.docDesigner = this.desifiletoken[0];
        input.user.docElectrician = this.elecfiletoken[0];
        input.user.docInstallerFileName = this.instfilename[0];
        input.user.docDesignerFileName = this.desifilename[0];
        input.user.docElectricianFileName = this.elecfilename[0];
        input.user.userName = this.user.name + '' + this.user.surname;
        input.setRandomPassword = true;
        // input.user.userPasswordRepeat = 'NULL';
        input.user.ismyinstalleruser = true;
        // input.user.isLockoutEnabled = true;
        //console.log('userObject', input.user);

        this.saving = true;
        // this.filesUpload.uploadAll();
        this._userService.createOrUpdateInstaller(input)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                let log = new UserActivityLogDto();
                log.actionId = !input.user.id ? 81 : 82 ;
                log.actionNote = !input.user.id ? 'New Installer Created' : 'Installer Updated';
                log.section = this.sectionName;
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.instfiletoken = [];
                this.desifiletoken = [];
                this.elecfiletoken = [];
                this.instfilename = [];
                this.desifilename = [];
                this.elecfilename = [];
                this.instuploader = null;
                this.desiuploader = null;
                this.elecuploader = null;
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        // this.userPasswordRepeat = '';
        this.instfiletoken = [];
        this.desifiletoken = [];
        this.elecfiletoken = [];
        this.instfilename = [];
        this.desifilename = [];
        this.elecfilename = [];
        this.instuploader = null;
        this.desiuploader = null;
        this.elecuploader = null;
        this.modal.hide();
    }

    getAssignedRoleCount(): number {
        return _.filter(this.roles, { isAssigned: true }).length;
    }

    downloadInstfile(): void {

        let FileName = AppConsts.docUrl + "/" + this.documentPath + this.fileinstname;
        window.open(FileName, "_blank");
    }
    downloadElecfile(): void {

        let FileName = AppConsts.docUrl + "/" + this.documentPath + this.fileelectname;
        window.open(FileName, "_blank");
    }
    downloadDesifile(): void {

        let FileName = AppConsts.docUrl + "/" + this.documentPath + this.filedesiname;
        window.open(FileName, "_blank");
    }


}
