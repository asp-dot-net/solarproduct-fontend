import { Component, Injector, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CreateOrEditUserOrgDto, GetInstallerForEditOutput, InstallerEditDto, UserRoleDto, UserServiceProxy } from '@shared/service-proxies/service-proxies';
import * as moment from 'moment';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'myInstallerDetailModal',
    templateUrl: './myinstaller-detail-modal.component.html'
})
export class MyinstallerDetailComponent extends AppComponentBase {

    @ViewChild('myInstallerDetailModal', { static: true }) modal: ModalDirective;

    active = false;
    saving = false;
    item: GetInstallerForEditOutput;
    roles: UserRoleDto[];
    userOrganization: CreateOrEditUserOrgDto[]
    constructor(
        injector: Injector,
        private _userService: UserServiceProxy,
        private spinner: NgxSpinnerService
    ) {
        super(injector);
        this.item = new GetInstallerForEditOutput();
        this.item.user = new InstallerEditDto();
    }

    show(userId?: number): void {
        // this.item = item;
        this.active = true;
        // if (!userId) {
        //     this.active = true;
        //     this.setRandomPassword = true;
        //     this.sendActivationEmail = true;
        //     this.user.isGoogle = "Google";
        // }
        this.spinner.show();
        this._userService.getInstallerForEdit(userId).subscribe(userResult => {
            console.log('userResult', userResult);
            this.item.user = userResult.user;
            this.roles = userResult.roles;
            this._userService.userWiseFromEmail(userId).subscribe(result => {
                debugger;
                this.userOrganization = [];
                result.map((item) => {
                    var userwiseorgandemaildetail = new CreateOrEditUserOrgDto()
                    userwiseorgandemaildetail.id = item.userEmailDetail.id;
                    userwiseorgandemaildetail.userId = item.userEmailDetail.userId;
                    userwiseorgandemaildetail.organizationUnitId = item.userEmailDetail.organizationUnitId;
                    userwiseorgandemaildetail.emailFromAdress = item.userEmailDetail.emailFromAdress;
                    this.userOrganization.push(userwiseorgandemaildetail)
                })
                if (result.length == 0) {
                    var userwiseorgandemaildetail = new CreateOrEditUserOrgDto()
                    this.userOrganization.push(userwiseorgandemaildetail);
                }
                console.log('this.userOrganization', this.userOrganization);
            });
            // userResult.allOrganizationUnits.forEach(item => {
            //     this.userOrganization.forEach(element => {
            //         if (item.id == element.organizationUnitId) {
            //             this.teams.push(element.team);
            //         }
            //     });
            // });
            // if (userId) {
            //     this.active = true;

            //     setTimeout(() => {
            //         this.setRandomPassword = false;
            //     }, 0);

            //     this.sendActivationEmail = false;

            //     this.isInstDate = this.user.installerAccreditationExpiryDate;
            //     this.isDesiDate = this.user.designerLicenseExpiryDate;
            //     this.isElecDate = this.user.electricianLicenseExpiryDate;
            //     this.documentPath = this.user.filePath;
            //     this.fileinstname = this.user.docInstaller;
            //     this.filedesiname = this.user.docDesigner;
            //     this.fileelectname = this.user.docElectrician;
            // }

            // this._profileService.getPasswordComplexitySetting().subscribe(passwordComplexityResult => {
            //     this.passwordComplexitySetting = passwordComplexityResult.setting;
            //     this.setPasswordComplexityInfo();
            //     this.modal.show();
            //     this.spinner.hide();
            //     this.initializeModal();
            // });
        });
        this.modal.show();
        this.spinner.hide();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    downloadfile(file , path): void {
        let FileName = AppConsts.docUrl + "/" + path +  file;
        window.open(FileName, "_blank");
    };
}
