import { Component, EventEmitter, Injector, Input, OnInit, Output, ViewChild } from '@angular/core';
import {
    LeadsServiceProxy, GetActivityLogViewDto, GetLeadForViewDto, GetLeadForChangeStatusOutput, LeadDto, CommonLookupDto, UserServiceProxy, GetMyInstallerActivityLogDto,
    UserActivityLogDto,
    UserActivityLogServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import PlaceResult = google.maps.places.PlaceResult;
import { JobsComponent } from '@app/main/jobs/jobs/jobs.component';
import { PromotionStopResponceComponent } from '@app/main/promotions/promotionUsers/stop-responce.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { AddActivityModalComponent } from '@app/main/myleads/myleads/add-activity-model.component';
import { MyinstallerActivityLogHistoryComponent } from './myinstaller-activity-log-history.component';
import { finalize } from 'rxjs/operators';


@Component({
    selector: 'activityLogMyInstaller',
    templateUrl: './activity-log-myinstaller.component.html',
    animations: [appModuleAnimation()]
})
export class ActivityLogMyinstallerComponent extends AppComponentBase implements OnInit {

    @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    @ViewChild('myinstalleractivityloghistory', { static: true }) myinstalleractivityloghistory: MyinstallerActivityLogHistoryComponent;
    @Input() SelectedLeadId: number = 0;
    @Output() reloadLead = new EventEmitter();
    active = false;
    saving = false;
    item: GetLeadForViewDto;
    activeTabIndex: number = 0;
    activityDatabyDate: any[] = [];
    actionId: number = 0;
    activity: boolean = false;
    leadActivityList: GetMyInstallerActivityLogDto[];
    leadActionList: CommonLookupDto[];
    leadId: number = 0;
    id: number = 0;
    role: string = '';
    sectionId: number = 0;
    showsectionwise = false;
    currentactivity: boolean = false;
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _router: Router,
        private spinner: NgxSpinnerService,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _userServiceProxy: UserServiceProxy,
    ) {
        super(injector);
        this.item = new GetLeadForViewDto();
        this.item.lead = new LeadDto();
    }


    ngOnInit(): void {
        //this.show(this._activatedRoute.snapshot.queryParams['id']);
        this._leadsServiceProxy.getAllLeadAction().subscribe(result => {
            this.leadActionList = result;
        });

        this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
            this.role = result;
            if (this.role == 'Sales Rep') {
                this.activity = true;
            }
        });
    }

        sectionName = '';
        show(leadId?: number, param?: number, sectionId?: number,section = ''): void {
        debugger;
        this.sectionName = section;
        this.sectionId = sectionId;
        this.activity = false;
        if (this.sectionId == 0 || this.sectionId > 12) {
            this.showsectionwise = true;
        } 
        if (param != null) {
            this.id = param;
        }
        this.leadId = leadId;
        if(this.sectionId == 23 || this.sectionId == 24 || this.sectionId == 25)
        {
            this.currentactivity =  true;
        }
        if (this.sectionId == 15) {
            this.actionId = 11;
            this._userServiceProxy.getMyInstallerActivityLog(leadId, this.actionId, this.sectionId, this.currentactivity, this.activity, leadId, true, 0).subscribe(result => {
              debugger;
                this.leadActivityList = result;
            });
        } else {
            this._userServiceProxy.getMyInstallerActivityLog(leadId, this.actionId, this.sectionId, this.currentactivity, this.activity, leadId, true, 0).subscribe(result => {
                debugger;
                this.leadActivityList = result;
            });
        }
    }

    showDetail(leadId: number): void {
        let that = this;
        debugger
        this._userServiceProxy.getMyInstallerActivityLog(leadId, 0, this.sectionId,this.currentactivity, false, leadId, true, 0).subscribe(result => {
            this.leadActivityList = result;
        });
    }

    registerToEvents() {
        abp.event.on('app.onCancelModalSaved', () => {
            this.showDetail(this.leadId);
        });
    }

    addActivitySuccess() {
        this.showDetail(this.leadId);
    }

    changeActivity(leadId: number) {
        let that = this;
        this.spinner.show();
        debugger
        this._userServiceProxy.getMyInstallerActivityLog(leadId, this.actionId, this.sectionId,this.currentactivity, false, leadId, true, 0).subscribe(result => {
            let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Refresh the data';
            log.section = this.sectionName; 
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            if (result.length > 0) {
                this.notify.success(this.l('ActivityFiltered'));
               
            }
            else {
                this.notify.success(this.l('NoActivityFound'));
            }
            this.leadActivityList = result;
            this.spinner.hide();
        });
    }
    navigateToLeadHistory(leadid): void {

        this.myinstalleractivityloghistory.show(leadid);
    }

    myActivity(leadId: number) {
        let that = this;
        this.spinner.show();
        this._userServiceProxy.getMyInstallerActivityLog(leadId, this.actionId, this.sectionId,this.currentactivity, this.activity, leadId, true, 0).subscribe(result => {
            let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Refresh the data';
            log.section = this.sectionName; 
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            if (result.length > 0) {
                this.notify.success(this.l('ActivityFiltered'));
            }
            else {
                this.notify.success(this.l('NoActivityFound'));
            }
            this.leadActivityList = result;
            this.spinner.hide();
        });
    }

}
