﻿import { Component, ViewChild, Injector, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { LeadsServiceProxy, GetLeadForViewDto, LeadDto, GetActivityLogViewDto, GetLeadForChangeStatusOutput, UserServiceProxy, GetMyInstallerActivityLogDto, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import * as moment from 'moment';
import { JobsComponent } from '@app/main/jobs/jobs/jobs.component';
import { AgmCoreModule } from '@agm/core';
import PlaceResult = google.maps.places.PlaceResult;
import { Appearance, GermanAddress, Location } from '@angular-material-extensions/google-maps-autocomplete';
import { LazyLoadEvent } from 'primeng/public_api';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivityLogMyinstallerComponent } from './activity-log-myinstaller.component';
import { finalize } from 'rxjs/operators';


@Component({
    selector: 'viewmyInstallerDetail',
    templateUrl: './view-myintaller.component.html',
    styleUrls: ['./myintaller.component.less'],
    animations: [appModuleAnimation()]
})
export class ViewMyinstallerComponent extends AppComponentBase implements OnInit {

    // @ViewChild('addMyInstallerActivityModal', { static: true }) addMyInstallerActivityModal: ActivityLogMyinstallerComponent;
    @ViewChild('jobCreateOrEdit', { static: true }) jobCreateOrEdit: JobsComponent;
    // @ViewChild('createEditLead', { static: true }) createEditLead: CreateEditLeadComponent;
    @ViewChild('activityLogMyInstaller', { static: true }) activityLogMyInstaller: ActivityLogMyinstallerComponent;
    // @ViewChild('promoStopRespmodel', { static: true }) promot: PromotionStopResponceComponent;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    @Input() SelectedId: number = 0;
    @Output() reloadLead = new EventEmitter<boolean>();
    active = false;
    saving = false;
    item: GetLeadForViewDto;
    activityLog: GetMyInstallerActivityLogDto[];
    activeTabIndex: number = 0;
    activeTabIndexForPromotion: number = 0;
    activityDatabyDate: any[] = [];
    actionId: number = 0;
    activity: boolean = false;
    leadActivityList: GetMyInstallerActivityLogDto[];
    lat: number;
    lng: number;
    zoom: 20;
    showforresult = false;
    name: string;
    role: string = '';
    mainsearch = false;
    sectionId = 0;
    showsectionwise = false;
    sectionName = '';
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _router: Router,
        private spinner: NgxSpinnerService,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _userServiceProxy: UserServiceProxy,
    ) {
        super(injector);
        this.item = new GetLeadForViewDto();
        this.item.lead = new LeadDto();
    }


    ngOnInit(): void {
        this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
            this.role = result;
        });

        if (this.SelectedId > 0)
            this.showDetail(this.SelectedId);
        this.registerToEvents();
    }

    getParsedDate(strDate) {//get date formate
        if (strDate == "" || strDate == null || strDate == undefined) {
            return;
        }
        let month_names = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

        var strSplitDate = String(strDate._i).split('T');

        var dateArray = strSplitDate[0].split('-');
        let monthint = parseInt(dateArray[1]);
        let date = month_names[monthint - 1] + " " + dateArray[2];
        return date;

    }

    showDetail(leadId: number, LeadName?: string, sectionId?: number,section = ''): void {
        debugger;
        this.sectionName = section;
       
        if (sectionId == 15) {
            // this.activeTabIndexForPromotion = 0;
            this.activeTabIndex = 1;
        } else {
            this.activeTabIndex = 0;
            // this.activeTabIndexForPromotion = 1;
        }
        if (sectionId == 30) {
            this.mainsearch = true;
        }
        this.sectionId = sectionId;
        if (this.sectionId == 0 || this.sectionId > 12) {
            this.showsectionwise = true;
        }
        this.name = LeadName;
        let that = this;
        if (leadId != null) {
            // this.spinner.show();
            this.activityLogMyInstaller.show(leadId, 0, this.sectionId,this.sectionName);
            this._userServiceProxy.getMyInstallerActivityLog(leadId, 0, this.sectionId, false, false, leadId, false, 0).subscribe(result => {
                let lastdatetime !: moment.Moment;
                this.activityLog = result;
                this.leadActivityList = result;
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open Installer Detail by Click On Name';
                log.section = section; 
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
                let obj = {
                    activityDate: "",
                    DatewiseActivity: []
                }
                this.activityDatabyDate = [];
                this.activityLog.forEach(function (log) {
                    if (that.getParsedDate(log.creationTime) == that.getParsedDate(lastdatetime)) {
                        log.logDate = "";
                        let item1 = this.activityDatabyDate.find(activityDate => activityDate.activityDate === that.getParsedDate(lastdatetime));
                        item1.DatewiseActivity.push({ actionNote: log.actionNote });
                        item1.DatewiseActivity.push({ actionNote: log.creationTime });
                    }
                    else {
                        log.logDate = that.getParsedDate(log.creationTime);
                        obj.activityDate = log.logDate;
                        obj.DatewiseActivity.push(log.actionNote);
                        obj.DatewiseActivity.push(log.creationTime);
                        this.activityDatabyDate.push({
                            activityDate: log.logDate,
                            DatewiseActivity: [{ actionNote: log.actionNote }, { creationTime: log.creationTime }]
                        });
                    }
                    lastdatetime = log.creationTime;
                }.bind(that));
            });
        }

    }

    deleteLead(lead: LeadDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._leadsServiceProxy.deleteLeads(lead.id)
                        .subscribe(() => {
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            this.reloadLead.emit(true);
                        });
                }
            }
        );
    }

    warm(lead: LeadDto): void {
        let status: GetLeadForChangeStatusOutput = new GetLeadForChangeStatusOutput();
        status.id = lead.id;
        status.leadStatusID = 4;
        this.message.confirm('',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._leadsServiceProxy.changeStatus(status)
                        .subscribe(() => {
                            this.notify.success(this.l('LeadStatusChangedToWarm'));
                            //this._router.navigate(['/app/main/myleads/myleads']);
                            this.modalSave.emit(null);
                            this.showDetail(lead.id);
                            this.reloadLead.emit(false);
                        });
                }
            }
        );
    }

    cold(lead: LeadDto): void {
        let status: GetLeadForChangeStatusOutput = new GetLeadForChangeStatusOutput();
        status.id = lead.id;
        status.leadStatusID = 3;
        this.message.confirm('',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._leadsServiceProxy.changeStatus(status)
                        .subscribe(() => {
                            this.notify.success(this.l('LeadStatusChangedToCold'));
                            //this._router.navigate(['/app/main/myleads/myleads']);
                            this.modalSave.emit(null);
                            this.showDetail(lead.id);
                            this.reloadLead.emit(false);
                        });
                }
            }
        );
    }

    hot(lead: LeadDto): void {
        let status: GetLeadForChangeStatusOutput = new GetLeadForChangeStatusOutput();
        status.id = lead.id;
        status.leadStatusID = 5;
        this.message.confirm('',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._leadsServiceProxy.changeStatus(status)
                        .subscribe(() => {
                            this.notify.success(this.l('LeadStatusChangedToHot'));
                            //this._router.navigate(['/app/main/myleads/myleads']);
                            this.modalSave.emit(null);
                            this.showDetail(lead.id);
                            this.reloadLead.emit(false);
                        });
                }
            }
        );
    }

    cancel(lead: LeadDto): void {

        abp.event.trigger('app.show.cancelLeadModal', lead.id);
    }

    reject(id): void {
        abp.event.trigger('app.show.rejectLeadModal', id);
    }

    registerToEvents() {
        abp.event.on('app.onCancelModalSaved', () => {
            this.showDetail(this.SelectedId);
        });
    }

    addActivitySuccess() {

        this.showDetail(this.SelectedId);
    }

    changeActivity(lead: LeadDto) {
        let that = this;

        this._userServiceProxy.getMyInstallerActivityLog(lead.id, this.actionId, this.sectionId, false, false, 0, false, 0).subscribe(result => {
            if (result.length > 0) {
                this.notify.success(this.l('ActivityFiltered'));
            }
            else {
                this.notify.success(this.l('NoActivityFound'));
            }
            let lastdatetime !: moment.Moment;
            this.activityLog = result;
            this.leadActivityList = result;
            let obj = {
                activityDate: "",
                DatewiseActivity: []
            }
            this.activityDatabyDate = [];
            this.activityLog.forEach(function (log) {
                if (that.getParsedDate(log.creationTime) == that.getParsedDate(lastdatetime)) {
                    log.logDate = "";
                    let item1 = this.activityDatabyDate.find(activityDate => activityDate.activityDate === that.getParsedDate(lastdatetime));
                    item1.DatewiseActivity.push({ actionNote: log.actionNote });
                }
                else {
                    log.logDate = that.getParsedDate(log.creationTime);
                    obj.activityDate = log.logDate;
                    obj.DatewiseActivity.push(log.actionNote);
                    this.activityDatabyDate.push({
                        activityDate: log.logDate,
                        DatewiseActivity: [{ actionNote: log.actionNote }]
                    });
                }
                lastdatetime = log.creationTime;
            }.bind(that));

        });
    }

    myActivity(lead: LeadDto) {
        let that = this;

        this._userServiceProxy.getMyInstallerActivityLog(lead.id, this.actionId, this.sectionId, false, this.activity, 0, true, 0).subscribe(result => {
            if (result.length > 0) {
                this.notify.success(this.l('ActivityFiltered'));
            }
            else {
                this.notify.success(this.l('NoActivityFound'));
            }
            let lastdatetime !: moment.Moment;
            this.activityLog = result;
            this.leadActivityList = result;
            let obj = {
                activityDate: "",
                DatewiseActivity: []
            }
            this.activityDatabyDate = [];
            this.activityLog.forEach(function (log) {
                if (that.getParsedDate(log.creationTime) == that.getParsedDate(lastdatetime)) {
                    log.logDate = "";
                    let item1 = this.activityDatabyDate.find(activityDate => activityDate.activityDate === that.getParsedDate(lastdatetime));
                    item1.DatewiseActivity.push({ actionNote: log.actionNote });
                }
                else {
                    log.logDate = that.getParsedDate(log.creationTime);
                    obj.activityDate = log.logDate;
                    obj.DatewiseActivity.push(log.actionNote);
                    this.activityDatabyDate.push({
                        activityDate: log.logDate,
                        DatewiseActivity: [{ actionNote: log.actionNote }]
                    });
                }
                lastdatetime = log.creationTime;
            }.bind(that));

        });
    }
}
