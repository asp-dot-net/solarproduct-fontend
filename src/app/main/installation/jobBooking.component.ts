import { Component, Injector, ViewEncapsulation, ViewChild, Input, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JobsServiceProxy, JobDto, GetLeadForViewDto, LeadsServiceProxy, InstallationServiceProxy, LeadUsersLookupTableDto, CommonLookupDto, UserServiceProxy, OrganizationUnitDto, JobHouseTypeLookupTableDto, JobJobTypeLookupTableDto, LeadStateLookupTableDto, JobRoofTypeLookupTableDto, JobPaymentOptionLookupTableDto, CommonLookupServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { EntityTypeHistoryModalComponent } from '@app/shared/common/entityHistory/entity-type-history-modal.component';
import * as _ from 'lodash';
import * as moment from 'moment';
import { JobDetailModalComponent } from '@app/main/jobs/jobs/job-detail-model.component';

import { finalize } from 'rxjs/operators';
import { PendingInstallationDetailComponent } from './installation/pendinginstallationdetail.component';
import { AddActivityModalComponent } from '../myleads/myleads/add-activity-model.component';
import { ViewMyLeadComponent } from '../myleads/myleads/view-mylead.component';
import { Title } from '@angular/platform-browser';
import { JobBookingSmsEmailComponent } from './jobBooking-sms-email-model/jobBooking-sms-email.component';
import { ViewApplicationModelComponent } from '../jobs/jobs/view-application-model/view-application-model.component';
import { CommentModelComponent } from '../activitylog/comment-modal.component';
import { NotifyModelComponent } from '../activitylog/notify-modal.component';
import { ReminderModalComponent } from '../activitylog/reminder-modal.component';
import { ToDoModalComponent } from '../activitylog/todo-modal.component';

@Component({
    templateUrl: './jobBooking.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class JobBookingComponent extends AppComponentBase {

    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    assignLead = 0;
    ExpandedView: boolean = true;
    SelectedLeadId: number = 0;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    toggle: boolean = true;

    change() {
        this.toggle = !this.toggle;
      }

    @ViewChild('entityTypeHistoryModal', { static: true }) entityTypeHistoryModal: EntityTypeHistoryModalComponent;

    @ViewChild('PendingInstallationDetail', { static: true }) PendingInstallationDetail: PendingInstallationDetailComponent;
    @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
    @ViewChild('jobBookingsmsemail', { static: true }) jobBookingsmsemail: JobBookingSmsEmailComponent;
    @ViewChild('viewApplicationModal', { static: true }) viewApplicationModal: ViewApplicationModelComponent;
    @ViewChild('notifyModel', { static: true }) notifyModel: NotifyModelComponent;
    @ViewChild('commentModel', { static: true }) commentModel: CommentModelComponent;
    @ViewChild('todoModal', { static: true }) todoModal: ToDoModalComponent;
    @ViewChild('ReminderModal', { static: true }) ReminderModal: ReminderModalComponent;
    advancedFiltersAreShown = false;
    stausfilterSelect = 0;
    filterText = '';
    maxPanelOnFlatFilter: number;
    maxPanelOnFlatFilterEmpty: number;
    minPanelOnFlatFilter: number;
    minPanelOnFlatFilterEmpty: number;
    maxPanelOnPitchedFilter: number;
    maxPanelOnPitchedFilterEmpty: number;
    minPanelOnPitchedFilter: number;
    minPanelOnPitchedFilterEmpty: number;
    regPlanNoFilter = '';
    lotNumberFilter = '';
    peakMeterNoFilter = '';
    offPeakMeterFilter = '';
    enoughMeterSpaceFilter = -1;
    isSystemOffPeakFilter = -1;
    jobids=[];
    suburbFilter = '';
    stateFilter = '';
    jobTypeNameFilter = '';
    jobStatusNameFilter = '';
    roofTypeNameFilter = '';
    roofAngleNameFilter = '';
    elecDistributorNameFilter = '';
    leadCompanyNameFilter = '';
    elecRetailerNameFilter = '';
    paymentOptionNameFilter = '';
    depositOptionNameFilter = '';
    meterUpgradeNameFilter = '';
    meterPhaseNameFilter = '';
    promotionOfferNameFilter = '';
    houseTypeNameFilter = '';
    financeOptionNameFilter = '';
    tableRecords: any;
    // OpenRecordId: number = 0;
    totalcount = 0;
    count = 0;
    allUsers: CommonLookupDto[];
    _entityTypeFullName = 'TheSolarProduct.Jobs.Job';
    entityHistoryEnabled = false;
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnit = 0;
    organizationUnitlength: number = 0;
    stateNameFilter = "";
    dateFilterType = 'All';
    areaNameFilter = "";
    steetAddressFilter = "";
    postalcodefrom = "";
    postalcodeTo = "";
    housetypeId = 0;
    rooftypeid = 0;
    postalcode = "";
    jobstatusid = 0;
    status=0;
    panelmodel = "";
    invertmodel = "";
    paymenttypeid = 0;
    // StartDate: moment.Moment;
    // EndDate: moment.Moment;
    jobTypeId=0;
    invertSuggestions: string[];
    panelSuggestions: string[];
    allStates: LeadStateLookupTableDto[];
    jobTypes: JobJobTypeLookupTableDto[];
    jobHouseTypes: JobHouseTypeLookupTableDto[];
    jobRoofTypes: JobRoofTypeLookupTableDto[];
    jobPaymentOptions: JobPaymentOptionLookupTableDto[];
    // date = '01/01/2021'
    jobAssignUser = 0;
    // public sampleDateRange: moment.Moment[] = [moment(this.date), moment().endOf('day')];
    firstrowcount = 0;
    last = 0;
    date = new Date();
    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);
    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 250;
    FiltersData = false;
    filterName = 'JobNumber';
    orgCode = '';

    constructor(
        injector: Injector,
        private _insallationServiceProxy: InstallationServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _router: Router,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _jobsServiceProxy: JobsServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _userServiceProxy: UserServiceProxy,
        private titleService: Title
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        this.entityHistoryEnabled = this.setIsEntityHistoryEnabled();
        this.titleService.setTitle(this.appSession.tenancyName + " |  Job Booking");
       
        this._commonLookupService.getAllJobTypeForTableDropdown().subscribe(result => {
            this.jobTypes = result;
        });
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
        this._jobsServiceProxy.getAllUsers().subscribe(result => {
            this.allUsers = result;
        });
        this._jobsServiceProxy.getAllRoofTypeForTableDropdown().subscribe(result => {
            this.jobRoofTypes = result;
        });
        this._jobsServiceProxy.getAllHouseTypeForTableDropdown().subscribe(result => {
            this.jobHouseTypes = result;
        });

        this._commonLookupService.getAllPaymentOptionForTableDropdown().subscribe(result => {
            this.jobPaymentOptions = result;
        });
      
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Jobs';
            log.section = 'Jobs';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.orgCode =this.allOrganizationUnits[0].code;
            this.getJobs();
        });
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + 82 ;
        }
        else {
            this.testHeight = this.testHeight - 82 ;
        }
    }
    changeOrgCode() {
        this.orgCode = this.allOrganizationUnits.find(x => x.id == this.organizationUnit).code;
    }

    private setIsEntityHistoryEnabled(): boolean {
        let customSettings = (abp as any).custom;
        return this.isGrantedAny('Pages.Administration.AuditLogs') && customSettings.EntityHistory && customSettings.EntityHistory.isEnabled && _.filter(customSettings.EntityHistory.enabledEntities, entityType => entityType === this._entityTypeFullName).length === 1;
    }


    filterPanelModel(event): void {
        this._jobsServiceProxy.getpanelmodel(event.query).subscribe(output => {
            this.panelSuggestions = output;
        });
    }

    filterInvertModel(event): void {
        this._jobsServiceProxy.getinvertmodel(event.query).subscribe(output => {
            this.invertSuggestions = output;
        });
    }

    getJobs(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        // if (this.sampleDateRange != null) {
        //     this.StartDate = this.sampleDateRange[0];
        //     this.EndDate = this.sampleDateRange[1];
        // } else {
        //     this.StartDate = null;
        //     this.EndDate = null;
        // }

        this.primengTableHelper.showLoadingIndicator();
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._insallationServiceProxy.getJobBookingInstallationList(
            this.filterName,
            this.organizationUnit,
            this.status,
            filterText_,
            this.stateFilter,
            this.steetAddressFilter,
            this.jobTypeNameFilter,
            this.jobStatusNameFilter,
            this.housetypeId,
            this.rooftypeid,
            this.jobstatusid,
            this.panelmodel,
            this.invertmodel,
            this.paymenttypeid,
            this.areaNameFilter,
            this.dateFilterType,
            this.postalcodefrom,
            this.postalcodeTo,
            this.startDate,
            this.endDate,
            this.jobTypeId,
            undefined,
            undefined,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.tableRecords = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            this.shouldShow = false;

            console.log(result.items);
        });
    }

    reloadPage(event): void {
        this.ExpandedView = true;
        this.paginator.changePage(this.paginator.getPage());
    }


    // setOpenRecord(id) {
    //     if (id === this.OpenRecordId) { this.OpenRecordId = 0; return; }
    //     this.OpenRecordId = id;
    // }

    showHistory(job: JobDto): void {
        this.entityTypeHistoryModal.show({
            entityId: job.id.toString(),
            entityTypeFullName: this._entityTypeFullName,
            entityTypeDescription: ''
        });
    }
    checkAll(ev) {

        this.tableRecords.forEach(x => x.job.isSelected = ev.target.checked);
        this.oncheckboxCheck();
        /// this.totalcount = this.tableRecords.forEach(x => x.lead.isSelected = ev.target.checked);
    }

    oncheckboxCheck() {

        this.count = 0;
        this.tableRecords.forEach(item => {
            if (item.job.isSelected == true) {
                this.count = this.count + 1;
            }
        })
    }

    isAllChecked() {
        if (this.tableRecords)
            return this.tableRecords.every(_ => _.job.isSelected);
    }

    submit(): void {
         let selectedids = [];
        // this.saving = true;
    
            this.primengTableHelper.records.forEach(function (job) {
                if (job.job.isSelected) {
                    selectedids.push(job.job.id);
                }
            });
 
            if (selectedids.length == 0) {
                this.notify.warn(this.l('NoLeadsSelected'));
                return;
            }
          
            if (this.assignLead == 0) {
                this.notify.warn(this.l('PleaseSelectUser'));
                return;
            }
            this._jobsServiceProxy.updateBookingManagerID(this.assignLead,selectedids)
                .pipe(finalize(() => {  }))
                .subscribe(() => {
                    this.notify.info(this.l('AssignedSuccessfully'));
                });
     
       
    }
    // exportToExcel(): void {
    //     this._jobsServiceProxy.getJobsToExcel(
    //     this.filterText,
    //         this.maxPanelOnFlatFilter == null ? this.maxPanelOnFlatFilterEmpty: this.maxPanelOnFlatFilter,
    //         this.minPanelOnFlatFilter == null ? this.minPanelOnFlatFilterEmpty: this.minPanelOnFlatFilter,
    //         this.maxPanelOnPitchedFilter == null ? this.maxPanelOnPitchedFilterEmpty: this.maxPanelOnPitchedFilter,
    //         this.minPanelOnPitchedFilter == null ? this.minPanelOnPitchedFilterEmpty: this.minPanelOnPitchedFilter,
    //         this.regPlanNoFilter,
    //         this.lotNumberFilter,
    //         this.peakMeterNoFilter,
    //         this.offPeakMeterFilter,
    //         this.enoughMeterSpaceFilter,
    //         this.isSystemOffPeakFilter,
    //         this.suburbFilter,
    //         this.stateFilter,
    //         this.jobTypeNameFilter,
    //         this.jobStatusNameFilter,
    //         this.roofTypeNameFilter,
    //         this.roofAngleNameFilter,
    //         this.elecDistributorNameFilter,
    //         this.leadCompanyNameFilter,
    //         this.elecRetailerNameFilter,
    //         this.paymentOptionNameFilter,
    //         this.depositOptionNameFilter,
    //         this.meterUpgradeNameFilter,
    //         this.meterPhaseNameFilter,
    //         this.promotionOfferNameFilter,
    //         this.houseTypeNameFilter,
    //         this.financeOptionNameFilter,
    //     )
    //     .subscribe(result => {
    //         this._fileDownloadService.downloadTempFile(result);
    //      });
    // }
    expandGrid() {
        this.ExpandedView = true;
    }
    navigateToLeadDetail(leadid): void {
        debugger;
        this.ExpandedView = !this.ExpandedView;
        this.ExpandedView = false;
        this.SelectedLeadId = leadid;
        this.viewLeadDetail.showDetail(leadid,null,12,0,'Job Booking');
    }
    
    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Job Booking';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Job Booking';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }
    
}
