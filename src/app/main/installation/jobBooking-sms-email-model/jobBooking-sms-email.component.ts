import { Component, ElementRef, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivityLogServiceProxy, CommonLookupDto, EmailTemplateServiceProxy, GetJobForEditOutput, GetLeadForSMSEmailDto, GetLeadForSMSEmailTemplateDto, GetLeadForViewDto, JobsServiceProxy, LeadDto, LeadsServiceProxy, SmsEmailDto, SmsTemplatesServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { EmailEditorComponent } from 'angular-email-editor';
import * as _ from 'lodash';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'jobBookingsmsemail',
  templateUrl: './jobBooking-sms-email.component.html',
})
export class JobBookingSmsEmailComponent extends AppComponentBase implements OnInit {
  @ViewChild("myNameElem") myNameElem: ElementRef;
  @ViewChild('addModal', { static: true }) modal: ModalDirective;
  ///@ViewChild('viewLeadDetail', {static: true}) viewLeadDetail: ViewMyLeadComponent;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  saving = false;
  id = 0;
  allEmailTemplates: CommonLookupDto[];
  allSMSTemplates: CommonLookupDto[];
  installer: GetJobForEditOutput;
  emailData = '';
  smsData = '';
  emailTemplate: number;
  uploadUrl: string;
  uploadedFiles: any[] = [];
  myDate = new Date();
  activityType: number;
  // item: GetLeadForViewDto;
  activityLog: SmsEmailDto = new SmsEmailDto();
  activityName = "";
  total = 0;
  credit = 0;
  customeTagsId = 0;
  role: string = '';
  leadCompanyName: any;
  subject = '';
  types = 1;
  email = "";
  mobile = "";
  ccbox = false;
  bccbox = false;
  fromemails: CommonLookupDto[];
  lead : GetLeadForSMSEmailDto = new GetLeadForSMSEmailDto();
  item : GetLeadForSMSEmailTemplateDto = new GetLeadForSMSEmailTemplateDto();


  // viewLeadDetail: ViewMyLeadComponent;
  constructor(
    injector: Injector,
    private _leadsServiceProxy: LeadsServiceProxy,
    private _emailTemplateServiceProxy: EmailTemplateServiceProxy,
    private _smsTemplateServiceProxy: SmsTemplatesServiceProxy,
    private _router: Router,
    private _jobsServiceProxy: JobsServiceProxy,
    private spinner: NgxSpinnerService,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
        , private _activityLogServiceProxy : ActivityLogServiceProxy
    ) {
    super(injector);
    this.item = new GetLeadForSMSEmailTemplateDto();
    this.lead = new GetLeadForSMSEmailDto();
  }

  @ViewChild(EmailEditorComponent)
  private emailEditor: EmailEditorComponent;

  ///private viewLeadDetail: ViewMyLeadComponent;

  ngOnInit(): void {
    this.activityLog.emailTemplateId = 0;
    this.activityLog.smsTemplateId = 0;
    this.activityLog.customeTagsId = 0;
    this.activityLog.subject = '';


  }
 sectionName = ''; 
  show(leadId: number, id: number, trackerid?: number,section = ''): void {
    this.sectionName = section;
    let log = new UserActivityLogDto();
    log.actionId = 79;
    log.actionNote ='Open ' + (id == 1 ? 'SMS' : 'Email');
    log.section = section;
    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {
    }); 
    this.activityLog.emailTemplateId = 0;
    this.activityLog.smsTemplateId = 0;
    this.activityLog.customeTagsId = 0;
    this.activityLog.subject = '';
    this.activityLog.body = "";
    this.activityLog.trackerId = trackerid;
    this.activityLog.leadId =leadId;
    if (id == 1) {
      this.id = id;
      this.activityType = 1;
    }
    else if (id == 2) {
      this.id = id;
      this.activityType = 2;
    }
    this.spinner.show();
    this._leadsServiceProxy.getOrgWiseDefultandownemailadd(leadId).subscribe(result => {
      debugger;
      this.fromemails = result;
      this.activityLog.emailFrom = this.fromemails[0].displayName;
    });

    this._activityLogServiceProxy.getLeadForSMSEmailActivityInvoiceIssued(leadId).subscribe(result => {
      this.lead = result;
      this.leadCompanyName = result.companyName;
      this.mobile = result.mobile;
      this.email = result.email;
      this.selection();
      this.spinner.hide();
      this.modal.show();
    });

    // this._leadsServiceProxy.getLeadForView(leadId,0).subscribe(result => {
    //   this.item = result;
    //   this.leadCompanyName = result.lead.companyName;
    //   this.mobile = result.lead.mobile;
    //   this.email = result.lead.email;
    //   this.selection();
    //   this.spinner.hide();
    //   this.modal.show();
    // });
    this._jobsServiceProxy.getJobdetailbylead(leadId).subscribe(result => {
      debugger;
      this.installer = result;
    });
    this._leadsServiceProxy.getallEmailTemplates(leadId).subscribe(result => {
      this.allEmailTemplates = result;
    });

    this._leadsServiceProxy.getallSMSTemplates(leadId).subscribe(result => {
      this.allSMSTemplates = result;
    });

    this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
      this.role = result;
    });

  }

  selection(): void {

    this.activityLog.body = '';
    this.activityLog.emailTemplateId = 0;
    this.activityLog.customeTagsId = 0;
    if (this.activityType == 1) {
      this.activityName = "SMS To ";
    }
    else if (this.activityType == 2) {
      this.activityName = "Email To ";
    }
  }

  ontypechange(event) {
    debugger;
    if (this.activityType == 1) {
      if (event.target.value == 1) {
        this.mobile = this.lead.mobile;
      } else {
        if (event.target.value == 2 && this.installer.installerID != null && this.installer.installerID != 0) {
          if (this.installer.installerMobile != null) {
            this.mobile = this.installer.installerMobile;
          } else {
            this.notify.warn('Installer MobileNumber Not Found');
            this.mobile = "";
          }

        } else {
          this.notify.warn('Installer Not Found');
          this.mobile = "";
        }
      }
    } else {
      if (event.target.value == 1) {
        this.email = this.lead.email;
      } else {
        if (event.target.value == 2 && this.installer.installerID != null && this.installer.installerID != 0) {
          if (this.installer.installerEmail != null) {
            this.email = this.installer.installerEmail;
          } else {
            this.notify.warn('Installer Email Not Found');
            this.email = "";
          }
        } else {
          this.notify.warn('No Installer Found');
          this.email = "";
        }
      }

    }
  }
  save(): void {
    debugger;
    // this.activityLog.leadId = this..id;
    this.email = this.installer.installerMobile;
    this.mobile =  this.installer.installerEmail;
    if (this.activityType == 1) {
      if (this.lead.mobile != null && this.lead.mobile != "") {
        this.saving = true;
        if (this.role != 'Admin') {
          if (this.total > 320) {
            this.notify.warn(this.l('You Can Not Add more than 320 characters'));
            this.saving = false;
          } else {
            this._jobsServiceProxy.sendSms(this.activityLog)
              .pipe(finalize(() => { this.saving = false; }))
              .subscribe(() => {
                let log = new UserActivityLogDto();
                log.actionId = 6;
                log.actionNote ='SMS Sent';
                log.section = this.sectionName;
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
                this.modal.hide();
                this.notify.info(this.l('SmsSendSuccessfully'));
                this.modalSave.emit(null);
                this.activityLog.body = "";
                this.activityLog.emailTemplateId = 0;
                this.activityLog.smsTemplateId = 0;
                this.activityLog.customeTagsId = 0;
                this.activityLog.subject = '';
              });
          }
        }
        else {
          this._jobsServiceProxy.sendSms(this.activityLog)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
              let log = new UserActivityLogDto();
              log.actionId = 6;
              log.actionNote ='SMS Sent';
              log.section = this.sectionName;
              this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                  .subscribe(() => {
              }); 
              this.modal.hide();
              this.notify.info(this.l('SmsSendSuccessfully'));
              this.modalSave.emit(null);
              this.activityLog.body = "";
              this.activityLog.emailTemplateId = 0;
              this.activityLog.smsTemplateId = 0;
              this.activityLog.customeTagsId = 0;
              this.activityLog.subject = '';
            });
        }
      }
      else {
        this.notify.warn(this.l('Mobile Number Not Found'));
      }
    }
    else {
      if (this.lead.email != null && this.lead.email != "") {
        this.saving = true;
        this._jobsServiceProxy.sendEmail(this.activityLog)
          .pipe(finalize(() => { this.saving = false; }))
          .subscribe(() => {
            let log = new UserActivityLogDto();
            log.actionId = 7;
            log.actionNote ='Email Sent';
            log.section = this.sectionName;
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
            this.notify.info(this.l('EmailSendSuccessfully'));
            this.modal.hide();
            this.modalSave.emit(null);
            this.activityLog.body = "";
            this.activityLog.emailTemplateId = 0;
            this.activityLog.smsTemplateId = 0;
            this.activityLog.customeTagsId = 0;
            this.activityLog.subject = '';
          });
      }
      else {
        this.notify.warn(this.l('Email Not Found'));
      }
    }
  }

  countCharcters(): void {

    if (this.role != 'Admin') {
      this.total = this.activityLog.body.length;
      this.credit = Math.ceil(this.total / 160);
      if (this.total > 320) {
        this.notify.warn(this.l('You Can Not Add more than 320 characters'));
      }
    }
    else {
      this.total = this.activityLog.body.length;
      this.credit = Math.ceil(this.total / 160);
    }


  }

  close(): void {
    this.modal.hide();
  }
  opencc(): void {
    this.ccbox = !this.ccbox;
  }
  openbcc(): void {
    this.bccbox = !this.bccbox;
  }

  onTagChange(event): void {

    const id = event.target.value;
    if (id == 1) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Customer.Name}}";
      } else {
        this.activityLog.body = "{{Customer.Name}}";
      }

    } else if (id == 2) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Customer.Mobile}}";
      } else {
        this.activityLog.body = "{{Customer.Mobile}}";
      }
    } else if (id == 3) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Customer.Phone}}";
      } else {
        this.activityLog.body = "{{Customer.Phone}}";
      }
    } else if (id == 4) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Customer.Email}}";
      } else {
        this.activityLog.body = "{{Customer.Email}}";
      }
    } else if (id == 5) {

      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Customer.Address}}";
      } else {
        this.activityLog.body = "{{Customer.Address}}";
      }
    } else if (id == 6) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Sales.Name}}";
      } else {
        this.activityLog.body = "{{Sales.Name}}";
      }
    } else if (id == 7) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Sales.Mobile}}";
      } else {
        this.activityLog.body = "{{Sales.Mobile}}";
      }
    } else if (id == 8) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Sales.Email}}";
      } else {
        this.activityLog.body = "{{Sales.Email}}";
      }
    }
    else if (id == 9) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Quote.ProjectNo}}";
      } else {
        this.activityLog.body = "{{Quote.ProjectNo}}";
      }
    }
    else if (id == 10) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Quote.SystemCapacity}}";
      } else {
        this.activityLog.body = "{{Quote.SystemCapacity}}";
      }
    }
    else if (id == 11) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Quote.AllproductItem}}";
      } else {
        this.activityLog.body = "{{Quote.AllproductItem}}";
      }
    }
    else if (id == 12) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Quote.TotalQuoteprice}}";
      } else {
        this.activityLog.body = "{{Quote.TotalQuoteprice}}";
      }
    }
    else if (id == 13) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Quote.InstallationDate}}";
      } else {
        this.activityLog.body = "{{Quote.InstallationDate}}";
      }
    }
    else if (id == 14) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Quote.InstallerName}}";
      } else {
        this.activityLog.body = "{{Quote.InstallerName}}";
      }
    }
  }


  saveDesign() {
    if (this.activityLog.emailTemplateId == 0) {
      this.saving = true;
      const emailHTML = this.activityLog.body;
      this.setHTML(emailHTML)
    }
    else {
      this.saving = true;
      this.emailEditor.editor.exportHtml((data) =>
        this.setHTML(data.html)
      );
    }
  }

  setHTML(emailHTML) {
    // let htmlTemplate = this.getEmailTemplate(emailHTML);
    // this.activityLog.body = htmlTemplate;
    // this.save();
    debugger;
    this.showMainSpinner();
    if(!this.item.id || this.item.id == 0){
      this._activityLogServiceProxy.getLeadForSMSEmailTemplate(this.activityLog.leadId,0,0).subscribe(result => {
        this.item = result;
        let htmlTemplate = this.getEmailTemplate(emailHTML,result);
        this.activityLog.body = htmlTemplate;
        this.save();
        this.hideMainSpinner(); 
      }, err => {
          this.hideMainSpinner(); 
      });
    }
    else{
      let htmlTemplate = this.getEmailTemplate(emailHTML,this.item);
      this.activityLog.body = htmlTemplate;
      this.save();
      this.hideMainSpinner(); 
    }
  }

  getEmailTemplate(emailHTML, tempdata :GetLeadForSMSEmailTemplateDto) {
    //let myTemplateStr = "Hello {{user.name}}, you are {{user.age.years}} years old";
    let myTemplateStr = emailHTML;
   // let addressValue = this.item.lead.address + " " + this.item.lead.suburb + ", " + this.item.lead.state + "-" + this.item.lead.postCode;
    let myVariables = {
      Customer: {
        Name: tempdata.companyName,
        Address: tempdata.address,
        Mobile: tempdata.mobile,
        Email: tempdata.email,
        Phone: tempdata.phone,
        SalesRep: tempdata.currentAssignUserName
      },
      Sales: {
        Name: tempdata.currentAssignUserName,
        Mobile: tempdata.currentAssignUserMobile,
        Email: tempdata.currentAssignUserEmail
      },
      Quote: {
        ProjectNo: tempdata.jobNumber,
        SystemCapacity: tempdata.systemCapacity,
        AllproductItem: tempdata.qunityAndModelList,
        TotalQuoteprice: tempdata.totalQuotaion,
        InstallationDate: tempdata.installationDate,
        InstallerName: tempdata.installerName,
      },
      Freebies: {
        DispatchedDate: tempdata.dispatchedDate,
        TrackingNo: tempdata.trackingNo,
        TransportCompany: tempdata.transportCompanyName,
        TransportLink: tempdata.transportLink,
        PromoType: tempdata.promoType,
      },
      Invoice: {
        UserName: tempdata.userName,
        UserMobile: tempdata.userMobile,
        UserEmail: tempdata.userEmail,
        Owning: tempdata.owing,
      },
      Organization: {
        orgName: tempdata.orgName,
        orgEmail: tempdata.orgEmail,
        orgMobile: tempdata.orgMobile,
        orglogo: AppConsts.docUrl + "/" + tempdata.orglogo,
      },
      Review: {
        link: tempdata.link,
      },
    }

    // use custom delimiter {{ }}
    _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;

    // interpolate
    let compiled = _.template(myTemplateStr);
    let myTemplateCompiled = compiled(myVariables);
    return myTemplateCompiled;
  }

  editorLoaded() {

    this.activityLog.body = "";
    if ((this.activityLog.emailTemplateId != 0 && this.activityLog.emailTemplateId !== null && this.activityLog.emailTemplateId !== undefined)) {
      this._jobsServiceProxy.getEmailTemplateForEditForEmail(this.activityLog.emailTemplateId).subscribe(result => {
        this.activityLog.subject = result.emailTemplate.subject;
        this.emailData = result.emailTemplate.body;
        if (this.emailData != "") {
          // this.emailData = this.getEmailTemplate(this.emailData);
          // this.emailEditor.editor.loadDesign(JSON.parse(this.emailData));
          this.showMainSpinner();
          if(!this.item.id || this.item.id == 0){
            this._activityLogServiceProxy.getLeadForSMSEmailTemplate(this.activityLog.leadId,0,0).subscribe(result => {
              this.item = result;
              this.emailData = this.getEmailTemplate(this.emailData,result);
              this.emailEditor.editor.loadDesign(JSON.parse(this.emailData));
              this.hideMainSpinner(); 
            }, err => {
                this.hideMainSpinner(); 
            });
          }
          else{
            this.emailData = this.getEmailTemplate(this.emailData, this.item);
            this.emailEditor.editor.loadDesign(JSON.parse(this.emailData));
            this.hideMainSpinner();
          }
        }
      });
    }

  }

  smseditorLoaded() {

    this.activityLog.body = "";
    if ((this.activityLog.smsTemplateId != 0 && this.activityLog.smsTemplateId !== null && this.activityLog.smsTemplateId !== undefined)) {
      this._jobsServiceProxy.getSmsTemplateForEditForSms(this.activityLog.smsTemplateId).subscribe(result => {
        this.smsData = result.smsTemplate.text;
        if (this.smsData != "") {
          this.setsmsHTML(this.smsData)
        }
      });
    }

  }


  setsmsHTML(smsHTML) {
    // let htmlTemplate = this.getsmsTemplate(smsHTML);
    // this.activityLog.body = htmlTemplate;
    // this.countCharcters();
    debugger;
    this.showMainSpinner();
    if(!this.item.id || this.item.id == 0){
      this._activityLogServiceProxy.getLeadForSMSEmailTemplate(this.activityLog.leadId,0,0).subscribe(result => {
        this.item = result;
        let htmlTemplate = this.getsmsTemplate(smsHTML,result);
        this.activityLog.body = htmlTemplate;
        this.countCharcters();
        this.hideMainSpinner(); 
      }, err => {
          this.hideMainSpinner(); 
      });
    }
    else{
      let htmlTemplate = this.getsmsTemplate(smsHTML,this.item);
      this.activityLog.body = htmlTemplate;
      this.countCharcters();
      this.hideMainSpinner(); 
    }
  }

  getsmsTemplate(smsHTML, tempdata :GetLeadForSMSEmailTemplateDto) {
    //let myTemplateStr = "Hello {{user.name}}, you are {{user.age.years}} years old";
    let myTemplateStr = smsHTML;
   // let addressValue = this.item.lead.address + " " + this.item.lead.suburb + ", " + this.item.lead.state + "-" + this.item.lead.postCode;
    let myVariables = {
      Customer: {
        Name: tempdata.companyName,
        Address: tempdata.address,
        Mobile: tempdata.mobile,
        Email: tempdata.email,
        Phone: tempdata.phone,
        SalesRep: tempdata.currentAssignUserName
      },
      Sales: {
        Name: tempdata.currentAssignUserName,
        Mobile: tempdata.currentAssignUserMobile,
        Email: tempdata.currentAssignUserEmail
      },
      Quote: {
        ProjectNo: tempdata.jobNumber,
        SystemCapacity: tempdata.systemCapacity,
        AllproductItem: tempdata.qunityAndModelList,
        TotalQuoteprice: tempdata.totalQuotaion,
        InstallationDate: tempdata.installationDate,
        InstallerName: tempdata.installerName,
      },
      Freebies: {
        DispatchedDate: tempdata.dispatchedDate,
        TrackingNo: tempdata.trackingNo,
        TransportCompany: tempdata.transportCompanyName,
        TransportLink: tempdata.transportLink,
        PromoType: tempdata.promoType,
      },
      Invoice: {
        UserName: tempdata.userName,
        UserMobile: tempdata.userMobile,
        UserEmail: tempdata.userEmail,
        Owning: tempdata.owing,
      },
      Organization: {
        orgName: tempdata.orgName,
        orgEmail: tempdata.orgEmail,
        orgMobile: tempdata.orgMobile,
        orglogo: AppConsts.docUrl + "/" + tempdata.orglogo,
      },
      Review: {
        link: tempdata.link,
      },
    }

    // use custom delimiter {{ }}
    _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;

    // interpolate
    let compiled = _.template(myTemplateStr);
    let myTemplateCompiled = compiled(myVariables);
    return myTemplateCompiled;
  }
}




