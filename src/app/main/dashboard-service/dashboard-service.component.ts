import { Component, Injector, ViewEncapsulation } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DashboardCustomizationConst } from '@app/shared/common/customizable-dashboard/DashboardCustomizationConsts';
import { Title } from '@angular/platform-browser';
import { DatePipe } from '@angular/common';
import { UserActivityLogServiceProxy, UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './dashboard-service.component.html',
    styleUrls: ['./dashboard-service.component.less'],
    encapsulation: ViewEncapsulation.None 
})

export class DashboardServiceComponent extends AppComponentBase {
    dashboardName = DashboardCustomizationConst.dashboardNames.defaultTenantDashboard;

    pipe = new DatePipe('en-US');
    today = this.pipe.transform(new Date(), 'MMM dd');

    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private titleService: Title) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Dashboard Service");
        let log = new UserActivityLogDto();
            log.actionId =79;
            log.actionNote ='Open';
            log.section = 'Dashboard Service';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
    }
    showchild: boolean = true;
    shouldShow: boolean = false;
    addTaskShow: boolean = false;
}
