import { Component, Injector, ViewEncapsulation, Input, Output, EventEmitter, ViewChild, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotifyService, TokenService, IAjaxResponse } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { finalize } from 'rxjs/operators';
import * as _ from 'lodash';
import * as moment from 'moment';
import { Location } from '@angular-material-extensions/google-maps-autocomplete';
import { AppConsts } from '@shared/AppConsts';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { PromotionUsersServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
    selector: 'promoStopRespmodel',
    templateUrl: './stop-responce.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class PromotionStopResponceComponent extends AppComponentBase implements OnInit {
   
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    active1 = true;
    saving = false;
    comment = "";
    leadId: number = 0;
    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _promotionUsersServiceProxy: PromotionUsersServiceProxy,
        private _activatedRoute: ActivatedRoute,
    ) {
        super(injector);
    }
    ngOnInit(): void {
    }

    SectionName = '';
    deattilshow(leadId?: number, section = ''): void {
        this.SectionName = section;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote = 'Open Promotion Responce' ;
        log.section = this.SectionName;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
        debugger;
        this.leadId = leadId;
        this.active = true;
        this.modal.show();
    }

    save(): void {
        debugger;
        this.saving = true;
        this._promotionUsersServiceProxy.updateResponce(this.leadId, this.comment)
        .pipe(finalize(() => { this.saving = false; }))
            .subscribe(result => {
                let log = new UserActivityLogDto();
                log.actionId = 22;
                log.actionNote = 'Promotion SMS Send' ;
                log.section = this.SectionName;
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
                this.comment = "";
                this.saving = false;
                this.notify.info(this.l('Sucessfully Updated'));
                this.modal.hide();
                this.active = false;
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}