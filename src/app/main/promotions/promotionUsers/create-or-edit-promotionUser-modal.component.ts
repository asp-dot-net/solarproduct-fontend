﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {
    PromotionUsersServiceProxy, CreateOrEditPromotionUserDto, PromotionUserPromotionLookupTableDto
    , PromotionUserLeadLookupTableDto
    , PromotionUserPromotionResponseStatusLookupTableDto
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { AppConsts } from '@shared/AppConsts';
import { HttpClient } from '@angular/common/http';
import { FileUpload } from 'primeng';

@Component({
    selector: 'createOrEditPromotionUserModal',
    templateUrl: './create-or-edit-promotionUser-modal.component.html'
})
export class CreateOrEditPromotionUserModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @ViewChild('ExcelFileUpload', { static: false }) excelFileUpload: FileUpload;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    promotionUser: CreateOrEditPromotionUserDto = new CreateOrEditPromotionUserDto();
    promotionTitleFilter = 0;
    allPromotions: PromotionUserPromotionLookupTableDto[];
    uploadUrl: string;

    constructor(
        injector: Injector,
        private _httpClient: HttpClient,
        private _promotionUsersServiceProxy: PromotionUsersServiceProxy
    ) {
        super(injector);
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/Users/ImportPromotionResponse';
    }

    show(organizationUnit?: number): void {
        this.active = true;
        this._promotionUsersServiceProxy.getAllPromotionForTableDropdown().subscribe(result => {
            this.allPromotions = result;
        });
        this.modal.show();
    }

    save(): void {
        this.saving = true;
        this._promotionUsersServiceProxy.createOrEdit(this.promotionUser)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    uploadExcel(data: { files: File }): void {
        const formData: FormData = new FormData();
        const file = data.files[0];
        formData.append('file', file, file.name);

        this._httpClient
            .post<any>(this.uploadUrl, formData)
            .pipe(finalize(() => this.excelFileUpload.clear()))
            .subscribe(response => {
                if (response.success) {
                    this.notify.success(this.l('ImportUsersProcessStart'));
                } else if (response.error != null) {
                    this.notify.error(this.l('ImportUsersUploadFailed'));
                }
            });
    }

    onUploadExcelError(): void {
        this.notify.error(this.l('ImportUsersUploadFailed'));
    }
}
