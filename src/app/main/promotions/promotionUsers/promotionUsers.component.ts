﻿import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
    PromotionUsersServiceProxy, PromotionUserDto,
    PromotionsServiceProxy, GetPromotionForViewDto,
    PromotionUserPromotionLookupTableDto,
    PromotionUserPromotionResponseStatusLookupTableDto,
    PromotionPromotionTypeLookupTableDto,
    LeadsServiceProxy,
    LeadUsersLookupTableDto,
    JobStatusTableDto,
    JobsServiceProxy,
    OrganizationUnitDto,
    UserServiceProxy,
    CommonLookupServiceProxy,
    UserActivityLogDto,
    UserActivityLogServiceProxy,
} from '@shared/service-proxies/service-proxies';

import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
// import { CreateOrEditPromotionUserModalComponent } from './create-or-edit-promotionUser-modal.component';

// import { ViewPromotionUserModalComponent } from './view-promotionUser-modal.component';
import { ViewPromotionModalComponent } from '.././promotions/view-promotion-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
// import { ViewMyLeadComponent } from '@app/main/myleads/myleads/view-mylead.component';
import { finalize } from 'rxjs/operators';
import { Title } from '@angular/platform-browser';
import { ViewApplicationModelComponent } from '@app/main/jobs/jobs/view-application-model/view-application-model.component';

@Component({
    templateUrl: './promotionUsers.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class PromotionUsersComponent extends AppComponentBase {
    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    organizationUnitlength: number = 0;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    // @ViewChild('createOrEditPromotionUserModal', { static: true }) createOrEditPromotionUserModal: CreateOrEditPromotionUserModalComponent;
    // @ViewChild('viewPromotionUserModalComponent', { static: true }) viewPromotionUserModal: ViewPromotionUserModalComponent;
    @ViewChild('viewPromotionModalComponent', { static: true }) viewPromotionModalComponent: ViewPromotionModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    // @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
    @ViewChild('viewApplicationModal', { static: true }) viewApplicationModal: ViewApplicationModelComponent;

    excelorcsvfile = 0;
    readunreadsmstag = 0;
    advancedFiltersAreShown = false;
    filterText = '';
    saving = false;
    maxResponseDateFilter: moment.Moment;
    minResponseDateFilter: moment.Moment;
    responseMessageFilter = '';
    promotionTitleFilter = 0;
    tableRecords: any;
    leadCopanyNameFilter = '';
    promotionResponseStatusNameFilter = '';
    promotionResponseStatusIdFilter = 0;
    allPromotionResponseStatuss: PromotionUserPromotionResponseStatusLookupTableDto[];
    allPromotions: PromotionUserPromotionLookupTableDto[];
    //sampleDateRange: moment.Moment[];
    FiltersData = true;
    allPromotionTypes: PromotionPromotionTypeLookupTableDto[];
    filteredTeams: LeadUsersLookupTableDto[];
    filteredManagers: LeadUsersLookupTableDto[];
    filteredReps: LeadUsersLookupTableDto[];
    filteredUsers: LeadUsersLookupTableDto[];
    alljobstatus: JobStatusTableDto[];
    responseType = 0;
    salesRepId = 0;
    teamId: number = 0;
    jobStatusIDFilter = [];
    // date = new Date();
    // firstDay = moment(new Date(this.date.getFullYear(), this.date.getMonth(), 1));
    // sampleDateRange: moment.Moment[] = [this.firstDay, moment().add(0, 'days').endOf('day')];
    date = new Date();
    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);
    // sampleDateRange: moment.Moment[] = [moment(this.date),moment(this.date)];
    responseDateFilter = 'Responce';
    // StartDate: moment.Moment;
    // EndDate: moment.Moment;
    totalPromotion = 0;
    interestedCount = 0;
    notInterestedCount = 0;
    otherCount = 0;
    unhandle = 0;
    perinterestedCount = 0;
    pernotInterestedCount = 0;
    perotherCount = 0;
    perunhandle = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnit: number = 0;
    promotionSold = 0;
    promotionProject = 0;
    noReplay = 0;
    perpromotionSold = 0;
    perpromotionProject = 0;
    pernoReplay = 0;
    ExpandedView: boolean = true;
    SelectedLeadId: number = 0;
    promotype = 0;
    readunreadsms = 2;
    firstrowcount = 0;
    last = 0;
    role: string = '';
    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 260;
    toggle: boolean = true;
    filterName = 'JobNumber';
    orgCode = 'AS';
    userId = 0;

    change() {
        this.toggle = !this.toggle;
      }

    constructor(
        injector: Injector,
        private _promotionUsersServiceProxy: PromotionUsersServiceProxy,
        private _promotionsServiceProxy: PromotionsServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _jobsServiceProxy: JobsServiceProxy,
        private _userServiceProxy: UserServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private titleService: Title
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Promotion Tracker");
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
            this.role = result;
        });

        this._commonLookupService.getTeamForFilter().subscribe(teams => {
            this.filteredTeams = teams;
            //this.teamId = teams[0].id;
        });

        this._promotionUsersServiceProxy.getAllPromotionResponseStatusForTableDropdown().subscribe(result => {
            this.allPromotionResponseStatuss = result;
        });

        this._promotionUsersServiceProxy.getAllPromotionForTableDropdown().subscribe(result => {
            this.allPromotions = result;
        });

        this._commonLookupService.getAllJobStatusTableDropdown().subscribe(result => {
            this.alljobstatus = result;
        });

        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Promotion Tracker';
            log.section = 'Promotion Tracker';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.orgCode = this.allOrganizationUnits[0].code;

            // this._leadsServiceProxy.getSalesRepForFilter(this.allOrganizationUnits[0].id, this.teamId).subscribe(rep => {
            //     this.filteredReps = rep;
            // });

            this.bindSalesRep();
            this.bindUsers();
            this.getPromotionUsers();
            // this.getCount(this.organizationUnit);
        });
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }

        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }

    bindSalesRep()
    {
        this._leadsServiceProxy.getSalesRepForFilter(this.organizationUnit, this.teamId).subscribe(rep => {
            this.filteredReps = rep;
        });
    }

    bindUsers(){
        this._commonLookupService.getAllUsersByRoleNameTableDropdown("User",this.organizationUnit).subscribe(u => {
            this.filteredUsers = u;
        })
    }
    // getCount(organizationUnit: number): void {
    //     this._jobsServiceProxy.getAllApplicationTrackerCount(this.organizationUnit).subscribe(result => {
    //         this.totalPromotion = result.totalPromotion;
    //         this.interestedCount = result.interestedCount;
    //         this.notInterestedCount = result.notInterestedCount;
    //         this.otherCount = result.otherCount;
    //         this.unhandle = result.unhandle;
    //         this.promotionSold = result.promotionSold;
    //         this.promotionProject = result.promotionProject;
    //         this.noReplay = result.noReplay;
    //     });
    // }

    getAllPromotionType() {
        this._promotionsServiceProxy.getAllPromotionTypeForTableDropdown().subscribe(result => {
            this.allPromotionTypes = result;
        });
    }

    updateResponseStatusFor(promotionUserId, promomotionResponseStatusId) {
        this.message.confirm(
            '',
            'Are you sure you want to change the response status?',
            (isConfirmed) => {
                if (isConfirmed) {
                    this._promotionUsersServiceProxy.updatePromotionResponseStatus(
                        promotionUserId,
                        promomotionResponseStatusId)
                        .subscribe(result => {
                            let log = new UserActivityLogDto();
                            log.actionId = 6;
                            log.actionNote ='Sms Send From Promotion Tacker';
                            log.section = 'Promotion Tracker';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            }); 
                            this.notify.info('Response updated successfully');
                            this.getPromotionUsers();
                        });
                }
            }
        );
    }

    getPromotionUsers(event?: LazyLoadEvent) {
        debugger;
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        // if (this.sampleDateRange != null) {
        //     this.StartDate = this.sampleDateRange[0];
        //     this.EndDate = this.sampleDateRange[1];
        // } else {
        //     this.StartDate = null;
        //     this.EndDate = null;
        // }


        this.primengTableHelper.showLoadingIndicator();
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._promotionUsersServiceProxy.getAll(
            this.filterName,
            filterText_,
            this.startDate,
            this.endDate,
            this.organizationUnit,
            this.responseMessageFilter,
            this.promotionTitleFilter,
            this.leadCopanyNameFilter,
            this.promotionResponseStatusNameFilter,
            this.promotionResponseStatusIdFilter != 0 ? this.promotionResponseStatusIdFilter : undefined,
            this.jobStatusIDFilter,
            this.teamId,
            this.salesRepId,
            this.responseDateFilter,
            this.promotype,
            this.readunreadsms,
            this.userId,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.tableRecords = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            if (result.totalCount > 0) {

                this.totalPromotion = result.items[0].totalPromotion;
                this.interestedCount = result.items[0].interestedCount;
                this.notInterestedCount = result.items[0].notInterestedCount;
                this.otherCount = result.items[0].otherCount;
                this.unhandle = result.items[0].unhandle;
                this.promotionSold = result.items[0].promotionSold;
                this.promotionProject = result.items[0].promotionProject;
                
                this.perinterestedCount = Math.floor((this.interestedCount / this.totalPromotion)*100);
                this.pernotInterestedCount = Math.floor((this.notInterestedCount / this.totalPromotion)*100);
                this.perotherCount = Math.floor((this.otherCount / this.totalPromotion)*100);
                this.perunhandle = Math.floor((this.unhandle / this.totalPromotion)*100);
                this.perpromotionSold = Math.floor((this.promotionSold / this.totalPromotion)*100);
                this.perpromotionProject = Math.floor((this.promotionProject / this.totalPromotion)*100);
            } else {
                this.totalPromotion = 0;
                this.interestedCount = 0;
                this.notInterestedCount = 0;
                this.otherCount = 0;
                this.unhandle = 0;
                this.promotionSold = 0;
                this.promotionProject = 0;
                this.noReplay = 0;
            }

            this.shouldShow = false;
            // if (result.totalCount > 0) {
            //     this.totalPromotion = result.items[0].totalPromotion;
            //     this.interestedCount = result.items[0].interestedCount;
            //     this.notInterestedCount = result.items[0].notInterestedCount;
            //     this.otherCount = result.items[0].otherCount;
            //     this.unhandle = result.items[0].unhandle;
            //     this.promotionSold = result.items[0].promotionSold;
            //     this.promotionProject = result.items[0].promotionProject;
            //     this.noReplay = result.items[0].noReplay;


            //     this.perinterestedCount = Math.floor((result.items[0].interestedCount / this.totalPromotion) * 100);
            //     this.pernotInterestedCount = Math.floor((result.items[0].notInterestedCount / this.totalPromotion) * 100);
            //     this.perotherCount = Math.floor((result.items[0].otherCount / this.totalPromotion) * 100);
            //     this.perunhandle = Math.floor((result.items[0].unhandle / this.totalPromotion) * 100);
            //     this.perpromotionSold = Math.floor((result.items[0].promotionSold / this.totalPromotion) * 100);
            //     this.perpromotionProject = Math.floor((result.items[0].promotionProject / this.totalPromotion) * 100);
            //     this.pernoReplay = Math.floor((result.items[0].noReplay / this.totalPromotion) * 100);

            // } else {
            //     this.totalPromotion = 0;
            //     this.interestedCount = 0;
            //     this.notInterestedCount = 0;
            //     this.otherCount = 0;
            //     this.unhandle = 0;
            //     this.promotionSold = 0;
            //     this.promotionProject = 0;
            //     this.noReplay = 0;
            // }
        });
    }
    getLeadSummaryCount() {
        debugger;
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._promotionUsersServiceProxy.getAllCount(
            this.filterName,
            filterText_,
            this.startDate,
            this.endDate,
            this.organizationUnit,
            this.responseMessageFilter,
            this.promotionTitleFilter,
            this.leadCopanyNameFilter,
            this.promotionResponseStatusNameFilter,
            this.promotionResponseStatusIdFilter != 0 ? this.promotionResponseStatusIdFilter : undefined,
            this.jobStatusIDFilter,
            this.teamId,
            this.salesRepId,
            this.responseDateFilter,
            this.promotype,
            this.readunreadsms,
            this.userId,
            undefined,
            undefined,
            undefined,
        ).subscribe(result => {
            debugger;
            if (result) {
                this.totalPromotion = result.totalPromotion;
                this.interestedCount = result.interestedCount;
                this.notInterestedCount = result.notInterestedCount;
                this.otherCount = result.otherCount;
                this.unhandle = result.unhandle;
                this.promotionSold = result.promotionSold;
                this.promotionProject = result.promotionProject;
                this.noReplay = result.noReplay;


                this.perinterestedCount = Math.floor((result.interestedCount / this.totalPromotion) * 100);
                this.pernotInterestedCount = Math.floor((result.notInterestedCount / this.totalPromotion) * 100);
                this.perotherCount = Math.floor((result.otherCount / this.totalPromotion) * 100);
                this.perunhandle = Math.floor((result.unhandle / this.totalPromotion) * 100);
                this.perpromotionSold = Math.floor((result.promotionSold / this.totalPromotion) * 100);
                this.perpromotionProject = Math.floor((result.promotionProject / this.totalPromotion) * 100);
                this.pernoReplay = Math.floor((result.noReplay / this.totalPromotion) * 100);

            }
        });
    }
    mark(id: number): void {
        this.message.confirm(
            this.l('AreYouSureWanttoMark'),
            this.l('Mark'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._promotionUsersServiceProxy.markAsReadPromotionSms(id)
                        .subscribe(() => {
                            this.getPromotionUsers()
                            this.notify.success(this.l('SuccessfullyMark'));
                        });
                }
            }
        );
    }

    reloadPage(event): void {
        this.ExpandedView = true;
        this.paginator.changePage(this.paginator.getPage());
    }

    // createPromotionUser(): void {
    //     this.createOrEditPromotionUserModal.show();
    // }

    deletePromotionUser(promotionUser: PromotionUserDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._promotionUsersServiceProxy.delete(promotionUser.id)
                        .subscribe(() => {
                            this.reloadPage(event);
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }
    changeOrgCode() {
        this.orgCode = this.allOrganizationUnits.find(x => x.id == this.organizationUnit).code;
        this.bindSalesRep();
        this.bindUsers();
    }
    exportToExcel(excelorcsv): void {
        this.excelorcsvfile = excelorcsv
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._promotionUsersServiceProxy.getPromotionUsersToExcel(
            this.filterName,
            filterText_,
            this.startDate,
            this.endDate,
            this.organizationUnit,
            this.responseMessageFilter,
            this.promotionTitleFilter,
            this.leadCopanyNameFilter,
            this.promotionResponseStatusNameFilter,
            this.promotionResponseStatusIdFilter != 0 ? this.promotionResponseStatusIdFilter : undefined,
            this.jobStatusIDFilter,
            this.teamId,
            this.salesRepId,
            this.responseDateFilter,
            this.promotype,
            this.readunreadsms,
            this.excelorcsvfile)
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            }
        );
    }

    expandGrid() {
        this.ExpandedView = true;
    }

    navigateToLeadDetail(leadid): void {
        // this.ExpandedView = !this.ExpandedView;
        // this.ExpandedView = false;
        // this.SelectedLeadId = leadid;
        // this.viewLeadDetail.showDetail(leadid, null, 15);
    }

    checkAll(ev) {
        debugger;
        this.tableRecords.forEach(x => x.isSelected = ev.target.checked);
    }

    isAllChecked() {
        if (this.tableRecords)
            return this.tableRecords.every(_ => _.isSelected);
    }


    submit(): void {
        debugger;
        let selectedids = [];
        this.saving = true;
        this.primengTableHelper.records.forEach(function (record) {
            if (record.isSelected) {
                selectedids.push(record.promotionUser.id);
            }
        });
        if (selectedids.length == 0) {
            this.notify.warn(this.l('NoDataSelected'));
            this.saving = false;
            return;
        }
        if (this.readunreadsmstag == 0) {
            this.notify.warn("PleaseSelectValue");
            this.saving = false;
            return;
        }
        this._promotionUsersServiceProxy.markAsReadPromotionSmsInBulk(this.readunreadsmstag, selectedids)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                let log = new UserActivityLogDto();
                log.actionId = 82;
                log.actionNote = 'Promotion SMS Mark as Read in Bulk' ;
                log.section = 'Promotions';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                })
                this.readunreadsmstag = 0;
                this.reloadPage(true);
                this.saving = false;
                this.notify.info(this.l('SuccessfullyMark'));
            });

    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Promotion Tracker';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Promotion Tracker';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }

}
