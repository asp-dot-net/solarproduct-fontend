﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetPromotionUserForViewDto, PromotionUserDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewPromotionUserModal',
    templateUrl: './view-promotionUser-modal.component.html'
})
export class ViewPromotionUserModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetPromotionUserForViewDto;


    constructor(
        injector: Injector
    ) {
        super(injector);
        this.item = new GetPromotionUserForViewDto();
        this.item.promotionUser = new PromotionUserDto();
    }

    show(item: GetPromotionUserForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
