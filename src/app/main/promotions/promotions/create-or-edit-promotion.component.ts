
import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { elementAt, finalize } from 'rxjs/operators';
import {
    PromotionsServiceProxy, CreateOrEditPromotionDto, PromotionPromotionTypeLookupTableDto,
    LeadsServiceProxy, LeadStateLookupTableDto,
    LeadSourcesServiceProxy, LeadSourceLookupTableDto,
    TeamDto, TeamsServiceProxy, NameValueOfString, NameValueOfInt32,
    LeadStatusLookupTableDto,
    CreateOrEditEmailTemplateDto,
    EmailTemplateServiceProxy,
    JobsServiceProxy,
    OrganizationUnitDto,
    UserServiceProxy,
    JobStatusTableDto,
    LeadUsersLookupTableDto,
    CommonLookupServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { debug } from 'console';
import { EmailEditorComponent } from 'angular-email-editor';
import { ActivatedRoute, Router } from '@angular/router';
import * as _ from 'lodash';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { appModuleAnimation } from '@shared/animations/routerTransition';
@Component({
    selector: 'app-create-or-edit-promotion',
    templateUrl: './create-or-edit-promotion.component.html',
    styleUrls: ['./create-or-edit-promotion.component.css'],
    animations: [appModuleAnimation()]
})
export class CreateOrEditPromotionComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    saving = false;
    showjobstatus = false;
    total = 0;
    credit = 0;
    organizationID = 0;
    promotion: CreateOrEditPromotionDto = new CreateOrEditPromotionDto();

    promotionTypeName = '';
    dateType = 'LeadCreated';

    allPromotionTypes: PromotionPromotionTypeLookupTableDto[];
    team: TeamDto;
    teams: LeadUsersLookupTableDto[];
    _teamid = '';
    alljobstatus: JobStatusTableDto[];
    // sampleDateRange: moment.Moment[];
    allLeadStatus: LeadStatusLookupTableDto[];
    allStates: LeadStateLookupTableDto[];
    allLeadSources: LeadSourceLookupTableDto[];
    SelectedPromotionTypeId: number;
    SelectedStateId: number;
    SelectedLeadSourceId: number;
    SelectedLeadStatusId: number;
    SelectedJobStatusId: number;
    SelectedTeamId: number;
    name: string;
    organizationunit: any;
    TotalLeads: number;
    FilteredLeadId_CSV: string;

    filteredState: NameValueOfString[];
    filteredLeadSource: NameValueOfString[];
    filteredLeadStatus: NameValueOfString[];
    filteredJobStatus: NameValueOfString[];
    filteredTeams: NameValueOfString[];

    // teamids: NameValueOfInt32[] = new Array<NameValueOfInt32>();
    // leadStatusids: NameValueOfInt32[] = new Array<NameValueOfInt32>();
    // leadSourceids: NameValueOfInt32[] = new Array<NameValueOfInt32>();
    // jobStatusids: NameValueOfInt32[] = new Array<NameValueOfInt32>();
    // stateids: NameValueOfInt32[] = new Array<NameValueOfInt32>();
    teamids = [];
    leadStatusids = [];
    leadSourceids = [];
    jobStatusids = [];
    stateids = [];
    email: CreateOrEditEmailTemplateDto = new CreateOrEditEmailTemplateDto();
    emailData = '';
    saveortest = 0;
    areaNameFilter = '';
    typeNameFilter = '';
    allOrganizationUnits: OrganizationUnitDto[];

    date = new Date();
    firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    startDate = moment(this.firstDay);
    endDate = moment().endOf('day');
    allAreas : string[] = [];
    areaFilter = [];

    constructor(
        injector: Injector,
        private _promotionsServiceProxy: PromotionsServiceProxy,
        private _teamService: TeamsServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _leadSourceServiceProxy: LeadSourcesServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _emailTemplateServiceProxy: EmailTemplateServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _jobsServiceProxy: JobsServiceProxy,
        private _router: Router,
        private _userServiceProxy: UserServiceProxy
    ) {

        super(injector);
        this.SelectedPromotionTypeId = 0;
        this.SelectedStateId = 0;
        this.SelectedLeadSourceId = 0;
        this.SelectedJobStatusId = 0;
        this.SelectedLeadStatusId = 0;
        this.SelectedTeamId = 0;
        this.total = 0;
        this.credit = 0;
        this.TotalLeads = 0;
        this.FilteredLeadId_CSV = '';

        this.getAllPromotionType();
        this.getAllTeams();
        this.getAllStates();
        this.getAllLeadSources();
        this.getOrganization();
        this.getAllAreas();
        

        this._activatedRoute.queryParams.subscribe(params => {
            this.email.id = params['eid'];
        });
    }
    @ViewChild(EmailEditorComponent)
    private emailEditor: EmailEditorComponent;

    ngOnInit(): void {
        this.promotion = new CreateOrEditPromotionDto();
        this.promotion.isFooterdAttached = true;
        this.promotion.isCimat=false;
        // this.organizationid = this._activatedRoute.snapshot.queryParams['organizationid'];
        // this.organizationunit = this.organizationid;
        this._commonLookupService.getAllJobStatusTableDropdown().subscribe(result => {
            this.alljobstatus = result;
        });

        this._commonLookupService.getAllLeadStatusForTableDropdown("").subscribe(result => {
           debugger;
            this.allLeadStatus = result;
        });
        
        this.active = true;
    }

    getOrganization(): void {
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
            this.promotion.organizationID = this.allOrganizationUnits[0].id;
        });
    }

    filterLeadSource(event): void {
        this._leadsServiceProxy.promotionGetAllLeadSource(event.query).subscribe(result => {
            this.filteredLeadSource = result;
        });
    }


    filterState(event): void {
        this._leadsServiceProxy.promotionGetAllState(event.query).subscribe(result => {
            this.filteredState = result;
        });
    }
    filterLeadStatus(event): void {
        this._leadsServiceProxy.promotionGetAllLeadStatus(event.query).subscribe(result => {
            this.filteredLeadStatus = result;
        });
    }

    filterJobStatus(event): void {
        this._leadsServiceProxy.promotionGetAllJobStatus(event.query).subscribe(result => {
            this.filteredJobStatus = result;

        });
    }

    filterTeams(event): void {
        this._teamService.getAllTeams(event.query).subscribe(teams => {
            this.filteredTeams = teams;
        });
    }

    getAllLeadStatus() {

        //Get LeadStatus
        this._leadsServiceProxy.promotionGetAllLeadStatus("").subscribe(result => {
            // this.allLeadStatus = result;
            this.filteredLeadStatus = result;
            //this.countLeads();
        });
    }

    getAllPromotionType() {

        this._promotionsServiceProxy.getAllPromotionTypeForTableDropdown().subscribe(result => {
            this.allPromotionTypes = result;
        });

    }
    getAllTeams() {
        //Get Teams
        this._leadsServiceProxy.getAllTeamsForFilter().subscribe(result => {
            this.teams = result;
        });

    }

    getAllStates() {
        //Get States
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
            let state = new LeadStateLookupTableDto();
            state.id = 0;
            state.displayName = "No State"
            this.allStates.push(state);
        });
    }

    getAllAreas(){
        debugger
        this.allAreas = [];
        this.allAreas.push("Metro");
        this.allAreas.push("Regional");
        this.allAreas.push("NoArea");
    }

    getAllLeadSources() {
        //Get LeadSources
        this._commonLookupService.getAllLeadSourceForTableDropdown().subscribe(result => {
            this.allLeadSources = result;
        });
    }
    countLeads(): void {
        debugger;

        this.showMainSpinner();
        this.TotalLeads = 0;
        
        this._leadsServiceProxy.getTotalLeadsCountForPromotion(
            this.SelectedLeadStatusId == 0 ? undefined : this.SelectedLeadStatusId,
            this.SelectedLeadSourceId == 0 ? undefined : this.SelectedLeadSourceId,
            // this.sampleDateRange != undefined && this.sampleDateRange.length > 0 ? this.sampleDateRange[0] : undefined,
            // this.sampleDateRange != undefined && this.sampleDateRange.length > 1 ? this.sampleDateRange[1] : undefined,
            this.startDate,
            this.endDate,
            this.SelectedStateId == 0 ? undefined : this.SelectedStateId,
            this.SelectedTeamId == 0 ? undefined : this.SelectedTeamId,
            this.leadStatusids,
            this.leadSourceids,
            this.stateids,
            this.teamids,
            this.showjobstatus ? this.jobStatusids : [],
            this.areaNameFilter,
            this.typeNameFilter,
            this.promotion.organizationID,
            this.dateType,
            this.areaFilter,
            undefined,
            undefined,
            undefined,
        ).subscribe(result => {
            this.FilteredLeadId_CSV = '';
            result.forEach(element => {
                this.FilteredLeadId_CSV = this.FilteredLeadId_CSV + element + ',';
            });
           
            this.TotalLeads = result.length;
            this.promotion.leadCount = result.length;
            this.hideMainSpinner();
        }, e => {
            this.hideMainSpinner();
        });
    }

    sendEmail() {
        this.saveortest = 1;
        this.saveDesign();
    }

    saveDesign() {
        this.saving = true;
        this.emailEditor.editor.exportHtml((data) =>
            this.saveHTML(data.html)
        );
    }

    saveHTML(data) {
        let htmlTemplate = this.getEmailTemplate(data);
        this.promotion.description = htmlTemplate;
        this.save();
    }

    getEmailTemplate(emailHTML) {
        let myTemplateStr = emailHTML;

        // interpolate
        let compiled = _.template(myTemplateStr);
        let myTemplateCompiled = compiled();
        return myTemplateCompiled;
    }

    Testing(): void {
        if (this.promotion.promotionTypeId == 1) {
            if (this.promotion.mobileNos != null) {
                this.saveortest = 2;
                this.save();
            } else {
                this.notify.warn('Plase Enter Mobile NO');
            }
        } else {
            if (this.promotion.emails != null) {
                this.saveortest = 2;
                this.saveDesign();
            } else {
                this.notify.warn('Plase Enter Email');
            }
        }
    }

    savesms(event: MouseEvent): void {
        debugger;
        (event.target as HTMLButtonElement).disabled = true;
        this.saveortest = 1;
        this.save();
    }

    save(): void {
        this.saving = true;
        this.promotion.saveOrTest = this.saveortest;
        this.promotion.promotionTypeId = this.SelectedPromotionTypeId;
        this.promotion.selectedLeadIdsForPromotion = this.FilteredLeadId_CSV;
        this.promotion.totalCharCount = this.total;
        this.promotion.totalCredit = this.credit;
        this.promotion.leadStatuses = "";
        this.leadStatusids.forEach(element => {
            this.promotion.leadStatuses  = this.promotion.leadStatuses + this.allLeadStatus.find(e =>e.value == element).name + ',';
        });
        this.promotion.states ="";
        this.stateids.forEach(element => {
            this.promotion.states  = this.promotion.states + this.allStates.find(e =>e.id == element).displayName + ',';
        });
        this.promotion.teams  = "";
        this.teamids.forEach(element => {
            this.promotion.teams  = this.promotion.teams + this.teams.find(e =>e.id == element).displayName + ',';
        });
        this.promotion.leadSources  = "";
        this.leadSourceids.forEach(element => {
            this.promotion.leadSources  = this.promotion.leadSources + this.allLeadSources.find(e =>e.id == element).displayName + ',';
        });
        this.promotion.area  = "";
        this.areaFilter.forEach(element => {
            this.promotion.area = this.promotion.area + element + " ,"
        })

        this.promotion.type  = this.typeNameFilter;
        
        this.promotion.startDateFilter = this.startDate;
        this.promotion.endDateFilter =this.endDate;
        if (this.promotion.saveOrTest == 2) {
            this._promotionsServiceProxy.createOrEdit(this.promotion)
                .pipe(finalize(() => { this.saving = false; }))
                .subscribe(() => {
                    this.notify.info(this.l('SavedSuccessfully'));
                    this.FilteredLeadId_CSV = '';
                    if (this.promotion.saveOrTest == 1) {
                        this.close();
                    }
                    this.modalSave.emit(null);
                });
        }
        else {
            debugger
            if (this.TotalLeads > 0 && this.promotion.saveOrTest == 1) {
                if (this.promotion.promotionTypeId == 2) {
                    if(this.promotion.emailFrom == null || this.promotion.emailFrom == "")
                    {
                        this.notify.warn(this.l('Please Enter From Email address'));
                        return;
                    }
                }
                this._promotionsServiceProxy.createOrEdit(this.promotion)
                    .pipe(finalize(() => { this.saving = false; }))
                    .subscribe(() => {
                        this.saving = false;
                        this.notify.info(this.l('SavedSuccessfully'));
                        this.FilteredLeadId_CSV = '';
                        if (this.promotion.saveOrTest == 1) {
                            this.close();
                        }
                        this._router.navigate(['app/main/promotions/promotions']);
                        this.modalSave.emit(null);
                    });
            } else {
                this.notify.error("No leads selected.");
                this.saving = false;
            }
        }
    }

    RefreshCountLeads(): void {
        this.countLeads();
    }

    countCharcters(): void {
        this.total = this.promotion.description.length;
        this.credit = Math.ceil(this.total / 160);
    }
     exportExcel() {
        this.saving = true;
        this.promotion.saveOrTest = 1;

        this.promotion.organizationID = this.organizationunit;
        this.promotion.promotionTypeId = this.SelectedPromotionTypeId;
        this.promotion.selectedLeadIdsForPromotion = this.FilteredLeadId_CSV;
        this.promotion.totalCharCount = this.total;
        this.promotion.totalCredit = this.credit;
        this.promotion.leadStatuses = "";
        this.leadStatusids.forEach(element => {
            this.promotion.leadStatuses  = this.promotion.leadStatuses + this.allLeadStatus.find(e =>e.value == element).name + ',';
        });
        this.promotion.states ="";
        this.stateids.forEach(element => {
            this.promotion.states  = this.promotion.states + this.allStates.find(e =>e.id == element).displayName + ',';
        });
        this.promotion.teams  = "";
        this.teamids.forEach(element => {
            this.promotion.teams  = this.promotion.teams + this.teams.find(e =>e.id == element).displayName + ',';
        });
        this.promotion.leadSources  = "";
        this.leadSourceids.forEach(element => {
            this.promotion.leadSources  = this.promotion.leadSources + this.allLeadSources.find(e =>e.id == element).displayName + ',';
        });
        this.promotion.area  = "";
        this.areaFilter.forEach(element => {
            this.promotion.area = this.promotion.area + element + " ,"
        })
        this.promotion.jobStatues  = "";
        this.jobStatusids.forEach(element => {
            this.promotion.jobStatues = this.promotion.jobStatues + element + " ,"
        })

        this.promotion.type  = this.typeNameFilter;
        
        this.promotion.startDateFilter = this.startDate;
        this.promotion.endDateFilter =this.endDate;
        // this.promotion.startDateFilter = this.sampleDateRange[0];
        // this.promotion.endDateFilter = this.sampleDateRange[1];

        // this.promotion.leadStatusIdsFilter = this.leadStatusids;
        // this.promotion.leadSourceIdsFilter = this.leadSourceids;
        // this.promotion.stateIdsFilter = this.stateids;
        // this.promotion.teamIdsFilter = this.teamids;
        // this.showjobstatus ? this.promotion.jobStatusIdsFilter = this.jobStatusids : [];
        // this.promotion.areaNameFilter = this.areaNameFilter;
        // this.promotion.typeNameFilter = this.typeNameFilter;

        this.promotion.organizationID = this.organizationunit;
        this.promotion.promotionTypeId = this.SelectedPromotionTypeId;
        //this.promotion.selectedLeadIdsForPromotion = this.FilteredLeadId_CSV;
        if (this.TotalLeads > 0) {
            this._promotionsServiceProxy.createWithExcel(this.promotion)
                .pipe(finalize(() => { this.saving = false; }))
                .subscribe((result) => {
                    this.notify.info(this.l('SavedSuccessfully'));
                    this._fileDownloadService.downloadTempFile(result);
                    this.FilteredLeadId_CSV = '';
                    if (this.promotion.saveOrTest == 1) {
                        this.close();
                    }
                    // this.modalSave.emit(null);
                });
        } else {
            this.notify.error("No leads selected.");
            this.saving = false;
        }
    }
    close(): void {
        this._router.navigate(['app/main/promotions/promotions']);
        this.active = false;
        this.total = 0;
        this.credit = 0;
        this.SelectedPromotionTypeId = 0;
        this.SelectedStateId = 0;
        this.SelectedLeadSourceId = 0;
        this.SelectedJobStatusId = 0;
        this.SelectedLeadStatusId = 0;
        this.SelectedTeamId = 0;
        this.total = 0;
        this.credit = 0;
        this.TotalLeads = 0;
        this.FilteredLeadId_CSV = '';
        this.areaNameFilter = '';
        this.typeNameFilter = '';
        this.jobStatusids = new Array<NameValueOfInt32>();;
        this.TotalLeads = 0;
        this.promotion.leadCount = 0;

    }

    leadStatusChange(): void {
        if (this.leadStatusids.includes(6)) {
            this.showjobstatus = true;
        } else {
            this.showjobstatus = false;
        }
    }
   
}
