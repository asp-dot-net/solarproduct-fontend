﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetPromotionForViewDto, PromotionDto, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'viewPromotionModal',
    templateUrl: './view-promotion-modal.component.html'
})
export class ViewPromotionModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetPromotionForViewDto;


    constructor(
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        injector: Injector
    ) {
        super(injector);
        this.item = new GetPromotionForViewDto();
        this.item.promotion = new PromotionDto();
    }

    show(item: GetPromotionForViewDto): void {
        let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='View Promotion Detail';
            log.section = "Wholesale Promotion Tracker";
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        this.item = item;
        this.active = true;
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
