import { Component, Injector, Input, ViewChild, ViewEncapsulation, HostListener } from '@angular/core';
import { CommonLookupServiceProxy, GetInstallerInvoiceForViewDto, JobInstallerInvoicesServiceProxy, JobsServiceProxy, LeadDto, LeadSourceLookupTableDto, LeadsServiceProxy, LeadStateLookupTableDto, LeadStatusLookupTableDto, LeadUsersLookupTableDto, OrganizationUnitDto, TokenAuthServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy, UserServiceProxy } from '@shared/service-proxies/service-proxies';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { AppConsts } from '@shared/AppConsts';
import { HttpClient } from '@angular/common/http';
import { finalize } from 'rxjs/operators';
import { FileUpload } from 'primeng/fileupload';
import { OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
//import { EditupdateNewinvoiceComponent } from './editupdate-newinvoice/editupdate-newinvoice.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ViewApplicationModelComponent } from '../jobs/jobs/view-application-model/view-application-model.component';
// import { AddActivityModalComponent } from '../myleads/myleads/add-activity-model.component';

import { CreateOrEditJobInvoiceModalComponent } from '../jobs/jobs/edit-jobinvoice-model.component';
import { AddWarrantyattachmentComponent } from './add-warrantyattachment.component';
import { WarrantySmsEmailModelComponent } from './warranty-sms-email-model.component';
import { Title } from '@angular/platform-browser';
// import { ViewMyLeadComponent } from '../myleads/myleads/view-mylead.component';
import { CommentModelComponent } from '../activitylog/comment-modal.component';
import { EmailModelComponent } from '../activitylog/email-modal.component';
import { NotifyModelComponent } from '../activitylog/notify-modal.component';
import { ReminderModalComponent } from '../activitylog/reminder-modal.component';
import { SMSModelComponent } from '../activitylog/sms-modal.component';
import { ToDoModalComponent } from '../activitylog/todo-modal.component';
// import { ReferralModalComponent } from './referral-modal.component';
// import { EditupdateReferralComponent } from './editupdate-referral.component';
//import { EditinstallerinvoiceComponent } from '../invoiceinstaller/edit-newinstallerinvoice/editinstallerinvoice.component';

@Component({
    selector: 'app-warrantytracker',
    templateUrl: './warrantytracker.component.html',
    styleUrls: ['./warrantytracker.component.css'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class WarrantytrackerComponent extends AppComponentBase implements OnInit {
    FiltersData = false;
    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    organizationUnitlength: number = 0;
    filterName = "JobNumber";

    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };
    @ViewChild('ExcelFileUpload', { static: false }) excelFileUpload: FileUpload;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @Input() SelectedLeadId: number = 0;
    // @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;

    @ViewChild('viewApplicationModal', { static: true }) viewApplicationModal: ViewApplicationModelComponent;
    // @ViewChild('addActivityModal') addActivityModal: AddActivityModalComponent;

    @ViewChild('editJobInvoiceModal', { static: true }) editJobInvoiceModal: CreateOrEditJobInvoiceModalComponent;
    @ViewChild('addwarrantyattachmentModal', { static: true }) addwarrantyattachmentModal: AddWarrantyattachmentComponent;
    @ViewChild('addwarrantySmsEmailModal', { static: true }) addwarrantySmsEmailModal: WarrantySmsEmailModelComponent;
    // @ViewChild('editupdatereferral', { static: true }) editupdatereferral: EditupdateReferralComponent;
    //@ViewChild('editinstallerinvoice', { static: true }) editinstallerinvoice: EditinstallerinvoiceComponent;
    @ViewChild('smsModel', { static: true }) smsModel: SMSModelComponent;
    @ViewChild('emailModel', { static: true }) emailModel: EmailModelComponent;
    @ViewChild('notifyModel', { static: true }) notifyModel: NotifyModelComponent;
    @ViewChild('commentModel', { static: true }) commentModel: CommentModelComponent;
    @ViewChild('todoModal', { static: true }) todoModal: ToDoModalComponent;
    @ViewChild('ReminderModal', { static: true }) ReminderModal: ReminderModalComponent;
    
    uploadUrl: string;
    advancedFiltersAreShown = false;
    filterText = '';
    copanyNameFilter = '';
    emailFilter = '';
    phoneFilter = '';
    mobileFilter = '';
    id = 0;
    addressFilter = '';
    requirementsFilter = '';
    postCodeSuburbFilter = '';
    stateNameFilter = '';
    streetNameFilter = '';
    postCodePostalCode2Filter = '';
    leadSourceNameFilter = '';
    leadSourceIdFilter = 0;
    //leadSubSourceNameFilter = '';
    leadStatusName = '';
    typeNameFilter = '';
    areaNameFilter = '';
    leadStatus: any;
    ExpandedView: boolean = true;
    allLeadStatus: LeadStatusLookupTableDto[];
    leadStatusId: number = 0;
    StartDate: moment.Moment;
    EndDate: moment.Moment;
    PageNo: number;
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnit = 0;
    invoiceType = '';
    suburbSuggestions: string[];
    allStates: LeadStateLookupTableDto[];
    // sampleDateRange: moment.Moment[] = [moment().add(-200, 'days').endOf('day'), moment().startOf('day')];
    date = new Date();
    sampleDateRange: moment.Moment[] = [ moment(this.date),  moment(this.date)];
    dateFilterType = 'All';
    installerId = '';
    filteredinstaller: string[];
    filteredReps: LeadUsersLookupTableDto[];
    paymentStatus = 2;
    stateid = '';
    salesRepId = 0;
    total = 0;
    leadName: string;
    sendtotal = 0;
    pendingtotal = 0;
    flag = true;
    documentStatus = 0;
    firstrowcount = 0;
    last = 0;

    public screenWidth: any;
    public screenHeight: any;
    testHeight = 330;
    toggle: boolean = true;
    orgCode = '';
    
    change() {
        this.toggle = !this.toggle;
    }

    constructor(
        injector: Injector,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _tokenAuth: TokenAuthServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _httpClient: HttpClient,
        private _jobServiceProxy: JobsServiceProxy,
        private _userServiceProxy: UserServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _jobInstallerInvoiceServiceProxy: JobInstallerInvoicesServiceProxy,
        private _router: Router,
        private titleService: Title
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Warranty Tracker");
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/Users/ImportLeadFromExcel';
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Warranty Tracker';
            log.section = 'Warranty Tracker';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.orgCode = this.allOrganizationUnits[0].code;
            this.getReferrallist();
            this.onsalesrep();
            // this.getCount(this.organizationUnit);

        });
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
        // this.getCount(this.organizationUnit);
    }
    @HostListener('window:resize', ['$event'])
    onResize(event) {
        this.screenHeight = window.innerHeight;
    }
    testHeightSize() {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82;
        }
        else {
            this.testHeight = this.testHeight - -82;
        }
    }
    onsalesrep() {
        this._leadsServiceProxy.getSalesRepForFilter(this.organizationUnit, undefined).subscribe(rep => {
            this.filteredReps = rep;
        });
    }
    // deleteEdition(edition: GetInstallerInvoiceForViewDto): void {   
    //     debugger;
    //     this.message.confirm(

    //         this.l('EditionDeleteWarningMessage', edition.invoiceNo),
    //         this.l('AreYouSure'),
    //         isConfirmed => {
    //             if (isConfirmed) {
    //                 this.id=edition.invoiceinstallerId;
    //                 this._jobInstallerInvoiceServiceProxy.delete(this.id).subscribe(() => {
    //                     this.getReferrallist();
    //                     this.notify.success(this.l('SuccessfullyDeleted'));
    //                 });
    //             }
    //         }
    //     );
    // }
    expandGrid() {
        this.ExpandedView = true;
    }
    clear() {
        this.filterText = '';
        this.postCodeSuburbFilter = '';
        this.stateNameFilter = '';
        this.leadSourceIdFilter = 0;
        this.postCodePostalCode2Filter = '';
        this.leadSourceNameFilter = '';
        this.sampleDateRange = [moment(this.date),moment(this.date)];
        this.getReferrallist();
    }

    filtersuburbs(event): void {
        this._commonLookupService.getOnlySuburb(event.query).subscribe(output => {
            this.suburbSuggestions = output;
        });
    }
    filterinstaller(event): void {

        this._jobInstallerInvoiceServiceProxy.getInstallerForDropdown(event.query).subscribe(result => {
            this.filteredinstaller = result;

        });
    }
    downloadfile1(filename, filepath): void {
        debugger
        let FileName = AppConsts.docUrl + "/" + filepath + filename;
        window.open(FileName, "_blank");

    }
    changeOrgCode() {
        this.orgCode = this.allOrganizationUnits.find(x => x.id == this.organizationUnit).code;
    }
    getReferrallist(event?: LazyLoadEvent) {

        this.StartDate = this.sampleDateRange[0];
        this.EndDate = this.sampleDateRange[1];
        this.primengTableHelper.showLoadingIndicator();
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._jobServiceProxy.getWarrantyTrackerList(
            this.filterName,
            this.dateFilterType,
            this.StartDate,
            this.EndDate,
            this.organizationUnit,
            this.invoiceType, this.installerId, filterText_, this.paymentStatus, this.documentStatus, this.stateid, this.salesRepId,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            console.log(result);
            this.primengTableHelper.hideLoadingIndicator();
            this.shouldShow = false;

            if (result.items.length > 0) {
                this.total = result.items[0].total;
                this.sendtotal = result.items[0].send;
                this.pendingtotal = result.items[0].pending;
            } else {
                this.total = 0;
                this.sendtotal = 0;
                this.pendingtotal = 0;
            }
            if (this.flag == false && result.totalCount != 0 && this.ExpandedView == false) {
                this.navigateToLeadDetail(this.SelectedLeadId)
            }
            else {
                if (this.ExpandedView == false && result.totalCount != 0) {
                    this.navigateToLeadDetail(result.items[0].leadid)
                }
                else {
                    this.ExpandedView = true;
                }
            }

        },
            err => {
                this.primengTableHelper.hideLoadingIndicator();
            });
        ///  this.getCount(this.organizationUnit);
    }

    downloadaprovefile(filename, filepath): void {
        debugger
        let FileName = AppConsts.docUrl + "/" + filepath + filename;
        window.open(FileName, "_blank");

    }

    filterCountries(event): void {
        this._commonLookupService.getAllLeadStatusForTableDropdown(event.query).subscribe(result => {
            this.allLeadStatus = result;
        });
    }
    reloadPage(flafValue: boolean): void {
        this.ExpandedView = true;
        this.paginator.changePage(this.paginator.getPage());
    }

    createLead(): void {
        this._router.navigate(['/app/main/leads/leads/createOrEdit']);
    }

    navigateToLeadDetail(leadid): void {
        // debugger;
        // this.ExpandedView = !this.ExpandedView;
        // this.ExpandedView = false;
        // this.SelectedLeadId = leadid;
        // this.leadName = "leadTracker"
        // this.viewLeadDetail.showDetail(leadid, null, 14);
    }

    deleteLead(lead: LeadDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    // this._leadsServiceProxy.delete(lead.id)
                    //     .subscribe(() => {
                    //         this.reloadPage();
                    //         this.notify.success(this.l('SuccessfullyDeleted'));
                    //     });
                }
            }
        );
    }

    // getCount(organizationUnit: number) {
    //     this._jobServiceProxy.getWarrantyTrackerCount(organizationUnit).subscribe(result => {
    //         this.total = result.warrantyTotalData;
    //         this.sendtotal = result.warrantyTotalEmailSend;
    //         this.pendingtotal = result.warrantyTotalEmailPending;
    //     });
    // }
    // exportToExcel(): void {
    //     this._leadsServiceProxy.getLeadsToExcel(
    //         this.filterText,
    //         this.copanyNameFilter,
    //         this.emailFilter,
    //         this.phoneFilter,
    //         this.mobileFilter,
    //         this.addressFilter,
    //         this.requirementsFilter,
    //         this.postCodeSuburbFilter,
    //         this.stateNameFilter,
    //         this.streetNameFilter,
    //         this.postCodePostalCode2Filter,
    //         this.leadSourceNameFilter,
    //         //this.leadSubSourceNameFilter,
    //     )
    //         .subscribe(result => {
    //             this._fileDownloadService.downloadTempFile(result);
    //         });
    // }

    uploadExcel(data: { files: File }): void {
        const formData: FormData = new FormData();
        const file = data.files[0];
        formData.append('file', file, file.name);

        // this._httpClient
        //     .post<any>(this.uploadUrl, formData)
        //     .pipe(finalize(() => this.excelFileUpload.clear()))
        //     .subscribe(response => {
        //         if (response.success) {
        //             this.notify.success(this.l('ImportLeadsProcessStart'));
        //         } else if (response.error != null) {
        //             this.notify.error(this.l('ImportLeadUploadFailed'));
        //         }
        //     });
    }

    onUploadExcelError(): void {
        this.notify.error(this.l('ImportUsersUploadFailed'));
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Warranty Tracker';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Warranty Tracker';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }

}
