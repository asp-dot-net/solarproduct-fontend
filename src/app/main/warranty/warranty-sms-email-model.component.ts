import { Component, ElementRef, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLookupDto, EmailTemplateServiceProxy, GetDocumentTypeForView, GetLeadForViewDto, JobsServiceProxy, LeadDto, LeadsServiceProxy, SendEmailAttachmentDto, SmsEmailDto, SmsTemplatesServiceProxy, UploadDocumentInput, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { EmailEditorComponent } from 'angular-email-editor';
import * as _ from 'lodash';

import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { AddWarrantyattachmentComponent } from './add-warrantyattachment.component';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { FileItem, FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { result } from 'lodash';
@Component({
  selector: 'addwarrantySmsEmailModal',
  templateUrl: './warranty-sms-email-model.component.html',
})
export class WarrantySmsEmailModelComponent extends AppComponentBase implements OnInit {
  @ViewChild("myNameElem") myNameElem: ElementRef;
  @ViewChild('addModal', { static: true }) modal: ModalDirective;
  ///@ViewChild('viewLeadDetail', {static: true}) viewLeadDetail: ViewMyLeadComponent;
  @ViewChild('addwarrantyattachmentModal', { static: true }) addwarrantyattachmentModal: AddWarrantyattachmentComponent;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('myInput')
  myInputVariable: ElementRef;
  active = false;
  saving = false;
  id = 0;
  allEmailTemplates: CommonLookupDto[];
  allSMSTemplates: CommonLookupDto[];
  emailData = '';
  smsData = '';
  emailTemplate: number;
  uploadUrl: string;
  uploadedFiles: any[] = [];
  myDate = new Date();
  activityType: number;
  item: GetLeadForViewDto;
  activityLog: SmsEmailDto = new SmsEmailDto();
  activityName = "";
  total = 0;
  credit = 0;
  customeTagsId = 0;
  role: string = '';
  leadCompanyName: any;
  subject: '';
  documentTypeId: number;
  public uploader: FileUploader;
  public maxfileBytesUserFriendlyValue = 5;
  private _uploaderOptions: FileUploaderOptions = {};
  filenName = [];
  public fileupload: FileUploader;
  private _fileuploadoption: FileUploaderOptions = {};
  DocumentTypes: GetDocumentTypeForView[];
  emailfrom = "";
  ccbox=false;
  bccbox=false;
  fromemails: CommonLookupDto[];
  criteriaavaibaleornot = false;
  header ="";
  // editorConfig: AngularEditorConfig = {
  //   editable: true,
  //   spellcheck: true,
  //   height: '15rem',
  //   minHeight: '5rem',
  //   placeholder: 'Enter text here...',
  //   translate: 'no',
  //   defaultParagraphSeparator: 'p',
  //   defaultFontName: 'Mirza',
  //   fonts: [
  //     {class: 'arial', name: 'Arial'},
  //     {class: 'times-new-roman', name: 'Times New Roman'},
  //     {class: 'Mirza', name: 'Mirza'},
  //     {class: 'comic-sans-ms', name: 'Comic Sans MS'}
  //   ],
  //   customClasses: [
  //     {
  //       name: "quote",
  //       class: "quote",
  //     },
  //     {
  //       name: 'redText',
  //       class: 'redText'
  //     },
  //     {
  //       name: "titleText",
  //       class: "titleText",
  //       tag: "h1",
  //     },
  //   ],
  //   uploadUrl: 'v1/image',
  //   uploadWithCredentials: false,
  //   sanitize: false,
  //   toolbarPosition: 'top',
  //   toolbarHiddenButtons: [
  //     ['bold', 'italic'],
  //     ['fontSize']
  //   ]
  // };
  // viewLeadDetail: ViewMyLeadComponent;
  constructor(
    injector: Injector,
    private _leadsServiceProxy: LeadsServiceProxy,
    private _emailTemplateServiceProxy: EmailTemplateServiceProxy,
    private _smsTemplateServiceProxy: SmsTemplatesServiceProxy,
    private _router: Router,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _tokenService: TokenService,
    private _jobsServiceProxy: JobsServiceProxy,
  ) {
    super(injector);
    this.item = new GetLeadForViewDto();
    this.item.lead = new LeadDto();
    //this.item.filelist = new SendEmailAttachmentDto{};  
  }

  @ViewChild(EmailEditorComponent)
  private emailEditor: EmailEditorComponent;

  ///private viewLeadDetail: ViewMyLeadComponent;

  ngOnInit(): void {
    this.activityLog.emailTemplateId = 0;
    this.activityLog.smsTemplateId = 0;
    this.activityLog.customeTagsId = 0;
    this.activityLog.subject = '';
  }

  show(leadId: number, id: number, trackerid?: number): void {
    this.activityLog = new  SmsEmailDto();
    this.item = new GetLeadForViewDto();
    this.activityLog.emailTemplateId = 0;
    this.activityLog.smsTemplateId = 0;
    this.activityLog.customeTagsId = 0;
    this.activityLog.subject = '';
    this.activityLog.body = "";
    this.activityLog.trackerId = trackerid;
    let log = new UserActivityLogDto();
             log.actionId = 79;
             log.actionNote ='Open Warranty Tracker Email';
             log.section = 'Warranty Tracker';
             this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                 .subscribe(() => {
             });
    // this.initializeModal();
    if (id == 1) {
      this.id = id;
      this.activityType = 1;
      this._leadsServiceProxy.checkorgwisephonedynamicavaibleornot(leadId).subscribe(result => {
        this.criteriaavaibaleornot = result;
       });
    }
    else if (id == 2) {
      this.id = id;
      this.activityType = 2;
      this._leadsServiceProxy.getOrgWiseDefultandownemailadd(leadId).subscribe(result => {
        debugger;
        this.fromemails = result;
        this.activityLog.emailFrom =this.fromemails[0].displayName;
      });
    }
    this._leadsServiceProxy.getLeadForView(leadId,0).subscribe(result => {

      this.item = result;
      console.log(this.item);
      // this.leadCompanyName = result.lead.companyName;
      this.header = result.lead.companyName + (result.jobNumber != "" ? " - " + result.jobNumber : "")

      this.selection();
      if(!this.criteriaavaibaleornot && id == 1)
      {
        this.message.error("authentication issue");
      } else {
        this.modal.show();
      }
    });
    this._leadsServiceProxy.getallEmailTemplates(leadId).subscribe(result => {
      this.allEmailTemplates = result;
    });
    this._jobsServiceProxy.getRemainingDocumentType(leadId).subscribe(result => {
      this.DocumentTypes = result;
    });

    this._leadsServiceProxy.getallSMSTemplates(leadId).subscribe(result => {
      this.allSMSTemplates = result;
    });
    this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
      this.role = result;
    });
    this.initializeModal();
  }

  selection(): void {

    this.activityLog.body = '';
    this.activityLog.emailTemplateId = 0;
    this.activityLog.customeTagsId = 0;
    this.activityLog.subject = '';
    this.activityLog.subject = "";
    if (this.activityType == 1) {
      this.activityName = "SMS To ";
    }
    else if (this.activityType == 2) {
      this.activityName = "Email To ";
    }
  }
  downloadaprovefile(filename, filepath): void {
    debugger
    let FileName = AppConsts.docUrl + "/" + filepath + filename;
    window.open(FileName, "_blank");

  }
  deletefiledata(documentId: any): void {
    if (documentId == 1) {
      this.item.signedQuotedDoc = null; this.item.signedQuotedDocPath = null;
    }
    else if (documentId == 2) { this.item.panelBrocherDoc = null, this.item.panelBrocherDocPath = null }

    else if (documentId == 3) { this.item.inverterBrocherDoc = null, this.item.inverterBrocherDocPath = null }
    else if (documentId == 4) { this.item.panelWarrantyDoc = null, this.item.panelWarrantyDocPath = null }
    else if (documentId == 5) { this.item.inverterWarrantyDoc = null, this.item.inverterWarrantyDocPath = null }
  }
  deleteattachfiledata(docId: any): void {
    debugger
    this._jobsServiceProxy.deleteWarrantyattachment(docId)
      .pipe(finalize(() => { this.saving = false; }))
      .subscribe(() => {
        this._leadsServiceProxy.getLeadForView(this.item.leadid,0).subscribe(result => {
          this.item = result;
        });
      });
  }
  getReferrallist(leadId: number): void {
    this._leadsServiceProxy.getLeadForView(leadId,0).subscribe(result => {

      this.item = result;
      console.log(this.item);
      this.leadCompanyName = result.lead.companyName;
      this.selection();
      this.modal.show();
    });
  }
  save(): void {

    this.activityLog.leadId = this.item.lead.id;

    this.saving = true;
    this.activityLog.leadId = this.item.jobid;
    this.activityLog.emailTo = this.item.lead.email
    this.activityLog.signedQuotedDoc = this.item.signedQuotedDoc;
    this.activityLog.signedQuotedDocPath = this.item.signedQuotedDocPath;
    this.activityLog.panelBrocherDoc = this.item.panelBrocherDoc;
    this.activityLog.panelBrocherDocPath = this.item.panelBrocherDocPath;
    this.activityLog.inverterBrocherDoc = this.item.inverterBrocherDoc;
    this.activityLog.inverterBrocherDocPath = this.item.inverterBrocherDocPath
    this.activityLog.inverterWarrantyDoc = this.item.inverterWarrantyDoc;
    this.activityLog.inverterWarrantyDocPath = this.item.inverterWarrantyDocPath
    this.activityLog.panelWarrantyDoc = this.item.panelWarrantyDoc;
    this.activityLog.panelWarrantyDocPath = this.item.panelWarrantyDocPath;

    this._jobsServiceProxy.sendWarrantymail(this.activityLog)
      .pipe(finalize(() => { this.saving = false; }))
      .subscribe(() => {
        let log = new UserActivityLogDto();
        log.actionId = 7;
        log.actionNote ='Email Send';
        log.section = 'Warranty Tracker';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
        this.notify.info(this.l('EmailSendSuccessfully'));
        this.modal.hide();
        this.modalSave.emit(null);
        this.activityLog.body = "";
        this.activityLog.emailTemplateId = 0;
        this.activityLog.smsTemplateId = 0;
        this.activityLog.customeTagsId = 0;
        this.activityLog.subject = '';
      });

  }

  countCharcters(): void {

    if (this.role != 'Admin') {
      this.total = this.activityLog.body.length;
      this.credit = Math.ceil(this.total / 160);
      if (this.total > 320) {
        this.notify.warn(this.l('You Can Not Add more than 320 characters'));
      }
    }
    else {
      this.total = this.activityLog.body.length;
      this.credit = Math.ceil(this.total / 160);
    }


  }

  close(): void {

    this.modal.hide();
  }
  addAttachment(): void {
    // this.modal.hide();
  }
  onTagChange(event): void {

    const id = event.target.value;
    if (id == 1) {
        if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Customer.Name}}";
        } else {
            this.activityLog.body = "{{Customer.Name}}";
        }

    } else if (id == 2) {
        if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Customer.Mobile}}";
        } else {
            this.activityLog.body = "{{Customer.Mobile}}";
        }
    } else if (id == 3) {
        if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Customer.Phone}}";
        } else {
            this.activityLog.body = "{{Customer.Phone}}";
        }
    } else if (id == 4) {
        if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Customer.Email}}";
        } else {
            this.activityLog.body = "{{Customer.Email}}";
        }
    } else if (id == 5) {

        if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Customer.Address}}";
        } else {
            this.activityLog.body = "{{Customer.Address}}";
        }
    } else if (id == 6) {
        if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Sales.Name}}";
        } else {
            this.activityLog.body = "{{Sales.Name}}";
        }
    } else if (id == 7) {
        if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Sales.Mobile}}";
        } else {
            this.activityLog.body = "{{Sales.Mobile}}";
        }
    } else if (id == 8) {
        if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Sales.Email}}";
        } else {
            this.activityLog.body = "{{Sales.Email}}";
        }
    }
    else if (id == 9) {
        if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Quote.ProjectNo}}";
        } else {
            this.activityLog.body = "{{Quote.ProjectNo}}";
        }
    }
    else if (id == 10) {
        if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Quote.SystemCapacity}}";
        } else {
            this.activityLog.body = "{{Quote.SystemCapacity}}";
        }
    }
    else if (id == 11) {
        if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Quote.AllproductItem}}";
        } else {
            this.activityLog.body = "{{Quote.AllproductItem}}";
        }
    }
    else if (id == 12) {
        if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Quote.TotalQuoteprice}}";
        } else {
            this.activityLog.body = "{{Quote.TotalQuoteprice}}";
        }
    }
    else if (id == 13) {
        if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Quote.InstallationDate}}";
        } else {
            this.activityLog.body = "{{Quote.InstallationDate}}";
        }
    }
    else if (id == 14) {
        if (this.activityLog.body != null) {
            this.activityLog.body = this.activityLog.body + " " + "{{Quote.InstallerName}}";
        } else {
            this.activityLog.body = "{{Quote.InstallerName}}";
        }
    }
    else if (id == 15) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Freebies.TransportCompany}}";
        } else {
          this.activityLog.body = "{{Freebies.TransportCompany}}";
        }
      }
      else if (id == 16) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Freebies.TransportLink}}";
        } else {
          this.activityLog.body = "{{Freebies.TransportLink}}";
        }
      }
      else if (id == 17) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Freebies.DispatchedDate}}";
        } else {
          this.activityLog.body = "{{Freebies.DispatchedDate}}";
        }
      }
      else if (id == 18) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Freebies.TrackingNo}}";
        } else {
          this.activityLog.body = "{{Freebies.TrackingNo}}";
        }
      }
      else if (id == 19) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Freebies.PromoType}}";
        } else {
          this.activityLog.body = "{{Freebies.PromoType}}";
        }
      }
      else if (id == 20) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Invoice.UserName}}";
        } else {
          this.activityLog.body = "{{Invoice.UserName}}";
        }
      }
      else if (id == 21) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Invoice.UserMobile}}";
        } else {
          this.activityLog.body = "{{Invoice.UserMobile}}";
        }
      }
      else if (id == 22) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Invoice.UserEmail}}";
        } else {
          this.activityLog.body = "{{Invoice.UserEmail}}";
        }
      }
      else if (id == 23) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Invoice.Owning}}";
        } else {
          this.activityLog.body = "{{Invoice.Owning}}";
        }
      }
      else if (id == 24) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Organization.orgEmail}}";
        } else {
          this.activityLog.body = "{{Organization.orgEmail}}";
        }
      }
      else if (id == 25) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Organization.orgMobile}}";
        } else {
          this.activityLog.body = "{{Organization.orgMobile}}";
        }
      }
      else if (id == 26) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Organization.orgMobile}}";
        } else {
          this.activityLog.body = "{{Organization.orgMobile}}";
        }
      }
      else if (id == 27) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Organization.orglogo}}";
        } else {
          this.activityLog.body = "{{Organization.orglogo}}";
        }
      }
      else if (id == 28) {
        if (this.activityLog.body != null) {
          this.activityLog.body = this.activityLog.body + " " + "{{Review.link}}";
        } else {
          this.activityLog.body = "{{Review.link}}";
        }
      }
      
  }

  savedocument(): void {
    debugger;
    if (this.documentTypeId == undefined) {
      // this.notify.warn(this.l('SelectDocumentType'));
      // return;
      this.documentTypeId = 0;
    }
    if (this.uploader.queue.length == 0) {
      this.notify.warn(this.l('SelectFileToUpload'));
      return;
    }
    this.uploader.uploadAll();
  };
  initializeModal(): void {
    this.active = true;
    //  this.temporaryPictureUrl = '';
    this.initFileUploader();
  }
  // save(): void {
  //     if (this.uploader.queue.length == 0) {
  //         this.notify.warn(this.l('SelectFileToUpload'));
  //         return;
  //     }
  //     this.uploader.uploadAll();
  // }

  // upload completed event
  fileChangeEvent(event: any): void {
    debugger;
    if (event.target.files[0].size > 5242880) { //5MB
      this.message.warn(this.l('ProfilePicture_Warn_SizeLimit', this.maxfileBytesUserFriendlyValue));
      return;
    }
    this.uploader.clearQueue();
    this.uploader.addToQueue([<File>event.target.files[0]]);
    this.savedocument();

  }
  guid(): string {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
  }

  guid1(): string {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
  }

  updateFile(fileToken: string, fileName: string, fileType: string, filePath: string): void {
    debugger;
    const input = new UploadDocumentInput();
    input.fileToken = fileToken;
    input.jobId = this.item.leadid;
    input.documentTypeId = this.documentTypeId;
    input.fileName = fileName;
    input.fileType = fileType;
    input.filePath = filePath;
    this.saving = true;
   
    if (this.documentTypeId == 12 || this.documentTypeId == 13) {
      input.jobId = this.item.jobid;
      this._jobsServiceProxy.saveDocument(input)
        .pipe(finalize(() => { this.saving = false; }))
        .subscribe(() => {
          this.notify.info(this.l('SavedSuccessfully'));
          this.myInputVariable.nativeElement.value = "";
          this._leadsServiceProxy.getLeadForView(this.item.leadid,0).subscribe(result => {
            this.item = result;
          });
        });

    } else {
      this._jobsServiceProxy.saveDocumentforwarrantymail(input)
        .pipe(finalize(() => { this.saving = false; }))
        .subscribe(() => {
          this.notify.info(this.l('SavedSuccessfully'));
          this.myInputVariable.nativeElement.value = "";
          this._leadsServiceProxy.getLeadForView(this.item.leadid,0).subscribe(result => {
            this.item = result;
          });
        });
    }
  }
  initFileUploader(): void {
    this.uploader = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Profile/UploadFile' });
    this._uploaderOptions.autoUpload = false;
    this._uploaderOptions.authToken = 'Bearer ' + this._tokenService.getToken();
    this._uploaderOptions.removeAfterUpload = true;
    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
    };
    this.fileupload = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Profile/UploadFile' });
    this._fileuploadoption.autoUpload = false;
    this._fileuploadoption.authToken = 'Bearer ' + this._tokenService.getToken();
    this._fileuploadoption.removeAfterUpload = true;
    this.fileupload.onAfterAddingFile = (file) => {
      file.withCredentials = false;
    };

    this.uploader.onBuildItemForm = (fileItem: FileItem, form: any) => {
      form.append('FileType', fileItem.file.type);
      form.append('FileName', fileItem.file.name);
      form.append('FileToken', this.guid());
      this.filenName.push(fileItem.file.name);

    };
    this.fileupload.onBuildItemForm = (fileItem: FileItem, form: any) => {
      form.append('FileType', fileItem.file.type);
      form.append('FileName', fileItem.file.name);
      form.append('FileToken', this.guid1());
      this.filenName.push(fileItem.file.name);

    };
    this.uploader.onSuccessItem = (item, response, status) => {
      const resp = <IAjaxResponse>JSON.parse(response);
      if (resp.success) {
        this.updateFile(resp.result.fileToken, resp.result.fileName, resp.result.fileType, resp.result.filePath);
      } else {
        this.message.error(resp.error.message);
      }
    };

    this.uploader.setOptions(this._uploaderOptions);
    this.fileupload.setOptions(this._fileuploadoption);
  }
  saveDesign() {

    this.saving = true;
    if (this.activityLog.emailTemplateId == 0) {

      const emailHTML = this.activityLog.body;
      this.setHTML(emailHTML)
    }
    else {
      this.saving = true;
      this.emailEditor.editor.exportHtml((data) =>
        this.setHTML(data.html)
      );
    }
  }

  setHTML(emailHTML) {
    debugger
    let htmlTemplate = this.getEmailTemplate(emailHTML);
    this.activityLog.body = htmlTemplate;
    this.save();
  }

  getEmailTemplate(emailHTML) {
    //let myTemplateStr = "Hello {{user.name}}, you are {{user.age.years}} years old";
    let myTemplateStr = emailHTML;
    let addressValue = this.item.lead.address + " " + this.item.lead.suburb + ", " + this.item.lead.state + "-" + this.item.lead.postCode;
    let myVariables = {
      Customer: {
        Name: this.item.lead.companyName,
        Address: addressValue,
        Mobile: this.item.lead.mobile,
        Email: this.item.lead.email,
        Phone: this.item.lead.phone,
        SalesRep: this.item.currentAssignUserName
      },
      Sales: {
        Name: this.item.currentAssignUserName,
        Mobile: this.item.currentAssignUserMobile,
        Email: this.item.currentAssignUserEmail
      },
      Quote: {
        ProjectNo: this.item.jobNumber,
        SystemCapacity: this.item.systemCapacity != null ? this.item.systemCapacity + " " + 'KW' : this.item.systemCapacity,
        AllproductItem: this.item.qunityAndModelList,
        TotalQuoteprice: this.item.totalQuotaion,
        InstallationDate: this.item.installationDate,
        InstallerName: this.item.installerName,
      },
      Invoice: {
        UserName: this.item.userName,
        UserMobile: this.item.userPhone,
        UserEmail: this.item.userEmail,
    },
    Organization: {
      orgName: this.item.orgName,
      orgEmail: this.item.orgEmail,
      orgMobile: this.item.orgMobile,
      orglogo: AppConsts.docUrl + "/" + this.item.orgLogo,
    },
    Review: {
     link: this.item.reviewlink,
    },
    }

    // use custom delimiter {{ }}
    _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;

    // interpolate
    let compiled = _.template(myTemplateStr);
    let myTemplateCompiled = compiled(myVariables);
    return myTemplateCompiled;
  }
  opencc():void{
    this.ccbox=!this.ccbox;
  }
  openbcc():void{
    this.bccbox=!this.bccbox;
  }
  editorLoaded() {

    this.activityLog.body = "";
    if ((this.activityLog.emailTemplateId != 0 && this.activityLog.emailTemplateId !== null && this.activityLog.emailTemplateId !== undefined)) {
      this._jobsServiceProxy.getEmailTemplateForEditForEmail(this.activityLog.emailTemplateId).subscribe(result => {
        this.activityLog.subject = result.emailTemplate.subject
        this.emailData = result.emailTemplate.body;
        if (this.emailData != "") {
          this.emailData = this.getEmailTemplate(this.emailData);
          this.emailEditor.editor.loadDesign(JSON.parse(this.emailData));
        }
      });
    }

  }

  smseditorLoaded() {

    this.activityLog.body = "";
    if ((this.activityLog.smsTemplateId != 0 && this.activityLog.smsTemplateId !== null && this.activityLog.smsTemplateId !== undefined)) {
      this._jobsServiceProxy.getSmsTemplateForEditForSms(this.activityLog.smsTemplateId).subscribe(result => {
        this.smsData = result.smsTemplate.text;
        if (this.smsData != "") {
          this.setsmsHTML(this.smsData)
        }
      });
    }

  }


  setsmsHTML(smsHTML) {
    let htmlTemplate = this.getsmsTemplate(smsHTML);
    this.activityLog.body = htmlTemplate;
    this.countCharcters();
  }

  getsmsTemplate(smsHTML) {
    //let myTemplateStr = "Hello {{user.name}}, you are {{user.age.years}} years old";
    let myTemplateStr = smsHTML;
    let addressValue = this.item.lead.address + " " + this.item.lead.suburb + ", " + this.item.lead.state + "-" + this.item.lead.postCode;
    let myVariables = {
      Customer: {
        Name: this.item.lead.companyName,
        Address: addressValue,
        Mobile: this.item.lead.mobile,
        Email: this.item.lead.email,
        Phone: this.item.lead.phone,
        SalesRep: this.item.currentAssignUserName
      },
      Sales: {
        Name: this.item.currentAssignUserName,
        Mobile: this.item.currentAssignUserMobile,
        Email: this.item.currentAssignUserEmail
      },
      Quote: {
        ProjectNo: this.item.jobNumber,
        SystemCapacity: this.item.systemCapacity != null ? this.item.systemCapacity + " " + 'KW' : this.item.systemCapacity,
        AllproductItem: this.item.qunityAndModelList,
        TotalQuoteprice: this.item.totalQuotaion,
        InstallationDate: this.item.installationDate,
        InstallerName: this.item.installerName,
      },
      Invoice: {
        UserName: this.item.userName,
        UserMobile: this.item.userPhone,
        UserEmail: this.item.userEmail,
    },
    Organization: {
      orgName: this.item.orgName,
      orgEmail: this.item.orgEmail,
      orgMobile: this.item.orgMobile,
      orglogo: AppConsts.docUrl + "/" + this.item.orgLogo,
    },
    Review: {
      link: this.item.reviewlink,
     },
    }

    // use custom delimiter {{ }}
    _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;

    // interpolate
    let compiled = _.template(myTemplateStr);
    let myTemplateCompiled = compiled(myVariables);
    return myTemplateCompiled;
  }
}





