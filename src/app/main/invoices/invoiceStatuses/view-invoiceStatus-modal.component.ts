﻿import {AppConsts} from "@shared/AppConsts";
import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetInvoiceStatusForViewDto, InvoiceStatusDto,UserActivityLogServiceProxy,UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
@Component({
    selector: 'viewInvoiceStatusModal',
    templateUrl: './view-invoiceStatus-modal.component.html'
})
export class ViewInvoiceStatusModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetInvoiceStatusForViewDto;


    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
        this.item = new GetInvoiceStatusForViewDto();
        this.item.invoiceStatus = new InvoiceStatusDto();
    }

    show(item: GetInvoiceStatusForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();

        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Invoice Status View : ' +this.item.invoiceStatus.name ;
        log.section = 'Invoice Status';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {            
     });
    }
    
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
