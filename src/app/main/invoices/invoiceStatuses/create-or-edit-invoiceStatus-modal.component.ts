﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { InvoiceStatusesServiceProxy, CreateOrEditInvoiceStatusDto ,UserActivityLogServiceProxy,UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'createOrEditInvoiceStatusModal',
    templateUrl: './create-or-edit-invoiceStatus-modal.component.html'
})
export class CreateOrEditInvoiceStatusModalComponent extends AppComponentBase implements OnInit{
   
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    invoiceStatus: CreateOrEditInvoiceStatusDto = new CreateOrEditInvoiceStatusDto();

    constructor(
        injector: Injector,
        private _invoiceStatusesServiceProxy: InvoiceStatusesServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {

        super(injector);
    }
    
    show(invoiceStatusId?: number): void {
    

        if (!invoiceStatusId) {
            this.invoiceStatus = new CreateOrEditInvoiceStatusDto();
            this.invoiceStatus.id = invoiceStatusId;
            this.active = true;
            this.modal.show();

            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Invoice Status';
            log.section = 'Invoice Status';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._invoiceStatusesServiceProxy.getInvoiceStatusForEdit(invoiceStatusId).subscribe(result => {
                this.invoiceStatus = result.invoiceStatus;
                this.active = true;
                this.modal.show();

                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Invoice Status: ' + this.invoiceStatus.name;
                log.section = 'Invoice Status';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        
        
    }

    onShown(): void {
        
        document.getElementById('InvoiceStatus_Name').focus();
    }

    save(): void {
            this.saving = true;
            this._invoiceStatusesServiceProxy.createOrEdit(this.invoiceStatus)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.invoiceStatus.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Invoice Status Updated : '+ this.invoiceStatus.name;
                    log.section = 'Invoice Status';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Invoice Status Created : '+ this.invoiceStatus.name;
                    log.section = 'Invoice Status';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }













    close(): void {
        this.active = false;
        this.modal.hide();
    }
    
     ngOnInit(): void {
        
     }    
}
