import { Component, Injector, ViewEncapsulation, ViewChild, OnInit, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLookupDto, CommonLookupServiceProxy, GetJobForViewDto, GetQuotationForViewDto, InvoicePaymentInvoicePaymentMethodLookupTableDto, InvoicePaymentsServiceProxy, JobFinanceOptionLookupTableDto, JobPaymentOptionLookupTableDto, JobsServiceProxy, JobStatusTableDto, LeadsServiceProxy, LeadStateLookupTableDto, OrganizationUnitDto, QuotationsServiceProxy, ReportServiceProxy, TokenAuthServiceProxy, UserServiceProxy, TexInvoiceDto, PaymentReceiptDto,InstallerServiceProxy, LeadUsersLookupTableDto, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { AppConsts } from '@shared/AppConsts';
import { HttpClient } from '@angular/common/http';
import { finalize } from 'rxjs/operators';
import { ViewApplicationModelComponent } from '@app/main/jobs/jobs/view-application-model/view-application-model.component';
import { AddActivityModalComponent } from '@app/main/myleads/myleads/add-activity-model.component';
// import { ViewMyLeadComponent } from '@app/main/myleads/myleads/view-mylead.component';
import { CreateOrEditJobInvoiceModalComponent } from '@app/main/jobs/jobs/edit-jobinvoice-model.component';
import { InvoiceIssuedSMSEmailModelComponent } from './invoice-issued-smsemail-model/invoice-issued-smsemail-model.component';
import { FileUpload } from 'primeng/fileupload';
import { ViewPaymentComponent } from '../view-payment.component';
import { DomSanitizer, SafeHtml, Title } from '@angular/platform-browser';
import { CommentModelComponent } from '@app/main/activitylog/comment-modal.component';
import { NotifyModelComponent } from '@app/main/activitylog/notify-modal.component';
import { ReminderModalComponent } from '@app/main/activitylog/reminder-modal.component';
import { ToDoModalComponent } from '@app/main/activitylog/todo-modal.component';
import { SMSModelComponent } from '@app/main/activitylog/sms-modal.component';
import { IssuedActivityLogModelComponent } from './Invoiceactivitylog-model.component';

@Component({
    templateUrl: './invoiceIssued.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class InvoiceIssuedComponent extends AppComponentBase implements OnInit {
    @ViewChild('viewApplicationModal', { static: true }) viewApplicationModal: ViewApplicationModelComponent;
    @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    //@ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
    @ViewChild('editJobInvoiceModal', { static: true }) editJobInvoiceModal: CreateOrEditJobInvoiceModalComponent;
    @ViewChild('invoiceIssuedpaymentSmsEmailModel', { static: true }) invoiceIssuedpaymentSmsEmailModel: InvoiceIssuedSMSEmailModelComponent;
    @ViewChild('ExcelFileUpload', { static: false }) excelFileUpload: FileUpload;
    @ViewChild('viewpaymentdetail', { static: false }) viewpaymentdetail: ViewPaymentComponent;
    @ViewChild('notifyModel', { static: true }) notifyModel: NotifyModelComponent;
    @ViewChild('commentModel', { static: true }) commentModel: CommentModelComponent;
    @ViewChild('todoModal', { static: true }) todoModal: ToDoModalComponent;
    @ViewChild('ReminderModal', { static: true }) ReminderModal: ReminderModalComponent;
    @ViewChild('smsModel', { static: true }) smsModel: SMSModelComponent;
    @ViewChild('issuedActivityLogModel', { static: true }) issuedActivityLogModel: IssuedActivityLogModelComponent;
    
    FiltersData = false;
    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    organizationUnitlength: number = 0;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    
    InstallerList: CommonLookupDto[];
    advancedFiltersAreShown = false;
    filterText = '';
    RefundDateRange: moment.Moment[];
    ActualPayDateRange: moment.Moment[];
    reportPath: string;
    maxRefundDateFilter: moment.Moment;
    minRefundDateFilter: moment.Moment;
    maxPaymentNumberFilter: number;
    maxPaymentNumberFilterEmpty: number;
    minPaymentNumberFilter: number;
    minPaymentNumberFilterEmpty: number;
    isVerifiedFilter = -1;
    receiptNumberFilter = '';
    maxActualPayDateFilter: moment.Moment;
    minActualPayDateFilter: moment.Moment;
    paidCommentFilter = '';
    jobNoteFilter = '';
    userNameFilter = '';
    userName2Filter = '';
    userName3Filter = '';
    jobPaymentOption = 0;
    organizationUnit = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    totalpaymentData = 0;
    totalpaymentVerifyData = 0;
    totalpaymentnotVerifyData = 0;
    totalcount: GetJobForViewDto;
    alljobstatus: JobStatusTableDto[];
    allStates: LeadStateLookupTableDto[];
    suburbSuggestions: string[];
    allInvoicePaymentMethods: InvoicePaymentInvoicePaymentMethodLookupTableDto[];
    allInvoicePaymentStatus: CommonLookupDto[];
    jobFinOptions: JobFinanceOptionLookupTableDto[];
    postCodeSuburbFilter = '';
    stateNameFilter = '';
    jobStatusID: [];
    paymentMethodId = 0;
    invoicePaymentStatusId = 0;
    financeOptionId = 0;
    totalAmountOfInvoice = "0";
    totalAmountReceived = 0;
    balanceOwning = 0;
    invoiceDateNameFilter = 'InstallDate';
    
    date = new Date();
    firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    // startDate = moment(this.firstDay);
    // endDate = moment().endOf('day');
    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);

    postalcodefrom = '';
    postalcodeTo = '';
    areaNameFilter = '';
    installerid = 0;
    ExpandedView: boolean = true;
    SelectedLeadId: number = 0;
    JobQuotations: GetQuotationForViewDto[];
    jobPaymentOptions: JobPaymentOptionLookupTableDto[];
    totalCancelAmount = 0;
    totalCustAmountOfInvoice = 0;
    totalCustBAlOwning = 0;
    totalCustAmountReceived = 0;
    totalVICAmountOfInvoice = 0;
    totalVICAmountReceived = 0;
    totalVICBAlOwning = 0;
    totalRefundamount = 0;
    newtotalAmountReceived = "0";
    newbalanceOwning = "0";
    uploadUrl: string;
    invoicenamefilter= 'All';
    excelorcsvfile = 0;
    firstrowcount = 0;
    last = 0;
    vicrebate = "All";
    solarRebateStatus = 0;
    salesRepId = 0;
    totalSystemCapacity = 0;

    taxInvoiceData: TexInvoiceDto;
    paymentReceiptData: PaymentReceiptDto;

    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 330;

    toggle: boolean = true;

    filteredReps: LeadUsersLookupTableDto[];
    filterName = 'JobNumber';
    orgCode ='';
    change() {
        this.toggle = !this.toggle;
      }

    constructor(
        injector: Injector,
        private _installerServiceProxy: InstallerServiceProxy,
        private _invoicePaymentsServiceProxy: InvoicePaymentsServiceProxy,
        private _notifyService: NotifyService,
        private _commonLookupService: CommonLookupServiceProxy,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService,
        private _userServiceProxy: UserServiceProxy,
        private _router: Router,
        private _jobsServiceProxy: JobsServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _httpClient: HttpClient,
        private _reportServiceProxy: ReportServiceProxy,
        private _quotationsServiceProxy: QuotationsServiceProxy,
        private titleService: Title,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private sanitizer: DomSanitizer
    ) {
        super(injector);
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/Users/ImportInvoicesFromExcel';
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        this.titleService.setTitle(this.appSession.tenancyName + " |  Invoice Issued");
       
        this._commonLookupService.getAllJobStatusTableDropdown().subscribe(result => {
            this.alljobstatus = result;
        });
        this._commonLookupService.getAllPaymentOptionForTableDropdown().subscribe(result => {
            this.jobPaymentOptions = result;
        });
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });

        this._invoicePaymentsServiceProxy.getAllInvoicePaymentMethodForTableDropdown().subscribe(result => {
            this.allInvoicePaymentMethods = result;
        });
        this._invoicePaymentsServiceProxy.getAllInvoiceStatusForTableDropdown().subscribe(result => {
            this.allInvoicePaymentStatus = result;
        });

        this._commonLookupService.getAllFinanceOptionForTableDropdown().subscribe(result => {
            this.jobFinOptions = result;
        });
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Invoice Issued';
            log.section = 'Invoice Issued';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.orgCode = this.allOrganizationUnits[0].code;
            const newLocal = this;
            this._commonLookupService.getSalesRepForFilter(this.organizationUnit, undefined).subscribe(rep => {
                this.filteredReps = rep;
            });
            newLocal.getInvoiceIssued();
            // this.getCount(this.organizationUnit);
        });
        this.getInstaller();
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }
        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }

    filtersuburbs(event): void {
        this._commonLookupService.getOnlySuburb(event.query).subscribe(output => {
            this.suburbSuggestions = output;
        });
    }
    installerSearchResult: any [];
    filterInstaller(event): void {
      this.installerSearchResult = Object.assign([], this.InstallerList).filter(
        item => item.displayName.toLowerCase().indexOf(event.query.toLowerCase()) > -1
      )
    }
    selectInstaller(event): void {
        this.installerid = event.id;
        this.getInvoiceIssued();
    }
    getInstaller(): void {
        this._installerServiceProxy.getAllInstallers(this.organizationUnit).subscribe(result => {
            this.InstallerList = result;
        });
    };
 
    // getCount(organizationUnit: number) {
    //     this._jobsServiceProxy.getAllApplicationTrackerCount(organizationUnit).subscribe(result => {
    //         this.totalpaymentData = result.totalpaymentData;
    //         this.totalpaymentVerifyData = result.totalpaymentVerifyData;
    //         this.totalpaymentnotVerifyData = result.totalpaymentnotVerifyData;
    //         this.totalAmountOfInvoice = result.totalAmountOfInvoice;
    //         this.totalAmountReceived = result.totalAmountReceived;
    //         this.balanceOwning = result.balanceOwning;
    //         this.totalCancelAmount = result.totalCancelAmount;
    //         this.totalCustAmountOfInvoice = result.totalCustAmountOfInvoice;
    //         this.totalCustBAlOwning = result.totalCustBAlOwning;
    //         this.totalCustAmountReceived = result.totalCustAmountReceived;
    //         this.totalVICAmountOfInvoice = result.totalVICAmountOfInvoice;
    //         this.totalVICAmountReceived = result.totalVICAmountReceived;
    //         this.totalVICBAlOwning = result.totalVICBAlOwning;
    //     });

    // }
    changeOrgCode() {
        this.orgCode = this.allOrganizationUnits.find(x => x.id == this.organizationUnit).code;
    }
    getInvoiceIssued(event?: LazyLoadEvent) {
           debugger;
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        
        this.primengTableHelper.showLoadingIndicator();
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._invoicePaymentsServiceProxy.getAllInvoiceIssuedData(
            this.filterName,
            filterText_,
            this.jobNoteFilter,
            this.jobPaymentOption,
            this.organizationUnit,
            undefined,
            this.salesRepId,
            this.postCodeSuburbFilter,
            this.stateNameFilter,
            this.jobStatusID,
            this.paymentMethodId,
            this.invoicePaymentStatusId,
            this.financeOptionId,
            this.invoiceDateNameFilter,
            this.startDate,
            this.endDate,
            this.postalcodefrom,
            this.postalcodeTo,
            this.areaNameFilter,
            this.installerid,
            this.invoicenamefilter,
            undefined,
            this.vicrebate,
            this.solarRebateStatus,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {

            //console.log(result.items);

            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            this.shouldShow = false;
            if (result.totalCount > 0) {

                //this.totalpaymentData = result.items[0].totalpaymentData;
                // this.totalpaymentVerifyData = result.items[0].totalpaymentVerifyData;
                // this.totalpaymentnotVerifyData = result.items[0].totalpaymentnotVerifyData;
                this.totalAmountOfInvoice = (result.items[0].totalAmountOfInvoice.toFixed(2));
                // this.totalRefundamount = result.items[0].totalRefundamount;
                // this.totalAmountReceived = (result.items[0].totalAmountReceived - result.items[0].totalRefundamount);
                this.newtotalAmountReceived = (result.items[0].newTotalAmountReceived).toFixed(2);
                // this.balanceOwning = (result.items[0].totalAmountOfInvoice - result.items[0].totalAmountReceived);
                this.newbalanceOwning = (result.items[0].totalAmountOfInvoice - result.items[0].newTotalAmountReceived).toFixed(2);
                //this.totalCancelAmount = (result.items[0].totalCancelAmount);
                this.totalVICAmountOfInvoice = (result.items[0].totalVICAmountOfInvoice) + (result.items[0].totalVICLoanOfInvoice);
                this.totalCustAmountOfInvoice = (result.items[0].totalAmountOfInvoice) - (this.totalVICAmountOfInvoice);
                this.totalCustAmountReceived = (result.items[0].totalCustAmountReceived);
                this.totalCustBAlOwning = (this.totalCustAmountOfInvoice - result.items[0].totalCustAmountReceived);
                this.totalVICAmountReceived = result.items[0].totalVICAmmointReceived;
                this.totalVICBAlOwning = (this.totalVICAmountOfInvoice - this.totalVICAmountReceived);
                this.totalSystemCapacity = result.items[0].totalSystemCapacity;

            } else {
                this.totalpaymentData = 0;
                this.totalpaymentVerifyData = 0;
                this.totalpaymentnotVerifyData = 0;
                this.totalAmountOfInvoice = "0";
                this.totalRefundamount = 0;
                this.totalAmountReceived = 0;
                this.balanceOwning = 0;
                this.totalCancelAmount = 0;
                this.totalCustAmountOfInvoice = 0;
                this.totalCustAmountReceived = 0;
                this.totalCustBAlOwning = 0;
                this.totalVICAmountOfInvoice = 0;
                this.totalVICAmountReceived = 0;
                this.totalVICBAlOwning = 0;
                this.newtotalAmountReceived = "0";
                this.newbalanceOwning = "0";
                this.totalSystemCapacity = 0;
            }
        },
        err => {
            this.primengTableHelper.hideLoadingIndicator();
        });
        // this.getCount(this.organizationUnit);
    }
    getJobQuotations(jobid): void {
        this._quotationsServiceProxy.getAll(jobid, '', 0, 9999).subscribe(result => {

            this.JobQuotations = result.items;
            if (result.items.length > 0) {
                this.reportPath = this.JobQuotations[0].quotation.quoteFilePath;
            } else {
            }
        })
    }

    htmlStr: any;
    taxInvoice(jobid): void {

        this.spinnerService.show();
        this._reportServiceProxy.taxInvoiceByJobId(jobid).subscribe(result => {
            this.taxInvoiceData = result;

            //console.log(this.taxInvoiceData);

            if(this.taxInvoiceData.viewHtml != '')
            {
                this.htmlStr = this.GetTaxInvoice(this.taxInvoiceData.viewHtml);
                this.htmlStr = this.sanitizer.bypassSecurityTrustHtml(this.htmlStr);

                let html = this.htmlStr.changingThisBreaksApplicationSecurity;
                this._commonLookupService.downloadPdf(this.taxInvoiceData.jobNumber, "TaxInvoice", html).subscribe(result => {
                    let FileName = result;
                    this.spinnerService.hide();
                    window.open(result, "_blank");
                },
                err => {
                    this.spinnerService.hide();
                });
            }
        },
        err => {
            this.spinnerService.hide();
        });
        
    }

    paymentReceipt(jobid): void {
        // this.getJobQuotations(jobid);
        // this._reportServiceProxy.paymentReceipt(jobid, this.organizationUnit, true)
        //     .subscribe(result => {
        //         let FileName = AppConsts.docUrl + "/" + this.reportPath + result + ".pdf";
        //         window.open(FileName, "_blank");
        //     });

        this.spinnerService.show();
        this._reportServiceProxy.paymentReceiptByJobId(jobid).subscribe(result => {
            this.paymentReceiptData = result;

            console.log(this.paymentReceiptData);

            if(this.paymentReceiptData.viewHtml != '')
            {
                // this.htmlStr = this.GetPaymentReceipt(this.taxInvoiceData.viewHtml);
                // this.htmlStr = this.sanitizer.bypassSecurityTrustHtml(this.htmlStr);
                // let html = this.htmlStr.changingThisBreaksApplicationSecurity;

                let html = this.GetPaymentReceipt(this.paymentReceiptData.viewHtml);

                this._commonLookupService.downloadPdf(this.paymentReceiptData.jobNumber, "PaymetReceipt", html).subscribe(result => {
                    let FileName = result;
                    this.spinnerService.hide();
                    window.open(result, "_blank");
                },
                err => {
                    this.spinnerService.hide();
                });
            }
        },
        err => {
            this.spinnerService.hide();
        });

    }

    reloadPage($event): void {
        this.ExpandedView = true;
        this.paginator.changePage(this.paginator.getPage());
    }

    exportToExcel(excelorcsv): void {
        
        this.excelorcsvfile = excelorcsv;
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._invoicePaymentsServiceProxy.getAllInvoiceIssuedDataExcel(
            this.filterName,
            filterText_,
            this.jobNoteFilter,
            this.jobPaymentOption,
            this.organizationUnit,
            undefined,
            this.salesRepId,
            this.postCodeSuburbFilter,
            this.stateNameFilter,
            this.jobStatusID,
            this.paymentMethodId,
            this.invoicePaymentStatusId,
            this.financeOptionId,
            this.invoiceDateNameFilter,
            this.startDate,
            this.endDate,
            this.postalcodefrom,
            this.postalcodeTo,
            this.areaNameFilter,
            this.installerid,
            this.invoicenamefilter,
            this.excelorcsvfile ,
            this.vicrebate,
            this.solarRebateStatus,
            undefined,
            undefined,
            undefined,
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    depositeexportToExcel(): void {
        
        this._invoicePaymentsServiceProxy.getAllInvoiceIssueddepositeDataExcel(
            undefined,
            undefined,
            undefined,
            undefined,
            this.organizationUnit,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    owningexportToExcel(): void {
       
        this._invoicePaymentsServiceProxy.getAllInvoiceIssuedOwningDataExcel(
            undefined,
            undefined,
            undefined,
            undefined,
            this.organizationUnit,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined
        )
        .subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
        });
    }

    clear() {
        this.filterText = '';
        this.jobPaymentOption = 0;
        this.ActualPayDateRange = null;
        this.postCodeSuburbFilter = '';
        this.stateNameFilter = '';
        this.jobStatusID = [];
        this.paymentMethodId = 0;
        this.invoicePaymentStatusId = 0;
        this.financeOptionId = 0;
        this.postalcodefrom = '';
        this.postalcodeTo = '';
        this.areaNameFilter = '';
        this.installerid = 0;
        this.salesRepId = 0;
        this.invoiceDateNameFilter = 'InstallDate';
        let date = new Date();
        let firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
        this.startDate =  moment(this.date);
        this.endDate =  moment(this.date);
        
        this.getInvoiceIssued();
    }

    quickview(jobid): void {
        this.viewApplicationModal.show(jobid);
    }

    navigateToLeadDetail(leadid): void {
        // this.ExpandedView = !this.ExpandedView;
        // this.ExpandedView = false;
        // this.SelectedLeadId = leadid;
        //this.viewLeadDetail.showDetail(leadid,null,9);
    }

    // uploadExcel(data: { files: File }): void {
    //     const formData: FormData = new FormData();
    //     const file = data.files[0];
    //     formData.append('file' + ',' + this.organizationUnit, file, file.name);

    //     this._httpClient
    //         .post<any>(this.uploadUrl, formData)
    //         .pipe(finalize(() => this.excelFileUpload.clear()))
    //         .subscribe(response => {
    //             if (response.success) {
    //                 this.notify.success(this.l('ImportInvoiceProcessStart'));
    //             } else if (response.error != null) {
    //                 this.notify.error(this.l('ImportInvoiceUploadFailed'));
    //             }
    //         });
    // }
    expandGrid() {
        this.ExpandedView = true;
    }
    // onUploadExcelError(): void {
    //     this.notify.error(this.l('ImportInvoiceUploadFailed'));
    // }

    GetTaxInvoice(html) {
        let htmlstring = html;

        let myTemplateStr = htmlstring;
        let TableList = [];

        if (this.taxInvoiceData.qunityAndModelLists != null) {
            this.taxInvoiceData.qunityAndModelLists.forEach(obj => {
                TableList.push("<div class='mb5'>" + obj.name + "</div>");
            })
        }

        var netCost = this.taxInvoiceData.netCost != "" ? parseFloat(this.taxInvoiceData.netCost) : 0;
        var gst = (netCost * 10) / 100;

        let myVariables = {
            TI: {
                JobNumber: this.taxInvoiceData.jobNumber,
                Date: this.taxInvoiceData.date,
                BalanceDue: this.taxInvoiceData.balanceDue,

                Cust: {
                    Name: this.taxInvoiceData.name,
                    Mobile: this.taxInvoiceData.mobile,
                    Email: this.taxInvoiceData.email,
                    AddressLine1: this.taxInvoiceData.addressLine1,
                    AddressLine2: this.taxInvoiceData.addressLine2,
                    MeterPhase: this.taxInvoiceData.meterPhase,
                    MeterUpgrad: this.taxInvoiceData.meterUpgrad,
                    RoofType: this.taxInvoiceData.roofType,
                    PropertyType: this.taxInvoiceData.propertyType,
                    RoofPitch: this.taxInvoiceData.roofPitch,
                    ElecDist: this.taxInvoiceData.elecDist,
                    ElecRetailer: this.taxInvoiceData.elecRetailer,
                    NIMINumber: this.taxInvoiceData.nmiNumber,
                    MeterNo: this.taxInvoiceData.meterNo
                },

                PD: {

                    SystemDetails: TableList.toString().replace(/,/g, ''),
                    //SystemDetails: '',
                    TCost: this.taxInvoiceData.totalCost != "" && this.taxInvoiceData.totalCost != "0" ? this.taxInvoiceData.totalCost : "0.00",
                    TotalCost: this.taxInvoiceData.tCost != "" && this.taxInvoiceData.tCost != "0" ? this.taxInvoiceData.tCost : "0.00",
                    Stc: this.taxInvoiceData.stc != "" && this.taxInvoiceData.stc != "0" ? this.taxInvoiceData.stc : "0.00",
                    SolarRebate: this.taxInvoiceData.solarRebate != "" && this.taxInvoiceData.solarRebate != "0" ? this.taxInvoiceData.solarRebate : "0.00",
                    SolarLoan: this.taxInvoiceData.solarLoan != "" && this.taxInvoiceData.solarLoan != "0" ? this.taxInvoiceData.solarLoan : "0.00",
                    NetCost: this.taxInvoiceData.netCost != "" && this.taxInvoiceData.netCost != "0" ? this.taxInvoiceData.netCost : "0.00",
                    Deposit: this.taxInvoiceData.deposit != "" && this.taxInvoiceData.deposit != "0" ? this.taxInvoiceData.deposit : "0.00",
                    ACharge: this.taxInvoiceData.aCharge != "" && this.taxInvoiceData.aCharge != "0" ? this.taxInvoiceData.aCharge : "0.00",
                    Discount: this.taxInvoiceData.discount != "" && this.taxInvoiceData.discount != "0" ? this.taxInvoiceData.discount : "0.00",
                    Gst: gst
                }
            }
        }

        // use custom delimiter {{ }}
        _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;
    
        // interpolate
        let compiled = _.template(myTemplateStr);
        let myTemplateCompiled = compiled(myVariables);
        return myTemplateCompiled;
    }

    GetPaymentReceipt(html) {
        let htmlstring = html;

        let myTemplateStr = htmlstring;
        let TableList = [];

        if (this.paymentReceiptData.paymentDetails != null) {
            this.paymentReceiptData.paymentDetails.forEach(obj => {
                // TableList.push("<div class='mb5'>" + obj.name + "</div>");
                TableList.push("<tr><td>" + obj.date + "</td><td>$" + obj.amount + "</td><td>$" + obj.ssCharge + "</td><td>" + obj.method + "</td></tr>");
            })
        }

        let myVariables = {
            PR: {
                JobNumber: this.paymentReceiptData.jobNumber,
                Date: this.paymentReceiptData.date,
                
                Cust: {
                    Name: this.paymentReceiptData.name,
                    Mobile: this.paymentReceiptData.mobile,
                    Email: this.paymentReceiptData.email,
                    AddressLine1: this.paymentReceiptData.addressLine1,
                    AddressLine2: this.paymentReceiptData.addressLine2
                },

                ID: {
                    TCost: this.paymentReceiptData.totalCost != "" && this.paymentReceiptData.totalCost != "0" ? this.paymentReceiptData.totalCost : "0.00",
                    LessStcRebate: this.paymentReceiptData.lessStcRebate != "" && this.paymentReceiptData.lessStcRebate != "0" ? this.paymentReceiptData.lessStcRebate : "0.00",
                    SolarVICRebate: this.paymentReceiptData.solarVICRebate != "" && this.paymentReceiptData.solarVICRebate != "0" ? this.paymentReceiptData.solarVICRebate : "0.00",
                    SolarVICLoan: this.paymentReceiptData.solarVICLoan != "" && this.paymentReceiptData.solarVICLoan != "0" ? this.paymentReceiptData.solarVICLoan : "0.00",
                    ACharge: this.paymentReceiptData.aCharge != "" && this.paymentReceiptData.aCharge != "0" ? this.paymentReceiptData.aCharge : "0.00",
                    Discount: this.paymentReceiptData.discount != "" && this.paymentReceiptData.discount != "0" ? this.paymentReceiptData.discount : "0.00",
                    NetCost: this.paymentReceiptData.netCost != "" && this.paymentReceiptData.netCost != "0" ? this.paymentReceiptData.netCost : "0.00",
                    BalanceDue: this.paymentReceiptData.balanceDue != "" && this.paymentReceiptData.balanceDue != "0" ? this.paymentReceiptData.balanceDue : "0.00"
                },

                PD: {
                    PaymentDetails: TableList.toString().replace(/,/g, '')
                    //PaymentDetails: TableList.toString()
                },

                SA: {
                    AddressLine1: this.paymentReceiptData.siteAddressLine1,
                    AddressLine2: this.paymentReceiptData.siteAddressLine2
                }
            },
        }

        // use custom delimiter {{ }}
        _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;
    
        // interpolate
        let compiled = _.template(myTemplateStr);
        let myTemplateCompiled = compiled(myVariables);
        return myTemplateCompiled;
    }

    onOrganizationUnitChange() {
        this._commonLookupService.getSalesRepForFilter(this.organizationUnit, undefined).subscribe(rep => {
            this.filteredReps = rep;
        });
        this.getInvoiceIssued();

    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Invoice Issued';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Invoice Issued';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }
}
