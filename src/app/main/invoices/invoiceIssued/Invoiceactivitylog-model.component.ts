import { Component, EventEmitter, Injector, Input, OnInit, Output, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { CommentModelComponent } from "@app/main/activitylog/comment-modal.component";
import { EmailModelComponent } from "@app/main/activitylog/email-modal.component";
import { NotifyModelComponent } from "@app/main/activitylog/notify-modal.component";
import { ReminderModalComponent } from "@app/main/activitylog/reminder-modal.component";
import { SMSModelComponent } from "@app/main/activitylog/sms-modal.component";
import { ToDoModalComponent } from "@app/main/activitylog/todo-modal.component";
import { ActivityLogHistoryComponent } from "@app/main/myleads/myleads/activity-log-history.component";
import { ActivityLogPromotionDetailComponent } from "@app/main/myleads/myleads/activity-log-promotion-detail.component";
import { AddActivityModalComponent } from "@app/main/myleads/myleads/add-activity-model.component";
import { AppComponentBase } from "@shared/common/app-component-base";
import { ActivityLogServiceProxy, ActivitylogInput, CommonLookupDto, GetActivityLogViewDto, GetLeadForActivityOutput, GetLeadForSMSEmailTemplateDto, GetLeadForViewDto, LeadDto, LeadsServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from "@shared/service-proxies/service-proxies";
import { ModalDirective } from "ngx-bootstrap/modal";
import { NgxSpinnerService } from "ngx-spinner";
import { finalize } from "rxjs/operators";

@Component({
    selector: 'issuedActivityLogModel',
    templateUrl: './Invoiceactivitylog-model.component.html',
  })
  
export class IssuedActivityLogModelComponent extends AppComponentBase implements OnInit {
    @ViewChild('IssuedActivityLogModel', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    
    @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    @ViewChild('activityloghistory', { static: true }) activityloghistory: ActivityLogHistoryComponent;
    @ViewChild('activityLogPromotionDetail', { static: true }) activityLogPromotionDetail: ActivityLogPromotionDetailComponent;

    @ViewChild('smsModel', { static: true }) smsModel: SMSModelComponent;
    @ViewChild('emailModel', { static: true }) emailModel: EmailModelComponent;
    @ViewChild('notifyModel', { static: true }) notifyModel: NotifyModelComponent;
    @ViewChild('commentModel', { static: true }) commentModel: CommentModelComponent;
    @ViewChild('todoModal', { static: true }) todoModal: ToDoModalComponent;
    @ViewChild('ReminderModal', { static: true }) ReminderModal: ReminderModalComponent;

    // @Input() SelectedLeadId: number = 0;
    // @Output() reloadLead = new EventEmitter();
    active = false;
    saving = false;
    item: GetLeadForViewDto;
    activeTabIndex: number = 0;
    activityDatabyDate: any[] = [];
    actionId: number = 0;
    activity: boolean = false;
    leadActivityList: GetActivityLogViewDto[];
    leadActionList: CommonLookupDto[];
    leadId: number = 0;
   
    role: string = '';
    sectionId: number = 0;
    showsectionwise = false;
    currentactivity: boolean = false;
    allActivity: boolean = false;
    serviceId : number =0 ;
    activityType = 6;
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _router: Router,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private spinner: NgxSpinnerService
    ) {
        super(injector);
        this.item = new GetLeadForViewDto();
        this.item.lead = new LeadDto();
    }


    ngOnInit(): void {
        //this.show(this._activatedRoute.snapshot.queryParams['id']);
        this._leadsServiceProxy.getAllLeadAction().subscribe(result => {
            this.leadActionList = result;
        });

        this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
            this.role = result;
            if (this.role == 'Sales Rep') {
                this.activity = true;
            }
        });
    }

    sectionName = '';
    show(leadId?: number,sectionId?: number,section = ''): void {
        debugger;
        this.activityType = 6;
        this.leadActivityList = [];
        this.sectionId = sectionId;
        this.activity = false;
        this.allActivity = false;
        this.sectionName =section;
        let log = new UserActivityLogDto();
             log.actionId = 79;
             log.actionNote ='Open Activity';
             log.section = section;
             this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                 .subscribe(() => {
             });
        // this.serviceId = service;
        if (this.sectionId == 0 || this.sectionId > 12) {
            this.showsectionwise = true;
        } 
        //console.log(this.sectionId);
        if (this.sectionId == 30) {
            this.showsectionwise = false;
        } 
        this.leadId = leadId;
        if(this.sectionId == 23 || this.sectionId == 24 || this.sectionId == 25)
        {
            this.currentactivity =  true;
        }
        if (this.sectionId == 15) {
            this.actionId = 11;
            this._leadsServiceProxy.getLeadActivityLog(leadId, this.actionId, this.sectionId, this.currentactivity, this.activity, 0, this.allActivity, this.serviceId).subscribe(result => {
              debugger;
                this.leadActivityList = result;
            });
        }
        else if (this.sectionId == 28) {
            this.actionId = 6;
            this._leadsServiceProxy.getLeadActivityLog(leadId, this.actionId, this.sectionId, this.currentactivity, this.activity, 0, this.allActivity, this.serviceId).subscribe(result => {
              debugger;
                this.leadActivityList = result;
            });
        }
         else {
            
            this._leadsServiceProxy.getLeadActivityLog(leadId, this.actionId, this.sectionId, this.currentactivity, this.activity, 0, this.allActivity, this.serviceId).subscribe(result => {
                debugger;
                this.leadActivityList = result;
            });
        }
        this.modal.show()
    }

    showDetail(leadId: number): void {
        let that = this;
        debugger
        this._leadsServiceProxy.getLeadActivityLog(leadId, 0, this.sectionId,this.currentactivity, false, 0, this.allActivity, this.serviceId).subscribe(result => {
            this.leadActivityList = result;
        });
    }

    registerToEvents() {
        abp.event.on('app.onCancelModalSaved', () => {
            this.showDetail(this.leadId);
        });
    }

    addActivitySuccess() {
        this.showDetail(this.leadId);
    }

    changeActivity(leadId: number) {
        let that = this;
        this.spinner.show();
        debugger
        this._leadsServiceProxy.getLeadActivityLog(leadId, this.actionId, this.sectionId,this.currentactivity, false, 0, this.allActivity, this.serviceId).subscribe(result => {
            if (result.length > 0) {
                this.notify.success(this.l('ActivityFiltered'));
               
            }
            else {
                this.notify.success(this.l('NoActivityFound'));
            }
            this.leadActivityList = result;
            this.spinner.hide();
        });
    }
    navigateToLeadHistory(leadid): void {

        this.activityloghistory.show(leadid);
    }

    myActivity(leadId: number) {
        let that = this;
        this.spinner.show();
        this._leadsServiceProxy.getLeadActivityLog(leadId, this.actionId, this.sectionId,this.currentactivity, this.activity, 0, this.allActivity, this.serviceId).subscribe(result => {
            if (result.length > 0) {
                this.notify.success(this.l('ActivityFiltered'));
            }
            else {
                this.notify.success(this.l('NoActivityFound'));
            }
            this.leadActivityList = result;
            this.spinner.hide();
        });
    }

    showActivity(){
        if(this.activityType == 6){
            this.smsModel.show(this.leadId,this.sectionId,this.serviceId)
        }
        else if(this.activityType == 7){
            this.emailModel.show(this.leadId,this.sectionId,this.serviceId)
        }
        else if(this.activityType == 8){
            this.ReminderModal.show(this.leadId,this.sectionId,this.serviceId)
        }
        else if(this.activityType == 9){
            this.notifyModel.show(this.leadId,this.sectionId,this.serviceId)
        }
        else if(this.activityType == 24){
            this.commentModel.show(this.leadId,this.sectionId,this.serviceId)
        }
        else if(this.activityType == 25){
            this.todoModal.show(this.leadId,this.sectionId,this.serviceId)
        }

   
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}