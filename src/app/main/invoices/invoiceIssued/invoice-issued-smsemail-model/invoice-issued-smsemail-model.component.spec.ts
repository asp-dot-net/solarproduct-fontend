import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceIssuedSMSEmailModelComponent } from './invoice-issued-smsemail-model.component';

describe('InvoiceIssuedSMSEmailModelComponent', () => {
  let component: InvoiceIssuedSMSEmailModelComponent;
  let fixture: ComponentFixture<InvoiceIssuedSMSEmailModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceIssuedSMSEmailModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceIssuedSMSEmailModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
