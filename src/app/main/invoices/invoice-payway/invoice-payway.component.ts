import { Component, Injector, OnInit, ViewChild, HostListener } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLookupServiceProxy, InvoicePaymentInvoicePaymentMethodLookupTableDto, InvoicePaymentsServiceProxy, JobInstallerInvoicesServiceProxy, JobPaymentOptionLookupTableDto, JobsServiceProxy, LeadsServiceProxy, LeadStateLookupTableDto, OrganizationUnitDto, UserActivityLogDto, UserActivityLogServiceProxy, UserServiceProxy } from '@shared/service-proxies/service-proxies';
import * as moment from 'moment';
import { FileUpload, LazyLoadEvent, Paginator, Table } from 'primeng';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppConsts } from '@shared/AppConsts';
import { finalize } from 'rxjs/operators';
import { AddActivityModalComponent } from '@app/main/myleads/myleads/add-activity-model.component';
import { TokenService } from 'abp-ng2-module';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { Title } from '@angular/platform-browser';
import { InvoicePayWaySMSEmailModelComponent } from './new-invoice-payway-smsemail-model/new-invoice-payway-smsemail-model.component';

@Component({
    templateUrl: './invoice-payway.component.html',
    styleUrls: ['./invoice-payway.component.css']
})
export class InvoicePayWayComponent extends AppComponentBase implements OnInit {

    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    organizationUnitlength: number = 0;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('ExcelFileUpload', { static: false }) excelFileUpload: FileUpload;
    @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    @ViewChild('newinvoicePayWaySmsEmailModel', { static: true }) newinvoicePayWaySmsEmailModel: InvoicePayWaySMSEmailModelComponent;
  
    FiltersData = false;
    jobPaymentOption = 0;
    organizationUnit = 0;
    state = '';
    totalData = 0;
    excelorcsvfiles = 0;
    totalpaidammount = 0;
    paymenttypeid = 0;
    payby = 0;
    dateFilterType = "All";
    allOrganizationUnits: OrganizationUnitDto[];
    filterText = '';
    date = new Date();
    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);
    // sampleDateRange: moment.Moment[] = [moment().add(-7, 'days').endOf('day'), moment().add(0, 'days').startOf('day')];
    uploadUrl: string;
    allStates: LeadStateLookupTableDto[];
    allInvoicePaymentMethods: InvoicePaymentInvoicePaymentMethodLookupTableDto[];
    jobPaymentOptions: JobPaymentOptionLookupTableDto[];
    ExpandedView: boolean = true;
    firstrowcount = 0;
    last = 0;
    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 330;
    toggle: boolean = true;
    filterName = 'JobNumber';
    orgCode ='';
    change() {
        this.toggle = !this.toggle;
    }

    constructor(
        injector: Injector,
        private _jobInstallerInvoiceServiceProxy: JobInstallerInvoicesServiceProxy,
        private _httpClient: HttpClient,
         private _commonLookupService: CommonLookupServiceProxy,
        private _invoicePaymentsServiceProxy: InvoicePaymentsServiceProxy,
        private _tokenService: TokenService,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private titleService: Title
    ) {
        super(injector);
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/Users/ImportInvoicesFromExcel';
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        this.titleService.setTitle(this.appSession.tenancyName + " |  Invoice PayWay");
     
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
        this._invoicePaymentsServiceProxy.getAllInvoicePaymentMethodForTableDropdown().subscribe(result => {
            this.allInvoicePaymentMethods = result;
        });
        this._commonLookupService.getAllPaymentOptionForTableDropdown().subscribe(result => {
            this.jobPaymentOptions = result;
        });
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Invoice PayWay';
            log.section = 'PayWay';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.orgCode = this.allOrganizationUnits[0].code;
            const newLocal = this;
            newLocal.getinvoicepaywaydata();
            // this.getCount(this.organizationUnit);
        });
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }
        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }

    getinvoicepaywaydata(event?: LazyLoadEvent) {
          debugger;
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        // if (this.sampleDateRange != null) {
        //     this.startDate = this.sampleDateRange[0];
        //     this.endDate = this.sampleDateRange[1];
        // } else {
        //     this.startDate = null;
        //     this.endDate = null;
        // }

        this.primengTableHelper.showLoadingIndicator();
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._jobInstallerInvoiceServiceProxy.getPayWayListingData(
            this.filterName,
            filterText_,
            this.organizationUnit,
            undefined,
            this.startDate,
            this.endDate,
            this.state,
            this.paymenttypeid,
            this.payby,
            this.dateFilterType,
            undefined,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
            this.shouldShow = false;
            if (result.totalCount > 0) {

                this.totalData = result.items[0].totalInvoicePayment;
                this.totalpaidammount =  result.items[0].ammountRcv;
            } else {
                this.totalData = 0;
                this.totalpaidammount = 0;
            }
        });
    }
    uploadExcel(data: { files: File }): void {
        var headers_object = new HttpHeaders().set("Authorization", "Bearer " + this._tokenService.getToken());

        const httpOptions = {
            headers: headers_object
        };

        const formData: FormData = new FormData();
        const file = data.files[0];
        formData.append('file' + ',' + this.organizationUnit, file, file.name);

        this._httpClient
            .post<any>(this.uploadUrl, formData, httpOptions)
            .pipe(finalize(() => this.excelFileUpload.clear()))
            .subscribe(response => {
                if (response.success) {
                    this.notify.success(this.l('ImportInvoiceProcessStart'));
                } else if (response.error != null) {
                    this.notify.error(this.l('ImportInvoiceUploadFailed'));
                }
            });
    }
    onUploadExcelError(): void {
        this.notify.error(this.l('ImportInvoiceUploadFailed'));
    }

    clear() {
        this.filterText = '';
        this.payby = 0;
        this.paymenttypeid = 0;
        this.state = '';
        this.dateFilterType = '';
        this.organizationUnit = 0;
        this.startDate = moment(this.date);
        this.endDate = moment(this.date);
        // this.sampleDateRange = [moment().add(-7, 'days').endOf('day'), moment().add(0, 'days').startOf('day')];
        this.getinvoicepaywaydata();
    }

    exportToExcel(excelorcsv): void {
        // if (this.sampleDateRange != null) {
        //     this.startDate = this.sampleDateRange[0];
        //     this.endDate = this.sampleDateRange[1];
        // } else {
        //     this.startDate = null;
        //     this.endDate = null;
        // }
        this.excelorcsvfiles = excelorcsv;
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._jobInstallerInvoiceServiceProxy.getInvoicePayWayimportToExcel(
            this.filterName,
            filterText_,
            this.organizationUnit,
            undefined,
            this.startDate,
            this.endDate,
            this.state,
            this.paymenttypeid,
            this.payby,
            this.dateFilterType,
            this.excelorcsvfiles,
            this.primengTableHelper.getSorting(this.dataTable),
            undefined,
            undefined,
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    changeOrgCode() {
        this.orgCode = this.allOrganizationUnits.find(x => x.id == this.organizationUnit).code;
    }
    
    reloadPage($event): void {
        this.ExpandedView = true;
        this.paginator.changePage(this.paginator.getPage());
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'PayWay';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'PayWay';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }
}