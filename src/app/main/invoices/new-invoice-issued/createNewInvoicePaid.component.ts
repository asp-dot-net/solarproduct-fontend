import { Component, ViewChild, Injector, Output, EventEmitter, OnInit, ElementRef, Directive, Optional } from '@angular/core';
import {
    CreatePaidInvoiceDto,
    CreateOrEditImportDto,
    ImportDataViewDto,
    InstallerInvoiceImportDataViewDto,
    InvoicePaymentInvoicePaymentMethodLookupTableDto,
    InvoicePaymentsServiceProxy,
    JobInstallerInvoicesServiceProxy,
    LeadsServiceProxy,
    UserActivityLogDto,
    UserActivityLogServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute, Router } from '@angular/router';
import { ViewMyLeadComponent } from '@app/main/myleads/myleads/view-mylead.component';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import * as moment from 'moment';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'createnewinvoices',
    templateUrl: './createNewInvoicePaid.component.html',
})
export class CreateInvoicePaidComponent extends AppComponentBase implements OnInit {
    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    sectionId: number = 0;
    saving = false;
    createNewInvoiceDto: CreatePaidInvoiceDto = new CreatePaidInvoiceDto();
    allInvoicePaymentMethods: InvoicePaymentInvoicePaymentMethodLookupTableDto[];
    // iteam: CreateOrEditImportDto;
    // active = false;
    // importdate: moment.Moment;
    // paymentid = 0;
    // description = "";
    // ammount = 0;
    // ssCharge = 0;
    // purchaseNumber = "";
    // receiptNumber = "";
    // invoiceNotesDescription = "";
    // jobId = 0;

    filteredJobNumber: any[];
    jobNumber: string = "";
    
    constructor(
        injector: Injector,
        private _router: Router,
        private _invoicePaymentsServiceProxy: InvoicePaymentsServiceProxy,
        private _jobInstallerInvoiceServiceProxy: JobInstallerInvoicesServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private spinner: NgxSpinnerService
    ) {
        super(injector);
    }

    ngOnInit() {
        this._invoicePaymentsServiceProxy.getAllInvoicePaymentMethodForTableDropdown().subscribe(result => {
            this.allInvoicePaymentMethods = result;
        });
    }

    sectionName = '';
    show(sectionId,section =''): void {
        this.sectionName = section;
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Open For Create new Invoice';
        log.section = 'Invoice Paid';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
        // this.jobNumber = "";
        this.sectionId = sectionId;
        this.createNewInvoiceDto = new CreatePaidInvoiceDto();

        this.modal.show();
        // this.spinner.show();
        // this._jobInstallerInvoiceServiceProxy.getImportDataForEdit(id).subscribe(result => {
        //     debugger;
        //     this.iteam = result;
        //     this.importdate = this.iteam.date;
        //     this.paymentid = this.iteam.invoicePaymentMethodId;
        //     this.description = this.iteam.description;
        //     this.ammount = this.iteam.paidAmmount;
        //     this.ssCharge = this.iteam.ssCharge;
        //     this.purchaseNumber = this.iteam.purchaseNumber;
        //     this.receiptNumber = this.iteam.receiptNumber;
        //     this.invoiceNotesDescription = this.iteam.invoiceNotesDescription;
        //     this.jobId = this.iteam.jobId;

        //     this.active = true;
             
        //     this.spinner.hide();
        // });
    }

    filterjobnumber(event): void {

        this._jobInstallerInvoiceServiceProxy.getAllJobNumberForMyService(event.query).subscribe(result => {
            this.filteredJobNumber = result;

        });
    }

    OnSelectDetails(event): void {
    
        this.createNewInvoiceDto.jobId = event.jobId;
        this.createNewInvoiceDto.jobNumber = event.jobNumber;
        // this.service.name = event.customerName;
        // this.service.mobile = event.mobile;
        // this.service.email = event.email;
        // this.service.address = event.address;
        // this.service.state = event.state;
    }

    save(): void {
        this.saving = true;
        // this.iteam.date = this.importdate;
        // this.iteam.invoicePaymentMethodId = this.paymentid;
        // this.iteam.description = this.description;
        // this.iteam.paidAmmount = this.ammount;
        // this.iteam.ssCharge = this.ssCharge;
        // this.iteam.purchaseNumber = this.purchaseNumber;
        // this.iteam.receiptNumber = this.receiptNumber;
        // this.iteam.invoiceNotesDescription = this.invoiceNotesDescription;
        // this.iteam.jobId = this.jobId;
        this.createNewInvoiceDto.sectionId = this.sectionId;
        this._jobInstallerInvoiceServiceProxy.createManualInvoicePaid(this.createNewInvoiceDto)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                let log = new UserActivityLogDto();
                log.actionId = 58;
                log.actionNote ='Invoice Created';
                log.section = this.sectionName;
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
                this.modal.hide();
                this.notify.info(this.l('SavedSuccessfully'));
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.modal.hide();
    }


}