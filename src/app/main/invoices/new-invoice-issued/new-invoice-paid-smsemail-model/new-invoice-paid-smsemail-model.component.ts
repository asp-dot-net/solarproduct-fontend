import { Component, ElementRef, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivityLogServiceProxy, CommonLookupDto, EmailTemplateServiceProxy, GetLeadForSMSEmailDto, GetLeadForSMSEmailTemplateDto, GetLeadForViewDto, GetQuotationForViewDto, JobsServiceProxy, LeadDto, LeadsServiceProxy, QuotationsServiceProxy, ReportServiceProxy, SmsEmailDto, SmsTemplatesServiceProxy, UploadDocumentInput, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { EmailEditorComponent } from 'angular-email-editor';
import * as _ from 'lodash';
import { FileItem, FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'newinvoicepaidSmsEmailModel',
  templateUrl: './new-invoice-paid-smsemail-model.component.html',
})
export class InvoicePaidSMSEmailModelComponent extends AppComponentBase implements OnInit {
  @ViewChild("myNameElem") myNameElem: ElementRef;
  @ViewChild('addModal', { static: true }) modal: ModalDirective;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('myInput') myInputVariable: ElementRef;
  saving = false;
  id = 0;
  allEmailTemplates: CommonLookupDto[];
  allSMSTemplates: CommonLookupDto[];
  emailData = '';
  smsData = '';
  emailTemplate: number;
  uploadUrl: string;
  uploadedFiles: any[] = [];
  myDate = new Date();
  activityType: number;
  // item: GetLeadForViewDto;
  activityLog: SmsEmailDto = new SmsEmailDto();
  activityName = "";
  total = 0;
  credit = 0;
  customeTagsId = 0;
  role: string = '';
  leadCompanyName: any;
  filename = "";
  JobQuotations: GetQuotationForViewDto[];
  reportPath: string;
  public uploader: FileUploader;
  public maxfileBytesUserFriendlyValue = 5;
  private _uploaderOptions: FileUploaderOptions = {};
  tempids = [];
  filenName = [];
  emailfrom = "";
  ccbox = false;
  bccbox = false;
  fromemails: CommonLookupDto[];
  criteriaavaibaleornot = false;

  lead : GetLeadForSMSEmailDto = new GetLeadForSMSEmailDto();
  item : GetLeadForSMSEmailTemplateDto = new GetLeadForSMSEmailTemplateDto();
  header ="";

  constructor(
    injector: Injector,
    private _leadsServiceProxy: LeadsServiceProxy,
    private _emailTemplateServiceProxy: EmailTemplateServiceProxy,
    private _smsTemplateServiceProxy: SmsTemplatesServiceProxy,
    private _router: Router,
    private _tokenService: TokenService,
    private _jobsServiceProxy: JobsServiceProxy,
    private _reportServiceProxy: ReportServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _quotationsServiceProxy: QuotationsServiceProxy,
    private spinner: NgxSpinnerService,
    private _activityLogServiceProxy :ActivityLogServiceProxy
  ) {
    super(injector);
    this.item = new GetLeadForSMSEmailTemplateDto();
    this.lead = new GetLeadForSMSEmailDto();
  }

  @ViewChild(EmailEditorComponent)
  private emailEditor: EmailEditorComponent;

  ngOnInit(): void {
    this.activityLog.emailTemplateId = 0;
    this.activityLog.smsTemplateId = 0;
    this.activityLog.customeTagsId = 0;
  }

    sectionName = '';
    show(leadId: number, id: number, trackerid?: number, importid?: number, orgid?: number,section = ''): void {
      this.sectionName =section;
    let log = new UserActivityLogDto();
    log.actionId = 79;
    log.actionNote ='Open Email';
    log.section = section;
    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {
    });
    debugger;
    this.activityLog = new SmsEmailDto();
    this.item = new GetLeadForSMSEmailTemplateDto();
    this.lead = new GetLeadForSMSEmailDto();
    this.activityLog.leadId = leadId;
    this.activityLog.emailTemplateId = 0;
    this.activityLog.smsTemplateId = 0;
    this.activityLog.customeTagsId = 0;
    this.activityLog.body = "";
    this.activityLog.trackerId = trackerid;
    this.activityLog.id = importid;
    if (id == 1) {
      this.id = id;
      this.activityType = 1;
      this._leadsServiceProxy.checkorgwisephonedynamicavaibleornot(leadId).subscribe(result => {
        this.criteriaavaibaleornot = result;
      });
    }
    else if (id == 2) {
      this.id = id;
      this.activityType = 2;
      this._leadsServiceProxy.getOrgWiseDefultandownemailadd(leadId).subscribe(result => {
        debugger;
        this.fromemails = result;
        this.activityLog.emailFrom = this.fromemails[0].displayName;
      });
    }
    else if (trackerid == 26) {
      this.id = id;
      this.activityType = 26;
    }
    this.spinner.show();
    // this._leadsServiceProxy.getLeadForView(leadId, 0).subscribe(result => {
    //   this.item = result;
    //   this.leadCompanyName = result.lead.companyName;
    //   //this.paymentrecept(this.item.jobid, orgid)
    //   this.selection();
    //   this.initializeModal();
    //   if (!this.criteriaavaibaleornot && id == 1) {
    //     this.message.error("authentication issue");
    //   } else {
    //     this.modal.show();
    //     this.spinner.hide();
    //   }
    // });

    this._activityLogServiceProxy.getLeadForSMSEmailActivityInvoiceIssued(leadId).subscribe(result => {
      this.lead= result;
      this.leadCompanyName = result.companyName;
      this.header = result.companyName + (result.jobnumber != "" ? " - " + result.jobnumber : "")

      this.selection();
      this.initializeModal();
      this.modal.show();
      this.spinner.hide();
    })

    this._leadsServiceProxy.getallEmailTemplates(leadId).subscribe(result => {
      this.allEmailTemplates = result;
    });

    this._leadsServiceProxy.getallSMSTemplates(leadId).subscribe(result => {
      this.allSMSTemplates = result;
    });

    this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
      this.role = result;
    });
  }

  // paymentrecept(jobid, org): void {
  //   this.getJobQuotations(jobid);
  //   this._reportServiceProxy.paymentReceipt(jobid, org, true)
  //     .subscribe(result => {
  //       debugger;
  //       this.filename = result + ".pdf";
  //     });
  // }


  getJobQuotations(jobid): void {
    this._quotationsServiceProxy.getAll(jobid, '', 0, 9999).subscribe(result => {

      this.JobQuotations = result.items;
      if (result.items.length > 0) {
        this.reportPath = this.JobQuotations[0].quotation.quoteFilePath;
      } else {
      }
    })
  }
  selection(): void {
    this.activityLog.body = '';
    this.activityLog.emailTemplateId = 0;
    this.activityLog.customeTagsId = 0;
    if (this.activityType == 1) {
      this.activityName = "SMS To ";
    }
    else if (this.activityType == 2) {
      this.activityName = "Email To ";
    }
  }

  save(): void {
    //this.activityLog.leadId = leadid;
    // if (this.activityType == 1) {
    //   this.saving = true;
    //   if (this.role != 'Admin') {
    //     if (this.total > 320) {
    //       this.notify.warn(this.l('You Can Not Add more than 320 characters'));
    //       this.saving = false;
    //     } else {
    //       this._jobsServiceProxy.sendSms(this.activityLog)
    //         .pipe(finalize(() => { this.saving = false; }))
    //         .subscribe(() => {
    //           this.modal.hide();
    //           this.notify.info(this.l('SmsSendSuccessfully'));
    //           this.modalSave.emit(null);
    //           this.activityLog.body = "";
    //           this.activityLog.emailTemplateId = 0;
    //           this.activityLog.smsTemplateId = 0;
    //           this.activityLog.customeTagsId = 0
    //         });
    //     }
    //   }
    //   else {
    //     this._jobsServiceProxy.sendSms(this.activityLog)
    //       .pipe(finalize(() => { this.saving = false; }))
    //       .subscribe(() => {
    //         this.modal.hide();
    //         this.notify.info(this.l('SmsSendSuccessfully'));
    //         this.modalSave.emit(null);
    //         this.activityLog.body = "";
    //         this.activityLog.emailTemplateId = 0;
    //         this.activityLog.smsTemplateId = 0;
    //         this.activityLog.customeTagsId = 0
    //       });
    //   }
    // }
    // else {
      this.saving = true;
      this.activityLog.id = this.item.jobId;
      this.activityLog.taxInvoiceFileName = this.filename;
      this.activityLog.taxInvoiceDocPath = this.reportPath;
      this.activityLog.emailTo = this.lead.email;
      this._jobsServiceProxy.sendEmail(this.activityLog)
        .pipe(finalize(() => { this.saving = false; }))
        .subscribe(() => {
          let log = new UserActivityLogDto();
          log.actionId = 7;
          log.actionNote ='Email Send';
          log.section = this.sectionName;
          this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
              .subscribe(() => {
          });
          this.notify.info(this.l('EmailSendSuccessfully'));
          this.modal.hide();
          this.modalSave.emit(null);
          this.activityLog.body = "";
          this.activityLog.emailTemplateId = 0;
          this.activityLog.smsTemplateId = 0;
          this.activityLog.customeTagsId = 0
        });
    //}
  }

  countCharcters(): void {
    if (this.role != 'Admin') {
      this.total = this.activityLog.body.length;
      this.credit = Math.ceil(this.total / 160);
      if (this.total > 320) {
        this.notify.warn(this.l('You Can Not Add more than 320 characters'));
      }
    }
    else {
      this.total = this.activityLog.body.length;
      this.credit = Math.ceil(this.total / 160);
    }
  }

  close(): void {
    this.lead.filelist.forEach(item => {
      this.tempids.push(item.id)
    });
    //this.deletetempfile(this.tempids)
    this.modal.hide();
  }

  onTagChange(event): void {

    const id = event.target.value;
    if (id == 1) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Customer.Name}}";
      } else {
        this.activityLog.body = "{{Customer.Name}}";
      }

    } else if (id == 2) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Customer.Mobile}}";
      } else {
        this.activityLog.body = "{{Customer.Mobile}}";
      }
    } else if (id == 3) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Customer.Phone}}";
      } else {
        this.activityLog.body = "{{Customer.Phone}}";
      }
    } else if (id == 4) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Customer.Email}}";
      } else {
        this.activityLog.body = "{{Customer.Email}}";
      }
    } else if (id == 5) {

      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Customer.Address}}";
      } else {
        this.activityLog.body = "{{Customer.Address}}";
      }
    } else if (id == 6) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Sales.Name}}";
      } else {
        this.activityLog.body = "{{Sales.Name}}";
      }
    } else if (id == 7) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Sales.Mobile}}";
      } else {
        this.activityLog.body = "{{Sales.Mobile}}";
      }
    } else if (id == 8) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Sales.Email}}";
      } else {
        this.activityLog.body = "{{Sales.Email}}";
      }
    }
    else if (id == 9) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Quote.ProjectNo}}";
      } else {
        this.activityLog.body = "{{Quote.ProjectNo}}";
      }
    }
    else if (id == 10) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Quote.SystemCapacity}}";
      } else {
        this.activityLog.body = "{{Quote.SystemCapacity}}";
      }
    }
    else if (id == 11) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Quote.AllproductItem}}";
      } else {
        this.activityLog.body = "{{Quote.AllproductItem}}";
      }
    }
    else if (id == 12) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Quote.TotalQuoteprice}}";
      } else {
        this.activityLog.body = "{{Quote.TotalQuoteprice}}";
      }
    }
    else if (id == 13) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Quote.InstallationDate}}";
      } else {
        this.activityLog.body = "{{Quote.InstallationDate}}";
      }
    }
    else if (id == 14) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Quote.InstallerName}}";
      } else {
        this.activityLog.body = "{{Quote.InstallerName}}";
      }
    }
    else if (id == 15) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Freebies.TransportCompany}}";
      } else {
        this.activityLog.body = "{{Freebies.TransportCompany}}";
      }
    }
    else if (id == 16) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Freebies.TransportLink}}";
      } else {
        this.activityLog.body = "{{Freebies.TransportLink}}";
      }
    }
    else if (id == 17) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Freebies.DispatchedDate}}";
      } else {
        this.activityLog.body = "{{Freebies.DispatchedDate}}";
      }
    }
    else if (id == 18) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Freebies.TrackingNo}}";
      } else {
        this.activityLog.body = "{{Freebies.TrackingNo}}";
      }
    }
    else if (id == 19) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Freebies.PromoType}}";
      } else {
        this.activityLog.body = "{{Freebies.PromoType}}";
      }
    }
    else if (id == 20) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Invoice.UserName}}";
      } else {
        this.activityLog.body = "{{Invoice.UserName}}";
      }
    }
    else if (id == 21) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Invoice.UserMobile}}";
      } else {
        this.activityLog.body = "{{Invoice.UserMobile}}";
      }
    }
    else if (id == 22) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Invoice.UserEmail}}";
      } else {
        this.activityLog.body = "{{Invoice.UserEmail}}";
      }
    }
    else if (id == 23) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Invoice.Owning}}";
      } else {
        this.activityLog.body = "{{Invoice.Owning}}";
      }
    }
    else if (id == 24) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Organization.orgName}}";
      } else {
        this.activityLog.body = "{{Organization.orgName}}";
      }
    }
    else if (id == 25) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Organization.orgEmail}}";
      } else {
        this.activityLog.body = "{{Organization.orgEmail}}";
      }
    }
    else if (id == 26) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Organization.orgMobile}}";
      } else {
        this.activityLog.body = "{{Organization.orgMobile}}";
      }
    }
    else if (id == 27) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Organization.orglogo}}";
      } else {
        this.activityLog.body = "{{Organization.orglogo}}";
      }
    }
    else if (id == 28) {
      if (this.activityLog.body != null) {
        this.activityLog.body = this.activityLog.body + " " + "{{Review.link}}";
      } else {
        this.activityLog.body = "{{Review.link}}";
      }
    }
}

  saveDesign() {
    if (this.activityLog.emailTemplateId == 0) {
      this.saving = true;
      const emailHTML = this.activityLog.body;
      this.setHTML(emailHTML)
    }
    else {
      this.saving = true;
      this.emailEditor.editor.exportHtml((data) =>
        this.setHTML(data.html)
      );
    }
  }

  setHTML(emailHTML) {
    this.showMainSpinner();
    if(!this.item.id || this.item.id == 0){
      this._activityLogServiceProxy.getLeadForSMSEmailTemplate(this.activityLog.leadId,0,0).subscribe(result => {
        this.item = result;
        let htmlTemplate = this.getEmailTemplate(emailHTML,result);
        this.activityLog.body = htmlTemplate;
        this.save();
        this.hideMainSpinner(); 
      }, err => {
          this.hideMainSpinner(); 
      });
    }
    else{
      let htmlTemplate = this.getEmailTemplate(emailHTML,this.item);
      this.activityLog.body = htmlTemplate;
      this.save();
      this.hideMainSpinner(); 
    }
  }
  opencc(): void {
    this.ccbox = !this.ccbox;
  }
  openbcc(): void {
    this.bccbox = !this.bccbox;
  }
  getEmailTemplate(emailHTML, templateData : GetLeadForSMSEmailTemplateDto) {
    //let myTemplateStr = "Hello {{user.name}}, you are {{user.age.years}} years old";
    let myTemplateStr = emailHTML;
    //let addressValue = this.item.lead.address + " " + this.item.lead.suburb + ", " + this.item.lead.state + "-" + this.item.lead.postCode;
    let myVariables = {
      Customer: {
        Name: templateData.companyName,
        Address: templateData.address,
        Mobile: templateData.mobile,
        Email: templateData.email,
        Phone: templateData.phone,
        SalesRep: templateData.currentAssignUserName
      },
      Sales: {
        Name: templateData.currentAssignUserName,
        Mobile: templateData.currentAssignUserMobile,
        Email: templateData.currentAssignUserEmail
      },
      Quote: {
        ProjectNo: templateData.jobNumber,
        SystemCapacity: templateData.systemCapacity,
        AllproductItem: templateData.qunityAndModelList,
        TotalQuoteprice: templateData.totalQuotaion,
        InstallationDate: templateData.installationDate,
        InstallerName: templateData.installerName,
      },
      Freebies: {
        DispatchedDate: templateData.dispatchedDate,
        TrackingNo: templateData.trackingNo,
        TransportCompany: templateData.transportCompanyName,
        TransportLink: templateData.transportLink,
        PromoType: templateData.promoType,
      },
      Invoice: {
        UserName: templateData.userName,
        UserMobile: templateData.userMobile,
        UserEmail: templateData.userEmail,
        Owning: templateData.owing,
      },
      Organization: {
        orgName: templateData.orgName,
        orgEmail: templateData.orgEmail,
        orgMobile: templateData.orgMobile,
        orglogo: AppConsts.docUrl + "/" + templateData.orglogo,
      },
      Review: {
        link: templateData.link,
      },
    }
    // use custom delimiter {{ }}
    _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;

    // interpolate
    let compiled = _.template(myTemplateStr);
    let myTemplateCompiled = compiled(myVariables);
    return myTemplateCompiled;
  }

  editorLoaded() {
    this.activityLog.body = "";
    if ((this.activityLog.emailTemplateId != 0 && this.activityLog.emailTemplateId !== null && this.activityLog.emailTemplateId !== undefined)) {
      this._jobsServiceProxy.getEmailTemplateForEditForEmail(this.activityLog.emailTemplateId).subscribe(result => {
        this.emailData = result.emailTemplate.body;
        if (this.emailData != "") {
          // this.emailData = this.getEmailTemplate(this.emailData);
          // this.emailEditor.editor.loadDesign(JSON.parse(this.emailData));
          this.showMainSpinner();
          if(!this.item.id || this.item.id == 0){
            this._activityLogServiceProxy.getLeadForSMSEmailTemplate(this.activityLog.leadId,0,0).subscribe(result => {
              this.item = result;
              this.emailData = this.getEmailTemplate(this.emailData,result);
              this.emailEditor.editor.loadDesign(JSON.parse(this.emailData));
              this.hideMainSpinner(); 
            }, err => {
                this.hideMainSpinner(); 
            });
          }
          else{
            this.emailData = this.getEmailTemplate(this.emailData, this.item);
            this.emailEditor.editor.loadDesign(JSON.parse(this.emailData));
            this.hideMainSpinner();
          }
        }
      });
    }
  }

  smseditorLoaded() {
    this.activityLog.body = "";
    if ((this.activityLog.smsTemplateId != 0 && this.activityLog.smsTemplateId !== null && this.activityLog.smsTemplateId !== undefined)) {
      this._jobsServiceProxy.getSmsTemplateForEditForSms(this.activityLog.smsTemplateId).subscribe(result => {
        this.smsData = result.smsTemplate.text;
        if (this.smsData != "") {
          this.setsmsHTML(this.smsData)
        }
      });
    }
  }

  setsmsHTML(smsHTML) {
    let htmlTemplate = this.getsmsTemplate(smsHTML);
    this.activityLog.body = htmlTemplate;
    this.countCharcters();
  }

  getsmsTemplate(smsHTML) {
    let myTemplateStr = smsHTML;
    //let addressValue = this.item.lead.address + " " + this.item.lead.suburb + ", " + this.item.lead.state + "-" + this.item.lead.postCode;
    let myVariables = {
      // Customer: {
      //   Name: this.item.lead.companyName,
      //   Address: addressValue,
      //   Mobile: this.item.lead.mobile,
      //   Email: this.item.lead.email,
      //   Phone: this.item.lead.phone,
      //   SalesRep: this.item.currentAssignUserName
      // },
      // Sales: {
      //   Name: this.item.currentAssignUserName,
      //   Mobile: this.item.currentAssignUserMobile,
      //   Email: this.item.currentAssignUserEmail
      // },
      // Quote: {
      //   ProjectNo: this.item.jobNumber,
      //   SystemCapacity: this.item.systemCapacity != null ? this.item.systemCapacity + " " + 'KW' : this.item.systemCapacity,
      //   AllproductItem: this.item.qunityAndModelList,
      //   TotalQuoteprice: this.item.totalQuotaion,
      //   InstallationDate: this.item.installationDate,
      //   InstallerName: this.item.installerName,
      // },
      // Freebies: {
      //   DispatchedDate: this.item.dispatchedDate,
      //   TrackingNo: this.item.trackingNo,
      //   TransportCompany: this.item.transportCompanyName,
      //   TransportLink: this.item.transportLink,
      //   PromoType: this.item.freebiesPromoType,
      // },
      // Invoice: {
      //   UserName: this.item.userName,
      //   UserMobile: this.item.userPhone,
      //   UserEmail: this.item.userEmail,
      //   Owning: this.item.owning,
      // },
      // Organization: {
      //   orgName: this.item.orgName,
      //   orgEmail: this.item.orgEmail,
      //   orgMobile: this.item.orgMobile,
      //   orglogo: AppConsts.docUrl + "/" + this.item.orgLogo,
      // },
      // Review: {
      //   link: this.item.reviewlink,
      // },
    }

    // use custom delimiter {{ }}
    _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;

    // interpolate
    let compiled = _.template(myTemplateStr);
    let myTemplateCompiled = compiled(myVariables);
    return myTemplateCompiled;
  }

  // upload completed event
  fileChangeEvent(event: any): void {
    debugger;
    if (event.target.files[0].size > 5242880) { //5MB
      this.message.warn(this.l('ProfilePicture_Warn_SizeLimit', this.maxfileBytesUserFriendlyValue));
      return;
    }
    this.uploader.clearQueue();
    this.uploader.addToQueue([<File>event.target.files[0]]);
    this.savedocument();
  }
  savedocument(): void {
    this.uploader.uploadAll();
  };

  initializeModal(): void {
    this.initFileUploader();
  }

  initFileUploader(): void {
    this.uploader = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Profile/UploadFile' });
    this._uploaderOptions.autoUpload = false;
    this._uploaderOptions.authToken = 'Bearer ' + this._tokenService.getToken();
    this._uploaderOptions.removeAfterUpload = true;
    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
    };
    this.uploader.onBuildItemForm = (fileItem: FileItem, form: any) => {
      form.append('FileType', fileItem.file.type);
      form.append('FileName', fileItem.file.name);
      form.append('FileToken', this.guid());
      this.filenName.push(fileItem.file.name);

    };
    this.uploader.onSuccessItem = (item, response, status) => {
      const resp = <IAjaxResponse>JSON.parse(response);
      if (resp.success) {
        this.updateFile(resp.result.fileToken, resp.result.fileName, resp.result.fileType, resp.result.filePath);
      } else {
        this.message.error(resp.error.message);
      }
    };

    this.uploader.setOptions(this._uploaderOptions);
  }

  guid(): string {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
  }

  updateFile(fileToken: string, fileName: string, fileType: string, filePath: string): void {
    debugger;
    const input = new UploadDocumentInput();
    input.fileToken = fileToken;
    input.jobId = this.item.id;
    input.documentTypeId = 0;
    input.fileName = fileName;
    input.fileType = fileType;
    input.filePath = filePath;
    this.saving = true;
    this._jobsServiceProxy.saveDocumentforwarrantymail(input)
      .pipe(finalize(() => { this.saving = false; }))
      .subscribe(() => {
        debugger;


        // this._leadsServiceProxy.getLeadForView(this.item.lead.id, 0).subscribe(result => {
        //   this.item = result;
        // });
        this._activityLogServiceProxy.getLeadForSMSEmailActivityInvoiceIssued(this.item.id).subscribe(result => {
          this.lead = result;
        });
        this.notify.info(this.l('SavedSuccessfully'));
        this.myInputVariable.nativeElement.value = "";
      });
  }

  downloadaprovefile(filename, filepath): void {
    debugger
    let FileName = AppConsts.docUrl + "/" + filepath + filename;
    window.open(FileName, "_blank");

  }
  deletefiledata(documentId: any): void {
    if (documentId == 1) {
      this.filename = null; this.reportPath = null;
    }
  }

  deleteattachfiledata(docId: any): void {
    debugger
    this._jobsServiceProxy.deleteWarrantyattachment(docId)
      .pipe(finalize(() => { this.saving = false; }))
      .subscribe(() => {
        // this._leadsServiceProxy.getLeadForView(this.item.leadid, 0).subscribe(result => {
        //   this.item = result;
        // });
        this._activityLogServiceProxy.getLeadForSMSEmailActivityInvoiceIssued(this.item.id).subscribe(result => {
          this.lead = result;
        });
      });
  }
}