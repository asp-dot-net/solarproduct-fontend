import { Component, Injector, OnInit, ViewChild, HostListener, ViewEncapsulation } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLookupServiceProxy, InvoicePaymentInvoicePaymentMethodLookupTableDto, InvoicePaymentsServiceProxy, JobInstallerInvoicesServiceProxy, JobPaymentOptionLookupTableDto, JobsServiceProxy, LeadsServiceProxy, LeadStateLookupTableDto, OrganizationUnitDto, UserActivityLogDto, UserActivityLogServiceProxy, UserServiceProxy } from '@shared/service-proxies/service-proxies';
import * as moment from 'moment';
import { FileUpload } from 'primeng';
import { LazyLoadEvent } from 'primeng/public_api';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppConsts } from '@shared/AppConsts';
import { finalize } from 'rxjs/operators';
import { AddActivityModalComponent } from '@app/main/myleads/myleads/add-activity-model.component';
import { TokenService } from 'abp-ng2-module';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { EditInvoiceComponent } from './edit-new-invoice.component';
import { InvoicePaidSMSEmailModelComponent } from './new-invoice-paid-smsemail-model/new-invoice-paid-smsemail-model.component';
import { Title } from '@angular/platform-browser';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import * as _ from 'lodash';
import { CreateInvoicePaidComponent } from './createNewInvoicePaid.component';
import { CommentModelComponent } from '@app/main/activitylog/comment-modal.component';
import { NotifyModelComponent } from '@app/main/activitylog/notify-modal.component';
import { ReminderModalComponent } from '@app/main/activitylog/reminder-modal.component';
import { ToDoModalComponent } from '@app/main/activitylog/todo-modal.component';

@Component({
    selector: 'app-new-invoice-issued',
    templateUrl: './new-invoice-issued.component.html',
    styleUrls: ['./new-invoice-issued.component.css'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class NewInvoiceIssuedComponent extends AppComponentBase implements OnInit {
    FiltersData = false;
    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    organizationUnitlength: number = 0;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('ExcelFileUpload', { static: false }) excelFileUpload: FileUpload;
    @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    @ViewChild('editnewinvoices', { static: true }) editnewinvoices: EditInvoiceComponent;
    @ViewChild('newinvoicepaidSmsEmailModel', { static: true }) newinvoicepaidSmsEmailModel: InvoicePaidSMSEmailModelComponent;
    @ViewChild('createnewinvoices', { static: true }) createnewinvoices: CreateInvoicePaidComponent;
    @ViewChild('notifyModel', { static: true }) notifyModel: NotifyModelComponent;
    @ViewChild('commentModel', { static: true }) commentModel: CommentModelComponent;
    @ViewChild('todoModal', { static: true }) todoModal: ToDoModalComponent;
    @ViewChild('ReminderModal', { static: true }) ReminderModal: ReminderModalComponent;
    
    jobPaymentOption = 0;
    organizationUnit = 0;
    state = '';
    totalData = 0;
    excelorcsvfiles = 0;
    totalpaidammount = 0;
    totalWOPaidammount = 0;
    paymenttypeid = 0;
    payby = 0;
    dateFilterType = "CreationDate";
    allOrganizationUnits: OrganizationUnitDto[];
    filterText = '';

    date = new Date();
    // firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    // startDate = moment(this.firstDay);
    // endDate = moment().endOf('day');
    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);
    uploadUrl: string;
    allStates: LeadStateLookupTableDto[];
    allInvoicePaymentMethods: InvoicePaymentInvoicePaymentMethodLookupTableDto[];
    jobPaymentOptions: JobPaymentOptionLookupTableDto[];
    ExpandedView: boolean = true;
    firstrowcount = 0;
    last = 0;
    
    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 330;
    toggle: boolean = true;
    filterName = 'JobNumber';
    orgCode ='';
    
    change() {
        this.toggle = !this.toggle;
      }
    
    constructor(
        injector: Injector,
        private _jobInstallerInvoiceServiceProxy: JobInstallerInvoicesServiceProxy,
        private _userServiceProxy: UserServiceProxy,
        private _jobsServiceProxy: JobsServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _httpClient: HttpClient,
         private _commonLookupService: CommonLookupServiceProxy,
        private _invoicePaymentsServiceProxy: InvoicePaymentsServiceProxy,
        private _tokenService: TokenService,
        private _fileDownloadService: FileDownloadService,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private titleService: Title
    ) {
        super(injector);
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/Users/ImportInvoicesFromExcel';
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        this.titleService.setTitle(this.appSession.tenancyName + " |  Invoice Paid");
     
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
        this._invoicePaymentsServiceProxy.getAllInvoicePaymentMethodForTableDropdown().subscribe(result => {
            this.allInvoicePaymentMethods = result;
        });
        this._commonLookupService.getAllPaymentOptionForTableDropdown().subscribe(result => {
            this.jobPaymentOptions = result;
        });
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Invoice Paid';
            log.section = 'Invoice Paid';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.orgCode = this.allOrganizationUnits[0].code;
            const newLocal = this;
            newLocal.getinvoiceimportedata();
            // this.getCount(this.organizationUnit);
        });
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }
        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }

    getinvoiceimportedata(event?: LazyLoadEvent) {

        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
      
        this.showMainSpinner();
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._jobInstallerInvoiceServiceProxy.getAllInvoiceImportData(
            this.filterName,
            filterText_,
            this.organizationUnit,
            undefined,
            this.startDate,
            this.endDate,
            this.state,
            this.paymenttypeid,
            this.payby,
            this.dateFilterType,
            undefined,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.hideMainSpinner();
            //this.primengTableHelper.hideLoadingIndicator();
            this.shouldShow = false;
            if (result.totalCount > 0) {

                this.totalData = result.items[0].totalInvoicePayment;
                this.totalpaidammount = result.items[0].ammountRcv;
                this.totalWOPaidammount = result.items[0].ammountRcvWO;
            } else {
                this.totalData = 0;
                this.totalpaidammount = 0;
                this.totalWOPaidammount = 0;
            }
        }, err => {
            //this.primengTableHelper.hideLoadingIndicator();
            this.hideMainSpinner();
        });
    }
    
    uploadExcel(data: { files: File }): void {
        var headers_object = new HttpHeaders().set("Authorization", "Bearer " + this._tokenService.getToken());

        const httpOptions = {
            headers: headers_object
        };

        const formData: FormData = new FormData();
        const file = data.files[0];
        formData.append('file' + ',' + this.organizationUnit, file, file.name);
        this._httpClient
            .post<any>(this.uploadUrl, formData, httpOptions)
            .pipe(finalize(() => this.excelFileUpload.clear()))
            .subscribe(response => {
                if (response.success) {
                    this.notify.success("ImportInvoiceProcessStart");
                } else if (response.error != null) {
                    this.notify.error("ImportInvoiceUploadFailed");
                }
            });
    }
    onUploadExcelError(): void {
        this.notify.error("ImportInvoiceUploadFailed");
    }

    clear() {
        this.filterText = '';
        this.payby = 0;
        this.paymenttypeid = 0;
        this.state = '';
        this.dateFilterType = 'dateFilterType';
        this.organizationUnit = 0;

        let date = new Date();
        let firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
        this.startDate = moment(this.date);
        this.endDate = moment(this.date);
        
        this.getinvoiceimportedata();
    }

    exportToExcel(excelorcsv): void {
        this.excelorcsvfiles = excelorcsv;
        var filterText_ = this.filterText;
        if(this.filterName == 'JobNumber' &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._jobInstallerInvoiceServiceProxy.getinvoiceimportToExcel(
            this.filterName,
            filterText_,
            this.organizationUnit,
            undefined,
            this.startDate,
            this.endDate,
            this.state,
            this.paymenttypeid,
            this.payby,
            this.dateFilterType,
            this.excelorcsvfiles,
            this.primengTableHelper.getSorting(this.dataTable),
            undefined,
            undefined,
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }
    changeOrgCode() {
        this.orgCode = this.allOrganizationUnits.find(x => x.id == this.organizationUnit).code;
    }
    deleteLeadExpense(id): void {
        this.message.confirm(
            this.l('AreYouSureWanttoDelete'),
            this.l('Delete'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._jobInstallerInvoiceServiceProxy.deleteImportData(id,10)
                        .subscribe(() => {
                            let log = new UserActivityLogDto();
                            log.actionId = 37;
                            log.actionNote ='Invoice Paid Deleted';
                            log.section = 'Invoice Paid';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            }); 
                            this.reloadPage(event);
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    reloadPage($event): void {
        this.ExpandedView = true;
        this.paginator.changePage(this.paginator.getPage());
    }

    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Invoice Paid';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Invoice Paid';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }

}