﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit, ElementRef, Directive, Optional } from '@angular/core';
import {
    JobsServiceProxy, CreateOrEditJobDto, CreateOrEditJobVariationDto, JobVariationVariationLookupTableDto, CommonLookupServiceProxy, GetInvoicePaymentForViewDto, 
    ImportDataViewDto,
    InstallerInvoiceImportDataViewDto,
    InvoicePaymentsServiceProxy,
    LeadsServiceProxy,
    UserActivityLogDto,
    UserActivityLogServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute, Router } from '@angular/router';
import { ViewMyLeadComponent } from '@app/main/myleads/myleads/view-mylead.component';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { result } from 'lodash';
import { finalize } from 'rxjs/operators';


@Component({
    selector: 'viewpaymentdetail',
    templateUrl: './view-payment.component.html',
})

export class ViewPaymentComponent extends AppComponentBase implements OnInit {
    applicationview: ImportDataViewDto[];
    @ViewChild('viewModel', { static: true }) modal: ModalDirective;
    active = false;
    count = 0;
    billToPay: number = 0;
    totalInvoiceAmountPaid: any = 0;
    jobCurrentStatus: string;


    job: CreateOrEditJobDto = new CreateOrEditJobDto();
    JobVariations: CreateOrEditJobVariationDto[];
    variations: JobVariationVariationLookupTableDto[];
    JobInvoices: GetInvoicePaymentForViewDto[];

    constructor(
        injector: Injector,
        private _jobServiceProxy: JobsServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _invoicePaymentsServiceProxy: InvoicePaymentsServiceProxy,
        private spinner: NgxSpinnerService,
        private _commonLookupService: CommonLookupServiceProxy,
        private _invoicePaymentServiceProxy: InvoicePaymentsServiceProxy

    
    ) {
        super(injector);
    }
    ngOnInit(): void {
        this.job.totalCost = 0;
        this._commonLookupService.getAllVariationForTableDropdown().subscribe(result => {
            this.variations = result;
        });
    }

    getJobInvoices(jobid): void {
        this.totalInvoiceAmountPaid = 0;
        // this._invoicePaymentServiceProxy.getInvoicesByJobId(jobid).subscribe(result => {
        //     this.JobInvoices = result;
        //     this.JobInvoices.map((item) => {
        //         this.totalInvoiceAmountPaid = this.totalInvoiceAmountPaid + item.invoicePayment.invoicePayTotal;
        //     });
        //     this._jobServiceProxy.getJobStatus(this.job.id).subscribe(result => {
        //         this.jobCurrentStatus = result;
        //     });
        // });
        this._invoicePaymentsServiceProxy.customerPaidForActualAmount(jobid).subscribe(result => {
            this.totalInvoiceAmountPaid = result;
        });
    } 

    calculateRates(): void {
        let that = this;
        let BasicTotal = this.job.basicCost;
        let SolarVLCRebate = this.job.solarVICRebate;
        let SolarVICLoanDiscont = this.job.solarVICLoanDiscont;       

        if (this.JobVariations) {
            this.JobVariations.forEach(function (item) {
                let varobj = that.variations.filter(x => x.id == item.variationId);
                if (item.cost > 0) {
                    if (varobj[0].actionName == "Minus") {
                        BasicTotal = parseFloat(BasicTotal.toString()) - parseFloat(item.cost.toString());
                    }
                    else {
                        BasicTotal = parseFloat(BasicTotal.toString()) + parseFloat(item.cost.toString());
                    }
                } else {
                    BasicTotal = parseFloat(BasicTotal.toString());
                }
            })
            if (this.job.solarVICRebate != null && this.job.solarVICLoanDiscont != null) {
                this.job.totalCost = parseFloat(BasicTotal.toString());
                this.job.solarVICRebate = parseFloat(SolarVLCRebate.toString());
                this.job.solarVICLoanDiscont = parseFloat(SolarVICLoanDiscont.toString());
                let totalCostafterdic = (this.job.totalCost) - (this.job.solarVICRebate + this.job.solarVICLoanDiscont)
                if (this.job.depositRequired < 0) {
                    this.job.depositRequired = Math.round(totalCostafterdic * 10 / 100);
                }
                this.billToPay = (totalCostafterdic) - (this.job.depositRequired);
            } else {
                this.job.totalCost = parseFloat(BasicTotal.toString());
                if (this.job.depositRequired < 0) {
                    this.job.depositRequired = Math.round(this.job.totalCost * 10 / 100);
                }
                this.billToPay = (this.job.totalCost) - (this.job.depositRequired);
            }
        }
    }

    SectionName = '';

    show(jobid: number,leadId: number,Section = ''): void {
        debugger;
        this.spinner.show();
        this.SectionName = Section;
        this._invoicePaymentsServiceProxy.getAllinvoiceiisuedpaymentView(jobid).subscribe(result => {
            this.applicationview = result;
            this.count =  result.length
            this.active = true;
            this.getJobInvoices(jobid);
            this.modal.show();
            this.spinner.hide();

        });
        this._jobServiceProxy.getJobByLeadId(leadId).subscribe(result => {
            this.job = result.job;
        })
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote = 'View Payment Detail';
        log.section = Section;
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }


    close(): void {
        this.active = false;
        this.modal.hide();
    }

}
