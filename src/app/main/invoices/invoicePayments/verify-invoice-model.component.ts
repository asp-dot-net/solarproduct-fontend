import { Component, ElementRef, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import {  InvoicePaymentsServiceProxy, GetInvoicePaymentForEditOutput, CreateOrEditInvoicePaymentDto } from '@shared/service-proxies/service-proxies';
import * as _ from 'lodash';
import * as moment from 'moment';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
    selector: 'verifyInvoiceModal',
    templateUrl: './verify-invoice-model.component.html'
})
export class VerifyInvoiceModalComponent extends AppComponentBase {

    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    saving = false;
    paymentDate: moment.Moment;
    item: GetInvoicePaymentForEditOutput;
    comment = "";
    leadCompanyName	:any;
    jobNumber:any;	
    constructor(
        injector: Injector,
        private _invoicePaymentServiceProxy : InvoicePaymentsServiceProxy,
        private _router: Router
    ) {
        super(injector);
        this.item = new GetInvoicePaymentForEditOutput();
        this.item.invoicePayment = new CreateOrEditInvoicePaymentDto();
    }

    show(leadId: number): void {
        this._invoicePaymentServiceProxy.getInvoicePaymentForEdit(leadId).subscribe(result => {      
            this.item = result;
            this.comment = this.item.invoicePayment.paidComment;
            this.paymentDate = this.item.invoicePayment.actualPayDate;
            this.leadCompanyName = result.leadCompanyName;
            this.jobNumber = result.jobNumber;
            this.modal.show();
       });
    }

    save(): void {
        this.saving = true;
        this.item.invoicePayment.actualPayDate = this.paymentDate;
        this.item.invoicePayment.paidComment = this.comment;
        this._invoicePaymentServiceProxy.verifyInvoicePayment(this.item.invoicePayment)
        .pipe(finalize(() => { this.saving = false;}))
        .subscribe(() => {
            this.modal.hide();
            this.notify.info(this.l('SavedSuccessfully'));
            this.comment = "";
            this.modalSave.emit(null);
        });
    }

    close(): void {
        this.modal.hide();
    }

}