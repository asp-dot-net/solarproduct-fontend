﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { InvoicePaymentsServiceProxy, CreateOrEditInvoicePaymentDto
					,InvoicePaymentUserLookupTableDto
					,InvoicePaymentInvoicePaymentMethodLookupTableDto
					} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import {Observable} from "@node_modules/rxjs";


@Component({
    templateUrl: './create-or-edit-invoicePayment.component.html',
    animations: [appModuleAnimation()]
})
export class CreateOrEditInvoicePaymentComponent extends AppComponentBase implements OnInit {
    active = false;
    saving = false;
    
    invoicePayment: CreateOrEditInvoicePaymentDto = new CreateOrEditInvoicePaymentDto();

    jobNote = '';
    userName = '';
    userName2 = '';
    userName3 = '';
    invoicePaymentMethodPaymentMethod = '';

	//allJobs: InvoicePaymentJobLookupTableDto[];
    allUsers: InvoicePaymentUserLookupTableDto[];
    allInvoicePaymentMethods: InvoicePaymentInvoicePaymentMethodLookupTableDto[];
				

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,        
        private _invoicePaymentsServiceProxy: InvoicePaymentsServiceProxy,
        private _router: Router
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.show(this._activatedRoute.snapshot.queryParams['id']);
    }

    show(invoicePaymentId?: number): void {

        if (!invoicePaymentId) {
            this.invoicePayment = new CreateOrEditInvoicePaymentDto();
            this.invoicePayment.id = invoicePaymentId;
            this.invoicePayment.invoicePayDate = moment().startOf('day');
            this.invoicePayment.verifiedOn = moment().startOf('day');
            this.invoicePayment.refundDate = moment().startOf('day');
            this.invoicePayment.actualPayDate = moment().startOf('day');
            this.jobNote = '';
            this.userName = '';
            this.userName2 = '';
            this.userName3 = '';
            this.invoicePaymentMethodPaymentMethod = '';

            this.active = true;
        } else {
            this._invoicePaymentsServiceProxy.getInvoicePaymentForEdit(invoicePaymentId).subscribe(result => {
                this.invoicePayment = result.invoicePayment;

                this.jobNote = result.jobNote;
                this.userName = result.userName;
                this.userName2 = result.userName2;
                this.userName3 = result.userName3;
                this.invoicePaymentMethodPaymentMethod = result.invoicePaymentMethodPaymentMethod;
               
                this.active = true;
            });
        }
                    // this._invoicePaymentsServiceProxy.getAllJobForTableDropdown().subscribe(result => {						
					// 	this.allJobs = result;
					// });
		this._invoicePaymentsServiceProxy.getAllUserForTableDropdown().subscribe(result => {						
			this.allUsers = result;
		});
					// this._invoicePaymentsServiceProxy.getAllUserForTableDropdown().subscribe(result => {						
					// 	this.allUsers = result;
					// });
					// this._invoicePaymentsServiceProxy.getAllUserForTableDropdown().subscribe(result => {						
					// 	this.allUsers = result;
					// });
		this._invoicePaymentsServiceProxy.getAllInvoicePaymentMethodForTableDropdown().subscribe(result => {						
			this.allInvoicePaymentMethods = result;
		});
					
    }

    private saveInternal(): Observable<void> {
            this.saving = true;
            
        
        return this._invoicePaymentsServiceProxy.createOrEdit(this.invoicePayment)
         .pipe(finalize(() => { 
            this.saving = false;               
            this.notify.info(this.l('SavedSuccessfully'));
         }));
    }
    
    save(): void {
        this.saveInternal().subscribe(x => {
             this._router.navigate( ['/app/main/invoices/invoicePayments']);
        })
    }
    
    saveAndNew(): void {
        this.saveInternal().subscribe(x => {
            this.invoicePayment = new CreateOrEditInvoicePaymentDto();
        })
    }







}
