﻿import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { InvoicePaymentsServiceProxy, InvoicePaymentDto, GetInvoicePaymentForEditOutput, OrganizationUnitDto, UserServiceProxy, JobsServiceProxy, GetJobForViewDto, JobStatusTableDto, LeadsServiceProxy, LeadStateLookupTableDto, InvoicePaymentInvoicePaymentMethodLookupTableDto, CommonLookupDto, JobFinanceOptionLookupTableDto, JobPaymentOptionLookupTableDto, CommonLookupServiceProxy } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';


import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { VerifyInvoiceModalComponent } from './verify-invoice-model.component';
import { OnInit } from '@angular/core';
import { ViewApplicationModelComponent } from '@app/main/jobs/jobs/view-application-model/view-application-model.component';
import { AddActivityModalComponent } from '@app/main/myleads/myleads/add-activity-model.component';
import { SmsEmailModelComponent } from './sms-email-model/sms-email-model.component';
import { ViewMyLeadComponent } from '@app/main/myleads/myleads/view-mylead.component';

@Component({
    templateUrl: './invoicePayments.component.html',
    styleUrls: ['./invoicePayments.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class InvoicePaymentsComponent extends AppComponentBase implements OnInit {

    show: boolean = true;
    showchild: boolean = true;
    shouldShow: boolean = false;
    organizationUnitlength: number=0;
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };
    @ViewChild('verifyInvoiceModal', { static: true }) verifyInvoiceModal: VerifyInvoiceModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('viewApplicationModal', { static: true }) viewApplicationModal: ViewApplicationModelComponent;
    @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    @ViewChild('invoicepaymentSmsEmailModel', { static: true }) invoicepaymentSmsEmailModel: SmsEmailModelComponent;
    @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
    advancedFiltersAreShown = false;
    filterText = '';
    RefundDateRange: moment.Moment[];
    ActualPayDateRange: moment.Moment[];

    maxRefundDateFilter: moment.Moment;
    minRefundDateFilter: moment.Moment;
    maxPaymentNumberFilter: number;
    maxPaymentNumberFilterEmpty: number;
    minPaymentNumberFilter: number;
    minPaymentNumberFilterEmpty: number;
    isVerifiedFilter = -1;
    receiptNumberFilter = '';
    maxActualPayDateFilter: moment.Moment;
    minActualPayDateFilter: moment.Moment;
    paidCommentFilter = '';
    jobNoteFilter = '';
    userNameFilter = '';
    userName2Filter = '';
    userName3Filter = '';
    jobPaymentOption = 0;
    item: GetInvoicePaymentForEditOutput;
    organizationUnit = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    totalpaymentData = 0;
    totalpaymentVerifyData = 0;
    totalpaymentnotVerifyData = 0;
    totalcount: GetJobForViewDto;
    alljobstatus: JobStatusTableDto[];
    allStates: LeadStateLookupTableDto[];
    suburbSuggestions: string[];
    allInvoicePaymentMethods: InvoicePaymentInvoicePaymentMethodLookupTableDto[];
    allInvoicePaymentStatus: CommonLookupDto[];
    jobFinOptions: JobFinanceOptionLookupTableDto[];
    postCodeSuburbFilter = '';
    stateNameFilter = '';
    jobStatusID: [];
    invoicePaymentMethodId = 0;
    invoicePaymentStatusId = 0;
    financeOptionId = 0;
    totalAmountOfInvoice = 0;
    totalCancelAmount = 0;
    totalAmountReceived = 0;
    balanceOwning = 0;
    invoiceDateNameFilter = 'All';
    startDate: moment.Moment;
    endDate: moment.Moment;
    sampleDateRange: moment.Moment[] = [moment().add(-7, 'days').endOf('day'), moment().add(0, 'days').startOf('day')];
    ExpandedView: boolean = true;
    SelectedLeadId: number = 0;
    jobPaymentOptions: JobPaymentOptionLookupTableDto[];
    totalCustAmountOfInvoice = 0;
    totalCustBAlOwning = 0;
    totalCustAmountReceived = 0;
    totalVICAmountOfInvoice = 0;
    totalVICAmountReceived = 0;
    totalVICBAlOwning = 0;
    totalRefundamount = 0;
    constructor(
        injector: Injector,
        private _invoicePaymentsServiceProxy: InvoicePaymentsServiceProxy,
        private _notifyService: NotifyService,
        private _commonLookupService: CommonLookupServiceProxy,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService,
        private _userServiceProxy: UserServiceProxy,
        private _router: Router,
        private _jobsServiceProxy: JobsServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.getInvoicePayments();
            // this.getCount(this.organizationUnit);
        });
        this._commonLookupService.getAllJobStatusTableDropdown().subscribe(result => {
            this.alljobstatus = result;
        });
        this._commonLookupService.getAllPaymentOptionForTableDropdown().subscribe(result => {
            this.jobPaymentOptions = result;
        });
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });

        this._invoicePaymentsServiceProxy.getAllInvoicePaymentMethodForTableDropdown().subscribe(result => {
            this.allInvoicePaymentMethods = result;
        });
        this._invoicePaymentsServiceProxy.getAllInvoiceStatusForTableDropdown().subscribe(result => {
            this.allInvoicePaymentStatus = result;
        });

        this._commonLookupService.getAllFinanceOptionForTableDropdown().subscribe(result => {
            this.jobFinOptions = result;
        });
    }

    filtersuburbs(event): void {
        this._commonLookupService.getOnlySuburb(event.query).subscribe(output => {
            this.suburbSuggestions = output;
        });
    }

    // getCount(organizationUnit: number) {
    //     this._jobsServiceProxy.getAllApplicationTrackerCount(organizationUnit).subscribe(result => {
    //         this.totalpaymentData = result.totalpaymentData;
    //         this.totalpaymentVerifyData = result.totalpaymentVerifyData;
    //         this.totalpaymentnotVerifyData = result.totalpaymentnotVerifyData;
    //         this.totalAmountOfInvoice = result.totalAmountOfInvoice;
    //         this.totalAmountReceived = result.totalAmountReceived;
    //         this.balanceOwning = result.balanceOwning;
    //         this.totalCancelAmount = result.totalCancelAmount;
    //         this.totalCustAmountOfInvoice = result.totalCustAmountOfInvoice;
    //         this.totalCustBAlOwning = result.totalCustBAlOwning;
    //         this.totalCustAmountReceived = result.totalCustAmountReceived;
    //         this.totalVICAmountOfInvoice = result.totalVICAmountOfInvoice;
    //         this.totalVICAmountReceived = result.totalVICAmountReceived;
    //         this.totalVICBAlOwning = result.totalVICBAlOwning;
    //     });

    // }

    getInvoicePayments(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        if(this.sampleDateRange != null){
            this.startDate = this.sampleDateRange[0];
            this.endDate = this.sampleDateRange[1];
        } else{
            this.startDate = this.sampleDateRange[0];
            this.endDate = this.sampleDateRange[1];
        }

        this.primengTableHelper.showLoadingIndicator();
        this.primengTableHelper.showLoadingIndicator();

        this._invoicePaymentsServiceProxy.getAll(
            this.filterText,
            this.RefundDateRange != undefined && this.RefundDateRange.length > 1 ? this.RefundDateRange[1] : undefined,
            this.RefundDateRange != undefined && this.RefundDateRange.length > 0 ? this.RefundDateRange[0] : undefined,
            this.maxPaymentNumberFilter == null ? this.maxPaymentNumberFilterEmpty : this.maxPaymentNumberFilter,
            this.minPaymentNumberFilter == null ? this.minPaymentNumberFilterEmpty : this.minPaymentNumberFilter,
            this.isVerifiedFilter,
            this.receiptNumberFilter,
            this.ActualPayDateRange != undefined && this.ActualPayDateRange.length > 1 ? this.ActualPayDateRange[1] : undefined,
            this.ActualPayDateRange != undefined && this.ActualPayDateRange.length > 0 ? this.ActualPayDateRange[0] : undefined,
            this.paidCommentFilter,
            this.jobNoteFilter,
            this.userNameFilter,
            this.userName2Filter,
            this.userName3Filter,
            this.jobPaymentOption,
            this.organizationUnit,
            undefined,
            undefined,
            this.postCodeSuburbFilter,
            this.stateNameFilter,
            this.jobStatusID,
            this.invoicePaymentMethodId,
            this.invoicePaymentStatusId,
            this.financeOptionId,
            this.invoiceDateNameFilter,
            this.startDate,
            this.endDate,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
            this.shouldShow = false;
            if (result.totalCount > 0) {
                
                this.totalpaymentData = result.items[0].totalpaymentData;
                this.totalpaymentVerifyData = result.items[0].totalpaymentVerifyData;
                this.totalpaymentnotVerifyData = result.items[0].totalpaymentnotVerifyData;
                this.totalAmountOfInvoice = result.items[0].totalAmountOfInvoice;
                this.totalRefundamount = result.items[0].totalRefundamount;
                this.totalAmountReceived = (result.items[0].totalAmountReceived - result.items[0].totalRefundamount);
                this.balanceOwning = (result.items[0].totalAmountOfInvoice - result.items[0].totalAmountReceived);
                this.totalCancelAmount = (result.items[0].totalCancelAmount);
                this.totalCustAmountOfInvoice = (result.items[0].totalCustAmountOfInvoice - result.items[0].totalVICAmountOfInvoice);
                this.totalCustAmountReceived = (result.items[0].totalCustAmountReceived);
                this.totalCustBAlOwning = (this.totalCustAmountOfInvoice - result.items[0].totalCustAmountReceived);
                this.totalVICAmountOfInvoice = (result.items[0].totalVICAmountOfInvoice);
                this.totalVICAmountReceived = 0;
                this.totalVICBAlOwning = (result.items[0].totalVICAmountOfInvoice - this.totalVICAmountReceived);
            } else {
                this.totalpaymentData = 0;
                this.totalpaymentVerifyData = 0;
                this.totalpaymentnotVerifyData = 0;
                this.totalAmountOfInvoice = 0;
                this.totalRefundamount = 0;
                this.totalAmountReceived = 0;
                this.balanceOwning = 0;
                this.totalCancelAmount = 0;
                this.totalCustAmountOfInvoice = 0;
                this.totalCustAmountReceived = 0;
                this.totalCustBAlOwning = 0;
                this.totalVICAmountOfInvoice = 0;
                this.totalVICAmountReceived = 0;
                this.totalVICBAlOwning = 0;
            }
        });
        // this.getCount(this.organizationUnit);
    }

    reloadPage($event): void {
        this.ExpandedView = true;
        this.paginator.changePage(this.paginator.getPage());
    }

    // createInvoicePayment(): void {
    //     this._router.navigate(['/app/main/invoices/invoicePayments/createOrEdit']);
    // }

    clear() {
        this.filterText = '';
        this.jobPaymentOption = 0;
        this.ActualPayDateRange = null;
        this.postCodeSuburbFilter = '';
        this.stateNameFilter = '';
        this.jobStatusID = [];
        this.invoicePaymentMethodId = 0;
        this.invoicePaymentStatusId = 0;
        this.financeOptionId = 0;
        this.invoiceDateNameFilter = 'All';
        this.sampleDateRange = [moment().add(-7, 'days').endOf('day'), moment().add(0, 'days').startOf('day')];
        this.getInvoicePayments();
    }

    verifyInvoicePayment(invoicePayment): void {
        // this.message.confirm(
        //     this.l('AreYouSureWanttoVerifyInvoice'),
        //     this.l('VerifyInvoice'),
        //     (isConfirmed) => {
        //         if (isConfirmed) {
        //             this._invoicePaymentsServiceProxy.verifyInvoicePayment(invoicePayment)
        //                 .subscribe(() => {
        //                     this.reloadPage();
        //                     this.notify.info(this.l('SavedSuccessfully'));
        //                 });
        //         }
        //     }
        // );
        this.verifyInvoiceModal.show(invoicePayment.id);
    }

    // deleteInvoicePayment(invoicePayment: InvoicePaymentDto): void {
    //     this.message.confirm(
    //         this.l('AreYouSureWanttoDelete'),
    //         this.l('Delete'),
    //         (isConfirmed) => {
    //             if (isConfirmed) {
    //                 this._invoicePaymentsServiceProxy.delete(invoicePayment.id)
    //                     .subscribe(() => {
    //                         this.reloadPage();
    //                         this.notify.success(this.l('SuccessfullyDeleted'));
    //                     });
    //             }
    //         }
    //     );
    // }

    exportToExcel(): void {
        if(this.sampleDateRange != null){
            this.startDate = this.sampleDateRange[0];
            this.endDate = this.sampleDateRange[1];
        } else{
            this.startDate = this.sampleDateRange[0];
            this.endDate = this.sampleDateRange[1];
        }
        this._invoicePaymentsServiceProxy.getInvoicePaymentsToExcel(
            this.filterText,
            this.RefundDateRange != undefined && this.RefundDateRange.length > 1 ? this.RefundDateRange[1] : undefined,
            this.RefundDateRange != undefined && this.RefundDateRange.length > 0 ? this.RefundDateRange[0] : undefined,
            this.maxPaymentNumberFilter == null ? this.maxPaymentNumberFilterEmpty : this.maxPaymentNumberFilter,
            this.minPaymentNumberFilter == null ? this.minPaymentNumberFilterEmpty : this.minPaymentNumberFilter,
            this.isVerifiedFilter,
            this.receiptNumberFilter,
            this.ActualPayDateRange != undefined && this.ActualPayDateRange.length > 1 ? this.ActualPayDateRange[1] : undefined,
            this.ActualPayDateRange != undefined && this.ActualPayDateRange.length > 0 ? this.ActualPayDateRange[0] : undefined,
            this.paidCommentFilter,
            this.jobNoteFilter,
            this.userNameFilter,
            this.userName2Filter,
            this.userName3Filter,
            this.jobPaymentOption,
            this.organizationUnit,
            undefined,
            undefined,
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    quickview(jobid): void {
        this.viewApplicationModal.show(jobid);
    }

    expandGrid() {
        this.ExpandedView = true;
    }

    navigateToLeadDetail(leadid): void {
        this.ExpandedView = !this.ExpandedView;
        this.ExpandedView = false;
        this.SelectedLeadId = leadid;
        this.viewLeadDetail.showDetail(leadid);
    }
}
