import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmsEmailModelComponent } from './sms-email-model.component';

describe('SmsEmailModelComponent', () => {
  let component: SmsEmailModelComponent;
  let fixture: ComponentFixture<SmsEmailModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmsEmailModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmsEmailModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
