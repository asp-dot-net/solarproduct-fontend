﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { InvoicePaymentMethodsServiceProxy, CreateOrEditInvoicePaymentMethodDto,UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';

@Component({
    selector: 'createOrEditInvoicePaymentMethodModal',
    templateUrl: './create-or-edit-invoicePaymentMethod-modal.component.html'
})
export class CreateOrEditInvoicePaymentMethodModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    invoicePaymentMethod: CreateOrEditInvoicePaymentMethodDto = new CreateOrEditInvoicePaymentMethodDto();
    constructor(
        injector: Injector,
        private _invoicePaymentMethodsServiceProxy: InvoicePaymentMethodsServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
    }

    show(invoicePaymentMethodId?: number): void {

        if (!invoicePaymentMethodId) {
            this.invoicePaymentMethod = new CreateOrEditInvoicePaymentMethodDto();
            this.invoicePaymentMethod.id = invoicePaymentMethodId;
            this.active = true;
            this.modal.show();

            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Invoice Payments';
            log.section = 'Invoice Payments';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._invoicePaymentMethodsServiceProxy.getInvoicePaymentMethodForEdit(invoicePaymentMethodId).subscribe(result => {
                this.invoicePaymentMethod = result.invoicePaymentMethod;
                this.active = true;
                this.modal.show();

                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Invoice Payments : ' + this.invoicePaymentMethod.paymentMethod;
                log.section = 'Invoice Payments';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        
    }

    onShown(): void {
        
        document.getElementById('InvoicePaymentMethod_PaymentMethod').focus();
    }

    save(): void {
            this.saving = true;

			
            this._invoicePaymentMethodsServiceProxy.createOrEdit(this.invoicePaymentMethod)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.invoicePaymentMethod.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Invoice Payments Updated : '+ this.invoicePaymentMethod.paymentMethod;
                    log.section = 'Invoice Payments';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Invoice Payments Created : '+ this.invoicePaymentMethod.paymentMethod;
                    log.section = 'Invoice Payments';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }

             });
    }







    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
