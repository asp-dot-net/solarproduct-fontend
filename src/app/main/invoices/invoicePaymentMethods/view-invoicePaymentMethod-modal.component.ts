﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetInvoicePaymentMethodForViewDto, InvoicePaymentMethodDto ,UserActivityLogServiceProxy, UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
@Component({
    selector: 'viewInvoicePaymentMethodModal',
    templateUrl: './view-invoicePaymentMethod-modal.component.html'
})
export class ViewInvoicePaymentMethodModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    item: GetInvoicePaymentMethodForViewDto;
    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
        this.item = new GetInvoicePaymentMethodForViewDto();
        this.item.invoicePaymentMethod = new InvoicePaymentMethodDto();
    }

    show(item: GetInvoicePaymentMethodForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Invoice Payments View : ' +this.item.invoicePaymentMethod.paymentMethod ;
        log.section = 'Invoice Payments';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {            
     });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
