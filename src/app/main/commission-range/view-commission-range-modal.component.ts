﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { CreateOrEditCommissionRangesDto ,UserActivityLogServiceProxy,UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
@Component({
    selector: 'viewcommissionRangeModal',
    templateUrl: './view-commission-range-modal.component.html'
})
export class viewcommissionRangeModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    item: CreateOrEditCommissionRangesDto;
    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
        this.item = new CreateOrEditCommissionRangesDto();
    }

    show(item: CreateOrEditCommissionRangesDto): void {
        debugger
        this.item = item;
        this.active = true;
        this.modal.show();

        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Opened Commission Range View : ' + item.range;
        log.section = 'Commission Range';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {            
         }); 
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
