﻿import { Title } from '@angular/platform-browser';
import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import {  CommissionRangesServiceProxy, CommonLookupServiceProxy, OrganizationUnitDto, UserActivityLogDto,CreateOrEditCommissionRangesDto ,UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { createOrEditCommissionRangeModalComponent } from './commission-range-modal.component';
import { viewcommissionRangeModalComponent } from './view-commission-range-modal.component';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './commission-range.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class CommissionRangeComponent extends AppComponentBase {
    
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('createOrEditCommissionRangeModalComponent', { static: true }) createOrEditCommissionRangeModal: createOrEditCommissionRangeModalComponent;
    @ViewChild('viewcommissionRangeModalComponent', { static: true }) viewcommissionRangeModal: viewcommissionRangeModalComponent;
    
    advancedFiltersAreShown = false;
    filterText = '';
    nameFilter = '';
    firstrowcount = 0;
    last = 0;
    shouldShow: boolean = false;
    date = new Date();
    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);
    public screenHeight: any;  
    testHeight = 250;
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnitlength = 0;
    organizationUnit = 0;
    stHeight = 250;
    
    constructor(
        injector: Injector,
        private _commissionRangeServiceProxy : CommissionRangesServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _commonLookupService : CommonLookupServiceProxy,
        private titleService: Title,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
            ) {
                super(injector);
                this.titleService.setTitle(this.appSession.tenancyName + " |  Commission Range");
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.getCommissionRanges();
        });
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Commission Range';
        log.section = 'Commission Range';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  

    getCommissionRanges(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();
        debugger;

        this._commissionRangeServiceProxy.getAll(
            this.filterText,
            this.organizationUnit,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    // createLeadSource(): void {
    //     this._router.navigate(['/app/main/leadSources/leadSources/createOrEdit']);        
    // }
    searchLog() : void {
        debugger;
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Commission Range';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    deleteLeadSource(createOrEdit: CreateOrEditCommissionRangesDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._commissionRangeServiceProxy.delete(createOrEdit.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete Commission Range: ' + createOrEdit.range;
                            log.section = 'Commission Range';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            }); 
                        });
                }
            }
        );
    }

    // exportToExcel(): void {
    //     this._leadSourcesServiceProxy.getLeadSourcesToExcel(
    //     this.filterText,
    //         this.nameFilter,
    //     )
    //     .subscribe(result => {
    //         this._fileDownloadService.downloadTempFile(result);
    //      });
    // }
    
    
    
    
    
}
