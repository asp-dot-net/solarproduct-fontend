﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { OrganizationUnitDto, CommonLookupServiceProxy, CommissionRangesServiceProxy, CreateOrEditCommissionRangesDto ,UserActivityLogServiceProxy,UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';


@Component({
    selector: 'createOrEditCommissionRangeModal',
    templateUrl: './commission-range-modal.component.html'
})
export class createOrEditCommissionRangeModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    
    isActive = false;
    active = false;
    saving = false;

    CommissionRange: CreateOrEditCommissionRangesDto = new CreateOrEditCommissionRangesDto();
    organizationUnit = [];
    allOrganizationUnits: OrganizationUnitDto[];

    constructor(
        injector: Injector,
        private _commissionRangeServiceProxy: CommissionRangesServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }

    show(CommissionRangeId?: number): void { 
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
            if (!CommissionRangeId) {
                this.CommissionRange = new CreateOrEditCommissionRangesDto();
                this.CommissionRange.id = CommissionRangeId;
                this.active = true;
                this.isActive = true;
                setTimeout  (() => {
                    this.modal.show();
                },2000);
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Create New Commission Range';
                log.section = 'Commission Range';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
    
            } else {
                this._commissionRangeServiceProxy.getCommissionRangeForView(CommissionRangeId).subscribe(result => {
                    this.CommissionRange = result.commissionRangesDto;
                    this.active = true;
                    this.isActive = true;
                    this.modal.show();
                    let log = new UserActivityLogDto();
                    log.actionId = 79;
                    log.actionNote ='Open For Edit Commission Range : ' + this.CommissionRange.range;
                    log.section = 'Commission Range';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    });   
                });
            }  
            
        });

          
        
    }

    save(): void {
        this.saving = true;

        this._commissionRangeServiceProxy.createOrEdit(this.CommissionRange)
            .pipe(finalize(() => { this.saving = false;}))
            .subscribe(() => {
            this.notify.info(this.l('SavedSuccessfully'));
            this.close();
            this.modalSave.emit(null);
            if(this.CommissionRange.id){
                let log = new UserActivityLogDto();
                log.actionId = 82;
                log.actionNote ='Commission Range Updated : '+ this.CommissionRange.range;
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }else{
                let log = new UserActivityLogDto();
                log.actionId = 81;
                log.actionNote ='Commission Range Created : '+ this.CommissionRange.range;
                log.section = 'Commission Range';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            }
        });
    }

    onShown(): void {
        
        document.getElementById('LeadSource_Name').focus();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
