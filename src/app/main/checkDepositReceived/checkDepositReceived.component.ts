import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { UserActivityLogServiceProxy, UserActivityLogDto, CheckDepositeServiceProxy, CheckDepositReceivedDto  } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CreateOrEditCheckReceivedModalComponent } from '../checkDepositReceived/create-or-edit-checkDepositeReceived-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import { Title } from '@angular/platform-browser';
import { finalize } from 'rxjs/operators';
@Component({
    templateUrl: './checkDepositReceived.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class CheckDepositReceivedComponent extends AppComponentBase {
    @ViewChild('createOrEditCheckDepositeReceivedModal', { static: true }) createOrEditCheckDepositeReceivedModal: CreateOrEditCheckReceivedModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    advancedFiltersAreShown = false;
    filterText = '';
    public screenHeight: any;  
    constructor(
        injector: Injector,
        private _checkdepositReceivedServiceProxy: CheckDepositeServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private titleService: Title
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Check Deposit Received");  
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Check Deposit Received';
        log.section = 'Check Deposit Received';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });       
    }
    searchLog() : void {
        debugger;
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Check Deposit Received';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    getAll(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();
        this._checkdepositReceivedServiceProxy.getAll(
            this.filterText,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
        }), err => { this.primengTableHelper.hideLoadingIndicator(); };
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    delete(checkDepositReceived: CheckDepositReceivedDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._checkdepositReceivedServiceProxy.delete(checkDepositReceived.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete Check Deposit Received: ' + checkDepositReceived.name;
                            log.section = 'Check Deposit Received';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            }); 
                        });
                }
            }
        );
    }

     exportToExcel(): void {
        this._checkdepositReceivedServiceProxy.getCheckDepositeReceiveToExcel(
         this.filterText,
         )
         .subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
          });
     }
}
