import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { CheckDepositeServiceProxy, CreateOrEditCheckDepositReceivedDto,UserActivityLogServiceProxy,UserActivityLogDto} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';

@Component({
    selector: 'createOrEditCheckDepositeReceivedModal',
    templateUrl: './create-or-edit-checkDepositeReceived-modal.component.html'
})
export class CreateOrEditCheckReceivedModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    saving = false;

    checkDepositReceived: CreateOrEditCheckDepositReceivedDto  = new CreateOrEditCheckDepositReceivedDto();

    constructor(
        injector: Injector,
        private _checkdepositReceivedServiceProxy: CheckDepositeServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }

    show(checkDepositid?: number): void {

        if (!checkDepositid) {
            this.checkDepositReceived = new CreateOrEditCheckDepositReceivedDto();
            this.checkDepositReceived.id = checkDepositid;
            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Check Deposit Received';
            log.section = 'Check Deposit Received';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 

        } else {
            this._checkdepositReceivedServiceProxy.getCheckDepositReceivedForEdit(checkDepositid).subscribe(result => {
                this.checkDepositReceived = result.checkDepositReceived;
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Check Deposit Received : ' + this.checkDepositReceived.name;
                log.section = 'Check Deposit Received';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 
            });
        } 
    }

    save(): void {
            this.saving = true;	
            this._checkdepositReceivedServiceProxy.createOrEdit(this.checkDepositReceived)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.checkDepositReceived.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Check Deposit Received Updated : '+ this.checkDepositReceived.name;
                    log.section = 'Check Deposit Received';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Check Deposit Received Created : '+ this.checkDepositReceived.name;
                    log.section = 'Check Deposit Received';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
