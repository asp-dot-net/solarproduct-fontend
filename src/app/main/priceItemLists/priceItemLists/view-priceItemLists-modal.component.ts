﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetPriceItemListsForViewDto,UserActivityLogServiceProxy, UserActivityLogDto, PriceItemListsDto, ServiceSourcesDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
@Component({
    selector: 'viewPriceItemListssModal',
    templateUrl: './view-priceItemLists-modal.component.html'
})
export class ViewPriceItemListsModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetPriceItemListsForViewDto;


    constructor(
        injector: Injector,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
        this.item = new GetPriceItemListsForViewDto();
        this.item.priceItemLists = new PriceItemListsDto();
    }

    show(item: GetPriceItemListsForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();

        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote ='Opened Price Item View : ' + this.item.priceItemLists.name;
        log.section = 'Price Item';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {            
         });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
