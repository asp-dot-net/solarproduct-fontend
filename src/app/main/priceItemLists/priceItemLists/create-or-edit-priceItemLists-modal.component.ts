﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { CreateOrEditPriceItemListsDto, PriceItemListsServiceProxy,UserActivityLogServiceProxy, UserActivityLogDto } from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'createOrEditPriceItemListsModal',
    templateUrl: './create-or-edit-PriceItemLists-modal.component.html'
})
export class CreateOrEditPriceItemListsModalComponent extends AppComponentBase {
   
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    PriceItemLists: CreateOrEditPriceItemListsDto = new CreateOrEditPriceItemListsDto();

    constructor(
        injector: Injector,
        private _PriceItemListsServiceProxy: PriceItemListsServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy
    ) {
        super(injector);
    }
    
    show(PriceItemListsId?: number): void {
    

        if (!PriceItemListsId) {
            this.PriceItemLists = new CreateOrEditPriceItemListsDto();
            this.PriceItemLists.id = PriceItemListsId;

            this.active = true;
            this.modal.show();

            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Price Item';
            log.section = 'Price Item';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        } else {
            this._PriceItemListsServiceProxy.getPriceItemListsForEdit(PriceItemListsId).subscribe(result => {
                this.PriceItemLists = result.priceItemLists;
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Price Item : ' + this.PriceItemLists.name;
                log.section = 'Price Item';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
            });
        }
        
    }
    onShown(): void {
        
        document.getElementById('PriceItemLists_PriceItemLists').focus();
    }
    save(): void {
            this.saving = true;

            this._PriceItemListsServiceProxy.createOrEdit(this.PriceItemLists)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.PriceItemLists.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Price Item Updated : '+ this.PriceItemLists.name;
                    log.section = 'Price Item';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Price Item Created : '+ this.PriceItemLists.name;
                    log.section = 'Price Item';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
