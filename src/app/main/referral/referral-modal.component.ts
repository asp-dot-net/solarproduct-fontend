import { Component, ElementRef, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { JobsServiceProxy, GetInstallerInvoiceForViewDto, CreateOrEditJobDto, JobElecDistributorLookupTableDto, JobElecRetailerLookupTableDto, JobRoofAngleLookupTableDto, CommonLookupDto, LeadsServiceProxy, InstallerServiceProxy, JobInstallerInvoicesServiceProxy, CreateOrEditJobInstallerInvoiceDto, GetJobForEditOutput, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import * as _ from 'lodash';
import * as moment from 'moment';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap/modal';
@Component({
  selector: 'referralModal',
  templateUrl: './referral-modal.component.html'
})
export class ReferralModalComponent extends AppComponentBase {
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('addModal', { static: true }) modal: ModalDirective;


  saving = false;
  distAppliedDate: moment.Moment;
  MeterNumber = "";
  ApprovalRefNo = "";
  comment = "";
  item: GetJobForEditOutput;
  refferedJobName: string;
  trackerid = 0;
  filteredLeadSource: any[];
  NoData = false;
  filterdata = false;
  //distdate:; moment.Moment;
  constructor(
    injector: Injector,
    private _jobsServiceProxy: JobsServiceProxy,
    private _router: Router,
    private _leadsServiceProxy: LeadsServiceProxy,
    private _installerServiceProxy: InstallerServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _jobInstallerInvoiceServiceProxy: JobInstallerInvoicesServiceProxy,
  ) {
    super(injector);
    this.item = new GetJobForEditOutput();
        this.item.job = new CreateOrEditJobDto();

  }


    sectionName = '';
    show(leadid: number, trackerid?: number,section = ''): void {
    debugger;
    this.sectionName = section;
    let log = new UserActivityLogDto();
    log.actionId = 79;
    log.actionNote ='Open Referral Notes';
    log.section = section;
    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {
    });
    this.NoData = false;
    this.filterdata = false;
    this.refferedJobName = "";
    this._jobsServiceProxy.getJobForEdit(leadid).subscribe(result => {
      debugger;
      this.item.job = result.job;
      //let event ={query : result.refferedJobName}
      this._jobsServiceProxy.getAlljobNumberforAdd(result.refferedJobName).subscribe(result => {
        //this.filteredLeadSource = result;
        this.refferedJobName = result[0].jobNumber;
        
      });
      
      //this.OnSelectDetails(this.filteredLeadSource[0]);
      this.modal.show();
  });
    // this.modal.show();
  }

  save(): void {
    this.saving = true;
    this.item.job.refferedJobName=this.refferedJobName;
    this.item.job.section = this.sectionName;
    this._jobsServiceProxy.createOrEdit(this.item.job)
        .pipe(finalize(() => { this.saving = false; }))
        .subscribe(() => {
            this.close();
            this.modalSave.emit(null);
            this.notify.info(this.l('SavedSuccessfully'));
            this.comment = "";
        });
  }

  filterjobnumber(event): void {
    debugger;
    this._jobsServiceProxy.getAlljobNumberforAdd(event).subscribe(result => {
      this.filteredLeadSource = result;
      if(result.length > 0){
        this.NoData = false;
        this.filterdata = true;
      }
      else{
        this.NoData = true;
        this.filterdata = false;
      }

      
    });
}

  close(): void {
    this.modal.hide();
  }

  OnSelectDetails(event): void {
    debugger;
    this.refferedJobName = event.jobNumber;
    this.NoData = false;
    this.filterdata = false;
  }

}