import { EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CreateOrEditJobDto, CreateOrEditJobInstallerInvoiceDto, GetJobForEditOutput, JobInstallerInvoicesServiceProxy, JobsServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import * as moment from 'moment';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/internal/operators/finalize';

@Component({
  selector: 'editupdatereferral',
  templateUrl: './editupdate-referral.component.html',
  styleUrls: ['./editupdate-referral.component.css']
})
export class EditupdateReferralComponent extends AppComponentBase {

  @ViewChild('addModal', { static: true }) modal: ModalDirective;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  saving = false;
  distAppliedDate: moment.Moment;
  MeterNumber = "";
  ApprovalRefNo = "";
  item: GetJobForEditOutput;
  comment = "";
    remarks: string;
    date: moment.Moment;
  constructor(
      injector: Injector,
      private _jobServiceProxy: JobsServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _jobInstallerInvoiceServiceProxy: JobInstallerInvoicesServiceProxy,
      private _router: Router
  ) {
      super(injector);
      this.item = new GetJobForEditOutput();
      this.item.job = new CreateOrEditJobDto();
  }

  // show(id: number): void {
  //     this._jobInstallerInvoiceServiceProxy.getInstallerNewList(id).subscribe(result => {      
  //         this.item = result;
  //         this.modal.show();
  //    });
  // }
    sectionName = '';
    show(leadid?: number,section = ''): void {
      this.modal.show();
      debugger;
      this.sectionName = section;
    let log = new UserActivityLogDto();
    log.actionId = 79;
    log.actionNote ='Open Change Paid Date';
    log.section = section;
    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
        .subscribe(() => {
    });
      this._jobServiceProxy.getJobForEdit(leadid).subscribe(result => {
        debugger;
        this.item.job = result.job;
            
          });
      }

  save(): void {
      this.saving = true;
    //   this.item.isPaid = true;
    //   this.item.isVerify = true;
    this.item.job.section = this.sectionName;
      this._jobServiceProxy.createOrEdit(this.item.job)
      .pipe(finalize(() => { this.saving = false;}))
      .subscribe(() => {
          this.modal.hide();
          this.notify.info(this.l('SavedSuccessfully'));
          this.modalSave.emit(null);
          this.remarks = "";
          this.date = null;
      });
  }

  close(): void {
      this.modal.hide();
  }


}
