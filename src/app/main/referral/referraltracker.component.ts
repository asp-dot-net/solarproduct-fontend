import { Component, Injector, ViewChild, ViewEncapsulation, HostListener } from '@angular/core';
import { CommonLookupServiceProxy, GetInstallerInvoiceForViewDto, JobInstallerInvoicesServiceProxy, JobsServiceProxy, JobTrackerServiceProxy, LeadDto, LeadSourceLookupTableDto, LeadsServiceProxy, LeadStateLookupTableDto, LeadStatusLookupTableDto, LeadUsersLookupTableDto, OrganizationUnitDto, ReportServiceProxy, TokenAuthServiceProxy, UserActivityLogDto, UserActivityLogServiceProxy, UserServiceProxy } from '@shared/service-proxies/service-proxies';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { AppConsts } from '@shared/AppConsts';
import { HttpClient } from '@angular/common/http';
import { finalize } from 'rxjs/operators';
import { FileUpload } from 'primeng/fileupload';
import { OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
//import { EditupdateNewinvoiceComponent } from './editupdate-newinvoice/editupdate-newinvoice.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ViewApplicationModelComponent } from '../jobs/jobs/view-application-model/view-application-model.component';
// import { AddActivityModalComponent } from '../myleads/myleads/add-activity-model.component';
// import { ViewMyLeadComponent } from '../myleads/myleads/view-mylead.component';
// import { CreateOrEditJobInvoiceModalComponent } from '../jobs/jobs/edit-jobinvoice-model.component';
import { ReferralModalComponent } from './referral-modal.component';
import { EditupdateReferralComponent } from './editupdate-referral.component';
import { DomSanitizer, Title } from '@angular/platform-browser';
import { SMSModelComponent } from '../activitylog/sms-modal.component';
import { CommentModelComponent } from '../activitylog/comment-modal.component';
import { EmailModelComponent } from '../activitylog/email-modal.component';
import { NotifyModelComponent } from '../activitylog/notify-modal.component';
import { ReminderModalComponent } from '../activitylog/reminder-modal.component';
import { ToDoModalComponent } from '../activitylog/todo-modal.component';
//import { EditinstallerinvoiceComponent } from '../invoiceinstaller/edit-newinstallerinvoice/editinstallerinvoice.component';

@Component({
    selector: 'app-referraltracker',
    templateUrl: './referraltracker.component.html',
    // styleUrls: ['./referraltracker.component.css'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ReferraltrackerComponent extends AppComponentBase implements OnInit {

    show: boolean = true;
    FiltersData = false;
    showchild: boolean = true;
    shouldShow: boolean = false;
    organizationUnitlength: number = 0;
    excelorcsvfile = 0;
    toggleBlock() {
        this.show = !this.show;
    };                                                                        
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };
    @ViewChild('ExcelFileUpload', { static: false }) excelFileUpload: FileUpload;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    @ViewChild('viewApplicationModal', { static: true }) viewApplicationModal: ViewApplicationModelComponent;
    // @ViewChild('addActivityModal') addActivityModal: AddActivityModalComponent;
    // @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
    // @ViewChild('editJobInvoiceModal', { static: true }) editJobInvoiceModal: CreateOrEditJobInvoiceModalComponent;
    @ViewChild('referralModal', { static: true }) referralModal: ReferralModalComponent;

    @ViewChild('editupdatereferral', { static: true }) editupdatereferral: EditupdateReferralComponent;
    @ViewChild('smsModel', { static: true }) smsModel: SMSModelComponent;
    @ViewChild('emailModel', { static: true }) emailModel: EmailModelComponent;
    @ViewChild('notifyModel', { static: true }) notifyModel: NotifyModelComponent;
    @ViewChild('commentModel', { static: true }) commentModel: CommentModelComponent;
    @ViewChild('todoModal', { static: true }) todoModal: ToDoModalComponent;
    @ViewChild('ReminderModal', { static: true }) ReminderModal: ReminderModalComponent;
    //@ViewChild('editinstallerinvoice', { static: true }) editinstallerinvoice: EditinstallerinvoiceComponent;
    uploadUrl: string;
    advancedFiltersAreShown = false;
    filterText = '';
    copanyNameFilter = '';
    emailFilter = '';
    phoneFilter = '';
    mobileFilter = '';
    id = 0;
    addressFilter = '';
    requirementsFilter = '';
    postCodeSuburbFilter = '';
    stateNameFilter = '';
    streetNameFilter = '';
    postCodePostalCode2Filter = '';
    leadSourceNameFilter = '';
    leadSourceIdFilter = 0;
    //leadSubSourceNameFilter = '';
    leadStatusName = '';
    typeNameFilter = '';
    areaNameFilter = '';
    leadStatus: any;

    allLeadStatus: LeadStatusLookupTableDto[];
    leadStatusId: number = 0;
    StartDate: moment.Moment;
    EndDate: moment.Moment;
    PageNo: number;
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnit = 0;
    invoiceType = '';
    suburbSuggestions: string[];
    allStates: LeadStateLookupTableDto[];
    
    dateFilterType = 'DepositeReceived';
    date = new Date();
    // firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    // startDate = moment(this.firstDay);
    // endDate = moment().endOf('day');
    startDate: moment.Moment = moment(this.date);
    endDate: moment.Moment = moment(this.date);

    installerId = '';
    filteredinstaller: string[];
    filteredReps: LeadUsersLookupTableDto[];
    paymentStatus = 1;
    stateid = '';
    salesRepId = 0;
    totalReferral = 0;
    awaitingReferral = 0;
    readytopayReferral = 0;
    paidReferral = 0;
    firstrowcount = 0;
    last = 0;

    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 330;
    toggle: boolean = true;
    filterName = "RefferedProjectNo";
    orgCode = '';
    verify = "All";
    change() {
        this.toggle = !this.toggle;
      }
    
    constructor(
        injector: Injector,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _httpClient: HttpClient,
        private _jobServiceProxy: JobsServiceProxy,
        private _userServiceProxy: UserServiceProxy,
        private _jobTrackerServiceProxy: JobTrackerServiceProxy,
        private _jobInstallerInvoiceServiceProxy: JobInstallerInvoicesServiceProxy,
        private _router: Router,
        private titleService: Title,
        private _fileDownloadService: FileDownloadService,
        private _reportServiceProxy: ReportServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private sanitizer: DomSanitizer,

    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Referral Tracker");
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/Users/ImportLeadFromExcel';
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        this._leadsServiceProxy.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
       
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open Referral Tracker';
            log.section = 'Referral Tracker';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.orgCode = this.allOrganizationUnits[0].code;
            this.getReferrallist();
            debugger
            this._commonLookupService.getSalesRepForFilter(this.organizationUnit, undefined).subscribe(rep => {
                this.filteredReps = rep;
            });
            
        });
        //  this.getCount(this.organizationUnit);
    }
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }
        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }
    
    // onsalesrep() {
    //     debugger
        
    // }
    revertInvoice(id: any): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._jobServiceProxy.revertApprovedInvoice(id)
                        .subscribe(() => {
                            let log = new UserActivityLogDto();
                            log.actionId = 11;
                            log.actionNote ='Payment Return' ;
                            log.section = 'Referral Tracker';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            }); 
                            this.reloadPage();
                            this.notify.success(this.l('Successfullyreverted'));
                        });
                }
            }
        );
    }
    revertNote(id: any): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._jobServiceProxy.revertNote(id)
                        .subscribe(() => {
                            let log = new UserActivityLogDto();
                            log.actionId = 11;
                            log.actionNote ='Note Revert' ;
                            log.section = 'Referral Tracker';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            }); 
                            this.reloadPage();
                            this.notify.success(this.l('Successfullyreverted'));
                        });
                }
            }
        );
    }
    changeOrgCode() {
        this.orgCode = this.allOrganizationUnits.find(x => x.id == this.organizationUnit).code;
    }
    // deleteEdition(edition: GetInstallerInvoiceForViewDto): void {
    //     debugger;
    //     this.message.confirm(

    //         this.l('EditionDeleteWarningMessage', edition.invoiceNo),
    //         this.l('AreYouSure'),
    //         isConfirmed => {
    //             if (isConfirmed) {
    //                 this.id = edition.invoiceinstallerId;
    //                 this._jobInstallerInvoiceServiceProxy.delete(this.id).subscribe(() => {
    //                     this.getReferrallist();
    //                     this.notify.success(this.l('SuccessfullyDeleted'));
    //                 });
    //             }
    //         }
    //     );
    // }

    clear() {
        this.filterText = '';
        this.postCodeSuburbFilter = '';
        this.stateNameFilter = '';
        this.leadSourceIdFilter = 0;
        this.postCodePostalCode2Filter = '';
        this.leadSourceNameFilter = '';
        let date = new Date();
        let firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
        this.startDate = moment(this.date);
        this.endDate = moment(this.date);
        this.getReferrallist();
    }

    filtersuburbs(event): void {
        this._commonLookupService.getOnlySuburb(event.query).subscribe(output => {
            this.suburbSuggestions = output;
        });
    }
    filterinstaller(event): void {

        this._jobInstallerInvoiceServiceProxy.getInstallerForDropdown(event.query).subscribe(result => {
            this.filteredinstaller = result;

        });
    }
    downloadfile1(filename, filepath): void {
        
        let FileName = AppConsts.docUrl + "/" + filepath + filename;
        window.open(FileName, "_blank");

    }
    getReferrallist(event?: LazyLoadEvent) {
        
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();
        var filterText_ = this.filterText;
        if((this.filterName == 'JobNumber' || this.filterName == 'RefferedProjectNo') &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._jobTrackerServiceProxy.getAllReferralTracker(
            this.filterName,
            filterText_,
            this.organizationUnit,
            this.paymentStatus,
            this.stateNameFilter,
            this.salesRepId,
            this.dateFilterType,
            this.startDate,
            this.endDate,
            this.verify,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            console.log(result.totalCount);
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount =  totalrows + 1;
            this.last = totalrows + result.items.length;
            if (result.items.length > 0) {
                this.totalReferral = result.items[0].summary.total;
                this.awaitingReferral = result.items[0].summary.awaiting;
                this.readytopayReferral = result.items[0].summary.readyToPay;
                this.paidReferral = result.items[0].summary.paid;
            } else {
                this.totalReferral = 0;
                this.awaitingReferral = 0;
                this.readytopayReferral = 0;
                this.paidReferral = 0;
            }
            this.primengTableHelper.hideLoadingIndicator();
            this.shouldShow = false;
        });

        // this._jobServiceProxy.getReferralTrackerList(
        //     this.dateFilterType,
        //     this.StartDate,
        //     this.EndDate,
        //     this.organizationUnit,
        //     this.invoiceType,
        //     this.installerId,
        //     this.filterText,
        //     this.paymentStatus,
        //     this.stateid,
        //     this.salesRepId,
        //     undefined,
        //     this.primengTableHelper.getSorting(this.dataTable),
        //     this.primengTableHelper.getSkipCount(this.paginator, event),
        //     this.primengTableHelper.getMaxResultCount(this.paginator, event)
        // ).subscribe(result => {
        //     this.primengTableHelper.totalRecordsCount = result.totalCount;
        //     this.primengTableHelper.records = result.items;
        //     const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
        //     this.firstrowcount =  totalrows + 1;
        //     this.last = totalrows + result.items.length;
        //     debugger;
        //     if (result.items.length > 0) {
        //         this.totalReferral = result.items[0].totalReferral;
        //         this.awaitingReferral = result.items[0].awaitingReferral;
        //         this.readytopayReferral = result.items[0].readytopayReferral;
        //         this.paidReferral = result.items[0].paidReferral;
        //     } else {
        //         this.totalReferral = 0;
        //         this.awaitingReferral = 0;
        //         this.readytopayReferral = 0;
        //         this.paidReferral = 0;
        //     }
        //     this.primengTableHelper.hideLoadingIndicator();
        //     this.shouldShow = false;
        // });
    }

    filterCountries(event): void {
        this._commonLookupService.getAllLeadStatusForTableDropdown(event.query).subscribe(result => {
            this.allLeadStatus = result;
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createLead(): void {
        this._router.navigate(['/app/main/leads/leads/createOrEdit']);
    }

    navigateToLeadDetail(leadid): void {
        this._router.navigate(['/app/main/closedlead/closedlead/viewclosedlead'], { queryParams: { LeadId: leadid } });
    }

    deleteLead(lead: LeadDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    // this._leadsServiceProxy.delete(lead.id)
                    //     .subscribe(() => {
                    //         this.reloadPage();
                    //         this.notify.success(this.l('SuccessfullyDeleted'));
                    //     });
                }
            }
        );
    }

    // getCount(organizationUnit: number) {
    //     this._jobServiceProxy.getAllApplicationTrackerCount(organizationUnit).subscribe(result => {
    //         this.totalReferral = result.totalReferral;
    //         this.awaitingReferral = result.awaitingReferral;
    //         this.readytopayReferral = result.readytopayReferral;
    //         this.paidReferral = result.paidReferral;
    //     });
    // }
    exportToExcel(excelorcsv): void {
        debugger;
        // if (this.primengTableHelper.shouldResetPaging(event)) {
        //     this.paginator.changePage(0);
        //     return;
        // }
        
        this.primengTableHelper.showLoadingIndicator();
        this.excelorcsvfile = excelorcsv;
        var filterText_ = this.filterText;

        if((this.filterName == 'JobNumber' || this.filterName == 'RefferedProjectNo') &&  this.filterText != null && this.filterText.trim() != ''){
            filterText_ = this.orgCode + this.filterText.trim();
        }
        this._jobServiceProxy.getRefferalTrackerToExcel(
            this.filterName,
            filterText_,
            this.organizationUnit,
            this.paymentStatus,
            this.stateNameFilter,
            this.salesRepId,
            this.dateFilterType,
            this.startDate,
            this.endDate,
            this.excelorcsvfile,
            "",
            0,
            25
        ).subscribe(result => {
                    this._fileDownloadService.downloadTempFile(result);
                    this.primengTableHelper.hideLoadingIndicator();
                });
    }

    uploadExcel(data: { files: File }): void {
        const formData: FormData = new FormData();
        const file = data.files[0];
        formData.append('file', file, file.name);

        // this._httpClient
        //     .post<any>(this.uploadUrl, formData)
        //     .pipe(finalize(() => this.excelFileUpload.clear()))
        //     .subscribe(response => {
        //         if (response.success) {
        //             this.notify.success(this.l('ImportLeadsProcessStart'));
        //         } else if (response.error != null) {
        //             this.notify.error(this.l('ImportLeadUploadFailed'));
        //         }
        //     });
    }

    onUploadExcelError(): void {
        this.notify.error(this.l('ImportUsersUploadFailed'));
    }

    verifyorNotVerifyRefferal(id, vefifyornot, msg): void {
        this.message.confirm(
            '',
            "Are You Sure You Want to " + msg,
            (isConfirmed) => {
                if (isConfirmed) {
                    this._jobServiceProxy.verifyorNotVerifyJobRefund(id, vefifyornot)
                        .subscribe(() => {
                            let log = new UserActivityLogDto();
                            log.actionId = 15;
                            log.actionNote = vefifyornot ? 'Job Refund Verify' : 'Job Refund Not Verify';
                            log.section = 'Referral Tracker';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            }); 
                            this.getReferrallist();
                            this.notify.success("Sucessfully " + msg);
                        });
                }
            }
        );
    }

    RefStr: any;
    ReferralPDF(Id,jobnumber): void {
        
        this.spinnerService.show();
        this._reportServiceProxy.getReffarelByJobId(Id).subscribe(result => {
    
            if(result.viewHtml != '')
            {
                this.RefStr = this.GetReferralForm(result);
                this.RefStr = this.sanitizer.bypassSecurityTrustHtml(this.RefStr);
    
                let html = this.RefStr.changingThisBreaksApplicationSecurity;
                this._commonLookupService.downloadPdf(jobnumber, "Referral Form", html).subscribe(result => {
                    this.spinnerService.hide();
                    window.open(result, "_blank");
                },
                err => {
                    this.spinnerService.hide();
                });
            }
        },
        err => {
            this.spinnerService.hide();
        });
    }
    GetReferralForm(result) {
        let htmlstring = result.viewHtml;

        let myTemplateStr = htmlstring;

        console.log(result);
        let myVariables = {
            R: {
                JobNumber: result.jobNumber,
                JobStatus: result.jobStatus,
                EntryDate: result.entryDate,                
                Name: result.name,
                Mobile: result.mobile,
                Email: result.email,
                AddressLine1: result.addressLine1,
                AddressLine2: result.addressLine2,
                SalesRepName: result.salesRepName,
                DepDate: result.depositDate,
                Manager: result.manager,
                NoOfPanels: result.noOfPanels,
                CusBsb: result.cusBSB,
                CusAccNumber: result.cusAccNumber,
                CusAccName: result.cusAccName,
                RefAmount: result.refAmount,
                RefName : result.refName,
                RefProj : result.refProj,
                RefAddress : result.refAddress
            }
        }

        // use custom delimiter {{ }}
        _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;
    
        // interpolate
        let compiled = _.template(myTemplateStr);
        let myTemplateCompiled = compiled(myVariables);
        return myTemplateCompiled;
    }
    addSearchLog(filter): void {
        let log = new UserActivityLogDto();
            log.actionId = 80;
            log.actionNote ='Searched by '+ filter ;
            log.section = 'Referral Tracker';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }

    RereshLog():void{
        let log = new UserActivityLogDto();
        log.actionId = 80;
        log.actionNote ='Refresh the data' ;
        log.section = 'Referral Tracker';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        }); 
    }
}
