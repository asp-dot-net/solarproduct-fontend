import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RejectleadsComponent } from './rejectleads.component';

describe('RejectleadsComponent', () => {
  let component: RejectleadsComponent;
  let fixture: ComponentFixture<RejectleadsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RejectleadsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RejectleadsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
