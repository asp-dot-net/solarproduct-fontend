import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {  CreateOrEditCheckApplicationDto, CheckApplicationsServiceProxy,UserActivityLogServiceProxy,UserActivityLogDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';



@Component({
    selector: 'createOrEditCheckApplicationModal',
    templateUrl: './create-or-edit-checkapplication-modal.component.html'
})
export class CreateOrEditCheckApplicationModalComponent extends AppComponentBase {
   
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    checkApplication: CreateOrEditCheckApplicationDto = new CreateOrEditCheckApplicationDto();

    constructor(
        injector: Injector,
        private _checkApplicationServiceProxy: CheckApplicationsServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }
    
    show(checkApplicationId?: number): void {
    

        if (!checkApplicationId) {
            this.checkApplication = new CreateOrEditCheckApplicationDto();
            this.checkApplication.id = checkApplicationId;
            this.active = true;
            this.modal.show();
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote ='Open For Create New Check Application';
            log.section = 'Check Application';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 

        } else {
            this._checkApplicationServiceProxy.getCheckApplicationForEdit(checkApplicationId).subscribe(result => {
                this.checkApplication = result.chcekApplications;
                this.active = true;
                this.modal.show();
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Check Application : ' + this.checkApplication.name;
                log.section = 'Check Application';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                }); 

            });
        }
        
    }
    onShown(): void {
        
        document.getElementById('CheckApplications_CheckApplications').focus();
    }
    save(): void {
            this.saving = true;
            this._checkApplicationServiceProxy.createOrEdit(this.checkApplication)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.checkApplication.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Check Application Updated : '+ this.checkApplication.name;
                    log.section = 'Check Application';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Check Application Created : '+ this.checkApplication.name;
                    log.section = 'Check Application';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
             });
    }


    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
