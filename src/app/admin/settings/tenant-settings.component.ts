import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { Component, Injector, OnInit } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    SettingScopes,
    SendTestEmailInput,
    SendSMSInput,
    TenantSettingsEditDto,
    TenantSettingsServiceProxy,
    CommonLookupDto,
    CommonLookupServiceProxy,
    EmailSettingsDto
} from '@shared/service-proxies/service-proxies';
import { FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { finalize } from 'rxjs/operators';
import { Title } from '@angular/platform-browser';
import { UserActivityLogDto,UserActivityLogServiceProxy} from '@shared/service-proxies/service-proxies';
@Component({
    templateUrl: './tenant-settings.component.html',
    styleUrls: ['./tenants-settings.component.less'],
    animations: [appModuleAnimation()]
})
export class TenantSettingsComponent extends AppComponentBase implements OnInit {

    usingDefaultTimeZone = false;
    initialTimeZone: string = null;
    testEmailAddress: string = undefined;
    testPhoneNumber: string = undefined;
    setRandomPassword: boolean;

    isMultiTenancyEnabled: boolean = this.multiTenancy.isEnabled;
    showTimezoneSelection: boolean = abp.clock.provider.supportsMultipleTimezone;
    activeTabIndex: number = (abp.clock.provider.supportsMultipleTimezone) ? 0 : 1;
    loading = false;
    settings: TenantSettingsEditDto = undefined;

    logoUploader: FileUploader;
    customCssUploader: FileUploader;

    remoteServiceBaseUrl = AppConsts.remoteServiceBaseUrl;

    defaultTimezoneScope: SettingScopes = SettingScopes.Tenant;

    enabledSocialLoginSettings: string[];
    useFacebookHostSettings: boolean;
    useGoogleHostSettings: boolean;
    useMicrosoftHostSettings: boolean;
    emailProvider: CommonLookupDto[];

    constructor(
        injector: Injector,
        private _tenantSettingsService: TenantSettingsServiceProxy,
        private _tokenService: TokenService,
        private titleService: Title,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _commonLookupServiceProxy: CommonLookupServiceProxy
        ) {
            super(injector);
            this.titleService.setTitle(this.appSession.tenancyName + " |  Settings");
    }

    ngOnInit(): void {
        this.testEmailAddress = this.appSession.user.emailAddress;

        this._commonLookupServiceProxy.getAllEmailProviderDropdown().subscribe((result) => {
            this.emailProvider = result;
            console.log(this.emailProvider);
        });

        this.getSettings();
        this.initUploaders();
        this.loadSocialLoginSettings();

        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Tenant Settings';
        log.section = 'Tenant Settings';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });
    }
    // searchLog() : void {
    //     debugger;
    //     let log = new UserActivityLogDto();
    //         log.actionId =80;
    //         log.actionNote ='Searched by Filters';
    //         log.section = 'Tenant Settings';
    //         this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
    //             .subscribe(() => {
    //         }); 
    // }
    getSettings(): void {
        this.loading = true;
        this._tenantSettingsService.getAllSettings()
            .pipe(finalize(() => {
                this.loading = false;
            }))
            .subscribe((result: TenantSettingsEditDto) => {
                this.settings = result;
                if (this.settings.general) {
                    this.initialTimeZone = this.settings.general.timezone;
                    this.usingDefaultTimeZone = this.settings.general.timezoneForComparison === abp.setting.values['Abp.Timing.TimeZone'];
                }
                this.useFacebookHostSettings = !this.settings.externalLoginProviderSettings.facebook.appId;
                this.useGoogleHostSettings = !this.settings.externalLoginProviderSettings.google.clientId;
                this.useMicrosoftHostSettings = !this.settings.externalLoginProviderSettings.microsoft.clientId;
                console.log(this.settings.emailSetting);

                if(this.settings.emailSetting.length == 0)
                {
                    this.settings.emailSetting.push(new EmailSettingsDto());
                }
            });
    }

    initUploaders(): void {
        this.logoUploader = this.createUploader(
            '/TenantCustomization/UploadLogo',
            result => {
                this.appSession.tenant.logoFileType = result.fileType;
                this.appSession.tenant.logoId = result.id;
            }
        );

        this.customCssUploader = this.createUploader(
            '/TenantCustomization/UploadCustomCss',
            result => {
                this.appSession.tenant.customCssId = result.id;

                let oldTenantCustomCss = document.getElementById('TenantCustomCss');
                if (oldTenantCustomCss) {
                    oldTenantCustomCss.remove();
                }

                let tenantCustomCss = document.createElement('link');
                tenantCustomCss.setAttribute('id', 'TenantCustomCss');
                tenantCustomCss.setAttribute('rel', 'stylesheet');
                tenantCustomCss.setAttribute('href', AppConsts.remoteServiceBaseUrl + '/TenantCustomization/GetCustomCss?tenantId=' + this.appSession.tenant.id);
                document.head.appendChild(tenantCustomCss);
            }
        );
    }

    createUploader(url: string, success?: (result: any) => void): FileUploader {
        const uploader = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + url });

        uploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

        uploader.onSuccessItem = (item, response, status) => {
            const ajaxResponse = <IAjaxResponse>JSON.parse(response);
            if (ajaxResponse.success) {
                this.notify.info(this.l('SavedSuccessfully'));
                if (success) {
                    success(ajaxResponse.result);
                }
            } else {
                this.message.error(ajaxResponse.error.message);
            }
        };

        const uploaderOptions: FileUploaderOptions = {};
        uploaderOptions.authToken = 'Bearer ' + this._tokenService.getToken();
        uploaderOptions.removeAfterUpload = true;
        uploader.setOptions(uploaderOptions);
        return uploader;
    }

    uploadLogo(): void {
        this.logoUploader.uploadAll();
    }

    uploadCustomCss(): void {
        this.customCssUploader.uploadAll();
    }

    clearLogo(): void {
        this._tenantSettingsService.clearLogo().subscribe(() => {
            this.appSession.tenant.logoFileType = null;
            this.appSession.tenant.logoId = null;
            this.notify.info(this.l('ClearedSuccessfully'));
        });
    }

    clearCustomCss(): void {
        this._tenantSettingsService.clearCustomCss().subscribe(() => {
            this.appSession.tenant.customCssId = null;

            let oldTenantCustomCss = document.getElementById('TenantCustomCss');
            if (oldTenantCustomCss) {
                oldTenantCustomCss.remove();
            }

            this.notify.info(this.l('ClearedSuccessfully'));
        });
    }

    saveAll(): void {
        this._tenantSettingsService.updateAllSettings(this.settings).subscribe(() => {
            this.notify.info(this.l('SavedSuccessfully'));
            let log = new UserActivityLogDto();
            log.actionId = 82;
            log.actionNote ='Tenant Settings Updated  '+ this.settings.general.timezone;
            log.section = 'Tenant Settings';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
            if (abp.clock.provider.supportsMultipleTimezone && this.usingDefaultTimeZone && this.initialTimeZone !== this.settings.general.timezone) {
                this.message.info(this.l('TimeZoneSettingChangedRefreshPageNotification')).then(() => {
                    window.location.reload();
                });
            }
        });
    }

    sendTestEmail(): void {
        const input = new SendTestEmailInput();
        input.emailAddress = this.testEmailAddress;
        this._tenantSettingsService.sendTestEmail(input).subscribe(result => {
            this.notify.info(this.l('TestEmailSentSuccessfully'));
        });
    }

    sendTestSMS(): void {
        const input = new SendSMSInput();
        input.phoneNumber = this.testPhoneNumber;
        input.text = "Test SMS";
        this._tenantSettingsService.sendTestSMS(input).subscribe(result => {
            this.notify.info(this.l('TestSMSSentSuccessfully'));
        });
    }
    loadSocialLoginSettings(): void {
        const self = this;
        this._tenantSettingsService.getEnabledSocialLoginSettings()
            .subscribe(setting => {
                self.enabledSocialLoginSettings = setting.enabledSocialLoginSettings;
            });
    }

    clearFacebookSettings(): void {
        this.settings.externalLoginProviderSettings.facebook.appId = '';
        this.settings.externalLoginProviderSettings.facebook.appSecret = '';
    }

    clearGoogleSettings(): void {
        this.settings.externalLoginProviderSettings.google.clientId = '';
        this.settings.externalLoginProviderSettings.google.clientSecret = '';
        this.settings.externalLoginProviderSettings.google.userInfoEndpoint = '';
    }

    clearMicrosoftSettings(): void {
        this.settings.externalLoginProviderSettings.microsoft.clientId = '';
        this.settings.externalLoginProviderSettings.microsoft.clientSecret = '';
    }

    removeSetting(item): void {
        if (this.settings.emailSetting.length == 1)
            return;
        
        if (this.settings.emailSetting.indexOf(item) === -1) {
            
        } else {
            this.settings.emailSetting.splice(this.settings.emailSetting.indexOf(item), 1);
        }
    }

    addSetting(): void {
        this.settings.emailSetting.push(new EmailSettingsDto());
    }

    sendTestEmailSetting(): void {
        const input = new SendTestEmailInput();
        input.emailAddress = this.testEmailAddress;
        this._tenantSettingsService.sendTestEmail(input).subscribe(result => {
            this.notify.info(this.l('TestEmailSentSuccessfully'));
        });
    }
}
