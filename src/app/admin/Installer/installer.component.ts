import { Component, Injector, ViewChild, ViewEncapsulation, AfterViewInit, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppConsts } from '@shared/AppConsts';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { EntityDtoOfInt64, UserListDto, UserServiceProxy, PermissionServiceProxy, FlatPermissionDto, OrganizationUnitDto, CommonLookupServiceProxy    
} from '@shared/service-proxies/service-proxies';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { LazyLoadEvent } from 'primeng/public_api';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FileUpload } from 'primeng/fileupload';
import { finalize } from 'rxjs/operators';
import { PermissionTreeModalComponent } from '../shared/permission-tree-modal.component';
import { ManageEntityDynamicParameterValuesModalComponent } from '@app/admin/dynamic-entity-parameters/entity-dynamic-parameter/entity-dynamic-parameter-value/manage-entity-dynamic-parameter-values-modal.component';
import { InstallerImpersonationService } from './impersonation.service';
// import { InstallerImpersonationService } from '@app/admin/Installer/impersonation.service';
import { CreateOrEditInstallerModalComponent } from './create-or-edit-installer-modal.component';
// import { CreateOrEditInstallerModalComponent } from '@app/admin/Installer/create-or-edit-installer-modal.component';
import { EditInstallerPermissionsModalComponent } from './edit-installer-permissions-modal.component';
// import { EditInstallerPermissionsModalComponent } from '@app/admin/Installer/edit-installer-permissions-modal.component';
import { LocalStorageService } from '@shared/utils/local-storage.service';
import { FileUploader } from 'ng2-file-upload';
import { InstallerContractModalComponent } from './intaller-contract-model';
import {UserActivityLogDto,UserActivityLogServiceProxy} from '@shared/service-proxies/service-proxies';
import { Title } from '@angular/platform-browser';
import { TokenService } from 'abp-ng2-module';

@Component({
    templateUrl: './installer.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./installer.component.less'],    
    animations: [appModuleAnimation()]
})
export class InstallerComponent extends AppComponentBase implements AfterViewInit {
    show: boolean = true;
    showchild: boolean = true;
    
    toggleBlock() {
        this.show = !this.show;
    };
    toggleBlockChild() {
        this.showchild = !this.showchild;
    };

    toggle: boolean = true;

    change() {
        this.toggle = !this.toggle;
      }

    @ViewChild('createOrEditInstallerModal', { static: true }) createOrEditInstallerModal: CreateOrEditInstallerModalComponent;
    @ViewChild('editInstallerPermissionsModal', { static: true }) editInstallerPermissionsModal: EditInstallerPermissionsModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('permissionFilterTreeModal', { static: true }) permissionFilterTreeModal: PermissionTreeModalComponent;
    @ViewChild('ExcelFileUpload', { static: false }) excelFileUpload: FileUpload;
    @ViewChild('dynamicParametersModal', { static: true }) dynamicParametersModal: ManageEntityDynamicParameterValuesModalComponent;
    // @ViewChild('organizationDoc', { static: true }) organizationDoc: OrganizationDocComponent;
    @ViewChild('installerContractModal', { static: true }) installerContractModal: InstallerContractModalComponent;
    
    FiltersData = false;
    uploadUrl: string;
    updateInstallerUrl: string;
    //Filters
    allOrganizationUnits: OrganizationUnitDto[];
    advancedFiltersAreShown = false;
    filterText = '';
    ismyinstalleruser = false;
    role = '';
    organizationUnit = '';
    onlyLockedUsers = false;
    public fileupload: FileUploader;
    firstrowcount = 0;
    last = 0;

    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 330;

    constructor(
        injector: Injector,
        public _impersonationService: InstallerImpersonationService,
        private _userServiceProxy: UserServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _activatedRoute: ActivatedRoute,
        private _httpClient: HttpClient,
        private _localStorageService: LocalStorageService,
        private _router: Router,
        private titleService: Title,
        private _tokenService: TokenService
    ) {
        super(injector);
        // this.titleService.setTitle("SolarProduct | Installer");
        this.titleService.setTitle(this.appSession.tenancyName + " | Installer");
        this.filterText = this._activatedRoute.snapshot.queryParams['filterText'] || '';
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/Users/ImportFromExcel';
        this.updateInstallerUrl = AppConsts.remoteServiceBaseUrl + '/Users/UpdateInstallerFormGreenDeal';
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
        });
    }

    searchLog() : void {
        debugger;
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Installer';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
        
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Installer';
        log.section = 'Installer';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });       
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;  
              
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }
        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }

    ngAfterViewInit(): void {
        this.primengTableHelper.adjustScroll(this.dataTable);
    }

    getUsers(event?: LazyLoadEvent) {

        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);

            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._userServiceProxy.getInstaller(
            '',
            this.filterText,
            this.permissionFilterTreeModal.getSelectedPermissions(),
            this.role !== '' ? parseInt(this.role) : undefined,
            "Installer",
            this.organizationUnit !== '' ? parseInt(this.organizationUnit) : undefined,
            this.onlyLockedUsers,
            this.ismyinstalleruser,
            undefined,
            undefined,
            0,
            0,
            0,
            0,
            "",
            "",
            undefined,
            undefined,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getMaxResultCount(this.paginator, event),
            this.primengTableHelper.getSkipCount(this.paginator, event)
        ).pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator())).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    unlockUser(record): void {
        this._userServiceProxy.unlockSalesRep(new EntityDtoOfInt64({ id: record.id })).subscribe(() => {
            this.notify.success(this.l('UnlockedTheUser', record.userName));
        });
    }

    getRolesAsString(roles): string {
        let roleNames = '';

        for (let j = 0; j < roles.length; j++) {
            if (roleNames.length) {
                roleNames = roleNames + ', ';
            }

            roleNames = roleNames + roles[j].roleName;
        }

        return roleNames;
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    uploadExcel(data: { files: File }): void {
        const formData: FormData = new FormData();
        const file = data.files[0];
        formData.append('file', file, file.name);

        this._httpClient
            .post<any>(this.uploadUrl, formData)
            .pipe(finalize(() => this.excelFileUpload.clear()))
            .subscribe(response => {
                if (response.success) {
                    this.notify.success(this.l('ImportUsersProcessStart'));
                } else if (response.error != null) {
                    this.notify.error(this.l('ImportUsersUploadFailed'));
                }
            }
        );
    }

    onUploadExcelError(): void {
        this.notify.error(this.l('ImportUsersUploadFailed'));
    }

    exportToExcel(): void {
        this._userServiceProxy.getInstallerToExcel(
            '',
            this.filterText,
            this.permissionFilterTreeModal.getSelectedPermissions(),
            this.role !== '' ? parseInt(this.role) : undefined,
            "Installer",
            this.organizationUnit !== '' ? parseInt(this.organizationUnit) : undefined,
            this.onlyLockedUsers,
            this.ismyinstalleruser,
            undefined,
            this.primengTableHelper.getSorting(this.dataTable),
            "",
            0,
            0,
            0,
            0,
            "",
            "",
            undefined,
            undefined)
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            }
        );
    }


    createUser(): void {
        
        this.createOrEditInstallerModal.show();
        // this._router.navigate(['/app/main/installer/createOrEdit']);
    }

    deleteUser(user: UserListDto): void {
        if (user.userName === AppConsts.userManagement.defaultAdminUserName) {
            this.message.warn(this.l('{0}UserCannotBeDeleted', AppConsts.userManagement.defaultAdminUserName));
            return;
        }

        this.message.confirm(
            this.l('UserDeleteWarningMessage', user.userName),
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._userServiceProxy.deleteSalesRep(user.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete Installer : ' + user.userName;
                            log.section = 'Installer';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });
                        }
                    );
                }
            }
        );
    }

    showDynamicParameters(user: UserListDto): void {
        this.dynamicParametersModal.show('TheSolarProduct.Authorization.Users.User', user.id.toString());
    }

    setUsersProfilePictureUrl(users: UserListDto[]): void {
        for (let i = 0; i < users.length; i++) {
            let user = users[i];
            this._localStorageService.getItem(AppConsts.authorization.encrptedAuthTokenName, function (err, value) {
                let profilePictureUrl = AppConsts.remoteServiceBaseUrl + '/Profile/GetProfilePictureByUser?userId=' + user.id + '&' + AppConsts.authorization.encrptedAuthTokenName + '=' + encodeURIComponent(value.token);
                (user as any).profilePictureUrl = profilePictureUrl;
            });
        }
    }

    updateInstallerFromGreenDeal(): void {
        var headers_object = new HttpHeaders().set("Authorization", "Bearer " + this._tokenService.getToken());

        const httpOptions = {
            headers: headers_object
        };

        this._httpClient.post<any>(this.updateInstallerUrl, null, httpOptions)
            .subscribe(response => {
                if (response.success) {
                    this.notify.success(this.l('UpdateInstallerProcessStart'));
                } else if (response.error != null) {
                    this.notify.error(this.l('UpdateInstallerFailed'));
                }
            }
        );
    }
}
