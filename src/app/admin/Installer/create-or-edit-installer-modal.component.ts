import { AfterViewChecked, Component, ElementRef, EventEmitter, Injector, Optional, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    CreateOrUpdateInstallerInput, OrganizationUnitDto, PasswordComplexitySetting, ProfileServiceProxy,
    InstallerEditDto, UserRoleDto, UserServiceProxy,
    LeadStateLookupTableDto, LeadSourceLookupTableDto, LeadsServiceProxy
    , CreateOrEditUserOrgDto, CommonLookupServiceProxy, InstallerServiceProxy, UserIPAddressForEdit
} from '@shared/service-proxies/service-proxies';

import { FileUpload } from 'primeng/fileupload';
import { NotifyService, TokenService, IAjaxResponse } from 'abp-ng2-module';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { HttpClient } from '@angular/common/http';
import { IOrganizationUnitsTreeComponentData, OrganizationUnitsTreeComponent } from '../shared/organization-unit-tree.component';
import * as _ from 'lodash';
import * as moment from 'moment';
import { finalize } from 'rxjs/operators';
import { forEach } from 'lodash';
import { debug } from 'console';
import { base64ToFile, ImageCroppedEvent } from 'ngx-image-cropper';
import { AgmCoreModule } from '@agm/core';
import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';
import PlaceResult = google.maps.places.PlaceResult;
import { Appearance, GermanAddress, Location } from '@angular-material-extensions/google-maps-autocomplete';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { FileUploader, FileUploaderOptions, FileItem } from 'ng2-file-upload';
import { NgxSpinnerService } from 'ngx-spinner';
import {UserActivityLogDto,UserActivityLogServiceProxy} from '@shared/service-proxies/service-proxies';
@Component({
    selector: 'createOrEditInstallerModal',
    templateUrl: './create-or-edit-installer-modal.component.html',
    styles: [`.user-edit-dialog-profile-image {
             margin-bottom: 20px;
        }`
    ],
    styleUrls: ['./create-or-edit-installer-modal.component.less'],
    animations: [appModuleAnimation()]
})

export class CreateOrEditInstallerModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @ViewChild('organizationUnitTree') organizationUnitTree: OrganizationUnitsTreeComponent;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;F
    saving = false;
    canChangeUserName = true;
    isTwoFactorEnabled: boolean = this.setting.getBoolean('Abp.Zero.UserManagement.TwoFactorLogin.IsEnabled');
    isLockoutEnabled: boolean = this.setting.getBoolean('Abp.Zero.UserManagement.UserLockOut.IsEnabled');
    passwordComplexitySetting: PasswordComplexitySetting = new PasswordComplexitySetting();
    maxFilesize = 5;
    isInstaller = false;
    isElectrician = false;
    isDesigner = false
    user: InstallerEditDto = new InstallerEditDto();
    roles: UserRoleDto[];
    sendActivationEmail = true;
    setRandomPassword = true;
    passwordComplexityInfo = '';
    profilePicture: string;
    allOrganizationUnits: OrganizationUnitDto[];
    memberedOrganizationUnits: string[];
    userPasswordRepeat = '';
    allStates: LeadStateLookupTableDto[];
    allLeadSources: LeadSourceLookupTableDto[];
    postCodeSuburb = '';
    stateName = '';
    leadSourceName = '';
    LeadStatusName = '';
    latitude = '';
    longitude = '';
    leadStatus: any;
    unitType: any;
    streetType: any;
    streetName: any;
    suburb: any;
    postalUnitType: any;
    postalStreetType: any;
    postalStreetName: any;
    postalSuburb: any;
    unitTypesSuggestions: string[];
    streetTypesSuggestions: string[];
    streetNamesSuggestions: string[];
    suburbSuggestions: string[];
    postalunitTypesSuggestions: string[];
    postalstreetTypesSuggestions: string[];
    postalstreetNamesSuggestions: string[];
    postalsuburbSuggestions: string[];
    selectAddress: boolean = true;
    installer_uploadUrl: string;
    designer_uploadUrl: string;
    electrician_uploadUrl: string;
    other_uploadUrl: string;

    public filesUpload: FileUploader;
    private _uploaderOptions: FileUploaderOptions = {};
    public maxfileBytesUserFriendlyValue = 5;
    public documentPath = "";
    isInstDate: moment.Moment;
    isDesiDate: moment.Moment;
    isElecDate: moment.Moment;
    role: string = '';
    public instuploader: FileUploader;
    public desiuploader: FileUploader;
    public elecuploader: FileUploader;
    instfiletoken = [];
    instfilename = [];

    desifiletoken = [];
    desifilename = [];

    elecfiletoken = [];
    elecfilename = [];
    fileinstname = "";
    filedesiname = "";
    fileelectname = "";
    userOrganization: CreateOrEditUserOrgDto[]
    editedOrganization: any;

    userIPAddress: UserIPAddressForEdit[];

    constructor(
        injector: Injector,
        private _userService: UserServiceProxy,
        private _profileService: ProfileServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _tokenService: TokenService,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _installerServiceProxy: InstallerServiceProxy,
        private spinner: NgxSpinnerService
    ) {
        super(injector);
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
        this._leadsServiceProxy.getCurrentUserRole().subscribe(result => {
            this.role = result;
        });
    }

    onShown(): void {
        // this.organizationUnitTree.data = <IOrganizationUnitsTreeComponentData>{
        //     allOrganizationUnits: this.allOrganizationUnits,
        //     selectedOrganizationUnits: this.memberedOrganizationUnits
        // };
        document.getElementById('Name').focus();
    }

    google(): void {
        this.selectAddress = true;
    }

    database(): void {
        this.selectAddress = false;
    }

    onAutocompleteSelected(result: PlaceResult) {

        if (result.address_components.length == 7) {
            this.user.streetNo = result.address_components[0].long_name.toUpperCase();
            var str = result.address_components[1].long_name.toUpperCase();
            var splitted = str.split(" ");
            this.streetName = splitted[0].toUpperCase().trim();
            this.streetType = splitted[1].toUpperCase().trim();
            this.suburb = result.address_components[2].long_name.toUpperCase().trim();
            //this.installerAddress1.stateName = result.address_components[4].short_name;
            // this._leadsServiceProxy.getSuburbId(result.address_components[6].long_name).subscribe(result => {						
            //     this.installerAddress1.suburb = result;
            // });
            this._leadsServiceProxy.stateId(result.address_components[4].short_name).subscribe(result => {
                this.user.stateId = result;
            });
            this.user.postCode = result.address_components[6].long_name.toUpperCase();
            this.user.address = this.user.unit + " " + this.user.unitType + " " + this.user.streetNo + " " + this.streetName + " " + this.streetType;  //result.formatted_address;
        }
        else {
            var str1 = result.address_components[0].long_name.toUpperCase();
            var splitted1 = str1.split(" ");
            if (splitted1.length == 1) {
                this.user.unit = splitted1[0].toUpperCase().trim();
            }
            else {
                this.user.unit = splitted1[1].toUpperCase().trim();
                this.unitType = splitted1[0].toUpperCase().trim();
                this.user.unitType = splitted1[0].toUpperCase().trim();
            }
            this.user.streetNo = result.address_components[1].long_name.toUpperCase();
            var str = result.address_components[2].long_name.toUpperCase();
            var splitted = str.split(" ");
            this.streetName = splitted[0].toUpperCase().trim();
            this.streetType = splitted[1].toUpperCase().trim();
            this.suburb = result.address_components[3].long_name.toUpperCase().trim();
            //this.installerAddress1.stateName = result.address_components[5].short_name;
            // this._leadsServiceProxy.getSuburbId(result.address_components[7].long_name).subscribe(result => {						
            //     this.installerAddress1.suburbId = result;
            // });
            this._leadsServiceProxy.stateId(result.address_components[5].short_name).subscribe(result => {
                this.user.stateId = result;
            });
            this.user.postCode = result.address_components[7].long_name.toUpperCase();
            this.user.address = this.user.unit + " " + this.user.unitType + " " + this.user.streetNo + " " + this.streetName + " " + this.streetType;  //result.formatted_address;
        }
    }

    onLocationSelected(location: Location) {
        this.user.latitude = location.latitude.toString();
        this.user.longitude = location.longitude.toString();
    }

    onBeforeSend(event): void {
        event.xhr.setRequestHeader('Authorization', 'Bearer ' + abp.auth.getToken());
    }

    //get Address Drowns : get UnitType
    filterunitTypes(event): void {
        this._leadsServiceProxy.getAllUnitType(event.query).subscribe(output => {
            this.unitTypesSuggestions = output;
        });
    }

    // get StreetType
    filterstreettypes(event): void {
        this._leadsServiceProxy.getAllStreetType(event.query).subscribe(output => {
            this.streetTypesSuggestions = output;
        });
    }

    // get StreetName
    filterstreetnames(event): void {
        this._leadsServiceProxy.getAllStreetName(event.query).subscribe(output => {
            this.streetNamesSuggestions = output;
        });
    }

    // get Suburb
    filtersuburbs(event): void {
        this._leadsServiceProxy.getAllSuburb(event.query).subscribe(output => {
            this.suburbSuggestions = output;
        });
    }

    //get state & Postcode
    fillstatepostcode(): void {
        var splitted = this.suburb.split(" || ");
        this.user.suburb = splitted[0].trim();
        this._leadsServiceProxy.stateId(splitted[1].trim()).subscribe(result => {
            this.user.stateId = result;
        });
        this.user.postCode = splitted[2].trim();
    }
    show(userId?: number): void {
        debugger;
        if (!userId) {
            this.active = true;
            this.setRandomPassword = true;
            this.sendActivationEmail = true;
            this.user.isGoogle = "Google";
            // let log = new UserActivityLogDto();
            // log.actionId = 79;
            // log.actionNote = 'Open For Create New Installer';
            // log.section = 'Installer';
            // this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            //     .subscribe(() => {});
        }
        this.spinner.show();
        this._userService.getInstallerForEdit(userId).subscribe(userResult => {
            if (this.user.isGoogle == "Google") {
                this.selectAddress = true;
            }
            else {
                this.selectAddress = false;
            }
            this.user = userResult.user;
            this.roles = userResult.roles;
            this.canChangeUserName = this.user.userName !== AppConsts.userManagement.defaultAdminUserName;

            this.allOrganizationUnits = userResult.allOrganizationUnits;
            this.memberedOrganizationUnits = userResult.memberedOrganizationUnits;
            this.getProfilePicture(userId);

            this.streetType = this.user.streetType;
            this.streetName = this.user.streetName;
            this.unitType = this.user.unitType;
            this.suburb = this.user.suburb;
            this._userService.userWiseFromEmail(userId).subscribe(result => {
                debugger;
                this.userOrganization = [];
                this.editedOrganization = [];
                result.map((item) => {
                    var userwiseorgandemaildetail = new CreateOrEditUserOrgDto()
                    userwiseorgandemaildetail.id = item.userEmailDetail.id;
                    userwiseorgandemaildetail.userId = item.userEmailDetail.userId;
                    userwiseorgandemaildetail.organizationUnitId = item.userEmailDetail.organizationUnitId;
                    userwiseorgandemaildetail.emailFromAdress = item.userEmailDetail.emailFromAdress;
                    this.userOrganization.push(userwiseorgandemaildetail)
                    this.editedOrganization.push(userwiseorgandemaildetail.organizationUnitId)
                })
                if (result.length == 0) {
                    var userwiseorgandemaildetail = new CreateOrEditUserOrgDto()
                    this.userOrganization.push(userwiseorgandemaildetail);
                    this.editedOrganization.push(userwiseorgandemaildetail.organizationUnitId)
                }
            });
            if (userId) {
                this.active = true;

                setTimeout(() => {
                    this.setRandomPassword = false;
                }, 0);

                this.sendActivationEmail = false;

                this.isInstDate = this.user.installerAccreditationExpiryDate;
                this.isDesiDate = this.user.designerLicenseExpiryDate;
                this.isElecDate = this.user.electricianLicenseExpiryDate;
                this.documentPath = this.user.filePath;
                this.fileinstname = this.user.docInstaller;
                this.filedesiname = this.user.docDesigner;
                this.fileelectname = this.user.docElectrician;
                // let log = new UserActivityLogDto();
                // log.actionId = 79;
                // log.actionNote = 'Open For Edit Installer: ' + this.user.userName;
                // log.section = 'Installer';
                // this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                //     .subscribe(() => {});
                
            }

            this.userIPAddress = [];
            if(userResult.userIPAddress.length == 0)
            {
                this.userIPAddress.push(new UserIPAddressForEdit());
            }
            else {
                this.userIPAddress = userResult.userIPAddress;
            }

            this._profileService.getPasswordComplexitySetting().subscribe(passwordComplexityResult => {
                this.passwordComplexitySetting = passwordComplexityResult.setting;
                this.setPasswordComplexityInfo();
                this.modal.show();
                this.spinner.hide();
                this.initializeModal();
            });
        });
    }
    adduseremailorgDetail(): void {
        this.userOrganization.push(new CreateOrEditUserOrgDto);
    }
    removeJoboldsysdetail(userOrganizationss): void {
        debugger;
        if (this.userOrganization.length == 1)
            return;
        this.userOrganization = this.userOrganization.filter(item => item.organizationUnitId != userOrganizationss.organizationUnitId);
    }
    setPasswordComplexityInfo(): void {

        this.passwordComplexityInfo = '<ul>';

        if (this.passwordComplexitySetting.requireDigit) {
            this.passwordComplexityInfo += '<li>' + this.l('PasswordComplexity_RequireDigit_Hint') + '</li>';
        }

        if (this.passwordComplexitySetting.requireLowercase) {
            this.passwordComplexityInfo += '<li>' + this.l('PasswordComplexity_RequireLowercase_Hint') + '</li>';
        }

        if (this.passwordComplexitySetting.requireUppercase) {
            this.passwordComplexityInfo += '<li>' + this.l('PasswordComplexity_RequireUppercase_Hint') + '</li>';
        }

        if (this.passwordComplexitySetting.requireNonAlphanumeric) {
            this.passwordComplexityInfo += '<li>' + this.l('PasswordComplexity_RequireNonAlphanumeric_Hint') + '</li>';
        }

        if (this.passwordComplexitySetting.requiredLength) {
            this.passwordComplexityInfo += '<li>' + this.l('PasswordComplexity_RequiredLength_Hint', this.passwordComplexitySetting.requiredLength) + '</li>';
        }

        this.passwordComplexityInfo += '</ul>';
    }

    getProfilePicture(userId: number): void {
        if (!userId) {
            this.profilePicture = this.appRootUrl() + 'assets/common/images/default-profile-picture.png';
            return;
        }
        this._profileService.getProfilePictureByUser(userId).subscribe(result => {
            if (result && result.profilePicture) {
                this.profilePicture = 'data:image/jpeg;base64,' + result.profilePicture;
            } else {
                this.profilePicture = this.appRootUrl() + 'assets/common/images/default-profile-picture.png';
            }
        });
    }



    initializeModal(): void {
        this.active = true;
        this.initFileUploader();
        this.initDesiFileUploader();
        this.initElecFileUploader();
    }

    filechangeEvent(event: any): void {
        if (event.target.files[0].size > 5242880) { //5MB
            this.message.warn(this.l('ProfilePicture_Warn_SizeLimit', this.maxfileBytesUserFriendlyValue));
            return;
        }
        this.instuploader.clearQueue();
        this.instuploader.addToQueue([<File>event.target.files[0]]);
        this.instuploader.uploadAll();
    }
    fileisDesichangeEvent(event: any): void {
        if (event.target.files[0].size > 5242880) { //5MB
            this.message.warn(this.l('ProfilePicture_Warn_SizeLimit', this.maxfileBytesUserFriendlyValue));
            return;
        }
        this.desiuploader.clearQueue();
        this.desiuploader.addToQueue([<File>event.target.files[0]]);
        this.desiuploader.uploadAll();
    }
    fileisElecchangeEvent(event: any): void {
        if (event.target.files[0].size > 5242880) { //5MB
            this.message.warn(this.l('ProfilePicture_Warn_SizeLimit', this.maxfileBytesUserFriendlyValue));
            return;
        }
        this.elecuploader.clearQueue();
        this.elecuploader.addToQueue([<File>event.target.files[0]]);
        this.elecuploader.uploadAll();
    }
    initFileUploader(): void {
        this.instuploader = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Profile/UploadFile' });
        this._uploaderOptions.autoUpload = false;
        this._uploaderOptions.authToken = 'Bearer ' + this._tokenService.getToken();
        this._uploaderOptions.removeAfterUpload = true;
        this.instuploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

        this.instuploader.onBuildItemForm = (fileItem: FileItem, form: any) => {
            form.append('FileType', fileItem.file.type);
            form.append('FileName', fileItem.file.name);
            form.append('FileToken', this.instguid());
        };

        this.instuploader.onSuccessItem = (item, response, status) => {
            const resp = <IAjaxResponse>JSON.parse(response);
            if (resp.success) {
                this.instfiletoken.push(resp.result.fileToken);
                this.instfilename.push(resp.result.fileName);
            } else {
                this.message.error(resp.error.message);
            }
        };

        this.instuploader.setOptions(this._uploaderOptions);
    }

    initDesiFileUploader(): void {
        this.desiuploader = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Profile/UploadFile' });
        this._uploaderOptions.autoUpload = false;
        this._uploaderOptions.authToken = 'Bearer ' + this._tokenService.getToken();
        this._uploaderOptions.removeAfterUpload = true;
        this.desiuploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

        this.desiuploader.onBuildItemForm = (fileItem: FileItem, form: any) => {
            form.append('FileType', fileItem.file.type);
            form.append('FileName', fileItem.file.name);
            form.append('FileToken', this.desiguid());
        };

        this.desiuploader.onSuccessItem = (item, response, status) => {
            const resp = <IAjaxResponse>JSON.parse(response);
            if (resp.success) {
                this.desifiletoken.push(resp.result.fileToken);
                this.desifilename.push(resp.result.fileName);
            } else {
                this.message.error(resp.error.message);
            }
        };

        this.desiuploader.setOptions(this._uploaderOptions);
    }

    initElecFileUploader(): void {
        this.elecuploader = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Profile/UploadFile' });
        this._uploaderOptions.autoUpload = false;
        this._uploaderOptions.authToken = 'Bearer ' + this._tokenService.getToken();
        this._uploaderOptions.removeAfterUpload = true;
        this.elecuploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

        this.elecuploader.onBuildItemForm = (fileItem: FileItem, form: any) => {
            form.append('FileType', fileItem.file.type);
            form.append('FileName', fileItem.file.name);
            form.append('FileToken', this.elecguid());
        };

        this.elecuploader.onSuccessItem = (item, response, status) => {
            const resp = <IAjaxResponse>JSON.parse(response);
            if (resp.success) {
                this.elecfiletoken.push(resp.result.fileToken);
                this.elecfilename.push(resp.result.fileName);
            } else {
                this.message.error(resp.error.message);
            }
        };

        this.elecuploader.setOptions(this._uploaderOptions);
    }

    instguid(): string {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }


    desiguid(): string {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }


    elecguid(): string {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }



    save(): void {
        
        let input = new CreateOrUpdateInstallerInput();
        input.assignedRoleNames =
            _.map(
                _.filter(this.roles,
                    {
                        isAssigned: true,
                        inheritedFromOrganizationUnit: false
                    }),
                role => role.roleName
            );

        input.assignedRoleNames.pop();
        input.assignedRoleNames.pop();
        input.assignedRoleNames.push("Installer");//Only Installer
     //   input.organizationUnits = this.organizationUnitTree.getSelectedOrganizations();
        input.userEmailDetail = this.userOrganization;

        input.user = this.user;
        input.setRandomPassword = this.setRandomPassword;
        input.sendActivationEmail = this.sendActivationEmail;
        input.user.target = "N.A.";
        input.user.categoryId = 0;
        input.user.streetName = this.streetName;
        input.user.streetType = this.streetType;
        input.user.unitType = this.unitType;
        input.user.suburb = this.suburb;
        input.user.installerAccreditationExpiryDate = this.isInstDate;
        input.user.designerLicenseExpiryDate = this.isDesiDate;
        input.user.electricianLicenseExpiryDate = this.isElecDate;
        input.user.docInstaller = this.instfiletoken[0];
        input.user.docDesigner = this.desifiletoken[0];
        input.user.docElectrician = this.elecfiletoken[0];
        input.user.docInstallerFileName = this.instfilename[0];
        input.user.docDesignerFileName = this.desifilename[0];
        input.user.docElectricianFileName = this.elecfilename[0];

        input.userIPAddress = [];
        this.userIPAddress.map((item) => {
            let userIP = new UserIPAddressForEdit();
            userIP.ipAdress = item.ipAdress;
            userIP.isActive = item.isActive;
            if (item.ipAdress != undefined && item.ipAdress != '') {
                input.userIPAddress.push(userIP);
            }
        });

        this.saving = true;
        // this.filesUpload.uploadAll();
        this._userService.createOrUpdateInstaller(input)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                if(this.user.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Installer Updated : '+ this.user.userName;
                    log.section = 'Installer';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Installer Created : '+ this.user.userName;
                    log.section = 'Installer';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
                this.close();
                this.instfiletoken = [];
                this.desifiletoken = [];
                this.elecfiletoken = [];
                this.instfilename = [];
                this.desifilename = [];
                this.elecfilename = [];
                this.instuploader = null;
                this.desiuploader = null;
                this.elecuploader = null;
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.userPasswordRepeat = '';
        this.instfiletoken = [];
        this.desifiletoken = [];
        this.elecfiletoken = [];
        this.instfilename = [];
        this.desifilename = [];
        this.elecfilename = [];
        this.instuploader = null;
        this.desiuploader = null;
        this.elecuploader = null;
        this.modal.hide();
    }

    getAssignedRoleCount(): number {
        return _.filter(this.roles, { isAssigned: true }).length;
    }

    downloadInstfile(): void {

        let FileName = AppConsts.docUrl + "/" + this.documentPath + this.fileinstname;
        window.open(FileName, "_blank");
    }
    downloadElecfile(): void {

        let FileName = AppConsts.docUrl + "/" + this.documentPath + this.fileelectname;
        window.open(FileName, "_blank");
    }
    downloadDesifile(): void {

        let FileName = AppConsts.docUrl + "/" + this.documentPath + this.filedesiname;
        window.open(FileName, "_blank");
    }

    checkAccreditationNumber(event): void {
        let userId = this.user.id == null || this.user.id == undefined ? 0 : this.user.id;

        this._installerServiceProxy.checkIntallerAccreditation(userId, event.target.value).subscribe(result => {
            if(result)
            {
                this.notify.warn(this.l('AccreditationNumberAlreadyExists'));
                this.user.installerAccreditationNumber = "";
            }
        });
    }

    addUserIPAddress(): void {
        this.userIPAddress.push(new UserIPAddressForEdit());
    }

    removeUserIPAddress(rowitem): void {
       
        if (this.userIPAddress.indexOf(rowitem) === -1) {
            // this.JobVariations.push(rowitem);
        } else {
            this.userIPAddress.splice(this.userIPAddress.indexOf(rowitem), 1);
        }
    }
}
