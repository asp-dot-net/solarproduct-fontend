import { AfterViewChecked, Component, ElementRef, EventEmitter, Injector, OnInit, Optional, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    CreateOrUpdateInstallerInput, OrganizationUnitDto, PasswordComplexitySetting, ProfileServiceProxy,
    InstallerEditDto, UserRoleDto, UserServiceProxy,
    LeadStateLookupTableDto, LeadSourceLookupTableDto, LeadsServiceProxy, CreateOrEditLeadDto, InstallerAddressDto
    , CreateOrEditInstallerDetailDto, InstallerDetailDto, UpdateProfilePictureInput, UploadDocumentInput, QuotationsServiceProxy, CommonLookupServiceProxy
} from '@shared/service-proxies/service-proxies';

import { FileUpload } from 'primeng/fileupload';
import { NotifyService, TokenService, IAjaxResponse } from 'abp-ng2-module';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { HttpClient } from '@angular/common/http';
import { IOrganizationUnitsTreeComponentData, OrganizationUnitsTreeComponent } from '../shared/organization-unit-tree.component';
import * as _ from 'lodash';
import * as moment from 'moment';
import { finalize } from 'rxjs/operators';
import { forEach } from 'lodash';
import { debug } from 'console';
import { base64ToFile, ImageCroppedEvent } from 'ngx-image-cropper';
import { AgmCoreModule } from '@agm/core';
import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';
import PlaceResult = google.maps.places.PlaceResult;
import { Appearance, GermanAddress, Location } from '@angular-material-extensions/google-maps-autocomplete';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { FileUploader, FileUploaderOptions, FileItem } from 'ng2-file-upload';
import { ActivatedRoute } from '@angular/router';

@Component({
    templateUrl: './create-or-edit-Installer.component.html',
    animations: [appModuleAnimation()]
})

export class CreateOrEditInstallerComponent extends AppComponentBase implements OnInit{

    //@ViewChild('createOrEditModal', {static: true}) modal: ModalDirective;
    @ViewChild('organizationUnitTree') organizationUnitTree: OrganizationUnitsTreeComponent;
    //@Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    canChangeUserName = true;
    isTwoFactorEnabled: boolean = this.setting.getBoolean('Abp.Zero.UserManagement.TwoFactorLogin.IsEnabled');
    isLockoutEnabled: boolean = this.setting.getBoolean('Abp.Zero.UserManagement.UserLockOut.IsEnabled');
    passwordComplexitySetting: PasswordComplexitySetting = new PasswordComplexitySetting();
    maxFilesize = 5;
    isInstaller = false;
    isElectrician = false;
    isDesigner = false
    user: InstallerEditDto = new InstallerEditDto();
    roles: UserRoleDto[];
    sendActivationEmail = true;
    setRandomPassword = true;
    passwordComplexityInfo = '';
    profilePicture: string;
    allOrganizationUnits: OrganizationUnitDto[];
    memberedOrganizationUnits: string[];
    userPasswordRepeat = '';
    allStates: LeadStateLookupTableDto[];
    allLeadSources: LeadSourceLookupTableDto[];
    postCodeSuburb = '';
    stateName = '';
    leadSourceName = '';
    LeadStatusName = '';
    latitude = '';
    longitude = '';
    leadStatus: any;
    unitType: any;
    streetType: any;
    streetName: any;
    suburb: any;
    postalUnitType: any;
    postalStreetType: any;
    postalStreetName: any;
    postalSuburb: any;
    unitTypesSuggestions: string[];
    streetTypesSuggestions: string[];
    streetNamesSuggestions: string[];
    suburbSuggestions: string[];
    postalunitTypesSuggestions: string[];
    postalstreetTypesSuggestions: string[];
    postalstreetNamesSuggestions: string[];
    postalsuburbSuggestions: string[];
    selectAddress: boolean = true;
    installer_uploadUrl: string;
    designer_uploadUrl: string;
    electrician_uploadUrl: string;
    other_uploadUrl: string;

    public filesUpload: FileUploader;
    private _uploaderOptions: FileUploaderOptions = {};
    public maxfileBytesUserFriendlyValue = 5;

    isInstDate: moment.Moment;
    isDesiDate: moment.Moment;
    isElecDate: moment.Moment;

    constructor(
        injector: Injector,
        private _userService: UserServiceProxy,
        private _profileService: ProfileServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _tokenService: TokenService,
        private _httpClient: HttpClient,
        private _commonLookupService: CommonLookupServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _quotationsServiceProxy: QuotationsServiceProxy
    ) {
        super(injector);
        this._commonLookupService.getAllStateForTableDropdown().subscribe(result => {
            this.allStates = result;
        });
    }

    ngOnInit(): void {
        
        this.show(this._activatedRoute.snapshot.queryParams['id']);
        // this.show();
    }

    onShown(): void {
        this.organizationUnitTree.data = <IOrganizationUnitsTreeComponentData>{
            allOrganizationUnits: this.allOrganizationUnits,
            selectedOrganizationUnits: this.memberedOrganizationUnits
        };
    }

    google(): void {
        this.selectAddress = true;
    }

    database(): void {
        this.selectAddress = false;
    }

    onAutocompleteSelected(result: PlaceResult) {

        if (result.address_components.length == 7) {
            this.user.streetNo = result.address_components[0].long_name.toUpperCase();
            var str = result.address_components[1].long_name.toUpperCase();
            var splitted = str.split(" ");
            this.streetName = splitted[0].toUpperCase().trim();
            this.streetType = splitted[1].toUpperCase().trim();
            this.suburb = result.address_components[2].long_name.toUpperCase().trim();
            //this.installerAddress1.stateName = result.address_components[4].short_name;
            // this._leadsServiceProxy.getSuburbId(result.address_components[6].long_name).subscribe(result => {						
            //     this.installerAddress1.suburb = result;
            // });
            this._leadsServiceProxy.stateId(result.address_components[4].short_name).subscribe(result => {
                this.user.stateId = result;
            });
            this.user.postCode = result.address_components[6].long_name.toUpperCase();
            this.user.address = this.user.unit + " " + this.user.unitType + " " + this.user.streetNo + " " + this.streetName + " " + this.streetType;  //result.formatted_address;
        }
        else {
            var str1 = result.address_components[0].long_name.toUpperCase();
            var splitted1 = str1.split(" ");
            if (splitted1.length == 1) {
                this.user.unit = splitted1[0].toUpperCase().trim();
            }
            else {
                this.user.unit = splitted1[1].toUpperCase().trim();
                this.unitType = splitted1[0].toUpperCase().trim();
                this.user.unitType = splitted1[0].toUpperCase().trim();
            }
            this.user.streetNo = result.address_components[1].long_name.toUpperCase();
            var str = result.address_components[2].long_name.toUpperCase();
            var splitted = str.split(" ");
            this.streetName = splitted[0].toUpperCase().trim();
            this.streetType = splitted[1].toUpperCase().trim();
            this.suburb = result.address_components[3].long_name.toUpperCase().trim();
            //this.installerAddress1.stateName = result.address_components[5].short_name;
            // this._leadsServiceProxy.getSuburbId(result.address_components[7].long_name).subscribe(result => {						
            //     this.installerAddress1.suburbId = result;
            // });
            this._leadsServiceProxy.stateId(result.address_components[5].short_name).subscribe(result => {
                this.user.stateId = result;
            });
            this.user.postCode = result.address_components[7].long_name.toUpperCase();
            this.user.address = this.user.unit + " " + this.user.unitType + " " + this.user.streetNo + " " + this.streetName + " " + this.streetType;  //result.formatted_address;
        }
    }

    onLocationSelected(location: Location) {
        this.user.latitude = location.latitude.toString();
        this.user.longitude = location.longitude.toString();
    }

    onBeforeSend(event): void {
        event.xhr.setRequestHeader('Authorization', 'Bearer ' + abp.auth.getToken());
    }

    //get Address Drowns : get UnitType
    filterunitTypes(event): void {
        this._leadsServiceProxy.getAllUnitType(event.query).subscribe(output => {
            this.unitTypesSuggestions = output;
        });
    }

    // get StreetType
    filterstreettypes(event): void {
        this._leadsServiceProxy.getAllStreetType(event.query).subscribe(output => {
            this.streetTypesSuggestions = output;
        });
    }

    // get StreetName
    filterstreetnames(event): void {
        this._leadsServiceProxy.getAllStreetName(event.query).subscribe(output => {
            this.streetNamesSuggestions = output;
        });
    }

    // get Suburb
    filtersuburbs(event): void {
        this._leadsServiceProxy.getAllSuburb(event.query).subscribe(output => {
            this.suburbSuggestions = output;
        });
    }

    //get state & Postcode
    fillstatepostcode(): void {
        var splitted = this.suburb.split(" || ");
        this.user.suburb = splitted[0].trim();
        this._leadsServiceProxy.stateId(splitted[1].trim()).subscribe(result => {
            this.user.stateId = result;
        });
        this.user.postCode = splitted[2].trim();
    }

    show(userId?: number): void {
        if (!userId) {
            this.active = true;
            this.setRandomPassword = true;
            this.sendActivationEmail = true;
            this.user.isGoogle = "Google";
        }
        this._userService.getInstallerForEdit(userId).subscribe(userResult => {
            if (this.user.isGoogle == "Google") {
                this.selectAddress = true;
            }
            else {
                this.selectAddress = false;
            }
            this.user = userResult.user;
            this.roles = userResult.roles;
            this.canChangeUserName = this.user.userName !== AppConsts.userManagement.defaultAdminUserName;

            this.allOrganizationUnits = userResult.allOrganizationUnits;
            this.memberedOrganizationUnits = userResult.memberedOrganizationUnits;
            this.getProfilePicture(userId);

            this.streetType = this.user.streetType;
            this.streetName = this.user.streetName;
            this.unitType = this.user.unitType;
            this.suburb = this.user.suburb;

            if (userId) {
                this.active = true;

                setTimeout(() => {
                    this.setRandomPassword = false;
                }, 0);

                this.sendActivationEmail = false;

                this.isInstDate = this.user.installerAccreditationExpiryDate;
                this.isDesiDate = this.user.designerLicenseExpiryDate;
                this.isElecDate = this.user.electricianLicenseExpiryDate;
            }

            this._profileService.getPasswordComplexitySetting().subscribe(passwordComplexityResult => {
                this.passwordComplexitySetting = passwordComplexityResult.setting;
                this.setPasswordComplexityInfo();
                //this.modal.show();
            });
        });
    }

    setPasswordComplexityInfo(): void {

        this.passwordComplexityInfo = '<ul>';

        if (this.passwordComplexitySetting.requireDigit) {
            this.passwordComplexityInfo += '<li>' + this.l('PasswordComplexity_RequireDigit_Hint') + '</li>';
        }

        if (this.passwordComplexitySetting.requireLowercase) {
            this.passwordComplexityInfo += '<li>' + this.l('PasswordComplexity_RequireLowercase_Hint') + '</li>';
        }

        if (this.passwordComplexitySetting.requireUppercase) {
            this.passwordComplexityInfo += '<li>' + this.l('PasswordComplexity_RequireUppercase_Hint') + '</li>';
        }

        if (this.passwordComplexitySetting.requireNonAlphanumeric) {
            this.passwordComplexityInfo += '<li>' + this.l('PasswordComplexity_RequireNonAlphanumeric_Hint') + '</li>';
        }

        if (this.passwordComplexitySetting.requiredLength) {
            this.passwordComplexityInfo += '<li>' + this.l('PasswordComplexity_RequiredLength_Hint', this.passwordComplexitySetting.requiredLength) + '</li>';
        }

        this.passwordComplexityInfo += '</ul>';
    }

    getProfilePicture(userId: number): void {
        if (!userId) {
            this.profilePicture = this.appRootUrl() + 'assets/common/images/default-profile-picture.png';
            return;
        }
        this._profileService.getProfilePictureByUser(userId).subscribe(result => {
            if (result && result.profilePicture) {
                this.profilePicture = 'data:image/jpeg;base64,' + result.profilePicture;
            } else {
                this.profilePicture = this.appRootUrl() + 'assets/common/images/default-profile-picture.png';
            }
        });
    }

    filechangeEvent(event: any): void {
        
        if (event.target.files[0].size > 5242880) { //5MB
            this.message.warn(this.l('ProfilePicture_Warn_SizeLimit', this.maxfileBytesUserFriendlyValue));
            return;
        }
        this.filesUpload.clearQueue();
        this.filesUpload.addToQueue([<File>event.target.files[0]]);
    }

    initFileUploader(): void {
        this.filesUpload = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Profile/UploadFile' });
        this._uploaderOptions.autoUpload = false;
        this._uploaderOptions.authToken = 'Bearer ' + this._tokenService.getToken();
        this._uploaderOptions.removeAfterUpload = true;
        this.filesUpload.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

        this.filesUpload.onBuildItemForm = (fileItem: FileItem, form: any) => {
            form.append('FileType', fileItem.file.type);
            form.append('FileName', fileItem.file.name);
            form.append('FileToken', this.guid());
        };

        this.filesUpload.onSuccessItem = (item, response, status) => {
            const resp = <IAjaxResponse>JSON.parse(response);
            if (resp.success) {
                this.updateFile(resp.result.fileToken, resp.result.fileName, resp.result.fileType, resp.result.filePath);
            } else {
                this.message.error(resp.error.message);
            }
        };

        this.filesUpload.setOptions(this._uploaderOptions);
    }

    guid(): string {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }

    updateFile(fileToken: string, fileName: string, fileType: string, filePath: string): void {
        const input = new UploadDocumentInput();
        input.fileToken = fileToken;
        input.fileName = fileName;
        input.fileType = fileType;
        input.filePath = filePath;
        this.saving = true;
        this._quotationsServiceProxy.saveDocument(input)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.filesUpload = null;
                this.notify.info(this.l('SavedSuccessfully'));
            });
    }

    save(): void {
        let input = new CreateOrUpdateInstallerInput();

        input.user = this.user;
        input.setRandomPassword = this.setRandomPassword;
        input.sendActivationEmail = this.sendActivationEmail;
        input.user.target = "N.A.";
        input.user.categoryId = 0;
        input.user.streetName = this.streetName;
        input.user.streetType = this.streetType;
        input.user.unitType = this.unitType;
        input.user.suburb = this.suburb;
        this.user.installerAccreditationExpiryDate = this.isInstDate;
        this.user.designerLicenseExpiryDate = this.isDesiDate;
        this.user.electricianLicenseExpiryDate = this.isElecDate;

        input.assignedRoleNames =
            _.map(
                _.filter(this.roles,
                    {
                        isAssigned: true,
                        inheritedFromOrganizationUnit: false
                    }),
                role => role.roleName
            );

        input.assignedRoleNames.pop();
        input.assignedRoleNames.pop();
        input.assignedRoleNames.push("Installer");//Only Installer
        input.organizationUnits = this.organizationUnitTree.getSelectedOrganizations();
        this.saving = true;
        // this.filesUpload.uploadAll();
        this._userService.createOrUpdateInstaller(input)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                //this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.userPasswordRepeat = '';
        //this.modal.hide();
    }

    getAssignedRoleCount(): number {
        return _.filter(this.roles, { isAssigned: true }).length;
    }
}
