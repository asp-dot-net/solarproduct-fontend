import { Component, Injector, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { InstallerContractDto, InstallerDetailsServiceProxy, OrganizationUnitDto, UserServiceProxy } from '@shared/service-proxies/service-proxies';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { FileItem, FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'installerContractModal',
    templateUrl: './intaller-contract-model.html'
})
export class InstallerContractModalComponent extends AppComponentBase {

    @ViewChild('installerModal', { static: true }) modal: ModalDirective;

    active = false;
    saving = false;
    iteam: InstallerContractDto = new InstallerContractDto();
    allOrganizationUnits: OrganizationUnitDto[];
    userwithorgwisedoclist: InstallerContractDto[];
    orgId: any;
    fileName: any;
    userid: any;
    organizationlist: any[];
    public maxfileBytesUserFriendlyValue = 5;
    public uploader: FileUploader;
    private _uploaderOptions: FileUploaderOptions = {};

    constructor(
        injector: Injector,
        private _tokenService: TokenService,
        private _userServiceProxy: UserServiceProxy,
        private _installerdetailServiceProxy: InstallerDetailsServiceProxy
    ) {
        super(injector);
    }

    show(userid: number): void {
        this.userid = userid;
        this._userServiceProxy.getUserWiseOrganizationUnit(userid).subscribe(output => {
            this.allOrganizationUnits = output;
        });
        this._installerdetailServiceProxy.getUserWiseDocList(userid).subscribe(output => {
            this.userwithorgwisedoclist = output;
        });
        this.active = true;
        this.initializeModal();
        this.modal.show();
    }
    initializeModal(): void {
        this.active = true;
        this.initDesiFileUploader();
    }

    initDesiFileUploader(): void {
        this.uploader = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Profile/UploadFile' });
        this._uploaderOptions.autoUpload = false;
        this._uploaderOptions.authToken = 'Bearer ' + this._tokenService.getToken();
        this._uploaderOptions.removeAfterUpload = true;
        this.uploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

        this.uploader.onBuildItemForm = (fileItem: FileItem, form: any) => {
            form.append('FileType', fileItem.file.type);
            form.append('FileName', fileItem.file.name);
            form.append('FileToken', this.guid());
        };

        this.uploader.onSuccessItem = (item, response, status) => {
            const resp = <IAjaxResponse>JSON.parse(response);
            if (resp.success) {
                this.updateFile(resp.result.fileToken, resp.result.fileName, resp.result.fileType, resp.result.filePath);
            } else {
                this.message.error(resp.error.message);
            }
        };
        this.uploader.setOptions(this._uploaderOptions);
    }

    updateFile(fileToken: string, fileName: string, fileType: string, filePath: string): void {
        const input = new InstallerContractDto();
        input.fileToken = fileToken;
        input.fileName = fileName;
        input.userId = this.userid;
        input.organizationId = this.iteam.organizationId;
        this.saving = true;
        this._installerdetailServiceProxy.checkExistList(input.userId, input.organizationId)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(result => {
                if (result == true) {
                    this.message.confirm('Are You Sure You Want To Proceed',
                        "Document Already Exist With this Organization",
                        (isConfirmed) => {
                            if (isConfirmed) {
                                this._installerdetailServiceProxy.createOrEditOrganizationDoc(input)
                                    .pipe(finalize(() => { this.saving = false; }))
                                    .subscribe(() => {
                                        this.notify.info(this.l('SavedSuccessfully'));
                                        this.uploader = null;
                                        this.iteam.organizationId = 0;
                                        this._installerdetailServiceProxy.getUserWiseDocList(this.userid).subscribe(output => {
                                            this.userwithorgwisedoclist = output;
                                        });
                                        this.initializeModal();
                                    });
                            } else {
                                this.uploader = null;
                                this.iteam.organizationId = 0;
                                this.saving = false;
                                this.initializeModal();
                            }
                        }
                    )
                }
                else {
                    this._installerdetailServiceProxy.createOrEditOrganizationDoc(input)
                        .pipe(finalize(() => { this.saving = false; }))
                        .subscribe(() => {
                            this.notify.info(this.l('SavedSuccessfully'));
                            this.uploader = null;
                            this.iteam.organizationId = 0;
                            this._installerdetailServiceProxy.getUserWiseDocList(this.userid).subscribe(output => {
                                this.userwithorgwisedoclist = output;
                            });
                            this.initializeModal();
                        });
                }
            });
    }

    guid(): string {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }

    filechangeEvent(event: any): void {
        if (event.target.files[0].size > 5242880) { //5MB
            this.message.warn(this.l('ProfilePicture_Warn_SizeLimit', this.maxfileBytesUserFriendlyValue));
            return;
        }
        this.uploader.clearQueue();
        this.uploader.addToQueue([<File>event.target.files[0]]);
    }

    save(): void {
        this.uploader.uploadAll();
    }

    downloadfile(file): void {
        let FileName = AppConsts.docUrl + "/" + file.filePath;
        window.open(FileName, "_blank");
    };

    cancel(): void {
        this.modal.hide();
    }
}
