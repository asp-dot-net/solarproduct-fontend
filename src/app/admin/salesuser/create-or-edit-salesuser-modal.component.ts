import { AfterViewChecked, Component, ElementRef, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CreateOrUpdateUserInput, OrganizationUnitDto, PasswordComplexitySetting, ProfileServiceProxy,
     UserEditDto, UserRoleDto, UserServiceProxy,
     CategoryDto, CategoriesServiceProxy,
     TeamDto, TeamsServiceProxy, NameValueOfString,
     UserTeamDto,  UserTeamsServiceProxy, CreateOrEditUserOrgDto, UserIPAddressForEdit
    } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { IOrganizationUnitsTreeComponentData, OrganizationUnitsTreeComponent } from '../shared/organization-unit-tree.component';
import * as _ from 'lodash';
import { finalize } from 'rxjs/operators';
import { UserActivityLogDto, UserActivityLogServiceProxy} from '@shared/service-proxies/service-proxies';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'createOrEditSalesUserModal',
    templateUrl: './create-or-edit-salesuser-modal.component.html',
    styles: [`.user-edit-dialog-profile-image {
             margin-bottom: 20px;
        }`
    ]
})
export class CreateOrEditSalesUserModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', {static: true}) modal: ModalDirective;
    @ViewChild('organizationUnitTree') organizationUnitTree: OrganizationUnitsTreeComponent;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    canChangeUserName = true;
    isTwoFactorEnabled: boolean = this.setting.getBoolean('Abp.Zero.UserManagement.TwoFactorLogin.IsEnabled');
    isLockoutEnabled: boolean = this.setting.getBoolean('Abp.Zero.UserManagement.UserLockOut.IsEnabled');
    passwordComplexitySetting: PasswordComplexitySetting = new PasswordComplexitySetting();

    user: UserEditDto = new UserEditDto();
    roles: UserRoleDto[];
    category: CategoryDto = new CategoryDto();
    categories: CategoryDto[] = [];
    team: TeamDto;
    teams: TeamDto[]=[];
    sendActivationEmail = true;
    setRandomPassword = true;
    passwordComplexityInfo = '';
    profilePicture: string;

    filteredTeams: NameValueOfString[];

    allOrganizationUnits: OrganizationUnitDto[];
    memberedOrganizationUnits: string[];
    userPasswordRepeat = '';
    teamselcted : any
    teamids: NameValueOfString[] = new Array<NameValueOfString>();
    _teamid =  '';
    userOrganization: CreateOrEditUserOrgDto[]
    editedOrganization: any;

    loginUser = "";

    userIPAddress: UserIPAddressForEdit[];

    constructor(
        injector: Injector,
        private _userService: UserServiceProxy,
        private _profileService: ProfileServiceProxy,
        private _categoryService: CategoriesServiceProxy,
        private _teamService: TeamsServiceProxy,
        private _userTeamService: UserTeamsServiceProxy,
        private spinner: NgxSpinnerService,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
    ) {
        super(injector);
    }

    show(userId?: number): void {

        this.loginUser = this.appSession.user.userName;

        this.getCategories();
        this.getTeams();
        
        if (!userId) {
            this.active = true;
            this.setRandomPassword = true;
            this.sendActivationEmail = true;
            this.teamids = [];

            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote = 'Open For Create New Sales Rep';
            log.section = 'Sales Rep';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {});
        }
        this.spinner.show();
        this._userService.getSalesRepForEdit(userId).subscribe(userResult => {
            
            this.user = userResult.user;
            this.roles = userResult.roles;
            this.canChangeUserName = this.user.userName !== AppConsts.userManagement.defaultAdminUserName;
            
            this.allOrganizationUnits = userResult.allOrganizationUnits;
            this.memberedOrganizationUnits = userResult.memberedOrganizationUnits;
            //this.getUserTeam();
            this.getProfilePicture(userId);

            if (userId){

                this._teamService.getAllTeamsAssignedTo(undefined, this.user.id, undefined,undefined,undefined, undefined, undefined,undefined)
                .subscribe(result => {
                    this.teamids = result;
                });
            let log = new UserActivityLogDto();
            log.actionId = 79;
            log.actionNote = 'Open For Edit Sales Rep: ' + this.user.userName;
            log.section = 'Sales Rep';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {});
            }

            this._userService.userWiseFromEmail(userId).subscribe(result => {
                debugger;
                this.userOrganization = [];
                this.editedOrganization = [];
                result.map((item) => {
                    var userwiseorgandemaildetail = new CreateOrEditUserOrgDto()
                    userwiseorgandemaildetail.id = item.userEmailDetail.id;
                    userwiseorgandemaildetail.userId = item.userEmailDetail.userId;
                    userwiseorgandemaildetail.organizationUnitId = item.userEmailDetail.organizationUnitId;
                    userwiseorgandemaildetail.emailFromAdress = item.userEmailDetail.emailFromAdress;
                    this.userOrganization.push(userwiseorgandemaildetail)
                    this.editedOrganization.push(userwiseorgandemaildetail.organizationUnitId)
                })
                if (result.length == 0) {
                    var userwiseorgandemaildetail = new CreateOrEditUserOrgDto()
                    this.userOrganization.push(userwiseorgandemaildetail);
                    this.editedOrganization.push(userwiseorgandemaildetail.organizationUnitId)
                }
            });

            this.userIPAddress = [];
            if(userResult.userIPAddress.length == 0)
            {
                this.userIPAddress.push(new UserIPAddressForEdit());
            }
            else {
                this.userIPAddress = userResult.userIPAddress;
            }

            if (userId) {
                this.active = true;

                setTimeout(() => {
                    this.setRandomPassword = false;
                }, 0);

                this.sendActivationEmail = false;
            }

            this._profileService.getPasswordComplexitySetting().subscribe(passwordComplexityResult => {
                this.passwordComplexitySetting = passwordComplexityResult.setting;
                this.setPasswordComplexityInfo();
                this.modal.show();
                this.spinner.hide();
            });
        });
    }
    adduseremailorgDetail(): void {
        this.userOrganization.push(new CreateOrEditUserOrgDto);
    }
    removeJoboldsysdetail(userOrganizationss): void {
        debugger;
        if (this.userOrganization.length == 1)
            return;
        this.userOrganization = this.userOrganization.filter(item => item.organizationUnitId != userOrganizationss.organizationUnitId);
    }

    // filterTeams(event): void {
    //     this._teamService.getTeams(event.query, this.user.id.toString()).subscribe(teams => {
    //             this.filteredTeams = teams;
    //         });
    //     }

    filterTeams(event): void {
        if(this.user.id){
            this._teamService.getTeams(event.query, this.user.id.toString()).subscribe(teams => {
                this.filteredTeams = teams;
            });
        }
        else{
            this._teamService.getTeams(event.query, "").subscribe(teams => {
                this.filteredTeams = teams;
            });
        }
    }
    
    getUserTeam(): void{
        this._teamid = '';
        if (this.user.id!=null)
        {
                this._userTeamService.getAll(undefined,  this.user.id, undefined, undefined, undefined, undefined, undefined, undefined)
            .subscribe(result => {
                this._teamid = result.items[0].userTeam.teamId.toString();
            });

        }   
    }

    getTeams():void {    
        this.teams = [];
        this._teamService.getAll(undefined, undefined, undefined, undefined, undefined)
        .subscribe(result => {
            result.items.forEach(element => {
                this.teams.push(element.team);
            });
        });
    }

    getCategories(): void {
        this.categories = []
        this._categoryService.getAll(undefined, undefined, undefined, undefined, undefined)
            .subscribe(result => {
                result.items.forEach(element => {
                    this.categories.push(element.category);
                });
            });
    }

    setPasswordComplexityInfo(): void {

        this.passwordComplexityInfo = '<ul>';

        if (this.passwordComplexitySetting.requireDigit) {
            this.passwordComplexityInfo += '<li>' + this.l('PasswordComplexity_RequireDigit_Hint') + '</li>';
        }

        if (this.passwordComplexitySetting.requireLowercase) {
            this.passwordComplexityInfo += '<li>' + this.l('PasswordComplexity_RequireLowercase_Hint') + '</li>';
        }

        if (this.passwordComplexitySetting.requireUppercase) {
            this.passwordComplexityInfo += '<li>' + this.l('PasswordComplexity_RequireUppercase_Hint') + '</li>';
        }

        if (this.passwordComplexitySetting.requireNonAlphanumeric) {
            this.passwordComplexityInfo += '<li>' + this.l('PasswordComplexity_RequireNonAlphanumeric_Hint') + '</li>';
        }

        if (this.passwordComplexitySetting.requiredLength) {
            this.passwordComplexityInfo += '<li>' + this.l('PasswordComplexity_RequiredLength_Hint', this.passwordComplexitySetting.requiredLength) + '</li>';
        }

        this.passwordComplexityInfo += '</ul>';
    }
    getProfilePicture(userId: number): void {
        if (!userId) {
            this.profilePicture = this.appRootUrl() + 'assets/common/images/default-profile-picture.png';
            return;
        }

        this._profileService.getProfilePictureByUser(userId).subscribe(result => {
            if (result && result.profilePicture) {
                this.profilePicture = 'data:image/jpeg;base64,' + result.profilePicture;
            } else {
                this.profilePicture = this.appRootUrl() + 'assets/common/images/default-profile-picture.png';
            }
        });
    }

    // getProfilePicture(profilePictureId: string): void {
    //     if (!profilePictureId) {
    //         this.profilePicture = this.appRootUrl() + 'assets/common/images/default-profile-picture.png';
    //     } else {
    //         this._profileService.getProfilePictureById(profilePictureId).subscribe(result => {

    //             if (result && result.profilePicture) {
    //                 this.profilePicture = 'data:image/jpeg;base64,' + result.profilePicture;
    //             } else {
    //                 this.profilePicture = this.appRootUrl() + 'assets/common/images/default-profile-picture.png';
    //             }
    //         });
    //     }
    // }

    onShown(): void {
        // this.organizationUnitTree.data = <IOrganizationUnitsTreeComponentData>{
        //     allOrganizationUnits: this.allOrganizationUnits,
        //     selectedOrganizationUnits: this.memberedOrganizationUnits
        // };
        
        document.getElementById('Name').focus();
    }

    save(): void {
        let input = new CreateOrUpdateUserInput();
        
        input.user = this.user;
        input.setRandomPassword = this.setRandomPassword;
        input.sendActivationEmail = this.sendActivationEmail;

        // input.teamId =  parseInt(this._teamid);
        input.assignedTeamNames = this.teamids;

        input.assignedRoleNames =
            _.map(
                _.filter(this.roles, 
                    { 
                        isAssigned: true,
                        inheritedFromOrganizationUnit: false }), 
                        role => role.roleName   
            );
        
        input.assignedRoleNames.pop();
        input.assignedRoleNames.push("Sales Rep"); //Only Sales Representative only
        
        //input.organizationUnits = this.organizationUnitTree.getSelectedOrganizations();
        input.userEmailDetail = this.userOrganization;

        input.userIPAddress = [];
        this.userIPAddress.map((item) => {
            let userIP = new UserIPAddressForEdit();
            userIP.ipAdress = item.ipAdress;
            userIP.isActive = item.isActive;
            if (item.ipAdress != undefined && item.ipAdress != '') {
                input.userIPAddress.push(userIP);
            }
        });

        this.saving = true;
        
        this._userService.createOrUpdateSalesRep(input)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                if(this.user.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Sales Rep Updated : '+ this.user.userName;
                    log.section = 'Sales Rep';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Sales Rep Created : '+ this.user.userName;
                    log.section = 'Sales Rep';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
            });
    }

    close(): void {
        this.active = false;
        this.userPasswordRepeat = '';
        this.modal.hide();
    }

    getAssignedRoleCount(): number {
        return _.filter(this.roles, { isAssigned: true }).length;
    }

    addUserIPAddress(): void {
        this.userIPAddress.push(new UserIPAddressForEdit());
    }

    removeUserIPAddress(rowitem): void {
       
        if (this.userIPAddress.indexOf(rowitem) === -1) {
            // this.JobVariations.push(rowitem);
        } else {
            this.userIPAddress.splice(this.userIPAddress.indexOf(rowitem), 1);
        }
    }
}
