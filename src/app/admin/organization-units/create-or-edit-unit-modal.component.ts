import { ChangeDetectorRef, Component, ElementRef, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CreateOrganizationUnitInput, LeadsServiceProxy, OrganizationUnitDto, OrganizationUnitMapDto, OrganizationUnitServiceProxy, STCProviderDto, UpdateOrganizationUnitInput, UserActivityLogDto, UserActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { create } from 'lodash';
import { FileItem, FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

export interface IOrganizationUnitOnEdit {
    id?: number;
    parentId?: number;
    displayName?: string;
    organizationCode?: string;
    projectId?: string;
    greenBoatUsername?: string;
    greenBoatPassword?: string;
    defaultFromAddress?: string;
    defaultFromDisplayName?: string;
    foneDynamicsPhoneNumber?: string;
    foneDynamicsPropertySid?: string;
    foneDynamicsAccountSid?: string;
    foneDynamicsToken?: string;
    email?: string;
    mobile?: string;
    filetoken?: string;
    logoFileName?: string;
    merchantId?: string;
    publishKey?: string;
    secrateKey?: string;
    businesscode?: string;
    authorizationKey?: string;
    westPacSecreteKey?: string;
    westPacPublishKey?: string;
    surchargePublishKey?: string;
    surchargeSecrateKey?: string;
    surchargeAuthorizationKey?: string;
    greenBoatPasswordForFetch?: string;
    greenBoatUsernameForFetch?: string;
    abnNumber?: string;

    mapProvider?: string;
    mapApiKey?: string;

    organizationUnitMaps?: OrganizationUnitMapDto[];
    isDefault?: boolean;

    smsForm?: string;

    accountId?: string;

    gateWayAccountEmail?: string;

    gateWayAccountPassword?: string;
    autoAssignLeadUserId?: number;
    stcProviderList?: STCProviderDto[];
    projectOrderNo?: number;
    sectionName?: string;
}

@Component({
    selector: 'createOrEditOrganizationUnitModal',
    templateUrl: './create-or-edit-unit-modal.component.html'
})
export class CreateOrEditUnitModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @ViewChild('organizationUnitDisplayName', { static: true }) organizationUnitDisplayNameInput: ElementRef;

    @Output() unitCreated: EventEmitter<OrganizationUnitDto> = new EventEmitter<OrganizationUnitDto>();
    @Output() unitUpdated: EventEmitter<OrganizationUnitDto> = new EventEmitter<OrganizationUnitDto>();
    @ViewChild('myInput')
    myInputVariable: ElementRef;
    active = false;
    saving = false;
    public uploader: FileUploader;
    public maxfileBytesUserFriendlyValue = 5;
    private _uploaderOptions: FileUploaderOptions = {};
    organizationUnit: IOrganizationUnitOnEdit = {};
    activeTabIndex: number = (abp.clock.provider.supportsMultipleTimezone) ? 0 : 1;
    filetoken: string;
    fileName: string;
    public temporaryPictureUrl: string;
    public fileupload: FileUploader;
    public temporaryFileUrl: string;
    private _fileuploadoption: FileUploaderOptions = {};
    allUsers = [];
    mapList: any[];
    stcProviderList: any[];
    constructor(
        injector: Injector,
        private _organizationUnitService: OrganizationUnitServiceProxy,
        private _changeDetector: ChangeDetectorRef,
        private _tokenService: TokenService,
        private _userActivityLogServiceProxy: UserActivityLogServiceProxy,
        private _leadsServiceProxy: LeadsServiceProxy
    ) {
        super(injector);
    }

    onShown(): void {
        document.getElementById('OrganizationUnitDisplayName').focus();
    }

    show(organizationUnit: IOrganizationUnitOnEdit): void {
        let log = new UserActivityLogDto();
        log.actionId = 79;
        log.actionNote = !organizationUnit.id ? 'Open For Create New Orgnization' : 'Open For Edit Orgnization : ' + organizationUnit.displayName;
        log.section = "Organization";
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
            });
        this.organizationUnit = organizationUnit;

        console.log(this.organizationUnit.organizationUnitMaps);
        this.mapList = [];
        this._leadsServiceProxy.getSalesManagerUser(organizationUnit.id).subscribe(result => {
            this.allUsers = result;
        });

        if (this.organizationUnit.organizationUnitMaps == undefined || this.organizationUnit.organizationUnitMaps == null) {
            this.mapList.push(new OrganizationUnitMapDto);
        }
        else {
            if (this.organizationUnit.organizationUnitMaps.length > 0) {
                this.organizationUnit.organizationUnitMaps.map((item) => {
                    var mapDetails = new OrganizationUnitMapDto;
                    mapDetails.id = item.id;
                    mapDetails.mapProvider = item.mapProvider,
                        mapDetails.mapApiKey = item.mapApiKey

                    this.mapList.push(mapDetails);
                });
            }
            else {
                this.mapList.push(new OrganizationUnitMapDto);
            }
        }
        this.stcProviderList = [];
        if (this.organizationUnit.stcProviderList == undefined || this.organizationUnit.stcProviderList == null) {
            this.stcProviderList.push(new OrganizationUnitMapDto);
        }
        else {
            if (this.organizationUnit.stcProviderList.length > 0) {
                this.organizationUnit.stcProviderList.map((item) => {
                    var stc = new STCProviderDto;
                    stc.id = item.id;
                    stc.provider = item.provider;
                    stc.name = item.name;
                    stc.userId = item.userId;
                    stc.password = item.password;

                    this.stcProviderList.push(stc);
                });
            }
            else {
                this.stcProviderList.push(new STCProviderDto);
            }
        }
        this.active = true;
        this.initializeModal();
        this.modal.show();
        this._changeDetector.detectChanges();
    }

    save(): void {
        if (!this.organizationUnit.id) {
            this.createUnit();
        } else {
            this.updateUnit();
        }
    }

    createUnit() {
        const createInput = new CreateOrganizationUnitInput();
        createInput.parentId = this.organizationUnit.parentId;
        createInput.displayName = this.organizationUnit.displayName;
        createInput.organizationCode = this.organizationUnit.organizationCode;
        createInput.projectId = this.organizationUnit.projectId;
        createInput.greenBoatUsername = this.organizationUnit.greenBoatUsername;
        createInput.greenBoatPassword = this.organizationUnit.greenBoatPassword;
        createInput.defaultFromAddress = this.organizationUnit.defaultFromAddress;
        createInput.defaultFromDisplayName = this.organizationUnit.defaultFromDisplayName;
        createInput.foneDynamicsPhoneNumber = this.organizationUnit.foneDynamicsPhoneNumber;
        createInput.foneDynamicsPropertySid = this.organizationUnit.foneDynamicsPropertySid;
        createInput.foneDynamicsAccountSid = this.organizationUnit.foneDynamicsAccountSid;
        createInput.email = this.organizationUnit.email;
        createInput.mobile = this.organizationUnit.mobile;
        createInput.fileToken = this.filetoken;
        createInput.logoFileName = this.fileName;
        createInput.publishKey = this.organizationUnit.publishKey;
        createInput.secrateKey = this.organizationUnit.secrateKey;
        createInput.merchantId = this.organizationUnit.merchantId;
        createInput.businesscode = this.organizationUnit.businesscode;
        createInput.authorizationKey = this.organizationUnit.authorizationKey;
        createInput.westPacSecreteKey = this.organizationUnit.westPacSecreteKey;
        createInput.westPacPublishKey = this.organizationUnit.westPacPublishKey;
        createInput.surchargePublishKey = this.organizationUnit.surchargePublishKey;
        createInput.surchargeSecrateKey = this.organizationUnit.surchargeSecrateKey;
        createInput.surchargeAuthorizationKey = this.organizationUnit.surchargeAuthorizationKey;
        createInput.greenBoatPasswordForFetch = this.organizationUnit.greenBoatPasswordForFetch;
        createInput.greenBoatUsernameForFetch = this.organizationUnit.greenBoatUsernameForFetch;
        createInput.abnNumber = this.organizationUnit.abnNumber;
        createInput.organizationUnitMaps = this.mapList;
        createInput.isDefault = this.organizationUnit.isDefault;
        createInput.smsFrom = this.organizationUnit.smsForm;
        createInput.accountId = this.organizationUnit.accountId;
        createInput.gateWayAccountEmail = this.organizationUnit.gateWayAccountEmail;
        createInput.gateWayAccountPassword = this.organizationUnit.gateWayAccountPassword;
        createInput.autoAssignLeadUserId = this.organizationUnit.autoAssignLeadUserId;
        createInput.stcProviderList = this.stcProviderList;
        createInput.projectOrderNo = this.organizationUnit.projectOrderNo;
        this.saving = true;
        this._organizationUnitService
            .createOrganizationUnit(createInput)
            .pipe(finalize(() => this.saving = false))
            .subscribe((result: OrganizationUnitDto) => {
                let log = new UserActivityLogDto();
                log.actionId = 81;
                log.actionNote = 'Organization Created : ' + this.organizationUnit.displayName;
                log.section = this.organizationUnit.sectionName;
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                    });
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.unitCreated.emit(null);
                this.myInputVariable.nativeElement.value = "";
            });
    }

    updateUnit() {
        const updateInput = new UpdateOrganizationUnitInput();
        updateInput.id = this.organizationUnit.id;
        updateInput.displayName = this.organizationUnit.displayName;
        updateInput.organizationCode = this.organizationUnit.organizationCode;
        updateInput.projectId = this.organizationUnit.projectId;
        updateInput.greenBoatUsername = this.organizationUnit.greenBoatUsername;
        updateInput.greenBoatPassword = this.organizationUnit.greenBoatPassword;
        updateInput.defaultFromAddress = this.organizationUnit.defaultFromAddress;
        updateInput.defaultFromDisplayName = this.organizationUnit.defaultFromDisplayName;
        updateInput.foneDynamicsPhoneNumber = this.organizationUnit.foneDynamicsPhoneNumber;
        updateInput.foneDynamicsPropertySid = this.organizationUnit.foneDynamicsPropertySid;
        updateInput.foneDynamicsAccountSid = this.organizationUnit.foneDynamicsAccountSid;
        updateInput.foneDynamicsToken = this.organizationUnit.foneDynamicsToken;
        updateInput.email = this.organizationUnit.email;
        updateInput.mobile = this.organizationUnit.mobile;
        updateInput.fileToken = this.filetoken;
        updateInput.logoFileName = this.fileName;
        updateInput.publishKey = this.organizationUnit.publishKey;
        updateInput.secrateKey = this.organizationUnit.secrateKey;
        updateInput.merchantId = this.organizationUnit.merchantId;
        updateInput.businesscode = this.organizationUnit.businesscode;
        updateInput.authorizationKey = this.organizationUnit.authorizationKey;
        updateInput.westPacSecreteKey = this.organizationUnit.westPacSecreteKey;
        updateInput.westPacPublishKey = this.organizationUnit.westPacPublishKey;
        updateInput.surchargePublishKey = this.organizationUnit.surchargePublishKey;
        updateInput.surchargeSecrateKey = this.organizationUnit.surchargeSecrateKey;
        updateInput.surchargeAuthorizationKey = this.organizationUnit.surchargeAuthorizationKey;
        updateInput.greenBoatPasswordForFetch = this.organizationUnit.greenBoatPasswordForFetch;
        updateInput.greenBoatUsernameForFetch = this.organizationUnit.greenBoatUsernameForFetch;
        updateInput.abnNumber = this.organizationUnit.abnNumber;
        updateInput.organizationUnitMaps = this.mapList;
        updateInput.isDefault = this.organizationUnit.isDefault;
        updateInput.smsFrom = this.organizationUnit.smsForm;
        updateInput.accountId = this.organizationUnit.accountId;
        updateInput.gateWayAccountEmail = this.organizationUnit.gateWayAccountEmail;
        updateInput.gateWayAccountPassword = this.organizationUnit.gateWayAccountPassword;
        updateInput.autoAssignLeadUserId = this.organizationUnit.autoAssignLeadUserId;
        updateInput.stcProviderList = this.stcProviderList;
        updateInput.projectOrderNo = this.organizationUnit.projectOrderNo;
        this.saving = true;
        this._organizationUnitService
            .updateOrganizationUnit(updateInput)
            .pipe(finalize(() => this.saving = false))
            .subscribe((result: OrganizationUnitDto) => {
                let log = new UserActivityLogDto();
                log.actionId = 82;
                log.actionNote = 'Organization Updated : ' + this.organizationUnit.displayName;
                log.section = this.organizationUnit.sectionName;
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                    });
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.unitUpdated.emit(null);
                this.myInputVariable.nativeElement.value = "";
            });
    }

    close(): void {
        this.modal.hide();
        this.active = false;
    }
    initializeModal(): void {
        this.active = true;
        this.temporaryPictureUrl = '';
        this.initFileUploader();
    }
    initFileUploader(): void {
        this.uploader = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Profile/UploadFile' });
        this._uploaderOptions.autoUpload = false;
        this._uploaderOptions.authToken = 'Bearer ' + this._tokenService.getToken();
        this._uploaderOptions.removeAfterUpload = true;
        this.uploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

        this.uploader.onBuildItemForm = (fileItem: FileItem, form: any) => {
            form.append('FileType', fileItem.file.type);
            form.append('FileName', 'ProfilePicture');
            form.append('FileToken', this.guid());
        };

        this.uploader.onSuccessItem = (item, response, status) => {

            const resp = <IAjaxResponse>JSON.parse(response);
            if (resp.success) {
                this.filetoken = resp.result.fileToken;
                this.fileName = resp.result.fileName;
            } else {
                this.message.error(resp.error.message);
            }
        };

        this.uploader.setOptions(this._uploaderOptions);
    }


    guid(): string {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }

    fileChangeEvent(event: any): void {
        if (event.target.files[0].size > 5242880) { //5MB
            this.message.warn(this.l('ProfilePicture_Warn_SizeLimit', this.maxfileBytesUserFriendlyValue));
            return;
        }
        this.uploader.clearQueue();
        this.uploader.addToQueue([<File>event.target.files[0]]);
        this.uploader.uploadAll();
    }

    //MapItem: OrganizationUnitMapDto = { mapProvider: '', mapApiKey: '' };
    addPicklistProduct(): void {
        this.mapList.push(new OrganizationUnitMapDto);
    }

    removePicklistProduct(obj): void {

        if (this.mapList.length == 1)
            return;
        this.mapList = this.mapList.filter(item => item.mapProvider != obj.mapProvider);
        //this.calculateSTCRebate(0);

        if (this.mapList.length == 0) {
            this.addPicklistProduct();
        }
    }

    addSTCProvider(): void {
        this.stcProviderList.push(new STCProviderDto);
    }

    removeSTCProvider(obj): void {

        if (this.stcProviderList.length == 1)
            return;

        // if (this.stcProviderList.indexOf(obj) === -1) {

        // } else {
        this.stcProviderList.splice(this.stcProviderList.indexOf(obj), 1);
        //}

        //this.calculateSTCRebate(0);

        if (this.stcProviderList.length == 0) {
            this.addSTCProvider();
        }
    }
}
