import { Component, Injector, ViewChild, ViewEncapsulation, AfterViewInit, HostListener } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppConsts } from '@shared/AppConsts';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { EntityDtoOfInt64, UserListDto, UserServiceProxy,  FlatPermissionDto, OrganizationUnitDto, CommonLookupServiceProxy } from '@shared/service-proxies/service-proxies';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { LazyLoadEvent } from 'primeng/public_api';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { HttpClient } from '@angular/common/http';
import { FileUpload } from 'primeng/fileupload';
import { finalize } from 'rxjs/operators';
import { PermissionTreeModalComponent } from '../shared/permission-tree-modal.component';
import { ManageEntityDynamicParameterValuesModalComponent } from '@app/admin/dynamic-entity-parameters/entity-dynamic-parameter/entity-dynamic-parameter-value/manage-entity-dynamic-parameter-values-modal.component';
import { SalesManagerImpersonationService } from '../salesmanager/impersonation.service';
import { CreateOrEditSalesManagerModalComponent } from './create-or-edit-salesmanager-modal.component';
import { EditSalesManagerPermissionsModalComponent } from './edit-salesmanager-permissions-modal.component';
import { Title } from '@angular/platform-browser';
import { UserActivityLogDto,UserActivityLogServiceProxy} from '@shared/service-proxies/service-proxies';
@Component({
    templateUrl: './salesmanager.component.html',
    styleUrls: ['./salesmanager.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class SalesManagerComponent extends AppComponentBase implements AfterViewInit {
    show: boolean = true;
    showchild: boolean = true;
    toggleBlock(){
        this.show = !this.show;
    };
    toggleBlockChild(){
        this.showchild = !this.showchild;
    };
    
    @ViewChild('createOrEditSalesManagerModal', { static: true }) createOrEditSalesManagerModal: CreateOrEditSalesManagerModalComponent;
    @ViewChild('editSalesManagerPermissionsModal', { static: true }) editSalesManagerPermissionsModal: EditSalesManagerPermissionsModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('permissionFilterTreeModal', { static: true }) permissionFilterTreeModal: PermissionTreeModalComponent;
    @ViewChild('ExcelFileUpload', { static: false }) excelFileUpload: FileUpload;
    @ViewChild('dynamicParametersModal', { static: true }) dynamicParametersModal: ManageEntityDynamicParameterValuesModalComponent;

    FiltersData = false;
    uploadUrl: string;
    //Filters
    allOrganizationUnits: OrganizationUnitDto[];
    advancedFiltersAreShown = false;
    filterText = '';
    role = '';
    organizationUnit = '';
    rolename = 'Sales Manager';
    onlyLockedUsers = false;
    firstrowcount = 0;
    last = 0;

    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 330;
   
    constructor(
        injector: Injector,
        public _impersonationService: SalesManagerImpersonationService,
        private _userServiceProxy: UserServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _activatedRoute: ActivatedRoute,
         private _commonLookupService: CommonLookupServiceProxy,
         private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private _httpClient: HttpClient,
        private titleService: Title
        ) {
            super(injector);
            this.titleService.setTitle(this.appSession.tenancyName + " |  Sales Manager");
        this.filterText = this._activatedRoute.snapshot.queryParams['filterText'] || '';
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/Users/ImportFromExcel';
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
        });
    }

    searchLog() : void {
        debugger;
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Sales Manager';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Sales Manager';
        log.section = 'Sales Manager';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });       
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82 ;
        }
        else {
            this.testHeight = this.testHeight - -82 ;
        }
    }

    ngAfterViewInit(): void {
        this.primengTableHelper.adjustScroll(this.dataTable);
    }

    getUsers(event?: LazyLoadEvent) { 

        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);

            return;
        }
        
        this.primengTableHelper.showLoadingIndicator();
        this._userServiceProxy.getSalesManager(
            this.filterText,
            this.permissionFilterTreeModal.getSelectedPermissions(),
            this.role !== '' ? parseInt(this.role) : undefined,
            this.rolename = "Sales Manager",
            this.organizationUnit !== '' ? parseInt(this.organizationUnit): undefined,
            this.onlyLockedUsers,
            undefined,
            undefined,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getMaxResultCount(this.paginator, event),
            this.primengTableHelper.getSkipCount(this.paginator, event)
        ).pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator())).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }
    
    unlockUser(record): void {
        this._userServiceProxy.unlockSalesManager(new EntityDtoOfInt64({ id: record.id })).subscribe(() => {
            this.notify.success(this.l('UnlockedTheUser', record.userName));
        });
    }

    getRolesAsString(roles): string {
        let roleNames = '';

        for (let j = 0; j < roles.length; j++) {
            if (roleNames.length) {
                roleNames = roleNames + ', ';
            }

            roleNames = roleNames + roles[j].roleName;
        }

        return roleNames;
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    exportToExcel(): void {
        this._userServiceProxy.getSalesManagerToExcel(
            this.filterText,
            this.permissionFilterTreeModal.getSelectedPermissions(),
            this.role !== '' ? parseInt(this.role) : undefined,
            this.rolename = "Sales Manager",
            this.organizationUnit !== '' ? parseInt(this.organizationUnit): undefined,
            this.onlyLockedUsers,
            undefined,
            undefined,
            this.primengTableHelper.getSorting(this.dataTable))
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    createUser(): void {
        this.createOrEditSalesManagerModal.show();
    }
    uploadExcel(data: { files: File }): void {
        const formData: FormData = new FormData();
        const file = data.files[0];
        formData.append('file', file, file.name);

        this._httpClient
            .post<any>(this.uploadUrl, formData)
            .pipe(finalize(() => this.excelFileUpload.clear()))
            .subscribe(response => {
                if (response.success) {
                    this.notify.success(this.l('ImportUsersProcessStart'));
                } else if (response.error != null) {
                    this.notify.error(this.l('ImportUsersUploadFailed'));
                }
            });
    }

    onUploadExcelError(): void {
        this.notify.error(this.l('ImportUsersUploadFailed'));
    }

    deleteUser(user: UserListDto): void {
        if (user.userName === AppConsts.userManagement.defaultAdminUserName) {
            this.message.warn(this.l('{0}UserCannotBeDeleted', AppConsts.userManagement.defaultAdminUserName));
            return;
        }

        this.message.confirm(
            this.l('UserDeleteWarningMessage', user.userName),
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._userServiceProxy.deleteSalesManager(user.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    showDynamicParameters(user: UserListDto): void {
        this.dynamicParametersModal.show('TheSolarDemo.Authorization.Users.User', user.id.toString());
    }
}
