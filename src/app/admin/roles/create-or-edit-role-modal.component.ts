import { Component, ElementRef, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CreateOrUpdateRoleInput, RoleEditDto, RoleServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { PermissionTreeComponent } from '../shared/permission-tree.component';
import { finalize } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import {UserActivityLogDto,UserActivityLogServiceProxy} from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'createOrEditRoleModal',
    styleUrls: ['./roles.component.less'],
    templateUrl: './create-or-edit-role-modal.component.html'
})
export class CreateOrEditRoleModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', {static: true}) modal: ModalDirective;
    @ViewChild('permissionTree') permissionTree: PermissionTreeComponent;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    role: RoleEditDto = new RoleEditDto();
    constructor(
        injector: Injector,
        private _roleService: RoleServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private spinner: NgxSpinnerService
    ) {
        super(injector);
    }

    show(roleId?: number): void {
        const self = this;
        self.active = true;
        this.spinner.show();
        debugger
        if(roleId){
            self._roleService.getRoleForEdit(roleId).subscribe(result => {
                self.role = result.role;
                this.permissionTree.editData = result;
                self.modal.show(); });
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Edit Roles : ' + self.role.displayName;
                log.section = 'Roles';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            });
        }
        else{
            this.role = new RoleEditDto();
            
                let log = new UserActivityLogDto();
                log.actionId = 79;
                log.actionNote ='Open For Create New Roles';
                log.section = 'Roles';
                this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                    .subscribe(() => {
                });
                this.modal.show();
        }
       this.spinner.hide();

           
            
       
    }
    
    onShown(): void {
        document.getElementById('RoleDisplayName').focus();
    }

    save(): void {
        const self = this;

        const input = new CreateOrUpdateRoleInput();
        input.role = self.role;
        input.grantedPermissionNames = self.permissionTree.getGrantedPermissionNames();

        this.saving = true;
        this._roleService.createOrUpdateRole(input)
            .pipe(finalize(() => this.saving = false))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);

                if(this.role.id){
                    let log = new UserActivityLogDto();
                    log.actionId = 82;
                    log.actionNote ='Roles Updated : '+ this.role.displayName;
                    log.section = 'Roles';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }else{
                    let log = new UserActivityLogDto();
                    log.actionId = 81;
                    log.actionNote ='Roles Created : '+ this.role.displayName;
                    log.section = 'Roles';
                    this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                        .subscribe(() => {
                    }); 
                }
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
