import { Component, Injector, ViewChild, OnInit, HostListener } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { RoleListDto, RoleServiceProxy, PermissionServiceProxy, FlatPermissionDto } from '@shared/service-proxies/service-proxies';
import { Table } from 'primeng/table';
import { CreateOrEditRoleModalComponent } from './create-or-edit-role-modal.component';
import { EntityTypeHistoryModalComponent } from '@app/shared/common/entityHistory/entity-type-history-modal.component';
import * as _ from 'lodash';
import { finalize } from 'rxjs/operators';
import { PermissionTreeModalComponent } from '../shared/permission-tree-modal.component';
import { Title } from '@angular/platform-browser';
import { LazyLoadEvent, Paginator } from 'primeng';
import {UserActivityLogDto,UserActivityLogServiceProxy} from '@shared/service-proxies/service-proxies';
@Component({
    templateUrl: './roles.component.html',
    styleUrls: ['./roles.component.less'],
    animations: [appModuleAnimation()]
})
export class RolesComponent extends AppComponentBase  {

    @ViewChild('createOrEditRoleModal', { static: true }) createOrEditRoleModal: CreateOrEditRoleModalComponent;
    @ViewChild('entityTypeHistoryModal', { static: true }) entityTypeHistoryModal: EntityTypeHistoryModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('permissionFilterTreeModal', { static: true }) permissionFilterTreeModal: PermissionTreeModalComponent;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    _entityTypeFullName = 'TheSolarProduct.Authorization.Roles.Role';
    entityHistoryEnabled = false;
    firstrowcount = 0;
    last = 0;

    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 250;

    toggle: boolean = true;

    change() {
        this.toggle = !this.toggle;
      }

    constructor(
        injector: Injector,
        private _roleService: RoleServiceProxy,
        private _userActivityLogServiceProxy : UserActivityLogServiceProxy,
        private titleService: Title
    ) {
        super(injector);
        this.titleService.setTitle(this.appSession.tenancyName + " |  Roles");
       
            
    }

    
    searchLog() : void {
        debugger;
        let log = new UserActivityLogDto();
            log.actionId =80;
            log.actionNote ='Searched by Filters';
            log.section = 'Roles';
            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                .subscribe(() => {
            }); 
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight; 
        this.setIsEntityHistoryEnabled();
        let log = new UserActivityLogDto();
        log.actionId =79;
        log.actionNote ='Open Roles';
        log.section = 'Roles';
        this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
            .subscribe(() => {
        });       
    }
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    
    private setIsEntityHistoryEnabled(): void {
        let customSettings = (abp as any).custom;
        this.entityHistoryEnabled = customSettings.EntityHistory && customSettings.EntityHistory.isEnabled && _.filter(customSettings.EntityHistory.enabledEntities, entityType => entityType === this._entityTypeFullName).length === 1;
    }

    getRoles(event?: LazyLoadEvent): void {
        debugger
        this.primengTableHelper.showLoadingIndicator();
        let selectedPermissions = this.permissionFilterTreeModal.getSelectedPermissions();

        this._roleService.getRoles(selectedPermissions)
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe(result => {
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.totalRecordsCount = result.items.length;
                const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
                this.firstrowcount = totalrows + 1;
                this.last = totalrows + result.items.length;
                this.primengTableHelper.hideLoadingIndicator();
            });
                   
    }

    createRole(): void {
        this.createOrEditRoleModal.show();
    }

    showHistory(role: RoleListDto): void {
        this.entityTypeHistoryModal.show({
            entityId: role.id.toString(),
            entityTypeFullName: this._entityTypeFullName,
            entityTypeDescription: role.displayName
        });
    }

    deleteRole(role: RoleListDto): void {
        let self = this;
        self.message.confirm(
            self.l('RoleDeleteWarningMessage', role.displayName),
            this.l('AreYouSure'),
            isConfirmed => {
                if (isConfirmed) {
                    this._roleService.deleteRole(role.id).subscribe(() => {
                        this.getRoles();
                        abp.notify.success(this.l('SuccessfullyDeleted'));

                            let log = new UserActivityLogDto();
                            log.actionId = 83;
                            log.actionNote ='Delete Roles: ' + role.displayName;
                            log.section = 'Roles';
                            this._userActivityLogServiceProxy.create(log).pipe(finalize(() => { }))
                                .subscribe(() => {
                            });
                    });
                }
            }
        );
    }
}
