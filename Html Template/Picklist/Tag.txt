{{PL.JobNumber}}
{{PL.Date}}

{{PL.Cust.Name}}
{{PL.Cust.Mobile}}
{{PL.Cust.Email}}
{{PL.Cust.AddressLine1}}
{{PL.Cust.AddressLine2}}

{{PL.Cust.ManualQuoteNo}}
{{PL.Cust.StoreLoc}}

{{PL.Cust.MeterPhase}}
{{PL.Cust.MeterUpgrad}}
{{PL.Cust.RoofType}}
{{PL.Cust.PropertyType}}
{{PL.Cust.RoofPitch}}
{{PL.Cust.ElecDist}}
{{PL.Cust.ElecRetailer}}
{{PL.Cust.NMINumber}}
{{PL.Cust.MeterNo}}

{{PL.Cust.DateString}}  -- Only for Arise. Format => March 17, 2022 (Tue)
{{PL.Cust.DateString}}

{{PL.ID.InstallerName}}
{{PL.ID.InstallationDate}}
{{PL.ID.SA.AddressLine1}}
{{PL.ID.SA.AddressLine2}}

{{PL.SN.Notes}}


{{PL.PicklistItemDetails}}

<tr><th>category</th><th>Item</th><th>Model</th><th style='width:30px'>QTY</th><th>Done</th></tr>

{{PL.CustomerSign.PickedBy}}
{{PL.CustomerSign.PickedDate}}
{{PL.CustomerSign.SignSrc}}